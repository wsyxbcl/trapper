=================
 TRAPPER PROJECT
=================

.. image:: trapper/trapper-project/trapper/apps/common/static/images/logo/logo_text.png

Overview
--------

Trapper is an open source, `django <https://www.djangoproject.com/>`_ based web application to manage `camera trapping <https://en.wikipedia.org/wiki/Camera_trap>`_ projects. Motion-triggered camera trapping is increasingly becoming an important tool in ecological research. Because of the nature of collected data (multimedia files) even relatively small camera-trapping projects can generate large and complex datasets. The organization of these large collections of multimedia files and efficient querying for a particular subsets of data, especially in a spatio-temporal context, is often a challenging task. Without an appropriate software solution this can become a serious data management problem, leading to delays and inaccessibility of data in the long run. We propose a new approach which, in contrary to available software solutions, is a fully open-source web application using spatially enabled data that can handle arbitrary media types (both pictures and videos), supports collaborative work on a project and data sharing between system users. We used state of the art and well-recognized open-source software components and modern, general purpose programming language Python to design a flexible software framework for data management in camera trapping studies.

TRAPPER in a nuthsell
---------------------
- it is open-source
- provides a spatially-enabled database backend
- can handle both pictures & videos
- provides a flexible model of classifications
- promotes data re-use
- supports collaborative work on a project
- provides API

Installation
------------
1) You need Docker Engine and Docker Compose to run TRAPPER:
   
   * Installing Docker Engine ⯈ https://docs.docker.com/install/

   * Installing Docker Compose ⯈ https://docs.docker.com/compose/

2) Clone the source code of TRAPPER to your local repository:
   
   .. code-block:: console
                   
      $ git clone https://gitlab.com/trapper-project/trapper.git
    
3) To get the most up-to-date version of TRAPPER switch to a branch ``development``:

   .. code-block:: console

      $ git checkout development

4) Copy the ``trapper.env`` file to the ``.env`` file, you can do it using the command:

   .. code-block:: console
      
      $ cp trapper.env .env
 
5) Adjust the variables in the ``.env`` file if you use a non-standard installation, e.g. the external postgresql database. See the full documentation to learn more about all possible configuration parameters.


6) Run TRAPPER with the following command:

   * Production

     .. code-block:: console
		     
	$ ./start.sh -pb prod

   * Development

     .. code-block:: console

	$ ./start.sh -pb dev

   .. note::
      To learn more about all possible running options simply type ``./start.sh``. 

   
7) Enter the ``trapper`` docker container and create the ``superuser``:

   .. code-block:: console
		   
      $ docker exec -it trapper bash
      $ python /app/trapper/trapper-project/manage.py createsuperuser

8) To turn off the application execute the command:
   
   .. code-block:: console

      $ ./start.sh prod stop

Demo
----
`<https://demo.trapper-project.org>`_

Read more
---------

Bubnicki, J. W., Churski, M. and Kuijper, D. P. (2016), TRAPPER: an open source web‐based application to manage camera trapping projects. Methods Ecol Evol, 7: 1209-1216. doi:10.1111/2041-210X.12571

`<https://besjournals.onlinelibrary.wiley.com/doi/10.1111/2041-210X.12571>`_

For more news about TRAPPER please visit the Open Science Conservation Fund (OSCF) website:

`<https://os-conservation.org>`_


Documentation
-------------

`<https://trapper-project.readthedocs.io>`_


Contribute
----------

- Issue Tracker: `<https://gitlab.com/trapper-project/trapper/-/issues>`_
- Source Code: `<https://gitlab.com/trapper-project/trapper>`_

  
Support
-------

If you are having issues, please let us know:

trapper-project@gmail.com

contact@os-conservation.org

We also have a mailing list:

trapper-project@googlegroups.com

https://groups.google.com/d/forum/trapper-project


License
-------

Copyright (C) 2022 Open Science Conservation Fund (OSCF)

TRAPPER is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TRAPPER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TRAPPER. If not, see http://www.gnu.org/licenses/.
