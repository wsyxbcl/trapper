#!/bin/bash
/app/trapper/wait_for.sh postgres:5432 -t 15 -- echo "Database is up!"
/app/trapper/wait_for.sh rabbitmq:5672 -t 15 -- echo "RabbitMQ is up!"

python /app/trapper/trapper-project/manage.py makemigrations --merge --no-input
python /app/trapper/trapper-project/manage.py migrate
python /app/trapper/trapper-project/manage.py collectstatic --noinput
python /app/trapper/trapper-project/manage.py compilemessages

python /app/trapper/trapper-project/manage.py runserver 0.0.0.0:8000 --settings=conf.settings.development
