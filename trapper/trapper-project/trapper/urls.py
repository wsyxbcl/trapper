# -*- coding: utf-8 -*-
import debug_toolbar
from django.conf import settings
from django.conf.urls import include
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import reverse_lazy, re_path
from django.views.decorators.cache import cache_control, cache_page, never_cache
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic import RedirectView
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework import permissions
from trapper.apps.accounts.views import accounts as accounts_views
from umap import views as umap_views
from umap.decorators import (
    map_permissions_check,
    login_required_if_not_anonymous_allowed,
)
from umap.utils import decorated_patterns

admin.autodiscover()


swagger_schema_view = get_schema_view(
    openapi.Info(
        title="TRAPPER API",
        default_version="v1",
        description="",
        terms_of_service="terms of service",
        contact=openapi.Contact(email="contact@os-conservation.org"),
        # license=openapi.License(name="BSD License"),
    ),
    public=False,
    permission_classes=[permissions.IsAuthenticated],
)


urlpatterns = [
    # Overrides
    # Overwrite account inactive url provided by django-allauth so user
    # is redirected to homepage instead of templateless inactive user
    re_path(
        r"^account/inactive/$",
        RedirectView.as_view(url=reverse_lazy("trapper_index")),
        name="account_inactive",
    ),
    # Trapper homepage
    re_path(r"^$", accounts_views.view_index, name="trapper_index"),
    # Media classification urls
    re_path(
        r"^media_classification/",
        include("trapper.apps.media_classification.urls", "media_classification"),
    ),
    # Research urls
    re_path(r"^research/", include("trapper.apps.research.urls")),
    # Storage urls
    re_path(r"^storage/", include("trapper.apps.storage.urls", "storage")),
    # Messaging urls
    re_path(r"^messaging/", include("trapper.apps.messaging.urls")),
    # Accounts urls
    re_path("", include("django.contrib.auth.urls")),
    re_path(r"^accounts/", include("trapper.apps.accounts.urls")),
    re_path(r"^account/", include("allauth.urls")),
    # GeoMap urls
    re_path(r"^geomap/", include("trapper.apps.geomap.urls")),
    # Comments
    re_path(r"^comments/", include("trapper.apps.comments.urls")),
    # Extra tables
    re_path(r"^tables/", include("trapper.apps.extra_tables.urls")),
    # Sendfile urls
    re_path(r"^serve/", include("trapper.apps.sendfile.urls")),
    re_path(r"^grappelli/", include("grappelli.urls")),
    re_path(r"^admin/", admin.site.urls),
    # REST API Swagger docs
    re_path(
        r"^docs/api/swagger(?P<format>\.json|\.yaml)$",
        swagger_schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    re_path(
        r"^docs/api/$",
        swagger_schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    re_path(
        r"^docs/api/redoc/$",
        swagger_schema_view.with_ui("redoc", cache_timeout=0),
        name="schema-redoc",
    ),
    # Debug Toolbar
    re_path(r"^__debug__/", include(debug_toolbar.urls)),
    # JupyterLite urls
    re_path(r"^jupyterlite/", include("trapper.apps.jupyterlite.urls")),
]

# Umap patterns
urlpatterns += (
    re_path(
        r"^map/(?P<pk>\d+)/geojson/$",
        umap_views.MapViewGeoJSON.as_view(),
        name="map_geojson",
    ),
    re_path(
        r"^map/anonymous-edit/(?P<signature>.+)$",
        umap_views.MapAnonymousEditUrl.as_view(),
        name="map_anonymous_edit_url",
    ),
    re_path(
        r"^pictogram/json/$",
        umap_views.PictogramJSONList.as_view(),
        name="pictogram_list_json",
    ),
    re_path(
        r"^$",
        accounts_views.view_index,
        name="user_dashboard",
    ),
)
urlpatterns += decorated_patterns(
    cache_control(must_revalidate=True),
    re_path(
        r"^datalayer/(?P<pk>[\d]+)/$",
        umap_views.DataLayerView.as_view(),
        name="datalayer_view",
    ),  # noqa
    re_path(
        r"^datalayer/(?P<pk>[\d]+)/versions/$",
        umap_views.DataLayerVersions.as_view(),
        name="datalayer_versions",
    ),  # noqa
    re_path(
        r"^datalayer/(?P<pk>[\d]+)/(?P<name>[_\w]+.geojson)$",
        umap_views.DataLayerVersion.as_view(),
        name="datalayer_version",
    ),  # noqa
)
urlpatterns += decorated_patterns(
    [ensure_csrf_cookie],
    re_path(
        r"^map/(?P<slug>[-_\w]+)_(?P<pk>\d+)$", umap_views.MapView.as_view(), name="map"
    ),  # noqa
    re_path(r"^map/new/$", umap_views.MapNew.as_view(), name="map_new"),
)
urlpatterns += decorated_patterns(
    [login_required_if_not_anonymous_allowed, never_cache],
    re_path(r"^map/create/$", umap_views.MapCreate.as_view(), name="map_create"),
)
urlpatterns += decorated_patterns(
    [map_permissions_check, never_cache],
    re_path(
        r"^map/(?P<map_id>[\d]+)/update/settings/$",
        umap_views.MapUpdate.as_view(),
        name="map_update",
    ),
    re_path(
        r"^map/(?P<map_id>[\d]+)/update/permissions/$",
        umap_views.UpdateMapPermissions.as_view(),
        name="map_update_permissions",
    ),
    re_path(
        r"^map/(?P<map_id>[\d]+)/update/owner/$",
        umap_views.AttachAnonymousMap.as_view(),
        name="map_attach_owner",
    ),
    re_path(
        r"^map/(?P<map_id>[\d]+)/update/delete/$",
        umap_views.MapDelete.as_view(),
        name="map_delete",
    ),
    re_path(
        r"^map/(?P<map_id>[\d]+)/update/clone/$",
        umap_views.MapClone.as_view(),
        name="map_clone",
    ),
    re_path(
        r"^map/(?P<map_id>[\d]+)/datalayer/create/$",
        umap_views.DataLayerCreate.as_view(),
        name="datalayer_create",
    ),
    re_path(
        r"^map/(?P<map_id>[\d]+)/datalayer/update/(?P<pk>\d+)/$",
        umap_views.DataLayerUpdate.as_view(),
        name="datalayer_update",
    ),
    re_path(
        r"^map/(?P<map_id>[\d]+)/datalayer/delete/(?P<pk>\d+)/$",
        umap_views.DataLayerDelete.as_view(),
        name="datalayer_delete",
    ),
)
urlpatterns += i18n_patterns(
    re_path(
        r"^showcase/$",
        cache_page(24 * 60 * 60)(umap_views.showcase),
        name="maps_showcase",
    ),
    re_path(r"^search/$", umap_views.search, name="search"),
    re_path(r"^about/$", umap_views.about, name="about"),
    re_path(r"^user/(?P<username>.+)/$", umap_views.user_maps, name="user_maps"),
)

if settings.DEBUG:
    urlpatterns = (
        urlpatterns
        + []
        + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
        + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    )
