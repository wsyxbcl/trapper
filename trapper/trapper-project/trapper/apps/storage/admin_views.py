from django.conf import settings
from django.contrib import messages
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from trapper.apps.accounts.models import UserTask
from trapper.apps.storage.models import Resource
from trapper.apps.storage.tasks import celery_update_thumbnails
from trapper.middleware import get_current_user


def generate_missing_thumbnails(request):
    resources = Resource.objects.filter(file_thumbnail="")
    if resources:
        if settings.CELERY_ENABLED:
            user = get_current_user()
            task = celery_update_thumbnails.delay(resources=resources)
            user_task = UserTask(user=user, task_id=task.task_id)
            user_task.save()
            messages.success(
                request,
                _("Successfully started a celery task to create missing thumbnails"),
            )
        else:
            celery_update_thumbnails(resources)
    else:
        messages.success(request, _("No resources without thumbnails found"))
    return redirect(reverse("admin:storage_resource_changelist"))
