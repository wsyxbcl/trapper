# -*- coding: utf-8 -*-
"""
Celery configuration requires that all tasks that should be available for
asynchronous processing should be created in `tasks.py` file

This module contains minimal logic required to run celery task and
notify about results, classess and functions that actually do work
(i.e. creating thumbnails, processing uploaded collections) are defined
in other modules
"""
import datetime
import os
import shutil
import tempfile
import zipfile
from secrets import token_urlsafe

import pandas
from celery import shared_task
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.utils.text import format_lazy
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from trapper.apps.accounts.models import UserDataPackage
from trapper.apps.accounts.taxonomy import PackageType
from trapper.apps.messaging.models import Message
from trapper.apps.storage.collection_upload import (
    CollectionProcessor,
    CollectionProcessorException,
)
from trapper.apps.storage.taxonomy import ResourceType
from trapper.apps.storage.thumbnailer import Thumbnailer, ThumbnailerException


@shared_task(serializer="pickle")
def celery_update_thumbnails(resources):
    """
    Celery task that create thumbnails for a list of images/videos

    :param resources: list of storage.Resource model instances
    """
    for resource in resources:
        if resource.resource_type in ResourceType.THUMBNAIL_TYPES:
            try:
                Thumbnailer(resource=resource).create()
            except ThumbnailerException:
                continue


@shared_task(serializer="pickle")
def celery_process_collection_upload(definition_file, archive_file, owner, remove_zip):
    """
    Celery task that creates collections using provided (uploaded)
    definition (YAML) and data (ZIP archive) files.

    :param definition_file: a definition file (YAML)
    :param archive_file: a data file (ZIP archive) or a path to already
    uploaded archive
    :param owner: a user that will be an owner of new collection
    :param remove_zip: bool, whether to remove zip from storage after upload
    """
    start = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S`")

    message_success = _(
        "Collections in the data package {archive_file} that you have uploaded at "
        "{start} have been successfully processed at {end}."
    )
    message_success_errors = _("List of objects that could not be processed: {errors}")
    message_failure = _(
        "Collections in the data package that you have uploaded at {start} could "
        "not be processed due to the following errors: {errors}"
    )

    try:
        processor = CollectionProcessor(
            definition_file=definition_file,
            archive_file=archive_file,
            owner=owner,
            celery_enabled=settings.CELERY_ENABLED,
        )
        processor.create()
    except CollectionProcessorException as error:
        end = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S`")
        Message.objects.create(
            subject=_("Collection upload failed"),
            text=message_failure.format(start=start, errors=error.args[0]),
            user_from=owner,
            user_to=owner,
            date_sent=end,
        )
    else:
        end = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S`")
        errors = []
        for col_name in processor.errors.keys():
            collection = processor.errors[col_name]
            errors.append("<li>{name}<ul>".format(name=col_name))
            for dep_name in collection.keys():
                deployment = collection[dep_name]
                errors.append("<li>{name}<ul>".format(name=dep_name))
                for resource in deployment:
                    errors.append(
                        "<li>{name}: {error}</li>".format(
                            name=resource[0], error=resource[1]
                        )
                    )
                errors.append("</ul></li>")
            errors.append("</ul></li>")

        archive_name = os.path.basename(processor.archive.filename)

        message_success = message_success.format(
            start=start, end=end, archive_file=archive_name
        )
        if errors:
            message_success_errors = message_success_errors.format(
                errors="<br>".join(errors)
            )
        else:
            if remove_zip:
                os.remove(archive_file)

        Message.objects.create(
            subject=format_lazy(
                _("Collections ({archive_name}) upload finished successfully"),
                archive_name=archive_name,
            ),
            text=message_success_errors,
            user_from=owner,
            user_to=owner,
            date_sent=end,
        )
        return message_success


@shared_task(serializer="pickle")
def celery_create_media_package(resources, user, package_name, metadata=False):
    """
    Celery task that creates a data package (archive) from selected
    resources.

    :param resources: queryset of storage.Resource model
    """
    from trapper.apps.storage.serializers import ResourceTableSerializer

    USE_7ZIP = getattr(settings, "USE_7ZIP", False)
    timestamp = now()
    if package_name:
        package_name = ".".join([package_name, "zip"])
    package_name = package_name or "media_{0}.zip".format(
        timestamp.strftime("%d%m%Y_%H%M%S")
    )
    tmpdir_package = tempfile.mkdtemp()
    tmp_package_path = os.path.join(tmpdir_package, package_name)

    if USE_7ZIP:
        # alternative (faster) system-based method to build an archive
        # e.g. 7z a -m0=Copy {package_name}.7z {file_paths}
        pass
    else:
        with zipfile.ZipFile(tmp_package_path, "w", allowZip64=True) as zipf:
            if metadata:
                mdata = ResourceTableSerializer(resources, many=True).data
                df = pandas.DataFrame(mdata)
                df.sort_values(["pk", "name"], inplace=True)
                with tempfile.NamedTemporaryFile() as temp:
                    df.to_csv(temp.name, encoding="utf-8", index=False)
                    zipf.write(temp.name, "00_METADATA.csv")
            for resource in resources:
                try:
                    base_name = "_".join([str(resource.pk), resource.name])
                    filename = ".".join([base_name, resource.mime_type.split("/")[1]])
                    # use .writestr instead of .write to support remote storages
                    zipf.writestr(filename, resource.file.read())
                except Exception:
                    continue
        zipf.close()

    user_data_package_obj = UserDataPackage(
        user=user, date_created=timestamp, package_type=PackageType.MEDIA_FILES
    )
    suf_file = SimpleUploadedFile(
        package_name, open(tmp_package_path, "rb").read(), content_type="zip"
    )
    try:
        user_data_package_obj.package.save(package_name, suf_file, save=False)
        user_data_package_obj.save()
    except Exception as e:
        msg = _(f"Error: {e}")
    finally:
        shutil.rmtree(tmpdir_package)
        msg = _(
            f"The requested data package: {package_name} has been successfully generated."
        )
    return msg


@shared_task(serializer="pickle")
def celery_regenerate_tokens(resources):
    """
    Celery task that re-generates tokens for selected resources.

    :param resources: queryset of storage.Resource model
    """
    to_update = []
    for resource in resources:
        resource.token = token_urlsafe()
        to_update.append(resource)
    resources.model.objects.bulk_update(
        to_update, fields=["token"], batch_size=settings.BULK_BATCH_SIZE
    )
    msg = _(f"{len(resources)} tokens have been successfully (re)generated.")
    return msg
