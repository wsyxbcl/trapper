from django.db import migrations


def remove_is_empty_from_data(apps, schema_editor):
    Resource = apps.get_model("storage", "Resource")
    qs = Resource.objects.exclude(data={})
    for res in qs.iterator(chunk_size=1000):
        data = res.data
        if "is_empty" in data:
            empty = data.pop("is_empty")
            if empty:
                data["observations"] = [{"observation_type": "blank"}]
        res.data = data
    Resource.objects.bulk_update(qs, ["data", ], batch_size=10000)


class Migration(migrations.Migration):

    dependencies = [
        ('storage', '0017_alter_resource_data'),
    ]

    operations = [
        migrations.RunPython(remove_is_empty_from_data)
    ]
