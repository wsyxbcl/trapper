# -*- coding: utf-8 -*-
"""
Simple admin interface to control changes in models.
"""

from bulk_update.helper import bulk_update
from django.contrib import admin
from django.shortcuts import render
from django.urls import path
from django.utils.safestring import mark_safe

from trapper.apps.storage.admin_views import generate_missing_thumbnails
from trapper.apps.storage.forms import AdminSetOwnerManagersForm
from trapper.apps.storage.models import Resource, Collection, CollectionMember


class CollectionAdmin(admin.ModelAdmin):
    list_display = ("name", "status", "owner", "date_created")
    list_select_related = ("owner",)
    search_fields = ("name", "description")
    list_filter = ("status", "date_created", "owner")
    raw_id_fields = ("resources", "owner", "managers")


class ResourceAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "mime_type",
        "owner",
        "status",
        "deployment",
        "tag_list",
        "date_uploaded",
        "date_recorded",
        "link",
    )
    search_fields = (
        "name",
        "deployment__deployment_id",
        "owner__username",
        "tags__name",
    )
    list_filter = ("date_uploaded", "date_recorded", "resource_type", "collection")
    raw_id_fields = ("owner", "managers", "deployment")
    actions = [
        "set_owner_managers_action",
    ]
    change_list_template = "admin/resources_changelist.html"

    def get_queryset(self, request):
        return (
            super(ResourceAdmin, self)
            .get_queryset(request)
            .select_related(
                "owner",
                "deployment",
            )
            .prefetch_related(
                "tags",
            )
        )

    def tag_list(self, item):
        return u", ".join(o.name for o in item.tags.all())

    def link(self, item):
        return mark_safe(
            u'<a href="{url}" target="_blank">Link</a>'.format(
                url=item.get_url_original()
            )
        )

    link.short_description = "See file"

    def set_owner_managers_action(self, request, queryset):
        if "do_action" in request.POST:
            form = AdminSetOwnerManagersForm(request.POST)
            if form.is_valid():
                owner = form.cleaned_data.get("owner")
                managers = form.cleaned_data.get("managers")
                if managers:
                    managers_through_model = getattr(Resource, "managers").through
                    managers_to_update = []

                for item in queryset:
                    if owner:
                        item.owner = owner
                    if managers:
                        for m in managers:
                            managers_through_item = managers_through_model(user=m)
                            setattr(managers_through_item, "resource", item)
                            managers_to_update.append(managers_through_item)
                if owner:
                    # bulk update owner field
                    bulk_update(queryset, update_fields=["owner"])

                if managers:
                    # bulk update m2m managers field
                    managers_through_model.objects.filter(
                        **{"resource__in": queryset}
                    ).delete()
                    managers_through_model.objects.bulk_create(managers_to_update)

                self.message_user(
                    request,
                    "You have successfully set the owner and managers of selected resources.",
                    level=25,
                )
                return
            else:
                self.message_user(
                    request, "You have to choose at aleast one option.", level=40
                )
        form = AdminSetOwnerManagersForm(
            initial={
                "_selected_action": request.POST.getlist(
                    admin.helpers.ACTION_CHECKBOX_NAME
                )
            }
        )
        return render(
            request,
            "admin/storage/action_set_owner_managers.html",
            {
                "title": u"Set owner and managers of selected resources",
                "queryset": queryset,
                "form": form,
            },
        )

    set_owner_managers_action.short_description = (
        u"Set owner and managers of selected resources"
    )

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path(
                "generate-thumbnails",
                self.admin_site.admin_view(generate_missing_thumbnails),
                name="generate-thumbnails",
            )
        ]
        return urls + my_urls


class CollectionMemberAdmin(admin.ModelAdmin):
    search_fields = ("collection__name",)
    list_display = ("collection", "user", "get_level_display", "date_created")
    list_filter = ("level", "user")


admin.site.register(Resource, ResourceAdmin)
admin.site.register(Collection, CollectionAdmin)
admin.site.register(CollectionMember, CollectionMemberAdmin)
