# -*- coding: utf-8 -*-

"""This module contains models and signals of the Storage application"""
import itertools
import os.path
from mimetypes import guess_type
from secrets import token_urlsafe
from urllib.parse import urljoin

from botocore.exceptions import ClientError
from django.apps import apps
from django.conf import settings
from django.contrib.gis.db import models as gis_models
from django.contrib.gis.db.models import Extent
from django.contrib.gis.geos import Polygon
from django.contrib.postgres.indexes import GinIndex
from django.contrib.postgres.search import SearchVectorField, SearchVector
from django.db import models
from django.db.models import Value, F
from django.db.models.aggregates import Sum
from django.db.models.signals import m2m_changed, post_delete
from django.dispatch import receiver
from django.template.defaultfilters import filesizeformat
from django.urls import reverse
from django.utils.timezone import now, get_current_timezone
from django.utils.translation import gettext_lazy as _
from taggit.managers import TaggableManager

from trapper.apps.accounts.models import UserTask
from trapper.apps.common.fields import SafeTextField
from trapper.apps.common.models import BaseAccessMember
from trapper.apps.common.utils.datetime_tools import set_correct_offset_dst
from trapper.apps.geomap.models import Location
from trapper.apps.messaging.models import Message
from trapper.apps.messaging.taxonomies import MessageType
from trapper.apps.storage.mixins import AccessModelMixin, APIContextManagerMixin
from trapper.apps.storage.tasks import celery_update_thumbnails
from trapper.apps.storage.taxonomy import (
    ResourceMimeType,
    ResourceStatus,
    ResourceType,
    CollectionStatus,
    CollectionMemberLevels,
)
from trapper.apps.storage.thumbnailer import Thumbnailer
from trapper.middleware import get_current_user


class ResourceManager(APIContextManagerMixin, models.Manager):
    """The manager of the :class:`Resource` model."""

    url_update = "storage:resource_update"
    url_detail = "storage:resource_detail"
    url_delete = "storage:resource_delete"
    use_for_related_fields = True

    def get_accessible(self, user=None, base_queryset=None):
        """For given user it returns all accessible instances of the :class:`Resource`
        model. If user is not provided then currently logged in user is used.
        """
        public = ResourceStatus.PUBLIC
        user = user or get_current_user()
        if base_queryset is None:
            queryset = self.get_queryset()
        else:
            queryset = base_queryset
        if not user.is_authenticated:
            return queryset.none()
        if user.is_staff:
            return queryset
        rproject_role_model = apps.get_model("research", "researchprojectrole")
        rproject_pks = (
            rproject_role_model.objects.filter(user=user)
            .values_list("project_id", flat=True)
            .distinct()
        )
        qs = queryset.exclude(
            ~models.Q(status=public),
            ~models.Q(owner=user),
            ~models.Q(managers=user),
            ~models.Q(deployment__location__research_project__project_roles__user=user),
            ~models.Q(deployment__location__research_projects_member__in=rproject_pks),
        )
        return qs


UPLOAD_DIR = "protected/storage/resource/"


def UPLOAD_DIR_BASE(instance):
    return "{0}/user_id_{1}/{2}".format(
        UPLOAD_DIR, instance.owner.id, instance.date_uploaded.strftime("%Y%m%d")
    )


def UPLOAD_DIR_F(instance, filename):
    return "{0}/{1}".format(UPLOAD_DIR_BASE(instance), filename)


def THUMBNAIL_DIR_F(instance, filename):
    return "{0}/{1}/{2}".format(UPLOAD_DIR_BASE(instance), "thumbnails", filename)


def PREVIEW_DIR_F(instance, filename):
    return "{0}/{1}/{2}".format(UPLOAD_DIR_BASE(instance), "previews", filename)


class Resource(AccessModelMixin, models.Model):
    """Model describing the most basic entities used in other modules.
    Resource is usually a video or an image.
    In order to provide some robust playback features, it is possible to upload
    up to two separate resource files.

    Each resource can have its own share status: Private, Public or access on
    demand.

    Resources can be grouped into :class:`Collection`.
    """

    status_choices = ResourceStatus

    name = models.CharField(max_length=255, verbose_name=_("Name"))
    file = models.FileField(
        upload_to=UPLOAD_DIR_F, max_length=255, verbose_name=_("File")
    )
    file_thumbnail = models.ImageField(
        upload_to=THUMBNAIL_DIR_F,
        blank=True,
        null=True,
        editable=False,
        verbose_name=_("File thumbnail"),
    )
    file_preview = models.ImageField(
        upload_to=PREVIEW_DIR_F,
        blank=True,
        null=True,
        editable=False,
        verbose_name=_("File preview"),
    )
    extra_file = models.FileField(
        upload_to=UPLOAD_DIR, null=True, blank=True, verbose_name=_("Extra file")
    )
    mime_type = models.CharField(
        choices=ResourceMimeType.CHOICES,
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("MIME type"),
    )
    extra_mime_type = models.CharField(
        choices=ResourceMimeType.CHOICES,
        max_length=255,
        null=True,
        blank=True,
        verbose_name=_("Extra MIME type"),
    )
    resource_type = models.CharField(
        choices=ResourceType.CHOICES,
        max_length=1,
        null=True,
        blank=True,
        verbose_name=_("Resource type"),
    )
    file_size = models.IntegerField(
        blank=True, default=0, verbose_name=_("File size in bytes")
    )
    file_preview_size = models.IntegerField(
        blank=True, default=0, verbose_name=_("Preview file size in bytes")
    )
    file_thumbnail_size = models.IntegerField(
        blank=True, default=0, verbose_name=_("Thumbnail file size in bytes")
    )
    date_uploaded = models.DateTimeField(
        null=True, blank=True, default=now, verbose_name=_("Date uploaded")
    )
    date_recorded = models.DateTimeField(verbose_name=_("Date recorded"))
    token = models.CharField(
        max_length=255, default=token_urlsafe, verbose_name=_("Token")
    )
    deployment = models.ForeignKey(
        "geomap.Deployment",
        null=True,
        blank=True,
        related_name="resources",
        on_delete=models.PROTECT,
        verbose_name=_("Deployment"),
    )
    data = models.JSONField(null=True, blank=True, default=dict, verbose_name=_("Data"))
    search_data_vector = SearchVectorField(null=True, blank=True)
    search_name_vector = SearchVectorField(null=True, blank=True)
    status = models.CharField(
        choices=status_choices.CHOICES,
        max_length=8,
        default="Private",
        verbose_name=_("Status"),
    )
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="owned_resources",
        on_delete=models.DO_NOTHING,
    )
    managers = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        blank=True,
        related_name="managed_resources",
        verbose_name=_("Managers"),
    )
    description = SafeTextField(blank=True, null=True, verbose_name=_("Description"))
    inherit_prefix = models.BooleanField(
        default=False, verbose_name=_("Deployment ID prefix")
    )
    custom_prefix = models.CharField(
        max_length=255, blank=True, null=True, verbose_name=_("Custom prefix")
    )
    exif_data = models.CharField(
        max_length=255, blank=True, default="", verbose_name=_("EXIF data")
    )
    favourite = models.BooleanField(default=False, blank=True)
    humans_blurred = models.BooleanField(
        default=False, blank=True, verbose_name=_("Humans blurred")
    )

    objects = ResourceManager()
    tags = TaggableManager(blank=True)

    class Meta:
        ordering = ("-date_recorded",)
        indexes = [
            models.Index(fields=["-date_recorded"]),
            models.Index(fields=["deployment", "-date_recorded"]),
            GinIndex(fields=["search_data_vector"]),
            GinIndex(fields=["search_name_vector"]),
        ]
        constraints = [
            models.UniqueConstraint(
                fields=["name", "deployment"], name="unique_name_per_deployment"
            )
        ]

    def can_view(self, user=None):
        user = user or get_current_user()
        if not user or not user.is_authenticated:
            return False
        if user.is_staff or self.status == "Public":
            return True
        if self.collection_set.filter(status="Public").exists():
            return True
        access_status = (
            self.owner_id == user.pk
            or user.pk in self.managers.values_list("pk", flat=True)
        )
        if access_status:
            return True
        qs1 = user.collectionmember_set.values_list("collection__pk", flat=True)
        qs2 = self.collection_set.values_list("pk", flat=True).exclude(
            owner=user, managers=user
        )
        return len(list(set(qs1) & set(qs2))) > 0

    def __str__(self):
        return f"{self.get_resource_type_display()}: {self.name}"

    @property
    def timezone(self):
        if self.deployment_id:
            return self.deployment.location.timezone
        else:
            return get_current_timezone()

    @property
    def ignore_DST(self):
        if self.deployment_id:
            return self.deployment.location.ignore_DST
        return False

    @property
    def date_recorded_tz(self):
        """Return local datetime according to location's timezone. If resource
        does not have an assigned location then default timezone is used."""
        return set_correct_offset_dst(
            self.date_recorded, self.timezone, self.ignore_DST
        )

    @property
    def prefixed_name(self):
        """Name of resource that support custom prefix and deployment_id (if
        inheriting is enabled)"""
        prefix_parts = []

        if self.custom_prefix:
            prefix_parts.append(self.custom_prefix)

        if self.inherit_prefix and self.deployment:
            deployment_id = self.deployment.deployment_id
            if deployment_id:
                prefix_parts.append(deployment_id)

        prefix_parts.append(self.name)
        return "_".join(prefix_parts)

    def save(self, **kwargs):
        """
        On save update Resource metadata and name search vector
        """

        # first update mime_type and resource_type
        self.update_metadata()

        if not self.search_name_vector:
            self.search_name_vector = SearchVector(Value(self.name))
        super(Resource, self).save(**kwargs)

    def refresh_collection_bbox(self):
        """Refresh all connected collections bbox for given resource"""
        for collection in self.collection_set.all():
            collection.refresh_bbox()

    def refresh_collection_period(self):
        """Refresh all connected collections periods for given resource"""
        for collection in self.collection_set.all():
            collection.refresh_period()

    @property
    def is_public(self):
        """Return True if resource is publicly available"""
        return self.status == ResourceStatus.PUBLIC

    def get_absolute_url(self):
        """Get the absolute url for an instance of this model."""
        return reverse("storage:resource_detail", kwargs={"pk": self.pk})

    def can_remove_comment(self, user=None):
        """Method used by trapper.apps.comments application
        to determine when comments related to Resource can be removed"""
        user = user or get_current_user()
        return self.owner == user

    def generate_thumbnails(self):
        """When resource is created, thumbnail is generated
        For small resources thumbnail is generated without celery.
        For larger ones, if celery processing is enabled - thumbnail
        is generated as celery task"""
        if not self.mime_type or not self.resource_type:
            self.update_metadata()
        if self.resource_type in ResourceType.THUMBNAIL_TYPES:
            if settings.CELERY_ENABLED:
                task = celery_update_thumbnails.delay(resources=[self])
                user_task = UserTask(user=self.owner, task_id=task.task_id)
                user_task.save()
            else:
                Thumbnailer(self).create()

    def update_metadata(self, commit=False):
        """Updates the internal metadata about the resource.

        :param commit: States whether to perform self.save() at the end of
            this method
        """
        self.update_mimetype(commit=False)
        self.update_resource_type(commit=False)
        self.update_file_size(commit=False)
        if commit:
            self.save()

    def update_file_size(self, commit=False):
        try:
            self.file_size = self.file.size
            if commit:
                self.save()
        except (FileNotFoundError, ClientError):
            pass

    def update_resource_type(self, commit=False):
        """Sets resource_type based on mime_type.

        :param commit: States whether to perform self.save() at the end of
            this method
        """
        if not self.mime_type:
            self.update_mimetype(commit=False)
        if self.mime_type.startswith("audio"):
            self.resource_type = ResourceType.TYPE_AUDIO
        elif self.mime_type.startswith("video"):
            self.resource_type = ResourceType.TYPE_VIDEO
        elif self.mime_type.startswith("image"):
            self.resource_type = ResourceType.TYPE_IMAGE
        if commit:
            self.save()

    def update_mimetype(self, commit=False):
        """Sets the mime_type for the resource.
        This is obtained by trying to *guess* the mime type based on the
        resource file.

        :param commit: States whether to perform self.save() at the end of
            this method
        """
        undefined_mime = "application/octet-stream"
        # use url instead of path to support remote storages and
        # split url to remove any extra params before guess_type
        self.mime_type = guess_type(self.file.url.split("?")[0])[0] or undefined_mime
        if self.extra_file:
            self.extra_mime_type = (
                guess_type(self.extra_file.url.split("?")[0])[0] or undefined_mime
            )
        if commit:
            self.save()

    def get_icon_class(self):
        return "add_icon_%s" % self.get_resource_type_display().lower()

    def get_url(self):
        field = "file"
        if self.resource_type == ResourceType.TYPE_IMAGE:
            field = "pfile"
        return reverse(
            "storage:resource_sendfile_media", kwargs={"pk": self.pk, "field": field}
        )

    def get_url_original(self, token=False, field="file"):
        url = reverse(
            "storage:resource_sendfile_media", kwargs={"pk": self.pk, "field": field}
        )
        if token:
            url = urljoin(url, f"?rt={self.token}")
        return url

    def get_url_with_filename(self, field="file", extra_file_name=None):
        if not extra_file_name:
            extra_file_name = os.path.basename(getattr(self, field).name)

        url = reverse(
            "storage:resource_sendfile_media_with_filename",
            kwargs={
                "resource_pk": self.pk,
                "resource_field": field,
                "token": self.token,
                "filename": extra_file_name,
            },
        )

        return url

    def get_thumbnail_url(self):
        """Return url of thumbnail based on resource type"""
        base_url = "/static/trapper_storage/img/{name}"
        url = reverse(
            "storage:resource_sendfile_media", kwargs={"pk": self.pk, "field": "tfile"}
        )
        thumbnail = base_url.format(name="no_thumb_100x100.jpg")
        if self.resource_type == ResourceType.TYPE_IMAGE:
            if self.file_thumbnail:
                thumbnail = url
            else:
                thumbnail = base_url.format(name="no_thumb_image_100x100.jpg")
        elif self.resource_type == ResourceType.TYPE_AUDIO:
            thumbnail = base_url.format(name="no_thumb_audio_100x100.jpg")
        elif self.resource_type == ResourceType.TYPE_VIDEO:
            if self.file_thumbnail:
                thumbnail = url
            else:
                thumbnail = base_url.format(name="no_thumb_video_100x100.jpg")
        return thumbnail

    def get_extra_url(self):
        return reverse(
            "storage:resource_sendfile_media", kwargs={"pk": self.pk, "field": "efile"}
        )

    def check_date_recorded(self):
        """Determines if a date when a resource was recorded falls within
        a deployment period. If resource has no deployment assigned returns
        True.
        """
        if not self.deployment:
            return True
        if self.deployment.start_date and self.deployment.end_date:
            return (
                self.deployment.start_date
                <= self.date_recorded
                <= self.deployment.end_date
            )
        else:
            # as it is impossible to test it returns True to not display
            # a warning message
            return True


class CollectionManager(APIContextManagerMixin, gis_models.Manager):
    """Manager for :class:`Collection` model.

    This manager contains additional logic used byy DRF serializers like
    details/update/delete urls
    """

    url_update = "storage:collection_update"
    url_detail = "storage:collection_detail"
    url_delete = "storage:collection_delete"

    def get_accessible(self, user=None, base_queryset=None, role_levels=None):
        """Return all :class:`Collection` instances that given user
        has access to. If user is not defined, then currently logged in user
        is used.
        If there is no authenticated user then only
        :class:`apps.storage.taxonomy.CollectionStatus.PUBLIC` collections are
        returned

        :param user: if not none then that user will be used to filter
            accessible collections. If passed user not logged in,
            then collections are limited to PUBLIC only. If user is None
            then currently logged in user is used.
        :param base_queryset: queryset used to limit checked collections.
            by default it's all collections.
        :param role_levels: list of :class:`CollectionMemberLevels` levels
            that user is required to have to access collections

        :return: collections queryset
        """
        user = user or get_current_user()
        public = CollectionStatus.PUBLIC
        if not role_levels:
            role_levels = (
                CollectionMemberLevels.ACCESS,
                CollectionMemberLevels.ACCESS_REQUEST,
            )
        if base_queryset is None:
            queryset = self.get_queryset()
        else:
            queryset = base_queryset
        if not user.is_authenticated:
            return queryset.none()
        if user.is_staff:
            return queryset
        return queryset.exclude(
            ~models.Q(owner=user),
            ~models.Q(managers=user),
            ~models.Q(status=public),
            ~models.Q(members=user),
        )

    def get_ondemand(self, user, base_queryset=None):
        user = user or get_current_user()
        if not user.is_authenticated:
            return Collection.objects.none()
        ondemand = CollectionStatus.ON_DEMAND
        if base_queryset is None:
            queryset = self.get_queryset()
        else:
            queryset = base_queryset
        return queryset.filter(status=ondemand).filter(
            ~models.Q(owner=user),
            ~models.Q(managers=user),
            ~models.Q(collection_request__user_from=user),
        )

    def get_editable(self, user=None, base_queryset=None):
        """Get queryset with collections that can be updated by given user

        :param user: if not none then that user will be used to filter
            accessible collections. If passed user not logged in,
            then queryset is empty. If user is None
            then currently logged in user is used.
        :param base_queryset: queryset used to limit checked collections.
            by default it's all collections.
        """
        user = user or get_current_user()
        if not user.is_authenticated:
            return Collection.objects.none()
        if base_queryset is None:
            queryset = self.get_queryset()
        else:
            queryset = base_queryset
        return queryset.exclude(~models.Q(owner=user), ~models.Q(managers=user))


class Collection(AccessModelMixin, models.Model):
    """Base container for grouping :class:`storage:Resource` model instances.
    Collections are used in other modules such as research projects or
    classification projects"""

    member_levels = CollectionMemberLevels
    status_choices = CollectionStatus

    name = models.CharField(max_length=255, verbose_name=_("Name"))
    description = SafeTextField(
        max_length=2000, null=True, blank=True, verbose_name=_("Description")
    )
    resources = models.ManyToManyField(Resource, verbose_name=_("Resources"))
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="owned_collections",
        on_delete=models.DO_NOTHING,
    )
    managers = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        blank=True,
        related_name="managed_collections",
        help_text=_("Select your managers."),
    )
    status = models.CharField(
        choices=CollectionStatus.CHOICES,
        max_length=8,
        default="Private",
        verbose_name=_("Status"),
    )
    date_created = models.DateTimeField(
        editable=False, auto_now_add=True, verbose_name=_("Date created")
    )

    members = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        through="CollectionMember",
        blank=True,
        verbose_name=_("Members"),
    )
    bbox = gis_models.PolygonField(blank=True, null=True, verbose_name=_("Bbox"))
    period_begin = models.DateTimeField(
        blank=True, null=True, verbose_name=_("Period begin")
    )
    period_end = models.DateTimeField(
        blank=True, null=True, verbose_name=_("Period end")
    )

    objects = CollectionManager()

    class Meta:
        ordering = [
            "name",
        ]

    @property
    def is_public(self):
        """Return True if resource is publicly available"""
        return self.status == ResourceStatus.PUBLIC

    def __str__(self):
        return str(self.name)

    def get_absolute_url(self):
        """Get the absolute url for an instance of this model."""
        return reverse("storage:collection_detail", kwargs={"pk": self.pk})

    def can_add_to_research_project(self, user=None):
        """Check if collection can be added to research project"""
        user = user or get_current_user()
        return user == self.owner or user in self.managers.all()

    def is_used(self, user=None, rproject=None, cproject=None):
        """The helper method to check if user should still have access to
        this collection.
        """
        # get models
        research_project = apps.get_model("research", "ResearchProject")
        classification_project = apps.get_model(
            "media_classification", "ClassificationProject"
        )
        user = user or get_current_user()
        # step 1
        rprojects = research_project.objects.filter(
            collections=self, project_roles__user=user
        ).distinct()
        if rproject:
            return rprojects.exists()
        # step 2
        cprojects = classification_project.objects.filter(
            collections__collection=self, classification_project_roles__user=user
        ).distinct()
        if cproject:
            return cprojects.exists()
        return rprojects.exists() or cprojects.exists()

    def delete(self, **kwargs):
        """Send notification before deleting objects"""
        # TODO: remove or fix notification part
        # delete_collection_notification(instance=self)
        return super(Collection, self).delete(**kwargs)

    def refresh_bbox(self):
        """Recalculate border polygon containing all coordinates from
        resources assigned to collection"""
        locations = Location.objects.filter(
            pk__in=self.resources.filter(
                deployment__location__isnull=False
            ).values_list("deployment__location__pk", flat=True)
        )
        if locations.count() > 1:
            extent = locations.aggregate(Extent("coordinates")).get(
                "coordinates__extent"
            )
            point1_x, point1_y, point2_x, point2_y = extent
            points = [
                (point1_x, point1_y),
                (point2_x, point1_y),
                (point2_x, point2_y),
                (point1_x, point2_y),
                (point1_x, point1_y),
            ]
            polygon = Polygon(points).buffer(1).envelope
            self.bbox = polygon
        else:
            self.bbox = None
            self.save()

    @property
    def period(self):
        """Return tuple with period dates"""
        return self.period_begin, self.period_end

    def refresh_period(self):
        """Recalculate period as oldest and latest date_recorded
        from all resources connected to collection"""
        period = self.resources.aggregate(
            begin=models.Min("date_recorded"), end=models.Max("date_recorded")
        )
        self.period_begin = period["begin"]
        self.period_end = period["end"]
        self.save()

    @property
    def resources_count(self):
        return self.resources.count()

    @property
    def collection_storage(self):
        """
        Calculates total storage taken by collection by iterating over all the files and summing FileField.size results
        """
        total = self.resources.annotate(
            total=F("file_size") + F("file_thumbnail_size") + F("file_preview_size")
        ).aggregate(Sum("total"))["total__sum"]
        return filesizeformat(total)


class CollectionMember(BaseAccessMember):
    """Class used to define user access levels for :class:`Collection`"""

    collection = models.ForeignKey(Collection, on_delete=models.DO_NOTHING)
    level = models.IntegerField(
        choices=CollectionMemberLevels.CHOICES, verbose_name=_("Level")
    )


@receiver(post_delete, sender=Resource)
def delete_files(sender, instance, **kwargs):
    if instance.file:
        instance.file.delete(False)
    if instance.file_thumbnail:
        instance.file_thumbnail.delete(False)
    if instance.extra_file:
        instance.extra_file.delete(False)
    if instance.file_preview:
        instance.file_preview.delete(False)


@receiver(m2m_changed, sender=Collection.resources.through)
def update_collection_data(sender, instance, action, **kwargs):
    """
    Signal used to update gis data in collection

    :param sender: :class:`Collection.resources.through`
    :param instance: :class:`Collection`
    :param action: pre_clear or post_add are used
    :param kwargs: additional arguments sent by signal
    """
    if action in ["post_add", "post_remove", "post_clear"]:
        instance.refresh_bbox()
        instance.refresh_period()


@receiver(m2m_changed, sender=Collection.resources.through)
def rebuild_cp_collections(sender, instance, action, **kwargs):
    """
    Signal used to update classification project collections i.e.
    to rebuild base classifications objects

    :param sender: :class:`Collection.resources.through`
    :param instance: :class:`Collection`
    :param action: pre_clear or post_add are used
    :param kwargs: additional arguments sent by signal
    """
    from trapper.apps.media_classification.models import ClassificationProjectCollection

    if action in ["post_add", "post_remove", "post_clear"]:
        cp_collections = ClassificationProjectCollection.objects.filter(
            collection__collection=instance
        )
        for cp_collection in cp_collections:
            cp_collection.rebuild_classifications()


def collections_access_grant(
    collections, users, level=CollectionMemberLevels.ACCESS_BASIC
):
    """
    Method used to grant an access to specified collections.

    Add specified permission level :class:`CollectionMemberLevels`
    to all specified users for all specified collections.

    :param users: iterable of :class:`auth.User` instances
    :param collections: iterable of :class:`Collection`
    :return: None
    """
    for user, collection in itertools.product(users, collections):
        if (
            not collection.owner_id == user.pk
            and user.pk not in collection.managers.values_list("pk", flat=True)
            and not collection.is_public
        ):
            CollectionMember.objects.get_or_create(
                collection=collection, user=user, level=level
            )


def collections_access_revoke(
    collection_pks,
    user_pks,
    rproject=None,
    cproject=None,
    level=CollectionMemberLevels.ACCESS_BASIC,
):
    """
    Method used to revoke an access to specified collections.

    Remove all entries with specified level :class:`CollectionMemberLevels`
    for all given collections and all given users.

    :param users: iterable of :class:`auth.User` instances
    :param collections: iterable of :class:`Collection`
    :return: None
    """
    queryset = CollectionMember.objects.filter(
        collection__pk__in=collection_pks, user__pk__in=user_pks, level=level
    ).prefetch_related("collection", "user")

    if level != CollectionMemberLevels.ACCESS_REQUEST:
        for m in queryset:
            user = m.user
            collection = m.collection
            params = {"user": user}
            if rproject:
                params.update({"rproject": rproject})
            if cproject:
                params.update({"cproject": cproject})
            if collection.is_used(**params):
                queryset = queryset.exclude(
                    collection=collection,
                    user=user,
                )
    queryset.delete()


def delete_collection_notification(instance):
    """
    Messages sent when collection is deleted to:
    * owner
    * all managers
    * owners of collections
    * all users that have access to resource
    """
    user = get_current_user()
    recipients = set()
    # Owner
    recipients.add(instance.owner)
    # managers
    recipients.update(instance.managers.all())
    # people with access
    recipients.update(
        CollectionMember.objects.filter(
            collection=instance, level=CollectionMemberLevels.ACCESS
        )
    )

    for recipient in recipients:
        if user and recipient:
            Message.objects.create(
                subject=_(f"Collection: {instance.name} has been deleted"),
                text=_(f"Collection: {instance.name} has been deleted"),
                user_from=user,
                user_to=recipient,
                date_sent=now(),
                message_type=MessageType.RESOURCE_DELETED,
            )
