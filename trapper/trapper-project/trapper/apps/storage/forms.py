# -*- coding: utf-8 -*-
"""Forms used by storage application.

This module contains forms used by :class:`apps.storage.models.Resource` and
:class:`apps.storage.models.Collection` models
"""
import datetime
import os
import zipfile

from crispy_forms.layout import Layout, Fieldset
from django import forms
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.postgres.search import SearchVector
from django.db.models import Q, Value
from django.forms.widgets import DateTimeInput
from django.forms.widgets import Textarea
from django.template.defaultfilters import filesizeformat
from django.utils.dateparse import parse_datetime
from django.utils.safestring import mark_safe
from django.utils.text import format_lazy
from django.utils.translation import gettext_lazy as _
from taggit.forms import TagField

from trapper.apps.accounts.utils import get_external_collections_path
from trapper.apps.common.fields import (
    OwnerModelMultipleChoiceField,
    SimpleTagField,
    RestrictedFileField,
)
from trapper.apps.common.forms import (
    BaseCrispyModelForm,
    BaseCrispyForm,
    BaseBulkUpdateForm,
)
from trapper.apps.common.tools import parse_pks
from trapper.apps.common.utils.datetime_tools import (
    localize_datetime_dst,
    set_correct_offset_dst,
)
from trapper.apps.geomap.models import Deployment
from trapper.apps.research.models import ResearchProject
from trapper.apps.research.taxonomy import ResearchProjectRoleType
from trapper.apps.storage.collection_upload import CollectionProcessor
from trapper.apps.storage.models import Resource, Collection
from trapper.apps.storage.taxonomy import (
    CollectionSettings,
    CollectionStatus,
    ResourceStatus,
)
from trapper.middleware import get_current_user

User = get_user_model()


class ResourceForm(BaseCrispyModelForm):
    """
    Modelform for creating :class:`apps.storage.models.Resource` objects
    """

    file = RestrictedFileField(
        help_text=_("File size must be under {max}").format(
            max=filesizeformat(settings.MAX_UPLOAD_SIZE)
        ),
        required=False,
        label=_("File"),
    )
    extra_file = RestrictedFileField(
        help_text=_("File size must be under {max}").format(
            max=filesizeformat(settings.MAX_UPLOAD_SIZE)
        ),
        required=False,
        label=_("Extra file"),
    )
    date_recorded = forms.CharField(
        widget=DateTimeInput(format="%Y-%m-%d %H:%M:%S"),
        label=_("Date recorded"),
    )
    managers = OwnerModelMultipleChoiceField(
        queryset=User.objects.all(), required=False, label=_("Managers")
    )
    tags = TagField(
        required=False, label=_("Tags"), help_text="Comma or space delimited tags."
    )

    select2_ajax_fields = (
        ("deployment", "/geomap/api/deployments", "pk", "deployment_id"),
    )

    class Meta:
        model = Resource
        exclude = [
            "owner",
            "date_uploaded",
            "mime_type",
            "extra_mime_type",
            "resource_type",
            "search_data_vector",
            "token",
            "search_name_vector",
        ]

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset(
                "",
                "name",
                "description",
                "date_recorded",
                "deployment",
                "file",
                "extra_file",
                "tags",
                "status",
                "managers",
                "inherit_prefix",
            ),
        )

    def save(self, commit=True):
        """On resource save, the update of the metadata is performed
        (see :meth:`.Resource.update_metadata`)
        """
        resource = super().save(commit=False)

        # localize datetime fields using a proper timezone and DST flag
        resource.date_recorded = localize_datetime_dst(
            parse_datetime(self.cleaned_data["date_recorded"]),
            resource.timezone,
            resource.ignore_DST,
        )

        managers = self.cleaned_data["managers"]
        tags = self.cleaned_data["tags"]

        if "name" in self.changed_data:
            resource.search_name_vector = SearchVector(Value(resource.name))
        if commit:
            resource.save()
        resource.managers.set(managers)
        resource.tags.clear()
        for tag in tags:
            resource.tags.add(tag)
        if "file" in self.changed_data or "extra_file" in self.changed_data:
            resource.update_metadata(commit=True)
            resource.generate_thumbnails()
        return resource

    def __init__(self, *args, **kwargs):
        """Customize each instance of form by setting queryset for
        locations and tags if fields are available in form.
        """
        super().__init__(*args, **kwargs)
        for fieldname in ["managers"]:
            self.fields[fieldname].help_text = None
        if "deployment" in self.fields:
            self.fields["deployment"].required = True
            self.fields["deployment"].queryset = Deployment.objects.get_accessible(
                editable_only=True
            )
            self.fields["deployment"].widget = forms.CharField.widget()

        if self.instance.pk:
            # set initial values for datetime fields using a proper timezone and DST flag
            self.initial["date_recorded"] = set_correct_offset_dst(
                self.instance.date_recorded,
                self.instance.timezone,
                self.instance.ignore_DST,
            )
            # when editing the resource, disable editing file field
            self.fields["file"].disabled = True
            self.fields["file"].widget = forms.HiddenInput()


class SimpleResourceForm(ResourceForm):
    """"""

    class Meta:
        model = Resource
        exclude = [
            "owner",
            "mime_type",
            "extra_mime_type",
            "resource_type",
            "file",
            "extra_file",
            "inherit_prefix",
            "custom_prefix",
            "search_data_vector",
            "search_name_vector",
            "token",
        ]

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset(
                "",
                "name",
                "date_recorded",
                "deployment",
                "tags",
                "status",
                "managers",
            ),
        )


def update_time_offset(obj: Resource, time_offset):
    obj.date_recorded += datetime.timedelta(seconds=time_offset)
    return ["date_recorded"]


class BulkUpdateResourceForm(BaseBulkUpdateForm):
    """"""

    tags2add = SimpleTagField(
        required=False,
        label=_("Tags to add"),
        help_text="Comma or space delimited tags.",
    )
    tags2remove = SimpleTagField(
        required=False,
        label=_("Tags to remove"),
        help_text="Comma or space delimited tags.",
    )
    clear_data = forms.BooleanField(
        required=False,
        label=_("Clear data"),
        initial=False,
        widget=forms.Select(choices=((False, "False"), (True, "True"))),
        help_text=_("Clear basic classification data."),
    )

    time_offset = forms.IntegerField(
        required=False,
        label=_("Time offset"),
        widget=forms.TextInput(),
        help_text=_("Time offset in seconds"),
    )

    select2_ajax_fields = (
        ("deployment", "/geomap/api/deployments", "pk", "deployment_id"),
    )

    custom_updaters = {"time_offset": update_time_offset}

    class Meta:
        model = Resource
        fields = ("status", "deployment", "managers")

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset(
                "",
                "status",
                "deployment",
                "tags2add",
                "tags2remove",
                "managers",
                "records_pks",
                "clear_data",
                "time_offset",
            ),
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["deployment"].widget = forms.CharField.widget()
        self.fields["managers"].help_text = ""

    def clean(self):
        cleaned_data = super().clean()
        clear_data = cleaned_data.pop("clear_data", None)
        if clear_data:
            cleaned_data["data"] = {}
        return cleaned_data


class ResourceDataPackageForm(BaseCrispyForm):
    """
    TODO: docstrings
    """

    package_name = forms.CharField(
        required=False,
        max_length=200,
        help_text=_(
            "Name of your data package. If not provided it will be set automatically."
        ),
    )
    metadata = forms.BooleanField(
        initial=False,
        required=False,
        label=_("Attach basic metadata"),
        help_text=_(
            "If checked, a data package will contain a table with basic metadata."
        ),
    )
    resources_pks = forms.CharField(
        widget=forms.HiddenInput(),
    )

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset("", "package_name", "metadata", "resources_pks"),
        )

    def clean_resources_pks(self):
        resources_pks = self.cleaned_data.pop("resources_pks", None)
        if resources_pks:
            pks_list = parse_pks(resources_pks)
            resources = Resource.objects.get_accessible().filter(pk__in=pks_list)
            if resources:
                self.cleaned_data["resources"] = resources


class ResourceRegenerateTokensForm(BaseCrispyForm):
    """
    TODO: docstrings
    """

    resources_pks = forms.CharField(widget=forms.HiddenInput())

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset("", "resources_pks"),
        )

    def clean_resources_pks(self):
        resources_pks = self.cleaned_data.pop("resources_pks", None)
        if resources_pks:
            pks_list = parse_pks(resources_pks)
            resources = Resource.objects.get_accessible().filter(pk__in=pks_list)
            if resources:
                self.cleaned_data["resources"] = resources


class CollectionForm(BaseCrispyModelForm):
    """
    Form definition for creating :class:apps.storage.models.Collection`
    objects.
    """

    managers = OwnerModelMultipleChoiceField(
        queryset=User.objects.all(), required=False
    )
    resources_pks = forms.CharField(widget=forms.widgets.HiddenInput, required=False)
    app = forms.CharField(widget=forms.widgets.HiddenInput, required=False)
    public_resources = forms.BooleanField(
        label=_("Public resources"),
        required=False,
        help_text=_(
            'Set the status of all resources to "public" (it works only for public collections).'
        ),
    )

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset(
                "",
                "name",
                "description",
                "managers",
                "status",
                "public_resources",
                "resources_pks",
                "app",
            ),
        )

    class Meta:
        model = Collection
        fields = ["name", "description", "status", "managers"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["managers"].help_text = None

    def clean(self):
        """
        When collection and its resources are saved and the status
        of this collection is set to "Public" or "OnDemand" check
        if this collection can be published or shared. This should
        be only possible when a user has editing rights to all
        resources that are part of this collection.
        """
        cleaned_data = super().clean()
        user = get_current_user()
        status = cleaned_data.get("status")
        resources_pks = cleaned_data.pop("resources_pks", None)
        app = cleaned_data.pop("app", None)
        if resources_pks:
            pks_list = parse_pks(resources_pks)
            if app == "media_classification":
                resources = Resource.objects.get_accessible(user=user).filter(
                    classifications__pk__in=pks_list
                )
            else:
                resources = Resource.objects.get_accessible(user=user).filter(
                    pk__in=pks_list
                )
            if not resources.count() == len(pks_list):
                errors = _(
                    "You do not have rights to all resources you want to add "
                    "to this collection."
                )
                raise forms.ValidationError(errors)
            cleaned_data["resources"] = resources
        if self.instance.pk:
            resources_all = self.instance.resources.all()
        else:
            resources_all = cleaned_data.get("resources", None)
        if not resources_all:
            return cleaned_data
        if status in [CollectionStatus.PUBLIC, CollectionStatus.ON_DEMAND]:
            if user.is_staff:
                resources_user = resources_all
            else:
                resources_user = resources_all.filter(Q(owner=user) | Q(managers=user))
            if resources_user.count() != resources_all.count():
                errors = mark_safe(
                    _(
                        "You are not allowed to set the status of this collection "
                        f"to {status} as you are not the owner of all resources "
                        "that are part of this collection."
                    )
                )
                raise forms.ValidationError(errors)
            if status == CollectionStatus.PUBLIC and cleaned_data.get(
                "public_resources"
            ):
                resources_user.update(status=ResourceStatus.PUBLIC)
        return cleaned_data

    def save(self, commit=True):
        """When form is saved, then all m2m relations are processed:
        * resources that can be added to collection are saved
        * managers are updated
        """
        collection = super().save(commit=False)
        resources = self.cleaned_data.get("resources", None)
        managers = self.cleaned_data.get("managers", None)
        if commit:
            collection.save()
        if resources:
            collection.resources.set(resources)
        collection.managers.set(managers)
        return collection


class CollectionResourceAppendForm(BaseCrispyForm):
    """"""

    collection = forms.ModelChoiceField(
        queryset=Collection.objects.none(),
        help_text=_("To which collection you want to add selected resources?"),
    )
    resources_pks = forms.CharField(
        widget=forms.HiddenInput(),
    )
    select2_ajax_fields = (("collection", "/storage/api/collections", "pk", "name"),)

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset("", "collection", "resources_pks"),
        )

    def clean_resources_pks(self):
        resources_pks = self.cleaned_data.pop("resources_pks", None)
        collection = self.cleaned_data.get("collection", None)

        if collection and resources_pks:
            pks_list = parse_pks(resources_pks)
            resources = Resource.objects.get_accessible().filter(pk__in=pks_list)
            new_resources = resources.exclude(
                pk__in=collection.resources.values_list("pk", flat=True)
            )

            if not collection.status == CollectionStatus.PRIVATE:
                n = resources.exclude(
                    Q(owner=self.user) | Q(managers=self.user)
                ).count()
                if n != 0:
                    error = mark_safe(
                        _(
                            "You have no permission to add some of selected resources "
                            f'to this "{collection.status.lower()}" collection'
                        )
                    )
                    self.cleaned_data["error"] = error

            if resources:
                if not new_resources:
                    error = mark_safe(
                        _("All selected resources are already in this collection.")
                    )
                    self.cleaned_data["error"] = error
                else:
                    self.cleaned_data["new_resources"] = new_resources
            else:
                error = mark_safe(
                    _(
                        "Nothing to process (most probably you have no permission "
                        "to run this action on selected resources)."
                    )
                )
                self.cleaned_data["error"] = error

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["collection"].widget = forms.CharField.widget()
        self.user = get_current_user()
        self.fields["collection"].queryset = Collection.objects.get_editable(
            user=self.user
        )


class BulkUpdateCollectionForm(BaseBulkUpdateForm):
    """"""

    class Meta:
        model = Collection
        fields = ("status", "managers")

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset("", "status", "managers", "records_pks"),
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["managers"].help_text = ""


class CollectionRequestForm(BaseCrispyForm):
    """"""

    REQUIRED_PROJECT_ROLES = [
        ResearchProjectRoleType.ADMIN,
        ResearchProjectRoleType.COLLABORATOR,
    ]

    project = forms.ModelChoiceField(
        queryset=ResearchProject.objects.none(),
        help_text=_(
            "In which project are you planning to use the requested collection?"
        ),
    )
    text = forms.CharField(widget=Textarea())
    object_pk = forms.IntegerField(widget=forms.HiddenInput())

    def __init__(self, **kwargs):
        """Limit available research projects only to those that are
        accessible to a user"""
        super().__init__(**kwargs)
        user = get_current_user()
        self.fields["project"].queryset = (
            ResearchProject.objects.filter(status=True)
            .filter(
                Q(owner=user)
                | (
                    Q(project_roles__name__in=self.REQUIRED_PROJECT_ROLES)
                    & Q(project_roles__user=user)
                )
            )
            .distinct()
        )

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset("", "project", "text", "object_pk"),
        )


class CollectionUploadConfigForm(BaseCrispyForm):
    """"""

    definition_file = RestrictedFileField(
        label=_("Data package definition file (YAML)"),
        required=False,
        max_upload_size=settings.MAX_UPLOAD_YAML_SIZE,
        help_text=format_lazy(
            _("File size must be under {max}. Only YAML files are allowed."),
            max=filesizeformat(settings.MAX_UPLOAD_YAML_SIZE),
        ),
    )

    def __init__(self, *args, **kwargs):
        """Get currently logged in user that will be used to determine
        where to look for previously uploaded files, and then
        generate list of files that are available for selection (by default
        only .yaml or .yml files are allowed)"""
        super().__init__(*args, **kwargs)

        user = get_current_user()
        self.username = user.username
        path = get_external_collections_path(username=self.username)
        files = []

        # If upload user directory doesn't exists, create it
        if not os.path.exists(path):
            os.makedirs(path)

        for filename in os.listdir(path):
            ext = os.path.splitext(filename)[1]
            if ext in [".yaml", ".yml"]:
                files.append(filename)
        files.sort()

        self.fields["uploaded_yaml"] = forms.ChoiceField(
            label=_("Uploaded data package definition file (YAML)"),
            choices=[("", "---------")] + [(file, file) for file in files],
            required=False,
            help_text=_(
                "Optionally choose already uploaded data package definition file "
                "(e.g. through FTP)"
            ),
        )

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset("", "definition_file", "uploaded_yaml"),
        )

    def clean(self):
        """Verify that either yaml file was submitted or already uploaded
        file was selected"""
        cleaned_data = self.cleaned_data

        definition_file = cleaned_data.get("definition_file")
        uploaded_yaml = cleaned_data.get("uploaded_yaml")

        if not definition_file and not uploaded_yaml:
            errors = mark_safe(_("You have to choose one of two options."))
            raise forms.ValidationError(errors)

        elif definition_file and uploaded_yaml:
            errors = mark_safe(_("You can select only one option."))
            raise forms.ValidationError(errors)
        return cleaned_data

    def clean_definition_file(self):
        """Validate config file to be valid yaml file and
        match schema used for uploading collections"""

        definition_file = self.cleaned_data.get("definition_file")
        if definition_file:
            user = get_current_user()
            definition_file_content = definition_file.read()
            processor = CollectionProcessor(
                definition_file=definition_file_content,
                owner=user,
                read_definition_file=False,
            )
            errors = processor.validate_definition()
            if errors:
                errors = mark_safe(
                    errors.replace(CollectionProcessor.SEPARATOR, "<br/>")
                )
                raise forms.ValidationError(errors)
            # save uploaded file in user's external_media directory
            file_path = get_external_collections_path(
                username=self.username, filename=definition_file.name
            )
            with open(file_path, "w") as destination:
                destination.write(definition_file_content.decode())
            return file_path
        return None

    def clean_uploaded_yaml(self):
        """Uploaded yaml file has to exist on the server.
        If it does, then the full path is returned as uploaded_media"""

        uploaded_yaml = self.cleaned_data.get("uploaded_yaml")
        if uploaded_yaml:
            user = get_current_user()
            file_path = get_external_collections_path(
                username=self.username, filename=uploaded_yaml
            )
            if not os.path.exists(file_path):
                errors = mark_safe(
                    _(
                        "We are sorry but this yaml file does not exist on "
                        "the server anymore."
                    )
                )
                raise forms.ValidationError(errors)
            processor = CollectionProcessor(definition_file=file_path, owner=user)
            errors = processor.validate_definition()
            if errors:
                errors = mark_safe(
                    errors.replace(CollectionProcessor.SEPARATOR, "<br/>")
                )
                raise forms.ValidationError(errors)
            return file_path
        return None


class CollectionUploadDataForm(BaseCrispyForm):
    """"""

    archive_file = RestrictedFileField(
        label=_("Collection archive"),
        required=False,
        max_upload_size=settings.MAX_UPLOAD_COLLECTION_SIZE,
        help_text=format_lazy(
            _("File size must be under {max}. Only ZIP files are allowed."),
            max=filesizeformat(settings.MAX_UPLOAD_COLLECTION_SIZE),
        ),
    )
    remove_zip = forms.BooleanField(
        label=_("Remove zip file"),
        initial=False,
        required=False,
        help_text=_("Remove zip file from storage after successful collection upload"),
    )

    def __init__(self, *args, **kwargs):
        """Get currently logged in user that will be used to determine
        where to look for previously uploaded files, and then
        generate list of files that are available for selection (by default
        only .zip files are allowed)"""
        super().__init__(*args, **kwargs)

        user = get_current_user()
        self.username = user.username
        path = get_external_collections_path(username=self.username)
        files = []

        # If upload user directory doesn't exists, create it
        if not os.path.exists(path):
            os.makedirs(path)

        for filename in os.listdir(path):
            ext = os.path.splitext(filename)[1]
            if ext in CollectionSettings.MEDIA_EXTENSIONS:
                files.append(filename)
        files.sort()

        self.fields["uploaded_media"] = forms.ChoiceField(
            label=_("Uploaded archive"),
            choices=[("", "---------")] + [(file, file) for file in files],
            required=False,
            help_text=_(
                "Optionally choose already uploaded data package (e.g. through FTP)"
            ),
        )

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset("", "archive_file", "uploaded_media", "remove_zip"),
        )

    def clean(self):
        """Verify that either archive file was submitted or already uploaded
        file was selected"""
        cleaned_data = self.cleaned_data

        archive_file = cleaned_data.get("archive_file")
        uploaded_media = cleaned_data.get("uploaded_media")

        if not archive_file and not uploaded_media:
            errors = mark_safe(_("You have to choose one of two options."))
            raise forms.ValidationError(errors)

        elif archive_file and uploaded_media:
            errors = mark_safe(_("You can select only one option."))
            raise forms.ValidationError(errors)
        return cleaned_data

    def clean_archive_file(self):
        """Uploaded archive has to be at least valid zipfile"""
        archive_file = self.cleaned_data.get("archive_file")
        if archive_file:
            if not zipfile.is_zipfile(archive_file):
                errors = mark_safe(_("This is not a valid ZIP file."))
                raise forms.ValidationError(errors)
            # save uploaded file in user's external_media directory
            file_path = get_external_collections_path(
                username=self.username, filename=archive_file.name
            )
            with open(file_path, "wb+") as destination:
                for chunk in archive_file.chunks():
                    destination.write(chunk)
            return file_path
        return None

    def clean_uploaded_media(self):
        """Uploaded media has to exist on the server.
        If it does, then the full path is returned as uploaded_media"""
        uploaded_media = self.cleaned_data.get("uploaded_media")
        if uploaded_media:
            file_path = get_external_collections_path(
                username=self.username, filename=uploaded_media
            )
            if not os.path.exists(file_path):
                errors = mark_safe(
                    _(
                        "We are sorry but this data package does not exist on "
                        "the server anymore."
                    )
                )
                raise forms.ValidationError(errors)
            if not zipfile.is_zipfile(file_path):
                errors = mark_safe(_("This is not a valid ZIP file."))
                raise forms.ValidationError(errors)
            return file_path
        return None


class AdminSetOwnerManagersForm(forms.Form):
    owner = forms.ModelChoiceField(queryset=User.objects.all(), required=False)

    managers = forms.ModelMultipleChoiceField(
        queryset=User.objects.all(), required=False
    )

    def clean(self):
        """"""
        cleaned_data = self.cleaned_data
        owner = cleaned_data.get("owner")
        managers = cleaned_data.get("managers")

        if not owner and not managers:
            errors = mark_safe(_("You have to choose at aleast one option."))
            raise forms.ValidationError(errors)
        return cleaned_data
