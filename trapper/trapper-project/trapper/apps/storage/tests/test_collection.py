# -*- coding: utf-8 -*-
import os
import json
import shutil
from datetime import datetime
from unittest import mock
from unittest.mock import Mock

import factory
import pytz
from django.contrib.auth.models import AnonymousUser
from django.urls import reverse
from django.utils.lorem_ipsum import words
from trapper.apps.accounts.tests.factories.user import UserFactory

from trapper.apps.accounts.utils import get_external_collections_path
from trapper.apps.common.utils.test_tools import (
    ExtendedTestCase,
    CollectionTestMixin,
    ResearchProjectTestMixin,
)
from trapper.apps.geomap.tests.factories.deployment import DeploymentFactory
from trapper.apps.geomap.tests.factories.location import LocationFactory
from trapper.apps.messaging.models import CollectionRequest
from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)
from trapper.apps.storage.forms import CollectionRequestForm, CollectionForm
from trapper.apps.storage.models import Resource, Collection, delete_files
from trapper.apps.storage.taxonomy import (
    ResourceStatus,
    CollectionStatus,
    CollectionMemberLevels,
)
from trapper.apps.research.models import ResearchProjectCollection
from trapper.apps.storage.tests.factories.collection import CollectionFactory
from trapper.apps.storage.tests.factories.resource import ResourceFactory


class BaseCollectionTestCase(ExtendedTestCase, CollectionTestMixin):
    def setUp(self):
        super().setUp()
        self.summon_alice()
        self.summon_ziutek()
        self.collection_public = self.create_collection(
            owner=self.alice, status=CollectionStatus.PUBLIC
        )
        self.collection_ondemand = self.create_collection(
            owner=self.alice, status=CollectionStatus.ON_DEMAND
        )
        self.collection_private = self.create_collection(
            owner=self.alice, status=CollectionStatus.PRIVATE
        )

    def get_collection_create_url(self):
        """Default url for creating collection"""
        return reverse("storage:collection_create")

    def get_collection_details_url(self, collection):
        """Default url for details"""
        return reverse("storage:collection_detail", kwargs={"pk": collection.pk})

    def get_collection_update_url(self, collection):
        """Default url for update"""
        return reverse("storage:collection_update", kwargs={"pk": collection.pk})

    def get_collection_delete_url(self, collection):
        """Default url for delete"""
        return reverse("storage:collection_delete", kwargs={"pk": collection.pk})

    def get_collection_request_url(self, collection):
        """Default url for collection permission request"""
        return reverse("storage:collection_request", kwargs={"pk": collection.pk})


class AnonymousCollectionTestCase(BaseCollectionTestCase):
    """
    Collection logic for anonymous users.
    """

    def test_collection_list(self):
        """
        Anonymous user can not access collection list via UI. It should be
        redirected to a login page.
        """
        url = reverse("storage:collection_list")
        self.assert_redirect(url)

    def test_collection_json(self):
        """
        Anonymous user can not see collections via API.
        """
        url = reverse("storage:api-collection-list")
        self.assert_forbidden(url=url)

    def test_collection_details(self):
        """
        Anonymous user has no permissions to access collection details
        view using UI.
        """
        url_private = self.get_collection_details_url(
            collection=self.collection_private
        )
        url_ondemand = self.get_collection_details_url(
            collection=self.collection_ondemand
        )
        url_public = self.get_collection_details_url(collection=self.collection_public)
        self.assert_forbidden(url=url_private)
        self.assert_forbidden(url=url_ondemand)
        self.assert_forbidden(url=url_public)

    def test_collection_delete(self):
        """
        Anonymous user should not get access here.
        """
        url = reverse("storage:collection_delete", kwargs={"pk": 1})
        self.assert_forbidden(url=url, method="get")
        self.assert_forbidden(url=url, method="post")

    def test_collection_delete_multiple(self):
        """
        Anonymous user should not get access here.
        """
        url = reverse("storage:collection_delete_multiple")
        self.assert_forbidden(url=url, method="post")

    def test_collection_update(self):
        """
        Anonymous user has no permissions to update.
        """
        url = reverse("storage:collection_update", kwargs={"pk": 1})
        self.assert_forbidden(url=url, method="post")

    def test_collection_request(self):
        """
        Anonymous user has to login before requesting access.
        """
        url = reverse("storage:collection_request", kwargs={"pk": 1})
        self.assert_forbidden(url=url, method="post")

    def test_bulk_update(self):
        """
        Anonymous user has to login before using bulk update.
        """
        url = reverse("storage:collection_bulk_update")
        self.assert_forbidden(url=url, method="get")
        self.assert_forbidden(url=url, method="post")

    def test_create_collection(self):
        """
        Anonymous user has to login before creating collection.
        """
        url = reverse("storage:collection_create")
        self.assert_forbidden(url=url, method="get")
        self.assert_forbidden(url=url, method="post")

    def test_append_collection(self):
        """
        Anonymous user has no permissions to append to collection.
        """
        url = reverse("storage:collection_append")
        self.assert_forbidden(url=url, method="post")


class CollectionPermissionsListTestCase(BaseCollectionTestCase):
    """
    Collection listing permission logic for logged in user.
    """

    def setUp(self):
        """
        Login alice by default for all tests.
        """
        super().setUp()
        self.login_alice()

    def test_collection_list(self):
        """
        Logged in user can access collection lisst.
        """
        url = reverse("storage:collection_list")
        self.assert_access_granted(url)

    def test_collection_json(self):
        """
        Logged in user can see:
        * PUBLIC collections
        * ONDEMAND collections even if user has no access to view details
        * Owned PRIVATE collections
        User cannot see other's people PRIVATE collections.
        """
        url = reverse("storage:api-collection-list")
        self.collection_private_ziutek = self.create_collection(
            owner=self.ziutek, status=CollectionStatus.PRIVATE
        )
        response = self.client.get(url)
        content = json.loads(response.content)["results"]
        collection_pk_list = [item["pk"] for item in content]
        self.assertIn(self.collection_public.pk, collection_pk_list, "Public not shown")
        self.assertIn(
            self.collection_ondemand.pk, collection_pk_list, "OnDemand not shown"
        )
        self.assertIn(
            self.collection_private.pk, collection_pk_list, "Private not shown (own)"
        )
        self.assertNotIn(
            self.collection_private_ziutek.pk,
            collection_pk_list,
            "Private not shown (ziutek)",
        )


class CollectionPermissionsDetailsTestCase(BaseCollectionTestCase):
    """
    Details permission logic for logged in user.
    """

    def test_details_owner(self):
        """
        Collection owner can access details.
        """
        self.login_alice()
        collection = self.create_collection(owner=self.alice)
        url = self.get_collection_details_url(collection=collection)
        self.assert_access_granted(url)

    def test_details_manager(self):
        """
        Collection manager can access details.
        """
        self.login_alice()
        collection = self.create_collection(owner=self.ziutek, managers=[self.alice])
        url = self.get_collection_details_url(collection=collection)
        self.assert_access_granted(url)

    def test_details_role_access(self):
        """
        CollectionMember with access level can access details.
        """
        self.login_alice()
        collection = self.create_collection(
            owner=self.ziutek, roles=[(self.alice, CollectionMemberLevels.ACCESS)]
        )
        url = self.get_collection_details_url(collection=collection)
        self.assert_access_granted(url)

    def test_details_role_access_basic(self):
        """
        CollectionMember with basic access can not access details.
        """
        self.login_alice()
        collection = self.create_collection(
            owner=self.ziutek, roles=[(self.alice, CollectionMemberLevels.ACCESS_BASIC)]
        )
        url = self.get_collection_details_url(collection=collection)
        self.assert_forbidden(url)

    def test_details_no_roles(self):
        """
        User without any roles which is neither manager nor owner can not
        access details.
        """
        self.login_alice()
        collection = self.create_collection(
            owner=self.ziutek,
        )
        url = self.get_collection_details_url(collection=collection)
        self.assert_forbidden(url)

    def test_list_resources_allowed(self):
        """
        If user has enough permission, she can see resources assigned to
        particular collection via API.
        """
        self.login_alice()
        resource_public = self.create_resource(
            owner=self.alice, status=ResourceStatus.PUBLIC
        )
        resource_private = self.create_resource(
            owner=self.alice, status=ResourceStatus.PRIVATE
        )
        collection = self.create_collection(
            owner=self.alice, resources=[resource_private, resource_public]
        )
        url = "{url}?collections={collection}".format(
            url=reverse("storage:api-resource-list"), collection=collection.pk
        )
        response = self.assert_access_granted(url)
        self.assertEqual(len(json.loads(response.content)["results"]), 2)

    def test_list_resources_forbidden(self):
        """
        If user has not enough permission, resource API should return
        only public records.
        """
        self.login_ziutek()
        resource_public = self.create_resource(
            owner=self.alice, status=ResourceStatus.PUBLIC
        )
        resource_private = self.create_resource(
            owner=self.alice, status=ResourceStatus.PRIVATE
        )
        collection = self.create_collection(
            owner=self.alice,
            resources=[resource_private, resource_public],
            status=CollectionStatus.PUBLIC,
        )
        url = "{url}?collections={collection}".format(
            url=reverse("storage:api-resource-list"), collection=collection.pk
        )
        response = self.assert_access_granted(url)
        # Private resource is exluded
        self.assertEqual(len(json.loads(response.content)["results"]), 1)


class CollectionPermissionsUpdateTestCase(BaseCollectionTestCase):
    """
    Update collection permission logic for logged in user.
    """

    def setUp(self):
        """
        Login alice by default for all tests.
        """
        super().setUp()
        self.login_alice()

    def test_get_update_owner(self):
        """
        Resource owner can update resource.
        """
        collection = self.create_collection(owner=self.alice)
        url = self.get_collection_update_url(collection=collection)
        self.assert_access_granted(url, method="post", data={"name": "test"})

    def test_get_update_manager(self):
        """
        Collection manager can update collection.
        """
        collection = self.create_collection(owner=self.ziutek, managers=[self.alice])
        url = self.get_collection_update_url(collection=collection)
        self.assert_access_granted(url, method="post", data={"name": "test"})

    def test_get_update_role_access(self):
        """
        CollectionMember with access level can not update collection.
        """
        collection = self.create_collection(
            owner=self.ziutek, roles=[(self.alice, CollectionMemberLevels.ACCESS)]
        )
        url = self.get_collection_update_url(collection=collection)
        self.assert_forbidden(url, method="post", data={"name": "test"})

    def test_get_update_role_access_basic(self):
        """
        CollectionMember with basic access level can not update collection.
        """
        collection = self.create_collection(
            owner=self.ziutek, roles=[(self.alice, CollectionMemberLevels.ACCESS_BASIC)]
        )
        url = self.get_collection_update_url(collection=collection)
        self.assert_forbidden(url, method="post", data={"name": "test"})

    def test_get_update_no_roles(self):
        """
        User without any roles which is neither manager nor owner can not
        update collection.
        """
        collection = self.create_collection(owner=self.ziutek)
        url = self.get_collection_update_url(collection=collection)
        self.assert_forbidden(url, method="post", data={"name": "test"})


class CollectionPermissionsDeleteTestCase(BaseCollectionTestCase):
    """
    Delete collection permission logic for logged in user. Note that delete
    multiple uses the same permission logic.
    """

    def setUp(self):
        """
        Login alice by default for all tests and prepare default
        redirection url.
        """
        super().setUp()
        self.login_alice()
        self.redirection = reverse("storage:collection_list")

    def test_delete_owner(self):
        """
        Collection owner can delete collection.
        """
        collection = self.create_collection(owner=self.alice)
        url = self.get_collection_delete_url(collection=collection)
        self.assert_redirect(url, self.redirection)

    def test_delete_manager(self):
        """
        Collection manager can delete collection.
        """
        collection = self.create_collection(owner=self.ziutek, managers=[self.alice])
        url = self.get_collection_delete_url(collection=collection)
        self.assert_redirect(url, self.redirection)

    def test_delete_role_access(self):
        """
        CollectionMember with access level can not delete collection.
        """
        collection = self.create_collection(
            owner=self.ziutek, roles=[(self.alice, CollectionMemberLevels.ACCESS)]
        )
        url = self.get_collection_delete_url(collection=collection)
        self.assert_forbidden(url)

    def test_delete_role_access_basic(self):
        """
        CollectionMember with basic access level can not delete collection.
        """
        collection = self.create_collection(
            owner=self.ziutek, roles=[(self.alice, CollectionMemberLevels.ACCESS_BASIC)]
        )
        url = self.get_collection_delete_url(collection=collection)
        self.assert_forbidden(url)

    def test_delete_no_roles(self):
        """
        User without any roles which is neither manager nor owner cannot delete
        collection.
        """
        collection = self.create_collection(
            owner=self.ziutek,
        )
        url = self.get_collection_delete_url(collection=collection)
        self.assert_forbidden(url)


class CollectionPermissionsAskAccessTestCase(BaseCollectionTestCase):
    """
    Ask for collection access permission logic for logged in user.
    """

    def setUp(self):
        """
        Login alice by default for all tests and prepare default
        redirection url.
        """
        super().setUp()
        self.login_alice()
        self.redirection = reverse("storage:collection_list")

    def test_ask_owner(self):
        """
        There is no point to ask for own collection.
        """
        collection = self.create_collection(owner=self.alice)
        url = self.get_collection_request_url(collection=collection)
        self.assert_forbidden(url, method="post")

    def test_ask_manager(self):
        """
        There is no point to ask for access to collection where user is
        a manager.
        """
        collection = self.create_collection(owner=self.ziutek, managers=[self.alice])
        url = self.get_collection_request_url(collection=collection)
        self.assert_forbidden(url, method="post")

    def test_ask_role_access(self):
        """
        There is no point to ask for access to collection if access is
        already granted.
        """
        collection = self.create_collection(
            owner=self.ziutek, roles=[(self.alice, CollectionMemberLevels.ACCESS)]
        )
        url = self.get_collection_request_url(collection=collection)
        self.assert_forbidden(url, method="post")

    def test_ask_no_roles(self):
        """
        User without any roles which is neither manager nor owner can ask for
        permission.
        """
        collection = self.create_collection(owner=self.ziutek)
        url = self.get_collection_request_url(collection=collection)
        response = self.assert_access_granted(url)
        form = self.assert_context_variable(response, "form")
        self.assertTrue(isinstance(form, CollectionRequestForm))


class CollectionPermissionsBulkUpdateTestCase(BaseCollectionTestCase):
    """
    Bulk update permission logic for logged in user.
    """

    def setUp(self):
        """
        Login alice by default for all tests and prepare default
        redirection url.
        """
        super().setUp()
        self.login_alice()
        self.redirection = reverse("storage:collection_list")

    def _call_helper(self, collection):
        """
        All tests call the same logic annd logged in user will get always
        response status 200. Difference is in response data which is json.
        """
        url = reverse("storage:collection_bulk_update")
        data = {
            "records_pks": [collection.pk],
            "status": CollectionStatus.PUBLIC,
            # TODO: fix bulk update form validation as now it is partly based
            # on a minimum number of fields in the posted data; thats why we
            # need here this crsf "mockup" field
            "csrfmockup": "123sth",
        }
        response = self.assert_access_granted(url, method="post", data=data)
        status = self.assert_json_context_variable(response, "success")
        return status

    def test_bulk_update_owner(self):
        """
        Owner can perform bulk update operation on collections.
        """
        collection = self.create_collection(owner=self.alice)
        status = self._call_helper(collection=collection)
        self.assertTrue(status)

    def test_bulk_update_manager(self):
        """
        Manager can perform bulk operation on collections.
        """
        collection = self.create_collection(owner=self.ziutek, managers=[self.alice])
        status = self._call_helper(collection=collection)
        self.assertTrue(status)

    def test_bulk_update_role_access(self):
        """
        ACCESS role is not enough to perform bulk update operation on
        collections.
        """
        collection = self.create_collection(
            owner=self.ziutek, roles=[(self.alice, CollectionMemberLevels.ACCESS)]
        )
        status = self._call_helper(collection=collection)
        self.assertFalse(status)

    def test_bulk_update_role_access_basic(self):
        """
        BASIC ACCESS status is not enough to perform bulk update operation on
        collections.
        """
        collection = self.create_collection(
            owner=self.ziutek, roles=[(self.alice, CollectionMemberLevels.ACCESS_BASIC)]
        )
        status = self._call_helper(collection=collection)
        self.assertFalse(status)

    def test_bulk_update_no_roles(self):
        """
        User without any roles which is neither manager nor owner can not perform
        bulk update operation on collections.
        """
        collection = self.create_collection(owner=self.ziutek)
        status = self._call_helper(collection=collection)
        self.assertFalse(status)


class CollectionTestCase(BaseCollectionTestCase, ResearchProjectTestMixin):
    """
    Tests related to modifying collections by performing various actions.
    """

    def test_collection_create(self):
        """
        Using create view create storage.Collection model instance.
        """
        resources = [self.create_resource(owner=self.alice) for _counter in range(3)]
        resources_pk = [resource.pk for resource in resources]
        url = reverse("storage:collection_create")
        self.login_alice()
        name = words(count=5, common=False)
        params = {
            "name": name,
            "status": CollectionStatus.PRIVATE,
            "resources_pks": ",".join([str(k) for k in resources_pk]),
        }
        response = self.client.post(url, params)
        self.assertEqual(response.status_code, 200, response.status_code)
        success = self.assert_json_context_variable(response, variable="success")
        self.assertTrue(success)
        self.assertTrue(
            Collection.objects.filter(
                name=name,
                status=CollectionStatus.PRIVATE,
            ).exists()
        )
        self.assertEqual(
            list(
                Collection.objects.get(
                    name=name, status=CollectionStatus.PRIVATE
                ).resources.values_list("pk", flat=True)
            ),
            sorted(resources_pk, reverse=True),
        )

    def test_collection_append(self):
        """
        Using append collection view alter existing collection by adding
        additional resource.
        """
        resource = self.create_resource(owner=self.alice)
        resource_append = self.create_resource(owner=self.alice)
        resource_append2 = self.create_resource(owner=self.alice)
        collection = self.create_collection(owner=self.alice, resources=[resource])
        url = reverse("storage:collection_append")
        self.login_alice()
        data = {
            "collection": collection.pk,
            "resources_pks": ",".join(
                [str(resource_append.pk), str(resource_append2.pk)]
            ),
        }
        response = self.assert_access_granted(url, method="post", data=data)
        self.assertTrue(self.assert_json_context_variable(response, "success"))
        resource_pks = Collection.objects.get(pk=collection.pk).resources.values_list(
            "pk", flat=True
        )
        self.assertEqual(
            list(resource_pks), ([resource_append2.pk, resource_append.pk, resource.pk])
        )

    def test_collection_update(self):
        """
        Using update view logged in user that has enough permissions can
        change existing collection.
        """
        self.login_alice()
        collection = self.create_collection(
            owner=self.alice, status=CollectionStatus.PRIVATE
        )
        url = reverse("storage:collection_update", kwargs={"pk": collection.pk})
        attributes = {
            "name": "test_name",
            "status": CollectionStatus.PUBLIC,
        }
        self.client.post(url, attributes)
        new_collection = Collection.objects.get(pk=collection.pk)
        self.assertEqual(new_collection.name, attributes["name"])
        self.assertEqual(new_collection.status, attributes["status"])

    def test_collection_delete(self):
        """
        Using delete view logged in user that has enough permissions can
        delete existing collection using GET.
        """
        self.login_alice()
        collection = self.create_collection(
            owner=self.alice, status=CollectionStatus.PRIVATE
        )
        url = reverse("storage:collection_delete", kwargs={"pk": collection.pk})
        self.client.get(url)
        self.assertFalse(Collection.objects.filter(pk=collection.pk).exists())

    def test_collection_delete_multiple(self):
        """
        Using delete view logged in user that has enough permissions can
        delete multiple collections using POST.
        """
        self.login_alice()
        collection1 = self.create_collection(
            owner=self.alice, status=CollectionStatus.PRIVATE
        )
        collection2 = self.create_collection(
            owner=self.alice, status=CollectionStatus.PRIVATE
        )
        url = reverse("storage:collection_delete_multiple")
        pks_list = [collection1.pk, collection2.pk]
        response = self.client.post(url, data={"pks": ",".join(map(str, pks_list))})
        status = self.assert_json_context_variable(response, "status")
        self.assertTrue(status)
        self.assertFalse(Collection.objects.filter(pk__in=pks_list).exists())

    def test_collection_ask_permissions(self):
        """
        Using ask permissions view logged in user that ask for access to other
        OnDemand collection that user has no access yet.
        """
        self.login_alice()
        collection = self.create_collection(owner=self.ziutek)
        research_project = self.create_research_project(owner=self.alice)
        url = reverse("storage:collection_request", kwargs={"pk": collection.pk})
        self.client.post(
            url,
            data={
                "text": "Test",
                "object_pk": collection.pk,
                "project": research_project.pk,
            },
        )
        self.assertTrue(
            CollectionRequest.objects.filter(
                user=self.ziutek,
                user_from=self.alice,
                collections=collection,
                project=research_project,
            )
        )

    def test_collection_bulk_update(self):
        """
        Using bulk update view logged in user that has enough permissions can
        modify various atributes of multiple collections at once.
        """
        self.login_alice()
        collections = [
            self.create_collection(owner=self.alice) for _counter in range(3)
        ]
        url = reverse("storage:collection_bulk_update")
        data = {
            "records_pks": ",".join(
                [str(collection.pk) for collection in collections] + [str(self.TEST_PK)]
            ),
            "status": CollectionStatus.PUBLIC,
            "managers": [self.ziutek.pk],
        }
        response = self.client.post(url, data=data)
        status = self.assert_json_context_variable(response, "success")
        self.assertTrue(status)
        for collection in collections:
            new_collection = Collection.objects.get(pk=collection.pk)
            self.assertEqual(new_collection.status, data["status"])
            self.assertEqual(
                list(new_collection.managers.values_list("pk", flat=True)),
                data["managers"],
            )

    def test_collection_upload(self):
        """
        To automate the process of creating large collections logged
        in user can upload the YAML data package definition file and then the
        corresponding ZIP with multimedia files. This test is for uploading both
        files via UI.
        """
        resp = self.create_research_project(
            owner=self.alice, name="TEST", acronym="TEST"
        )
        loc1 = self.create_location(owner=self.alice, location_id="001")
        loc2 = self.create_location(owner=self.alice, location_id="002")
        dep1 = self.create_deployment(
            self.alice, location=loc1, deployment_code="S1", research_project=resp
        )
        dep2 = self.create_deployment(
            self.alice, location=loc2, deployment_code="S2", research_project=resp
        )

        yaml_file = os.path.join(
            self.TEST_DATA_PATH, "collection_upload/test_collection.yaml"
        )
        zip_file = os.path.join(
            self.TEST_DATA_PATH, "collection_upload/test_collection.zip"
        )
        url = reverse("storage:collection_upload")
        redirection_url = reverse("storage:collection_list")
        self.login_alice()
        with open(yaml_file, "rb") as yaml_handler:
            params = {
                "collection_upload_wizard_view-current_step": 0,
                "0-definition_file": yaml_handler,
            }
            self.assert_access_granted(url, method="post", data=params)
        with open(zip_file, "rb") as zip_handler:
            params = {
                "collection_upload_wizard_view-current_step": 1,
                "1-archive_file": zip_handler,
            }
            self.assert_redirect(
                url, redirection=redirection_url, method="post", data=params
            )
        self.assertTrue(Collection.objects.filter(name="test_collection").exists())
        self.assertTrue(
            ResearchProjectCollection.objects.filter(
                collection__name="test_collection", project=resp
            ).exists()
        )
        self.assertTrue(
            Resource.objects.filter(name="IMAG001", deployment=dep1).exists()
        )
        self.assertTrue(
            Resource.objects.filter(name="IMAG002", deployment=dep2).exists()
        )

    def test_collection_upload_ftp(self):
        """
        To automate the process of creating large collections logged
        in user can upload the YAML data package definition file and then the
        corresponding ZIP with multimedia files. This test is for selecting
        already uploaded files (via FTP to the `external_media` location) via UI.
        """
        resp = self.create_research_project(
            owner=self.alice, name="TEST", acronym="TEST"
        )
        loc1 = self.create_location(owner=self.alice, location_id="001")
        loc2 = self.create_location(owner=self.alice, location_id="002")
        dep1 = self.create_deployment(
            self.alice, location=loc1, deployment_code="S1", research_project=resp
        )
        dep2 = self.create_deployment(
            self.alice, location=loc2, deployment_code="S2", research_project=resp
        )
        yaml_file = os.path.join(
            self.TEST_DATA_PATH, "collection_upload/test_collection.yaml"
        )
        zip_file = os.path.join(
            self.TEST_DATA_PATH, "collection_upload/test_collection.zip"
        )
        yaml_uploaded = get_external_collections_path(
            self.alice.username, "test_collection.yaml"
        )
        zip_uploaded = get_external_collections_path(
            self.alice.username, "test_collection.zip"
        )
        shutil.copyfile(yaml_file, yaml_uploaded)
        shutil.copyfile(zip_file, zip_uploaded)
        url = reverse("storage:collection_upload")
        redirection_url = reverse("storage:collection_list")
        self.login_alice()
        params = {
            "collection_upload_wizard_view-current_step": 0,
            "0-uploaded_yaml": "test_collection.yaml",
        }
        self.assert_access_granted(url, method="post", data=params)
        params = {
            "collection_upload_wizard_view-current_step": 1,
            "1-uploaded_media": "test_collection.zip",
        }
        self.assert_redirect(
            url, redirection=redirection_url, method="post", data=params
        )
        self.assertTrue(Collection.objects.filter(name="test_collection").exists())
        self.assertTrue(
            ResearchProjectCollection.objects.filter(
                collection__name="test_collection", project=resp
            ).exists()
        )
        self.assertTrue(
            Resource.objects.filter(name="IMAG001", deployment=dep1).exists()
        )
        self.assertTrue(
            Resource.objects.filter(name="IMAG002", deployment=dep2).exists()
        )

    def test_collection_upload_ftp_api(self):
        """
        To automate the process of creating large collections logged
        in user can upload the YAML data package definition file and then the
        corresponding ZIP with multimedia files. This test is for processing
        already uploaded files (via FTP to the `external_media` location) via API.
        """
        resp = self.create_research_project(
            owner=self.alice, name="TEST", acronym="TEST"
        )
        loc1 = self.create_location(owner=self.alice, location_id="001")
        loc2 = self.create_location(owner=self.alice, location_id="002")
        dep1 = self.create_deployment(
            self.alice, location=loc1, deployment_code="S1", research_project=resp
        )
        dep2 = self.create_deployment(
            self.alice, location=loc2, deployment_code="S2", research_project=resp
        )
        yaml_file = os.path.join(
            self.TEST_DATA_PATH, "collection_upload/test_collection.yaml"
        )
        zip_file = os.path.join(
            self.TEST_DATA_PATH, "collection_upload/test_collection.zip"
        )
        yaml_uploaded = get_external_collections_path(
            self.alice.username, "test_collection.yaml"
        )
        zip_uploaded = get_external_collections_path(
            self.alice.username, "test_collection.zip"
        )
        shutil.copyfile(yaml_file, yaml_uploaded)
        shutil.copyfile(zip_file, zip_uploaded)

        url = reverse("storage:api-collection-process")
        self.login_alice()
        data = {
            "yaml_file": "test_collection.yaml",
            "zip_file": "test_collection.zip",
            "remove_zip": True,
        }
        self.assert_access_granted(url, method="post", data=data)
        self.assertTrue(Collection.objects.filter(name="test_collection").exists())
        self.assertTrue(
            ResearchProjectCollection.objects.filter(
                collection__name="test_collection", project=resp
            ).exists()
        )
        self.assertTrue(
            Resource.objects.filter(name="IMAG001", deployment=dep1).exists()
        )
        self.assertTrue(
            Resource.objects.filter(name="IMAG002", deployment=dep2).exists()
        )


def abc():
    return UserFactory()


class CollectionTest(BaseCollectionTestCase):
    """
    Resource faker tests
    """

    @classmethod
    def setUpTestData(cls):
        super(CollectionTest, cls).setUpTestData()

        cls.user1 = UserFactory()
        cls.user2 = UserFactory()
        cls.user3 = UserFactory()
        cls.user4 = UserFactory()

        cls.research_project = ResearchProjectFactory(owner=cls.user1)
        cls.location = LocationFactory(
            owner=cls.user1, research_project=cls.research_project
        )
        cls.deployment = DeploymentFactory(
            owner=cls.user1,
            location=cls.location,
            research_project=cls.research_project,
        )

        cls.resource1 = ResourceFactory(owner=cls.user1)
        cls.resource2 = ResourceFactory(owner=cls.user1)
        cls.resource3 = ResourceFactory(owner=cls.user1, deployment=cls.deployment)

        # Set resource managers
        cls.resource1.managers.set((cls.user2,))

        cls.collection1 = CollectionFactory(owner=cls.user1)
        cls.collection2 = CollectionFactory(owner=cls.user1)

        # # Set collection resources
        cls.collection1.resources.set((cls.resource1,))
        cls.collection2.resources.set((cls.resource1, cls.resource3))

    def test_is_public(self):
        self.assertTrue(self.collection1.is_public)

    def test_get_absolute_url(self):
        url = reverse("storage:collection_detail", kwargs={"pk": self.collection1.pk})
        self.assertEqual(self.collection1.get_absolute_url(), url)

    def test_can_add_to_research_project(self):
        self.assertTrue(self.collection1.can_add_to_research_project(self.user1))

        self.assertFalse(self.collection1.can_add_to_research_project())

    def test_is_used(self):
        self.assertFalse(self.collection1.is_used(user=self.user1))

    def test_refresh_bbox(self):
        self.collection1.refresh_bbox()

    def test_period(self):
        self.assertEqual(
            self.collection1.period,
            (self.collection1.period_begin, self.collection1.period_end),
        )

    def test_refresh_period(self):
        time_now = datetime.now(pytz.utc)
        self.collection1.period_begin = time_now
        self.collection1.period_end = time_now
        self.assertEqual(self.collection1.period_begin, time_now)
        self.assertEqual(self.collection1.period_end, time_now)

        self.collection1.refresh_period()
        recorded_time = self.resource1.date_recorded.astimezone(pytz.utc)
        self.assertEqual(self.collection1.period_begin, recorded_time)
        self.assertEqual(self.collection1.period_end, recorded_time)

    @mock.patch("trapper.apps.storage.models.get_current_user", side_effect=abc)
    def test_manager_get_accessible(self, get_current_user_function):
        self.assertEqual(len(Collection.objects.get_accessible()), 3)

    @mock.patch("trapper.apps.storage.models.get_current_user", side_effect=abc)
    def test_manager_get_ondemand(self, get_current_user_function):
        self.assertEqual(len(Collection.objects.get_ondemand(self.user1)), 1)

    @mock.patch("trapper.apps.storage.models.get_current_user", side_effect=abc)
    def test_manager_get_editable(self, get_current_user_function):
        self.assertEqual(len(Collection.objects.get_editable()), 0)
        base_queryset = Collection.objects.all()
        self.assertEqual(
            len(
                Collection.objects.get_editable(
                    base_queryset=base_queryset, user=self.user1
                )
            ),
            2,
        )

    @mock.patch("trapper.apps.geomap.models.get_current_user", side_effect=abc)
    def test_forms(self, get_current_user_function):
        form_data = {
            "name": "testName",
            "description": "testDescription",
            "status": CollectionStatus.PUBLIC,
        }
        form = CollectionForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_collection_index_route(self):
        self.client.force_login(self.user1)
        response = self.client.get(reverse("storage:collection_index"))
        self.assertEqual(response.status_code, 302)

    def test_collection_list_route(self):
        self.client.force_login(self.user1)
        response = self.client.get(reverse("storage:collection_list"))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Collection")

    def test_collection_list_ondemand_route(self):
        self.client.force_login(self.user1)
        response = self.client.get(reverse("storage:collection_ondemand_list"))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Collection")

    def test_collection_detail_route(self):
        self.client.force_login(self.user1)
        response = self.client.get(
            reverse("storage:collection_detail", kwargs={"pk": self.collection1.pk})
        )
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Collection")

    def test_collection_delete_route(self):
        self.client.force_login(self.user1)
        collection_1 = CollectionFactory(owner=self.user1)
        self.assertTrue(Collection.objects.filter(id=collection_1.pk).exists())
        response = self.client.get(
            reverse("storage:collection_delete", kwargs={"pk": collection_1.pk})
        )
        self.assertEqual(response.status_code, 302)
        self.assertFalse(Collection.objects.filter(id=collection_1.pk).exists())

    def test_collection_resource_delete_route(self):
        self.client.force_login(self.user1)
        resource_1 = ResourceFactory(owner=self.user1)
        resource_2 = ResourceFactory(owner=self.user1)

        collection_1 = CollectionFactory(owner=self.user1)
        collection_1.resources.set(
            (
                resource_1,
                resource_2,
            )
        )

        data = {"pks": f"{resource_1.pk}, {resource_2.pk}"}

        self.assertTrue(Collection.objects.filter(id=collection_1.pk).exists())
        self.assertEqual(collection_1.resources.all().count(), 2)
        response = self.client.post(
            reverse(
                "storage:collection_resource_delete", kwargs={"pk": collection_1.pk}
            ),
            data,
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(collection_1.resources.all().count(), 0)

        response = self.client.post(
            reverse("storage:collection_resource_delete", kwargs={"pk": "1235456789"}),
            data,
        )
        self.assertEqual(response.status_code, 200)

        self.client.logout()
        response = self.client.post(
            reverse("storage:collection_resource_delete", kwargs={"pk": "1235456789"}),
            data,
        )
        self.assertEqual(response.status_code, 403)

    def test_collection_create_route(self):
        self.client.force_login(self.user1)
        new_collection = {
            "name": factory.Faker("word"),
            "description": factory.Faker("sentences"),
            "owner": self.user1,
            "status": CollectionStatus.PUBLIC,
        }
        response = self.client.post(
            reverse("storage:collection_create"), new_collection
        )
        self.assertEqual(response.status_code, 200)

    def test_collection_update_route(self):
        self.client.force_login(self.user1)
        collection_1 = CollectionFactory(owner=self.user1)

        update_collection = {
            "description": "description test",
        }
        response = self.client.post(
            reverse("storage:collection_update", kwargs={"pk": collection_1.pk}),
            update_collection,
        )
        self.assertEqual(response.status_code, 200)

    def test_collection_request_route(self):
        self.client.force_login(self.user4)
        collection_ondemand = CollectionFactory(
            owner=self.user1, status=CollectionStatus.ON_DEMAND
        )
        response = self.client.get(
            reverse("storage:collection_request", kwargs={"pk": collection_ondemand.pk})
        )
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Collection")

    def test_collection_get_accessible(self):
        Collection.objects.get_accessible(user=self.user1)
        Collection.objects.get_accessible(
            user=self.user1, base_queryset=Collection.objects.all()
        )
        Collection.objects.get_accessible(user=AnonymousUser())

    def test_collection_get_ondemand(self):
        Collection.objects.get_ondemand(user=self.user1)
        Collection.objects.get_ondemand(
            user=self.user1, base_queryset=Collection.objects.all()
        )
        Collection.objects.get_ondemand(user=AnonymousUser())

    def test_collection_get_editable(self):
        Collection.objects.get_editable(user=self.user1)
        Collection.objects.get_editable(
            user=self.user1, base_queryset=Collection.objects.all()
        )
        Collection.objects.get_editable(user=AnonymousUser())

    def test_collection_str(self):
        print(self.collection1)

    def test_collection_is_used(self):
        self.collection2.is_used(user=self.user1)
        self.collection2.is_used(user=self.user1, rproject=self.research_project)

    def test_delete_files(self):
        class MockFile:
            value = True

            def delete(self, val):
                return True

        instance_mock = Mock()
        instance_mock.file = MockFile()
        instance_mock.file_thumbnail = MockFile()
        instance_mock.extra_file = MockFile()
        instance_mock.file_preview = MockFile()

        delete_files("", instance_mock)
        instance_mock.file = False
        delete_files("", instance_mock)
        instance_mock.file_thumbnail = False
        delete_files("", instance_mock)
        instance_mock.extra_file = False
        delete_files("", instance_mock)
        instance_mock.file_preview = False
        delete_files("", instance_mock)
