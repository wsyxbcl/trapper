import factory

from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.storage.models import Collection
from trapper.apps.storage.taxonomy import CollectionStatus


class CollectionFactory(factory.DjangoModelFactory):
    class Meta:
        model = Collection

    name = factory.Faker("word")
    description = factory.Faker("sentences")
    owner = factory.SubFactory(UserFactory)
    status = CollectionStatus.PUBLIC
