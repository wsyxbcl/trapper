from django.utils.timezone import localtime, now
import factory

from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.geomap.tests.factories.deployment import DeploymentFactory
from trapper.apps.storage.models import Resource
from trapper.apps.storage.taxonomy import ResourceType, ResourceStatus


class ResourceFactory(factory.DjangoModelFactory):
    class Meta:
        model = Resource

    name = factory.Faker("word")
    file = factory.django.ImageField(
        filename="image.jpg", width=200, height=200, color="green"
    )
    extra_file = factory.django.FileField(filename="mp4.mp4")
    owner = factory.SubFactory(UserFactory)
    resource_type = ResourceType.TYPE_VIDEO
    file_thumbnail = factory.django.FileField(filename="thumbnail.jpg")
    date_recorded = localtime(now())

    status = ResourceStatus.PUBLIC
    description = factory.Faker("sentences")

    deployment = factory.SubFactory(DeploymentFactory, owner=owner)
