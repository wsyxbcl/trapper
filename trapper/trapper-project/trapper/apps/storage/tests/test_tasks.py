from django.test import TestCase
from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.storage.models import Resource
from trapper.apps.storage.tasks import (
    celery_update_thumbnails,
    celery_create_media_package,
    celery_regenerate_tokens,
)
from trapper.apps.storage.tests.factories.resource import ResourceFactory


class StorageTasksTest(TestCase):
    """Tests related to user django admin page"""

    @classmethod
    def setUpTestData(cls):
        super(StorageTasksTest, cls).setUpTestData()
        cls.user1 = UserFactory()
        cls.user2 = UserFactory()

        cls.resource1 = ResourceFactory(owner=cls.user1)
        cls.resource2 = ResourceFactory(owner=cls.user1)

        # Set resource managers
        cls.resource1.managers.set((cls.user2,))

    def test_celery_update_thumbnails(self):
        resources_none_queryset = Resource.objects.none()
        celery_update_thumbnails(resources_none_queryset)

    def test_celery_create_media_package(self):
        resources = Resource.objects.all()
        celery_create_media_package([], self.user1, "test_package")
        celery_create_media_package([], self.user1, "")  # 155->157
        celery_create_media_package(
            resources, self.user1, "test_package", metadata=True
        )  # 169->170

    def test_celery_regenerate_tokens(self):
        resources = Resource.objects.all()
        celery_regenerate_tokens(resources)
        resources_none_queryset = Resource.objects.none()
        celery_regenerate_tokens(resources_none_queryset)
