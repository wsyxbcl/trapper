# -*- coding: utf-8 -*-
import datetime
import json
import os
from unittest import mock

import factory
import pytz
from django.contrib.auth.models import AnonymousUser
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import override_settings
from django.urls import reverse
from django.utils.lorem_ipsum import words
from django.utils.timezone import now, localtime
from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.common.utils.test_tools import (
    ExtendedTestCase,
    ResourceTestMixin,
    CollectionTestMixin,
)
from trapper.apps.geomap.tests.factories.deployment import DeploymentFactory
from trapper.apps.geomap.tests.factories.location import LocationFactory
from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)
from trapper.apps.storage.forms import SimpleResourceForm
from trapper.apps.storage.models import Resource
from trapper.apps.storage.taxonomy import (
    ResourceStatus,
    CollectionStatus,
    ResourceType,
    ResourceMimeType,
)
from trapper.apps.storage.tests.factories.collection import CollectionFactory
from trapper.apps.storage.tests.factories.resource import ResourceFactory


class BaseResourceTestCase(ExtendedTestCase, ResourceTestMixin):
    def setUp(self):
        super(BaseResourceTestCase, self).setUp()
        self.summon_alice()
        self.summon_ziutek()

        self.resource_public = self.create_resource(
            owner=self.alice, status=ResourceStatus.PUBLIC
        )
        self.resource_private = self.create_resource(
            owner=self.alice, status=ResourceStatus.PRIVATE
        )

    def get_resource_create_url(self):
        """Default url for creating resource"""
        return reverse("storage:resource_create")

    def get_resource_details_url(self, resource):
        """Default url for details"""
        return reverse("storage:resource_detail", kwargs={"pk": resource.pk})

    def get_resource_update_url(self, resource):
        """Default url for update"""
        return reverse("storage:resource_update", kwargs={"pk": resource.pk})

    def get_resource_delete_url(self, resource):
        """Default url for delete"""
        return reverse("storage:resource_delete", kwargs={"pk": resource.pk})

    def get_collection_create_url(self):
        """Default url for creating collection"""
        return reverse("storage:collection_create")

    def get_collection_append_url(self):
        """Default url for appending collection"""
        return reverse("storage:collection_append")


class AnonymousResourceTestCase(BaseResourceTestCase):
    """
    Resource logic for anonymous users.
    """

    def test_resource_list(self):
        """
        Anonymous user can not access resources list via UI. It should be
        redirected to a login page.
        """
        url = reverse("storage:resource_list")
        self.assert_redirect(url)

    def test_resource_json(self):
        """
        Anonymous user can not see resources via API.
        """
        url = reverse("storage:api-resource-list")
        self.assert_forbidden(url=url)

    def test_resource_details(self):
        """
        Anonymous user has no permissions to access resource details
        view using UI.
        """
        url_private = self.get_resource_details_url(resource=self.resource_private)
        url_public = self.get_resource_details_url(resource=self.resource_public)
        self.assert_forbidden(url=url_private)
        self.assert_forbidden(url=url_public)

    def test_resource_create(self):
        """
        Anonymous user has to login before creating resource.
        """
        url = self.get_resource_create_url()
        self.assert_auth_required(url=url)

    def test_resource_delete(self):
        """
        Anonymous user should not get access here.
        """
        url = reverse("storage:resource_delete", kwargs={"pk": 1})
        self.assert_forbidden(url=url, method="get")
        self.assert_forbidden(url=url, method="post")

    def test_resource_delete_multiple(self):
        """
        Anonymous user should not get access here. Delete is handled by Ajax.
        """
        url = reverse("storage:resource_delete_multiple")
        self.assert_forbidden(url=url, method="post")

    def test_resource_update(self):
        """
        Anonymous user has no permissions to update.
        """
        url = reverse("storage:resource_update", kwargs={"pk": 1})
        self.assert_forbidden(url=url, method="post")

    def test_bulk_update(self):
        """
        Anonymous user has to login before using bulk update.
        """
        url = reverse("storage:resource_bulk_update")
        self.assert_forbidden(url=url, method="get")
        self.assert_forbidden(url=url, method="post")

    def test_define_prefix(self):
        """
        Anonymous user has to login before use define prefix.
        """
        url = reverse("storage:resource_define_prefix")
        self.assert_forbidden(url=url, method="post")


class ResourcePermissionsDetailsTestCase(BaseResourceTestCase):
    """
    Resource details permission logic for logged in user.
    """

    def setUp(self):
        """
        Login alice by default for all tests.
        """
        super(ResourcePermissionsDetailsTestCase, self).setUp()
        self.login_alice()

    def test_details_owner(self):
        """
        Resource owner can access details.
        """
        resource = self.create_resource(owner=self.alice)
        url = self.get_resource_details_url(resource=resource)
        self.assert_access_granted(url)

    def test_details_manager(self):
        """
        Resource manager can access details.
        """
        resource = self.create_resource(owner=self.ziutek, managers=[self.alice])
        url = self.get_resource_details_url(resource=resource)
        self.assert_access_granted(url)

    def test_details_role_access(self):
        """TODO: access from collection"""

    def test_details_no_roles(self):
        """
        Other users can not access a resource.
        """
        resource = self.create_resource(
            owner=self.ziutek,
        )
        url = self.get_resource_details_url(resource=resource)
        self.assert_forbidden(url)


class ResourcePermissionsUpdateTestCase(BaseResourceTestCase):
    """
    Update resource permission logic for logged in user.
    """

    def setUp(self):
        """
        Login alice by default for all tests.
        """
        super(ResourcePermissionsUpdateTestCase, self).setUp()
        self.login_alice()

    def test_update_owner(self):
        """
        Resource owner can update resource.
        """
        resource = self.create_resource(owner=self.alice)
        url = self.get_resource_update_url(resource=resource)
        self.assert_access_granted(url, method="post", data={"name": "test"})

    def test_update_manager(self):
        """
        Resource manager can update resource.
        """
        resource = self.create_resource(owner=self.ziutek, managers=[self.alice])
        url = self.get_resource_update_url(resource=resource)
        self.assert_access_granted(url, method="post", data={"name": "test"})

    def test_update_no_roles(self):
        """
        Other users can not update a resource.
        """
        resource = self.create_resource(owner=self.ziutek)
        url = self.get_resource_update_url(resource=resource)
        self.assert_forbidden(url, method="post", data={"name": "test"})


class ResourcePermissionsDeleteTestCase(BaseResourceTestCase):
    """
    Delete resource permission logic for logged in user. Note that delete
    multiple uses the same permission logic.
    """

    def setUp(self):
        """
        Login alice by default for all tests and prepare redirect url
        which is default for all tests.
        """
        super(ResourcePermissionsDeleteTestCase, self).setUp()
        self.login_alice()
        self.redirection = reverse("storage:resource_list")

    def test_delete_owner(self):
        """
        Resource owner can delete resource.
        """
        resource = self.create_resource(owner=self.alice)
        url = self.get_resource_delete_url(resource=resource)
        self.assert_redirect(url, self.redirection)

    def test_delete_manager(self):
        """
        Resource manager can delete resource.
        """
        resource = self.create_resource(owner=self.ziutek, managers=[self.alice])
        url = self.get_resource_delete_url(resource=resource)
        self.assert_redirect(url, self.redirection)

    def test_delete_no_roles(self):
        """
        Other users can not delete a resource.
        """
        resource = self.create_resource(owner=self.ziutek)
        url = self.get_resource_delete_url(resource=resource)
        self.assert_forbidden(url)


class ResourcePermissionsBulkUpdateTestCase(BaseResourceTestCase):
    """
    Bulk update permission logic for logged in user.
    """

    def setUp(self):
        """
        Login alice by default for all tests, and prepare redirect url
        which is default for all tests.
        """
        super(ResourcePermissionsBulkUpdateTestCase, self).setUp()
        self.login_alice()
        self.redirection = reverse("storage:resource_list")

    def _call_helper(self, resource):
        """
        All tests call the same logic and logged in user will get always
        response status 200. Difference is in response data which is json.
        """
        url = reverse("storage:resource_bulk_update")
        data = {
            "records_pks": [resource.pk],
            "status": ResourceStatus.PUBLIC,
            # TODO: fix bulk update form validation as now it is partly based
            # on a minimum number of fields in the posted data; thats why we
            # need here this crsf "mockup" field
            "csrfmockup": "123sth",
        }
        response = self.assert_access_granted(url, method="post", data=data)
        status = self.assert_json_context_variable(response, "success")
        return status

    def test_bulk_update_owner(self):
        """
        Owner can perform bulk operation on resources.
        """
        resource = self.create_resource(owner=self.alice)
        status = self._call_helper(resource=resource)
        self.assertTrue(status)

    def test_bulk_update_manager(self):
        """
        Manager can perform bulk operation on resources.
        """
        resource = self.create_resource(owner=self.ziutek, managers=[self.alice])
        status = self._call_helper(resource=resource)
        self.assertTrue(status)

    def test_bulk_update_no_roles(self):
        """
        Other users can not bulk update a resource.
        """
        resource = self.create_resource(owner=self.ziutek)
        status = self._call_helper(resource=resource)
        self.assertFalse(status)


class ResourcePermissionsDefinePrefixTestCase(BaseResourceTestCase):
    """
    Define prefix permission logic for logged in user.
    """

    def setUp(self):
        """
        Login alice by default for all tests, and prepare redirect url
        which is default for all tests and create default deployment.
        """
        super(ResourcePermissionsDefinePrefixTestCase, self).setUp()
        self.login_alice()
        self.redirection = reverse("storage:resource_list")
        self.deployment = self.create_deployment(owner=self.alice)

    def _call_helper(self, resource):
        """
        All tests call the same logic annd logged in user will get always
        response status 200. Difference is in response data which is json.
        """
        url = reverse("storage:resource_define_prefix")

        data = {"pks": [resource.pk], "custom_prefix": "test-prefix-"}
        response = self.assert_access_granted(url, method="post", data=data)
        status = self.assert_json_context_variable(response, "status")
        return status

    def test_define_prefix_owner(self):
        """
        Owner can change prefix on resources.
        """
        resource = self.create_resource(owner=self.alice, deployment=self.deployment)
        status = self._call_helper(resource=resource)
        self.assertTrue(status)

    def test_define_prefix_manager(self):
        """
        Manager can change prefix on resources.
        """
        resource = self.create_resource(
            owner=self.ziutek, managers=[self.alice], deployment=self.deployment
        )
        status = self._call_helper(resource=resource)
        self.assertTrue(status)

    def test_define_prefix_no_roles(self):
        """
        Other users can not bulk update a resource.
        """
        resource = self.create_resource(
            owner=self.ziutek,
            deployment=self.deployment,
        )
        status = self._call_helper(resource=resource)
        self.assertFalse(status)


class ResourcePermissionsCreateCollectionTestCase(BaseResourceTestCase):
    """
    Create collection permission logic for logged in user.
    """

    def setUp(self):
        """
        Login alice by default for all tests.
        """
        super(ResourcePermissionsCreateCollectionTestCase, self).setUp()
        self.login_alice()

    def test_create_collection_owner(self):
        """
        Owner can use a resource to create a collection.
        """
        resource = self.create_resource(owner=self.alice)
        url = self.get_collection_create_url()
        data = {
            "name": "test-collection",
            "status": CollectionStatus.PUBLIC,
            "resources_pks": resource.pk,
        }
        response = self.assert_access_granted(url, method="post", data=data)
        self.assertTrue(self.assert_json_context_variable(response, "success"))

    def test_create_collection_manager(self):
        """
        Manager can use a resource to create a collection.
        """
        resource = self.create_resource(owner=self.ziutek, managers=[self.alice])
        url = self.get_collection_create_url()
        data = {
            "name": "test-collection",
            "status": CollectionStatus.PUBLIC,
            "resources_pks": resource.pk,
        }
        response = self.assert_access_granted(url, method="post", data=data)
        self.assertTrue(self.assert_json_context_variable(response, "success"))

    def test_create_collection_role_access(self):
        """TODO: access from collection"""

    def test_create_collection_no_roles(self):
        """
        Other users can not use a resource to create a collection.
        """
        resource = self.create_resource(owner=self.ziutek)
        url = self.get_collection_create_url()
        data = {
            "name": "test-collection",
            "status": CollectionStatus.PUBLIC,
            "resources_pks": resource.pk,
        }
        response = self.assert_access_granted(url, method="post", data=data)
        self.assertFalse(self.assert_json_context_variable(response, "success"))


class ResourcePermissionsAppendCollectionTestCase(
    BaseResourceTestCase, CollectionTestMixin
):
    """
    Append to collection permission logic for logged in user.
    """

    def setUp(self):
        """
        Login alice by default for all tests, and prepare default
        collection and redirection url.
        """
        super(ResourcePermissionsAppendCollectionTestCase, self).setUp()
        self.login_alice()
        self.collection = self.create_collection(owner=self.alice)

    def test_append_collection_owner(self):
        """
        Owner can append resource to collection.
        """
        resource = self.create_resource(owner=self.alice)
        url = self.get_collection_append_url()
        data = {"collection": self.collection.pk, "resources_pks": resource.pk}
        response = self.assert_access_granted(url, method="post", data=data)
        self.assertTrue(self.assert_json_context_variable(response, "success"))

    def test_append_collection_manager(self):
        """
        Manager can append resource to collection.
        """
        resource = self.create_resource(owner=self.ziutek, managers=[self.alice])
        url = self.get_collection_append_url()
        data = {"collection": self.collection.pk, "resources_pks": resource.pk}
        response = self.assert_access_granted(url, method="post", data=data)
        self.assertTrue(self.assert_json_context_variable(response, "success"))

    def test_create_collection_role_access(self):
        """TODO: access from collection"""

    def test_create_collection_no_roles(self):
        """
        Other users can not append resource to collection.
        """
        resource = self.create_resource(owner=self.ziutek)
        url = self.get_collection_append_url()
        data = {"collection": self.collection.pk, "resources_pks": resource.pk}
        response = self.assert_access_granted(url, method="post", data=data)
        self.assertFalse(self.assert_json_context_variable(response, "success"))


class ResourcePermissionsListTestCase(BaseResourceTestCase):
    """Resource listing permission logic for logged in user"""

    def setUp(self):
        super(ResourcePermissionsListTestCase, self).setUp()
        self.login_alice()

    def test_resource_list(self):
        """Logged in user can access resource lisst"""
        url = reverse("storage:resource_list")
        self.assert_access_granted(url)

    def test_resource_json(self):
        """Logged in user can see

        * PUBLIC resources
        * PRIVATE resources

        User cannot see other's people PRIVATE resources
        """
        url = reverse("storage:api-resource-list")
        self.resource_private_ziutek = self.create_resource(
            owner=self.ziutek, status=ResourceStatus.PRIVATE
        )

        response = self.client.get(url)
        content = json.loads(response.content)["results"]
        resource_pk_list = [item["pk"] for item in content]

        self.assertIn(self.resource_public.pk, resource_pk_list, "Public not shown")
        self.assertIn(
            self.resource_private.pk, resource_pk_list, "Private not shown (own)"
        )
        self.assertNotIn(
            self.resource_private_ziutek.pk,
            resource_pk_list,
            "Private not shown (ziutek)",
        )


class ResourceTestCase(BaseResourceTestCase):
    """
    Tests related to modifying resources by performing various actions.
    """

    @override_settings(USE_TZ=False)
    def test_resource_create_upload(self):
        """"""
        deployment = self.create_deployment(owner=self.alice)
        image_file = os.path.join(self.SAMPLE_MEDIA_PATH, "image_1.jpg")
        url = self.get_resource_create_url()
        self.login_alice()
        name = words(count=5, common=False)
        with open(image_file, "rb") as file_handler:
            params = {
                "name": name,
                "date_recorded": "2015-08-08 06:06:06",
                "file": file_handler,
                "status": ResourceStatus.PRIVATE,
                "deployment": deployment.pk,
            }
            response = self.client.post(url, params)
            self.assertEqual(response.status_code, 302, response.status_code)
            self.assertTrue(
                Resource.objects.filter(
                    name=name,
                    date_recorded=datetime.datetime(2015, 8, 8, 6, 6, 6),
                    status=ResourceStatus.PRIVATE,
                    deployment=deployment,
                ).exists()
            )

    def test_resource_create_file_size(self):
        deployment = self.create_deployment(owner=self.alice)
        image_file = os.path.join(self.SAMPLE_MEDIA_PATH, "image_1.jpg")
        url = self.get_resource_create_url()
        self.login_alice()
        name = words(count=5, common=False)
        with open(image_file, "rb") as file_handler:
            params = {
                "name": name,
                "date_recorded": "2015-08-08 06:06:06",
                "file": file_handler,
                "status": ResourceStatus.PRIVATE,
                "deployment": deployment.pk,
            }
            self.client.post(url, params)

        resource = Resource.objects.get(name=name)
        self.assertNotEqual(resource.file_size, 0)

    def test_resource_update(self):
        """
        Using update view logged in user that has enough permissions can
        change existing resource.
        """
        self.login_alice()
        deployment = self.create_deployment(owner=self.alice)
        resource = self.create_resource(owner=self.alice, status=ResourceStatus.PRIVATE)
        url = self.get_resource_update_url(resource=resource)
        date_recorded = now()
        # Time is sent from non-utc timezone
        attributes = {
            "name": "test_name",
            "status": ResourceStatus.PUBLIC,
            "deployment": deployment.pk,
            "date_recorded": localtime(value=date_recorded).strftime(
                "%Y-%m-%d %H:%M:%S"
            ),
        }
        self.client.post(url, attributes)
        new_resource = Resource.objects.get(pk=resource.pk)
        self.assertEqual(new_resource.name, attributes["name"])
        self.assertEqual(new_resource.status, attributes["status"])
        self.assertEqual(new_resource.deployment, deployment)
        # We use format to strip miliseconds that aren't sent to backend
        self.assertEqual(
            new_resource.date_recorded.strftime("%d.%m.%Y %H:%M:%S"),
            date_recorded.strftime("%d.%m.%Y %H:%M:%S"),
        )

    def test_resource_delete(self):
        """
        Using delete view logged in user that has enough permissions can
        delete existing resource using GET.
        """
        self.login_alice()
        resource = self.create_resource(owner=self.alice, status=ResourceStatus.PRIVATE)

        url = self.get_resource_delete_url(resource=resource)

        self.client.get(url)
        self.assertFalse(Resource.objects.filter(pk=resource.pk).exists())

    def test_resource_delete_multiple(self):
        """
        Using delete view logged in user that has enough permissions can
        delete multiple resources using POST.
        """
        self.login_alice()
        resource1 = self.create_resource(
            owner=self.alice, status=ResourceStatus.PRIVATE
        )
        resource2 = self.create_resource(
            owner=self.alice, status=ResourceStatus.PRIVATE
        )
        url = reverse("storage:resource_delete_multiple")
        pks_list = [resource1.pk, resource2.pk]
        response = self.client.post(url, data={"pks": ",".join(map(str, pks_list))})
        status = self.assert_json_context_variable(response, "status")
        self.assertTrue(status)
        self.assertFalse(Resource.objects.filter(pk__in=pks_list).exists())

    def test_resource_bulk_update(self):
        """
        Using bulk update view logged in user that has enough permissions can
        modify various atributes of multiple resources at once.
        """
        self.login_alice()
        deployment = self.create_deployment(owner=self.alice)
        resources = []
        for _counter in range(3):
            r = self.create_resource(owner=self.alice)
            r.tags.add("ccc")
            resources.append(r)
        url = reverse("storage:resource_bulk_update")
        data = {
            "records_pks": ",".join(
                [str(resource.pk) for resource in resources] + [str(self.TEST_PK)]
            ),
            "tags2add": "aaaa,bbb",
            "tags2remove": "ccc",
            "deployment": deployment.pk,
            "status": ResourceStatus.PUBLIC,
            "managers": [self.ziutek.pk],
            "time_offset": 3600,
        }
        response = self.client.post(url, data=data)
        status = self.assert_json_context_variable(response, "success")
        self.assertTrue(status)
        for resource in resources:
            new_resource = Resource.objects.get(pk=resource.pk)
            self.assertEqual(new_resource.deployment.pk, data["deployment"])
            self.assertEqual(new_resource.status, data["status"])
            self.assertEqual(
                list(new_resource.tags.values_list("name", flat=True)),
                data["tags2add"].split(","),
            )
            self.assertEqual(
                list(new_resource.managers.values_list("pk", flat=True)),
                data["managers"],
            )
            self.assertEqual(
                new_resource.date_recorded,
                resource.date_recorded + datetime.timedelta(seconds=3600),
            )

    def test_resource_define_prefix(self):
        """
        Using define prefix view logged in user that has enough permissions can
        modify behaviour of resource name of multiple resources at one time.
        """
        self.login_alice()
        deployment = self.create_deployment(owner=self.alice)
        resources = [
            self.create_resource(owner=self.alice, deployment=deployment)
            for _counter in range(3)
        ]
        url = reverse("storage:resource_define_prefix")
        data = {
            "pks": ",".join(
                [str(resource.pk) for resource in resources] + [str(self.TEST_PK)]
            ),
            "custom_prefix": "test-prefix",
            "append": "true",
        }
        response = self.client.post(url, data=data)
        status = self.assert_json_context_variable(response, "status")
        self.assertTrue(status)
        for resource in resources:
            new_resource = Resource.objects.get(pk=resource.pk)
            self.assertEqual(new_resource.inherit_prefix, True)
            self.assertEqual(new_resource.custom_prefix, data["custom_prefix"])

    def test_resource_generate_thumbnail(self):
        """
        When resource is created and resource file type supports creating
        thumbnail, then thumbnail is created and assigned to `file_thumbnail`
        attribute.
        """
        image_file = os.path.join(self.SAMPLE_MEDIA_PATH, "image_1.jpg")
        with open(image_file, "rb") as handler:
            resource = self.create_resource(
                owner=self.alice, file_content=handler.read()
            )
            self.assertFalse(resource.file_thumbnail.name)
            self.assertEqual(resource.file_thumbnail_size, 0)
            resource.update_metadata(commit=True)
            resource.generate_thumbnails()
            self.assertTrue(resource.file_thumbnail.name)
            self.assertNotEqual(resource.file_thumbnail_size, 0)

    def test_resource_timezone_behaviour(self):
        timezone = pytz.timezone("Europe/Warsaw")
        location = self.create_location(
            owner=self.alice, timezone=timezone, ignore_DST=True
        )
        deployment = self.create_deployment(owner=self.alice, location=location)
        image_file = os.path.join(self.SAMPLE_MEDIA_PATH, "image_1.jpg")
        url = self.get_resource_create_url()
        self.login_alice()
        name = words(count=5, common=False)
        with open(image_file, "rb") as file_handler:
            params = {
                "name": name,
                "date_recorded": "2015-08-08 06:06:06",
                "file": file_handler,
                "status": ResourceStatus.PRIVATE,
                "deployment": deployment.pk,
            }
            self.client.post(url, params)

        new_resource = Resource.objects.get(name=name)

        # assert tz data is copied from location
        self.assertEqual(new_resource.timezone, timezone)
        self.assertTrue(new_resource.ignore_DST)

        # assert date is correctly calculated to UTC
        self.assertEqual(
            new_resource.date_recorded,
            datetime.datetime(2015, 8, 8, 5, 6, 6, tzinfo=pytz.UTC),
        )

        # assert dates are serialized with correct offsets
        self.assertEqual(
            new_resource.date_recorded_tz.strftime("%Y-%m-%d %H:%M %z"),
            "2015-08-08 06:06 +0100",
        )


def abc():
    return UserFactory()


class ResourceTest(ExtendedTestCase, ResourceTestMixin):
    """
    Resource faker tests
    """

    @classmethod
    def setUpTestData(cls):
        super(ResourceTest, cls).setUpTestData()

        cls.user1 = UserFactory()
        cls.user2 = UserFactory()
        cls.user3 = UserFactory()
        cls.user4 = UserFactory(is_staff=True)

        cls.research_project = ResearchProjectFactory(owner=cls.user1)

        cls.location1 = LocationFactory(
            owner=cls.user1, research_project=cls.research_project
        )

        cls.deployment1 = DeploymentFactory(
            owner=cls.user1,
            research_project=cls.research_project,
            location=cls.location1,
        )
        cls.deployment2 = DeploymentFactory(
            owner=cls.user1,
            research_project=cls.research_project,
            location=cls.location1,
            start_date=None,
            end_date=None,
        )

        cls.resource1 = ResourceFactory(owner=cls.user1)
        cls.resource2 = ResourceFactory(owner=cls.user1)
        cls.resource3 = ResourceFactory(
            owner=cls.user1, deployment=cls.deployment1, inherit_prefix=True
        )
        cls.resource4 = ResourceFactory(owner=cls.user1, deployment=cls.deployment2)

        # Set resource managers
        cls.resource1.managers.set((cls.user2,))

        cls.collection1 = CollectionFactory(owner=cls.user1, status="Private")

        # # Set collection resources
        cls.collection1.resources.set((cls.resource1,))

    def test_can_view(self):
        """Test can_view method"""
        self.assertTrue(self.resource1.is_public)

        # Status is PUBLIC and user is not authenticated
        self.assertFalse(self.resource1.can_view())

        # Status is PUBLIC and user is authenticated but not owner nor manager
        self.assertTrue(self.resource1.can_view(self.user3))

        # Status is PRIVATE and user is owner
        self.resource1.status = ResourceStatus.PRIVATE
        self.assertFalse(self.resource1.is_public)
        self.assertTrue(self.resource1.can_view(user=self.user1))

        # Status is PRIVATE and user is manager
        self.assertTrue(self.resource1.can_view(user=self.user2))

        # Status is PRIVATE and user is not owner nor manager
        self.assertFalse(self.resource1.can_view(user=self.user3))

        # Status is PRIVATE and user is not authenticated
        self.assertFalse(self.resource1.can_view(user=AnonymousUser()))

    def test_prefixed_name(self):
        self.assertEqual(self.resource1.prefixed_name, self.resource1.name)
        self.resource1.custom_prefix = "test"
        prefix_parts = [self.resource1.custom_prefix, self.resource1.name]
        self.assertEqual(self.resource1.prefixed_name, u"_".join(prefix_parts))

        self.resource3.custom_prefix = "test"
        prefix_parts = [
            self.resource3.custom_prefix,
            self.resource3.deployment.deployment_id,
            self.resource3.name,
        ]
        self.assertEqual(self.resource3.prefixed_name, u"_".join(prefix_parts))

        self.resource3.deployment.deployment_id = ""
        prefix_parts = [self.resource3.custom_prefix, self.resource3.name]
        self.assertEqual(self.resource3.prefixed_name, u"_".join(prefix_parts))

    def test_save(self):
        self.resource1.file = self.resource2.file

        self.resource1.save()
        self.assertEqual(self.resource1.file, self.resource2.file)

    def test_refresh_collection_bbox(self):
        self.collection1.resources.set((self.resource1,))
        self.resource1.refresh_collection_bbox()

    def test_can_remove_comment(self):
        # It should allow owner remove comment
        self.assertTrue(self.resource1.can_remove_comment(self.user1))

        # It shouldn't allow normal user remove comment
        self.assertFalse(self.resource1.can_remove_comment())

    def test_update_metadata(self):
        # It should update resource type to 'I'
        self.resource1.resource_type = "A"
        self.assertEqual(self.resource1.resource_type, "A")

        self.resource1.update_metadata()
        self.assertEqual(self.resource1.resource_type, ResourceType.TYPE_IMAGE)

    def test_update_resource_type(self):
        # It should update mime_type if not set
        self.resource1.mime_type = None
        self.assertEqual(self.resource1.mime_type, None)

        self.resource1.update_resource_type()
        self.assertEqual(self.resource1.mime_type, ResourceMimeType.IMAGE_JPEG)

        # It should update resource_type to 'A'
        self.resource1.mime_type = ResourceMimeType.AUDIO_MP3
        self.assertEqual(self.resource1.mime_type, ResourceMimeType.AUDIO_MP3)
        self.assertEqual(self.resource1.resource_type, ResourceType.TYPE_IMAGE)

        self.resource1.update_resource_type()
        self.assertEqual(self.resource1.resource_type, ResourceType.TYPE_AUDIO)

        # It should update resource_type to 'V'
        self.resource1.mime_type = ResourceMimeType.VIDEO_MP4
        self.assertEqual(self.resource1.mime_type, ResourceMimeType.VIDEO_MP4)
        self.assertEqual(self.resource1.resource_type, ResourceType.TYPE_AUDIO)

        self.resource1.update_resource_type()
        self.assertEqual(self.resource1.resource_type, ResourceType.TYPE_VIDEO)

        self.resource1.update_resource_type(commit=True)
        self.assertEqual(self.resource1.resource_type, ResourceType.TYPE_IMAGE)

        self.resource1.mime_type = "test/value"
        self.resource1.update_resource_type()  # 324->326

    def test_update_mimetype(self):
        # It should update mimetype with commit true parameter
        self.resource1.update_mimetype(commit=True)

    def test_update_filesize(self):
        self.resource2.file_size = 0
        self.assertEqual(self.resource2.file_size, 0)
        self.resource2.update_file_size()
        self.assertNotEqual(self.resource2.file_size, 0)

    def test_get_icon_class(self):
        self.assertEqual(self.resource1.get_resource_type_display().lower(), "image")
        self.assertEqual(self.resource1.get_icon_class(), "add_icon_image")

    def test_get_url(self):
        self.resource1.resource_type = ResourceType.TYPE_IMAGE
        url = reverse(
            "storage:resource_sendfile_media",
            kwargs={"pk": self.resource1.pk, "field": "pfile"},
        )
        self.assertEqual(self.resource1.get_url(), url)

        self.resource1.resource_type = ResourceType.TYPE_VIDEO
        url = reverse(
            "storage:resource_sendfile_media",
            kwargs={"pk": self.resource1.pk, "field": "file"},
        )
        self.assertEqual(self.resource1.get_url(), url)

    def test_get_url_original(self):
        url = reverse(
            "storage:resource_sendfile_media",
            kwargs={"pk": self.resource1.pk, "field": "file"},
        )
        self.assertEqual(self.resource1.get_url_original(), url)

    def test_get_thumbnail_url(self):
        base_url = "/static/trapper_storage/img/{name}"
        url = reverse(
            "storage:resource_sendfile_media",
            kwargs={"pk": self.resource1.pk, "field": "tfile"},
        )

        self.resource1.resource_type = ResourceType.TYPE_IMAGE
        self.assertEqual(self.resource1.get_thumbnail_url(), url)

        self.resource1.resource_type = ResourceType.TYPE_VIDEO
        self.assertEqual(self.resource1.get_thumbnail_url(), url)

        self.resource1.file_thumbnail = None

        self.resource1.resource_type = ResourceType.TYPE_IMAGE
        thumbnail_image = base_url.format(name="no_thumb_image_100x100.jpg")
        self.assertEqual(self.resource1.get_thumbnail_url(), thumbnail_image)

        self.resource1.resource_type = ResourceType.TYPE_AUDIO
        thumbnail_audio = base_url.format(name="no_thumb_audio_100x100.jpg")
        self.assertEqual(self.resource1.get_thumbnail_url(), thumbnail_audio)

        self.resource1.resource_type = ResourceType.TYPE_VIDEO
        thumbnail_video = base_url.format(name="no_thumb_video_100x100.jpg")
        self.assertEqual(self.resource1.get_thumbnail_url(), thumbnail_video)

        self.resource1.resource_type = None
        thumbnail_none = base_url.format(name="no_thumb_100x100.jpg")
        self.assertEqual(self.resource1.get_thumbnail_url(), thumbnail_none)

    def test_get_extra_url(self):
        extra_url = reverse(
            "storage:resource_sendfile_media",
            kwargs={"pk": self.resource1.pk, "field": "efile"},
        )
        self.assertEqual(self.resource1.get_extra_url(), extra_url)

    def test_check_date_recorded(self):
        self.resource1.deployment = None
        self.assertTrue(self.resource1.check_date_recorded)
        self.assertTrue(self.resource3.check_date_recorded)
        self.resource4.deployment.start_date = None
        self.deployment2.start_date = None
        self.assertTrue(self.resource4.check_date_recorded)

    @mock.patch("trapper.apps.geomap.models.get_current_user", side_effect=abc)
    def test_forms(self, get_current_user_function):
        form_data = {
            "name": "testName",
            "description": "testDescription",
            "date_recorded": datetime.datetime.now(),
            "file": factory.django.FileField(filename="mp4.mp4"),
            "status": ResourceStatus.PUBLIC,
        }
        form = SimpleResourceForm(data=form_data)
        form.fields["deployment"].required = False
        self.assertTrue(form.is_valid())

    def test_resource_index_route(self):
        self.client.force_login(self.user1)
        response = self.client.get(reverse("storage:resource_index"))
        self.assertEqual(response.status_code, 302)

    def test_resource_list_route(self):
        self.client.force_login(self.user1)
        response = self.client.get(reverse("storage:resource_list"))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Resources")

    def test_resource_detail_route(self):
        self.client.force_login(self.user1)
        response = self.client.get(
            reverse("storage:resource_detail", kwargs={"pk": self.resource1.pk})
        )
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Resources")

    def test_resource_delete_route(self):
        self.client.force_login(self.user1)
        resource_1 = ResourceFactory(owner=self.user1)
        self.assertTrue(Resource.objects.filter(id=resource_1.pk).exists())
        response = self.client.get(
            reverse("storage:resource_delete", kwargs={"pk": resource_1.pk})
        )
        self.assertEqual(response.status_code, 302)
        self.assertFalse(Resource.objects.filter(id=resource_1.pk).exists())

    def test_resource_define_prefix_route(self):
        self.client.force_login(self.user1)
        response = self.client.post(
            reverse("storage:resource_define_prefix"),
            {
                "pks": "pks_data",
                "custom_prefix": "custom_prefix_data",
                "inherit_prefix": "inherit_prefix_data",
            },
        )
        self.assertEqual(response.status_code, 200)

        response = self.client.post(
            reverse("storage:resource_define_prefix"),
            {
                "custom_prefix": "custom_prefix_data",
                "inherit_prefix": "inherit_prefix_data",
            },
        )
        self.assertEqual(response.status_code, 200)

    def test_resource_create_route(self):
        self.client.force_login(self.user1)
        new_resource = {
            "name": factory.Faker("word"),
            "file": SimpleUploadedFile("mp4.mp4", b""),
            "extra_file": factory.django.FileField(filename="mp4.mp4"),
            "owner": self.user1,
            "resource_type": ResourceType.TYPE_VIDEO,
            "file_thumbnail": factory.django.FileField(filename="thumbnail.jpg"),
            "date_recorded": datetime.datetime.now(),
            "status": ResourceStatus.PUBLIC,
            "description": factory.Faker("sentences"),
        }
        response = self.client.post(reverse("storage:resource_create"), new_resource)
        self.assertEqual(response.status_code, 200)

    def test_resource_update_route(self):
        self.client.force_login(self.user1)
        update_resource = {
            "description": "description test",
        }
        response = self.client.post(
            reverse("storage:resource_update", kwargs={"pk": self.resource2.pk}),
            update_resource,
        )
        self.assertEqual(response.status_code, 200)

    def test_resource_data_package_route(self):
        self.client.force_login(self.user1)
        response = self.client.get(reverse("storage:resource_data_package"))
        self.assertEqual(response.status_code, 200)

        self.client.logout()
        response = self.client.get(reverse("storage:resource_data_package"))
        self.assertEqual(response.status_code, 403)

    def test_resource_get_accessible(self):
        Resource.objects.get_accessible(user=self.user1)
        Resource.objects.get_accessible(
            user=self.user1, base_queryset=Resource.objects.all()
        )
        Resource.objects.get_accessible(
            user=self.user1, base_queryset=Resource.objects.filter(pk=self.resource1.pk)
        )
        Resource.objects.get_accessible(user=AnonymousUser())  # 62->63
        Resource.objects.get_accessible(user=self.user4)  # 64->65

    def test_resource_str(self):
        print(self.resource1)
