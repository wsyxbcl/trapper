# -*- coding: utf-8 -*-
"""
Views used to handle logic related to resource management in storage
application
"""
from braces.views import UserPassesTestMixin, JSONResponseMixin

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.postgres.search import SearchVector
from django.db.models import F, Q
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.template.defaultfilters import filesizeformat
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.text import format_lazy
from django.utils.translation import gettext_lazy as _
from django.views import generic

from trapper.apps.accounts.models import UserTask
from trapper.apps.common.tools import parse_pks
from trapper.apps.common.views import BaseDeleteView, BaseUpdateView, BaseBulkUpdateView
from trapper.apps.common.views import LoginRequiredMixin
from trapper.apps.geomap.models import MapManagerUtils
from trapper.apps.media_classification.models import ClassificationProject
from trapper.apps.research.models import ResearchProject
from trapper.apps.sendfile.views import BaseServeFileView
from trapper.apps.storage.forms import (
    ResourceForm,
    SimpleResourceForm,
    BulkUpdateResourceForm,
    ResourceDataPackageForm,
    ResourceRegenerateTokensForm,
)
from trapper.apps.storage.models import Resource, Collection
from trapper.apps.storage.tasks import (
    celery_create_media_package,
    celery_regenerate_tokens,
)

User = get_user_model()


class ResourceGridContextMixin:
    """Mixin used with views that use any collection listing
    for changing list behaviour.
    This mixin can be used to:

    * change resource url (i.e. add filtering)
    * append collection url
    * research projects related to collection
    * collections used in filters
    * maps used in filters
    * tags used in filters
    """

    def get_resource_url(self, **kwargs):
        """Return standard DRF API url for resources"""
        return reverse("storage:api-resource-list")

    def get_resource_context(self, **kwargs):
        """Build resource context"""
        context = {
            "data_url": self.get_resource_url(**kwargs),
            "collections": Collection.objects.get_accessible().values_list(
                "pk", "name"
            ),
            "tags": Resource.tags.values_list("pk", "name"),
            "maps": MapManagerUtils.get_accessible(user=self.request.user),
            "model_name": "resources",
            "update_redirect": False,
        }
        return context


class ResourceListView(
    LoginRequiredMixin, generic.TemplateView, ResourceGridContextMixin
):
    """View used for rendering template with resource grid.
    Context of view is updated with :class:`ResourceGridContextMixin`
    """

    model = Resource
    template_name = "storage/resources/resource_list.html"

    def get_context_data(self, **kwargs):
        """All we need to render base grid is:
        * Model name as title
        * Filter form instance

        This view is not serving any data. Data is read using DRF API
        """
        context = {
            "resource_context": self.get_resource_context(),
        }
        return context


view_resource_list = ResourceListView.as_view()


class ResourceDetailView(LoginRequiredMixin, UserPassesTestMixin, generic.DetailView):
    """View used for rendering details of specified resource.

    Before details are rendered, permissions are checked and if currently
    logged in user has not enough permissions to view details,
    proper message is displayed.
    """

    template_name = "storage/resources/resource_detail.html"
    model = Resource
    raise_exception = True

    def test_func(self, user):
        """
        Resource details can be seen only if user has enough permissions
        """
        return self.get_object().can_view(user)

    def get_context_data(self, **kwargs):
        """Update context used to render template with data related to
        resources
        """
        user = self.request.user
        context = super(ResourceDetailView, self).get_context_data(**kwargs)
        resource = context["object"]
        context["collections"] = (
            resource.collection_set.get_accessible(user=user)
            .select_related("owner")
            .prefetch_related("managers")
        )
        context["research_projects"] = (
            ResearchProject.objects.get_accessible(user=user)
            .filter(collections__resources=resource)
            .distinct()
        )
        context["classification_projects"] = (
            ClassificationProject.objects.get_accessible(user=user)
            .filter(
                classification_project_collections__collection__collection__resources=resource
            )
            .distinct()
        )
        context["share_resource"] = resource.get_url_original(token=True)
        return context


view_resource_detail = ResourceDetailView.as_view()


class ResourceCreateView(LoginRequiredMixin, generic.CreateView):
    """Resource create view.
    It handles the creation of the :class:`apps.storage.models.Resource` objects.
    """

    template_name = "storage/resources/resource_change.html"
    model = Resource
    form_class = ResourceForm

    def form_valid(self, form):
        """If form is valid then set `owner` as currently logged in user,
        and add message that resource has been created"""
        user = self.request.user
        form.instance.owner = user
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(
                f"A new resource <strong>{form.instance.name}</strong> has been successfully created!"
            ),
        )
        self.object = form.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        """If form is not valid, form is re-rendered with error details,
        and message about unsuccessfull operation is shown"""
        messages.add_message(
            self.request, messages.ERROR, _("Error creating new resource")
        )
        return super(ResourceCreateView, self).form_invalid(form)


view_resource_create = ResourceCreateView.as_view()


class ResourceDeleteView(BaseDeleteView):
    """View responsible for handling deletion of single or multiple
    resources.

    Only resources that user has enough permissions for can be deleted
    """

    model = Resource
    redirect_url = "storage:resource_list"


view_resource_delete = ResourceDeleteView.as_view()


class ResourceDefinePrefixView(LoginRequiredMixin, generic.View, JSONResponseMixin):
    """View used to define resource name prefix for multiple resources
    in single request using AJAX

    Only resources owned by currently logged in user can be updated
    """

    raise_exception = True

    def post(self, request, *args, **kwargs):
        user = request.user
        data = request.POST.get("pks", None)
        custom_prefix = request.POST.get("custom_prefix", None)
        inherit_prefix = request.POST.get("append", False) == "true"
        status = False
        msg = ""
        if data:
            values = parse_pks(pks=data)
            candidates = Resource.objects.filter(pk__in=values).filter(
                (Q(owner=user) | Q(managers=user))
            )
            if candidates:
                if custom_prefix:
                    candidates.update(custom_prefix=custom_prefix)
                if inherit_prefix:
                    candidates = candidates.filter(deployment__location__isnull=False)
                    if candidates:
                        candidates.update(
                            inherit_prefix=inherit_prefix,
                        )
                status = True
            else:
                msg = _("No resources to update")
        else:
            msg = _("Invalid request")
        context = {"status": status, "msg": msg}
        return self.render_json_response(context)


view_resource_define_prefix = ResourceDefinePrefixView.as_view()


class ResourceUpdateView(BaseUpdateView):
    """Resource update view."""

    template_name = "storage/resources/resource_change.html"
    template_name_modal = "storage/resources/resource_form.html"
    model = Resource
    raise_exception = True
    form_class = ResourceForm
    form_class_modal = SimpleResourceForm


view_resource_update = ResourceUpdateView.as_view()


class ResourceBulkUpdateView(BaseBulkUpdateView):
    """Resource bulk update view."""

    template_name = "forms/simple_crispy_form.html"
    form_class = BulkUpdateResourceForm
    raise_exception = True
    tags_field = "tags"

    def update_db_indexes(self, records):
        if self.cleaned_data.get("data") is not None:
            records.annotate(vector=SearchVector("data")).update(
                search_data_vector=F("vector")
            )


view_resource_bulk_update = ResourceBulkUpdateView.as_view()


class ResourceGenerateDataPackage(
    LoginRequiredMixin, generic.FormView, JSONResponseMixin
):
    """Generate data package with selected resources."""

    template_name = "forms/simple_crispy_form.html"
    form_class = ResourceDataPackageForm
    raise_exception = True
    MAX_SIZE = getattr(settings, "DATA_PACKAGE_MAX_SIZE", 10 * 1024 * 1024)

    def form_valid(self, form):
        """"""
        user = self.request.user
        resources = form.cleaned_data.get("resources", None)
        total_size = 0
        for k in resources:
            try:
                total_size = total_size + int(k.file.size)
            except Exception:
                continue
        if not resources:
            msg = _(
                "Nothing to process (most probably you have no permission "
                "to run this action on selected resources)."
            )
            context = {
                "success": False,
                "msg": msg,
            }
        elif total_size > self.MAX_SIZE:
            msg = format_lazy(
                _(
                    "We are sorry but the maximum size of a data package you can "
                    "request for is {max_size} at the moment. The size of your current "
                    "selection is {size}."
                ),
                max_size=filesizeformat(self.MAX_SIZE),
                size=filesizeformat(total_size),
            )
            context = {
                "success": False,
                "msg": msg,
            }
        else:
            params = {
                "resources": resources,
                "user": user,
                "package_name": form.cleaned_data.get("package_name", None),
                "metadata": form.cleaned_data.get("metadata", None),
            }
            if settings.CELERY_ENABLED:
                task = celery_create_media_package.delay(**params)
                user_task = UserTask(user=user, task_id=task.task_id)
                user_task.save()
                msg = _(
                    "You have successfully run the celery task. Your data package is "
                    "being generated now. "
                )
            else:
                msg = celery_create_media_package(**params)
            context = {
                "success": True,
                "msg": msg,
            }
        return self.render_json_response(context_dict=context)

    def form_invalid(self, form):
        context = {
            "success": False,
            "msg": _("Your form contains errors."),
            "form_html": render_to_string(self.template_name, {"form": form}),
        }
        return self.render_json_response(context_dict=context)


view_resource_data_package = ResourceGenerateDataPackage.as_view()


class ResourceRegenerateTokens(LoginRequiredMixin, generic.FormView, JSONResponseMixin):
    """Re-generate resource tokens."""

    template_name = "forms/simple_crispy_form.html"
    form_class = ResourceRegenerateTokensForm
    raise_exception = True

    def form_valid(self, form):
        user = self.request.user
        resources = form.cleaned_data.get("resources", None)
        params = {"resources": resources}
        if settings.CELERY_ENABLED:
            task = celery_regenerate_tokens.delay(**params)
            user_task = UserTask(user=user, task_id=task.task_id)
            user_task.save()
            msg = _(
                "You have successfully run the celery task. Resource tokens are "
                "being (re)generated now. "
            )
        else:
            msg = celery_regenerate_tokens(**params)
        context = {"success": True, "msg": msg}
        return self.render_json_response(context_dict=context)

    def form_invalid(self, form):
        context = {
            "success": False,
            "msg": _("Your form contains errors."),
            "form_html": render_to_string(self.template_name, {"form": form}),
        }
        return self.render_json_response(context_dict=context)


view_resource_regenerate_tokens = ResourceRegenerateTokens.as_view()


class ResourceSendfileMediaView(BaseServeFileView):
    authenticated_only = False

    field_map = {
        "pfile": "file_preview",
        "efile": "extra_file",
        "tfile": "file_thumbnail",
    }

    def access_granted(self, resource, resource_field):
        field = self.field_map.get(resource_field, "file")
        media_field = getattr(resource, field)
        if media_field:
            return self.serve_file(media_field, mimetype=resource.mime_type)
        else:
            raise Http404

    def access_revoked(self):
        raise Http404

    def get(self, request, *args, **kwargs):
        resource_pk = kwargs.get("pk", None)
        resource_field = kwargs.get("field", None)
        if not (resource_pk and resource_field):
            raise Http404
        user = self.request.user
        resource = get_object_or_404(Resource, pk=resource_pk)
        token_auth = False
        token = request.GET.get("rt")
        if token:
            token_auth = token == resource.token
        status = resource.can_view(user=user)
        if token_auth or user.is_superuser or status:
            response = self.access_granted(
                resource=resource, resource_field=resource_field
            )
        else:
            response = self.access_revoked()
        return response


view_resource_sendfile_media = ResourceSendfileMediaView.as_view()


class ResourceSendfileMediaWithFilenameView(ResourceSendfileMediaView):
    def get(self, request, resource_pk, resource_field, token, filename):
        if resource_field != "file" and resource_field not in self.field_map:
            raise Http404

        user = self.request.user
        resource = get_object_or_404(Resource, pk=resource_pk)
        token_auth = token == resource.token
        status = resource.can_view(user=user)
        if token_auth or user.is_superuser or status:
            response = self.access_granted(
                resource=resource, resource_field=resource_field
            )
        else:
            response = self.access_revoked()
        return response


view_resource_sendfile_media_with_file_name = (
    ResourceSendfileMediaWithFilenameView.as_view()
)
