# -*- coding: utf-8 -*-
"""
This module contains a logic responsible for handling the process of
uploading collections of resources.
"""
import os
import zipfile
from io import BytesIO
from collections import defaultdict

import pytz
import yaml
from django.contrib.postgres.search import SearchVector
from django.db.models import Value
from yaml.reader import ReaderError
from yaml.scanner import ScannerError

from django.apps import apps
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.files.uploadedfile import SimpleUploadedFile
from django.db import transaction
from django.utils import dateparse
from django.utils.timezone import now
from pykwalify.core import Core, SchemaError, CoreError

from trapper.apps.accounts.utils import get_pretty_username
from trapper.apps.geomap.models import Deployment
from trapper.apps.research.models import ResearchProject, ResearchProjectCollection
from trapper.apps.storage.taxonomy import ResourceMimeType


class CollectionProcessorException(Exception):
    """Default exception"""


class CollectionProcessor:
    """
    Class containing a logic necessary to process collection's definition
    (YAML) and data (ZIP archive) files.
    """

    SEPARATOR = "\n"

    def __init__(
        self,
        definition_file,
        archive_file=None,
        owner=None,
        read_definition_file=True,
        celery_enabled=False,
    ):
        """
        Initialize collection processor
        :param definition_file: yaml configuration for `archive_file`
        :param archive_file: full path to uploaded zip archive containing media files
        :param owner: instance of a user
        """
        self.resource_model = apps.get_model("storage", "Resource")
        if read_definition_file:
            self.definition_file = open(definition_file, "r").read()
        else:
            self.definition_file = definition_file
        self.definition_data = self.process_definition()
        self.definition = None
        self.archive_file = archive_file
        if archive_file:
            archive_file = open(archive_file, "rb")
            self.archive = zipfile.ZipFile(self.archive_file)
        else:
            self.archive = None
        self.owner = owner
        self.owner_display = get_pretty_username(user=owner)
        self.separator = None
        self.deployments = {}
        self.errors = {}
        self.celery_enabled = celery_enabled

    def process_definition(self):
        """Parse a definition file into a python dictionary"""
        try:
            loader = yaml.Cloader
        except AttributeError:
            loader = yaml.Loader
        try:
            data = yaml.load(self.definition_file, Loader=loader)
        except (ReaderError, ScannerError):
            data = None
        return data

    def get_dep_res_integrity_dict(self, project):
        qs = project.deployments.values_list("deployment_id", "resources__name")
        dep_res_dict = defaultdict(list)
        for k in qs:
            dep_res_dict[k[0]].append(k[1])
        return dep_res_dict

    def format_validation_errors(self, validator, separator=None):
        if separator is None:
            separator = self.SEPARATOR
        return separator.join(validator.validation_errors)

    @property
    def schema(self):
        """Collection processor uses yaml schema to check if a definition file
        is correct. The YAML schema is implemented with:
        `pykwalify <https://github.com/Grokzen/pykwalify>`_
        """
        return os.path.join(
            settings.PROJECT_ROOT, "trapper/apps/storage/schema/collection_schema.yaml"
        )

    def validate_definition(self, separator=None):
        """
        Check for possible errors within a definition file.
        """
        if separator is None:
            separator = self.SEPARATOR
        self.separator = separator
        try:
            validator = Core(
                source_data=self.definition_data, schema_files=[self.schema]
            )
        except (CoreError, UnicodeEncodeError):
            return "This is not a valid YAML definition file."
        try:
            self.definition = validator.validate(raise_exception=True)
        except SchemaError:
            return (
                "Error when opening the definition file. YAML syntax might "
                "be invalid.{separator}Details:{separator}{err}".format(
                    separator=separator,
                    err=self.format_validation_errors(
                        validator=validator, separator=separator
                    ),
                )
            )
        except Exception:
            return "This is not a valid YAML definition file."
        errors = []
        for collection_def in self.definition["collections"]:
            error = self.clean_project(collection_def.get("project_name"))
            if error:
                return error
            errors.extend(
                self.clean_deployments(
                    collection_def.get("deployments", []),
                    collection_def.get("name"),
                    collection_def.get("project_name"),
                    collection_def.get("timezone"),
                    collection_def.get("timezone_ignore_dst"),
                )
            )
            errors.extend(self.clean_managers(collection_def.get("managers", [])))
        return self.separator.join(errors)

    def clean_project(self, acronym):
        """Check if given research project exists in a database and if user that
        try to upload a collection has enough permissions to use it."""
        error = None
        try:
            project = ResearchProject.objects.get(acronym=acronym)
        except ResearchProject.DoesNotExist:
            error = f"Research project {acronym} does not exist."
        else:
            if not project.can_view(self.owner):
                error = f"You do not have enough permissions to use this project: {acronym}."
        return error

    def clean_deployments(
        self,
        deployments,
        collection_name,
        project_acronym,
        timezone,
        timezone_ignore_dst,
    ):
        """Check if given deployment exists in a database and if user that
        try to upload a collection has enough permissions to use it."""
        errors = []
        self.deployments[collection_name] = []
        for deployment_def in deployments:
            deployment_id = deployment_def.get("deployment_id")
            try:
                deployment = Deployment.objects.get(
                    research_project__acronym=project_acronym,
                    deployment_id=deployment_id,
                )
            except Deployment.DoesNotExist:
                errors.append(f"Deployment {deployment_id} does not exist.")
            else:
                if not deployment.can_update(self.owner):
                    errors.append(
                        "You do not have enough permissions to use this deployment: "
                        f"{deployment_id}."
                    )
                elif (
                    deployment.location.timezone.zone != timezone
                    or deployment.location.ignore_DST != timezone_ignore_dst
                ):
                    errors.append(
                        f"Provided timezone specification ({timezone} | Ignore DST: "
                        f"{timezone_ignore_dst}) "
                        f"does not match this deployment: {deployment_id} "
                        f"({deployment.location.timezone.zone} | Ignore DST: "
                        f"{deployment.location.ignore_DST})."
                    )
                else:
                    self.deployments[collection_name].append(deployment)
        return errors

    def clean_managers(self, managers):
        """Check if managers exist in a database."""
        user_model = get_user_model()
        errors = []
        for user_def in managers:
            username = user_def.get("username", None)
            try:
                user_model.objects.get(username=username)
            except user_model.DoesNotExist:
                errors.append("User {username} does not exist")
        return errors

    def read_from_zip(self, item):
        """Read an item from a zip archive. Item is a name of a file in
        an archive or a ZipInfo object."""
        try:
            bin_data = self.archive.read(item)
            temp_handle = BytesIO()
            temp_handle.write(bin_data)
            temp_handle.seek(0)
        except zipfile.BadZipfile:
            raise CollectionProcessorException(
                f"File {item.filename} from the archive could not be processed."
            )
        except zipfile.LargeZipFile:
            raise CollectionProcessorException(
                "Your file is too big and the ZIP64 functionality is not available."
            )
        return temp_handle

    def build_resource(self, resource_def, resources_dir, timestamp, deployment):
        """"""
        # first check db integrity i.e. if resource of given name and belonging to
        # given deployment exists in db
        resource_name = resource_def["name"]
        dep_res_names = self.dep_res_dict.get(deployment.deployment_id)
        if dep_res_names and resource_name in dep_res_names:
            error = (
                f"Resource {resource_name} from deployment {deployment.deployment_id} "
                "already exists in the database."
            )
            return (None, error)

        MEDIA_EXT = ResourceMimeType.MEDIA_EXTENSIONS
        resource_model = self.resource_model
        file_ext = os.path.splitext(resource_def["file"])[-1]
        if not file_ext.lower() in MEDIA_EXT:
            error = f"Not allowed mime type: *.{file_ext}"
            return (None, error)
        base_path = os.path.join(resources_dir, deployment.deployment_id)
        file_path = os.path.join(base_path, resource_def["file"])
        bin_data_file = self.read_from_zip(file_path)
        date_recorded = dateparse.parse_datetime(resource_def["date_recorded"])
        if not date_recorded:
            error = "Could not parse date recorded timestamp: {date}".format(
                date=resource_def["date_recorded"]
            )
            return (None, error)
        date_recorded = date_recorded.replace(tzinfo=pytz.UTC)
        resource = resource_model(
            name=resource_def["name"],
            owner=self.owner,
            date_recorded=date_recorded,
            date_uploaded=timestamp,
            deployment=deployment,
            search_name_vector=SearchVector(Value(resource_def["name"])),
        )
        suf_file = SimpleUploadedFile(
            resource_def.get("file"),
            bin_data_file.read(),
            content_type=file_ext,
        )
        resource.file.save(resource_def["file"], suf_file, save=False)
        resource.update_metadata()
        return (resource, None)

    @transaction.atomic
    def create(self):
        """When definition and data files are correct,
        :class:`apps.storage.models.Collection` instances and
        :class:`apps.storage.models.Resource` instances are created.

        In case of any errors the process is stopped and a proper message
        is displayed to a user.

        """
        from trapper.apps.storage.tasks import celery_update_thumbnails

        User = get_user_model()
        collection_model = apps.get_model("storage", "Collection")
        errors = self.validate_definition()
        if errors:
            raise CollectionProcessorException(f"Your config file is invalid: {errors}")
        collections = {}
        for collection_def in self.definition["collections"]:
            name = collection_def["name"]
            resources_dir = collection_def["resources_dir"]
            resources = {}
            project_name = collection_def.get("project_name", None)

            research_project = ResearchProject.objects.get(acronym=project_name)
            # generate projects deployments & resources dict for further db integrity validation
            self.dep_res_dict = self.get_dep_res_integrity_dict(research_project)

            managers = []
            manager_usernames = [
                item["username"] for item in collection_def.get("managers", [])
            ]
            if manager_usernames:
                managers = User.objects.filter(username__in=manager_usernames)
            collection, _created = collection_model.objects.get_or_create(
                name=name,
                owner=self.owner,
            )
            for item in managers:
                collection.managers.add(item)
            if research_project:
                ResearchProjectCollection.objects.get_or_create(
                    project=research_project, collection=collection
                )
            collections[resources_dir] = {
                "collection": collection,
                "resources": resources,
            }
            self.errors[collection.name] = {}
            resources_list = []
            timestamp = now()
            deployments_objects = self.deployments[name]
            deployments_objects.sort(key=lambda x: x.deployment_id)
            deployments_definitions = collection_def.get("deployments", [])
            deployments_definitions.sort(key=lambda x: x["deployment_id"])
            deployments = zip(deployments_objects, deployments_definitions)

            for _deployment in deployments:
                deployment = _deployment[0]
                self.errors[collection.name][deployment.deployment_id] = []
                for resource_def in _deployment[1]["resources"]:
                    try:
                        resource, error = self.build_resource(
                            resource_def, resources_dir, timestamp, deployment
                        )
                    except Exception as e:
                        resource = None
                        error = str(e)
                    if not error:
                        resources_list.append(resource)
                    else:
                        self.errors[collection.name][deployment.deployment_id].append(
                            (resource_def["file"], error)
                        )
                if not self.errors[collection.name][deployment.deployment_id]:
                    self.errors[collection.name].pop(deployment.deployment_id)

            self.resource_model.objects.bulk_create(
                resources_list, ignore_conflicts=True
            )
            resources = self.resource_model.objects.filter(date_uploaded=timestamp)
            resources_pks = [k.pk for k in resources]
            collection.resources.add(*resources_pks)
            if self.celery_enabled:
                celery_update_thumbnails.delay(resources)
            else:
                celery_update_thumbnails(resources)
            if not self.errors[collection.name]:
                self.errors.pop(collection.name)
