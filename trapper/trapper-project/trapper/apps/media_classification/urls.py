# -*- coding:utf-8 -*-

from django.conf.urls import include
from django.urls import reverse_lazy, path, re_path
from django.views.generic import RedirectView
from rest_framework.routers import DefaultRouter

import trapper.apps.media_classification.views.ai_classifications
from trapper.apps.media_classification.views import (
    api as classification_api_views,
    classifications as classifications_views,
    user_classifications as user_classifications_views,
    ai_classifications as ai_classifications_views,
    projects as projects_views,
    classificators as classificators_views,
    sequences as sequences_views,
)

app_name = "media_classification"

router = DefaultRouter(trailing_slash=False)

router.register(
    r"classificators",
    classification_api_views.ClassificatorViewSet,
    basename="api-classificator",
)

router.register(
    r"user-classifications",
    classification_api_views.UserClassificationViewSet,
    basename="api-user-classification",
)

router.register(
    r"classifications",
    classification_api_views.ClassificationViewSet,
    basename="api-classification",
)

router.register(
    r"ai-classifications",
    classification_api_views.AIClassificationViewSet,
    basename="api-ai-classification",
)

router.register(
    r"classifications_map",
    classification_api_views.ClassificationMapViewSet,
    basename="api-classification-map",
)

router.register(
    r"projects",
    classification_api_views.ClassificationProjectViewSet,
    basename="api-classification-project",
)

router.register(
    r"project/(?P<project_pk>\d+)/collections",
    classification_api_views.ClassificationProjectCollectionViewSet,
    basename="api-classification-project-collection",
)

router.register(
    r"sequences", classification_api_views.SequenceViewSet, basename="api-sequence"
)

router.register(
    r"collection/(?P<collection_pk>\d+)/resources/$",
    classification_api_views.ClassificationResourcesViewSet,
    basename="api-classification-resources",
)

urlpatterns = [
    re_path(r"^api/", include(router.urls)),
    re_path(
        r"^api/classifications/results/(?P<project_pk>\d+)/",
        classification_api_views.ClassificationResultsView.as_view(),
        name="api-classification-results",
    ),
    re_path(
        r"^api/ai-classifications/results/(?P<project_pk>\d+)/",
        classification_api_views.AIClassificationResultsView.as_view(),
        name="api-ai-classification-results",
    ),
    re_path(
        r"^api/classifications/results/agg/(?P<project_pk>\d+)/",
        classification_api_views.ClassificationResultsAggView.as_view(),
        name="api-classification-results-agg",
    ),
    re_path(
        r"^api/classifications/import/",
        classification_api_views.ClassificationImport.as_view(),
        name="api-classification-import",
    ),
    re_path(
        r"^api/media/(?P<project_pk>\d+)/",
        classification_api_views.ClassificationProjectMediaTableView.as_view(),
        name="api-media-export",
    ),
    re_path(
        r"^api/package/(?P<project_pk>\d+)/",
        classification_api_views.ResultsDataPackageAPIView.as_view(),
        name="api-package-export",
    ),
    path(
        "api/classify/ai/callback/<str:ai_provider_token>/",
        classification_api_views.classify_ai_callback,
        name="api-classify-ai-callback",
    ),
    path(
        "api/classify/ai/classification_extra/<str:ai_provider_token>/<str:classification_job_id>/<str:resource_name>/",
        classification_api_views.classify_ai_extra_resource,
        name="api-classify-ai-extra",
    ),
]

urlpatterns += [
    re_path(
        r"^$",
        RedirectView.as_view(url=reverse_lazy("media_classification:project_list")),
        name="project_index",
    ),
    # Project views
    re_path(r"project/list/$", projects_views.view_project_list, name="project_list"),
    re_path(
        r"project/detail/(?P<pk>\d+)/$",
        projects_views.view_project_detail,
        name="project_detail",
    ),
    re_path(
        r"project/create/$", projects_views.view_project_create, name="project_create"
    ),
    re_path(
        r"project/update/(?P<pk>\d+)/$",
        projects_views.view_project_update,
        name="project_update",
    ),
    re_path(
        r"project/delete/(?P<pk>\d+)/$",
        projects_views.view_project_delete,
        name="project_delete",
    ),
    re_path(
        r"^project/delete/$",
        projects_views.view_project_delete,
        name="project_delete_multiple",
    ),
    re_path(
        r"project/collection/add/$",
        projects_views.view_project_collection_add,
        name="project_collection_add",
    ),
    re_path(
        r"project/collection/delete/(?P<pk>\d+)/$",
        projects_views.view_project_collection_delete,
        name="project_collection_delete",
    ),
    re_path(
        r"^project/collection/delete/$",
        projects_views.view_project_collection_delete,
        name="project_collection_delete_multiple",
    ),
    re_path(
        r"^project/collection/blur/$",
        projects_views.view_project_collection_blur_humans,
        name="project_collection_blur_humans",
    ),
    # Classificator views
    re_path(
        r"classificator/list/$",
        classificators_views.view_classificator_list,
        name="classificator_list",
    ),
    re_path(
        r"classificator/detail/(?P<pk>\d+)/$",
        classificators_views.view_classificator_detail,
        name="classificator_detail",
    ),
    re_path(
        r"classificator/create/$",
        classificators_views.view_classificator_create,
        name="classificator_create",
    ),
    re_path(
        r"classificator/update/(?P<pk>\d+)/$",
        classificators_views.view_classificator_update,
        name="classificator_update",
    ),
    re_path(
        r"classificator/clone/(?P<pk>\d+)/$",
        classificators_views.view_classificator_clone,
        name="classificator_clone",
    ),
    re_path(
        r"^classificator/delete/(?P<pk>\d+)/$",
        classificators_views.view_classificator_delete,
        name="classificator_delete",
    ),
    re_path(
        r"^classificator/delete/$",
        classificators_views.view_classificator_delete,
        name="classificator_delete_multiple",
    ),
    # Sequence views
    re_path(
        r"sequence/create/$",
        sequences_views.view_sequence_change,
        name="sequence_create",
    ),
    re_path(
        r"sequence/update/$",
        sequences_views.view_sequence_change,
        name="sequence_update",
    ),
    re_path(
        r"sequence/change/$",
        sequences_views.view_sequence_change,
        name="sequence_change",
    ),
    re_path(
        r"sequence/delete/$",
        sequences_views.view_sequence_delete,
        name="sequence_delete",
    ),
    re_path(
        r"^sequence/build/$", sequences_views.view_sequence_build, name="sequence_build"
    ),
    # Classification views
    re_path(
        r"^classification/overview/$",
        classifications_views.view_classification_overview,
        name="classification_overview",
    ),
    re_path(
        r"^classification/list/(?P<pk>\d+)/$",
        classifications_views.view_classification_list,
        name="classification_list",
    ),
    re_path(
        r"classification/import/$",
        classifications_views.view_classification_import,
        name="classification_import",
    ),
    re_path(
        r"classification/import/(?P<pk>\d+)/$",
        classifications_views.view_classification_import,
        name="classification_import",
    ),
    re_path(
        r"classification/export/(?P<pk>\d+)/$",
        classifications_views.view_classification_export,
        name="classification_export",
    ),
    re_path(
        r"classification/publish/$",
        classifications_views.view_classification_publish,
        name="classification_publish",
    ),
    re_path(
        r"classification/feedback/(?P<pk>\d+)/$",
        classifications_views.view_classification_feedback,
        name="classification_feedback",
    ),
    re_path(
        r"classification/export-agg/(?P<pk>\d+)/$",
        classifications_views.view_classification_export_agg,
        name="classification_export_agg",
    ),
    re_path(
        r"classification/importattrs/(?P<pk>\d+)/$",
        classifications_views.view_classification_import_attrs,
        name="classification_importattrs",
    ),
    re_path(
        r"^classification/delete/(?P<pk>\d+)/$",
        classifications_views.view_classification_delete,
        name="classification_delete",
    ),
    re_path(
        r"^classification/delete/$",
        classifications_views.view_classification_delete,
        name="classification_delete_multiple",
    ),
    re_path(
        r"^classification/bulk-update/$",
        classifications_views.view_classification_bulk_update,
        name="classification_bulk_update",
    ),
    re_path(
        r"classify/(?P<project_pk>\d+)/(?P<collection_pk>\d+)/$",
        classifications_views.view_classify_collection,
        name="classify_collection",
    ),
    re_path(
        r"classify/(?P<pk>\d+)/$",
        classifications_views.view_classify_resource,
        name="classify",
    ),
    re_path(
        r"classify/(?P<pk>\d+)/user/(?P<user_pk>\d+)/$",
        classifications_views.view_classify_resource,
        name="classify_user",
    ),
    re_path(
        r"classify/form/(?P<project_pk>\d+)/$",
        classifications_views.view_classify_form,
        name="classify_form",
    ),
    re_path(
        r"classify/form/(?P<project_pk>\d+)/(?P<classification_pk>\d+)/$",
        classifications_views.view_classify_form,
        name="classify_form",
    ),
    re_path(
        r"classify/form/(?P<project_pk>\d+)/(?P<classification_pk>\d+)/(?P<user_pk>\d+)/$",
        classifications_views.view_classify_form,
        name="classify_form",
    ),
    path(
        "classify/<int:pk>/ai/<int:ai_provider_id>/",
        classifications_views.view_classify_resource,
        name="view_ai_classification",
    ),
    path(
        "classify/form/<int:project_pk>/",
        classifications_views.view_classify_form,
        name="classify_form",
    ),
    path(
        "classify/form/<int:project_pk>/<int:classification_pk>/",
        classifications_views.view_classify_form,
        name="classify_form",
    ),
    path(
        "classify/form/<int:project_pk>/<int:classification_pk>/user/<int:user_pk>/",
        classifications_views.view_classify_form,
        name="classify_form",
    ),
    path(
        "classify/form/<int:project_pk>/<int:classification_pk>/ai/<int:ai_provider_id>/",
        classifications_views.view_classify_form,
        name="classify_form_ai",
    ),
    re_path(
        r"classify/approve/(?P<pk>\d+)/$",
        classifications_views.view_classify_approve,
        name="classify_approve",
    ),
    path(
        "classify/approve_ai/<int:ai_classification_id>/",
        trapper.apps.media_classification.views.ai_classifications.view_classify_ai_approve,
        name="approve_ai_classification",
    ),
    re_path(
        r"^collection/append/$",
        classifications_views.view_class_collection_resource_append,
        name="collection_append",
    ),
    re_path(
        r"classify/ai/(?P<pk>\d+)/(?P<obj>\w+)/$",
        classifications_views.view_classify_ai,
        name="classify_ai",
    ),
    re_path(
        r"classify/bbox/(?P<classification_pk>\d+)/$",
        classifications_views.view_classify_bbox,
        name="classify_bbox",
    ),
    re_path(
        r"classify/bbox/user/(?P<user_classification_pk>\d+)/$",
        classifications_views.view_classify_bbox,
        name="classify_bbox_user",
    ),
    # UserClassification views
    re_path(
        r"user_classification/list/(?P<pk>\d+)/$",
        user_classifications_views.view_user_classification_list,
        name="user_classification_list",
    ),
    re_path(
        r"user_classification/approve/(?P<pk>\d+)/$",
        user_classifications_views.view_user_classification_bulk_approve,
        name="user_classification_bulk_approve",
    ),
    re_path(
        r"^user_classification/delete/(?P<pk>\d+)/$",
        user_classifications_views.view_user_classification_delete,
        name="user_classification_delete",
    ),
    re_path(
        r"^user_classification/delete/$",
        user_classifications_views.view_user_classification_delete,
        name="user_classification_delete_multiple",
    ),
]

urlpatterns += [
    path(
        "ai_classifications/list/<int:project_id>/",
        ai_classifications_views.view_ai_classification_list,
        name="ai_classification_list",
    ),
    path(
        "ai_classifications/approve/<int:project_id>/",
        ai_classifications_views.view_ai_classification_bulk_approve,
        name="ai_classification_bulk_approve",
    ),
    path(
        "ai_classifications/delete/<int:pk>/",
        ai_classifications_views.view_ai_classification_delete,
        name="ai_classification_delete",
    ),
    path(
        "ai_classifications/delete/",
        ai_classifications_views.view_ai_classification_delete,
        name="ai_classification_delete_multiple",
    ),
    path(
        "ai_classifications/copy-bboxes/<int:project_id>/",
        ai_classifications_views.view_copy_bboxes_from_ai,
        name="ai_classification_copy_bboxes",
    ),
]
