# -*- coding: utf-8 -*-
"""
Serializers used with media classification application for DRF.
"""
import hashlib
from urllib.parse import urljoin

import pandas

from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework import serializers

from trapper.apps.geomap.taxonomy import DeploymentCaptureMethod
from trapper.apps.common.utils.datetime_tools import set_correct_offset_dst
from trapper.apps.accounts.utils import get_pretty_username
from trapper.apps.common.serializers import BasePKSerializer, PrettyUserField
from trapper.apps.media_classification.models import (
    UserClassification,
    ClassificationProject,
    Classificator,
    Classification,
    ClassificationProjectCollection,
    Sequence,
    ClassificationDynamicAttrs,
    AIClassification,
)
from trapper.apps.media_classification.taxonomy import (
    ClassificatorSettings,
    ClassificationSettings,
    ObservationType,
    SpeciesAge,
    SpeciesSex,
    SpeciesBehaviour,
)
from trapper.apps.storage.models import Resource, Collection

User = get_user_model()


class UserNestedSerializer(BasePKSerializer):
    """Serializer for :class:`auth.User`that is used by
    :class:`apps.media_classification.models.Sequence`
    """

    class Meta:
        model = User
        fields = ("name", "url")

    name = serializers.SerializerMethodField()
    url = serializers.SerializerMethodField("get_profile")

    def get_name(self, item):
        """Custom method for retrieving prettified username"""
        return get_pretty_username(user=item)

    def get_profile(self, item):
        """Custom method for retrieving profile url"""
        return reverse("accounts:show_profile", kwargs={"username": item.username})


class ResourceNestedSerializer(BasePKSerializer):
    """Serializer for :class:`apps.storage.models.Resource`
    that is used by :class:`UserClassificationSerializer` and
    :class:`ClassificationSerializer`
    """

    class Meta:
        model = Resource
        fields = (
            "pk",
            "name",
            "resource_type",
            "thumbnail_url",
            "url",
            "mime",
            "date_recorded",
            "deployment",
        )

    date_recorded = serializers.ReadOnlyField(source="date_recorded_tz")
    deployment = serializers.ReadOnlyField(source="deployment_id")
    url = serializers.ReadOnlyField(source="get_url")
    mime = serializers.ReadOnlyField(source="mime_type")
    thumbnail_url = serializers.ReadOnlyField(source="get_thumbnail_url")

    def get_resource_url(self, item):
        """Custom method for retrieving resource file url"""
        if item.file:
            return item.file.url
        return None


class ResourceNestedMapSerializer(BasePKSerializer):
    """Serializer for :class:`apps.storage.models.Resource`
    that is used by :class:`UserClassificationSerializer` and
    :class:`ClassificationSerializer`
    """

    class Meta:
        model = Resource
        fields = (
            "pk",
            "name",
            "resource_type",
            "thumbnail_url",
            "date_recorded",
            "deployment",
        )

    date_recorded = serializers.ReadOnlyField(source="date_recorded_tz")
    deployment = serializers.ReadOnlyField(source="deployment.deployment_id")
    thumbnail_url = serializers.ReadOnlyField(source="get_thumbnail_url")


class CollectionNestedSerializer(BasePKSerializer):
    """Serializer for :class:`apps.storage.models.Collection`
    that is used by :class:`ClassificationSerializer`
    """

    class Meta:
        model = Collection
        fields = ("pk", "name", "url")

    url = serializers.SerializerMethodField("get_detail_data")

    def get_detail_data(self, item):
        """Custom method for retrieving detail url"""
        return Collection.objects.api_detail_context(
            item=item, user=self.context["request"].user
        )


class ClassificationProjectCollectionNestedSerializer(BasePKSerializer):
    """Serializer for
    :class:`apps.media_classification.models.ClassificationProjectCollection`
    that is used by :class:`ClassificationProjectSerializer`
    """

    class Meta:
        model = ClassificationProjectCollection
        fields = ("name", "classifications_count", "user_classifications_count")

    name = serializers.ReadOnlyField(source="collection.collection.name")
    classifications_count = serializers.SerializerMethodField()
    user_classifications_count = serializers.SerializerMethodField()

    def get_classifications_count(self, item, *args, **kwargs):
        """Custom method for retrieving number of classifications connected to
        given classification project collection"""
        return item.classifications.count()

    def get_user_classifications_count(self, item):
        """Custom method for retrieving number of user classifications
        connected to given classification project collection"""
        user = self.context["request"].user
        return item.classifications.filter(
            user_classifications__owner__username=user
        ).count()


class UserClassificationSerializer(BasePKSerializer):
    """Serializer for
    :class:`apps.media_classification.models.UserClassification`
    Serializer contains urls for details/delete user classification
    if user has enough permissions
    """

    active_standard = None

    class Meta:
        model = UserClassification
        fields = (
            "pk",
            "owner",
            "owner_profile",
            "classification",
            "resource",
            "collection",
            "updated_at",
            "approved",
            "created_at",
            "static_attrs",
            "dynamic_attrs",
            # data for action columns
            "detail_data",
            "delete_data",
        )

    owner = PrettyUserField()
    owner_profile = serializers.SerializerMethodField()
    resource = ResourceNestedSerializer(source="classification.resource")
    collection = serializers.ReadOnlyField(source="classification.collection_id")
    approved = serializers.SerializerMethodField()
    dynamic_attrs = serializers.SerializerMethodField()
    detail_data = serializers.SerializerMethodField()
    delete_data = serializers.SerializerMethodField()

    def get_dynamic_attrs(self, item):
        """Custom method for retrieving dynamic attributes connected to given
        classification"""
        if not self.active_standard:
            if item.classificator:
                self.active_standard = item.classificator.active_standard_attrs(
                    "DYNAMIC"
                )
            else:
                self.active_standard = (
                    ClassificatorSettings.STANDARD_ATTRS_REQUIRED_DYNAMIC
                )
        dynamic_attrs = item.dynamic_attrs.all()
        return_list = []
        for row in dynamic_attrs:
            row_attrs = {}
            for attr in self.active_standard:
                row_attrs[attr] = str(getattr(row, attr))
            row_attrs.update(row.attrs)
            return_list.append(row_attrs)
        return return_list

    def get_owner_profile(self, item):
        """Custom method for retrieving profile url"""
        return reverse(
            "accounts:show_profile", kwargs={"username": item.owner.username}
        )

    def get_detail_data(self, item):
        """Custom method for retrieving detail url"""
        return UserClassification.objects.api_detail_context(
            item=item, user=self.context["request"].user
        )

    def get_delete_data(self, item):
        """Custom method for retrieving delete url"""
        return UserClassification.objects.api_delete_context(
            item=item, user=self.context["request"].user
        )

    def get_approved(self, item):
        return item.classification.approved_source_id == item.pk


class ClassificationSerializer(BasePKSerializer):
    """Serializer for
    :class:`apps.media_classification.models.Classification`
    Serializer contains urls for details/delete classification if user
    has enough permissions
    """

    default_ai_model = None
    active_standard = None

    class Meta:
        model = Classification
        fields = (
            "pk",
            "resource",
            "collection",
            "updated_at",
            "is_setup",
            "static_attrs",
            "dynamic_attrs",
            "dynamic_attrs_ai",
            "status",
            "status_ai",
            "classified",
            "classified_ai",
            "classification_project",
            # action columns
            "detail_data",
            "delete_data",
            "classify_data",
            "update_data",
            "bboxes",
        )

    resource = ResourceNestedSerializer()
    collection = serializers.ReadOnlyField(source="collection_id")
    dynamic_attrs = serializers.SerializerMethodField()
    dynamic_attrs_ai = serializers.SerializerMethodField()
    classified = serializers.SerializerMethodField()
    classified_ai = serializers.SerializerMethodField()
    classification_project = serializers.SerializerMethodField()
    classify_data = serializers.SerializerMethodField()
    delete_data = serializers.SerializerMethodField()
    detail_data = serializers.SerializerMethodField()
    update_data = serializers.SerializerMethodField()
    bboxes = serializers.SerializerMethodField()

    def get_dynamic_attrs(self, item):
        """Custom method for retrieving dynamic attributes connected to given
        classification"""
        if not self.active_standard:
            if item.classificator:
                self.active_standard = item.classificator.active_standard_attrs(
                    "DYNAMIC"
                )
            else:
                self.active_standard = (
                    ClassificatorSettings.STANDARD_ATTRS_REQUIRED_DYNAMIC
                )
        dynamic_attrs = item.dynamic_attrs.all()
        return_list = []
        for row in dynamic_attrs:
            row_attrs = {}
            for attr in self.active_standard:
                row_attrs[attr] = str(getattr(row, attr))
            row_attrs.update(row.attrs)
            return_list.append(row_attrs)
        return return_list

    def get_dynamic_attrs_ai(self, item):
        """
        Custom method for serializing approved AIClassification's chosen dynamic attributes. Only exports attrs that
        were provided in AIClassification (None values are not included).
        Empty list is exported for not approved AIClassifications.
        Currently exports observation_type, count and species fields.
        """
        attrs = []
        if item.status_ai and item.approved_source_ai:
            ai_dynamic_attrs = item.approved_source_ai.dynamic_attrs.all()
            for row in ai_dynamic_attrs:
                exported_attrs = ["observation_type", "count"]
                row_attrs = {
                    attr: getattr(row, attr)
                    for attr in exported_attrs
                    if getattr(row, attr) is not None
                }
                attrs.append(row_attrs)

                # export species separately, as it is not serializable
                if row.species:
                    row_attrs["species"] = str(row.species)
        return attrs

    def get_classify_data(self, item):
        """Custom method for retrieving classification url"""
        return reverse("media_classification:classify", kwargs={"pk": item.pk})

    def get_detail_data(self, item):
        """Custom method for retrieving delete url"""
        return Resource.objects.api_detail_context(
            item=item.resource, user=self.context["request"].user
        )

    def get_update_data(self, item):
        """Custom method for retrieving delete url"""
        return Resource.objects.api_update_context(
            item=item.resource, user=self.context["request"].user
        )

    def get_delete_data(self, item):
        """Custom method for retrieving delete url"""
        return reverse(
            "media_classification:classification_delete", kwargs={"pk": item.pk}
        )

    def get_classified(self, item):
        return item.user_classifications.exists()

    def get_classified_ai(self, item):
        return item.ai_classifications.exists()

    def get_bboxes(self, item):
        return item.dynamic_attrs.filter(bboxes__isnull=False).exists()

    def get_classification_project(self, item):
        return item.project.name


class AIClassificationSerializer(BasePKSerializer):
    """Serializer for
    :class:`apps.media_classification.models.AIClassification`
    Serializer contains urls for details/delete AI classification
    if user has enough permissions
    """

    active_standard = None

    class Meta:
        model = AIClassification
        fields = (
            "pk",
            "owner",
            "owner_profile",
            "classification",
            "resource",
            "collection",
            "updated_at",
            "approved",
            "created_at",
            "static_attrs",
            "dynamic_attrs",
            # data for action columns
            "detail_data",
            "delete_data",
            "ai_provider",
        )

    owner = PrettyUserField()
    ai_provider = serializers.ReadOnlyField(source="model.name")
    owner_profile = serializers.SerializerMethodField()
    resource = ResourceNestedSerializer(source="classification.resource")
    collection = serializers.ReadOnlyField(source="classification.collection_id")
    approved = serializers.SerializerMethodField()
    dynamic_attrs = serializers.SerializerMethodField()
    detail_data = serializers.SerializerMethodField()
    delete_data = serializers.SerializerMethodField()

    def get_dynamic_attrs(self, item):
        """Custom method for retrieving dynamic attributes connected to given
        classification"""
        if not self.active_standard:
            if item.classificator:
                self.active_standard = item.classificator.active_standard_attrs(
                    "DYNAMIC"
                )
            else:
                self.active_standard = (
                    ClassificatorSettings.STANDARD_ATTRS_REQUIRED_DYNAMIC
                )
        dynamic_attrs = item.dynamic_attrs.all()
        return_list = []
        for row in dynamic_attrs:
            row_attrs = {}
            for attr in self.active_standard:
                row_attrs[attr] = str(getattr(row, attr))
            if row.classification_confidence:
                row_attrs[
                    "classification_confidence"
                ] = f"{row.classification_confidence:.2f}"
            row_attrs.update(row.attrs)
            return_list.append(row_attrs)
        return return_list

    def get_owner_profile(self, item):
        """Custom method for retrieving profile url"""
        return reverse(
            "accounts:show_profile", kwargs={"username": item.owner.username}
        )

    def get_detail_data(self, item):
        """Custom method for retrieving detail url"""
        return AIClassification.objects.api_detail_context(
            item=item, user=self.context["request"].user
        )

    def get_delete_data(self, item):
        """Custom method for retrieving delete url"""
        return AIClassification.objects.api_delete_context(
            item=item, user=self.context["request"].user
        )

    def get_approved(self, item):
        return item.classification.approved_source_ai_id == item.pk


class ClassificationMapSerializer(BasePKSerializer):
    """Serializer for
    :class:`apps.media_classification.models.Classification`
    Serializer contains urls for details/delete classification if user
    has enough permissions
    """

    class Meta:
        model = Classification
        fields = (
            "pk",
            "resource",
            "static_attrs",
            "dynamic_attrs",
            "classify_data",
            "project",
        )

    resource = ResourceNestedMapSerializer()
    dynamic_attrs = serializers.SerializerMethodField()
    classify_data = serializers.SerializerMethodField()

    def get_dynamic_attrs(self, item):
        """Custom method for retrieving dynamic attributes connected to given
        classification"""
        dynamic_attrs = item.dynamic_attrs.all()
        return_list = []
        for row in dynamic_attrs:
            return_list.append(row.attrs)
        return return_list

    def get_classify_data(self, item):
        """Custom method for retrieving classification url"""
        return Classification.objects.api_detail_context(
            item=item, user=self.context["request"].user
        )


class ClassificatorSerializer(BasePKSerializer):
    """Serializer for
    :class:`apps.media_classification.models.Classificator`
    Serializer contains urls for details/delete/update classificator if user
    has enough permissions
    """

    class Meta:
        model = Classificator
        fields = (
            "pk",
            "name",
            "owner",
            "owner_profile",
            "updated_date",
            "predefined_attrs",
            "static_attrs_order",
            "custom_attrs",
            "dynamic_attrs_order",
            "description",
            "classification_projects",
            # data for action columns
            "update_data",
            "detail_data",
            "delete_data",
        )

    owner = PrettyUserField()
    owner_profile = serializers.SerializerMethodField()
    classification_projects = serializers.ReadOnlyField(
        source="classification_projects.all.name"
    )

    update_data = serializers.SerializerMethodField()
    detail_data = serializers.SerializerMethodField()
    delete_data = serializers.SerializerMethodField()

    def get_owner_profile(self, item):
        """Custom method for retrieving profile url"""
        return reverse(
            "accounts:show_profile", kwargs={"username": item.owner.username}
        )

    def get_update_data(self, item):
        """Custom method for retrieving update url"""
        return Classificator.objects.api_update_context(
            item=item, user=self.context["request"].user
        )

    def get_detail_data(self, item):
        """Custom method for retrieving detail url"""
        return Classificator.objects.api_detail_context(
            item=item, user=self.context["request"].user
        )

    def get_delete_data(self, item):
        """Custom method for retrieving delete url"""
        return Classificator.objects.api_delete_context(
            item=item, user=self.context["request"].user
        )


class ClassificationProjectSerializer(BasePKSerializer):
    """Serializer for
    :class:`apps.media_classification.models.ClassificationProject`
    Serializer contains urls for details/delete/update classification project
    if user has enough permissions
    """

    class Meta:
        model = ClassificationProject
        fields = (
            "pk",
            "name",
            "owner",
            "owner_profile",
            "classificator",
            "research_project",
            "status",
            "is_active",
            "project_roles",
            "classificator_removed",
            # data for action columns
            "update_data",
            "detail_data",
            "delete_data",
        )

    owner = PrettyUserField()
    owner_profile = serializers.SerializerMethodField()
    research_project = serializers.ReadOnlyField(source="research_project.name")
    status = serializers.ReadOnlyField(source="get_status_display")
    classificator_removed = serializers.ReadOnlyField()
    is_active = serializers.ReadOnlyField()
    project_roles = serializers.SerializerMethodField("get_roles")

    update_data = serializers.SerializerMethodField()
    detail_data = serializers.SerializerMethodField()
    delete_data = serializers.SerializerMethodField()

    def get_owner_profile(self, item):
        """Custom method for retrieving profile url"""
        return reverse(
            "accounts:show_profile", kwargs={"username": item.owner.username}
        )

    def get_update_data(self, item):
        """Custom method for retrieving update url"""
        return ClassificationProject.objects.api_update_context(
            item=item, user=self.context["request"].user
        )

    def get_detail_data(self, item):
        """Custom method for retrieving detail url"""
        return ClassificationProject.objects.api_detail_context(
            item=item, user=self.context["request"].user
        )

    def get_delete_data(self, item):
        """Custom method for retrieving delete url"""
        return ClassificationProject.objects.api_delete_context(
            item=item, user=self.context["request"].user
        )

    def get_roles(self, item):
        """Custom method for retrieving list of roles connected to given
        classification project. Each role contain:

        * user (prettified version)
        * profile url
        * list of role names that user has in project
        """
        roles_list = []
        for user, role_list in item.get_roles().items():
            roles = [role.get_name_display() for role in role_list]
            roles.sort()
            roles_list.append(
                {
                    "user": get_pretty_username(user=user),
                    "profile": reverse(
                        "accounts:show_profile", kwargs={"username": user.username}
                    ),
                    "roles": roles,
                }
            )
        roles_list.sort(key=lambda x: x["user"])
        return roles_list


class ClassificationProjectCollectionSerializer(BasePKSerializer):
    """Serializer for
    :class:`apps.media_classification.models.ClassificationProjectCollection`
    Serializer contains urls for details/classify classification project
    collection if user has enough permissions
    """

    class Meta:
        model = ClassificationProjectCollection
        fields = (
            "pk",
            "collection_pk",
            "name",
            "status",
            "is_active",
            "detail_data",
            "classify_data",
            "approved_count",
            "classified_count",
            "total_count",
        )

    name = serializers.ReadOnlyField(source="collection.collection.name")
    collection_pk = serializers.ReadOnlyField(source="collection.collection.pk")
    status = serializers.ReadOnlyField(source="collection.collection.status")
    detail_data = serializers.SerializerMethodField()
    classify_data = serializers.SerializerMethodField()

    # basic stats
    approved_count = serializers.SerializerMethodField()
    classified_count = serializers.SerializerMethodField()
    total_count = serializers.SerializerMethodField()

    def get_detail_data(self, item):
        """Custom method for retrieving detail url"""
        return Collection.objects.api_detail_context(
            item=item.collection.collection, user=self.context["request"].user
        )

    def get_classify_data(self, item):
        """Custom method for retrieving classify url"""
        return ClassificationProjectCollection.objects.api_classify_context(
            item=item, user=self.context["request"].user
        )

    def get_classified_count(self, item, *args, **kwargs):
        """Custom method for retrieving number of classifications connected to
        given classification project collection"""
        return (
            item.classifications.filter(user_classifications__isnull=False)
            .distinct()
            .count()
        )

    def get_approved_count(self, item, *args, **kwargs):
        """Custom method for retrieving number of approved classifications
        connected to given classification project collection"""
        return item.classifications.filter(status=True).count()

    def get_total_count(self, item):
        """Custom method for retrieving number of resources
        connected to given classification project collection"""
        return item.classifications.count()


class SequenceReadSerializer(BasePKSerializer):
    """Serializer for
    :class:`apps.media_classification.models.Sequence`

    .. note::
        Within DRF sequences are readonly
    """

    class Meta:
        model = Sequence
        fields = (
            "pk",
            "sequence_id",
            "description",
            "collection",
            "resources",
            "created_at",
        )


class ClassificationResourceSerializer(BasePKSerializer):
    """Serializer for :class:`apps.storage.models.Resource`.

    This serializer has been defined because contains lots of heavy to retrieve
    information about classifications connected to resource, which aren't
    needed anywere else.
    """

    class Meta:
        model = Classification
        fields = (
            "pk",
            "name",
            "thumbnail_url",
            "url",
            "mime",
            "resource_type",
            "date_recorded",
            "sequence",
            "classification_data",
        )

    pk = serializers.ReadOnlyField(source="resource.pk")
    name = serializers.ReadOnlyField(source="resource.name")
    resource_type = serializers.ReadOnlyField(source="resource.resource_type")
    date_recorded = serializers.ReadOnlyField(source="resource.date_recorded_tz")
    url = serializers.ReadOnlyField(source="resource.get_url")
    mime = serializers.ReadOnlyField(source="resource.mime_type")
    thumbnail_url = serializers.ReadOnlyField(source="resource.get_thumbnail_url")
    sequence = serializers.ReadOnlyField(source="sequence.sequence_id")
    classification_data = serializers.SerializerMethodField()

    def get_classification_data(self, item):
        """Return information about classification connected to given
        resource that has been classified in given research project and
        belongs to given collection"""

        user_classifications = item.user_classifications.all()
        url = reverse("media_classification:classify", kwargs={"pk": item.pk})
        is_approved = item.is_approved
        is_classified = len(user_classifications) > 0
        has_bboxes_classified = any(k.has_bboxes is True for k in user_classifications)
        return {
            "pk": item.pk,
            "url": url,
            "is_approved": is_approved,
            "is_classified": is_classified,
            "has_bboxes_classified": has_bboxes_classified,
            "has_bboxes_approved": item.has_bboxes,
        }


class ClassificationDynamicAttrsSerializer(BasePKSerializer):
    """"""

    class Meta:
        model = ClassificationDynamicAttrs
        fields = (
            "pk",
            "observation_type",
            "species_id",
            "scientific_name",
            "species_common",
            "classification_confidence",
            "count",
            "count_new",
            "age",
            "sex",
            "behaviour",
            "individual_id",
            "attrs",
            "bboxes",
        )

    scientific_name = serializers.ReadOnlyField(
        source="species.latin_name", default=None
    )
    species_common = serializers.ReadOnlyField(
        source="species.english_name", default=None
    )


class ClassificationDataSerializer(BasePKSerializer):
    """"""

    class Meta:
        model = Classification
        fields = ("pk", "observations")

    observations = ClassificationDynamicAttrsSerializer(
        source="dynamic_attrs", many=True, read_only=True
    )


def prepare_media_table(
    queryset,
    trapper_url=False,
    trapper_url_token=False,
    host=None,
    private_human=True,
    private_vehicle=True,
    private_species=None,
):
    """
    Custom table serializer (supposed to be faster than standard DRF one).
    Returns `pandas.DataFrame` object.

    :param queryset: qs of Classification objects
    :param trapper_url: bool, whether to build absolute url for each resource
    :param trapper_url_token: bool, whether to attach trapper authentication token to resource urls
    :param host: str or None, url to instance host
    :param private_human: bool, whether to keep resources classified as human private
        by not adding tokens to urls and setting filePublic = False
    :param private_vehicle: bool, whether to keep resources classified as vehicle private
    :param private_species: list or None, list of species to keep resources as private

    Built according to Camtrap DP specifications for media.csv file
    Schema available at settings.CAMTRAP_PACKAGE_SCHEMAS_MEDIA
    See: https://tdwg.github.io/camtrap-dp
    """
    _url_template = "/storage/resource/media/{pk}/file/"
    fields = {
        # (internal_trapper_db_field, camtrapdp_name)
        "resource_id": "mediaID",
        "resource__deployment__deployment_id": "deploymentID",
        "resource__deployment__capture_method": "captureMethod",
        "resource__date_recorded": "timestamp",
        "resource__file": "filePath",
        "resource__name": "fileName",
        "resource__mime_type": "fileMediatype",
        "resource__exif_data": "exifData",
        "resource__favourite": "favorite",
        "resource__description": "mediaComments",
        "resource__id": "_id",
    }
    extra_fields = [
        "sequence__collection_id",
        "resource__token",
        "resource__deployment__location__timezone",
        "resource__deployment__location__ignore_DST",
        "dynamic_attrs__observation_type",
        "dynamic_attrs__species__pk",
    ]
    export_fields = [
        "mediaID",
        "deploymentID",
        "captureMethod",
        "timestamp",
        "filePath",
        "filePublic",
        "fileName",
        "fileMediatype",
        "exifData",
        "favorite",
        "mediaComments",
        "_id",
    ]

    query_fields = list(fields.keys()) + extra_fields
    values = list(queryset.values(*query_fields))

    # update table's input data
    for k in values:
        # decode captureMethod
        capture_method_choices = DeploymentCaptureMethod.EXPORT_MAP
        k["resource__deployment__capture_method"] = capture_method_choices.get(
            k["resource__deployment__capture_method"]
        )

        # set filePublic flag to later determine whether to add token to url, consider 3 conditions
        # trapper_url_token flag, private_human flag + observation_type, private_species + species
        file_public = trapper_url_token
        if trapper_url_token:
            if (
                private_human
                and k["dynamic_attrs__observation_type"] == ObservationType.HUMAN
            ):
                file_public = False
            if (
                private_vehicle
                and k["dynamic_attrs__observation_type"] == ObservationType.VEHICLE
            ):
                file_public = False
            if private_species and k["dynamic_attrs__species__pk"] in private_species:
                file_public = False
        k["filePublic"] = file_public

        # build absolute url to each resource when requested
        if trapper_url:
            resource_id = k.get("resource_id")
            url = _url_template.format(pk=resource_id)
            if host:
                url = urljoin(host, url)
            if file_public:
                resource_token = k.pop("resource__token")
                url = urljoin(url, f"?rt={resource_token}")
            k["resource__file"] = url

        # (re)format datetime fields
        timezone = k.pop("resource__deployment__location__timezone")
        ignore_DST = k.pop("resource__deployment__location__ignore_DST")
        k["resource__date_recorded"] = set_correct_offset_dst(
            k["resource__date_recorded"], timezone, ignore_DST
        ).strftime("%Y-%m-%dT%H:%M:%S%z")

    df = pandas.DataFrame.from_records(values)
    # translate columns to the standard headers
    df.columns = [fields.get(k, k) for k in df.columns]
    # only include standard fields in exported df
    df = df[export_fields]

    # querying for dynamic_attrs__observation_type can create multiple entries for a single resource
    # df needs to be flattened so that mediaID is unique for each record
    # the only difference between duplicates should be the filePublic column - if ordered, False will be first
    df = df.sort_values(["mediaID", "filePublic"])
    df = df.drop_duplicates(subset=["mediaID"])

    # sort a table
    df = df.sort_values(["deploymentID", "mediaID", "timestamp"])
    df.reset_index(inplace=True, drop=True)

    return df


def prepare_results_table(
    queryset,
    md5_sequence=True,
    include_trapper_id=True,
    include_events=True,
    events_agg_age=False,
    events_agg_sex=False,
    trapper_format=False,
):
    """
    Custom table serializer (supposed to be faster than standard DRF one).
    Returns `pandas.DataFrame` object.

    :param queryset: queryset of Classification objects
    :param md5_sequence: whether to include md5 hash of sequence_id
    :param include_trapper_id: Whether to include the '_id' column in exported tables.
        The '_id' column contains IDs from Trapper's database and is required to import data
        into Trapper."
    :param include_events: whether to include event-based observations
    :param events_agg_age: whether to aggregate age for event-based observations
    :param events_agg_sex: whether to aggregate sex for event-based observations
    :param trapper_format: whether to generate table in a format compatible with Trapper

    Built according to Camtrap DP specifications for observations.csv file
    Schema available at settings.CAMTRAP_PACKAGE_SCHEMAS_OBSERVATIONS
    See: https://tdwg.github.io/camtrap-dp

    Alternatively this table can be generated in a format compatible with Trapper.
    Choose Trapper format for internal export, useful for migration (export/import)
    and backing up your Trapper data.
    """
    fields = {
        # (internal_trapper_db_field, camtrapdp_name)
        "resource__deployment__deployment_id": "deploymentID",
        "resource_id": "mediaID",
        "sequence__sequence_id": "eventID",
        "resource__date_recorded": "eventStart",
        "dynamic_attrs__observation_type": "observationType",
        "dynamic_attrs__species__taxon_id": "taxonID",
        "dynamic_attrs__species__latin_name": "scientificName",
        "dynamic_attrs__count": "count",
        "dynamic_attrs__age": "lifeStage",
        "dynamic_attrs__sex": "sex",
        "dynamic_attrs__behaviour": "behavior",
        "dynamic_attrs__individual_id": "individualID",
        "id": "_id",
    }
    if not trapper_format:
        fields_order = ClassificationSettings.EXPORT_COLUMNS_CAMTRAPDP
    else:
        fields_order = ClassificationSettings.EXPORT_COLUMNS_TRAPPER

    # extra fields will not be added to the result table, but are needed to construct values
    # for fields added to fields_order above.
    extra_fields = [
        "is_setup",
        "static_attrs",
        "dynamic_attrs__attrs",
        "sequence__collection_id",
        "resource__deployment__location__timezone",
        "resource__deployment__location__ignore_DST",
        "approved_source",
        "approved_source__owner__username",
        "approved_source__updated_at",
        "approved_source_ai",
        "approved_source_ai__model__name",
        "approved_source_ai__updated_at",
        "dynamic_attrs__species__english_name",
        "dynamic_attrs__bboxes",
        "dynamic_attrs__classification_confidence",
        "dynamic_attrs__count_new",
        "dynamic_attrs__attrs__comments",
        "static_attrs__comments",
    ]

    query_fields = list(fields.keys()) + extra_fields
    values = list(queryset.values(*query_fields))

    for k in values:
        # every label from ClassificationSettings.EXPORT_COLUMNS_{CAMTRAPDP, TRAPPER} need to be
        # either included in the fields or manually set below

        # make sequence_id unique within a classification project
        sequence_id = k.get("sequence__sequence_id")
        if sequence_id:
            sequence_collection_id = k.get("sequence__collection_id")
            sequence_id_new = f"{str(sequence_id)}-{str(sequence_collection_id)}"
        else:
            sequence_id_new = str(k.get("resource_id"))
        k["sequence__sequence_id"] = sequence_id_new
        k.pop("sequence__collection_id")

        if md5_sequence:
            k["sequence__sequence_id"] = hashlib.md5(
                str(k["sequence__sequence_id"]).encode()
            ).hexdigest()

        # reformat date with correct timezone and ignore_DST setting and set eventStart and eventEnd
        timezone = k.pop("resource__deployment__location__timezone")
        ignore_DST = k.pop("resource__deployment__location__ignore_DST")
        timestamp = set_correct_offset_dst(
            k["resource__date_recorded"], timezone, ignore_DST
        )
        k["resource__date_recorded"] = timestamp
        k["eventEnd"] = timestamp

        k["observationLevel"] = "media"
        k["cameraSetupType"] = "setup" if k["is_setup"] else ""

        # map database age values to those present in the standard
        k["dynamic_attrs__age"] = SpeciesAge.EXPORT_MAP.get(k["dynamic_attrs__age"])
        sex = k["dynamic_attrs__sex"]
        k["dynamic_attrs__sex"] = sex if sex != SpeciesSex.UNDEFINED else ""
        behaviour = k["dynamic_attrs__behaviour"]
        k["dynamic_attrs__behaviour"] = (
            behaviour if behaviour != SpeciesBehaviour.UNDEFINED else ""
        )

        # insert empty values to fields not represented in trapper
        k["individualPositionRadius"] = ""
        k["individualPositionAngle"] = ""
        k["individualSpeed"] = ""

        approved_source = k.pop("approved_source")
        approved_source_username = k.pop("approved_source__owner__username")
        approved_source_at = k.pop("approved_source__updated_at")
        approved_source_ai = k.pop("approved_source_ai")
        approved_source_ai_name = k.pop("approved_source_ai__model__name")
        approved_source_ai_at = k.pop("approved_source_ai__updated_at")
        if approved_source:
            classification_method = "human"
            classified_by = approved_source_username
            updated_at = approved_source_at
        elif approved_source_ai:
            classification_method = "machine"
            classified_by = approved_source_ai_name
            updated_at = approved_source_ai_at
        else:
            classification_method = ""
            classified_by = ""
            updated_at = ""
        k["classificationMethod"] = classification_method
        k["classifiedBy"] = classified_by
        k["classificationTimestamp"] = updated_at
        k["classificationProbability"] = (
            k["dynamic_attrs__classification_confidence"] or ""
        )

        k["observationComments"] = " ".join(
            [
                k.pop("dynamic_attrs__attrs__comments") or "",
                k.pop("static_attrs__comments") or "",
            ]
        )

        # observationTags column is used for english name and custom attributes
        tags_dict = {}
        english_name = k["dynamic_attrs__species__english_name"]
        if english_name:
            tags_dict["eng"] = english_name

        if not trapper_format:
            count_new = k.get("dynamic_attrs__count_new")
            if count_new is not None:
                tags_dict["count_new"] = k.get("dynamic_attrs__count_new")

        if k.get("static_attrs"):
            k["static_attrs"].pop("comments", None)
            tags_dict.update(k.pop("static_attrs"))

        if k.get("dynamic_attrs__attrs"):
            k["dynamic_attrs__attrs"].pop("comments", None)
            tags_dict.update(k.pop("dynamic_attrs__attrs"))

        tags_str = "|".join([f"{k}:{v}" for k, v in tags_dict.items()])
        k["observationTags"] = tags_str

        # if classification has no observation_type, mark it as unclassified in results table
        if not k.get("dynamic_attrs__observation_type"):
            k.update({"dynamic_attrs__observation_type": ObservationType.UNCLASSIFIED})

        # if classification has observation type = human, fill with species' latin name
        if k["dynamic_attrs__observation_type"] == ObservationType.HUMAN:
            k["dynamic_attrs__species__latin_name"] = "Homo sapiens"

    df = pandas.DataFrame.from_records(values)

    # translate columns to the standard headers
    df.rename(columns=fields, inplace=True)

    # all fields still available including count_new, aggregate to events if required
    if include_events and not trapper_format:
        df.rename(columns={"dynamic_attrs__count_new": "count_new"}, inplace=True)

        # custom function to aggregate count_new
        def agg_count_new(x):
            value = x["count_new"].dropna().sum()
            if value == 0:
                # workaround for cases where count_new was wrong or not provided by an annotator
                return x["count"].max()
            return value

        group_columns = [
            "deploymentID",
            "eventID",
            "observationType",
            "scientificName",
        ]

        if events_agg_age:
            group_columns.append("lifeStage")
        if events_agg_sex:
            group_columns.append("sex")

        grouped = (
            df[df.observationType != ObservationType.BLANK]
            .groupby(group_columns)
            .apply(
                lambda x: pandas.Series(
                    {
                        "count": agg_count_new(x),
                        "eventStart": x["eventStart"].min(),
                        "eventEnd": x["eventEnd"].max(),
                        "classificationTimestamp": x["classificationTimestamp"].max(),
                        "cameraSetupType": x["cameraSetupType"].iloc[0],
                        "taxonID": x["taxonID"].iloc[0],
                    }
                )
            )
            .reset_index()
        )

        # add all standard columns
        empty_columns = [
            "mediaID",
            "bboxX",
            "bboxY",
            "bboxWidth",
            "bboxHeight",
            "classificationMethod",
            "classifiedBy",
            "classificationProbability",
            "observationTags",
            "observationComments",
            "_id",
            # individual attributes not supported yet for events
            "individualID",
            "individualPositionRadius",
            "individualPositionAngle",
            "individualSpeed",
            # behaviour not supported yet for events
            "behavior",
        ]

        if not events_agg_age:
            empty_columns.append("lifeStage")

        if not events_agg_sex:
            empty_columns.append("sex")

        grouped = grouped.reindex(
            columns=grouped.columns.tolist() + empty_columns, fill_value=""
        )
        grouped["observationLevel"] = "event"
        grouped = grouped[fields_order]

        # concatenate df and grouped and reset index
        df = pandas.concat([df, grouped]).reset_index(drop=True)

    if not trapper_format:
        # Camtrap DP format

        # explode dynamic_attrs__bboxes and set count to 1 for all exploded rows
        df = df.explode("dynamic_attrs__bboxes", ignore_index=True)

        # set count to 1 for all rows where dynamic_attrs__bboxes is a list with 4 elements
        df.loc[
            df["dynamic_attrs__bboxes"].apply(
                lambda x: isinstance(x, list) and len(x) == 4
            ),
            "count",
        ] = 1

        # unpack the list of bboxes in dynamic_attrs__bboxes to columns if not empty
        df["bboxX"], df["bboxY"], df["bboxWidth"], df["bboxHeight"] = (
            df["dynamic_attrs__bboxes"].str[0],
            df["dynamic_attrs__bboxes"].str[1],
            df["dynamic_attrs__bboxes"].str[2],
            df["dynamic_attrs__bboxes"].str[3],
        )

        # drop dynamic_attrs__bboxes column
        df.drop(columns=["dynamic_attrs__bboxes"], inplace=True)

    else:
        # TRAPPER format

        # rename dynamic_attrs__bboxes to bboxes
        df.rename(columns={"dynamic_attrs__bboxes": "bboxes"}, inplace=True)

        # replace empty list with empty string
        df["bboxes"] = df["bboxes"].apply(lambda x: "" if x == [] else x)

        # rename dynamic_attrs__count_new to countNew
        df.rename(columns={"dynamic_attrs__count_new": "countNew"}, inplace=True)

    # replace all NaNs and Nones with empty strings
    df = df.fillna("")
    df = df.replace({None: ""})

    # set empty count for blank images
    df.loc[df["observationType"] == ObservationType.BLANK, "count"] = ""

    # change count type to int for non empty values and convert back to str
    df["count"] = df["count"].apply(lambda x: str(int(x)) if x != "" else x)

    # reorder columns
    df = df[fields_order]
    df = df.sort_values(
        by=["deploymentID", "eventEnd", "observationLevel"],
        ascending=[True, True, False],
    )

    if not include_trapper_id:
        df.drop(columns=["_id"], inplace=True)

    # cast datetimes to str to keep the correct format
    dt_columns = ["eventStart", "eventEnd", "classificationTimestamp"]
    for col in dt_columns:
        df[col] = df[col].apply(
            lambda x: x.strftime("%Y-%m-%dT%H:%M:%S%z") if x else ""
        )

    # reset index
    df.reset_index(inplace=True, drop=True)

    # insert observationID
    df.insert(0, "observationID", df.index + 1)

    return df


def prepare_deployments_table(queryset):
    """
    Custom table serializer, accepts Deployments queryset and returns pandas DataFrame
    """
    columns = [
        "deploymentID",
        "locationID",
        "deploymentStart",
        "deploymentEnd",
        "longitude",
        "latitude",
    ]
    fields_to_extract = [
        "deployment_id",
        "location__location_id",
        "start_date",
        "end_date",
        "location__coordinates",
        "location__timezone",
        "location__ignore_DST",
    ]

    deployments = queryset.values_list(*fields_to_extract)
    deployments_data = []
    for k in deployments:
        timezone = k[5]
        ignore_DST = k[6]
        # (re)format date_recorded field
        start = set_correct_offset_dst(k[2], timezone, ignore_DST)
        end = set_correct_offset_dst(k[3], timezone, ignore_DST)
        longitude = round(k[4].x, 5)
        latitude = round(k[4].y, 5)
        deployments_data.append((k[0], k[1], start, end, longitude, latitude))

    ddf = pandas.DataFrame(deployments_data, columns=columns)
    ddf.dropna(inplace=True)

    ddf["days"] = (ddf.deploymentEnd - ddf.deploymentStart).dt.days.astype("int32")
    ddf["deploymentStart"] = ddf.deploymentStart.apply(
        lambda x: x.strftime("%Y-%m-%dT%H:%M:%S%z")
    )
    ddf["deploymentEnd"] = ddf.deploymentEnd.apply(
        lambda x: x.strftime("%Y-%m-%dT%H:%M:%S%z")
    )

    return ddf


def prepare_ai_results_table(queryset, trapper_format=False):
    """
    Custom table serializer (supposed to be faster than standard DRF one).
    Returns `pandas.DataFrame` object.

    :param queryset: queryset of Classification objects
    :param trapper_format: whether to generate table in a format compatible with Trapper

    Built according to Camtrap DP specifications for observations.csv file
    Schema available at settings.CAMTRAP_PACKAGE_SCHEMAS_OBSERVATIONS
    See: https://tdwg.github.io/camtrap-dp

    Alternatively this table can be generated in a format compatible with Trapper.
    Choose Trapper format for internal export, useful for migration (export/import)
    and backing up your Trapper data.
    """
    fields = {
        # (internal_trapper_db_field, camtrapdp_name)
        "classification__resource__deployment__deployment_id": "deploymentID",
        "classification__resource_id": "mediaID",
        "classification__resource__date_recorded": "eventStart",
        "dynamic_attrs__observation_type": "observationType",
        "dynamic_attrs__species__taxon_id": "taxonID",
        "dynamic_attrs__species__latin_name": "scientificName",
        "dynamic_attrs__count": "count",
        "dynamic_attrs__age": "lifeStage",
        "dynamic_attrs__sex": "sex",
        "dynamic_attrs__behaviour": "behavior",
        "dynamic_attrs__individual_id": "individualID",
        "model__name": "classifiedBy",
        "updated_at": "classificationTimestamp",
        "dynamic_attrs__classification_confidence": "classificationProbability",
        "id": "_id",
    }

    if not trapper_format:
        fields_order = ClassificationSettings.EXPORT_COLUMNS_CAMTRAPDP
    else:
        fields_order = ClassificationSettings.EXPORT_COLUMNS_TRAPPER

    extra_fields = [
        "classification__sequence__collection_id",
        "classification__resource__deployment__location__timezone",
        "classification__resource__deployment__location__ignore_DST",
        "dynamic_attrs__bboxes",
        "dynamic_attrs__species__english_name",
        "classification__is_setup",
        "dynamic_attrs__count_new",
    ]

    query_fields = list(fields.keys()) + extra_fields
    values = list(queryset.values(*query_fields))

    # update table's input data
    for k in values:
        # every label from ClassificationSettings.EXPORT_COLUMNS need to be either included in fields
        # or manually set below
        k["eventID"] = ""

        # (re)format date_recorded field
        timezone = k.pop("classification__resource__deployment__location__timezone")
        ignore_DST = k.pop("classification__resource__deployment__location__ignore_DST")
        timestamp = set_correct_offset_dst(
            k["classification__resource__date_recorded"], timezone, ignore_DST
        )
        k["classification__resource__date_recorded"] = timestamp
        k["eventEnd"] = timestamp
        k["observationLevel"] = "media"
        k["cameraSetupType"] = "setup" if k["classification__is_setup"] else ""

        k["dynamic_attrs__age"] = SpeciesAge.EXPORT_MAP.get(k["dynamic_attrs__age"])
        sex = k["dynamic_attrs__sex"]
        k["dynamic_attrs__sex"] = sex if sex != SpeciesSex.UNDEFINED else ""
        behaviour = k["dynamic_attrs__behaviour"]
        k["dynamic_attrs__behaviour"] = (
            behaviour if behaviour != SpeciesBehaviour.UNDEFINED else ""
        )

        # insert empty values to fields not represented in trapper
        k["individualPositionRadius"] = ""
        k["individualPositionAngle"] = ""
        k["individualSpeed"] = ""

        # get classificationMethod and classificationTimestamp
        k["classificationMethod"] = "machine"
        k["updated_at"] = k["updated_at"]

        # if classification has no observation_type, mark it as unclassified in results table
        if not k.get("dynamic_attrs__observation_type"):
            k.update({"dynamic_attrs__observation_type": "unclassified"})

        k["observationTags"] = ""
        k["observationComments"] = ""

    df = pandas.DataFrame.from_records(values)

    # translate columns to the standard headers
    df = df.rename(columns=fields)

    if not trapper_format:
        # Camtrap DP format

        # explode dynamic_attrs__bboxes and set count to 1 for all exploded rows
        df = df.explode("dynamic_attrs__bboxes", ignore_index=True)

        # set count to 1 for all rows where dynamic_attrs__bboxes is a list with 4 elements
        df.loc[
            df["dynamic_attrs__bboxes"].apply(
                lambda x: isinstance(x, list) and len(x) == 4
            ),
            "count",
        ] = 1

        # unpack the list of bboxes in dynamic_attrs__bboxes to columns if not empty
        df["bboxX"], df["bboxY"], df["bboxWidth"], df["bboxHeight"] = (
            df["dynamic_attrs__bboxes"].str[0],
            df["dynamic_attrs__bboxes"].str[1],
            df["dynamic_attrs__bboxes"].str[2],
            df["dynamic_attrs__bboxes"].str[3],
        )

        # drop dynamic_attrs__bboxes column
        df.drop(columns=["dynamic_attrs__bboxes"], inplace=True)

    else:
        # TRAPPER format

        # rename dynamic_attrs__bboxes to bboxes
        df.rename(columns={"dynamic_attrs__bboxes": "bboxes"}, inplace=True)

        # replace empty list with empty string
        df["bboxes"] = df["bboxes"].apply(lambda x: "" if x == [] else x)

        # rename dynamic_attrs__count_new to countNew
        df.rename(columns={"dynamic_attrs__count_new": "countNew"}, inplace=True)

    # replace all NaNs and Nones with empty strings
    df = df.fillna("")
    df = df.replace({None: ""})

    # set empty coount for blank images
    df.loc[df["observationType"] == ObservationType.BLANK, "count"] = ""

    # change count type to int for non empty values and convert back to str
    df["count"] = df["count"].apply(lambda x: str(int(x)) if x != "" else x)

    # reorder columns
    df = df[fields_order]

    # sort a table
    df = df.sort_values(["deploymentID", "mediaID", "eventStart"])
    df.reset_index(inplace=True, drop=True)

    # cast datetimes to str to keep the correct format
    dt_columns = ["eventStart", "eventEnd", "classificationTimestamp"]
    for col in dt_columns:
        df[col] = df[col].apply(
            lambda x: x.strftime("%Y-%m-%dT%H:%M:%S%z") if x else ""
        )

    # reset index
    df.reset_index(inplace=True, drop=True)

    # insert observationID
    df.insert(0, "observationID", df.index + 1)

    return df
