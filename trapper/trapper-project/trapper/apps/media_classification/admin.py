# -*- coding: utf-8 -*-
"""
Simple admin interface to interact with the models.
"""

from django.contrib import admin
from django.urls import reverse
from django.utils.safestring import mark_safe
from polymorphic.admin import (
    PolymorphicParentModelAdmin,
    PolymorphicChildModelAdmin,
)

from trapper.apps.media_classification.models import (
    AIClassificationJobAdditionalResource,
    Classificator,
    Classification,
    UserClassification,
    ClassificationDynamicAttrs,
    UserClassificationDynamicAttrs,
    ClassificationProject,
    ClassificationProjectRole,
    ClassificationProjectCollection,
    Sequence,
    AIProvider,
    TrapperAIProvider,
    AIProviderPermission,
    ExternalAIProvider,
    MegadetectorBatchProcessingProvider,
    AIClassification,
    AIClassificationJob,
    ObservationTypeAILabelMapping,
    SpeciesAILabelMapping,
    SexAILabelMapping,
    AgeCattegoryAILabelMapping,
    AIClassificationDynamicAttrs,
)


class ObservationTypeAILabelMappingInline(admin.TabularInline):
    model = ObservationTypeAILabelMapping
    extra = 1


class SpeciesAILabelMappingInline(admin.TabularInline):
    model = SpeciesAILabelMapping
    extra = 1
    autocomplete_fields = ["species"]


class SexAILabelMappingInline(admin.TabularInline):
    model = SexAILabelMapping
    extra = 1


class AgeCattegoryAILabelMappingInline(admin.TabularInline):
    model = AgeCattegoryAILabelMapping
    extra = 1


class ClassificationDynamicAttrsInline(admin.TabularInline):
    model = ClassificationDynamicAttrs
    extra = 0


class UserClassificationDynamicAttrsInline(admin.TabularInline):
    model = UserClassificationDynamicAttrs
    extra = 0


class AIClassificationDynamicAttrsInline(admin.TabularInline):
    model = AIClassificationDynamicAttrs
    extra = 0


class ClassificationAdmin(admin.ModelAdmin):
    list_display = (
        "__str__",
        "resource",
        "project",
        "collection",
        "status",
        "approved_by",
        "approved_at",
        "created_at",
        "sequence",
    )
    search_fields = (
        "resource__name",
        "collection__collection__collection__name",
        "dynamic_attrs__attrs",
    )
    list_filter = ("project", "collection", "status")
    raw_id_fields = (
        "resource",
        "collection",
        "sequence",
        "approved_source",
        "owner",
        "approved_by",
        "updated_by",
    )
    inlines = [
        ClassificationDynamicAttrsInline,
    ]


class ClassificationUserAdmin(admin.ModelAdmin):
    list_display = ("__str__", "classification", "created_at", "updated_at", "owner")
    search_fields = (
        "classification__resource__name",
        "dynamic_attrs__attrs",
        "owner__username",
    )
    list_filter = ("classification__project", "created_at", "updated_at")
    raw_id_fields = ("classification", "owner")
    inlines = [
        UserClassificationDynamicAttrsInline,
    ]


class AIClassificationAdmin(admin.ModelAdmin):
    list_display = (
        "__str__",
        "classification",
        "model",
        "created_at",
        "updated_at",
        "owner",
    )
    search_fields = ("classification__resource__name", "owner__username")
    list_filter = ("classification__project", "model", "created_at", "updated_at")
    raw_id_fields = ("classification", "owner")

    inlines = [
        AIClassificationDynamicAttrsInline,
    ]


class ProjectRoleInline(admin.TabularInline):
    model = ClassificationProjectRole
    extra = 0
    raw_id_fields = ["user"]


class ProjectCollectionInline(admin.TabularInline):
    model = ClassificationProjectCollection
    extra = 0
    raw_id_fields = ["collection"]
    readonly_fields = [
        "resources_count",
        "collection_storage",
    ]


class ClassificationProjectAdmin(admin.ModelAdmin):
    inlines = [ProjectRoleInline, ProjectCollectionInline]
    filter_horizontal = ("collections",)
    list_display = (
        "name",
        "research_project",
        "classificator",
        "owner",
        "date_created",
        "disabled_at",
        "disabled_by",
    )


class ClassificationProjectRoleAdmin(admin.ModelAdmin):
    list_display = ("__str__", "classification_project", "user", "name", "date_created")
    search_fields = ("user__username", "classification_project__name")
    list_filter = ("classification_project", "user", "name", "date_created")


class ClassificatorAdmin(admin.ModelAdmin):
    list_display = ("pk", "name", "created_date", "updated_date")


class ClassificationProjectCollectionAdmin(admin.ModelAdmin):
    list_display = (
        "__str__",
        "project",
        "collection",
        "is_active",
        "enable_sequencing_experts",
        "enable_crowdsourcing",
    )
    search_fields = ("collection__collection__name",)
    list_filter = ("project", "is_active")


class AIProviderChildAdmin(PolymorphicChildModelAdmin):
    base_model = AIProvider
    readonly_fields = ["id", "token"]

    inlines = [
        ObservationTypeAILabelMappingInline,
        SpeciesAILabelMappingInline,
        AgeCattegoryAILabelMappingInline,
        SexAILabelMappingInline,
    ]


class TrapperAIProviderAdmin(AIProviderChildAdmin):
    pass


class MegadetectorBatchProcessingProviderAdmin(AIProviderChildAdmin):
    pass


class ExternalAIProviderAdmin(AIProviderChildAdmin):
    pass


class AIProviderAdmin(PolymorphicParentModelAdmin):
    list_display = (
        "name",
        "description",
        "video_support",
    )
    inlines = [
        ObservationTypeAILabelMappingInline,
        SpeciesAILabelMappingInline,
        AgeCattegoryAILabelMappingInline,
        SexAILabelMappingInline,
    ]

    child_models = [
        MegadetectorBatchProcessingProvider,
        TrapperAIProvider,
        ExternalAIProvider,
    ]


class AIClassificationJobAdditionalResourceAdminInline(admin.TabularInline):
    model = AIClassificationJobAdditionalResource
    extra = 0
    list_display = (
        "id",
        "resource_name",
        "ai_provider",
        "classification_job",
    )
    readonly_fields = ("resource_url",)

    def resource_url(self, obj: AIClassificationJobAdditionalResource):
        url = reverse(
            "media_classification:api-classify-ai-extra",
            kwargs={
                "ai_provider_token": obj.ai_provider.token,
                "classification_job_id": obj.classification_job_id,
                "resource_name": obj.resource_name,
            },
        )

        return mark_safe(f'<a href="{url}" target="_blank">Data</a>')


@admin.register(AIClassificationJob)
class AIClassificationJobAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "user_remote_task_id",
        "ai_provider",
        "stage",
        "date_created",
        "date_modified",
    )
    actions = ("update_status", "reparse_results")

    readonly_fields = ("user_remote_task",)

    inlines = [AIClassificationJobAdditionalResourceAdminInline]

    def update_status(self, request, queryset):
        from .tasks import celery_update_remote_classification_task_status

        for job in queryset:
            celery_update_remote_classification_task_status.delay(job.id)

    def reparse_results(self, request, queryset):
        from .tasks import celery_update_remote_classification_task_status

        queryset.update(stage="parse_results")
        for job in queryset:
            celery_update_remote_classification_task_status.delay(job.id)


@admin.register(AIClassificationJobAdditionalResource)
class AIClassificationJobAdditionalResourceAdmin(admin.ModelAdmin):
    list_display = ("id", "resource_name", "ai_provider", "classification_job")
    readonly_fields = ("data",)


@admin.register(AIProviderPermission)
class AIProviderPermissionAdmin(admin.ModelAdmin):
    list_display = (
        "user",
        "ai_provider",
    )

    def has_add_permission(self, request):
        return request.user.is_superuser


admin.site.register(Classificator, ClassificatorAdmin)
admin.site.register(AIProvider, AIProviderAdmin)
admin.site.register(AIClassification, AIClassificationAdmin)
admin.site.register(
    ClassificationProjectCollection, ClassificationProjectCollectionAdmin
)
admin.site.register(Classification, ClassificationAdmin)
admin.site.register(UserClassification, ClassificationUserAdmin)
admin.site.register(ClassificationProjectRole, ClassificationProjectRoleAdmin)
admin.site.register(Sequence)
admin.site.register(ClassificationProject, ClassificationProjectAdmin)
admin.site.register(TrapperAIProvider, TrapperAIProviderAdmin)
admin.site.register(
    MegadetectorBatchProcessingProvider, MegadetectorBatchProcessingProviderAdmin
)
admin.site.register(ExternalAIProvider, ExternalAIProviderAdmin)
