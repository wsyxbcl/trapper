from django.db import migrations


def update_has_initial_data(apps, schema_editor):
    Classification = apps.get_model("media_classification", "Classification")
    classifications = Classification.objects.filter(dynamic_attrs__isnull=False, static_attrs__isnull=False,
                                                    has_initial_data=False)
    classifications.update(has_initial_data=True)


class Migration(migrations.Migration):

    dependencies = [
        ("media_classification", "0054_trapperaiprovider_ai_model_id"),
    ]

    operations = [
        migrations.RunPython(update_has_initial_data)
    ]
