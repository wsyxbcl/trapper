# Generated by Django 2.2.11 on 2020-05-10 09:55

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion
import trapper.apps.common.fields


class Migration(migrations.Migration):

    dependencies = [
        ("media_classification", "0012_auto_20200410_1410"),
    ]

    operations = [
        migrations.DeleteModel(
            name="UserClassificationManager",
        ),
        migrations.AlterModelOptions(
            name="classification",
            options={
                "ordering": ("resource",),
                "verbose_name": "Classification",
                "verbose_name_plural": "Classifications",
            },
        ),
        migrations.AlterModelOptions(
            name="classificationdynamicattrs",
            options={
                "verbose_name": "Classification dynamic attribute",
                "verbose_name_plural": "Classifications dynamic attributes",
            },
        ),
        migrations.AlterModelOptions(
            name="classificationproject",
            options={
                "ordering": ["-date_created"],
                "verbose_name": "Classification project",
                "verbose_name_plural": "Classification projects",
            },
        ),
        migrations.AlterModelOptions(
            name="classificationprojectcollection",
            options={
                "verbose_name": "Classification projects collection",
                "verbose_name_plural": "Classification projects collections",
            },
        ),
        migrations.AlterModelOptions(
            name="classificationprojectrole",
            options={
                "ordering": ["user", "name"],
                "verbose_name": "Classification project role",
                "verbose_name_plural": "Classification project roles",
            },
        ),
        migrations.AlterModelOptions(
            name="classificator",
            options={
                "verbose_name": "Classificator",
                "verbose_name_plural": "Classificators",
            },
        ),
        migrations.AddField(
            model_name="classification",
            name="base_classification",
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name="aiclassification",
            name="created_at",
            field=models.DateTimeField(auto_now_add=True, verbose_name="Created at"),
        ),
        migrations.AlterField(
            model_name="aiclassification",
            name="detected_objects",
            field=django.contrib.postgres.fields.jsonb.JSONField(
                blank=True, null=True, verbose_name="Detected objects"
            ),
        ),
        migrations.AlterField(
            model_name="aiclassification",
            name="results",
            field=django.contrib.postgres.fields.jsonb.JSONField(
                verbose_name="Results"
            ),
        ),
        migrations.AlterField(
            model_name="aiclassification",
            name="updated_at",
            field=models.DateTimeField(auto_now_add=True, verbose_name="Updated at"),
        ),
        migrations.AlterField(
            model_name="ailabel",
            name="age",
            field=models.CharField(
                choices=[
                    ("Adult", "Adult"),
                    ("Juvenile", "Juvenile"),
                    ("Unknown", "Unknown"),
                ],
                default="Unknown",
                max_length=20,
            ),
        ),
        migrations.AlterField(
            model_name="ailabel",
            name="sex",
            field=models.CharField(
                choices=[
                    ("Female", "Female"),
                    ("Male", "Male"),
                    ("Unknown", "Unknown"),
                ],
                default="Unknown",
                max_length=20,
            ),
        ),
        migrations.AlterField(
            model_name="aiprovider",
            name="api_auth_login",
            field=models.CharField(max_length=100, verbose_name="Api auth login"),
        ),
        migrations.AlterField(
            model_name="aiprovider",
            name="api_auth_passw",
            field=models.CharField(max_length=100, verbose_name="Api auth passw"),
        ),
        migrations.AlterField(
            model_name="aiprovider",
            name="api_url",
            field=models.URLField(verbose_name="Api URL"),
        ),
        migrations.AlterField(
            model_name="aiprovider",
            name="conf",
            field=django.contrib.postgres.fields.jsonb.JSONField(
                blank=True, null=True, verbose_name="Conf"
            ),
        ),
        migrations.AlterField(
            model_name="aiprovider",
            name="description",
            field=models.TextField(
                blank=True, max_length=1000, null=True, verbose_name="Description"
            ),
        ),
        migrations.AlterField(
            model_name="aiprovider",
            name="name",
            field=models.CharField(max_length=100, verbose_name="Name"),
        ),
        migrations.AlterField(
            model_name="aiprovider",
            name="object_based",
            field=models.BooleanField(default=True, verbose_name="Object based"),
        ),
        migrations.AlterField(
            model_name="aiprovider",
            name="video_support",
            field=models.BooleanField(default=False, verbose_name="Video support"),
        ),
        migrations.AlterField(
            model_name="classification",
            name="approved_ai_at",
            field=models.DateTimeField(
                blank=True, null=True, verbose_name="Approved AI at"
            ),
        ),
        migrations.AlterField(
            model_name="classification",
            name="approved_at",
            field=models.DateTimeField(
                blank=True, null=True, verbose_name="Approved at"
            ),
        ),
        migrations.AlterField(
            model_name="classification",
            name="created_at",
            field=models.DateTimeField(auto_created=True, verbose_name="Created at"),
        ),
        migrations.AlterField(
            model_name="classification",
            name="resource",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="classifications",
                to="storage.Resource",
            ),
        ),
        migrations.AlterField(
            model_name="classification",
            name="static_attrs",
            field=django.contrib.postgres.fields.jsonb.JSONField(
                blank=True, default=dict, null=True, verbose_name="Static attrs"
            ),
        ),
        migrations.AlterField(
            model_name="classification",
            name="status",
            field=models.BooleanField(default=False, verbose_name="Status"),
        ),
        migrations.AlterField(
            model_name="classification",
            name="status_ai",
            field=models.BooleanField(default=False, verbose_name="Status AI"),
        ),
        migrations.AlterField(
            model_name="classification",
            name="updated_at",
            field=models.DateTimeField(
                blank=True, null=True, verbose_name="Updated at"
            ),
        ),
        migrations.AlterField(
            model_name="classificationdynamicattrs",
            name="age",
            field=models.CharField(
                choices=[
                    ("Adult", "Adult"),
                    ("Juvenile", "Juvenile"),
                    ("Unknown", "Unknown"),
                ],
                default="Unknown",
                max_length=20,
            ),
        ),
        migrations.AlterField(
            model_name="classificationdynamicattrs",
            name="attrs",
            field=django.contrib.postgres.fields.jsonb.JSONField(
                blank=True, default=dict, null=True, verbose_name="Attrs"
            ),
        ),
        migrations.AlterField(
            model_name="classificationdynamicattrs",
            name="behaviour",
            field=models.CharField(
                choices=[
                    ("Grazing", "Grazing"),
                    ("Browsing", "Browsing"),
                    ("Rooting", "Rooting"),
                    ("Vigilance", "Vigilance"),
                    ("Running", "Running"),
                    ("Walking", "Walking"),
                    ("Unknown", "Unknown"),
                ],
                default="Unknown",
                max_length=20,
            ),
        ),
        migrations.AlterField(
            model_name="classificationdynamicattrs",
            name="sex",
            field=models.CharField(
                choices=[
                    ("Female", "Female"),
                    ("Male", "Male"),
                    ("Unknown", "Unknown"),
                ],
                default="Unknown",
                max_length=20,
            ),
        ),
        migrations.AlterField(
            model_name="classificationproject",
            name="classificator",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="classification_projects",
                to="media_classification.Classificator",
                verbose_name="Classificator",
            ),
        ),
        migrations.AlterField(
            model_name="classificationproject",
            name="date_created",
            field=models.DateTimeField(auto_now_add=True, verbose_name="Date created"),
        ),
        migrations.AlterField(
            model_name="classificationproject",
            name="default_ai_model",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="classification_projects",
                to="media_classification.AIProvider",
                verbose_name="Default AI model",
            ),
        ),
        migrations.AlterField(
            model_name="classificationproject",
            name="disabled_at",
            field=models.DateTimeField(
                blank=True, editable=False, null=True, verbose_name="Disabled at"
            ),
        ),
        migrations.AlterField(
            model_name="classificationproject",
            name="name",
            field=models.CharField(max_length=255, verbose_name="Name"),
        ),
        migrations.AlterField(
            model_name="classificationproject",
            name="research_project",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="classification_projects",
                to="research.ResearchProject",
                verbose_name="Research project",
            ),
        ),
        migrations.AlterField(
            model_name="classificationproject",
            name="status",
            field=models.IntegerField(
                choices=[(1, "Ongoing"), (2, "Finished")],
                default=1,
                verbose_name="Status",
            ),
        ),
        migrations.AlterField(
            model_name="classificationprojectrole",
            name="date_created",
            field=models.DateTimeField(auto_now_add=True, verbose_name="Date created"),
        ),
        migrations.AlterField(
            model_name="classificationprojectrole",
            name="name",
            field=models.IntegerField(
                choices=[(1, "Admin"), (2, "Expert"), (3, "Collaborator")],
                verbose_name="Name",
            ),
        ),
        migrations.AlterField(
            model_name="classificator",
            name="age",
            field=models.BooleanField(default=False, verbose_name="Age"),
        ),
        migrations.AlterField(
            model_name="classificator",
            name="behaviour",
            field=models.BooleanField(default=False, verbose_name="Behaviour"),
        ),
        migrations.AlterField(
            model_name="classificator",
            name="count",
            field=models.BooleanField(default=True, verbose_name="Count"),
        ),
        migrations.AlterField(
            model_name="classificator",
            name="count_new",
            field=models.BooleanField(default=False, verbose_name="Count new"),
        ),
        migrations.AlterField(
            model_name="classificator",
            name="created_date",
            field=models.DateTimeField(auto_now_add=True, verbose_name="Created date"),
        ),
        migrations.AlterField(
            model_name="classificator",
            name="custom_attrs",
            field=django.contrib.postgres.fields.jsonb.JSONField(
                blank=True, default=dict, null=True, verbose_name="Custom attrs"
            ),
        ),
        migrations.AlterField(
            model_name="classificator",
            name="description",
            field=trapper.apps.common.fields.SafeTextField(
                blank=True, max_length=2000, null=True, verbose_name="Description"
            ),
        ),
        migrations.AlterField(
            model_name="classificator",
            name="dynamic_attrs_order",
            field=models.TextField(
                blank=True, null=True, verbose_name="Dynamic attrs order"
            ),
        ),
        migrations.AlterField(
            model_name="classificator",
            name="individual_id",
            field=models.BooleanField(default=False, verbose_name="Individual ID"),
        ),
        migrations.AlterField(
            model_name="classificator",
            name="is_empty",
            field=models.BooleanField(default=True, verbose_name="Is empty"),
        ),
        migrations.AlterField(
            model_name="classificator",
            name="name",
            field=models.CharField(max_length=255, unique=True, verbose_name="Name"),
        ),
        migrations.AlterField(
            model_name="classificator",
            name="observation_type",
            field=models.BooleanField(default=True, verbose_name="Observation type"),
        ),
        migrations.AlterField(
            model_name="classificator",
            name="predefined_attrs",
            field=django.contrib.postgres.fields.jsonb.JSONField(
                blank=True, default=dict, null=True, verbose_name="Predefined attrs"
            ),
        ),
        migrations.AlterField(
            model_name="classificator",
            name="sex",
            field=models.BooleanField(default=False, verbose_name="Sex"),
        ),
        migrations.AlterField(
            model_name="classificator",
            name="species",
            field=models.ManyToManyField(
                blank=True, to="extra_tables.Species", verbose_name="Species"
            ),
        ),
        migrations.AlterField(
            model_name="classificator",
            name="static_attrs_order",
            field=models.TextField(
                blank=True, null=True, verbose_name="Static attrs order"
            ),
        ),
        migrations.AlterField(
            model_name="classificator",
            name="template",
            field=models.CharField(
                choices=[("inline", "Inline"), ("tab", "Tabbed")],
                default="inline",
                max_length=50,
                verbose_name="Template",
            ),
        ),
        migrations.AlterField(
            model_name="classificator",
            name="uncertainty",
            field=models.BooleanField(default=False, verbose_name="Uncertainty"),
        ),
        migrations.AlterField(
            model_name="classificator",
            name="updated_date",
            field=models.DateTimeField(auto_now=True, verbose_name="Updated date"),
        ),
        migrations.AlterField(
            model_name="sequence",
            name="created_at",
            field=models.DateTimeField(auto_now_add=True, verbose_name="Created at"),
        ),
        migrations.AlterField(
            model_name="sequence",
            name="description",
            field=models.TextField(
                blank=True, max_length=1000, null=True, verbose_name="Description"
            ),
        ),
        migrations.AlterField(
            model_name="sequence",
            name="resources",
            field=models.ManyToManyField(
                through="media_classification.SequenceResourceM2M",
                to="storage.Resource",
                verbose_name="Resources",
            ),
        ),
        migrations.AlterField(
            model_name="sequence",
            name="sequence_id",
            field=models.IntegerField(
                blank=True, null=True, verbose_name="Sequence id"
            ),
        ),
        migrations.AlterField(
            model_name="userclassification",
            name="created_at",
            field=models.DateTimeField(auto_now_add=True, verbose_name="Created at"),
        ),
        migrations.AlterField(
            model_name="userclassification",
            name="static_attrs",
            field=django.contrib.postgres.fields.jsonb.JSONField(
                blank=True, default=dict, null=True, verbose_name="Static attrs"
            ),
        ),
        migrations.AlterField(
            model_name="userclassification",
            name="updated_at",
            field=models.DateTimeField(auto_now_add=True, verbose_name="Updated at"),
        ),
        migrations.AlterField(
            model_name="userclassificationdynamicattrs",
            name="age",
            field=models.CharField(
                choices=[
                    ("Adult", "Adult"),
                    ("Juvenile", "Juvenile"),
                    ("Unknown", "Unknown"),
                ],
                default="Unknown",
                max_length=20,
            ),
        ),
        migrations.AlterField(
            model_name="userclassificationdynamicattrs",
            name="attrs",
            field=django.contrib.postgres.fields.jsonb.JSONField(
                blank=True, default=dict, null=True, verbose_name="Attrs"
            ),
        ),
        migrations.AlterField(
            model_name="userclassificationdynamicattrs",
            name="behaviour",
            field=models.CharField(
                choices=[
                    ("Grazing", "Grazing"),
                    ("Browsing", "Browsing"),
                    ("Rooting", "Rooting"),
                    ("Vigilance", "Vigilance"),
                    ("Running", "Running"),
                    ("Walking", "Walking"),
                    ("Unknown", "Unknown"),
                ],
                default="Unknown",
                max_length=20,
            ),
        ),
        migrations.AlterField(
            model_name="userclassificationdynamicattrs",
            name="sex",
            field=models.CharField(
                choices=[
                    ("Female", "Female"),
                    ("Male", "Male"),
                    ("Unknown", "Unknown"),
                ],
                default="Unknown",
                max_length=20,
            ),
        ),
        migrations.AddConstraint(
            model_name="classification",
            constraint=models.UniqueConstraint(
                condition=models.Q(base_classification=True),
                fields=("resource", "base_classification"),
                name="unique_base_classification",
            ),
        ),
    ]
