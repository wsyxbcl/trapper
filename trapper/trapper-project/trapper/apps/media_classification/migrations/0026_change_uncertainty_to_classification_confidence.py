from decimal import Decimal

from django.db import migrations


def fill_classification_confidence_data(apps, schema_editor):
    Classificator = apps.get_model("media_classification", "Classificator")
    Classificator.objects.filter(uncertainty=True).update(
        classification_confidence=True
    )
    ClassificationDynamicAttrs = apps.get_model(
        "media_classification", "ClassificationDynamicAttrs"
    )
    ClassificationDynamicAttrs.objects.filter(uncertainty__isnull=False).update(
        classification_confidence=Decimal("0.50")
    )
    UserClassificationDynamicAttrs = apps.get_model(
        "media_classification", "UserClassificationDynamicAttrs"
    )
    UserClassificationDynamicAttrs.objects.filter(uncertainty__isnull=False).update(
        classification_confidence=Decimal("0.50")
    )


class Migration(migrations.Migration):

    dependencies = [
        ("media_classification", "0025_auto_20211026_1503"),
    ]

    operations = [
        migrations.RunPython(fill_classification_confidence_data),
    ]
