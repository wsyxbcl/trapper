# Generated by Django 3.2.5 on 2022-01-05 12:42

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('media_classification', '0035_mark_homo_sapiens_observation_type_human'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aiclassification',
            name='created_at',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='Created at'),
        ),
        migrations.AlterField(
            model_name='aiclassification',
            name='updated_at',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='Updated at'),
        ),
        migrations.AlterField(
            model_name='userclassification',
            name='created_at',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='Created at'),
        ),
        migrations.AlterField(
            model_name='userclassification',
            name='updated_at',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='Updated at'),
        ),
    ]
