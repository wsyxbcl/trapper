# Generated by Django 3.2.5 on 2022-01-13 10:46

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('media_classification', '0052_merge_0036_auto_20220105_1342_0051_auto_20220107_1530'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aiprovider',
            name='minimum_confidence',
            field=models.FloatField(default=0.9, help_text='All observations with confidence below minimum level will be discarded', validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(1)], verbose_name='Minimum confidence'),
        ),
    ]
