# Generated by Django 2.2.17 on 2021-08-04 03:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("media_classification", "0027_auto_20210804_0554"),
    ]

    operations = [
        migrations.AddField(
            model_name="megadetectorbatchprocessingprovider",
            name="azure_storage_account_name",
            field=models.CharField(
                blank=True,
                default=None,
                help_text="Name of Azure Storage Account used for uploading data to Azure Storage. Required only when Azure Storage is used for providing files for MegaDetector",
                max_length=100,
                null=True,
                verbose_name="Azure Storage Account Name",
            ),
        ),
    ]
