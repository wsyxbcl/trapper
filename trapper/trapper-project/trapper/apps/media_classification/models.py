# -*- coding: utf-8 -*-
"""
Module contains models and signals for media classification application.
"""
import base64
import os
import uuid
from collections import OrderedDict
from typing import List

from django import forms
from django.conf import settings
from django.core.cache import cache
from django.core.exceptions import ValidationError, NON_FIELD_ERRORS
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save, post_delete, pre_delete
from django.dispatch import receiver
from django.urls import reverse
from django.utils import timezone
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from polymorphic.managers import PolymorphicManager
from polymorphic.models import PolymorphicModel

from trapper.apps.accounts.models import UserRemoteTask
from trapper.apps.common.fields import SafeTextField
from trapper.apps.common.utils.models import checkbox2select
from trapper.apps.extra_tables.models import Species
from trapper.apps.media_classification.cachekeys import get_form_fields_cache_name
from trapper.apps.media_classification.taxonomy import (
    ClassificatorSettings,
    ClassificationProjectRoleLevels,
    ClassificationProjectStatus,
    ClassificationStatus,
    ObservationType,
    SpeciesAge,
    SpeciesSex,
    SpeciesBehaviour,
)
from trapper.apps.research.models import ResearchProject, ResearchProjectCollection
from trapper.apps.storage.models import (
    Resource,
    Collection,
    collections_access_grant,
    collections_access_revoke,
)
from trapper.middleware import get_current_user


def get_ordered_values(keys_list, d):
    """Return sorted values from dictionary by given list of keys"""
    return [d.get(k) for k in keys_list]


class ClassificationProjectManager(models.Manager):
    """Manager for :class:`ClassificationProject` model.

    This manager contains additional logic used byy DRF serializers like
    details/update/delete urls
    """

    url_update = "media_classification:project_update"
    url_detail = "media_classification:project_detail"
    url_delete = "media_classification:project_delete"

    def api_update_context(self, item, user):
        """
        Method used in DRF api to return update url if user has permissions
        """
        context = None
        if item.can_update(user):
            context = reverse(self.url_update, kwargs={"pk": item.pk})
        return context

    def api_detail_context(self, item, user):
        """
        Method used in DRF api to return detail url if user has permissions
        """
        context = None
        if item.can_view(user):
            context = reverse(self.url_detail, kwargs={"pk": item.pk})
        return context

    def api_delete_context(self, item, user):
        """
        Method used in DRF api to return delete url if user has permissions
        """
        context = None
        if item.can_delete(user):
            context = reverse(self.url_delete, kwargs={"pk": item.pk})
        return context

    def get_accessible(self, user=None, base_queryset=None, role_levels=[1, 2, 3]):
        """Return all :class:`ClassificationProject` instances that given user
        has access to. If user is not defined, then currently logged in user
        is used.

        :param user: if not none then that user will be used to filter
            accessible classification projects. If passed user not logged in,
            then projects are limited to those with `crowdsourcing` enabled.
            If user is None then currently logged in user is used.
        :param base_queryset: queryset used to limit checked projects.
            by default it's all projects.

        :return: classification project queryset
        """

        user = user or get_current_user()
        if not user.is_authenticated:
            return ClassificationProject.objects.none()
        if base_queryset is None:
            queryset = super().get_queryset()
        else:
            queryset = base_queryset
        if user.is_superuser:
            return queryset

        queryset = queryset.filter(
            Q(owner=user)
            | Q(
                pk__in=ClassificationProjectRole.objects.filter(
                    user=user, name__in=role_levels
                ).values("classification_project_id")
            )
        ).distinct()
        return queryset


class ClassificationProject(models.Model):
    """
    Classification projects are containers for resource classifications.
    They are build on top of research project and can contain relation
    to research project collections from various research projects.

    Access to various operations on projects are described by
    :class:`ClassificationProjectRole` objects.

    Classification projects are last step linking resources and
    classifications.
    """

    name = models.CharField(max_length=255, verbose_name=_("Name"))
    research_project = models.ForeignKey(
        ResearchProject,
        related_name="classification_projects",
        on_delete=models.CASCADE,
        verbose_name=_("Research project"),
    )
    collections = models.ManyToManyField(
        ResearchProjectCollection,
        through="ClassificationProjectCollection",
        blank=True,
        related_name="classification_projects",
    )
    classificator = models.ForeignKey(
        "Classificator",
        related_name="classification_projects",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_("Classificator"),
    )
    default_ai_model = models.ForeignKey(
        "AIProvider",
        related_name="classification_projects",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_("Default AI model"),
    )
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)
    date_created = models.DateTimeField(
        auto_now_add=True, verbose_name=_("Date created")
    )
    status = models.IntegerField(
        choices=ClassificationProjectStatus.CHOICES,
        default=ClassificationProjectStatus.ONGOING,
        verbose_name=_("Status"),
    )
    deployment_based_nav = models.BooleanField(
        _("Deployment-based navigation"),
        default=True,
        help_text=_("Enable deployment-based navigation"),
    )
    enable_sequencing = models.BooleanField(
        _("Sequences"), default=True, help_text=_("Enable sequencing interface")
    )
    enable_crowdsourcing = models.BooleanField(
        default=True, help_text=_("Status if crowd-sourcing enabled for the project")
    )

    disabled_at = models.DateTimeField(
        blank=True, null=True, editable=False, verbose_name=_("Disabled at")
    )
    disabled_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        blank=True,
        null=True,
        editable=False,
        on_delete=models.SET_NULL,
    )

    objects = ClassificationProjectManager()

    class Meta:
        ordering = ["-date_created"]
        verbose_name = _("Classification project")
        verbose_name_plural = _("Classification projects")

    def __str__(self):
        return str(self.name)

    def is_finished(self):
        """Check if classification project is marked as finished"""
        return self.status == ClassificationProjectStatus.FINISHED

    def is_active(self):
        """Check if classification project is marked as **not** finished"""
        return self.status == ClassificationProjectStatus.ONGOING

    def get_classification_status(self, as_label=False):
        """Determine if project contains any approved classifications.
        Such project cannot be simply removed (see :func:`delete` for more
        details)

        :param as_label: boolean, if set to True, then status is given
            as label (determined from :class:`ClassificationStatus` choices
            that this model uses

        :return: project status boolean if `as_label` is False else string
        """
        status = self.classifications.filter(
            status=ClassificationStatus.APPROVED
        ).exists()
        if as_label:
            status_labels = ClassificationStatus.choices_as_dict()
            status = status_labels[status]
        return status

    def get_classification_stats(self):
        """
        Get list of resources that are assigned to this project.
        Then prepare stats as numbers:

        * approved - resources with approved classification
        * classified - resources with classifications (including approved
          classifications)
        * unclassified - resources without classification
        * classified_ai - resources with AI classification, approved or not
        * approved_ai - resources with approved AI classifications
        """
        classified_qs = self.classifications.filter(
            user_classifications__isnull=False
        ).distinct()
        approved = classified_qs.filter(
            status=True,
        ).count()
        classified = classified_qs.count()

        classified_ai_qs = self.classifications.filter(
            ai_classifications__isnull=False
        ).distinct()
        approved_ai = classified_ai_qs.filter(status_ai=True).count()
        classified_ai = classified_ai_qs.count()

        unclassified = self.classifications.filter(
            user_classifications__isnull=True, ai_classifications__isnull=True
        ).count()

        return {
            "approved": approved,
            "classified": classified,
            "unclassified": unclassified,
            "classified_ai": classified_ai,
            "approved_ai": approved_ai,
        }

    def get_roles(self):
        """
        Return mapping between users and their roles:

        .. code-block:: json

            {
                <user>: [<role_name>, <role_name>, ],
                <user>: [<role_name>, <role_name>, ],
            }
        """
        role_map = {}
        roles = self.classification_project_roles.all()
        for role in roles:
            role_map.setdefault(role.user, []).append(role)
        return role_map

    def get_user_roles_with_profiles(self):
        return self.classification_project_roles.all().select_related(
            "user", "user__userprofile"
        )

    @property
    def classificator_removed(self):
        """Check if project has attached classificator.

        Not having classificator has strong influence on what can be done with
        project
        """
        return not self.classificator

    def is_project_admin(self, user=None):
        """Determine if given user has enough permissions to be marked as
        Project Admin (PA)

        If no user is given, then currently logged in is used.
        Anonymous users cannot be admins

        Value is stored on instance cache to prevent recalculating it in the
        same request multiple times
        """
        if user.is_superuser:
            return True
        is_project_admin = getattr(self, "_is_project_admin", None)
        if is_project_admin is None:
            user = user or get_current_user()
            if user.is_authenticated:
                is_project_admin = (
                    self.owner == user
                    or self.classification_project_roles.filter(
                        user=user, name=ClassificationProjectRoleLevels.ADMIN
                    ).exists()
                )
            else:
                is_project_admin = False
            self._is_project_admin = is_project_admin
        return is_project_admin

    def is_project_expert(self, user=None):
        """Determine if given user has enough permissions to be marked as
        project expert

        If no user is given, then currently logged in is used.
        Anonymous users cannot be experts
        """
        user = user or get_current_user()

        if user.is_authenticated:
            status = (
                self.owner == user
                or self.classification_project_roles.filter(
                    user=user, name=ClassificationProjectRoleLevels.EXPERT
                ).exists()
            )
        else:
            status = False
        return status

    def get_user_roles(self, user=None):
        """Returns a tuple of project roles for given user.

        :return: list of role names of given user withing the project
        """
        user = user or get_current_user()
        roles = self.classification_project_roles.filter(user=user)
        return [r.get_name_display() for r in roles]

    def can_view(self, user=None):
        """
        Checks if given user has access to details.

        :param user: if passed, permissions will be checked against given
            user will be used. If no user is given or user is not authenticated
            then access is denied.

        :return: boolean access status
        """
        user = user or get_current_user()
        if user.is_authenticated:
            return (
                user.is_superuser
                or self.owner == user
                or self.classification_project_roles.filter(user=user).exists()
            )
        return False

    def can_update(self, user=None):
        """
        Checks if given user can update project.

        :param user: if passed, permissions will be checked against given
            user will be used. If no user is given or user is not authenticated
            then access is denied.

        :return: boolean access status
        """
        user = user or get_current_user()
        if user.is_authenticated:
            return (
                user.is_superuser
                or self.owner == user
                or self.classification_project_roles.filter(
                    user=user, name__in=ClassificationProjectRoleLevels.UPDATE
                ).exists()
            )
        return False

    def can_delete(self, user=None):
        """
        Classification project can be deleted only:
        * by owner
        * by project admin user

        If project has approved classifictions, then it's not really
        deleted, but marked as disabled

        :param user: User instance
        :return: Boolean with access status
        """
        user = user or get_current_user()
        if user.is_authenticated:
            return (
                user.is_superuser
                or self.owner == user
                or self.classification_project_roles.filter(
                    user=user, name__in=ClassificationProjectRoleLevels.DELETE
                ).exists()
            )
        return False

    def can_change_sequence(self, user=None):
        """
        Checks if given user can update sequence in project.

        :param user: if passed, permissions will be checked against given
            user will be used. If no user is given or user is not authenticated
            then access is denied.

        :return: boolean access status
        """
        user = user or get_current_user()
        return self.is_sequencing_enabled(user) and self.can_view(user=user)

    def is_sequencing_enabled(self, user=None):
        """Determine classify sequence box should be visible or not"""
        user = user or get_current_user()
        return self.enable_sequencing or self.is_project_admin(user=user)

    def can_view_classifications(self, user=None):
        """
        Checks if given user can view classifications made within project.

        :param user: if passed, permissions will be checked against given
            user will be used. If no user is given or user is not authenticated
            then access is denied.

        :return: boolean access status
        """
        user = user or get_current_user()
        return (
            user.is_superuser
            or self.owner == user
            or self.classification_project_roles.filter(
                user=user, name__in=ClassificationProjectRoleLevels.VIEW_CLASSIFICATIONS
            ).exists()
        )

    def can_view_ai_classifications(self, user=None):
        """
        Checks if given user can view AI classifications made within project.

        :param user: if passed, permissions will be checked against given
            user will be used. If no user is given or user is not authenticated
            then access is denied.

        :return: boolean access status
        """
        user = user or get_current_user()
        return (
            user.is_superuser
            or self.owner == user
            or self.classification_project_roles.filter(
                user=user,
                name__in=ClassificationProjectRoleLevels.VIEW_AI_CLASSIFICATIONS,
            ).exists()
        )

    def get_absolute_url(self):
        """Get the absolute url for an instance of this model."""
        return reverse("media_classification:project_detail", kwargs={"pk": self.pk})

    def delete(self, *args, **kwargs):
        """
        If project has at least one classification that has been approved,
        then removing it could resolve in significant data and work lost.

        That's why before classification project removal, test is made
        to determine if project can be safely removed.

        If project cannot be safely removed, then `disabled_at` is set to
        current datetime, and `disabled_by` is set to user that is
        trying to remove project.

        If project can be safely removed, then standard logic is called
        and object is removed from database
        """
        if self.get_classification_status():
            user = get_current_user()
            self.disabled_at = now()
            self.disabled_by = user
            self.save()
        else:
            super().delete(*args, **kwargs)


class ClassificationProjectRole(models.Model):
    """
    Model describing the user's role withing given
    :class:`ClassificationProject`
    """

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="classification_project_roles",
        help_text=_("User for which the role is defined"),
        on_delete=models.CASCADE,
    )
    date_created = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_("Date created")
    )
    name = models.IntegerField(
        choices=ClassificationProjectRoleLevels.CHOICES, verbose_name=_("Name")
    )

    classification_project = models.ForeignKey(
        ClassificationProject,
        related_name="classification_project_roles",
        help_text=_("Project for which the role is defined"),
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"Project:{self.classification_project_id} | User:{self.user.username} | Role:{self.name}"

    class Meta:
        ordering = ["user", "name"]
        unique_together = ("classification_project", "user")
        verbose_name = _("Classification project role")
        verbose_name_plural = _("Classification project roles")


class ClassificationProjectCollectionManager(models.Manager):
    """Manager for :class:`ClassificationProjectCollection` model.

    This manager contains additional logic used byy DRF serializers like
    details/update/delete/classify urls
    """

    url_detail = "storage:collection_detail"
    url_delete = "media_classification:project_collection_delete"
    url_classify = "media_classification:classify_collection"

    def api_detail_context(self, item, user):
        """
        Method used in DRF api to return detail url
        """
        return reverse(self.url_detail, kwargs={"pk": item.collection.pk})

    def api_delete_context(self, item, user):
        """
        Method used in DRF api to return delete url
        """
        return reverse(self.url_delete, kwargs={"pk": item.pk})

    def api_classify_context(self, item, user):
        """
        Method used in DRF api to return classify url
        """
        return reverse(
            self.url_classify,
            kwargs={"project_pk": item.project.pk, "collection_pk": item.pk},
        )


class ClassificationProjectCollection(models.Model):
    """model describes relation between :class:`ClassificationProject`
    and :class:`ClassificationProjectCollection` relationship."""

    project = models.ForeignKey(
        ClassificationProject,
        related_name="classification_project_collections",
        on_delete=models.CASCADE,
    )
    collection = models.ForeignKey(ResearchProjectCollection, on_delete=models.PROTECT)
    is_active = models.BooleanField(_("Active"), default=True)

    enable_sequencing_experts = models.BooleanField(
        _("Sequences editable by experts"),
        default=True,
        help_text=_("Allow experts to create and edit sequences"),
    )

    enable_crowdsourcing = models.BooleanField(
        default=True, help_text=_("Status if crowd-sourcing enabled for the project")
    )

    objects = ClassificationProjectCollectionManager()

    def __str__(self):
        return str("%s" % self.collection)

    def get_resources(self):
        """Returns all resources connected to this collection"""
        return self.collection.collection.resources.all()

    def get_unique_locations(self):
        """Returns all unique locations connected to this collection"""
        return set(
            self.collection.collection.resources.values_list(
                "deployment__location__pk", "deployment__location__location_id"
            )
        )

    def can_delete(self, user=None):
        """
        ClassificationProjectCollection can be deleted when user has
        permissions to change ClassificationProject
        """
        user = user or get_current_user()
        return self.project.can_update(user=user)

    def can_view(self, user=None):
        """
        ClassificationProjectCollection can be accessed when user has
        permissions to view :class:`apps.storage.models.Collection`
        """
        user = user or get_current_user()
        return self.collection.collection.can_view(user=user)

    def can_update(self, user=None):
        """
        ClassificationProjectCollection can be updated when user has
        permissions to change :class:`apps.storage.models.Collection`
        """
        user = user or get_current_user()
        return self.collection.collection.can_update(user=user)

    def can_classify(self, user=None):
        """
        ClassificationProjectCollection can be used in classification when
        user has permissions to view ClassificationProject and
        project has assignec :class:`Classificator`
        """
        user = user or get_current_user()
        return self.project.can_view(user=user) and self.project.classificator

    def get_resources_classification_status(self, resources):
        """Return list of resources list with information about
        their classification status"""
        status_list = dict(
            Classification.objects.filter(
                project__pk=self.project.pk, resource__in=resources
            ).values_list("resource__pk", "status")
        )

        return [
            (resource, status_list.get(resource.pk, ClassificationStatus.REJECTED))
            for resource in resources
        ]

    def is_sequencing_enabled(self, user=None):
        """
        Determine if user can use sequencing interface buttons for given
        collection.
        This is especially useful when project has sequencing enabled
        but user has only expert role in project, so by using
        collection params, this interface can be disabled for experts.

        Value is stored on instance cache to prevent recalculating it in the
        same request multiple times
        """
        sequencing_enabled = getattr(self, "_sequencing_enabled", None)
        if sequencing_enabled is None:
            user = user or get_current_user()
            project = self.project
            admin = project.is_project_admin(user=user)
            sequencing_enabled = admin or (
                project.enable_sequencing
                and project.is_project_expert(user=user)
                and self.enable_sequencing_experts
            )
            self._sequencing_enabled = sequencing_enabled
        return sequencing_enabled

    def rebuild_classifications(self):
        """
        This function is used by various signals to assure that classifications
        objects related to this classification project collection map to its
        resources.
        """
        collection_resources = self.collection.collection.resources.values_list(
            "pk", flat=True
        )
        classification_resources = self.classifications.values_list(
            "resource__pk", flat=True
        )
        diff_pks = set(collection_resources).difference(classification_resources)
        # bulk create of missing classifications objects
        insert_list = []
        for pk in diff_pks:
            obj = Classification(
                resource_id=pk,
                collection=self,
                project_id=self.project_id,
                created_at=now(),
                status=ClassificationStatus.REJECTED,
                updated_at=now(),
            )
            insert_list.append(obj)
        Classification.objects.bulk_create(insert_list)

    @property
    def resources_count(self):
        return self.collection.resources_count

    @property
    def collection_storage(self):
        return self.collection.collection_storage

    class Meta:
        verbose_name = _("Classification projects collection")
        verbose_name_plural = _("Classification projects collections")
        ordering = ("-id",)


class ClassificatorManager(models.Manager):
    """
    Manager for :class:`Classificator` model. This manager contains additional
    logic used by DRF serializers like details/update/delete urls.
    """

    url_update = "media_classification:classificator_update"
    url_detail = "media_classification:classificator_detail"
    url_delete = "media_classification:classificator_delete"

    def api_update_context(self, item, user):
        """
        Method used in DRF api to return update url if user has required
        permissions.
        """
        context = None
        if item.can_update(user):
            context = reverse(self.url_update, kwargs={"pk": item.pk})
        return context

    def api_detail_context(self, item, user):
        """
        Method used in DRF api to return detail url if user has required
        permissions.
        """
        context = None
        if item.can_view(user):
            context = reverse(self.url_detail, kwargs={"pk": item.pk})
        return context

    def api_delete_context(self, item, user):
        """
        Method used in DRF api to return delete url if user has required
        permissions.
        """
        context = None
        if item.can_delete(user):
            context = reverse(self.url_delete, kwargs={"pk": item.pk})
        return context

    def get_accessible(self, user=None):
        """
        Authenticated users can see all non-deleted classificators.
        """
        user = user or get_current_user()
        if user.is_authenticated:
            queryset = self.get_queryset()
        else:
            queryset = self.none()
        return queryset


class Classificator(models.Model):
    """
    Stores definition of a set of attributes describing given classification form.
    This attributes-set is defined per classification project.
    """

    SEPARATOR = ","
    CLONE_PATTERN = _("Copy_of_{name}_{tstamp}")

    name = models.CharField(max_length=255, unique=True, verbose_name=_("Name"))
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="user_classificators",
        on_delete=models.CASCADE,
    )
    template = models.CharField(
        max_length=50,
        choices=ClassificatorSettings.TEMPLATE_CHOICES,
        default=ClassificatorSettings.TEMPLATE_INLINE,
    )
    # standard attributes
    # when species field is null then all species are valid choices
    # in the classification form
    species = models.ManyToManyField(Species, blank=True, verbose_name=_("Species"))
    observation_type = models.BooleanField(
        default=True, verbose_name=_("Observation type")
    )
    is_setup = models.BooleanField(default=True, verbose_name=_("Is setup"))
    sex = models.BooleanField(default=False, verbose_name=_("Sex"))
    age = models.BooleanField(default=False, verbose_name=_("Age"))
    count = models.BooleanField(default=True, verbose_name=_("Count"))
    count_new = models.BooleanField(default=False, verbose_name=_("Count new"))
    behaviour = models.BooleanField(default=False, verbose_name=_("Behaviour"))
    individual_id = models.BooleanField(default=False, verbose_name=_("Individual ID"))
    classification_confidence = models.BooleanField(
        default=False, verbose_name=_("Classification confidence")
    )
    # custom and predefined attributes
    custom_attrs = models.JSONField(
        null=True, blank=True, default=dict, verbose_name=_("Custom attrs")
    )
    predefined_attrs = models.JSONField(
        null=True, blank=True, default=dict, verbose_name=_("Predefined attrs")
    )
    # other properties
    dynamic_attrs_order = models.TextField(
        null=True, blank=True, verbose_name=_("Dynamic attrs order")
    )
    static_attrs_order = models.TextField(
        null=True, blank=True, verbose_name=_("Static attrs order")
    )
    updated_date = models.DateTimeField(
        blank=True, auto_now=True, verbose_name=_("Updated date")
    )
    created_date = models.DateTimeField(
        blank=True, auto_now_add=True, verbose_name=_("Created date")
    )
    description = SafeTextField(
        max_length=2000, null=True, blank=True, verbose_name=_("Description")
    )

    objects = ClassificatorManager()

    def __str__(self):
        return self.name

    def save(self, **kwargs):
        self.update_attrs_order()
        super().save(**kwargs)
        # Clear form fields cache
        cache_name = get_form_fields_cache_name(self)
        cache.delete(cache_name)

    def delete(self, *args, **kwargs):
        status = self.get_classification_status()
        if status:
            return False
        else:
            super().delete(*args, **kwargs)

    def get_classification_status(self):
        for project in self.classification_projects.all():
            if project.get_classification_status():
                return True
        return False

    def get_absolute_url(self):
        return reverse(
            "media_classification:classificator_detail", kwargs={"pk": self.pk}
        )

    def can_view(self, user=None):
        return True

    def can_update(self, user=None):
        user = user or get_current_user()
        return self.owner == user or user.is_superuser

    def can_delete(self, user=None):
        user = user or get_current_user()
        return self.owner == user

    def active_standard_attrs(self, attrs_type, required=True):
        """
        Returns a list of active standard attributes. Optionaly, this method
        can return only non-required active standard dynamic attributes.
        The `attrs_type` parameter must be 'STATIC' or 'DYNAMIC'.
        """
        # non-required active attrs
        attrs = getattr(ClassificatorSettings, "STANDARD_ATTRS_OTHER_" + attrs_type)
        attrs = [a for a in attrs if getattr(self, a)]
        if required:
            return list(
                getattr(ClassificatorSettings, "STANDARD_ATTRS_REQUIRED_" + attrs_type)
                + attrs
            )
        return attrs

    @property
    def active_predefined_attrs(self):
        """
        Returns a list of active predefined attributes.
        """
        config = ClassificatorSettings.PREDEFINED_NAMES
        active = []
        for attr in config:
            if self.predefined_attrs.get(attr, None):
                active.append(attr)
        return active

    @property
    def active_custom_attrs(self):
        return list(self.custom_attrs.keys())

    def get_all_attrs_names(self):
        """
        Returns a list of all attributes for this classificator.
        """
        all_items = [[], []]  # [[dynamic_form], [static_form]]
        # add standard required dynamic attrs
        all_items[0].extend(self.active_standard_attrs("DYNAMIC"))
        # add standard required static attrs
        all_items[1].extend(self.active_standard_attrs("STATIC"))
        # add predefined and custom attributes
        for at in self.active_predefined_attrs:
            if self.predefined_attrs[at]["target"] == "D":
                all_items[0].append(at)
            else:
                all_items[1].append(at)
        for at in self.custom_attrs.keys():
            if self.custom_attrs[at]["target"] == "D":
                all_items[0].append(at)
            else:
                all_items[1].append(at)
        return all_items

    def _get_attrs_order(self, data):
        items = []
        if data:
            try:
                items = data.split(self.SEPARATOR)
            except (ValueError, AttributeError):
                pass
        return items

    def get_dynamic_attrs_order(self):
        return self._get_attrs_order(data=self.dynamic_attrs_order)

    def get_static_attrs_order(self):
        return self._get_attrs_order(data=self.static_attrs_order)

    def get_numerical_attrs(self):
        attrs = []
        if self.count:
            attrs.append("count")
        if self.count_new:
            attrs.append("count_new")
        for name, params in self.custom_attrs.items():
            if params["field_type"] == "I":
                attrs.append(name)
        return attrs

    def update_attrs_order(self, commit=False):
        """
        Recalculates attributes order when a classificator is changed.
        """
        all_attrs = self.get_all_attrs_names()
        d_attrs_order = self.get_dynamic_attrs_order()
        s_attrs_order = self.get_static_attrs_order()
        # first append new attributes to the end of the list
        for at in all_attrs[0]:  # dynamic_form
            if at not in d_attrs_order:
                d_attrs_order.append(at)
        for at in all_attrs[1]:  # static_form
            if at not in s_attrs_order:
                s_attrs_order.append(at)
        # then remove missing attributes from the list keeping its order
        d_attrs_order = [a for a in d_attrs_order if a in all_attrs[0]]
        s_attrs_order = [a for a in s_attrs_order if a in all_attrs[1]]
        self.dynamic_attrs_order = ",".join(d_attrs_order)
        self.static_attrs_order = ",".join(s_attrs_order)
        if commit:
            self.save()

    def prepare_predefined_initial(self):
        initial = {}
        attrs = self.predefined_attrs
        for k in attrs.keys():
            initial[k] = True
            for j in attrs[k].keys():
                initial["_".join([k, j])] = attrs[k][j]
        return initial

    def prepare_standard_dynamic_fields(self):
        fields = forms.modelform_factory(
            # using ClassificationDynamicAttrs instead of previously used StandardDynamicAttrsMixin,
            # as abstract models cannot be instantiated in django 3.2, so modelfactory with an abstract class
            # raises TypeError
            ClassificationDynamicAttrs,
            fields=self.active_standard_attrs("DYNAMIC"),
        )().fields
        # update species field
        selected_species = self.species.values_list("pk", "english_name", "latin_name")
        selected_species = [(k[0], f"{k[1]} ({k[2]})") for k in selected_species]
        choices = [("", "---------")]
        if selected_species:
            choices = choices + selected_species
        fields["species"] = forms.ChoiceField(choices=choices, required=False)
        # change all boolean fields to lists
        for field in fields:
            if isinstance(fields[field], forms.BooleanField):
                fields[field] = checkbox2select(fields[field])
        return fields

    def prepare_standard_static_fields(self):
        fields = forms.modelform_factory(
            # using ClassificationDynamicAttrs instead of previously used StandardStaticAttrsMixin,
            # as abstract models cannot be instantiated in django 3.2, so modelfactory with an abstract class
            # raises TypeError
            Classification,
            fields=self.active_standard_attrs("STATIC"),
        )().fields
        # change all boolean fields to lists
        for field in fields:
            if isinstance(fields[field], forms.BooleanField):
                fields[field] = checkbox2select(fields[field])
        return fields

    def prepare_form_fields(self):
        """
        As classificator is just a collection of required and user-defined fields
        these fields are used to generate the classification form. This method is
        responsible for converting a definition of classificator into a structured
        list of form fields.
        """
        cache_name = get_form_fields_cache_name(self)
        form_fields = cache.get(cache_name, settings.CACHE_UNDEFINED)

        if form_fields is settings.CACHE_UNDEFINED:
            form_fields = OrderedDict()
            form_fields["D"] = OrderedDict()  # "D" -> dynamic form
            form_fields["S"] = OrderedDict()  # "S" -> static form

            # 1) add standard dynamic attributes form fields
            standard_fields = self.prepare_standard_dynamic_fields()
            form_fields["D"].update(standard_fields)
            standard_fields = self.prepare_standard_static_fields()
            form_fields["S"].update(standard_fields)

            # 2) other predefined attributes:
            pas = ClassificatorSettings.PREDEFINED_ATTRIBUTES
            for k, v in self.predefined_attrs.items():
                params = {"required": v["required"]}
                widget = pas[k].get("widget", None)
                if widget:
                    params["widget"] = widget
                formfield = pas[k]["formfield"](**params)
                form_fields[v["target"]][k] = formfield

            # 3) custom attributes
            custom_attrs = self.custom_attrs
            # next prepare custom attributes:
            for key in custom_attrs:
                item = custom_attrs[key]
                values_key = item["values"]
                if values_key:
                    # values have been already checked during a form validation
                    # so just split them
                    if not isinstance(values_key, list):
                        splitted_values = values_key.split(",")
                    else:
                        splitted_values = values_key
                    choices = [("", "---------")] + list(
                        zip(splitted_values, splitted_values)
                    )
                    formfield = forms.ChoiceField(
                        choices=choices,
                        required=item["required"],
                        initial=item["initial"],
                    )
                else:
                    fields_key = item["field_type"]
                    formfield = ClassificatorSettings.FIELDS[fields_key](
                        required=item["required"],
                        initial=item["initial"],
                    )
                    if isinstance(formfield, forms.BooleanField):
                        formfield = checkbox2select(formfield)
                form_fields[item["target"]][key] = formfield

            # re-order field forms
            dao = self.get_dynamic_attrs_order()
            form_fields["D"] = OrderedDict((k, form_fields["D"][k]) for k in dao)
            # add hidden field for bboxes
            form_fields["D"]["bboxes"] = forms.CharField(
                widget=forms.HiddenInput(), required=False
            )
            sao = self.get_static_attrs_order()
            form_fields["S"] = OrderedDict((k, form_fields["S"][k]) for k in sao)
            cache.set(cache_name, form_fields, settings.CACHE_TIMEOUT)
        return form_fields

    def remove_custom_attr(self, name, commit=False):
        """
        Remove given attribute from a custom attributes list.
        """
        del self.custom_attrs[name]
        self.update_attrs_order()
        if commit:
            self.save()

    def set_custom_attr(self, name, params, commit=False):
        """
        Set custom attribute with provided parameters.
        """
        self.custom_attrs[name] = params
        self.update_attrs_order()
        if commit:
            self.save()

    def set_predefined_attrs(self, predefined_data, commit=False):
        """
        Set predefined attributes with provided data.
        """
        self.predefined_attrs = {}
        all_predefined = ClassificatorSettings.PREDEFINED_ATTRIBUTES
        for attr in all_predefined.keys():
            if predefined_data.get(attr, None):
                self.predefined_attrs[attr] = {}
                for p in ["required", "target"]:
                    p_name = "_".join([attr, p])
                    self.predefined_attrs[attr][p] = predefined_data[p_name]
        self.update_attrs_order()
        if commit:
            self.save()

    def get_custom_attrs_schema(self):
        attrs = self.custom_attrs
        attrs_types = ClassificatorSettings.TYPES_FRICTIONLESS
        attrs_defs = []
        for k, v in attrs.items():
            item_def = {
                "name": k,
                "type": attrs_types.get(v.get("field_type")),
                "format": "default",
                "description": v.get("description"),
                "constraints": {"required": v.get("required")},
            }
            values = v.get("values")
            if values:
                values = values.split(",")
                item_def["constraints"]["enum"] = values
            attrs_defs.append(item_def)
        return attrs_defs

    class Meta:
        verbose_name = _("Classificator")
        verbose_name_plural = _("Classificators")
        ordering = ("-created_date",)


class ClassificationManager(models.Manager):
    """Manager for :class:`Classification` model.

    This manager contains additional logic used by DRF serializers
    """

    url_detail = "media_classification:classify"
    url_delete = "media_classification:classification_delete"

    def get_accessible(self, user=None, base_queryset=None, role_levels=None):
        """Return all :class:`Classification` instances that given user
        has access to. If user is not defined, then currently logged in user
        is used.
        If there is no authenticated user, then empty queryset is returned

        :param user: if not none then that user will be used to filter
            accessible classifications. If passed user not logged in,
            then empty queryset is returned
            If user is None then currently logged in user is used.
        :param base_queryset: queryset used to limit checked classifications.
            by default it's all classifications.
        :param role_levels:

        :return: classifications queryset
        """
        user = user or get_current_user()

        if base_queryset is None:
            queryset = self.get_queryset()
        else:
            queryset = base_queryset

        if not user.is_authenticated:
            return queryset.none()
        if user.is_superuser:
            return queryset
        else:
            levels = role_levels or ClassificationProjectRoleLevels.VIEW_CLASSIFICATIONS
            cprojects = list(
                user.classification_project_roles.filter(name__in=levels).values_list(
                    "classification_project_id", flat=True
                )
            )
            qs = queryset.exclude(
                ~models.Q(project__owner=user), ~models.Q(project__pk__in=cprojects)
            )
        return qs

    def api_detail_context(self, item, user):
        """
        Method used in DRF api to return detail url if user has permissions
        """
        context = None
        if item.can_view(user):
            context = reverse(self.url_detail, kwargs={"pk": item.pk})
        return context

    def api_delete_context(self, item, user):
        """
        Method used in DRF api to return delete url if user has permissions
        """
        context = None
        if item.can_delete(user):
            context = reverse(self.url_delete, kwargs={"pk": item.pk})
        return context


# Mixin abstract class for static attrs providing standard properties
class StandardStaticAttrsMixin(models.Model):
    """
    Each :class:`Classification` and :class:`UserClassification` object can
    contain multiple static attributes which are stored in models inheriting
    from this mixin class. These static attributes are:

    1) Predefined set of standard attributes describing each resource: `is_setup`.
    These attributes are common to all classification projects.

    2) Custom additional attributes (`static_attrs`) as defined in
    :class:`Classificator` model.
    """

    is_setup = models.BooleanField(default=False)
    has_bboxes = models.BooleanField(default=False)
    # other custom attributes as defined in the classificator
    static_attrs = models.JSONField(
        null=True, blank=True, default=dict, verbose_name=_("Static attrs")
    )

    class Meta:
        abstract = True


# Mixin abstract class for dynamic attrs providing standard properties
class StandardDynamicAttrsMixin(models.Model):
    """
    Each :class:`Classification` and :class:`UserClassification` object can contain
    multiple dynamic attributes ("rows") which are stored in models inheriting from
    this mixin class. These dynamic attributes are:

    1) Predefined set of standard attributes describing detected individuals of given
    species: `species`, `age`, `sex`, `count`, `count_new`, `individual_id`, `classification_confidence`
    and `bboxes`. These attributes are common to all classification projects.

    2) Custom additional attributes (`attrs`) as defined in :class:`Classificator` model.
    """

    observation_type = models.CharField(choices=ObservationType.CHOICES, max_length=20)
    species = models.ForeignKey(
        Species, on_delete=models.SET_NULL, null=True, blank=True
    )
    sex = models.CharField(
        choices=SpeciesSex.CHOICES, default=SpeciesSex.UNDEFINED, max_length=20
    )
    age = models.CharField(
        choices=SpeciesAge.CHOICES, default=SpeciesAge.UNDEFINED, max_length=20
    )
    behaviour = models.CharField(
        choices=SpeciesBehaviour.CHOICES,
        default=SpeciesBehaviour.UNDEFINED,
        max_length=20,
    )
    count = models.IntegerField(default=1)
    count_new = models.IntegerField(null=True, blank=True)
    # optional attribute for "marked" species (human's classification)
    individual_id = models.CharField(null=True, blank=True, max_length=50)
    # confidence of this classification as a number from 0 to 1
    classification_confidence = models.DecimalField(
        null=True,
        blank=True,
        decimal_places=2,
        max_digits=3,
        validators=[MinValueValidator(0), MaxValueValidator(1)],
    )
    # 2D array for images and 3D array for videos (1 bbox per frame or second per object)
    bboxes = models.JSONField(null=True, blank=True, default=list)
    # other custom attributes as defined in the classificator
    attrs = models.JSONField(
        null=True, blank=True, default=dict, verbose_name=_("Attrs")
    )

    class Meta:
        abstract = True


class Classification(StandardStaticAttrsMixin):
    """
    This is a destination place for all work that is done with resources.
    Classification is always related to :class:`ClassificationProject` and
    single :class:`apps.storage.models.Resource`. The classification form
    displayed to a user is defined by :class:`Classificator`.
    """

    resource = models.ForeignKey(
        Resource, related_name="classifications", on_delete=models.CASCADE
    )
    base_classification = models.BooleanField(default=False)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="classifications_created",
        on_delete=models.SET_NULL,
    )
    created_at = models.DateTimeField(auto_created=True, verbose_name=_("Created at"))
    project = models.ForeignKey(
        ClassificationProject, related_name="classifications", on_delete=models.CASCADE
    )
    collection = models.ForeignKey(
        ClassificationProjectCollection,
        related_name="classifications",
        on_delete=models.CASCADE,
    )
    sequence = models.ForeignKey(
        "Sequence",
        null=True,
        blank=True,
        related_name="classifications",
        on_delete=models.SET_NULL,
    )
    status = models.BooleanField(default=False, verbose_name=_("Status"))
    status_ai = models.BooleanField(default=False, verbose_name=_("Status AI"))
    has_initial_data = models.BooleanField(
        default=False, verbose_name=_("Has initial data")
    )
    approved_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="classifications_approved",
        on_delete=models.SET_NULL,
    )
    approved_ai_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="aiclassifications_approved",
        on_delete=models.SET_NULL,
    )
    approved_at = models.DateTimeField(
        null=True, blank=True, verbose_name=_("Approved at")
    )
    approved_ai_at = models.DateTimeField(
        null=True, blank=True, verbose_name=_("Approved AI at")
    )
    approved_source = models.OneToOneField(
        "UserClassification",
        null=True,
        blank=True,
        related_name="userclassification_approved",
        on_delete=models.SET_NULL,
    )
    approved_source_ai = models.OneToOneField(
        "AIClassification",
        null=True,
        blank=True,
        related_name="aiclassification_approved",
        on_delete=models.SET_NULL,
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name="classifications_updated",
        on_delete=models.SET_NULL,
    )
    updated_at = models.DateTimeField(
        null=True, blank=True, verbose_name=_("Updated at")
    )

    objects = ClassificationManager()

    def __str__(self):
        return str("Classification: %s" % self.pk)

    @property
    def classificator(self):
        """Return classificator assigned to classification"""
        return self.project.classificator

    @property
    def is_approved(self):
        """Return True if classification is approved, False otherwise"""
        return self.status == ClassificationStatus.APPROVED

    @property
    def is_rejected(self):
        """Return True if classification is rejected, False otherwise"""
        return self.status == ClassificationStatus.REJECTED

    def get_user_classifications(self):
        """Return all user classifications that are connected to this
        classification.
        This method can be used to determine how often given resource
        was classified by users"""
        return self.user_classifications.all().order_by("updated_at")

    def can_view(self, user=None):
        """Check if given user (or currently logged in) can view details
        of classification.

        By default persons that have access to project can view classifications
        done within that project.
        """
        user = user or get_current_user()
        return self.project.can_view(user=user)

    def can_update(self, user=None):
        """Check if given user (or currently logged in) can update given
        classification

        By default Owner and project admin can update classification.
        """
        user = user or get_current_user()
        return self.owner == user or self.project.is_project_admin(user=user)

    def can_approve(self, user=None):
        """Check if given user (or currently logged in) can approve
        classification using classification box in classification details
        (this could be done from different classification details)

        By default only project admin can approve classification
        """
        user = user or get_current_user()
        return self.project.is_project_admin(user=user)

    def can_delete(self, user=None):
        """Check if given user (or currently logged in) can delete
        classification

        By default only project admin can approve classification
        """
        user = user or get_current_user()
        return self.project.is_project_admin(user=user)

    def delete(self, clear=True, *args, **kwargs):
        """If `clear` is True just clear all classification's data;
        do not delete the object itself."""
        if clear:
            user = get_current_user()
            self.is_setup = False
            self.has_bboxes = False
            self.static_attrs = {}
            self.dynamic_attrs.all().delete()
            self.status = False
            self.approved_by = None
            self.approved_at = None
            self.approved_source = None
            self.updated_at = now()
            self.updated_by = user
            self.save()
        else:
            super().delete(*args, **kwargs)

    def approve_user_classification(
        self, user_classification, classificator=None, user=None, commit=True
    ):
        """"""
        user = user or get_current_user()
        classificator = classificator or self.project.classificator
        static_standard_fields = classificator.active_standard_attrs("STATIC")
        dynamic_standard_fields = classificator.active_standard_attrs("DYNAMIC")
        self.has_initial_data = True
        self.status = True
        self.approved_by = user
        self.approved_at = now()
        self.approved_source = user_classification
        self.updated_at = now()
        self.updated_by = user
        self.has_bboxes = user_classification.has_bboxes
        # static attributes
        for attr_name in static_standard_fields:
            attr_value = getattr(user_classification, attr_name)
            setattr(self, attr_name, attr_value)
        self.static_attrs = user_classification.static_attrs
        if commit:
            self.save()
            # dynamic attributes
            self.dynamic_attrs.all().delete()
        if not commit:
            dattrs_list = []
        for user_dattrs in user_classification.dynamic_attrs.all():
            class_dattrs = ClassificationDynamicAttrs(classification=self)
            for attr_name in dynamic_standard_fields:
                if attr_name == "species":
                    class_dattrs.species_id = user_dattrs.species_id
                else:
                    attr_value = getattr(user_dattrs, attr_name)
                    setattr(class_dattrs, attr_name, attr_value)
            class_dattrs.attrs = user_dattrs.attrs
            class_dattrs.bboxes = user_dattrs.bboxes
            if commit:
                class_dattrs.save()
            else:
                dattrs_list.append(class_dattrs)
        if not commit:
            return dattrs_list

    @property
    def classification_method(self):
        if getattr(self, "approved_source_ai", None) is not None:
            return "machine"
        return "human"

    @property
    def classified_by(self):
        ai_classificator = getattr(self, "approved_source_ai", None)
        if ai_classificator is not None:
            return ai_classificator.name
        else:
            human_classification = getattr(self, "approved_source")
            owner = human_classification.owner
            return f"{owner.first_name} {owner.last_name}"

    def approve_ai_classification(
        self,
        fields_to_copy: List[str],
        minimum_confidence: float,
        mark_as_approved: bool,
        overwrite_attrs: bool,
        ai_classification: "AIClassification",
        classificator=None,
        user=None,
        commit=True,
    ):
        """"""
        user = user or get_current_user()
        classificator = classificator or self.project.classificator
        static_standard_fields = classificator.active_standard_attrs("STATIC")
        dynamic_standard_fields = classificator.active_standard_attrs("DYNAMIC") + [
            "classification_confidence"
        ]

        self.has_initial_data = overwrite_attrs
        if mark_as_approved:
            self.status_ai = True
            self.approved_ai_by = user
            self.approved_ai_at = timezone.now()
            self.approved_source_ai = ai_classification

        self.updated_at = timezone.now()
        self.updated_by = user

        if overwrite_attrs:
            if "bounding_boxes" in fields_to_copy:
                self.has_bboxes = ai_classification.has_bboxes

            # static attributes
            # we are skipping non-standard static attributes
            for attr_name in static_standard_fields:
                if attr_name not in fields_to_copy:
                    continue
                attr_value = getattr(ai_classification, attr_name)
                setattr(self, attr_name, attr_value)

            if commit:
                self.save()
                # dynamic attributes
                self.dynamic_attrs.all().delete()

            dattrs_list = []
            for ai_dattrs in ai_classification.dynamic_attrs.all():
                # skip observations for which confidence is too low; do not apply this test
                # to blank observations as they have a constant confidence value
                if (
                    ai_dattrs.observation_type != ObservationType.BLANK
                    and ai_dattrs.classification_confidence < minimum_confidence
                ):
                    continue

                class_dattrs = ClassificationDynamicAttrs(classification=self)
                for attr_name in dynamic_standard_fields:
                    if attr_name not in fields_to_copy:
                        continue
                    if attr_name == "species":
                        class_dattrs.species_id = ai_dattrs.species_id
                    else:
                        attr_value = getattr(ai_dattrs, attr_name)
                        setattr(class_dattrs, attr_name, attr_value)

                if "bounding_boxes" in fields_to_copy:
                    class_dattrs.bboxes = ai_dattrs.bboxes

                dattrs_list.append(class_dattrs)

            if commit:
                ClassificationDynamicAttrs.objects.bulk_create(dattrs_list)
            else:
                return dattrs_list
        else:  # overwrite_attrs = False
            if commit:
                self.save()
            return []

    def get_bboxes(self):
        "Returns the bounding boxes of all related observations"
        bboxes = []
        for i, dattr in enumerate(self.dynamic_attrs.all()):
            bboxes.append({"label": f"bbox{i + 1}", "bboxes": dattr.bboxes})
        return bboxes

    def clear_ai_classification_attributes(self):
        """
        Clears all attributes that were set by approving AI classification.
        """
        self.status_ai = False
        self.approved_ai_by = None
        self.approved_ai_at = None
        self.save()

    class Meta:
        ordering = ("resource__date_recorded",)
        verbose_name = _("Classification")
        verbose_name_plural = _("Classifications")
        constraints = [
            models.UniqueConstraint(
                fields=["resource", "base_classification"],
                condition=Q(base_classification=True),
                name="unique_base_classification",
            )
        ]


class ClassificationDynamicAttrs(StandardDynamicAttrsMixin):
    """
    See :class:`StandardDynamicAttrsMixin` for details.
    """

    classification = models.ForeignKey(
        Classification, related_name="dynamic_attrs", on_delete=models.CASCADE
    )

    def __str__(self):
        return (
            f"Classification:{self.classification.pk}|"
            f"Observation:{self.observation_type}"
        )

    class Meta:
        verbose_name = _("Classification dynamic attribute")
        verbose_name_plural = _("Classifications dynamic attributes")


class UserClassificationManager(models.Manager):
    """
    Manager for :class:`UserClassification` model. This manager contains additional logic
    used by DRF serializers.
    """

    url_detail = "media_classification:classify_user"
    url_delete = "media_classification:user_classification_delete"

    def get_accessible(self, user=None, base_queryset=None, role_levels=None):
        user = user or get_current_user()
        if base_queryset is None:
            queryset = self.get_queryset()
        else:
            queryset = base_queryset
        if not user.is_authenticated:
            return queryset.none()
        if user.is_superuser:
            return queryset
        else:
            levels = (
                role_levels or ClassificationProjectRoleLevels.VIEW_USER_CLASSIFICATIONS
            )
            cprojects = set(
                user.classification_project_roles.filter(name__in=levels).values_list(
                    "classification_project_id", flat=True
                )
            )
            qs = queryset.exclude(
                ~models.Q(classification__project__owner=user),
                ~models.Q(classification__project__in=cprojects),
            )
        return qs

    def api_detail_context(self, item, user):
        """
        Method used in DRF api to return detail url if user has permissions
        """
        context = None
        if item.can_view(user):
            context = reverse(
                self.url_detail,
                kwargs={"pk": item.classification.pk, "user_pk": item.owner.pk},
            )
        return context

    def api_delete_context(self, item, user):
        """
        Method used in DRF api to return delete url if user has permissions
        """
        context = None
        if item.can_delete(user):
            context = reverse(self.url_delete, kwargs={"pk": item.pk})
        return context


class UserClassification(StandardStaticAttrsMixin):
    """
    Model to store classifications made by humans. When project's admin approves selected
    :class:`UserClassification`, its data is transferred into :class:`Classification` object.
    """

    classification = models.ForeignKey(
        Classification, related_name="user_classifications", on_delete=models.CASCADE
    )
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="user_classifications",
        on_delete=models.PROTECT,
    )
    created_at = models.DateTimeField(default=now, verbose_name=_("Created at"))
    updated_at = models.DateTimeField(default=now, verbose_name=_("Updated at"))

    objects = UserClassificationManager()

    class Meta:
        ordering = ("classification__resource__date_recorded",)

    def __str__(self):
        return f"User classification: {self.pk}"

    @property
    def classificator(self):
        """Return classificator assigned to classification"""
        return self.classification.classificator

    def get_static_values(self):
        """Return ordered static values assigned to classificator"""
        predefined_order = self.classification.classificator.get_static_attrs_order()
        return get_ordered_values(predefined_order, self.static_attrs)

    def get_custom_values(self):
        """Return ordered static values assigned to classificator"""
        values_list = []
        custom_order = self.classification.classificator.get_dynamic_attrs_order()
        for row in self.custom_attrs.values_list("attrs", flat=True):
            values_list.append(get_ordered_values(custom_order, row))
        return values_list

    def get_bboxes(self):
        "Returns the bounding boxes of all related observations"
        bboxes = []
        for i, dattr in enumerate(self.dynamic_attrs.all()):
            bboxes.append({"label": f"bbox{i + 1}", "bboxes": dattr.bboxes})
        return bboxes

    def can_view(self, user=None):
        """Check if given user (or currently logged in) can view details
        of classification.

        By default persons that have access to project can view classifications
        done within that project.
        """
        user = user or get_current_user()
        return self.classification.project.can_view(user=user)

    def can_update(self, user=None):
        """Check if given user (or currently logged in) can update given
        classification

        By default Owner and project admin can update classification.
        """
        user = user or get_current_user()
        return self.owner == user or self.classification.project.is_project_admin(
            user=user
        )

    def can_delete(self, user=None):
        return self.can_update(user=user)


class UserClassificationDynamicAttrs(StandardDynamicAttrsMixin):
    """
    See :class:`StandardDynamicAttrsMixin` for details.

    When given :class:`UserClassification` is approved, dynamic attributes from this
    model are transferred into :class:`ClassificationDynamicAttrs`.
    """

    userclassification = models.ForeignKey(
        UserClassification, related_name="dynamic_attrs", on_delete=models.CASCADE
    )

    def __str__(self):
        return (
            f"UserClassification:{self.userclassification.pk}|"
            f"Observation:{self.observation_type}"
        )


def generate_token():
    return base64.b32encode(os.urandom(20)).decode()


class AIProviderManager(PolymorphicManager):
    """
    Manager for class AIProvider.
    Contains get_accessible method, which returns all AIProvider objects available to user, based on
    existing AIProviderPermission objects.
    """

    def get_accessible(self, user=None):
        user = user or get_current_user()
        if not user.is_authenticated:
            return self.none()

        if user.is_superuser:
            return AIProvider.objects.all()
        return AIProvider.objects.filter(user_permissions__user=user)


class AIProvider(PolymorphicModel):
    """
    Configuration of available AI models (only for system admins).
    """

    name = models.CharField(max_length=100, verbose_name=_("Name"))
    description = models.TextField(
        max_length=1000, null=True, blank=True, verbose_name=_("Description")
    )
    video_support = models.BooleanField(default=False, verbose_name=_("Video support"))
    object_based = models.BooleanField(default=True, verbose_name=_("Object based"))

    minimum_confidence = models.FloatField(
        default=0.9,
        validators=[MinValueValidator(0), MaxValueValidator(1)],
        verbose_name=_("Minimum confidence"),
        help_text=_(
            "All observations with confidence below minimum level will be discarded"
        ),
    )

    skip_empty = models.BooleanField(
        default=False,
        verbose_name=_("Skip empty"),
        help_text=_(
            "If selected, resources without any detected observations will be discarded "
            "(AI Classification objects will not be created)."
        ),
    )

    trapper_instance_url = models.URLField(
        blank=True,
        default="",
        verbose_name=_("Trapper Instance URL"),
        help_text=_("Base URL of Trapper server as visible by AI Provider"),
    )

    token = models.TextField(
        db_index=True,
        editable=False,
        verbose_name=_("Token"),
        help_text="Token for usage in callback URLs",
        default=generate_token,
    )
    objects = AIProviderManager()

    def __str__(self):
        return "AI provider: %s" % (self.name)

    def get_labels(self):
        labels = self.labels.all()
        if labels:
            return sorted([(k.value, k.get_label()) for k in labels])
        return []


class AIClassificationJob(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    user_remote_task = models.OneToOneField(UserRemoteTask, on_delete=models.CASCADE)
    ai_provider = models.ForeignKey(AIProvider, on_delete=models.CASCADE)
    stage = models.CharField(
        max_length=256,
        help_text=_("Stage name used internaly by remote task execution routines"),
        blank=True,
        null=True,
        default=None,
    )

    class Meta:
        ordering = ["-date_created"]


class TrapperAIProvider(AIProvider):
    api_url = models.URLField(verbose_name=_("Api URL"))
    api_auth_login = models.CharField(max_length=100, verbose_name=_("Api auth login"))
    api_auth_passw = models.CharField(max_length=100, verbose_name=_("Api auth passw"))
    ai_model_id = models.UUIDField(
        null=True, blank=True, verbose_name=_("Unique ID of model from TrapperAI")
    )

    conf = models.JSONField(null=True, blank=True, verbose_name=_("Conf"))

    def __str__(self):
        return "Trapper AI provider: %s" % (self.name)


class MegadetectorBatchProcessingProvider(AIProvider):
    api_url = models.URLField(verbose_name=_("Api URL"))
    model_version = models.CharField(
        max_length=100,
        verbose_name=_("Model version"),
        help_text=_("Version of the MegaDetector model to use"),
        blank=True,
        null=True,
        default=None,
    )
    caller = models.CharField(
        max_length=100,
        verbose_name=_("Caller"),
        help_text=_("An identifier used to whitelist users in Megadetector API"),
        blank=True,
        null=True,
        default=None,
    )

    azure_storage_account_name = models.CharField(
        max_length=100,
        verbose_name=_("Azure Storage Account Name"),
        help_text=_(
            "Name of Azure Storage Account used for uploading data to Azure Storage. "
            "Required only when Azure Storage is used for providing files for MegaDetector"
        ),
        blank=True,
        null=True,
        default=None,
    )

    azure_storage_account_access_key = models.TextField(
        verbose_name=_("Azure Storage Account Access Key"),
        help_text=_(
            "Azure Storage Account Access Key used for uploading data to Azure Storage. "
            "Required only when Azure Storage is used for providing files for MegaDetector"
        ),
        blank=True,
        null=True,
        default=None,
    )

    azure_storage_container = models.CharField(
        max_length=100,
        verbose_name=_("Azure Storage Container"),
        help_text=_(
            "Name of Azure Storage container used for uploading data to Azure Storage. "
            "Required only when Azure Storage is used for providing files for MegaDetector"
        ),
        blank=True,
        null=True,
        default=None,
    )

    use_azure_storage_for_image_list = models.BooleanField(
        verbose_name=_("Use Azure Storage for image list"),
        help_text=_(
            "Whether to use Azure Storage for storing JSON file with list of images to process. If enabled, "
            "Azure Storage Account Access Key and Azure Storage Container must be specified"
        ),
        default=False,
    )

    use_azure_storage_for_images = models.BooleanField(
        verbose_name=_("Use Azure Storage for images"),
        help_text=_(
            "Whether to use Azure Storage for storing images to process. If enabled, "
            "Azure Storage Account Access Key and Azure Storage Container must be specified"
        ),
        default=False,
    )

    def __str__(self):
        return "Megadetector Batch Processing provider: %s" % (self.name)


class ExternalAIProvider(AIProvider):
    def __str__(self):
        return f"External AI provider {self.name}"


class AIClassificationJobAdditionalResource(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    ai_provider = models.ForeignKey(AIProvider, on_delete=models.CASCADE)
    classification_job = models.ForeignKey(
        AIClassificationJob, on_delete=models.CASCADE
    )
    resource_name = models.CharField(max_length=100)

    data = models.JSONField(
        editable=False,
        default=dict,
        verbose_name=_("AI Provider Data"),
        help_text=_("Data used internaly by AI Provider"),
    )

    class Meta:
        unique_together = ("ai_provider", "classification_job", "resource_name")


class ObservationTypeAILabelMapping(models.Model):
    ai_provider = models.ForeignKey(
        AIProvider, related_name="observation_type_mappings", on_delete=models.CASCADE
    )
    value = models.CharField(max_length=50)
    observation_type = models.CharField(
        choices=ObservationType.CHOICES, null=True, blank=True, max_length=20
    )


class SpeciesAILabelMapping(models.Model):
    ai_provider = models.ForeignKey(
        AIProvider, related_name="species_ai_label_mappings", on_delete=models.CASCADE
    )
    value = models.CharField(max_length=50)
    species = models.ForeignKey(
        Species,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="species_ai_label_mappings",
    )


class SexAILabelMapping(models.Model):
    ai_provider = models.ForeignKey(
        AIProvider, related_name="sex_ai_label_mappings", on_delete=models.CASCADE
    )
    value = models.CharField(max_length=50)
    sex = models.CharField(
        choices=SpeciesSex.CHOICES, default=SpeciesSex.UNDEFINED, max_length=20
    )


class AgeCattegoryAILabelMapping(models.Model):
    ai_provider = models.ForeignKey(
        AIProvider, related_name="age_ai_label_mappings", on_delete=models.CASCADE
    )
    value = models.CharField(max_length=50)
    age = models.CharField(
        choices=SpeciesAge.CHOICES, default=SpeciesAge.UNDEFINED, max_length=20
    )


class AIProviderPermission(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="ai_provider_permissions",
        help_text=_("User for which the permission is defined"),
        on_delete=models.CASCADE,
    )
    ai_provider = models.ForeignKey(
        AIProvider,
        related_name="user_permissions",
        help_text=_("AI Provider for which the permission is defined"),
        on_delete=models.CASCADE,
    )


class AIClassificationManager(models.Manager):
    """Manager for :class:`Classification` model.

    This manager contains additional logic used by DRF serializers
    """

    url_detail = "media_classification:view_ai_classification"
    url_delete = "media_classification:ai_classification_delete"

    def get_accessible(self, user=None, base_queryset=None, role_levels=None):
        """Return all :class:`AIClassification` instances that given user
        has access to. If user is not defined, then currently logged in user
        is used.
        If there is no authenticated user, then empty queryset is returned

        :param user: if not none then that user will be used to filter
            accessible AI classifications. If passed user not logged in,
            then empty queryset is returned
            If user is None then currently logged in user is used.
        :param base_queryset: queryset used to limit checked AI classifications.
            by default it's all AI classifications.
        :param role_levels:

        :return: :AIClassification: queryset
        """
        user = user or get_current_user()

        if base_queryset is None:
            queryset = self.get_queryset()
        else:
            queryset = base_queryset

        if not user.is_authenticated:
            return queryset.none()
        if user.is_superuser:
            return queryset
        else:
            levels = (
                role_levels or ClassificationProjectRoleLevels.VIEW_AI_CLASSIFICATIONS
            )
            cprojects = set(
                user.classification_project_roles.filter(name__in=levels).values_list(
                    "classification_project_id", flat=True
                )
            )
            qs = queryset.exclude(
                ~models.Q(classification__project__owner=user),
                ~models.Q(classification__project__pk__in=cprojects),
            )
        return qs

    def api_detail_context(self, item, user):
        """
        Method used in DRF api to return detail url if user has permissions
        """
        context = None
        if item.can_view(user):
            context = reverse(
                self.url_detail,
                kwargs={"ai_provider_id": item.model_id, "pk": item.classification_id},
            )
        return context

    def api_delete_context(self, item, user):
        """
        Method used in DRF api to return delete url if user has permissions
        """
        context = None
        if item.can_delete(user):
            context = reverse(self.url_delete, kwargs={"pk": item.pk})
        return context


class AIClassification(StandardStaticAttrsMixin):
    """
    This model stores results of AI classification by multiple AI models
    registered in :class:`AIProvider`.
    """

    classification = models.ForeignKey(
        Classification, related_name="ai_classifications", on_delete=models.CASCADE
    )
    model = models.ForeignKey(
        AIProvider, related_name="classifications", on_delete=models.PROTECT
    )
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="ai_classifications",
        on_delete=models.PROTECT,
    )
    created_at = models.DateTimeField(default=now, verbose_name=_("Created at"))
    updated_at = models.DateTimeField(default=now, verbose_name=_("Updated at"))

    detected_objects = models.JSONField(
        blank=True, null=True, verbose_name=_("Detected objects")
    )

    objects = AIClassificationManager()

    class Meta:
        ordering = ("classification__resource__date_recorded",)

    def __str__(self):
        return "AI Classification: %s" % (self.pk)

    @property
    def classificator(self):
        """Return classificator assigned to classification"""
        return self.classification.classificator

    def can_view(self, user=None):
        """Check if given user (or currently logged in) can view details
        of AI classification.

        By default persons that have access to project can view AI classifications
        done within that project.
        """
        user = user or get_current_user()
        return self.classification.project.can_view(user=user)

    def can_update(self, user=None):
        """Check if given user (or currently logged in) can update given
        AI classification

        By default Owner and project admin can update AI classification.
        """
        user = user or get_current_user()
        return self.owner == user or self.classification.project.is_project_admin(
            user=user
        )

    def can_delete(self, user=None):
        return self.can_update(user=user)

    def get_bboxes(self):
        "Returns the bounding boxes of all related observations"
        bboxes = []
        for i, dattr in enumerate(self.dynamic_attrs.all()):
            bboxes.append({"label": f"bbox{i + 1}", "bboxes": dattr.bboxes})
        return bboxes

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    @property
    def sorted_det_objects_items(self):
        return sorted(self.detected_objects.items())


@receiver(pre_delete, sender=AIClassification)
def clear_deleted_ai_classification_data_from_classification(
    sender, instance, **kwargs
):
    """
    When AIClassification that was previously approved is deleted, clear fields status_ai, approved_ai_by,
    and approved_ai_at from Classification object.
    """
    if hasattr(instance, "aiclassification_approved"):
        instance.aiclassification_approved.clear_ai_classification_attributes()


class AIClassificationDynamicAttrs(StandardDynamicAttrsMixin):
    """
    See :class:`StandardDynamicAttrsMixin` for details.

    When given :class:`UserClassification` is approved, dynamic attributes from this
    model are transferred into :class:`ClassificationDynamicAttrs`.
    """

    ai_classification = models.ForeignKey(
        AIClassification, related_name="dynamic_attrs", on_delete=models.CASCADE
    )

    def __str__(self):
        return (
            f"AIClassification:{self.ai_classification_id}|"
            f"Observation:{self.observation_type}"
        )


class Sequence(models.Model):
    """
    Sequence of resources identified by an expert (ADMIN or EXPERT
    in a project).
    """

    sequence_id = models.IntegerField(
        null=True, blank=True, verbose_name=_("Sequence id")
    )
    description = models.TextField(
        max_length=1000, null=True, blank=True, verbose_name=_("Description")
    )
    collection = models.ForeignKey(
        ClassificationProjectCollection,
        related_name="sequences",
        on_delete=models.CASCADE,
    )
    resources = models.ManyToManyField(
        Resource, through="SequenceResourceM2M", verbose_name=_("Resources")
    )
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_("Created at"))
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    class Meta:
        ordering = [
            "sequence_id",
        ]

    def __str__(self):
        return "Sequence: %s" % (self.pk)

    def save(self, *args, **kwargs):
        if self.id is None:
            last_obj = (
                self.__class__.objects.filter(collection=self.collection)
                .order_by("sequence_id")
                .last()
            )
            if last_obj:
                self.sequence_id = last_obj.sequence_id + 1
            else:
                self.sequence_id = 1
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        """Return url of research project details"""
        return reverse("media_classification:sequence_detail", kwargs={"pk": self.pk})

    def can_delete(self, user=None):
        """Determines whether given user can delete sequence.

        :param: user :class:`auth.User` instance for which test is made
        :return: True if user can delete sequence, False otherwise
        """
        user = user or get_current_user()
        project = self.collection.project
        return (
            self.created_by == user and project.enable_sequencing
        ) or project.is_project_admin(user=user)

    def get_first_resources_timestamp(self):
        return (
            self.resources.all()
            .order_by("date_recorded")
            .first.date_recorded.strftime("%Y-%m-%dT%H:%M:%S%z")
        )


class SequenceResourceM2M(models.Model):
    sequence = models.ForeignKey(Sequence, on_delete=models.CASCADE)
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)

    def validate_unique(self, *args, **kwargs):
        super().validate_unique(*args, **kwargs)
        if not self.id:
            if (
                self.__class__.objects.exclude(sequence=self.sequence)
                .filter(
                    sequence__collection=self.sequence.collection,
                    resource=self.resource,
                )
                .exists()
            ):
                raise ValidationError(
                    {
                        NON_FIELD_ERRORS: [
                            _(
                                f"The resource {self.resource.name} is already a part "
                                "of another sequence."
                            )
                        ],
                    }
                )


@receiver(post_save, sender=ClassificationProjectRole)
def project_role_collections_access_grant(sender, instance, **kwargs):
    """
    TODO: docstrings
    """
    user = instance.user
    if (
        not ClassificationProjectRole.objects.filter(
            classification_project__pk=instance.classification_project_id, user=user
        )
        .exclude(pk=instance.pk)
        .exclude(user__pk=instance.classification_project.owner_id)
        .exists()
    ):
        collections_pks = instance.classification_project.collections.values_list(
            "collection__pk", flat=True
        )
        collections = Collection.objects.filter(
            pk__in=collections_pks
        ).prefetch_related("managers")

        collections_access_grant(users=[user], collections=collections)


@receiver(post_delete, sender=ClassificationProjectRole)
def project_role_collections_access_revoke(sender, instance, **kwargs):
    """
    TODO: docstrings
    """
    user = instance.user
    if (
        not ClassificationProjectRole.objects.filter(
            classification_project__pk=instance.classification_project_id, user=user
        )
        .exclude(pk=instance.pk)
        .exclude(user__id=instance.classification_project.owner_id)
        .exists()
    ):
        collection_pks = instance.classification_project.collections.values_list(
            "collection__pk", flat=True
        )
        collections_access_revoke(
            collection_pks=collection_pks, user_pks=[user.pk], cproject=True
        )


@receiver(post_save, sender=ClassificationProjectCollection)
def project_collections_access_grant(sender, instance, **kwargs):
    """
    Signal used to grant an access to collections that belong to the
    classification project for users that are a part of this project.
    """
    if kwargs["created"]:
        roles = (
            ClassificationProjectRole.objects.filter(
                classification_project=instance.project
            )
            .exclude(user=instance.project.owner)
            .prefetch_related("user")
        )
        users = set([item.user for item in roles])

        collections_access_grant(
            users=users, collections=[instance.collection.collection]
        )


@receiver(post_delete, sender=ClassificationProjectCollection)
def project_collections_access_revoke(sender, instance, **kwargs):
    """
    Signal used to revoke an access to collections that belong to the
    classification project for users that are not a part of this project
    anymore.
    """
    user_pks = (
        ClassificationProjectRole.objects.filter(
            classification_project=instance.project
        )
        .exclude(user=instance.project.owner)
        .values_list("user__pk", flat=True)
    )

    collections_access_revoke(
        user_pks=user_pks,
        collection_pks=[instance.collection.collection.pk],
        cproject=True,
    )


@receiver(post_save, sender=ClassificationProjectCollection)
def project_collection_rebuild_(sender, instance, **kwargs):
    """
    TODO: docstrings
    """
    instance.rebuild_classifications()
