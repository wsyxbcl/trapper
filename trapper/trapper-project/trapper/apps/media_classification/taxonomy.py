# -*- encoding: utf-8 -*-

from django import forms
from django.utils.translation import gettext_lazy as _

from trapper.apps.common.taxonomy import BaseTaxonomy


class ObservationType(BaseTaxonomy):
    HUMAN = "human"
    ANIMAL = "animal"
    VEHICLE = "vehicle"
    BLANK = "blank"
    UNKNOWN = "unknown"
    UNCLASSIFIED = "unclassified"

    CHOICES = (
        (HUMAN, _("Human")),
        (ANIMAL, _("Animal")),
        (VEHICLE, _("Vehicle")),
        (BLANK, _("Blank")),
        (UNKNOWN, _("Unknown")),
        (UNCLASSIFIED, _("Unclassified")),
    )


class SpeciesAge(BaseTaxonomy):
    UNDEFINED = "unknown"
    ADULT = "adult"
    SUBADULT = "subadult"
    JUVENILE = "juvenile"
    OFFSPRING = "offspring"

    CHOICES = (
        (UNDEFINED, _("Unknown")),
        (ADULT, _("Adult")),
        (SUBADULT, _("Subadult")),
        (JUVENILE, _("Juvenile")),
        (OFFSPRING, _("Offspring")),
    )

    EXPORT_MAP = {
        UNDEFINED: "",
        ADULT: ADULT,
        SUBADULT: SUBADULT,
        JUVENILE: JUVENILE,
        OFFSPRING: JUVENILE,
    }


class SpeciesSex(BaseTaxonomy):
    UNDEFINED = "unknown"
    FEMALE = "female"
    MALE = "male"

    CHOICES = (
        (UNDEFINED, _("Unknown")),
        (FEMALE, _("Female")),
        (MALE, _("Male")),
    )


class SpeciesBehaviour(BaseTaxonomy):
    UNDEFINED = "undefined"
    GRAZING = "grazing"
    BROWSING = "browsing"
    ROOTING = "rooting"
    VIGILANCE = "vigilance"
    RUNNING = "running"
    WALKING = "walking"

    CHOICES = (
        (UNDEFINED, _("Undefined")),
        (GRAZING, _("Grazing")),
        (BROWSING, _("Browsing")),
        (ROOTING, _("Rooting")),
        (VIGILANCE, _("Vigilance")),
        (RUNNING, _("Running")),
        (WALKING, _("Walking")),
    )


class ClassificatorSettings(BaseTaxonomy):
    """"""

    STANDARD_ATTRS_REQUIRED_STATIC = ["is_setup"]

    STANDARD_ATTRS_OTHER_STATIC = []

    STANDARD_ATTRS_STATIC = list(
        STANDARD_ATTRS_REQUIRED_STATIC + STANDARD_ATTRS_OTHER_STATIC
    )

    STANDARD_ATTRS_REQUIRED_DYNAMIC = ["observation_type", "species", "count"]

    STANDARD_ATTRS_OTHER_DYNAMIC = [
        "sex",
        "age",
        "behaviour",
        "count_new",
        "individual_id",
        "classification_confidence",
    ]

    STANDARD_ATTRS_DYNAMIC = list(
        STANDARD_ATTRS_REQUIRED_DYNAMIC + STANDARD_ATTRS_OTHER_DYNAMIC
    )

    STANDARD_ATTRS = list(STANDARD_ATTRS_STATIC + STANDARD_ATTRS_DYNAMIC)

    STANDARD_ATTRS_AUTO_LIST = {
        "observation_type": ("observation", ObservationType),
        "sex": ("sex", SpeciesSex),
        "age": ("age", SpeciesAge),
    }

    FIELDS = {
        "B": forms.BooleanField,
        "F": forms.FloatField,
        "I": forms.IntegerField,
        "S": forms.CharField,
    }
    FIELD_BOOLEAN = "B"
    FIELD_INTEGER = "I"
    FIELD_FLOAT = "F"
    FIELD_STRING = "S"

    FIELD_LABELS = {
        FIELD_BOOLEAN: _("Bool"),
        FIELD_FLOAT: _("Float"),
        FIELD_INTEGER: _("Integer"),
        FIELD_STRING: _("String"),
    }

    TYPES_FRICTIONLESS = {
        FIELD_BOOLEAN: "boolean",
        FIELD_FLOAT: "number",
        FIELD_INTEGER: "integer",
        FIELD_STRING: "string",
    }

    NUMERIC_MAPPERS = {"F": float, "I": int}

    FIELD_CHOICES = FIELD_LABELS.items()

    TARGET_STATIC = "S"
    TARGET_DYNAMIC = "D"
    TARGETS = {
        TARGET_STATIC: _("Static"),
        TARGET_DYNAMIC: _("Dynamic"),
    }

    TARGET_CHOICES = TARGETS.items()

    PREDEFINED_ATTRIBUTES = {
        "comments": {
            "label": _("Comments"),
            "help_text": _("Simple Comments Field (TextArea)"),
            "formfield": forms.CharField,
            "widget": forms.Textarea(attrs={"cols": 10, "rows": 3}),
        },
    }

    PREDEFINED_NAMES = list(PREDEFINED_ATTRIBUTES.keys())

    TEMPLATE_TAB = "tab"
    TEMPLATE_INLINE = "inline"

    TEMPLATE_CHOICES = ((TEMPLATE_INLINE, "Inline"), (TEMPLATE_TAB, "Tabbed"))


class ClassificationProjectRoleLevels(BaseTaxonomy):
    ADMIN = 1
    EXPERT = 2
    COLLABORATOR = 3

    ANY = (
        ADMIN,
        EXPERT,
        COLLABORATOR,
    )
    UPDATE = (ADMIN,)
    DELETE = (ADMIN,)
    VIEW_CLASSIFICATIONS = (ADMIN, COLLABORATOR)
    VIEW_AI_CLASSIFICATIONS = (ADMIN, COLLABORATOR)
    VIEW_USER_CLASSIFICATIONS = (ADMIN,)

    CHOICES = (
        (ADMIN, _("Admin")),
        (EXPERT, _("Expert")),
        (COLLABORATOR, _("Collaborator")),
    )

    SEQUENCE_CHOICES = ((EXPERT, _("Expert")),)


class ClassificationProjectStatus(BaseTaxonomy):
    ONGOING = 1
    FINISHED = 2

    CHOICES = (
        (ONGOING, _("Ongoing")),
        (FINISHED, _("Finished")),
    )


class ClassificationStatus(BaseTaxonomy):
    APPROVED = True
    REJECTED = False

    CHOICES = (
        (APPROVED, _("Approved")),
        (REJECTED, _("Rejected")),
    )

    CHOICES_DICT = dict(CHOICES)


class ClassifyMessages:
    MSG_CLASSIFICATOR_MISSING = _(
        "You can not classify records without a classificator assigned to a project."
    )
    MSG_PERMS_REQUIRED = _("You have to be a project admin to run this action.")
    MSG_CANNOT_MODIFY_AI_CLASSIFICATION = _("You cannot modify AI classification.")
    MSG_CLASSIFY_MULTIPLE_FAILED = _(
        "Unable to classify multiple resources. Please try again."
    )
    MSG_CLASSIFY_ERRORS = _("Your classification form contains errors.")
    MSG_APPROVE_PERMS = _(
        "You have no permission to approve selected classification(s). The admin role is"
        "required."
    )
    MSG_IS_APPROVED = _("This classification is already approved.")
    MSG_CLASSIFICATION_MISSING = _("There is no such a classification.")
    MSG_NO_CLASSIFICATION = _(
        "There is nothing to classify. Please select at least one record."
    )
    MSG_SUCCESS = _("Your classification(s) has been successfully saved in a database.")
    MSG_SUCCESS_APPROVED = _(
        "You have successfully approved selected classification(s)."
    )


class ClassificationSettings(BaseTaxonomy):
    IMPORT_REQUIRED_COLUMNS = [
        "observationType",
        "scientificName",
        "count",
        "_id",
    ]

    # maps attribute names to csv header; only contains cases where those values differ
    # species info is handled separately in the importer
    IMPORT_ATTRS_MAP = {
        "observation_type": "observationType",
        "is_setup": "cameraSetup",
        "count_new": "countNew",
        "age": "lifeStage",
        "individual_id": "individualID",
        "classification_confidence": "classificationProbability",
    }

    EXPORT_COLUMNS_CAMTRAPDP = [
        "deploymentID",
        "mediaID",
        "eventID",
        "eventStart",
        "eventEnd",
        "observationLevel",
        "observationType",
        "cameraSetupType",
        "taxonID",
        "scientificName",
        "count",
        "lifeStage",
        "sex",
        "behavior",
        "individualID",
        "individualPositionRadius",
        "individualPositionAngle",
        "individualSpeed",
        "bboxX",
        "bboxY",
        "bboxWidth",
        "bboxHeight",
        "classificationMethod",
        "classifiedBy",
        "classificationTimestamp",
        "classificationProbability",
        "observationTags",
        "observationComments",
        "_id",
    ]

    EXPORT_COLUMNS_TRAPPER = [
        "deploymentID",
        "mediaID",
        "eventID",
        "eventStart",
        "eventEnd",
        "observationLevel",
        "observationType",
        "cameraSetupType",
        "taxonID",
        "scientificName",
        "count",
        "countNew",
        "lifeStage",
        "sex",
        "behavior",
        "individualID",
        "bboxes",
        "classificationMethod",
        "classifiedBy",
        "classificationTimestamp",
        "classificationProbability",
        "observationTags",
        "observationComments",
        "_id",
    ]


class AIClassificationSettings(BaseTaxonomy):
    AI_ATTRIBUTES = ["observation_type", "species", "age", "sex", "count"]
