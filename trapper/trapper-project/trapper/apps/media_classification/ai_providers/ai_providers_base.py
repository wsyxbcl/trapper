import abc
import uuid
from dataclasses import dataclass, asdict
from typing import List, Optional
from urllib.parse import urljoin

from dacite import from_dict
from django.shortcuts import get_object_or_404
from django.db import transaction
from django.utils.translation import gettext_lazy as _
from rest_framework import status
from rest_framework.response import Response
from trapper.apps.accounts.models import UserRemoteTask
from trapper.apps.accounts.taxonomy import UserRemoteTaskStatus
from trapper.apps.media_classification.models import (
    AIProvider,
    AIClassificationJobAdditionalResource,
    AIClassificationJob,
    AIClassification,
    AIClassificationDynamicAttrs,
)


class AIProviderException(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return f"AIProviderException: {self.message}"


@dataclass
class RemoteClassificationParams:
    """
    Stores basic parameters for given remote AI classification run
    """

    classification_ids: List[int]
    classify_preview_files: bool = False
    project_id: Optional[int] = None


@dataclass
class DetectionInfo:
    """
    Class containing basic, supported information about objects detected on images
    """

    observation_type: Optional[str] = None
    species_id: Optional[int] = None
    sex: Optional[str] = None
    age_cattegory: Optional[str] = None
    behaviour: Optional[str] = None
    bounding_box: Optional[list] = None
    confidence: Optional[float] = None


class BaseAIProviderManager(abc.ABC):
    """
    Base class for all remote classification managers.
    Handles common logic for all connectors.
    """

    config: AIProvider

    def __init__(self):
        self.stages = {}

    def get_absolute_url(self, url):
        return urljoin(self.config.trapper_instance_url + "/", url.lstrip("/"))

    def save_remote_classification_params(
        self, params: RemoteClassificationParams, classification_job_id: uuid.UUID
    ):
        """
        Serialize RemoteClassificationParams and save it to the database
        """
        data = asdict(params)

        AIClassificationJobAdditionalResource.objects.create(
            resource_name="params",
            ai_provider=self.config,
            classification_job_id=classification_job_id,
            data=data,
        )

    def load_remote_classification_params(
        self, classification_job_id: uuid.UUID
    ) -> RemoteClassificationParams:
        """
        Load RemoteClassificationParams from the database
        """
        resource = AIClassificationJobAdditionalResource.objects.get(
            ai_provider_id=self.config.pk,
            classification_job_id=classification_job_id,
            resource_name="params",
        )

        data = resource.data

        return from_dict(RemoteClassificationParams, data)

    def init_job(self, user_id) -> AIClassificationJob:
        """
        Create UserRemoteTask and AIClassificationJob objects
        """
        user_remote_task = UserRemoteTask(
            name=_(f"AI Classification: {self.config}"),
            user_id=user_id,
            status="STARTED",
        )
        user_remote_task.save()
        job = AIClassificationJob.objects.create(
            user_remote_task=user_remote_task, ai_provider=self.config
        )
        return job

    def get_extra_resource(self, request, classification_job_id, resource_name):
        """
        Return AI Classification Job Additional Resources based on Job ID and resource name.
        This can be for example list of files to classify.
        """

        resource = get_object_or_404(
            AIClassificationJobAdditionalResource,
            ai_provider_id=self.config.pk,
            classification_job_id=classification_job_id,
            resource_name=resource_name,
        )

        return Response(status=status.HTTP_200_OK, data=resource.data)

    def prepare_ai_classification_object(self, classification_id, owner):
        """
        Helper method that creates pre-configured AIClassification object
        """
        ai_classification_object = AIClassification(
            classification_id=classification_id,
            model=self.config,
            owner=owner,
            has_bboxes=True,
        )
        return ai_classification_object

    def prepare_ai_classification_dynamic_objects(
        self,
        ai_classification_object_id,
        detected_objects: List[DetectionInfo],
        empty_score: float,
    ):
        """
        Prepare AI classification objects for given AI classificatioin id,
        based on list of `DetectionInfo` objects
        """
        v = []
        if not detected_objects:
            dynamic_attrs = AIClassificationDynamicAttrs(
                ai_classification_id=ai_classification_object_id,
                observation_type="blank",
                classification_confidence=empty_score,
            )
            v.append(dynamic_attrs)

        for obj in detected_objects:
            dynamic_attrs = AIClassificationDynamicAttrs(
                ai_classification_id=ai_classification_object_id
            )

            if obj.observation_type:
                dynamic_attrs.observation_type = obj.observation_type

            if obj.species_id:
                dynamic_attrs.species_id = obj.species_id

            if obj.behaviour:
                dynamic_attrs.behaviour = obj.behaviour

            if obj.age_cattegory:
                dynamic_attrs.age = obj.age_cattegory

            if obj.sex:
                dynamic_attrs.sex = obj.sex

            if obj.bounding_box:
                dynamic_attrs.bboxes = [obj.bounding_box]

            if obj.confidence:
                dynamic_attrs.classification_confidence = obj.confidence

            v.append(dynamic_attrs)

        return v

    def create_ai_classification_objects(self, ai_classifications):
        """
        Creates AI Classification objects in database, based on provided list of new AI Classifications.
        Deletes already existing AI Classification objects if those exists for same Classification and AI Provider,
        so there is always at most one AI classification object per Classification for given AI Provider.
        """
        ai_classifications_old_keys = [
            obj.classification_id for obj in ai_classifications
        ]

        with transaction.atomic():
            # first delete old AI classifications made by the same AI model
            # (if there are any)
            AIClassification.objects.filter(
                classification__pk__in=ai_classifications_old_keys,
                model_id=self.config.id,
            ).delete()
            # then bulk_create new (updated) objects
            ai_classifications = AIClassification.objects.bulk_create(
                ai_classifications
            )

        return ai_classifications

    def fail_job(self, job: AIClassificationJob, msg: str):
        """
        Mark job and associated remote task status as failed
        """
        job.stage = "failed"
        remote_task = job.user_remote_task
        remote_task.status = UserRemoteTaskStatus.FAILURE

        remote_task.log = msg

        print(f"AI Provider job failure: {msg}")

        with transaction.atomic():
            remote_task.save()
            job.save()

    def log(self, job: AIClassificationJob, msg: str):
        remote_task = job.user_remote_task

        remote_task.log = msg
        print(f"[Remote task {remote_task.task_id} log] {msg}")

        remote_task.save()

    def update_remote_classification_task_status(self, job: AIClassificationJob):
        """
        Run stage processing logic based on current stage
        """
        stage = self.stages.get(job.stage)
        if stage:
            stage(job)
        else:
            print(f"Unknown stage: {job.stage}")

    def set_stage(self, job, stage_name):
        """
        Helper method to set stage name and save stage object
        """
        job.stage = stage_name
        job.save()

    def handle_callback(self, request):
        """
        Handler for webhook-based external AI classification connector (like Trapper AI)
        """
        pass

    def request_classification(self, params: RemoteClassificationParams, user_id):
        pass
