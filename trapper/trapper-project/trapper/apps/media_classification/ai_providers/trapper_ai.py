import json
from urllib.parse import urljoin
import uuid

import requests
from django.conf import settings
from trapper.apps.accounts.models import UserRemoteTask
from trapper.apps.media_classification.ai_providers.ai_providers_base import (
    BaseAIProviderManager,
    RemoteClassificationParams,
    AIProviderException,
    DetectionInfo,
)
from trapper.apps.media_classification.ai_providers.async_tasks import (
    schedule_status_update,
)
from trapper.apps.media_classification.models import (
    TrapperAIProvider,
    Classification,
    AIClassificationJob,
    AIClassificationDynamicAttrs,
    AIClassificationJobAdditionalResource,
)

from typing import List, Optional
from dataclasses import dataclass
from dataclasses_json import dataclass_json, Undefined
from enum import Enum


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass
class TrapperAISample:
    sample_id: int
    sample_url: str


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass
class TrapperAIDetectionRequest:
    ai_model_id: uuid.UUID
    samples: List[TrapperAISample]


class TrapperAIJobStatus(Enum):
    Initial = 0
    InProgress = 1
    Done = 2
    Failed = 3


class TrapperAIResourceStatus(Enum):
    INITIAL = 0
    IN_PROGRESS = 100
    FINISHED = 200
    FAILED = 300


class TrapperAIBatchProcessingStage(Enum):
    CREATED = 100
    RESOURCE_DOWNLOADING = 200
    RESOURCE_DOWNLOADING_FINISHED = 250
    PREDICTION = 300
    FINISHED = 400


class TrapperAIBatchProcessingStatus(Enum):
    CREATED = 100
    IN_PROGRESS = 200
    SUCCEEDED = 300
    FAILED = 400
    CANCELED = 500


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass
class TrapperAIJobStatusDto:
    value: TrapperAIJobStatus
    label: str

    def is_done(self):
        return self.value == TrapperAIJobStatus.Done

    def is_failed(self):
        return self.value == TrapperAIJobStatus.Failed

    @classmethod
    def create(cls, value: TrapperAIJobStatus):
        return cls(value=value, label=value.name)


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass
class TrapperAiResourceStatusDto:
    value: TrapperAIResourceStatus
    label: str

    def is_finished(self):
        return self.value == TrapperAIResourceStatus.Finished

    @classmethod
    def create(cls, value: TrapperAIResourceStatus):
        return cls(value=value, label=value.name)


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass
class TrapperAiBatchProcessingStatusDto:
    value: TrapperAIBatchProcessingStatus
    label: str

    @classmethod
    def create(cls, value: TrapperAIBatchProcessingStatus):
        return cls(value=value, label=value.name)


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass
class TrapperAiBatchProcessingStageDto:
    value: TrapperAIBatchProcessingStage
    label: str

    @classmethod
    def create(cls, value: TrapperAIBatchProcessingStage):
        return cls(value=value, label=value.name)


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass
class TrapperAIProgressMetrics:
    in_progress: int
    finished: int
    failed: int

    @classmethod
    def initial(cls):
        return cls(in_progress=0, failed=0, finished=0)


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass
class TrapperAIBatchData:
    id: str
    processing_status: TrapperAiBatchProcessingStatusDto
    processing_stage: TrapperAiBatchProcessingStageDto

    @classmethod
    def create_finished(cls):
        return cls(
            id=str(uuid.uuid4()),
            processing_status=TrapperAiBatchProcessingStatusDto.create(
                TrapperAIBatchProcessingStatus.SUCCEEDED,
            ),
            processing_stage=TrapperAiBatchProcessingStageDto.create(
                TrapperAIBatchProcessingStage.FINISHED
            ),
        )


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass
class TrapperAIJobCreatedResponse:
    id: str


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass
class TrapperAIJobData:
    id: str
    status: TrapperAIJobStatusDto

    progress_metrics: TrapperAIProgressMetrics

    batches: List[TrapperAIBatchData]

    def is_done(self):
        return self.status.is_done()

    def is_failed(self):
        return self.status.is_failed()


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass
class TrapperAIObjectDetectionRequestDataWithSampleIds(TrapperAIJobData):
    samples_uuids: List[uuid.UUID]


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass
class TrapperAIFetchDetectionsRequest:
    uuids: List[uuid.UUID]


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass
class TrapperAIPredictionResults:
    bboxes: List[List[float]]
    scores: List[float]
    classes: List[float]


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass
class TrapperAITaskResult:
    id: uuid.UUID
    prediction_results: Optional[TrapperAIPredictionResults]
    status: TrapperAiResourceStatusDto
    sample_id: int
    processing_error_msg: str
    job: str

    def is_finished(self):
        return self.status.is_finished()


class TrapperAIService:
    ENDPOINT_REQUEST_DETECTIONS = "/api/prediction_jobs/"
    ENDPOINT_REQUEST_JOB_STATUS = "/api/prediction_jobs/"
    ENDPOINT_REQUEST_BATCH_DATA = "/api/resource_batches/"
    ENDPOINT_TASK_RESULTS = "/api/tasks_result/"
    VERIFY_SSL = settings.REQUESTS_VERIFY_SSL

    def __init__(self, api_url: str, login: str, password: str, model_id: uuid.UUID):
        self.api_url = api_url
        self.login = login
        self.password = password
        self.model_id = model_id

    def request_detections(
        self,
        samples: List[TrapperAISample],
    ):
        """
        Make request to the external Trapper AI instance.
        Throws an AIProviderException on failure
        """

        request_params = TrapperAIDetectionRequest(
            ai_model_id=self.model_id,
            samples=samples,
        )

        headers = {"Content-Type": "application/json"}

        response = requests.post(
            urljoin(self.api_url, self.ENDPOINT_REQUEST_DETECTIONS),
            data=request_params.to_json(),
            auth=(self.login, self.password),
            headers=headers,
            verify=self.VERIFY_SSL,
        )

        print(response)

        if response.status_code != 201:
            msg = response.text
            raise AIProviderException(message=msg)

        return TrapperAIJobCreatedResponse.from_dict(response.json())

    def fetch_job_status(self, job_id: str) -> TrapperAIJobData:
        response = requests.get(
            url=urljoin(self.api_url, self.ENDPOINT_REQUEST_JOB_STATUS + f"{job_id}/"),
            auth=(self.login, self.password),
            verify=self.VERIFY_SSL,
        )

        response.raise_for_status()

        info = TrapperAIJobData.from_dict(response.json())

        return info

    def fetch_detections(self, batch_id: uuid.UUID) -> List[TrapperAITaskResult]:
        headers = {"Content-Type": "application/json"}
        response = requests.get(
            url=urljoin(
                self.api_url,
                self.ENDPOINT_REQUEST_BATCH_DATA + f"{str(batch_id)}/results/",
            ),
            auth=(self.login, self.password),
            verify=self.VERIFY_SSL,
            headers=headers,
        )

        response.raise_for_status()

        results: List[TrapperAITaskResult] = TrapperAITaskResult.schema().loads(
            response.text, many=True
        )

        return results


class TrapperAIProviderManager(BaseAIProviderManager):
    def __init__(self, config: TrapperAIProvider):
        super().__init__()
        self.config = config
        self.trapper_ai_service = TrapperAIService(
            api_url=self.config.api_url,
            login=self.config.api_auth_login,
            password=self.config.api_auth_passw,
            model_id=self.config.ai_model_id,
        )

        self.stages = {
            "prepare_classification_run": self.prepare_classification_run,
            # "parse_results": self.parse_results,
            "fetch_results": self.fetch_results,
        }

    def request_classification(self, params: RemoteClassificationParams, user_id):
        job = self.init_job(user_id)
        self.save_remote_classification_params(params, job.id)
        stage_name = "prepare_classification_run"
        self.set_stage(job, stage_name)

        schedule_status_update(job.id)

    def prepare_classification_run(self, job: AIClassificationJob):
        remote_task: UserRemoteTask = job.user_remote_task
        params = self.load_remote_classification_params(job.id)

        data = self._get_resources_url(params)

        try:
            response = self.trapper_ai_service.request_detections(
                data,
            )

            job_data = self._save_job_data(job, json.loads(response.to_json()))

            self.log(
                job,
                "TrapperAI processing in progress...",
            )

            print(job_data.data)
            job.stage = "fetch_results"
            job.save()
            schedule_status_update(job.id, 5)
        except AIProviderException as ex:
            msg = ex.message
            remote_task.status = "REJECTED"
            remote_task.log = msg

            remote_task.save()

            self.set_stage(job, "rejected")

    def _save_job_data(self, job: AIClassificationJob, data: dict):
        (job_data, _,) = AIClassificationJobAdditionalResource.objects.get_or_create(
            resource_name="trapper_ai_job_data",
            ai_provider=self.config,
            classification_job=job,
        )

        job_data.data = data
        job_data.save()

        return job_data

    def _get_resources_url(
        self, params: RemoteClassificationParams
    ) -> List[TrapperAISample]:
        data = []
        classifications = Classification.objects.filter(
            pk__in=params.classification_ids
        ).select_related("resource")
        for classification in classifications:
            field = "file"
            if params.classify_preview_files:
                field = "pfile"
            url = classification.resource.get_url_original(token=True, field=field)
            url = self.get_absolute_url(url)
            data.append(TrapperAISample(sample_id=classification.pk, sample_url=url))

        return data

    def _get_processed_batch_list(self, job: AIClassificationJob) -> List[str]:
        res = AIClassificationJobAdditionalResource.objects.filter(
            resource_name="trapper_ai_processed_batches",
            ai_provider=self.config,
            classification_job=job,
        ).first()

        if not res:
            return []

        return res.data

    def _save_processed_batch_list(
        self, job: AIClassificationJob, processed_batches: List[str]
    ) -> List[str]:
        res, _ = AIClassificationJobAdditionalResource.objects.get_or_create(
            resource_name="trapper_ai_processed_batches",
            ai_provider=self.config,
            classification_job=job,
        )

        res.data = processed_batches

        res.save()

    def fetch_results(self, job: AIClassificationJob):
        trapper_ai_job_data: TrapperAIJobCreatedResponse = (
            TrapperAIJobCreatedResponse.from_dict(
                AIClassificationJobAdditionalResource.objects.get(
                    resource_name="trapper_ai_job_data",
                    classification_job=job,
                ).data
            )
        )
        request_id = trapper_ai_job_data.id
        try:
            trapperai_job = self.trapper_ai_service.fetch_job_status(request_id)
        except requests.RequestException as ex:
            print(ex)
            print("Http error, scheduling to try again")
            schedule_status_update(job.id, 30)
            return

        already_processed_batches = self._get_processed_batch_list(job)

        all_ready_batches = [
            batch
            for batch in trapperai_job.batches
            if batch.processing_stage.value == TrapperAIBatchProcessingStage.FINISHED
        ]

        for batch in all_ready_batches:
            if batch.id in already_processed_batches:
                continue

            self._fetch_and_process_batch_results(job, batch_id=batch.id)
            already_processed_batches.append(batch.id)
            self._save_processed_batch_list(job, already_processed_batches)

            m = trapperai_job.progress_metrics
            total = m.finished + m.failed + m.in_progress
            progress_percent = 100 * (m.finished + m.failed) / total
            self.log(
                job,
                f"TrapperAI processing in progress - {progress_percent:0.2f}% "
                f"({m.finished} batches finished; {m.failed} batches failed; {m.in_progress} batches in progress). "
                f"Fetched {len(already_processed_batches)} of {len(trapperai_job.batches)} batches.",
            )

        if trapperai_job.is_done() and len(already_processed_batches) == len(
            trapperai_job.batches
        ):
            self._mark_finished(job)
        elif trapperai_job.is_failed():
            self._mark_failed(job)
        else:
            schedule_status_update(job.id, 10)

    def _mark_finished(self, job: AIClassificationJob):
        self.set_stage(job, "finished")

        task = job.user_remote_task
        task.status = "SUCCESS"
        task.log = f"""
                        Resources have been successfully classified by your
                        AI provider ({self.config.name}) and saved in a database.
                        """
        task.save()

    def _mark_failed(self, job: AIClassificationJob):
        self.set_stage(job, "failed")

        task = job.user_remote_task
        task.status = "FAILED"
        task.log = """
                        TrapperAI reported processing error.
                        """
        task.save()

    def _fetch_and_process_batch_results(self, job: AIClassificationJob, batch_id: str):
        print("_fetch_batch_results")
        results = self.trapper_ai_service.fetch_detections(batch_id=batch_id)

        detection_category_mapping = {
            ot.value: ot.observation_type
            for ot in self.config.observation_type_mappings.all()
        }
        print("Detection category mapping", detection_category_mapping)

        species_category_mapping = {
            ot.value: ot.species_id
            for ot in self.config.species_ai_label_mappings.all()
        }
        print("Species category mapping", species_category_mapping)

        ai_classifications = []
        all_detected_objects = {}
        owner = job.user_remote_task.user

        def trapperai_to_trapper_bounding_box(bounding_box):
            y, x, y2, x2 = bounding_box
            w = x2 - x
            h = y2 - y
            return [x, y, w, h]

        print(f"Found {len(results)} observations")

        for result in results:
            classification_id = result.sample_id
            classification_results = result.prediction_results

            detected_objects = []
            bboxes = classification_results.bboxes
            scores = classification_results.scores
            classes = classification_results.classes

            bboxes = list(map(trapperai_to_trapper_bounding_box, bboxes))

            for raw_label_code, score, bounding_box in zip(classes, scores, bboxes):
                # score = round(score, 2)
                if score < self.config.minimum_confidence:
                    continue
                label_code = str(int(raw_label_code))
                observation_type: Optional[str] = None
                species_id: Optional[int] = None
                if species_category_mapping:
                    species_id = species_category_mapping.get(label_code)
                    if not species_id:
                        self.fail_job(
                            job,
                            f"Species category label mapping from '{label_code}' not found. "
                            f"Check your TrapperAI provider config.",
                        )
                        return
                if detection_category_mapping:
                    observation_type = detection_category_mapping.get(label_code)
                    if not observation_type:
                        self.fail_job(
                            job,
                            f"Detection category label mapping from '{label_code}' not found. "
                            f"Check your TrapperAI provider config.",
                        )
                        return

                detection_info = DetectionInfo(
                    observation_type=observation_type,
                    species_id=species_id,
                    confidence=score,
                    bounding_box=bounding_box,
                )

                detected_objects.append(detection_info)

            ai_classification_object = self.prepare_ai_classification_object(
                classification_id, owner
            )
            if not detected_objects:
                ai_classification_object.has_bboxes = False
            ai_classifications.append(ai_classification_object)
            all_detected_objects[classification_id] = detected_objects

        ai_classifications = self.create_ai_classification_objects(ai_classifications)
        print(f"Created {len(ai_classifications)} AIClassification objects")

        ai_classification_dynamic_objects = []
        for ai_classification in ai_classifications:
            ai_classification_dynamic_objects += (
                self.prepare_ai_classification_dynamic_objects(
                    ai_classification.pk,
                    all_detected_objects[ai_classification.classification_id],
                    empty_score=self.config.minimum_confidence,
                )
            )

        AIClassificationDynamicAttrs.objects.bulk_create(
            ai_classification_dynamic_objects
        )
