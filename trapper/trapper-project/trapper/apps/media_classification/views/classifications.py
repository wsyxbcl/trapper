# -*- coding: utf-8 -*-
"""
Views used to handle logic related to classification management in media
classification application
"""
from collections import namedtuple
from itertools import chain
import json
from random import choice as random_choice
from typing import Optional, Iterable, Dict, Any, cast

from braces.views import UserPassesTestMixin, JSONResponseMixin

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.core.files.images import get_image_dimensions
from django.forms import BaseFormSet, DecimalField
from django.forms.models import formset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.utils.http import urlencode
from django.views import generic

from trapper.apps.media_classification.forms import ClassificationExportAggForm
from trapper.apps.accounts.models import UserTask
from trapper.apps.common.tools import datetime_aware
from trapper.apps.common.tools import parse_pks
from trapper.apps.common.views import (
    LoginRequiredMixin,
    BaseDeleteView,
    BaseBulkUpdateView,
)
from trapper.apps.geomap.models import MapManagerUtils
from trapper.apps.media_classification.forms import (
    ClassificationForm,
    BulkUpdateClassificationForm,
    ClassificationImportForm,
    ClassificationExportForm,
    CollectionResourceAppendForm,
    ClassifyAIForm,
    ClassificationFeedbackForm,
    ClassificationImportAttrsForm,
    ClassificationPublishForm,
    ClassifyBboxForm,
)
from trapper.apps.media_classification.models import (
    ClassificationProject,
    Classification,
    ClassificationDynamicAttrs,
    UserClassification,
    UserClassificationDynamicAttrs,
    ClassificationProjectCollection,
    AIClassification,
    StandardDynamicAttrsMixin,
    AIClassificationDynamicAttrs,
    StandardStaticAttrsMixin,
)
from trapper.apps.media_classification.tasks import (
    celery_import_classifications,
    celery_resource_feedback,
    celery_import_standard_attributes,
    celery_results_to_data_package,
    ClassificationImporter,
    celery_publish_data_package,
)
from trapper.apps.media_classification.taxonomy import (
    ClassificatorSettings,
    ClassifyMessages,
    ClassificationProjectRoleLevels,
    ObservationType,
)
from trapper.apps.storage.models import Collection
from trapper.apps.storage.views.collection import CollectionResourceAppendView

from trapper.apps.media_classification.ai_providers.ai_provider_factory import (
    get_ai_provider_manager,
)
from trapper.apps.media_classification.ai_providers.ai_providers_base import (
    AIProviderException,
    RemoteClassificationParams,
)

User = get_user_model()


class ClassifyFormView(
    LoginRequiredMixin, UserPassesTestMixin, generic.View, JSONResponseMixin
):
    """
    TODO: docstrings
    """

    http_method_names = ["get", "post"]
    raise_exception = True
    form_template = (
        "media_classification/classifications/classify/forms_{form_type}.html"
    )
    dynamic_form_template = None
    class_user = None
    current_user = None
    project = None
    collection = None
    classification = None
    user_classification = None
    resource = None

    def test_func(self, user):
        project_pk = self.kwargs.get("project_pk", None)
        self.project = get_object_or_404(ClassificationProject, pk=project_pk)
        # if there is no classificator assigned to the project there is no reason
        # to go further
        if not self.project.classificator:
            status = False
            msg = _("There is no classificator assigned to this project.")
            return self.render_json_response({"status": status, "msg": msg})
        return self.project.can_view(user)

    def get_collection(self):
        collection_pk = self.kwargs.get("collection_pk", None)
        if collection_pk:
            self.collection = get_object_or_404(Collection, pk=collection_pk)

    def get_classification(self):
        classification_pk = self.kwargs.get("classification_pk", None)
        if classification_pk:
            self.classification = get_object_or_404(
                Classification, pk=classification_pk
            )
            self.resource = self.classification.resource

    def get_ai_classification(self):
        self.ai_classification = None
        ai_provider_id = self.kwargs.get("ai_provider_id", None)
        if ai_provider_id:
            self.ai_classification = get_object_or_404(
                AIClassification,
                model_id=ai_provider_id,
                classification=self.classification,
            )

    def get_user(self):
        """
        TODO: docstrings
        """
        self.current_user = self.request.user
        user_pk = self.kwargs.get("user_pk", None)
        if user_pk:
            self.class_user = get_object_or_404(User, pk=user_pk)
        else:
            self.class_user = self.current_user

    def get_form_template(self, classificator):
        """Classificator can use different templates that are used
        to render classification forms; here we can determine which
        template should be used"""

        template = None
        if classificator is not None:
            template = self.form_template.format(form_type=classificator.template)
        return template

    def _copy_dattrs_from(
        self,
        classificator,
        dynamic_attributes_list: Iterable[StandardDynamicAttrsMixin],
    ):
        dynamic_formset_initial = []
        for dattrs in dynamic_attributes_list:
            # if dattrs come from observations stored in Resource.data
            if type(dattrs) is dict:
                dattrs["species"] = dattrs.pop("species_id", None)
                dattrs = namedtuple("dattrs", dattrs.keys())(*dattrs.values())

            dattrs_initial: Dict[str, Any] = {}
            # First set all active standard attributes
            for attr_name in classificator.active_standard_attrs("DYNAMIC"):
                attr_value = getattr(dattrs, attr_name)
                if attr_name == "species" and attr_value:
                    attr_value = getattr(attr_value, "pk", None) or attr_value
                dattrs_initial[attr_name] = attr_value
            # bboxes
            if dattrs.bboxes:
                dattrs_initial["bboxes"] = dattrs.bboxes
            if dattrs.attrs:
                dattrs_initial.update(cast(Dict[str, Any], dattrs.attrs))

            if hasattr(dattrs, "classification_confidence"):
                dattrs_initial["classification_confidence"] = cast(
                    AIClassificationDynamicAttrs, dattrs
                ).classification_confidence

            dynamic_formset_initial.append(dattrs_initial)

        return dynamic_formset_initial

    def get_dynamic_form(
        self,
        classificator,
        fields_defs,
        user_classification=None,
        ai_classification: Optional[AIClassification] = None,
        readonly=False,
    ):
        """
        Build a dynamic formset that will be used for classifications.
        """
        dynamic_formset: Optional[BaseFormSet] = None
        if fields_defs["D"]:
            dynamic_attrs_order = classificator.get_dynamic_attrs_order()
            dynamic_formset_initial = []
            extra = 0
            if ai_classification:
                print("Copying from AI Classification")
                dynamic_attrs_order.append("classification_confidence")
                fields_defs["D"]["classification_confidence"] = DecimalField(
                    disabled=True, required=False, label=_("Confidence")
                )
                dynamic_formset_initial = self._copy_dattrs_from(
                    classificator, ai_classification.dynamic_attrs.all()
                )
            elif user_classification:
                print("Copying from User classification")
                dynamic_formset_initial = self._copy_dattrs_from(
                    classificator, user_classification.dynamic_attrs.all()
                )
            elif self.classification and self.classification.status:
                print("Copying from initial data")
                dynamic_formset_initial = self._copy_dattrs_from(
                    classificator, self.classification.dynamic_attrs.all()
                )

            elif self.classification and self.classification.status_ai:
                print("Copying from approved AI classification")
                dynamic_formset_initial = self._copy_dattrs_from(
                    classificator,
                    self.classification.approved_source_ai.dynamic_attrs.all(),
                )

            # set initial data from resource's internal attributes (if available)
            elif self.resource and self.resource.data:
                print("Copying from resource initial data")
                dynamic_formset_initial = self._copy_dattrs_from(
                    classificator, self.resource.data.get("observations", [])
                )
            else:
                print("Not copying anything")
                extra = 1

            dynamic_class_formset = formset_factory(ClassificationForm, extra=extra)

            dynamic_formset = dynamic_class_formset(
                data=self.request.POST or None,
                initial=dynamic_formset_initial,
                form_kwargs={
                    "fields_defs": fields_defs["D"],
                    "attrs_order": dynamic_attrs_order,
                    "readonly": readonly,
                },
            )
            self.dynamic_form_template = dynamic_formset.empty_form
        return dynamic_formset

    def _copy_static_attrs_from(
        self,
        classificator,
        static_attrs: StandardStaticAttrsMixin,
    ):
        static_class_form_initial = {}
        # First set all active standard attributes
        for attr_name in classificator.active_standard_attrs("STATIC"):
            attr_value = getattr(static_attrs, attr_name)
            static_class_form_initial[attr_name] = attr_value
        # Then set custom attributes
        if static_attrs.static_attrs:
            static_class_form_initial.update(
                cast(Dict[str, Any], static_attrs.static_attrs)
            )

        return static_class_form_initial

    def get_static_form(
        self,
        classificator,
        fields_defs,
        user_classification=None,
        ai_classification=None,
        readonly=False,
    ):
        """
        Build a static form that will be used for classifications.
        """
        static_class_form = None
        if fields_defs["S"]:
            static_attrs_order = classificator.get_static_attrs_order()
            static_class_form_initial = {}
            if ai_classification:
                static_class_form_initial = self._copy_static_attrs_from(
                    classificator, ai_classification
                )
            elif user_classification:
                static_class_form_initial = self._copy_static_attrs_from(
                    classificator, user_classification
                )
            elif self.classification and self.classification.has_initial_data:
                static_class_form_initial = self._copy_static_attrs_from(
                    classificator, self.classification
                )
            static_class_form = ClassificationForm(
                data=self.request.POST or None,
                fields_defs=fields_defs["S"],
                attrs_order=static_attrs_order,
                initial=static_class_form_initial,
                readonly=readonly,
            )
        return static_class_form

    def get_bboxes(self, classification):
        bboxes = classification.dynamic_attrs.values_list("bboxes", flat=True)
        try:
            bboxes = list(chain(*bboxes))
        except TypeError:
            bboxes = []
        return bboxes

    def is_readonly(self, is_project_admin):
        """
        Determine if form should be readonly or not.
        """
        return self.current_user != self.class_user and not is_project_admin

    def get(self, request, *args, **kwargs):
        """
        TODO: docstrings
        """
        self.get_user()
        self.get_classification()
        self.get_ai_classification()
        if self.classification:
            self.user_classification = self.classification.user_classifications.filter(
                owner=self.class_user
            ).first()

        classificator = self.project.classificator
        is_project_admin = self.project.is_project_admin(user=self.current_user)
        is_readonly = (
            self.is_readonly(is_project_admin=is_project_admin)
            or self.ai_classification is not None
        )
        fields_defs = classificator.prepare_form_fields()
        dynamic_form = self.get_dynamic_form(
            classificator=classificator,
            fields_defs=fields_defs,
            user_classification=self.user_classification,
            ai_classification=self.ai_classification,
            readonly=is_readonly,
        )
        static_form = self.get_static_form(
            classificator=classificator,
            fields_defs=fields_defs,
            user_classification=self.user_classification,
            ai_classification=self.ai_classification,
            readonly=is_readonly,
        )
        form_context = {
            "static_form": static_form,
            "dynamic_form": dynamic_form,
            "dynamic_form_template": self.dynamic_form_template,
            "is_project_admin": is_project_admin,
            "is_readonly": is_readonly,
            "resource_type": self.resource.resource_type if self.resource else "",
        }
        html_form_template = self.get_form_template(classificator=classificator)
        response_context = {
            "status": True,
            "msg": kwargs.get("msg"),
            "form_html": render_to_string(html_form_template, form_context),
        }
        return self.render_json_response(context_dict=response_context)

    def post(self, request, *args, **kwargs):
        """
        When a user has required permissions and filled in all neccessery
        fields in the classification form then this method is used to save all
        provided data to a database object
        :class:apps.media_classification.models.UserClassification`.

        If a currently logged in user is a Project Admin and decide that
        this classification should be approved, then after user classification
        is saved, :class:apps.media_classification.models.Classification`
        object is updated with the current classification data and this
        classification is marked as approved.
        """
        classificator = self.project.classificator
        now = datetime_aware()
        self.get_user()
        self.get_classification()
        self.get_ai_classification()

        # Only project admins can update classifications of other users
        is_project_admin = self.project.is_project_admin(self.current_user)
        if (
            self.current_user != self.class_user or "approve" in request.POST
        ) and not is_project_admin:
            response_context = {
                "status": False,
                "msg": ClassifyMessages.MSG_PERMS_REQUIRED,
            }
            return self.render_json_response(context_dict=response_context)

        if self.ai_classification:
            response_context = {
                "status": False,
                "msg": ClassifyMessages.MSG_CANNOT_MODIFY_AI_CLASSIFICATION,
            }
            return self.render_json_response(context_dict=response_context)

        # Check if there are any selected (multiple) resources
        selected_resources = request.POST.get("selected_resources")
        if selected_resources:
            classifications = set(
                self.project.classifications.filter(
                    resource__pk__in=parse_pks(selected_resources)
                )
            )
        else:
            classifications = set()
        if self.classification:
            classifications.update({self.classification})

        if len(classifications) == 0:
            response_context = {
                "status": False,
                "msg": ClassifyMessages.MSG_NO_CLASSIFICATION,
            }
            return self.render_json_response(context_dict=response_context)

        # Build classification form fields
        fields_defs = classificator.prepare_form_fields()

        # Iterate over classification objects and save data
        for classification in classifications:
            user_classification, created = UserClassification.objects.get_or_create(
                classification=classification,
                owner=self.class_user,
                defaults={"created_at": now, "updated_at": now},
            )
            if not created:
                user_classification.updated_at = now

            dynamic_form = self.get_dynamic_form(
                classificator=classificator,
                fields_defs=fields_defs,
                user_classification=user_classification,
            )

            # make sure that all forms in this formset will be properly validated
            # including the extra one which is empty by default
            if dynamic_form:
                for form in dynamic_form:
                    form.empty_permitted = False

            static_form = self.get_static_form(
                classificator=classificator,
                fields_defs=fields_defs,
                user_classification=user_classification,
            )

            # validate both forms
            if not (static_form.is_valid() and dynamic_form.is_valid()):
                # return rendered classification form with validation errors
                form_context = {
                    "static_form": static_form,
                    "dynamic_form": dynamic_form,
                    "dynamic_form_template": self.dynamic_form_template,
                    "is_project_admin": is_project_admin,
                    "resource_type": self.resource.resource_type
                    if self.resource
                    else "",
                }
                html_form_template = self.get_form_template(classificator=classificator)
                response_context = {
                    "status": False,
                    "msg": ClassifyMessages.MSG_CLASSIFY_ERRORS,
                    "form_html": render_to_string(html_form_template, form_context),
                }
                return self.render_json_response(context_dict=response_context)

            # both forms are valid and now we can save data to db
            classification.updated_at = now
            classification.updated_by = self.current_user
            classification.save()

            static_standard_fields = classificator.active_standard_attrs("STATIC")
            dynamic_standard_fields = classificator.active_standard_attrs("DYNAMIC")

            # Static form

            # First save all active standard attributes
            for attr_name in static_standard_fields:
                attr_value = static_form.cleaned_data.pop(attr_name)
                setattr(user_classification, attr_name, attr_value)
            # Then set custom attributes
            user_classification.static_attrs = static_form.cleaned_data
            user_classification.save()

            # Dynamic form

            # get bboxes for bulk classification if available
            # bulk classification -> usually an entire sequence of resources is
            # selected and we want to prevent overwriting bboxes with the same form
            # data for different resources in this sequence
            bboxes = self.get_bboxes(user_classification)
            if not bboxes and classification.status:
                bboxes = self.get_bboxes(classification)
            if not bboxes and classification.status_ai:
                bboxes = self.get_bboxes(classification)
            if not bboxes:
                bboxes = list(
                    chain(*classification.resource.data.get("osbervations", []))
                )

            # bulk-delete of old rows
            user_classification.dynamic_attrs.all().delete()

            # populate new ones
            for dform in dynamic_form.forms:
                dynamic_attrs = UserClassificationDynamicAttrs(
                    userclassification=user_classification
                )

                # First save all active standard attributes
                for attr_name in dynamic_standard_fields:
                    attr_value = dform.cleaned_data.pop(attr_name, None)
                    if attr_name == "species":
                        attr_name = "species_id"
                    setattr(dynamic_attrs, attr_name, attr_value)

                # do not overwrite bboxes in a sequence
                bboxes_form = dform.cleaned_data.pop("bboxes") or []
                if classification == self.classification:
                    dynamic_attrs.bboxes = bboxes_form
                elif bboxes:
                    dynamic_attrs.bboxes = []
                    for i in range(dynamic_attrs.count):
                        try:
                            dynamic_attrs.bboxes.append(bboxes.pop(0))
                        except IndexError:
                            break
                else:
                    dynamic_attrs.bboxes = []

                # Then set custom attributes
                dynamic_attrs.attrs = dform.cleaned_data
                dynamic_attrs.save()

            # set has_bboxes attribute for user_classification
            bboxes = user_classification.dynamic_attrs.values_list("bboxes", flat=True)
            if any(bboxes):
                user_classification.has_bboxes = True
                user_classification.save()

            # Approve user classification (if requested) i.e. transfer attributes values
            # from the user_classification to the classification object
            if request.POST.get("approve"):
                classification.approve_user_classification(
                    user_classification, user=self.current_user
                )

        msg = ClassifyMessages.MSG_SUCCESS
        return self.get(request, msg=msg, *args, **kwargs)


view_classify_form = ClassifyFormView.as_view()


class ClassifyCollectionView(
    LoginRequiredMixin, UserPassesTestMixin, generic.TemplateView
):
    """
    TODO: docstrings
    """

    raise_exception = True
    template_name = "media_classification/classifications/classify/index_bulk.html"
    project = None

    def get_project(self, project_pk):
        """
        Return classification project with given pk or return HTTP 404.
        """
        return get_object_or_404(ClassificationProject, pk=project_pk)

    def test_func(self, user):
        """
        This view can be accessed only if currently logged in user
        can at least view classification given project.
        """
        project_pk = self.kwargs.get("project_pk", None)
        self.project = self.get_project(project_pk=project_pk)
        return self.project.can_view(user)

    def get_collection(self, project_pk, collection_pk):
        """
        Return classification project collection with given pk within given
        classification project or return HTTP 404.
        """
        return get_object_or_404(
            ClassificationProjectCollection, project__pk=project_pk, pk=collection_pk
        )

    def get_context_data(self, **kwargs):
        collection = self.get_collection(
            project_pk=self.project.pk, collection_pk=kwargs.get("collection_pk")
        )
        params = {}
        deployments = self.request.GET.get("deployments")
        if deployments:
            params.update({"resource__deployment__pk__in": parse_pks(deployments)})
        classification = (
            collection.classifications.filter(**params)
            .order_by("resource__date_recorded")
            .first()
        )
        context = {
            "project": self.project,
            "collection": collection,
            "storage_collection": collection.collection.collection,
            "total_collection": collection.collection.collection.resources.count(),
            "classification": classification,
            "is_project_admin": self.project.is_project_admin(user=self.request.user),
        }
        return context


view_classify_collection = ClassifyCollectionView.as_view()


class ClassifyResourceView(LoginRequiredMixin, UserPassesTestMixin, generic.View):
    """
    Classify resources according to defined classifcation forms.
    """

    http_method_names = ["get"]
    raise_exception = True
    readonly = False
    classification = None

    def test_func(self, user):
        """
        Only users that have access to given classification project can
        classify resources.
        """
        classification_pk = self.kwargs.get("pk", None)
        self.classification = self.get_classification(classification_pk)
        return self.classification.project.can_view(user)

    def get_classification(self, classification_pk):
        """
        Return classification with given pk or return HTTP 404.
        """
        return get_object_or_404(
            Classification.objects.select_related(
                "project__owner",
                "project__classificator",
                "resource",
                "resource__deployment",
            ).prefetch_related("user_classifications__owner"),
            pk=classification_pk,
        )

    def get_user(self):
        user_pk = self.kwargs.get("user_pk", None)
        user = None
        if user_pk:
            user = get_object_or_404(User, pk=user_pk)
        if not user:
            user = self.request.user
        return user

    def get_selected_ai_classification(self):
        ai_provider_id = self.kwargs.get("ai_provider_id")
        if not ai_provider_id:
            return None

        return get_object_or_404(
            AIClassification,
            model_id=ai_provider_id,
            classification=self.classification,
        )

    def is_readonly(self, is_project_admin):
        """
        Determine if form should be readonly or not.
        """
        return self.readonly and not is_project_admin

    def get(self, request, *args, **kwargs):
        """
        TODO: docstrings
        """
        class_user = self.get_user()
        current_user = request.user
        user_classifications = self.classification.user_classifications.all()
        request_user_classification = next(
            (k for k in user_classifications if k.owner == current_user), None
        )
        user_classification = next(
            (k for k in user_classifications if k.owner == class_user), None
        )

        ai_classification = self.get_selected_ai_classification()

        collection = self.classification.collection
        project = self.classification.project
        is_project_admin = project.is_project_admin(user=current_user)
        is_readonly = self.is_readonly(is_project_admin=is_project_admin)
        storage_collection = collection.collection.collection
        resource = self.classification.resource
        ai_classifications = self.classification.ai_classifications.all()

        is_ai_classification_view = ai_classification is not None
        is_user_classification_view = (
            ai_classification is None and user_classification is not None
        )

        context = {
            "class_user": class_user,
            "project": project,
            "collection": collection,
            "storage_collection": storage_collection,
            "resource": resource,
            "user_classifications": user_classifications,
            "classification": self.classification,
            "ai_classifications": ai_classifications,
            "user_classification": user_classification,
            "ai_classification": ai_classification,
            "request_user_classification": request_user_classification,
            "is_project_admin": is_project_admin,
            "is_readonly": is_readonly,
            "is_ai_classification_view": is_ai_classification_view,
            "is_user_classification_view": is_user_classification_view,
        }
        return render(
            request, "media_classification/classifications/classify/index.html", context
        )


view_classify_resource = ClassifyResourceView.as_view()


class ClassificationGridContextMixin:
    """"""

    def get_classification_url(self, **kwargs):
        return reverse("media_classification:api-classification-list")

    def get_classification_context(self, **kwargs):
        project = kwargs.get("project", None)
        is_admin = project.is_project_admin(self.request.user)
        context = {
            "data_url": self.get_classification_url(**kwargs),
            "maps": MapManagerUtils.get_accessible(user=self.request.user),
            "project": project,
            "model_name": "classifications",
            "update_redirect": "true",
            "hide_delete": not is_admin,
            "is_admin": is_admin,
        }
        if project:
            context["collections"] = ClassificationProjectCollection.objects.filter(
                project=project
            ).values_list("pk", "collection__collection__name")
        return context


class ClassificationListView(
    LoginRequiredMixin,
    UserPassesTestMixin,
    generic.ListView,
    ClassificationGridContextMixin,
):
    """
    List view of :class:`apps.media_classification.models.Classification`
    instances. This list is always limited to a given classification project.
    """

    template_name = "media_classification/classifications/list.html"
    raise_exception = True

    def get_classification_url(self, **kwargs):
        """
        Alter url for classification DRF API to get only classifications
        that belong to given classification project.
        """
        project_pk = self.kwargs["pk"]
        return "{url}?project={pk}".format(
            url=reverse("media_classification:api-classification-list"), pk=project_pk
        )

    def get_project(self):
        """
        Return classification project with given pk or return HTTP 404.
        """
        return get_object_or_404(ClassificationProject, pk=self.kwargs["pk"])

    def test_func(self, user):
        """
        Check if user has enough permissions to view classifications.
        """
        return user.is_staff or self.get_project().can_view_classifications(user)

    def build_filters(self):
        """
        Build a dynamic filters definition for the classification results list view.
        """
        filter_definition = []
        project = self.get_project()
        classificator = project.classificator
        if not classificator:
            messages.warning(
                request=self.request,
                message=_("This project has no classificator assigned."),
            )
            return filter_definition
        # standard attributes
        attrs_list = ClassificatorSettings.STANDARD_ATTRS_AUTO_LIST
        for attr_name, params in attrs_list.items():
            field = {
                "label": _(params[0].capitalize()),
                "name": attr_name,
                "field": attr_name,
                "tag": {"name": "select", "is_block": True},
                "values": [("", _("All"))] + list(params[1].CHOICES),
            }
            filter_definition.append(field)
        custom_def = classificator.custom_attrs
        for name, params in custom_def.items():
            if params["target"] == "S":
                field_type = "static_attrs"
            else:
                field_type = "dynamic_attrs"
            field = {
                "label": _(name.capitalize()),
                "name": name,
                "field": field_type,
            }
            # boolean
            if params["field_type"] == "B":
                field["tag"] = {"name": "select", "is_block": True}
                field["values"] = [("", "All"), ("True", "Yes"), ("False", "No")]
            # list of string
            if params["field_type"] == "S" and params["values"]:
                values = params["values"].split(",")
                field["tag"] = {"name": "select", "is_block": True}
                field["values"] = [("", _("All"))] + list(zip(values, values))
            filter_definition.append(field)
        return filter_definition

    def get_context_data(self, **kwargs):
        """
        Update context used to render template with classification context
        and filters.
        """
        project = self.get_project()
        context = {
            "classification_context": self.get_classification_context(project=project),
        }
        context["classification_context"]["filters"] = self.build_filters()
        context["classification_context"]["update_redirect"] = False
        return context

    def get_queryset(self, **kwargs):
        pass


view_classification_list = ClassificationListView.as_view()


class ClassificationDeleteView(BaseDeleteView):
    """
    TODO: docstrings
    """

    model = Classification
    redirect_url = "media_classification:classification_list"

    def add_message(self, status, template, item):
        """
        Classification has no `name` attribute so we have to overwrite
        this method.
        """
        messages.add_message(self.request, status, template)

    def get_redirect_url(self):
        """
        Redirect to the classification list.
        """
        url = reverse(self.redirect_url, args=[self.item.project.pk])
        return redirect(url)

    def delete_item(self, item):
        item.delete(clear=True)

    def bulk_delete(self, queryset):
        """
        Instead of deleting classification objects bulk-clear their attributes.
        """
        to_update = []
        pks = []
        for obj in queryset:
            obj.is_setup = False
            obj.has_bboxes = False
            obj.static_attrs = {}
            obj.status = False
            obj.status_ai = False
            obj.approved_by = None
            obj.approved_ai_by = None
            obj.approved_at = None
            obj.approved_ai_at = None
            obj.approved_source_id = None
            obj.approved_source_ai_id = None
            obj.updated_at = datetime_aware()
            obj.updated_by = self.request.user
            to_update.append(obj)
            pks.append(obj.pk)
        Classification.objects.bulk_update(
            to_update,
            fields=[
                "is_setup",
                "has_bboxes",
                "static_attrs",
                "status",
                "status_ai",
                "approved_by",
                "approved_ai_by",
                "approved_at",
                "approved_ai_at",
                "approved_source_id",
                "approved_source_ai_id",
                "updated_at",
                "updated_by",
            ],
            batch_size=500,  # settings.HEAVY_BULK_UPDATE_BATCH_SIZE,
        )
        ClassificationDynamicAttrs.objects.filter(classification__pk__in=pks).delete()

    def filter_editable(self, queryset, user):
        return self.model.objects.get_accessible(
            user=user,
            base_queryset=queryset,
            role_levels=[ClassificationProjectRoleLevels.ADMIN],
        )


view_classification_delete = ClassificationDeleteView.as_view()


class ClassificationBulkUpdateView(BaseBulkUpdateView):
    """Resource bulk update view."""

    template_name = "forms/simple_crispy_form.html"
    form_class = BulkUpdateClassificationForm
    raise_exception = True


view_classification_bulk_update = ClassificationBulkUpdateView.as_view()


class ClassificationFeedbackView(
    LoginRequiredMixin, UserPassesTestMixin, generic.FormView, JSONResponseMixin
):
    """"""

    form_class = ClassificationFeedbackForm
    template_name = "media_classification/classifications/feedback_form.html"
    success_url = "."
    project = None
    raise_exception = True

    def test_func(self, user):
        """
        Check if user has enough permissions to send classifications for AI
        classification.
        """
        self.project = self.get_project()
        return self.project.is_project_admin(user)

    def get_project(self):
        return get_object_or_404(ClassificationProject, pk=self.kwargs.get("pk"))

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["project"] = self.project
        return kwargs

    def form_valid(self, form):
        user = self.request.user
        params = {
            "user": self.request.user,
            "classifications": form.cleaned_data.get("classifications"),
            "base_classification": form.cleaned_data.get("base_classification", False),
        }
        tags = [
            k
            for k in form.cleaned_data.keys()
            if "custom_" in k and form.cleaned_data.get(k)
        ]
        if tags:
            params["tags"] = tags
        if settings.CELERY_ENABLED:
            task = celery_resource_feedback.delay(**params)
            user_task = UserTask(user=user, task_id=task.task_id)
            user_task.save()
            msg = _("You have successfully run the feedback action!")
        else:
            msg = celery_resource_feedback(**params)
        status = True
        return self.render_json_response({"status": status, "msg": msg})

    def form_invalid(self, form):
        context = {
            "success": False,
            "msg": _("Your form contains errors."),
            "form_html": render_to_string(self.template_name, {"form": form}),
        }
        return self.render_json_response(context_dict=context)


view_classification_feedback = ClassificationFeedbackView.as_view()


class ClassificationExportAggFormView(
    LoginRequiredMixin, UserPassesTestMixin, generic.FormView, JSONResponseMixin
):
    form_class = ClassificationExportAggForm
    template_name = (
        "media_classification/classifications/classifications_export_agg_form.html"
    )
    success_url = "."
    project = None
    raise_exception = True

    def test_func(self, user):
        """
        Check if user has enough permissions to export classifications.
        """
        self.project = self.get_project()
        return self.project.is_project_admin(user)

    def get_project(self):
        return get_object_or_404(ClassificationProject, pk=self.kwargs.get("pk"))

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["project"] = self.project
        return kwargs

    def form_valid(self, form):
        agg_api_url = reverse(
            "media_classification:api-classification-results-agg",
            kwargs={"project_pk": self.project.pk},
        )
        filters = form.cleaned_data.get("api_url_filters", "")
        params = {
            "agg_target_level": form.cleaned_data["agg_target_level"],
            "event_fun": form.cleaned_data["event_fun"],
            "count_fun": form.cleaned_data["count_fun"],
            "count_var": form.cleaned_data["count_var"],
            "all_dep": form.cleaned_data["all_dep"],
            "filter_dep": form.cleaned_data["filter_dep"],
            "geojson": form.cleaned_data["geojson"],
        }

        filters += "&" + urlencode(params)
        agg_api_url += filters

        return self.render_json_response(
            {
                "status": True,
                "msg": _("Exporting trapping rates..."),
                "url": agg_api_url,
            }
        )

    def form_invalid(self, form):
        context = {
            "status": False,
            "msg": _("Your form contains errors."),
            "form_html": render_to_string(self.template_name, {"form": form}),
        }
        return self.render_json_response(context_dict=context)


view_classification_export_agg = ClassificationExportAggFormView.as_view()


class ClassificationImportAttrsView(
    LoginRequiredMixin, UserPassesTestMixin, generic.FormView, JSONResponseMixin
):
    """
    Imports standard attributes from `self.resource.data` JSONField.
    """

    form_class = ClassificationImportAttrsForm
    template_name = "media_classification/classifications/import_attrs_form.html"
    success_url = "."
    project = None
    raise_exception = True

    def test_func(self, user):
        """
        Check if user has enough permissions to import standard attributes.
        """
        self.project = self.get_project()
        return self.project.is_project_admin(user)

    def get_project(self):
        return get_object_or_404(ClassificationProject, pk=self.kwargs.get("pk"))

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["project"] = self.project
        return kwargs

    def form_valid(self, form):
        user = self.request.user
        params = {
            "user": self.request.user,
            "collections": form.cleaned_data.get("collections"),
        }
        if settings.CELERY_ENABLED:
            task = celery_import_standard_attributes.delay(**params)
            user_task = UserTask(user=user, task_id=task.task_id)
            user_task.save()
            msg = _(
                "You have successfully run the action of importing standard attributes!"
            )
        else:
            msg = celery_import_standard_attributes(**params)
        status = True
        return self.render_json_response({"status": status, "msg": msg})

    def form_invalid(self, form):
        context = {
            "success": False,
            "msg": _("Your form contains errors."),
            "form_html": render_to_string(self.template_name, {"form": form}),
        }
        return self.render_json_response(context_dict=context)


view_classification_import_attrs = ClassificationImportAttrsView.as_view()


class ClassifyApproveView(LoginRequiredMixin, generic.View):
    """
    TODO: docstrings
    """

    model = Classification

    def post(self, request, *args, **kwargs):
        error_redirect = redirect(
            request.META.get("HTTP_REFERER")
            or reverse("media_classification:project_index")
        )
        user = request.user
        try:
            user_classification = UserClassification.objects.get(pk=kwargs["pk"])
        except UserClassification.DoesNotExist:
            messages.error(
                request=request, message=ClassifyMessages.MSG_CLASSIFICATION_MISSING
            )
            return error_redirect
        classification = user_classification.classification
        if not classification.can_approve(user=user):
            messages.error(request=request, message=ClassifyMessages.MSG_APPROVE_PERMS)
            return error_redirect
        # If there is no classificator assigned to this classification project
        # there is no reason to go further
        classificator = classification.project.classificator
        if not classificator:
            messages.error(
                request=request, message=ClassifyMessages.MSG_CLASSIFICATOR_MISSING
            )
            return error_redirect
        # now do the job
        classification.approve_user_classification(user_classification, user=user)
        messages.success(request=request, message=ClassifyMessages.MSG_SUCCESS_APPROVED)
        return redirect(
            reverse("media_classification:classify", kwargs={"pk": classification.pk})
        )


view_classify_approve = ClassifyApproveView.as_view()


class ClassificationImportView(LoginRequiredMixin, generic.FormView):
    """
    Imports classifications from observations.csv file.
    """

    template_name = "media_classification/classifications/classification_import.html"
    template_errors = "common/table_errors.html"
    form_class = ClassificationImportForm
    success_url = None

    def form_valid(self, form):
        params = {
            "user": self.request.user,
            "data": form.cleaned_data.get("observations_df"),
            "project": form.cleaned_data.get("project"),
            "approve": form.cleaned_data.get("approve"),
            "import_bboxes": form.cleaned_data.get("import_bboxes"),
            "import_expert_classifications": form.cleaned_data.get(
                "import_expert_classifications"
            ),
            "import_ai_classifications": form.cleaned_data.get(
                "import_ai_classifications"
            ),
            "overwrite_attrs": form.cleaned_data.get("overwrite_attrs"),
            "ai_provider": form.cleaned_data.get("ai_provider"),
        }
        # initialize classification importer to do table validation
        try:
            importer = ClassificationImporter(**params)
        except ValueError as e:
            return render(
                self.request,
                self.template_errors,
                {
                    "exception": e.args[0],
                    "provided_columns": ", ".join(e.args[1]),  # provided_columns
                    "required_columns": ", ".join(e.args[2]),  # required_columns
                },
            )

        # first validate a table with frictionless
        importer.validate_table()
        if not importer.report.valid:
            return render(
                self.request,
                self.template_errors,
                {
                    "report": importer.report.to_json(),
                    "provided_columns": ", ".join(importer.provided_columns),
                    "required_columns": ", ".join(importer.required_columns),
                },
            )
        params["data"] = importer.data

        if settings.CELERY_ENABLED:
            task = celery_import_classifications.delay(**params)
            user_task = UserTask(user=self.request.user, task_id=task.task_id)
            user_task.save()
            msg = _(
                "You have successfully run a celery task. Classifications are being imported now."
            )
        else:
            msg = celery_import_classifications(**params)
        messages.success(request=self.request, message=msg)
        self.success_url = reverse("accounts:dashboard")
        return super().form_valid(form)


view_classification_import = ClassificationImportView.as_view()


class ClassificationExportView(
    LoginRequiredMixin, UserPassesTestMixin, generic.FormView
):
    """
    This view generates a standardized camera trap data package.
    """

    template_name = "media_classification/classifications/classification_export.html"
    form_class = ClassificationExportForm
    success_url = None
    project = None
    raise_exception = True

    def test_func(self, user):
        """
        Check if user has enough permissions to export classifications.
        """
        self.project = self.get_project()
        return self.project.is_project_admin(user)

    def get_project(self):
        return get_object_or_404(ClassificationProject, pk=self.kwargs.get("pk"))

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["project"] = self.project
        return kwargs

    def form_valid(self, form):
        user = self.request.user
        if not self.project.classificator:
            messages.error(
                request=self.request,
                message=_("There is no classificator assigned to this project."),
            )
            self.success_url = "."
        else:
            params = {
                "data": form.cleaned_data,
                "user": user,
                "project": self.project,
                "host": self.request.build_absolute_uri("/"),
            }
            if settings.CELERY_ENABLED:
                task = celery_results_to_data_package.delay(**params)
                user_task = UserTask(user=user, task_id=task.task_id)
                user_task.save()
                msg = _(
                    "You have successfully run a celery task. The requested data package "
                    "is being generated now."
                )
            else:
                msg = celery_results_to_data_package(**params)
            messages.success(request=self.request, message=msg)
            self.success_url = reverse(
                "media_classification:project_detail", kwargs={"pk": self.project.pk}
            )
        return super().form_valid(form)


view_classification_export = ClassificationExportView.as_view()


class ClassificationPublishView(LoginRequiredMixin, generic.FormView):
    """
    This view publishes a standardized camera trap data package in one of the supported data hubs.
    """

    template_name = "media_classification/classifications/classification_publish.html"
    form_class = ClassificationPublishForm
    success_url = None
    raise_exception = True

    def form_valid(self, form):
        user = self.request.user
        params = {"data": form.cleaned_data, "user": user}
        if settings.CELERY_ENABLED:
            task = celery_publish_data_package.delay(**params)
            user_task = UserTask(user=user, task_id=task.task_id)
            user_task.save()
            msg = _(
                "You have successfully run a celery task. The selected data package "
                "is being published now."
            )
        else:
            msg = celery_publish_data_package(**params)
        self.success_url = reverse("accounts:dashboard")
        messages.success(request=self.request, message=msg)
        return super().form_valid(form)


view_classification_publish = ClassificationPublishView.as_view()


class ClassCollectionResourceAppendView(CollectionResourceAppendView):
    """
    This view is used to append a list of resources to a collection. User is required
    to have at least access permissions for each resource that is added to collection.
    """

    template_name = "forms/simple_crispy_form.html"
    form_class = CollectionResourceAppendForm


view_class_collection_resource_append = ClassCollectionResourceAppendView.as_view()


class ClassifyAIView(
    LoginRequiredMixin, UserPassesTestMixin, generic.FormView, JSONResponseMixin
):
    """"""

    template_name = "media_classification/classifications/classify/classify_ai.html"
    form_class = ClassifyAIForm
    success_url = "."
    project = None

    def test_func(self, user):
        """
        Check if user has enough permissions to send classifications for AI
        classification.
        """
        self.project = self.get_project()
        self.classification_class = self.get_classification_class()
        return self.project.is_project_admin(user)

    def get_project(self):
        return get_object_or_404(ClassificationProject, pk=self.kwargs.get("pk"))

    def get_classification_class(self):
        """
        Both Classification and AIClassification object pks can be sent to this view.
        Use url parameter obj ("classification" or "aiclassification") to determine which.
        """
        class_dict = {
            "classification": Classification,
            "aiclassification": AIClassification,
        }

        class_str = self.kwargs.get("obj", "classification")
        if class_str not in class_dict:
            class_str = "classification"

        return class_dict[class_str]

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(
            {
                # "host": self.request.build_absolute_uri("/"),
                "project_pk": self.project.pk,
                "classification_class": self.classification_class,
            }
        )
        return kwargs

    def form_valid(self, form):
        user = self.request.user
        ai_model = form.cleaned_data.get("ai_model")
        context = self.start_remote_ai_classification(ai_model, form, user)
        return self.render_json_response(context_dict=context)

    def start_remote_ai_classification(self, ai_model, form, user):
        manager = get_ai_provider_manager(ai_model)

        try:
            classifications = form.cleaned_data.get("classifications")
            classification_ids = [
                classification.pk for classification in classifications
            ]
            classify_preview_files = form.cleaned_data.get("classify_preview_files")

            params = RemoteClassificationParams(
                classification_ids=classification_ids,
                classify_preview_files=classify_preview_files,
                project_id=self.project.pk,
            )

            manager.request_classification(
                params=params,
                user_id=user.pk,
            )
            msg = _("You have successfully sent data for AI classification!")
            success = True

        except AIProviderException as e:
            success = False
            msg = str(e)
        context = {"success": success, "msg": msg}
        return context

    def form_invalid(self, form):
        """If form is not valid, form is re-rendered with error details,
        and message about unsuccessful operation is shown"""
        context = {
            "success": False,
            "msg": _("Your form contains errors."),
            "form_html": render_to_string(
                self.template_name,
                {"form": form},
            ),
        }
        return self.render_json_response(context_dict=context)


view_classify_ai = ClassifyAIView.as_view()


class ClassifyBboxView(LoginRequiredMixin, UserPassesTestMixin, generic.FormView):
    """
    TODO: docstrings
    """

    template_name = "media_classification/classifications/classify/classify_bbox.html"
    form_class = ClassifyBboxForm
    raise_exception = True
    success_url = None
    project = None
    classification = None
    resource = None
    next_obj = None

    def test_func(self, user):
        return self.project.can_view_classifications(user)

    def dispatch(self, request, *args, **kwargs):
        self.get_classification()
        if not self.resource.resource_type == "I":
            messages.info(
                request=self.request,
                message=_("The current implementation works only with images."),
            )
            return HttpResponseRedirect(self.request.META.get("HTTP_REFERER"))
        if not self.resource.file_preview.storage.exists(
            self.resource.file_preview.name
        ):
            messages.info(
                request=self.request,
                message=_("There is no preview file available for this image."),
            )
            return HttpResponseRedirect(self.request.META.get("HTTP_REFERER"))
        return super().dispatch(request, *args, **kwargs)

    def get_classification(self):
        """Return Classification object for given pk or return HTTP 404"""

        classification_pk = self.kwargs.get("classification_pk")
        if not classification_pk:
            classification_pk = self.kwargs.get("user_classification_pk")
            self.classification = get_object_or_404(
                UserClassification, pk=classification_pk
            )
            self.project = self.classification.classification.project
            self.resource = self.classification.classification.resource
            next_obj_qs = (
                UserClassification.objects.filter(
                    owner=self.request.user,
                    has_bboxes=False,
                    dynamic_attrs__isnull=False,
                    classification__collection=self.classification.classification.collection,
                )
                .exclude(pk=self.classification.pk)
                .order_by()
            )
            next_obj_qs_count = next_obj_qs.count()
            if next_obj_qs_count > 0:
                random_obj_id = random_choice(range(0, next_obj_qs_count))
                self.next_obj = next_obj_qs[random_obj_id]
                self.success_url = reverse(
                    "media_classification:classify_bbox_user",
                    kwargs={"user_classification_pk": self.next_obj.pk},
                )
            else:
                self.success_url = "."
        else:
            self.classification = get_object_or_404(
                Classification, pk=classification_pk
            )
            self.project = self.classification.project
            self.resource = self.classification.resource
            next_obj_qs = (
                Classification.objects.filter(
                    has_bboxes=False,
                    dynamic_attrs__isnull=False,
                    collection=self.classification.collection,
                )
                .exclude(pk=self.classification.pk)
                .order_by()
            )
            next_obj_qs_count = next_obj_qs.count()
            if next_obj_qs_count > 0:
                random_obj_id = random_choice(range(0, next_obj_qs_count))
                self.next_obj = next_obj_qs[random_obj_id]
                self.success_url = reverse(
                    "media_classification:classify_bbox",
                    kwargs={"classification_pk": self.next_obj.pk},
                )
            else:
                self.success_url = "."

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        dynamic_attrs = self.classification.dynamic_attrs.all()
        labels_ids = {}
        for i, dattr in enumerate(dynamic_attrs):
            labels_ids[f"bbox{i+1}"] = {"pk": dattr.pk, "bboxes": dattr.bboxes}
            dattr.label = f"bbox{i+1}"
        width, height = get_image_dimensions(self.resource.file_preview.file)
        context.update(
            {
                "classification": self.classification,
                "dynamic_attrs": dynamic_attrs,
                "labels_ids": json.dumps(labels_ids),
                "resource": self.resource,
                "img_width": width,
                "img_height": height,
                "input_method": "select",
            }
        )
        return context

    def form_valid(self, form):
        bboxes = form.cleaned_data["bboxes"]
        self.classification.has_bboxes = len(bboxes) > 0
        self.classification.save()
        bboxes_agg = {}
        for bbox in bboxes:
            if not bboxes_agg.get(bbox["pk"]):
                bboxes_agg[bbox["pk"]] = [bbox["bbox"]]
            else:
                bboxes_agg[bbox["pk"]].append(bbox["bbox"])
        dynamic_attrs = self.classification.dynamic_attrs.all()
        for dattr in dynamic_attrs:
            dattr.bboxes = []
            bboxes = bboxes_agg.get(dattr.pk)
            if bboxes:
                dattr.bboxes = bboxes
            else:
                dattr.bboxes = None
            dattr.save()
        if self.request.POST.get("save"):
            messages.success(
                request=self.request,
                message=_(
                    "The bounding boxes have been successfully saved in the database!"
                ),
            )
            self.success_url = "."
        if not self.next_obj and self.request.POST.get("next"):
            messages.info(
                request=self.request,
                message=_("All images in this collection already have bounding boxes!"),
            )
        return super().form_valid(form)


view_classify_bbox = ClassifyBboxView.as_view()


class ClassificationOverviewView(
    LoginRequiredMixin,
    generic.ListView,
):
    """
    List view of :class:`apps.media_classification.models.Classification` objects.
    Shows all Classification objects available to the user from all ClassificationProjects they can access.
    Allows filtering Classifications based on default classificator fields.
    """

    template_name = "media_classification/classifications/list.html"
    raise_exception = True

    def get_classification_url(self, **kwargs):
        return reverse("media_classification:api-classification-list")

    def build_filters(self):
        filter_definition = []
        obs_type_field = {
            "label": _("Observation"),
            "name": "observation_type",
            "field": "observation_type",
            "tag": {"name": "select", "is_block": True},
            "values": [("", _("All"))] + list(ObservationType.CHOICES),
        }
        filter_definition.append(obs_type_field)

        return filter_definition

    def get_context_data(self, *, object_list=None, **kwargs):
        classification_context = {
            "classification_overview": True,
            "data_url": self.get_classification_url(**kwargs),
            "model_name": "classifications",
            "hide_detail": True,
            "hide_update": True,
            "hide_delete": True,
            "filters": self.build_filters(),
        }
        context = {"classification_context": classification_context}
        return context

    def get_queryset(self):
        pass


view_classification_overview = ClassificationOverviewView.as_view()
