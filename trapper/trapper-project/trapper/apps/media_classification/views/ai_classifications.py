from braces.views import UserPassesTestMixin, JSONResponseMixin
from django.conf import settings
from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views import generic

from trapper.apps.accounts.models import UserTask
from trapper.apps.common.views import LoginRequiredMixin, BaseDeleteView
from trapper.apps.media_classification.forms import (
    BulkApproveAIClassificationForm,
    ApproveAIClassificationForm,
    CopyBboxesFromAIForm,
)
from trapper.apps.media_classification.models import (
    ClassificationProject,
    ClassificationProjectCollection,
    AIClassification,
    AIProvider,
)
from trapper.apps.media_classification.tasks import (
    celery_approve_ai_classifications,
    celery_copy_bboxes_from_ai,
)
from trapper.apps.media_classification.taxonomy import (
    ClassificationProjectRoleLevels,
    ClassifyMessages,
    ObservationType,
)


class AIClassificationListView(
    LoginRequiredMixin,
    UserPassesTestMixin,
    generic.ListView,
):
    """
    List view of :class:`apps.media_classification.models.Classification`
    instances. This list is always limited to a given classification project.
    """

    template_name = "media_classification/ai_classifications/list/list.html"
    raise_exception = True

    def get_ai_classification_context(self, **kwargs):
        project = kwargs.get("project", None)
        is_admin = project.is_project_admin(self.request.user)
        context = {
            "data_url": self.get_ai_classification_url(**kwargs),
            # "maps": MapManagerUtils.get_accessible(user=self.request.user),
            "project": project,
            "model_name": "ai classifications",
            "update_redirect": "true",
            "hide_delete": not is_admin,
            "is_admin": is_admin,
        }
        if project:
            context["collections"] = ClassificationProjectCollection.objects.filter(
                project=project
            ).values_list("pk", "collection__collection__name")

        context["ai_providers"] = AIProvider.objects.all().values_list("pk", "name")

        return context

    def get_ai_classification_url(self, **kwargs):
        """
        Alter url for classification DRF API to get only classifications
        that belong to given classification project.
        """
        project_pk = self.kwargs["project_id"]
        return "{url}?project={pk}".format(
            url=reverse("media_classification:api-ai-classification-list"),
            pk=project_pk,
        )

    def get_project(self) -> ClassificationProject:
        """
        Return classification project with given pk or return HTTP 404.
        """
        return get_object_or_404(ClassificationProject, pk=self.kwargs["project_id"])

    def test_func(self, user):
        """
        Check if user has enough permissions to view classifications.
        """
        return user.is_staff or self.get_project().can_view_ai_classifications(user)

    def build_filters(self):
        """
        Build a dynamic filters definition for the classification results list view.
        """
        filter_definition = []

        field = {
            "label": _("Observation"),
            "name": "observation_type",
            "field": "observation_type",
            "tag": {"name": "select", "is_block": True},
            "values": [("", _("All"))] + list(ObservationType.CHOICES),
        }
        filter_definition.append(field)
        return filter_definition

    def get_context_data(self, **kwargs):
        """
        Update context used to render template with classification context
        and filters.
        """
        project = self.get_project()
        context = {
            "ai_classification_context": self.get_ai_classification_context(
                project=project
            ),
        }
        context["ai_classification_context"]["filters"] = self.build_filters()
        context["ai_classification_context"]["update_redirect"] = False
        return context

    def get_queryset(self, **kwargs):
        pass


view_ai_classification_list = AIClassificationListView.as_view()


class BulkApproveAIClassificationView(
    LoginRequiredMixin, UserPassesTestMixin, generic.FormView, JSONResponseMixin
):
    """
    TODO: docstrings
    """

    # http_method_names = ["post"]
    template_name = "forms/simple_crispy_form.html"
    form_class = BulkApproveAIClassificationForm
    raise_exception = True
    project = None

    def get_project(self):
        """Return classification project for given pk or return HTTP 404"""
        return get_object_or_404(ClassificationProject, pk=self.kwargs["project_id"])

    def test_func(self, user):
        """Check if user has enough permissions to aprove AI classifications"""
        self.project = self.get_project()
        return user.is_staff or self.project.is_project_admin(user)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"project": self.project})
        return kwargs

    def raise_exception_handler(self, request, *args, **kwargs):
        context = {"success": False, "msg": "Authentication required"}
        return self.render_json_response(context)

    def approve_classifications(self, form):
        task = celery_approve_ai_classifications.delay(
            user=self.request.user,
            project_id=form.project.pk,
            ai_classification_pks=form.cleaned_data["ai_classification_pks"],
            fields_to_copy=form.cleaned_data["fields_to_copy"],
            mark_as_approved=form.cleaned_data["mark_as_approved"],
            minimum_confidence=form.cleaned_data["minimum_confidence"],
            overwrite_attrs=form.cleaned_data["overwrite_attrs"],
        )
        user_task = UserTask(user=self.request.user, task_id=task.task_id)
        user_task.save()
        return {
            "success": True,
            "msg": _("You have successfully run the classifications approve action!"),
        }

    def form_valid(self, form):
        error = form.cleaned_data.get("error", None)
        if not error:
            context = self.approve_classifications(form)
        else:
            context = {
                "success": False,
                "msg": error,
            }
        return self.render_json_response(context_dict=context)


view_ai_classification_bulk_approve = BulkApproveAIClassificationView.as_view()


class AIClassificationDeleteView(BaseDeleteView):
    """
    TODO: docstrings
    """

    model = AIClassification
    model_name = "AI classification"

    def get_redirect_url(self):
        return redirect(self.request.META["HTTP_REFERER"])

    def filter_editable(self, queryset, user):
        return self.model.objects.get_accessible(
            user=user,
            base_queryset=queryset,
            role_levels=[ClassificationProjectRoleLevels.ADMIN],
        )


view_ai_classification_delete = AIClassificationDeleteView.as_view()


class ClassifyApproveAIView(
    LoginRequiredMixin, UserPassesTestMixin, generic.FormView, JSONResponseMixin
):
    template_name = "forms/simple_crispy_form.html"
    form_class = ApproveAIClassificationForm
    raise_exception = True
    ai_classification = None

    def get_ai_classification(self):
        """Return classification project for given pk or return HTTP 404"""
        return get_object_or_404(
            AIClassification, pk=self.kwargs["ai_classification_id"]
        )

    def test_func(self, user):
        """Check if user has enough permissions to aprove AI classifications"""
        self.ai_classification = self.get_ai_classification()
        project = self.ai_classification.classification.project
        return user.is_staff or project.is_project_admin(user)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(
            {
                "ai_classification": self.ai_classification,
            }
        )
        return kwargs

    def raise_exception_handler(self, request, *args, **kwargs):
        context = {"status": False, "msg": "Authentication required"}
        return self.render_json_response(context)

    def approve_classifications(self, form):
        self.ai_classification.classification.approve_ai_classification(
            ai_classification=self.ai_classification,
            fields_to_copy=form.cleaned_data["fields_to_copy"],
            minimum_confidence=form.cleaned_data["minimum_confidence"],
            mark_as_approved=form.cleaned_data["mark_as_approved"],
            overwrite_attrs=form.cleaned_data["overwrite_attrs"],
            classificator=self.ai_classification.classificator,
            user=self.request.user,
            commit=True,
        )

    def form_valid(self, form):
        def make_error_response(msg: str):
            return self.render_json_response(
                context_dict={
                    "success": False,
                    "msg": error,
                }
            )

        # If there is no classificator assigned to this classification project
        # there is no reason to go further
        classificator = self.ai_classification.classificator
        if not classificator:
            return make_error_response(ClassifyMessages.MSG_CLASSIFICATOR_MISSING)

        error = form.cleaned_data.get("error", None)

        if error:
            messages.error(request=self.request, message=error)
            return make_error_response(error)

        self.ai_classification.classification.approve_ai_classification(
            ai_classification=self.ai_classification,
            fields_to_copy=form.cleaned_data["fields_to_copy"],
            minimum_confidence=form.cleaned_data["minimum_confidence"],
            mark_as_approved=form.cleaned_data["mark_as_approved"],
            overwrite_attrs=form.cleaned_data["overwrite_attrs"],
            classificator=classificator,
            user=self.request.user,
            commit=True,
        )

        return self.render_json_response(
            context_dict={
                "success": True,
                "msg": ClassifyMessages.MSG_SUCCESS_APPROVED,
            }
        )


view_classify_ai_approve = ClassifyApproveAIView.as_view()


class CopyBboxesFromAIView(
    LoginRequiredMixin, UserPassesTestMixin, generic.FormView, JSONResponseMixin
):
    template_name = "media_classification/ai_classifications/copy_bboxes_form.html"
    raise_exception = True
    project = None
    form_class = CopyBboxesFromAIForm

    def get_project(self):
        """Return classification project for given pk or return HTTP 404"""
        return get_object_or_404(ClassificationProject, pk=self.kwargs["project_id"])

    def test_func(self, user):
        """Check if user has enough permissions to copy bboxes from AI Classifications"""
        self.project = self.get_project()
        return user.is_superuser or self.project.is_project_admin(user)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"project": self.project})
        return kwargs

    def copy_bboxes_from_ai(self, form):
        params = {
            "user": self.request.user,
            "project_id": self.project.pk,
            "ai_classification_pks": form.cleaned_data["ai_classification_pks"],
            "match_fields": form.cleaned_data["match_fields"],
            "overwrite_bboxes": form.cleaned_data["overwrite_bboxes"],
        }
        if settings.CELERY_ENABLED:
            task = celery_copy_bboxes_from_ai.delay(**params)
            user_task = UserTask(user=self.request.user, task_id=task.task_id)
            user_task.save()
        else:
            celery_copy_bboxes_from_ai(**params)
        return {
            "success": True,
            "msg": _(
                "You have successfully run the action to copy bboxes from AI Classifications!"
            ),
        }

    def form_valid(self, form):
        error = form.cleaned_data.get("error", None)
        if not error:
            context = self.copy_bboxes_from_ai(form)
        else:
            context = {
                "success": False,
                "msg": error,
            }
        return self.render_json_response(context_dict=context)


view_copy_bboxes_from_ai = CopyBboxesFromAIView.as_view()
