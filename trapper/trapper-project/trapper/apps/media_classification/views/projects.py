# -*- coding: utf-8 -*-
"""
Views used to handle logic related to classification projects management in
media classification application
"""
from braces.views import UserPassesTestMixin, JSONResponseMixin
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.http import HttpResponseRedirect
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views import generic
from extra_views import CreateWithInlinesView, UpdateWithInlinesView, NamedFormsetsMixin

from trapper.apps.accounts.models import UserTask
from trapper.apps.media_classification.tasks import celery_blur_humans
from trapper.apps.research.models import ResearchProject
from trapper.apps.common.views import BaseDeleteView, LoginRequiredMixin
from trapper.apps.media_classification.forms import (
    ProjectForm,
    ProjectRoleInline,
    ProjectRoleCreateInline,
    ProjectCollectionAddForm,
    ProjectCollectionBlurHumansForm,
)
from trapper.apps.media_classification.models import (
    ClassificationProject,
    ClassificationProjectCollection,
)
from trapper.apps.storage.views.collection import CollectionGridContextMixin

User = get_user_model()


class ClassificationProjectGridContextMixin:
    """Mixin used with views that use any classificator listing
    for changing list behaviour.
    This mixin can be used to:

    * change classification project url (i.e. add filtering)
    """

    def get_classification_project_url(self, **kwargs):
        """Return standard DRF API url for classification projects"""
        return reverse("media_classification:api-classification-project-list")

    def get_classification_project_context(self, **kwargs):
        """Build classification project context"""
        context = {
            "data_url": self.get_classification_project_url(**kwargs),
            "model_name": "classification projects",
            "update_redirect": "true",
            "sortable": [
                "name",
            ],
            "research_projects": ResearchProject.objects.get_accessible(
                user=self.request.user
            )
            .distinct()
            .values_list("pk", "acronym"),
        }

        return context


class ProjectListView(
    LoginRequiredMixin, generic.ListView, ClassificationProjectGridContextMixin
):
    """List view of the
    :class:`apps.media_classification.models.ClassificationProject` instances.

    Only classification project that user has enough permissions to view
    are displayed.

    Anonymous users can see only projects that have crowdsourcing enabled.
    """

    model = ClassificationProject
    context_object_name = "projects"
    template_name = "media_classification/projects/list.html"

    def get_context_data(self, **kwargs):
        """Update context used to render template with classificator context
        and owners used for filtering"""
        project_context = self.get_classification_project_context()
        context = {
            "classification_project_context": project_context,
        }
        return context

    def get_queryset(self, *args, **kwargs):
        """Limit classification project only to those, which user can view
        details for"""
        base_queryset = super().get_queryset()
        projects = self.model.objects.get_accessible(
            user=self.request.user, base_queryset=base_queryset
        )
        return projects


view_project_list = ProjectListView.as_view()


class ProjectDetailView(
    LoginRequiredMixin,
    UserPassesTestMixin,
    generic.DetailView,
    CollectionGridContextMixin,
):
    """View used for rendering details of specified classification project.
    User is required to have enough permissions to view project details.
    """

    model = ClassificationProject
    context_object_name = "project"
    template_name = "media_classification/projects/detail.html"
    raise_exception = True

    def test_func(self, user):
        """Verify that user has enough permissions to view details"""
        return self.get_object().can_view(user)

    def get_collection_url(self, **kwargs):
        """Alter url for collections DRF API, to get only collections that
        belongs to research project and are accessible for currently logged in
        user"""
        classification_project = kwargs.get("classification_project")
        return reverse(
            "media_classification:api-classification-project-collection-list",
            kwargs={"project_pk": classification_project.pk},
        )

    def get_context_data(self, **kwargs):
        """Update context data with owners of classification projects
        for filtering"""
        context = super().get_context_data(**kwargs)
        context["collection_context"] = self.get_collection_context(
            classification_project=context["object"]
        )
        context["collection_context"]["collection_field"] = "collection_pk"
        context["collection_context"]["hide_delete"] = True
        context["collection_context"]["hide_update"] = True
        context["collection_context"]["model_name"] = "project collections"
        return context


view_project_detail = ProjectDetailView.as_view()


class ProjectCreateView(LoginRequiredMixin, CreateWithInlinesView, NamedFormsetsMixin):
    """ClassificationProject's create view.
    Handle the creation of the
    :class:`apps.media_classification.models.ClassificationProject` objects.
    """

    model = ClassificationProject
    form_class = ProjectForm
    template_name = "media_classification/projects/change.html"
    inlines = [
        ProjectRoleCreateInline,
    ]
    inlines_names = [
        "projectrole_formset",
    ]

    def get_form_kwargs(self):
        """List of research project collections that should be assigned to
        created collection are passed through `request.GET.selected` key as
        list of integers separated by comma"""
        kwargs = super().get_form_kwargs()
        kwargs["initial"]["selected"] = self.request.GET.get("selected", None)
        return kwargs

    def forms_valid(self, form, inlines):
        """If form is valid then set `owner` as currently logged in user,
        and add message that classification project has been created

        After that all roles selected in creation form are saved into database
        """
        self.object = form.save(commit=False)
        self.object.owner = self.request.user
        self.object.save()

        projectrole_formset = inlines[0]
        projectrole_formset.save()

        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(
                f"The classification project {form.instance.name} has been successfully created!"
            ),
        )
        return HttpResponseRedirect(self.get_success_url())

    def forms_invalid(self, form, inlines):
        """If form is not valid, form is re-rendered with error details,
        and message about unsuccessfull operation is shown"""
        messages.add_message(
            self.request, messages.ERROR, _("Error creating new classification project")
        )
        return super().forms_invalid(form, inlines)


view_project_create = ProjectCreateView.as_view()


class ProjectUpdateView(
    LoginRequiredMixin, UserPassesTestMixin, UpdateWithInlinesView, NamedFormsetsMixin
):
    """ClassificationProject's update view.
    Handle the update of the
    :class:`apps.media_classification.models.ClassificationProject` objects.
    """

    model = ClassificationProject
    form_class = ProjectForm
    template_name = "media_classification/projects/change.html"
    inlines = [
        ProjectRoleInline,
    ]
    inlines_names = [
        "projectrole_formset",
    ]
    raise_exception = True

    def test_func(self, user):
        """Update is available only for users that have enough permissions"""
        return self.get_object().can_update(user)

    def forms_valid(self, form, inlines):
        """If form is valid then set `owner` as currently logged in user,
        and add message that classification project has been created

        After that all roles selected in creation form are saved into database
        """
        self.object = form.save(commit=False)
        self.object.owner = self.request.user
        self.object.save()
        # update project roles
        self.object.classification_project_roles.all().delete()
        projectrole_formset = inlines[0]
        for projectrole_form in projectrole_formset:
            projectrole_form.save()
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(
                f"The classification project {form.instance.name} has been successfully updated."
            ),
        )
        return HttpResponseRedirect(self.get_success_url())

    def forms_invalid(self, form, inlines):
        """If form is not valid, form is re-rendered with error details,
        and message about unsuccessfull operation is shown"""
        messages.add_message(
            self.request, messages.ERROR, _("Error updating classification project")
        )
        return super().forms_invalid(form, inlines)


view_project_update = ProjectUpdateView.as_view()


class ProjectDeleteView(BaseDeleteView):
    """View responsible for handling deletion of single or multiple
    classification projects.

    Only projects that user has enough permissions for can be deleted
    """

    model = ClassificationProject
    redirect_url = "media_classification:project_list"

    def bulk_delete(self, queryset):
        """
        Overwrite this method to use model's delete method which checks each
        single project if it should be disabled instead of deleted.
        """
        for project in queryset:
            project.delete()


view_project_delete = ProjectDeleteView.as_view()


class ProjectCollectionDeleteView(BaseDeleteView):
    """View responsible for handling deletion of single or multiple
    classification project collections.

    Only collections that user has enough permissions for can be deleted
    """

    model = ClassificationProjectCollection
    success_msg_tmpl = _('Classification project collection "{name}" has been deleted')
    fail_msg_tmpl = _('You cannot remove classification project collection "{name}"')
    redirect_url = "media_classification:project_list"

    def filter_editable(self, queryset, user):
        """Overwrite this method to check if user is an admin of a project
        that selected collections belong to"""
        to_delete = []
        for obj in queryset:
            if obj.can_delete(user):
                to_delete.append(obj)
        return to_delete

    def bulk_delete(self, queryset):
        for obj in queryset:
            obj.delete()

    def add_message(self, status, template, item):
        """ClassificationProjectCollection has no `name` attribute so we have
        to overwrite this method"""
        messages.add_message(
            self.request, status, template.format(name=item.collection.collection.name)
        )


view_project_collection_delete = ProjectCollectionDeleteView.as_view()


class ProjectCollectionBlurHumans(
    LoginRequiredMixin, generic.FormView, JSONResponseMixin
):
    template_name = "forms/simple_crispy_form.html"
    form_class = ProjectCollectionBlurHumansForm
    success_url = "."

    def form_valid(self, form):
        project_collections_pks = form.cleaned_data.get("pks")
        params = {
            "pks": project_collections_pks,
            "user": self.request.user,
            "exclude_blurred": form.cleaned_data.get("exclude_blurred", True),
        }
        if settings.CELERY_ENABLED:
            task = celery_blur_humans.delay(**params)
            user_task = UserTask(user=self.request.user, task_id=task.task_id)
            user_task.save()
        else:
            celery_blur_humans(**params)

        context = {
            "success": True,
            "msg": _(
                "You have successfully started celery task to blur humans in resources."
            ),
        }
        return self.render_json_response(context_dict=context)

    def form_invalid(self, form):
        context = {
            "success": False,
            "msg": _("Your form contains errors."),
        }
        return self.render_json_response(context_dict=context)


view_project_collection_blur_humans = ProjectCollectionBlurHumans.as_view()


class ProjectCollectionAddView(LoginRequiredMixin, generic.FormView, JSONResponseMixin):
    """
    View used to add collections to existing research project

    User is required to have at least project update permissions to
    add collections, and each collection has to be accessible by user.
    """

    template_name = "forms/simple_crispy_form.html"
    form_class = ProjectCollectionAddForm
    raise_exception = True

    def form_valid(self, form):
        """"""
        error = form.cleaned_data.get("error", None)
        if not error:
            new_collections = form.cleaned_data.get("new_collections", None)
            project = form.cleaned_data.get("project", None)
            for k in new_collections:
                obj = ClassificationProjectCollection(project=project, collection=k)
                obj.save()
            msg = _(
                f"You have successfully added {new_collections.count()} new collections to the"
                "selected classification project."
            )
            context = {
                "success": True,
                "msg": msg,
            }
        else:
            context = {
                "success": False,
                "msg": error,
            }
        return self.render_json_response(context_dict=context)

    def form_invalid(self, form):
        """If form is not valid, form is re-rendered with error details,
        and message about unsuccessfull operation is shown"""
        context = {
            "success": False,
            "msg": _("Your form contain errors"),
            "form_html": render_to_string(
                self.template_name, {"form": form}, request=self.request
            ),
        }
        return self.render_json_response(context_dict=context)


view_project_collection_add = ProjectCollectionAddView.as_view()
