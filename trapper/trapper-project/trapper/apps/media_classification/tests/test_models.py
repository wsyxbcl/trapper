from unittest.mock import patch, Mock

from django.contrib.auth.models import AnonymousUser
from django.test import TestCase
from django.urls import reverse
from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.extra_tables.models import Species
from trapper.apps.geomap.tests.factories.deployment import DeploymentFactory
from trapper.apps.geomap.tests.factories.location import LocationFactory
from trapper.apps.media_classification.models import (
    get_ordered_values,
    ClassificationProject,
    ClassificationProjectCollection,
    Classificator,
    Classification,
    AIProvider,
    AIClassification,
)
from trapper.apps.media_classification.taxonomy import (
    ClassificationProjectStatus,
    ClassificationProjectRoleLevels,
    ClassificationStatus,
    ObservationType,
    SpeciesSex,
    SpeciesAge,
)
from trapper.apps.media_classification.tests.factories.classification import (
    ClassificationFactory,
)
from trapper.apps.media_classification.tests.factories.classification_project_collection import (
    ClassificationProjectCollectionFactory,
)
from trapper.apps.media_classification.tests.factories.classification_project_role import (
    ClassificationProjectRoleFactory,
)
from trapper.apps.media_classification.tests.factories.classificator import (
    ClassificatorFactory,
)
from trapper.apps.media_classification.tests.factories.user_classification import (
    UserClassificationFactory,
)
from trapper.apps.research.models import ResearchProjectCollection
from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)
from trapper.apps.storage.models import Resource
from trapper.apps.storage.taxonomy import ResourceStatus, CollectionStatus
from trapper.apps.storage.tests.factories.classification_project import (
    ClassificationProjectFactory,
)
from trapper.apps.storage.tests.factories.collection import CollectionFactory
from trapper.apps.storage.tests.factories.resource import ResourceFactory


class ClassificationProjectTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(ClassificationProjectTest, cls).setUpTestData()

        cls.user1 = UserFactory()
        cls.user2 = UserFactory()
        cls.user3 = UserFactory()
        cls.user4 = UserFactory()

        cls.research_project = ResearchProjectFactory(owner=cls.user1)
        cls.location = LocationFactory(
            owner=cls.user1, research_project=cls.research_project
        )
        cls.deployment = DeploymentFactory(
            owner=cls.user1,
            location=cls.location,
            research_project=cls.research_project,
        )
        ResourceFactory.create_batch(
            2, owner=cls.user1, deployment=cls.deployment, status=ResourceStatus.PRIVATE
        )
        cls.collection = CollectionFactory(
            owner=cls.user1, status=CollectionStatus.PRIVATE
        )
        cls.collection.resources.set(Resource.objects.all())
        cls.classificator = ClassificatorFactory(owner=cls.user1)

        cls.classification_project = ClassificationProjectFactory(
            research_project=cls.research_project,
            owner=cls.user1,
            classificator=cls.classificator,
        )

        resources_array = Resource.objects.all()
        cls.research_project.collections.set([cls.collection])

        collection = ResearchProjectCollection.objects.filter(
            project=cls.classification_project.research_project.pk
        )[0]
        classification_project_collection = ClassificationProjectCollectionFactory(
            project=cls.classification_project, collection=collection
        )

        for resource in resources_array:
            ClassificationFactory(
                resource=resource,
                owner=cls.user1,
                project=cls.classification_project,
                collection=classification_project_collection,
            )

        ClassificationProjectRoleFactory(
            name=ClassificationProjectRoleLevels.ADMIN,
            classification_project=cls.classification_project,
            user=cls.user2,
        )
        ClassificationProjectRoleFactory(
            name=ClassificationProjectRoleLevels.EXPERT,
            classification_project=cls.classification_project,
            user=cls.user3,
        )

    def test_get_ordered_values(self):
        keys = ["key1", "key2"]
        dictionary = {"key2": "value2", "key1": "value1"}
        get_ordered_values(keys, dictionary)

    def test_classification_project_str(self):
        print(self.classification_project)

    def test_classification_project_is_finished(self):
        self.classification_project.status = ClassificationProjectStatus.FINISHED
        self.assertTrue(self.classification_project.is_finished())

        self.classification_project.status = ClassificationProjectStatus.ONGOING
        self.assertFalse(self.classification_project.is_finished())

    def test_classification_project_is_active(self):
        self.classification_project.status = ClassificationProjectStatus.ONGOING
        self.assertTrue(self.classification_project.is_active())

        self.classification_project.status = ClassificationProjectStatus.FINISHED
        self.assertFalse(self.classification_project.is_active())

    def test_classification_project_get_classification_status(self):
        self.classification_project.get_classification_status()
        self.classification_project.get_classification_status(as_label=True)

    def test_classification_project_get_classification_stats(self):
        self.classification_project.get_classification_stats()

    def test_classification_project_get_roles(self):
        self.classification_project.get_roles()

    def test_classification_project_classificator_removed(self):
        self.assertFalse(self.classification_project.classificator_removed)

    def test_classification_project_is_project_admin(self):
        classification_project = ClassificationProject.objects.get(
            pk=self.classification_project.pk
        )
        self.assertTrue(classification_project.is_project_admin(self.user2))
        classification_project = ClassificationProject.objects.get(
            pk=self.classification_project.pk
        )
        self.assertFalse(classification_project.is_project_admin(self.user3))
        classification_project = ClassificationProject.objects.get(
            pk=self.classification_project.pk
        )
        self.assertFalse(classification_project.is_project_admin(AnonymousUser()))

    def test_classification_project_is_project_expert(self):
        classification_project = ClassificationProject.objects.get(
            pk=self.classification_project.pk
        )
        self.assertTrue(classification_project.is_project_expert(self.user3))
        classification_project = ClassificationProject.objects.get(
            pk=self.classification_project.pk
        )
        self.assertFalse(classification_project.is_project_expert(self.user2))
        classification_project = ClassificationProject.objects.get(
            pk=self.classification_project.pk
        )
        self.assertFalse(classification_project.is_project_expert(AnonymousUser()))

    def test_classification_project_get_user_roles(self):
        self.assertEqual(
            self.classification_project.get_user_roles(self.user3), ["Expert"]
        )
        self.assertEqual(
            self.classification_project.get_user_roles(self.user2), ["Admin"]
        )

    def test_classification_project_can_view(self):
        self.assertTrue(self.classification_project.can_view(self.user2))
        self.assertTrue(self.classification_project.can_view(self.user3))
        self.assertFalse(self.classification_project.can_view(AnonymousUser()))

    def test_classification_project_can_update(self):
        self.assertTrue(self.classification_project.can_update(self.user2))
        self.assertFalse(self.classification_project.can_update(self.user3))
        self.assertFalse(self.classification_project.can_update(AnonymousUser()))

    def test_classification_project_can_delete(self):
        self.assertTrue(self.classification_project.can_delete(self.user2))
        self.assertFalse(self.classification_project.can_delete(self.user3))
        self.assertFalse(self.classification_project.can_delete(AnonymousUser()))

    def test_classification_project_can_change_sequence(self):
        self.assertTrue(self.classification_project.can_change_sequence(self.user2))
        self.assertTrue(self.classification_project.can_change_sequence(self.user3))
        self.assertFalse(
            self.classification_project.can_change_sequence(AnonymousUser())
        )

    def test_classification_project_is_sequencing_enabled(self):
        self.assertTrue(self.classification_project.is_sequencing_enabled(self.user2))
        self.assertTrue(self.classification_project.is_sequencing_enabled(self.user3))
        self.assertTrue(
            self.classification_project.is_sequencing_enabled(AnonymousUser())
        )

    def test_classification_project_can_view_classifications(self):
        self.assertTrue(
            self.classification_project.can_view_classifications(self.user2)
        )
        self.assertFalse(
            self.classification_project.can_view_classifications(self.user3)
        )

    def test_classification_project_get_absolute_url(self):
        self.assertEqual(
            self.classification_project.get_absolute_url(),
            reverse(
                "media_classification:project_detail",
                kwargs={"pk": self.classification_project.pk},
            ),
        )

    def test_classification_project_api_update_context(self):
        ClassificationProject.objects.api_update_context(
            self.classification_project, self.user2
        )
        ClassificationProject.objects.api_update_context(
            self.classification_project, AnonymousUser()
        )

    def test_classification_project_api_detail_context(self):
        ClassificationProject.objects.api_detail_context(
            self.classification_project, self.user2
        )
        ClassificationProject.objects.api_detail_context(
            self.classification_project, AnonymousUser()
        )

    def test_classification_project_api_delete_context(self):
        ClassificationProject.objects.api_delete_context(
            self.classification_project, self.user2
        )
        ClassificationProject.objects.api_delete_context(
            self.classification_project, AnonymousUser()
        )

    def test_classification_project_get_accessible(self):
        ClassificationProject.objects.get_accessible(user=self.user2)
        ClassificationProject.objects.get_accessible(user=AnonymousUser())


class ClassificationProjectCollectionTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(ClassificationProjectCollectionTest, cls).setUpTestData()

        cls.user1 = UserFactory()
        cls.user2 = UserFactory()
        cls.user3 = UserFactory()
        cls.user4 = UserFactory()

        cls.research_project = ResearchProjectFactory(owner=cls.user1)
        cls.location = LocationFactory(
            owner=cls.user1, research_project=cls.research_project
        )
        cls.deployment = DeploymentFactory(
            owner=cls.user1,
            location=cls.location,
            research_project=cls.research_project,
        )
        cls.resource1 = ResourceFactory(
            owner=cls.user1, deployment=cls.deployment, status=ResourceStatus.PRIVATE
        )

        ResourceFactory(
            owner=cls.user1, deployment=cls.deployment, status=ResourceStatus.PRIVATE
        )

        cls.collection = CollectionFactory(
            owner=cls.user1, status=CollectionStatus.PRIVATE
        )
        cls.collection.resources.set(Resource.objects.all())
        cls.classificator = ClassificatorFactory(owner=cls.user1)

        cls.classification_project = ClassificationProjectFactory(
            research_project=cls.research_project,
            owner=cls.user1,
            classificator=cls.classificator,
        )

        resources_array = Resource.objects.all()
        cls.research_project.collections.set([cls.collection])

        collection = ResearchProjectCollection.objects.filter(
            project=cls.classification_project.research_project.pk
        )[0]
        cls.classification_project_collection = ClassificationProjectCollectionFactory(
            project=cls.classification_project, collection=collection
        )

        for resource in resources_array:
            ClassificationFactory(
                resource=resource,
                owner=cls.user1,
                project=cls.classification_project,
                collection=cls.classification_project_collection,
            )

        cls.classification_project_role1 = ClassificationProjectRoleFactory(
            name=ClassificationProjectRoleLevels.ADMIN,
            classification_project=cls.classification_project,
            user=cls.user2,
        )
        ClassificationProjectRoleFactory(
            name=ClassificationProjectRoleLevels.EXPERT,
            classification_project=cls.classification_project,
            user=cls.user3,
        )

    def test_classification_project_collection_str(self):
        print(self.classification_project_collection)

    def test_classification_project_collection_get_resources(self):
        self.classification_project_collection.get_resources()

    def test_classification_project_collection_get_unique_locations(self):
        self.classification_project_collection.get_unique_locations()

    def test_classification_project_collection_can_delete(self):
        self.assertTrue(
            self.classification_project_collection.can_delete(user=self.user2)
        )
        self.assertFalse(
            self.classification_project_collection.can_delete(user=self.user3)
        )

    def test_classification_project_collection_can_view(self):
        self.assertFalse(
            self.classification_project_collection.can_view(user=self.user2)
        )
        self.assertFalse(
            self.classification_project_collection.can_view(user=self.user3)
        )

    def test_classification_project_collection_can_update(self):
        self.assertFalse(
            self.classification_project_collection.can_update(user=self.user2)
        )
        self.assertFalse(
            self.classification_project_collection.can_update(user=self.user3)
        )

    def test_classification_project_collection_can_classify(self):
        self.assertTrue(
            self.classification_project_collection.can_classify(user=self.user2)
        )
        self.assertEqual(
            self.classification_project_collection.can_classify(user=self.user3),
            self.classificator,
        )

    def test_classification_project_collection_get_resources_classification_status(
        self,
    ):
        self.classification_project_collection.get_resources_classification_status(
            [self.resource1]
        )

    def test_classification_project_collection_is_sequencing_enabled(self):
        self.assertTrue(
            self.classification_project_collection.is_sequencing_enabled(
                user=self.user2
            )
        )
        self.assertTrue(
            self.classification_project_collection.is_sequencing_enabled(
                user=self.user3
            )
        )

    def test_classification_project_collection_rebuild_classifications(self):
        self.classification_project_collection.rebuild_classifications()

    def test_classification_project_collection_api_detail_context(self):
        ClassificationProjectCollection.objects.api_detail_context(
            self.classification_project_collection, user=self.user1
        )
        ClassificationProjectCollection.objects.api_detail_context(
            self.classification_project_collection, user=AnonymousUser()
        )

    def test_classification_project_collection_api_delete_context(self):
        ClassificationProjectCollection.objects.api_delete_context(
            self.classification_project_collection, user=self.user1
        )
        ClassificationProjectCollection.objects.api_delete_context(
            self.classification_project_collection, user=AnonymousUser()
        )

    def test_classification_project_collection_api_classify_context(self):
        ClassificationProjectCollection.objects.api_classify_context(
            self.classification_project_collection, user=self.user1
        )
        ClassificationProjectCollection.objects.api_classify_context(
            self.classification_project_collection, user=AnonymousUser()
        )


class ClassificatorTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(ClassificatorTest, cls).setUpTestData()

        cls.user1 = UserFactory()
        cls.user2 = UserFactory()
        cls.user3 = UserFactory()
        cls.user4 = UserFactory()

        cls.research_project = ResearchProjectFactory(owner=cls.user1)
        cls.location = LocationFactory(
            owner=cls.user1, research_project=cls.research_project
        )
        cls.deployment = DeploymentFactory(
            owner=cls.user1,
            location=cls.location,
            research_project=cls.research_project,
        )
        cls.resource1 = ResourceFactory(
            owner=cls.user1, deployment=cls.deployment, status=ResourceStatus.PRIVATE
        )

        ResourceFactory(
            owner=cls.user1, deployment=cls.deployment, status=ResourceStatus.PRIVATE
        )

        cls.collection = CollectionFactory(
            owner=cls.user1, status=CollectionStatus.PRIVATE
        )
        cls.collection.resources.set(Resource.objects.all())
        cls.classificator = ClassificatorFactory(owner=cls.user1)

        cls.classification_project = ClassificationProjectFactory(
            research_project=cls.research_project,
            owner=cls.user1,
            classificator=cls.classificator,
        )

        resources_array = Resource.objects.all()
        cls.research_project.collections.set([cls.collection])

        collection = ResearchProjectCollection.objects.filter(
            project=cls.classification_project.research_project.pk
        )[0]
        cls.classification_project_collection = ClassificationProjectCollectionFactory(
            project=cls.classification_project, collection=collection
        )

        for resource in resources_array:
            ClassificationFactory(
                resource=resource,
                owner=cls.user1,
                project=cls.classification_project,
                collection=cls.classification_project_collection,
            )

        cls.classification_project_role1 = ClassificationProjectRoleFactory(
            name=ClassificationProjectRoleLevels.ADMIN,
            classification_project=cls.classification_project,
            user=cls.user2,
        )
        ClassificationProjectRoleFactory(
            name=ClassificationProjectRoleLevels.EXPERT,
            classification_project=cls.classification_project,
            user=cls.user3,
        )

    def test_classificator_str(self):
        print(self.classificator)

    def test_classificator_delete(self):
        classificator = ClassificatorFactory(owner=self.user1)
        self.assertTrue(Classificator.objects.filter(pk=classificator.pk).exists())
        classificator.delete()
        self.assertFalse(Classificator.objects.filter(pk=classificator.pk).exists())

    @patch(
        "trapper.apps.media_classification.models.Classificator.get_classification_status"
    )
    def test_classificator_delete_(self, mock_status):
        mock_status.return_value = True
        classificator = ClassificatorFactory(owner=self.user1)
        self.assertTrue(Classificator.objects.filter(pk=classificator.pk).exists())
        self.assertFalse(classificator.delete())
        self.assertTrue(Classificator.objects.filter(pk=classificator.pk).exists())

    def test_classificator_get_classification_status(self):
        self.assertFalse(self.classificator.get_classification_status())

    @patch(
        "trapper.apps.media_classification.models.ClassificationProject.get_classification_status"
    )
    def test_classificator_get_classification_status_(self, mock_status):
        mock_status.return_value = True
        self.assertTrue(self.classificator.get_classification_status())

    def test_classificator_get_absolute_url(self):
        self.assertEqual(
            self.classificator.get_absolute_url(),
            reverse(
                "media_classification:classificator_detail",
                kwargs={"pk": self.classificator.pk},
            ),
        )

    def test_classificator_can_view(self):
        self.assertTrue(self.classificator.can_view())
        self.assertTrue(self.classificator.can_view(user=self.user3))

    def test_classificator_can_update(self):
        self.assertTrue(self.classificator.can_update(user=self.user1))
        self.assertFalse(self.classificator.can_update(user=self.user2))

    def test_classificator_can_delete(self):
        self.assertTrue(self.classificator.can_delete(user=self.user1))
        self.assertFalse(self.classificator.can_delete(user=self.user2))

    def test_classificator_active_standard_attrs(self):
        self.assertTrue(self.classificator.active_standard_attrs("STATIC"))
        self.assertFalse(
            self.classificator.active_standard_attrs("STATIC", required=False)
        )

    # def test_classificator_update_attrs_order(self):
    #     self.classificator.update_attrs_order(commit=True)

    def test_classificator_prepare_predefined_initial(self):
        self.classificator.prepare_predefined_initial()

    def test_classificator_prepare_form_fields(self):
        self.classificator.prepare_form_fields()

    # def test_classificator_set_custom_attr(self):
    #     self.classificator.set_custom_attr('name', 'test', commit=True)

    # def test_classificator_set_predefined_attrs(self):
    #     self.classificator.set_predefined_attrs('help_text', 'TEXT', commit=True)

    def test_classificator_api_update_context(self):
        Classificator.objects.api_update_context(self.classificator, self.user2)
        Classificator.objects.api_update_context(self.classificator, AnonymousUser())

    def test_classificator_api_detail_context(self):
        Classificator.objects.api_detail_context(self.classificator, self.user2)
        Classificator.objects.api_detail_context(self.classificator, AnonymousUser())

    def test_classificator_api_delete_context(self):
        Classificator.objects.api_delete_context(self.classificator, self.user2)
        Classificator.objects.api_delete_context(self.classificator, AnonymousUser())

    def test_classificator_get_accessible(self):
        Classificator.objects.get_accessible(self.user2)
        Classificator.objects.get_accessible(AnonymousUser())


class ClassificationTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(ClassificationTest, cls).setUpTestData()

        cls.user1 = UserFactory()
        cls.user2 = UserFactory()
        cls.user3 = UserFactory()
        cls.user4 = UserFactory()

        cls.research_project = ResearchProjectFactory(owner=cls.user1)
        cls.location = LocationFactory(
            owner=cls.user1, research_project=cls.research_project
        )
        cls.deployment = DeploymentFactory(
            owner=cls.user1,
            location=cls.location,
            research_project=cls.research_project,
        )
        cls.resource1 = ResourceFactory(
            owner=cls.user1, deployment=cls.deployment, status=ResourceStatus.PRIVATE
        )

        ResourceFactory(
            owner=cls.user1, deployment=cls.deployment, status=ResourceStatus.PRIVATE
        )

        cls.collection = CollectionFactory(
            owner=cls.user1, status=CollectionStatus.PRIVATE
        )
        cls.collection.resources.set(Resource.objects.all())
        cls.classificator = ClassificatorFactory(owner=cls.user1)

        cls.classification_project = ClassificationProjectFactory(
            research_project=cls.research_project,
            owner=cls.user1,
            classificator=cls.classificator,
        )

        resources_array = Resource.objects.all()
        cls.research_project.collections.set([cls.collection])

        collection = ResearchProjectCollection.objects.filter(
            project=cls.classification_project.research_project.pk
        )[0]
        cls.classification_project_collection = ClassificationProjectCollectionFactory(
            project=cls.classification_project, collection=collection
        )

        for resource in resources_array:
            ClassificationFactory(
                resource=resource,
                owner=cls.user1,
                project=cls.classification_project,
                collection=cls.classification_project_collection,
            )

        cls.resource = ResourceFactory(
            owner=cls.user1, deployment=cls.deployment, status=ResourceStatus.PRIVATE
        )
        cls.classification = ClassificationFactory(
            resource=cls.resource,
            owner=cls.user1,
            project=cls.classification_project,
            collection=cls.classification_project_collection,
        )

        cls.classification_project_role1 = ClassificationProjectRoleFactory(
            name=ClassificationProjectRoleLevels.ADMIN,
            classification_project=cls.classification_project,
            user=cls.user2,
        )
        ClassificationProjectRoleFactory(
            name=ClassificationProjectRoleLevels.EXPERT,
            classification_project=cls.classification_project,
            user=cls.user3,
        )

    def test_classification_str(self):
        print(self.classification)

    def test_classification_classificator_property(self):
        self.assertEqual(
            self.classification.classificator, self.classification.project.classificator
        )

    def test_classification_is_approved(self):
        self.classification.status = ClassificationStatus.APPROVED
        self.assertTrue(self.classification.is_approved)
        self.classification.status = ClassificationStatus.REJECTED
        self.assertFalse(self.classification.is_approved)

    def test_classification_is_rejected(self):
        self.classification.status = ClassificationStatus.REJECTED
        self.assertTrue(self.classification.is_rejected)
        self.classification.status = ClassificationStatus.APPROVED
        self.assertFalse(self.classification.is_rejected)

    def test_classification_get_user_classifications(self):
        self.classification.get_user_classifications()

    def test_classification_can_view(self):
        self.assertTrue(self.classification.can_view(user=self.user1))
        self.assertTrue(self.classification.can_view(user=self.user2))
        self.assertFalse(self.classification.can_view(user=AnonymousUser()))

    def test_classification_can_update(self):
        self.assertTrue(self.classification.can_update(user=self.user1))
        self.assertTrue(self.classification.can_update(user=self.user2))
        self.assertTrue(self.classification.can_update(user=AnonymousUser()))

    def test_classification_can_approve(self):
        self.assertTrue(self.classification.can_approve(user=self.user1))
        self.assertTrue(self.classification.can_approve(user=self.user2))
        self.assertTrue(self.classification.can_approve(user=AnonymousUser()))

    def test_classification_can_delete(self):
        self.assertTrue(self.classification.can_delete(user=self.user1))
        self.assertTrue(self.classification.can_delete(user=self.user2))
        self.assertTrue(self.classification.can_delete(user=AnonymousUser()))

    def test_classification_api_detail_context(self):
        Classification.objects.api_detail_context(self.classification, self.user2)
        Classification.objects.api_detail_context(self.classification, AnonymousUser())

    def test_classification_api_delete_context(self):
        Classification.objects.api_delete_context(self.classification, self.user2)
        Classification.objects.api_delete_context(self.classification, AnonymousUser())

    def test_classification_get_accessible(self):
        Classification.objects.get_accessible(self.user2)
        Classification.objects.get_accessible(AnonymousUser())


class UserClassificationTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(UserClassificationTest, cls).setUpTestData()

        cls.user1 = UserFactory()
        cls.user2 = UserFactory()
        cls.user3 = UserFactory()
        cls.user4 = UserFactory()

        cls.research_project = ResearchProjectFactory(owner=cls.user1)
        cls.location = LocationFactory(
            owner=cls.user1, research_project=cls.research_project
        )
        cls.deployment = DeploymentFactory(
            owner=cls.user1,
            location=cls.location,
            research_project=cls.research_project,
        )
        cls.resource1 = ResourceFactory(
            owner=cls.user1, deployment=cls.deployment, status=ResourceStatus.PRIVATE
        )

        ResourceFactory(
            owner=cls.user1, deployment=cls.deployment, status=ResourceStatus.PRIVATE
        )

        cls.collection = CollectionFactory(
            owner=cls.user1, status=CollectionStatus.PRIVATE
        )
        cls.collection.resources.set(Resource.objects.all())
        cls.classificator = ClassificatorFactory(owner=cls.user1)

        cls.classification_project = ClassificationProjectFactory(
            research_project=cls.research_project,
            owner=cls.user1,
            classificator=cls.classificator,
        )

        resources_array = Resource.objects.all()
        cls.research_project.collections.set([cls.collection])

        collection = ResearchProjectCollection.objects.filter(
            project=cls.classification_project.research_project.pk
        )[0]
        cls.classification_project_collection = ClassificationProjectCollectionFactory(
            project=cls.classification_project, collection=collection
        )

        for resource in resources_array:
            ClassificationFactory(
                resource=resource,
                owner=cls.user1,
                project=cls.classification_project,
                collection=cls.classification_project_collection,
            )

        cls.resource = ResourceFactory(
            owner=cls.user1, deployment=cls.deployment, status=ResourceStatus.PRIVATE
        )
        cls.classification = ClassificationFactory(
            resource=cls.resource,
            owner=cls.user1,
            project=cls.classification_project,
            collection=cls.classification_project_collection,
        )

        cls.classification_project_role1 = ClassificationProjectRoleFactory(
            name=ClassificationProjectRoleLevels.ADMIN,
            classification_project=cls.classification_project,
            user=cls.user2,
        )
        ClassificationProjectRoleFactory(
            name=ClassificationProjectRoleLevels.EXPERT,
            classification_project=cls.classification_project,
            user=cls.user3,
        )
        cls.user_classification = UserClassificationFactory(
            owner=cls.user2, classification=cls.classification
        )

    def test_user_classification_str(self):
        print(self.user_classification)

    def test_user_classification_classificator_property(self):
        self.assertEqual(
            self.user_classification.classificator,
            self.user_classification.classification.classificator,
        )

    def test_user_classification_get_static_values(self):
        self.user_classification.get_static_values()

    # def test_user_classification_get_custom_values(self):
    #     self.user_classification.get_custom_values()

    def test_user_classification_can_view(self):
        self.assertTrue(self.user_classification.can_view(user=self.user1))
        self.assertTrue(self.user_classification.can_view(user=self.user2))
        self.assertFalse(self.user_classification.can_view(user=AnonymousUser()))

    def test_user_classification_can_update(self):
        self.assertTrue(self.user_classification.can_update(user=self.user1))
        self.assertTrue(self.user_classification.can_update(user=self.user2))
        self.assertTrue(self.user_classification.can_update(user=AnonymousUser()))

    def test_user_classification_can_delete(self):
        self.assertTrue(self.user_classification.can_delete(user=self.user1))
        self.assertTrue(self.user_classification.can_delete(user=self.user2))
        self.assertTrue(self.user_classification.can_delete(user=AnonymousUser()))
