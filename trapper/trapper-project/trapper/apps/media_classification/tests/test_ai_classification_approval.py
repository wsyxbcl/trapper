from django.contrib.auth import get_user_model
from django.test import TestCase
from trapper.apps.media_classification.models import (
    AIClassification,
    AIClassificationDynamicAttrs,
    Classification,
    ClassificationDynamicAttrs,
)

from trapper.apps.media_classification.tasks.classification_approvals import (
    ApproveAIClassifications,
)

User = get_user_model()


class TestApproveAIClassifications(TestCase):
    fixtures = [
        "ai_classification_tests_fixtures.yaml",
        "ai_classification_approval_fixtures.yaml",
    ]

    def setUp(self) -> None:
        super().setUp()
        self.project_owner = User.objects.get(pk=1)
        self.some_user = User.objects.get(pk=2)

    def _run_ai_approval_process(
        self,
        fields_to_copy,
        mark_as_approved=False,
        minimum_confidence=0,
        overwrite_attrs=True,
    ):
        ai_classification = AIClassification.objects.get(pk=123)
        appr = ApproveAIClassifications(
            user=self.project_owner,
            project_id=4,
            ai_classification_pks=[123],
            fields_to_copy=fields_to_copy,
            minimum_confidence=minimum_confidence,
            mark_as_approved=mark_as_approved,
            overwrite_attrs=overwrite_attrs,
        )

        appr.run()

        classification: Classification = Classification.objects.get(pk=72)
        dynamic_attrs = list(
            ClassificationDynamicAttrs.objects.filter(classification=classification)
        )
        ai_dynamic_attrs = list(
            AIClassificationDynamicAttrs.objects.filter(
                ai_classification=ai_classification
            )
        )

        return classification, dynamic_attrs, ai_dynamic_attrs

    def test_ai_classification_copy_parameters(self):
        classification, dynamic_attrs, ai_dynamic_attrs = self._run_ai_approval_process(
            fields_to_copy=[
                "observation_type",
                "species",
                "count",
                "bounding_boxes",
            ],
        )

        assert len(dynamic_attrs) == 2
        for merged, ai in zip(dynamic_attrs, ai_dynamic_attrs):
            assert merged.observation_type == ai.observation_type
            assert merged.species == ai.species
            assert merged.count == ai.count
            assert merged.bboxes == ai.bboxes

        assert classification.approved_by is None
        assert classification.status_ai == False

    def test_ai_classification_approve(self):
        classification, dynamic_attrs, ai_dynamic_attrs = self._run_ai_approval_process(
            fields_to_copy=[
                "observation_type",
                "species",
                "count",
                "bounding_boxes",
            ],
            mark_as_approved=True,
        )

        assert len(dynamic_attrs) == 2
        for merged, ai in zip(dynamic_attrs, ai_dynamic_attrs):
            assert merged.observation_type == ai.observation_type
            assert merged.species == ai.species
            assert merged.count == ai.count
            assert merged.bboxes == ai.bboxes

        assert classification.approved_ai_by == self.project_owner
        assert classification.status_ai == True
        assert classification.approved_source_ai.id == 123

    def test_ai_classification_copy_parameters_skip_some(self):
        _, dynamic_attrs, ai_dynamic_attrs = self._run_ai_approval_process(
            fields_to_copy=[
                "species",
                "count",
                "bounding_boxes",
            ],
        )

        assert len(dynamic_attrs) == 2
        for merged, ai in zip(dynamic_attrs, ai_dynamic_attrs):
            assert merged.observation_type == ""  # skipped, so should not be copied
            assert merged.species == ai.species
            assert merged.count == ai.count
            assert merged.bboxes == ai.bboxes

    def test_ai_classification_copy_bounding_boxes(self):
        classification, dynamic_attrs, ai_dynamic_attrs = self._run_ai_approval_process(
            fields_to_copy=[
                "bounding_boxes",
            ],
        )

        assert classification.has_bboxes == True

        assert len(dynamic_attrs) == 2
        for merged, ai in zip(dynamic_attrs, ai_dynamic_attrs):
            assert merged.bboxes == ai.bboxes

    def test_ai_classification_skip_bounding_boxes(self):
        classification, dynamic_attrs, ai_dynamic_attrs = self._run_ai_approval_process(
            fields_to_copy=[],
        )

        assert classification.has_bboxes == False

        assert len(dynamic_attrs) == 2
        for merged, ai in zip(dynamic_attrs, ai_dynamic_attrs):
            assert merged.bboxes == []
            assert ai.bboxes != []

    def test_ai_classification_copy_minimum_confidence(self):
        classification, dynamic_attrs, ai_dynamic_attrs = self._run_ai_approval_process(
            fields_to_copy=[
                "observation_type",
                "species",
                "count",
                "bounding_boxes",
            ],
            minimum_confidence=0.90,
        )

        assert classification.has_bboxes == True

        assert len(dynamic_attrs) == 1
        assert len(ai_dynamic_attrs) == 2

    def test_ai_classification_overwrite_attrs(self):
        classification, dynamic_attrs, ai_dynamic_attrs = self._run_ai_approval_process(
            fields_to_copy=[
                "observation_type",
                "species",
                "count",
                "bounding_boxes",
            ],
            overwrite_attrs=True,
        )

        assert len(dynamic_attrs) == 2
        for merged, ai in zip(dynamic_attrs, ai_dynamic_attrs):
            assert merged.observation_type == ai.observation_type
            assert merged.species == ai.species
            assert merged.count == ai.count
            assert merged.bboxes == ai.bboxes

        assert classification.approved_ai_by is None
        assert classification.status_ai == False

    def test_ai_classification_skip_overwriting_args(self):
        classification, dynamic_attrs, ai_dynamic_attrs = self._run_ai_approval_process(
            fields_to_copy=[
                "observation_type",
                "species",
                "count",
            ],
            overwrite_attrs=False,
            mark_as_approved=True,
        )
        assert len(dynamic_attrs) == 0
        assert classification.approved_ai_by is not None
        assert classification.status_ai == True
