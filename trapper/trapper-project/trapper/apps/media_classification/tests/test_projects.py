# -*- coding: utf-8 -*-
import json

from django.urls import reverse

from trapper.apps.common.utils.test_tools import BaseMediaClassificationTestCase
from trapper.apps.media_classification.models import (
    ClassificationProject,
    ClassificationProjectRole,
    ClassificationProjectCollection,
)
from trapper.apps.media_classification.taxonomy import (
    ClassificationProjectRoleLevels,
    ClassificationProjectStatus,
    ClassificationStatus,
)


class BaseClassificationProjectTestCase(BaseMediaClassificationTestCase):
    """"""


class AnonymousProjectTestCase(BaseClassificationProjectTestCase):
    """
    Classification project logic for anonymous users.
    """

    def test_project_list(self):
        """
        Anonymous user cannot access to classification projects list.
        """
        url = reverse("media_classification:project_list")
        self.assert_auth_required(url)

    def test_project_json(self):
        """
        Anonymous user can can not see projects.
        """
        url = reverse("media_classification:api-classification-project-list")
        self.assert_forbidden(url)

    def test_details(self):
        """
        Anonymous user has to login before seeing details of project.
        """
        url = reverse("media_classification:project_detail", kwargs={"pk": 1})
        self.assert_forbidden(url)

    def test_create(self):
        """
        Anonymous user has to login before creating project.
        """
        url = reverse("media_classification:project_create")
        self.assert_auth_required(url=url)

    def test_delete(self):
        """
        Anonymous user should not get access here.
        """
        url = reverse("media_classification:project_delete", kwargs={"pk": 1})
        self.assert_forbidden(url=url, method="get")
        url = reverse("media_classification:project_delete_multiple")
        self.assert_forbidden(url=url, method="post")

    def test_update(self):
        """
        Anonymous user has no permissions to update project.
        """
        url = reverse("media_classification:project_update", kwargs={"pk": 1})
        self.assert_forbidden(url=url, method="post")

    def test_add_collection(self):
        """
        Anonymous user has no permissions to add collection to project.
        """
        url = reverse("media_classification:project_collection_add")
        self.assert_forbidden(url=url, method="post")

    def test_remove_collection(self):
        """
        Anonymous user has no permissions to remove collection from project.
        """
        url = reverse(
            "media_classification:project_collection_delete", kwargs={"pk": 1}
        )
        self.assert_forbidden(url=url, method="get")
        url = reverse("media_classification:project_collection_delete_multiple")
        self.assert_forbidden(url=url, method="post")


class ProjectPermissionsListTestCase(BaseClassificationProjectTestCase):
    """
    Classification project list permission logic for authenticated users.
    """

    def test_project_list(self):
        """
        Authenticated user can access classification projects list.
        """
        self.login_alice()
        url = reverse("media_classification:project_list")
        self.assert_access_granted(url)

    def test_project_json(self):
        """
        Authenticated user can access classification projects list via API.
        """
        self.login_alice()
        research_project = self.create_research_project(owner=self.alice)
        url = reverse("media_classification:api-classification-project-list")
        project1 = self.create_classification_project(
            owner=self.alice, research_project=research_project
        )
        project2 = self.create_classification_project(
            owner=self.ziutek, research_project=research_project
        )
        response = self.client.get(url)
        content = json.loads(response.content)["results"]
        project_pk_list = [item["pk"] for item in content]
        self.assertIn(project1.pk, project_pk_list)
        self.assertNotIn(project2.pk, project_pk_list)


class ProjectPermissionsDetailsTestCase(BaseClassificationProjectTestCase):
    """
    Details permission logic for logged in user.
    """

    def setUp(self):
        """
        Login alice by default for all tests.
        """
        super().setUp()
        self.login_alice()

    def _call_helper(self, owner, roles, status=True):
        super()._call_helper(owner, roles, status=status)

    def test_details_owner(self):
        """
        Project owner can access details.
        """
        owner = self.alice
        roles = None
        self._call_helper(owner=owner, roles=roles)
        url = self.get_classification_project_details_url(
            project=self.classification_project
        )
        self.assert_access_granted(url)

    def test_details_role_admin(self):
        """
        User with the `Admin` role can access details.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(owner=owner, roles=roles)
        url = self.get_classification_project_details_url(
            project=self.classification_project
        )
        self.assert_access_granted(url)

    def test_details_role_expert(self):
        """
        User with the `Expert` role can access details.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(owner=owner, roles=roles)
        url = self.get_classification_project_details_url(
            project=self.classification_project
        )
        self.assert_access_granted(url)

    def test_details_role_collaborator(self):
        """
        User with the `Collaborator` role can access details.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        self._call_helper(owner=owner, roles=roles)
        url = self.get_classification_project_details_url(
            project=self.classification_project
        )
        self.assert_access_granted(url)

    def test_details_no_roles(self):
        """
        User without any role can not see details.
        """
        owner = self.ziutek
        roles = None
        self._call_helper(owner=owner, roles=roles)
        url = self.get_classification_project_details_url(
            project=self.classification_project
        )
        self.assert_forbidden(url)

    def test_list_collections_allowed(self):
        """
        If user has enough permissions to view project details she can see all
        collections assigned to the project via API.
        """
        owner = self.alice
        roles = None
        self._call_helper(owner=owner, roles=roles)
        url = reverse(
            "media_classification:api-classification-project-collection-list",
            kwargs={"project_pk": self.classification_project.pk},
        )
        response = self.assert_access_granted(url)
        self.assertEqual(len(json.loads(response.content)["results"]), 1)

    def test_list_collections_forbidden(self):
        """
        If user has enough permissions to view project details she can see all
        collections assigned to the project via API.
        """
        owner = self.ziutek
        roles = None
        self._call_helper(owner=owner, roles=roles)
        url = reverse(
            "media_classification:api-classification-project-collection-list",
            kwargs={"project_pk": self.classification_project.pk},
        )
        response = self.assert_access_granted(url)
        self.assertEqual(len(json.loads(response.content)["results"]), 0)


class ProjectPermissionsUpdateTestCase(BaseClassificationProjectTestCase):
    """
    Update permission logic for logged in user.
    """

    params = None

    def setUp(self):
        """
        Login alice by default for all tests and prepare default params.
        """
        super().setUp()
        self.login_alice()

    def _call_helper(self, owner, roles, **kwargs):
        super()._call_helper(owner, roles, **kwargs)
        self.params = {
            "name": "TEST_UPDATE",
            "classificator": self.classificator.pk,
            "classification_project_roles-TOTAL_FORMS": 0,
            "classification_project_roles-INITIAL_FORMS": 0,
            "classification_project_roles-MAX_NUM_FORMS": 1000,
            "classification_project_collections-TOTAL_FORMS": 0,
            "classification_project_collections-INITIAL_FORMS": 0,
            "classification_project_collections-MAX_NUM_FORMS": 1000,
            "status": ClassificationProjectStatus.FINISHED,
            "research_project": "",
        }

    def test_update_owner(self):
        """
        Project owner can update project.
        """
        owner = self.alice
        roles = None
        self._call_helper(owner=owner, roles=roles)
        data = self.params.copy()
        data["research_project"] = self.research_project.pk
        url = self.get_classification_project_update_url(
            project=self.classification_project
        )
        redirection = self.get_classification_project_details_url(
            project=self.classification_project
        )
        self.assert_redirect(url, redirection, method="post", data=data)

    def test_update_role_admin(self):
        """
        User with `Admin` role can update project.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(owner=owner, roles=roles, rp_roles=roles)
        data = self.params.copy()
        data["research_project"] = self.research_project.pk
        url = self.get_classification_project_update_url(
            project=self.classification_project
        )
        redirection = self.get_classification_project_details_url(
            project=self.classification_project
        )
        self.assert_redirect(url, redirection, method="post", data=data)

    def test_update_role_expert(self):
        """
        User with `Expert` role can not update project.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(owner=owner, roles=roles)
        data = self.params.copy()
        data["research_project"] = self.research_project.pk
        url = self.get_classification_project_update_url(
            project=self.classification_project
        )
        self.assert_forbidden(url, method="post", data=self.params)

    def test_update_role_collaborator(self):
        """
        User with `Collaborator` role can not update project.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        self._call_helper(owner=owner, roles=roles)
        data = self.params.copy()
        data["research_project"] = self.research_project.pk
        url = self.get_classification_project_update_url(
            project=self.classification_project
        )
        self.assert_forbidden(url, method="post", data=self.params)

    def test_update_no_roles(self):
        """
        User without any role can not update project.
        """
        owner = self.ziutek
        roles = None
        self._call_helper(owner=owner, roles=roles)
        data = self.params.copy()
        data["research_project"] = self.research_project.pk
        url = self.get_classification_project_update_url(
            project=self.classification_project
        )
        self.assert_forbidden(url, method="post", data=self.params)


class ProjectPermissionsDeleteTestCase(BaseClassificationProjectTestCase):
    """
    Delete permission logic for logged in user.
    """

    def setUp(self):
        """
        Login alice by default for all tests.
        """
        super().setUp()
        self.login_alice()

    def _call_helper(self, owner, roles, **kwargs):
        super()._call_helper(owner, roles, **kwargs)

    def test_delete_owner(self):
        """
        Project owner can delete project.
        """
        owner = self.alice
        roles = None
        self._call_helper(owner=owner, roles=roles)
        url = self.get_classification_project_delete_url(
            project=self.classification_project
        )
        redirection = self.get_classification_project_list_url()
        self.assert_redirect(url, redirection)

    def test_delete_role_admin(self):
        """
        User with `Admin` role can delete project.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(owner=owner, roles=roles)
        url = self.get_classification_project_delete_url(
            project=self.classification_project
        )
        redirection = self.get_classification_project_list_url()
        self.assert_redirect(url, redirection)

    def test_delete_role_expert(self):
        """
        User with `Expert` role can not delete project.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(owner=owner, roles=roles)
        url = self.get_classification_project_delete_url(
            project=self.classification_project
        )
        self.assert_forbidden(url)

    def test_delete_role_collaborator(self):
        """
        User with `Collaborator` role can not delete project.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        self._call_helper(owner=owner, roles=roles)
        url = self.get_classification_project_delete_url(
            project=self.classification_project
        )
        self.assert_forbidden(url)

    def test_delete_no_roles(self):
        """
        User without any role can not delete project.
        """
        owner = self.ziutek
        roles = None
        self._call_helper(owner=owner, roles=roles)
        url = self.get_classification_project_delete_url(
            project=self.classification_project
        )
        self.assert_forbidden(url)


class ProjectPermissionsCollectionAddTestCase(BaseClassificationProjectTestCase):
    """
    Add classification project collection permission logic for logged in user.
    """

    def setUp(self):
        """
        Login alice by default for all tests.
        """
        super().setUp()
        self.login_alice()
        self.url = reverse("media_classification:project_collection_add")

    def _call_helper(self, owner, roles, **kwargs):
        super()._call_helper(owner, roles, **kwargs)
        self.classification_project.collections.through.objects.all().delete()
        self.params = {
            "project": self.classification_project.pk,
            "collections_pks": self.research_collection.pk,
            "rproject": self.research_project.pk,
        }

    def test_action_role_admin(self):
        """
        User with `Admin` role can add collection to project.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(owner=owner, roles=roles, rp_roles=roles)
        response = self.assert_access_granted(self.url, method="post", data=self.params)
        self.assertTrue(self.assert_json_context_variable(response, "success"))

    def test_action_role_expert(self):
        """
        User with `Expert` role can not add collection to project.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(self.url, method="post", data=self.params)
        self.assertFalse(self.assert_json_context_variable(response, "success"))

    def test_action_role_collaborator(self):
        """
        User with `Collaborator` role cannot add collection to project.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(self.url, method="post", data=self.params)
        self.assertFalse(self.assert_json_context_variable(response, "success"))

    def test_action_no_roles(self):
        """
        User without any role cannot add collection to project.
        """
        owner = self.ziutek
        roles = None
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(self.url, method="post", data=self.params)
        self.assertFalse(self.assert_json_context_variable(response, "success"))


class ProjectPermissionsCollectionDeleteTestCase(BaseClassificationProjectTestCase):
    """
    Delete single classification project collection permission logic for
    logged in user.
    """

    def setUp(self):
        """
        Login alice by default for all tests.
        """
        super().setUp()
        self.redirection = self.get_classification_project_list_url()
        self.login_alice()

    def _call_helper(self, owner, roles, **kwargs):
        super()._call_helper(owner, roles, **kwargs)

    def test_action_owner(self):
        """
        Project owner can delete collection from project.
        """
        owner = self.alice
        roles = None
        self._call_helper(owner=owner, roles=roles)
        url = self.get_classification_project_collection_delete_url(
            collection=self.classification_collection
        )
        self.assert_redirect(url, self.redirection)

    def test_action_role_admin(self):
        """
        User with `Admin` role can delete collection from project.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(owner=owner, roles=roles)
        url = self.get_classification_project_collection_delete_url(
            collection=self.classification_collection
        )
        self.assert_redirect(url, self.redirection)

    def test_action_role_expert(self):
        """
        User with `Expert` role can not delete collection from project.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(owner=owner, roles=roles)
        url = self.get_classification_project_collection_delete_url(
            collection=self.classification_collection
        )
        self.assert_forbidden(url)

    def test_action_role_collaborator(self):
        """
        User with `Collaborator` role can not delete collection from project.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        self._call_helper(owner=owner, roles=roles)
        url = self.get_classification_project_collection_delete_url(
            collection=self.classification_collection
        )
        self.assert_forbidden(url)

    def test_action_no_roles(self):
        """
        User without any role can not delete collection from project.
        """
        owner = self.ziutek
        roles = None
        self._call_helper(owner=owner, roles=roles)
        url = self.get_classification_project_collection_delete_url(
            collection=self.classification_collection
        )
        self.assert_forbidden(url)


class ClassificationPermissionsCollectionRemoveMultipleTestCase(
    BaseClassificationProjectTestCase
):
    """
    Delete multiple classification project collections permission logic for
    logged in user.
    """

    def setUp(self):
        """
        Login alice by default for all tests.
        """
        super().setUp()
        self.login_alice()
        self.url = reverse("media_classification:project_collection_delete_multiple")

    def _call_helper(self, owner, roles, **kwargs):
        super()._call_helper(owner, roles, **kwargs)
        self.params = {"pks": self.classification_collection.pk}

    def test_action_owner(self):
        """
        Project owner can delete collections from project.
        """
        owner = self.alice
        roles = None
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(self.url, method="post", data=self.params)
        self.assertTrue(self.assert_json_context_variable(response, "status"))

    def test_action_role_admin(self):
        """
        User with `Admin` role can delete collections from project.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(self.url, method="post", data=self.params)
        self.assertTrue(self.assert_json_context_variable(response, "status"))

    def test_action_role_expert(self):
        """
        User with `Expert` role can not delete collections from project.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(self.url, method="post", data=self.params)
        self.assertFalse(self.assert_json_context_variable(response, "status"))

    def test_action_role_collaborator(self):
        """
        User with `Collaborator` role can not delete collections from project.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(self.url, method="post", data=self.params)
        self.assertFalse(self.assert_json_context_variable(response, "status"))

    def test_action_no_roles(self):
        """
        User without any role can not delete collections from project.
        """
        owner = self.ziutek
        roles = None
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(self.url, method="post", data=self.params)
        self.assertFalse(self.assert_json_context_variable(response, "status"))


class ClassificationProjectTestCase(BaseClassificationProjectTestCase):
    """"""

    def setUp(self):
        """
        Login alice by default for all tests.
        """
        super(ClassificationProjectTestCase, self).setUp()
        self.login_alice()

    def _call_helper(self, owner, roles, **kwargs):
        super()._call_helper(owner, roles, **kwargs)
        self.project_params = {
            "name": "TEST_PROJECT",
            "research_project": self.research_project.pk,
            "status": ClassificationProjectStatus.ONGOING,
            "classificator": self.classificator.pk,
            "enable_sequencing": "on",
            "classification_project_roles-0-id": "",
            "classification_project_roles-0-name": ClassificationProjectRoleLevels.EXPERT,
            "classification_project_roles-0-user": self.ziutek.pk,
            "classification_project_roles-INITIAL_FORMS": 0,
            "classification_project_roles-MAX_NUM_FORMS": 1000,
            "classification_project_roles-TOTAL_FORMS": 1,
        }

    def test_dashboard_projects(self):
        """
        Logged in user in dashboard can see list of own projects. Other users
        projects are exluded.
        """
        self._call_helper(owner=self.alice, roles=None)
        self.create_classification_project(
            owner=self.ziutek, research_project=self.research_project
        )
        url = reverse("accounts:dashboard")
        response = self.assert_access_granted(url)
        projects = self.assert_context_variable(response, "classification_projects")
        self.assertEqual(projects.count(), 1)

    def test_create(self):
        """
        Using create view logged in user that has enough permissions can
        create new classification project.
        """
        self._call_helper(owner=self.alice, roles=None)
        url = reverse("media_classification:project_create")
        data = self.project_params.copy()
        data["name"] = "TEST_CREATED"
        self.assert_redirect(url, method="post", data=data)
        self.assertTrue(
            ClassificationProject.objects.filter(
                name="TEST_CREATED",
            ).exists()
        )
        self.assertTrue(
            ClassificationProjectRole.objects.filter(
                user=self.ziutek,
                classification_project__name="TEST_CREATED",
                name=ClassificationProjectRoleLevels.EXPERT,
            ).exists()
        )

    def test_update(self):
        """
        Using update view logged in user that has enough permissions can
        change the existing classification project.
        """
        roles = [(self.ziutek, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(owner=self.alice, roles=roles)
        url = self.get_classification_project_update_url(
            project=self.classification_project
        )
        redirection = self.get_classification_project_details_url(
            project=self.classification_project
        )
        data = self.project_params.copy()
        data["name"] = "TEST_UPDATED"
        data["classification_project_roles-INITIAL_FORMS"] = 1
        data[
            "classification_project_roles-0-id"
        ] = self.classification_project.classification_project_roles.all()[0].pk
        self.assert_redirect(url, redirection=redirection, method="post", data=data)
        project = ClassificationProject.objects.get(pk=self.classification_project.pk)
        self.assertEqual(project.name, "TEST_UPDATED")
        self.assertTrue(
            project.classification_project_roles.filter(
                name=ClassificationProjectRoleLevels.EXPERT, user=self.ziutek
            )
        )

    def test_delete(self):
        """
        Using delete view logged in user that has enough permissions can
        delete existing project using GET. Only projects without any
        approved classification can be physically deleted.
        """
        roles = [(self.ziutek, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(
            owner=self.alice, roles=roles, status=ClassificationStatus.REJECTED
        )
        url = self.get_classification_project_delete_url(
            project=self.classification_project
        )
        redirection = self.get_classification_project_list_url()
        self.assert_redirect(url, redirection=redirection)
        self.assertFalse(
            ClassificationProject.objects.filter(
                pk=self.classification_project.pk
            ).exists()
        )
        self.assertFalse(
            ClassificationProjectRole.objects.filter(
                user=self.ziutek,
                classification_project=self.classification_project,
                name=ClassificationProjectRoleLevels.EXPERT,
            ).exists()
        )
        self.assertFalse(
            ClassificationProjectCollection.objects.filter(
                pk=self.classification_collection.pk
            ).exists()
        )

    def test_disable(self):
        """
        Using delete view logged in user that has enough permissions can
        disable the existing project using GET. This prevents loss of data
        when at least one approved classification exists.
        """
        roles = [(self.ziutek, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(
            owner=self.alice, roles=roles, status=ClassificationStatus.APPROVED
        )
        url = self.get_classification_project_delete_url(
            project=self.classification_project
        )
        redirection = self.get_classification_project_list_url()
        self.assert_redirect(url, redirection=redirection)
        self.assertTrue(
            ClassificationProject.objects.filter(
                pk=self.classification_project.pk, disabled_by=self.alice
            ).exists()
        )

    def test_delete_multiple(self):
        """
        Using delete view logged in user that has enough permissions can
        delete multiple projects using POST.
        """
        roles = [(self.ziutek, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(
            owner=self.alice, roles=roles, status=ClassificationStatus.REJECTED
        )
        url = reverse("media_classification:project_delete_multiple")
        data = {
            "pks": ",".join(
                [
                    str(self.classification_project.pk),
                ]
            )
        }
        response = self.assert_access_granted(url, method="post", data=data)
        status = self.assert_json_context_variable(response, "status")
        self.assertTrue(status)
        self.assertFalse(
            ClassificationProject.objects.filter(
                pk=self.classification_project.pk
            ).exists()
        )
        self.assertFalse(
            ClassificationProjectRole.objects.filter(
                user=self.ziutek,
                classification_project=self.classification_project,
                name=ClassificationProjectRoleLevels.EXPERT,
            ).exists()
        )
        self.assertFalse(
            ClassificationProjectCollection.objects.filter(
                pk=self.classification_collection.pk
            ).exists()
        )

    def test_disable_multiple(self):
        """
        Using delete view logged in user that has enough permissions can
        disable multiple projects using POST. This prevents loss
        of data when at least one approved classification exists.
        """
        roles = [(self.ziutek, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(
            owner=self.alice, roles=roles, status=ClassificationStatus.APPROVED
        )
        url = reverse("media_classification:project_delete_multiple")
        data = {
            "pks": ",".join(
                [
                    str(self.classification_project.pk),
                ]
            )
        }
        response = self.assert_access_granted(url, method="post", data=data)
        status = self.assert_json_context_variable(response, "status")
        self.assertTrue(status)
        self.assertTrue(
            ClassificationProject.objects.filter(
                pk=self.classification_project.pk, disabled_by=self.alice
            ).exists()
        )
