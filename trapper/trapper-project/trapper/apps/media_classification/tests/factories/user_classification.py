import datetime

import factory

from django.utils import timezone

from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.media_classification.models import UserClassification
from trapper.apps.media_classification.tests.factories.classification import (
    ClassificationFactory,
)


class UserClassificationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = UserClassification
        exclude = ("now",)

    owner = factory.SubFactory(UserFactory)
    classification = factory.SubFactory(ClassificationFactory)
    now = factory.LazyFunction(timezone.now)
    created_at = factory.LazyAttribute(lambda o: o.now - datetime.timedelta(minutes=10))
    updated_at = factory.LazyAttribute(lambda o: o.now - datetime.timedelta(minutes=10))
