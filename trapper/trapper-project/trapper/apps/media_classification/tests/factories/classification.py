import datetime
import factory

from django.utils import timezone

from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.media_classification.models import Classification
from trapper.apps.media_classification.tests.factories.classification_project_collection import (
    ClassificationProjectCollectionFactory,
)
from trapper.apps.storage.tests.factories.classification_project import (
    ClassificationProjectFactory,
)
from trapper.apps.storage.tests.factories.resource import ResourceFactory


class ClassificationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Classification
        exclude = ("now",)

    owner = factory.SubFactory(UserFactory)
    resource = factory.SubFactory(ResourceFactory, owner=owner)
    project = factory.SubFactory(ClassificationProjectFactory, owner=owner)
    collection = factory.SubFactory(ClassificationProjectCollectionFactory)

    now = factory.LazyFunction(timezone.now)
    created_at = factory.LazyAttribute(lambda o: o.now - datetime.timedelta(minutes=10))
    updated_at = factory.LazyAttribute(lambda o: o.now - datetime.timedelta(minutes=10))
    updated_by = owner

    sequence = None
    status = False
    status_ai = False

    approved_by = None
    approved_ai_by = None
    approved_at = None
    approved_ai_at = None
    approved_source = None
    approved_source_ai = None
