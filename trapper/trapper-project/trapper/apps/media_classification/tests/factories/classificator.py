import datetime
import factory

from django.utils import timezone

from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.media_classification.models import Classificator


class ClassificatorFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Classificator
        exclude = ("now",)

    name = factory.Faker("word")
    owner = factory.SubFactory(UserFactory)

    now = factory.LazyFunction(timezone.now)
    created_date = factory.LazyAttribute(
        lambda o: o.now - datetime.timedelta(minutes=10)
    )
    updated_date = factory.LazyAttribute(
        lambda o: o.now - datetime.timedelta(minutes=10)
    )

    # standard attributes
    # when species field is null then all species are valid choices
    # in the classification form
    # species = None
    observation_type = True
    is_setup = True
    sex = False
    age = False
    count = True
    count_new = False
    behaviour = False
    individual_id = False
    classification_confidence = False
    # custom and predefined attributes
    custom_attrs = {}
    predefined_attrs = {}
    # other properties
    dynamic_attrs_order = None
    static_attrs_order = None
    description = factory.Faker("paragraph", nb_sentences=3)
