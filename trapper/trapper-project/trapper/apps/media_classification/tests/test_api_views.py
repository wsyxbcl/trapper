import json

from django.core.cache import cache
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.geomap.tests.factories.deployment import DeploymentFactory
from trapper.apps.media_classification.taxonomy import ClassificationProjectRoleLevels
from trapper.apps.media_classification.tests.factories.classification import (
    ClassificationFactory,
)
from trapper.apps.media_classification.tests.factories.classification_project_role import (
    ClassificationProjectRoleFactory,
)
from trapper.apps.media_classification.tests.factories.classificator import (
    ClassificatorFactory,
)
from trapper.apps.media_classification.tests.factories.sequence import SequenceFactory
from trapper.apps.media_classification.tests.factories.user_classification import (
    UserClassificationFactory,
)
from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)
from trapper.apps.storage.tests.factories.classification_project import (
    ClassificationProjectFactory,
)
from trapper.apps.storage.tests.factories.resource import ResourceFactory


class CashelessAPITestCase(APITestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        cache.clear()


class UserClassificationViewSetTestCase(CashelessAPITestCase):
    def setUp(self):
        self.user = UserFactory(is_superuser=True)
        self.url = reverse("media_classification:api-user-classification-list")

    def test_list(self):
        uc = UserClassificationFactory()
        ClassificationProjectRoleFactory(
            user=self.user,
            classification_project=uc.classification.project,
            name=ClassificationProjectRoleLevels.ADMIN,
        )
        self.client.force_authenticate(self.user)

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)


class ClassificationViewSetTestCase(CashelessAPITestCase):
    def setUp(self):
        self.user = UserFactory(is_superuser=True)
        self.url = reverse("media_classification:api-classification-list")

    def test_list(self):
        classification = ClassificationFactory()
        ClassificationProjectRoleFactory(
            user=self.user,
            classification_project=classification.project,
            name=ClassificationProjectRoleLevels.ADMIN,
        )
        self.client.force_authenticate(self.user)

        response = self.client.get(
            self.url, data={"project": classification.project.id}
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)


class ClassificationResultsViewTestCase(CashelessAPITestCase):
    def setUp(self):
        self.user = UserFactory(is_superuser=True)
        self.client.force_authenticate(self.user)

        self.classification = ClassificationFactory(
            project__classificator=ClassificatorFactory()
        )
        ClassificationProjectRoleFactory(
            user=self.user,
            classification_project=self.classification.project,
            name=ClassificationProjectRoleLevels.ADMIN,
        )

    def test_list(self):
        url = reverse(
            "media_classification:api-classification-results",
            kwargs={"project_pk": self.classification.project.id},
        )
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response["Content-Type"], "text/csv")
        self.assertGreater(int(response["Content-Length"]), 0)

    def test_project_not_exists(self):
        classification = ClassificationFactory(
            project__classificator=ClassificatorFactory()
        )
        ClassificationProjectRoleFactory(
            user=self.user,
            classification_project=classification.project,
            name=ClassificationProjectRoleLevels.ADMIN,
        )
        self.client.force_authenticate(self.user)

        url = reverse(
            "media_classification:api-classification-results",
            kwargs={"project_pk": 1000},
        )
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class ClassificationResultsAggViewTestCase(CashelessAPITestCase):
    def setUp(self):
        self.user = UserFactory(is_superuser=True)
        self.client.force_authenticate(self.user)
        self.research_project = ResearchProjectFactory()
        self.deployment = DeploymentFactory(research_project=self.research_project)
        self.classification = ClassificationFactory(
            project__research_project=self.research_project,
            project__classificator=ClassificatorFactory(),
            resource__deployment=self.deployment,
        )
        ClassificationProjectRoleFactory(
            user=self.user,
            classification_project=self.classification.project,
            name=ClassificationProjectRoleLevels.ADMIN,
        )

        self.url = reverse(
            "media_classification:api-classification-results-agg",
            kwargs={"project_pk": self.classification.project.id},
        )

    def test_list_geojson(self):
        response = self.client.get(self.url, data={"geojson": True, "all_dep": True})

        expected_result = {
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "Feature",
                    "properties": {
                        "deploymentID": str(self.deployment),
                        "locationID": self.deployment.location.location_id,
                        "deploymentStart": self.deployment.start_date_tz.strftime(
                            "%Y-%m-%dT%H:%M:%S%z"
                        ),
                        "deploymentEnd": self.deployment.end_date_tz.strftime(
                            "%Y-%m-%dT%H:%M:%S%z"
                        ),
                        "days": (
                            self.deployment.end_date_tz - self.deployment.start_date_tz
                        ).days,
                        "observationType": "",
                        "scientificName": "",
                        "count": 0.0,
                        "trapRate": 0.0,
                    },
                    "geometry": {
                        "type": "Point",
                        "coordinates": [
                            round(self.deployment.location.coordinates.x, 5),
                            round(self.deployment.location.coordinates.y, 5),
                        ],
                    },
                }
            ],
        }

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), expected_result)

    def test_list_csv(self):
        response = self.client.get(self.url, data={"all_dep": True})

        expected_result = (
            "deploymentID,locationID,deploymentStart,deploymentEnd,longitude,latitude,days,observationType,scientificName,count,trapRate\n"
            f"{self.deployment},"
            f"{self.deployment.location.location_id},"
            f'{self.deployment.start_date_tz.strftime("%Y-%m-%dT%H:%M:%S%z")},'
            f'{self.deployment.end_date_tz.strftime("%Y-%m-%dT%H:%M:%S%z")},'
            f"{round(self.deployment.location.coordinates.x, 5)},"
            f"{round(self.deployment.location.coordinates.y, 5)},"
            f"{(self.deployment.end_date_tz - self.deployment.start_date_tz).days},"
            ",,0.0,0.0\n"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.content, expected_result.encode())


class ClassificatorViewSetTestCase(CashelessAPITestCase):
    def setUp(self):
        self.user = UserFactory(is_superuser=True)
        self.client.force_authenticate(self.user)
        self.classificator = ClassificatorFactory()
        self.url = reverse("media_classification:api-classificator-list")

    def test_list(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)


class ClassificationProjectViewSetTestCase(CashelessAPITestCase):
    def setUp(self):
        self.url = reverse("media_classification:api-classification-project-list")
        self.user = UserFactory(is_superuser=True)
        self.client.force_authenticate(self.user)

    def test_list(self):
        ClassificationProjectFactory(owner=self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)


class SequenceViewSetTestCase(CashelessAPITestCase):
    def setUp(self):
        self.url = reverse("media_classification:api-sequence-list")
        self.user = UserFactory(is_superuser=True)
        self.client.force_authenticate(self.user)

    def test_list(self):
        SequenceFactory()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)


class ClassificationResourcesViewSetTestCase(CashelessAPITestCase):
    def setUp(self):
        self.classification = ClassificationFactory()
        self.resource = ResourceFactory()
        self.url = reverse(
            "media_classification:api-classification-resources-list",
            kwargs={"collection_pk": self.classification.collection.id},
        )
        self.user = UserFactory(is_superuser=True)
        self.client.force_authenticate(self.user)

    def test_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)
