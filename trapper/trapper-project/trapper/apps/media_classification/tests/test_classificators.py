# -*- coding: utf-8 -*-
import json

from django.urls import reverse
from django import forms

from trapper.apps.common.utils.test_tools import BaseMediaClassificationTestCase
from trapper.apps.media_classification.models import Classificator


class BaseClassificatorTestCase(BaseMediaClassificationTestCase):
    """"""


class AnonymousClassificatorTestCase(BaseClassificatorTestCase):
    """
    Classificator logic for anonymous users.
    """

    def test_project_list(self):
        """
        Anonymous user has to login before seeing classificator list.
        """
        url = self.get_classificator_list_url()
        self.assert_auth_required(url)

    def test_classificator_json(self):
        """
        Anonymous user cannot see API data.
        """
        url = self.get_classificator_list_api_url()
        self.assert_forbidden(url)

    def test_details(self):
        """
        Anonymous user has to login before seeing details of classificator.
        """
        url = reverse("media_classification:classificator_detail", kwargs={"pk": 1})
        self.assert_auth_required(url=url)

    def test_create(self):
        """
        Anonymous user has to login before creating classificator.
        """
        url = reverse("media_classification:classificator_create")
        self.assert_auth_required(url=url)

    def test_delete(self):
        """
        Anonymous user should not get access here.
        """
        url = reverse("media_classification:classificator_delete", kwargs={"pk": 1})
        self.assert_forbidden(url=url, method="get")
        url = reverse("media_classification:classificator_delete_multiple")
        self.assert_forbidden(url=url, method="post")

    def test_update(self):
        """
        Anonymous user has no permissions to update project.
        """
        url = reverse("media_classification:classificator_update", kwargs={"pk": 1})
        self.assert_forbidden(url=url, method="post")


class ClassificatorPermissionsTestCase(BaseClassificatorTestCase):
    """
    Classificator permission logic for authenticated users.
    """

    def setUp(self):
        """
        Login alice for all tests.
        """
        super().setUp()
        self.login_alice()

    def test_list(self):
        """
        Authenticated user can access classificators list.
        """
        url = self.get_classificator_list_url()
        self.assert_access_granted(url)

    def test_json(self):
        """
        Authenticated user can access classificators list via API.
        """
        url = self.get_classificator_list_api_url()
        classificator1 = self.create_classificator(owner=self.alice)
        classificator2 = self.create_classificator(owner=self.ziutek)
        response = self.client.get(url)
        content = json.loads(response.content)["results"]
        classificators_list = [item["pk"] for item in content]
        self.assertIn(classificator1.pk, classificators_list)
        self.assertIn(classificator2.pk, classificators_list)

    def test_details_own(self):
        """
        Authenticated user can view details of own classificator.
        """
        classificator = self.create_classificator(owner=self.alice)
        url = self.get_classificator_detail_url(classificator=classificator)
        self.assert_access_granted(url)

    def test_details_other(self):
        """
        Authenticated user can view details of other user classificator.
        """
        classificator = self.create_classificator(owner=self.ziutek)
        url = self.get_classificator_detail_url(classificator=classificator)
        self.assert_access_granted(url)

    def test_update_own(self):
        """
        Authenticated user can update own classificator.
        """
        classificator = self.create_classificator(owner=self.alice)
        url = self.get_classificator_update_url(classificator=classificator)
        params = {"name": "Updated classificator"}
        self.assert_access_granted(url, method="post", data=params)

    def test_update_other(self):
        """
        Authenticated user can not update other user classificator.
        """
        classificator = self.create_classificator(owner=self.ziutek)
        url = self.get_classificator_update_url(classificator=classificator)
        params = {"name": "Updated classificator"}
        self.assert_forbidden(url, method="post", data=params)

    def test_delete_own(self):
        """
        Authenticated user can delete own classificator.
        """
        classificator = self.create_classificator(owner=self.alice)
        url = self.get_classificator_delete_url(classificator=classificator)
        params = {"pks": classificator.pk}
        response = self.assert_access_granted(url, method="post", data=params)
        status = self.assert_json_context_variable(response, "status")
        self.assertTrue(status)

    def test_delete_other(self):
        """
        Authenticated user can not delete other user classificators.
        """
        classificator = self.create_classificator(owner=self.ziutek)
        url = self.get_classificator_delete_url(classificator=classificator)
        params = {"pks": classificator.pk}
        response = self.assert_access_granted(url, method="post", data=params)
        status = self.assert_json_context_variable(response, "status")
        self.assertFalse(status)

    def test_clone_own(self):
        """
        Authenticated user can clone own classificator.
        """
        classificator = self.create_classificator(owner=self.alice)
        url = self.get_classificator_clone_url(classificator=classificator)
        response = self.client.post(url)
        self.assertEqual(response.status_code, 302)

    def test_clone_other(self):
        """
        Authenticated user can clone other user classificators.
        """
        classificator = self.create_classificator(owner=self.ziutek)
        url = self.get_classificator_clone_url(classificator=classificator)
        response = self.client.post(url)
        self.assertEqual(response.status_code, 302)


class ClassificatorTestCase(BaseClassificatorTestCase):
    """
    Classificator logic for authenticated users.
    """

    classificator_standard_attrs = {
        "classificator-name": "TEST",
        "classificator-species": "",
        "classificator-sex": "on",
        "classificator-age": "on",
        "classificator-static_attrs_order": "is_setup",
        "classificator-dynamic_attrs_order": "observation_type,species,count,sex,age",
    }
    classificator_predefined_attr = {
        "comments": "on",
        "comments_target": "S",
        "comments_required": True,
    }
    classificator_custom_attr = {
        "custom_attrs_manage": True,
        "name": "test_custom",
        "field_type": "S",
        "target": "D",
        "values": "val1,val2,val3",
        "initial": "val2",
        "description": "test",
    }

    def setUp(self):
        """Login alice for all tests"""
        super(ClassificatorTestCase, self).setUp()
        self.login_alice()
        # create Species object (default is "Wolf")
        self.species = self.create_species()
        self.classificator_standard_attrs["classificator-species"] = self.species.pk

    def _call_helper(self, owner, roles, status=True):
        super()._call_helper(owner, roles, status=status)

    def test_create_standard(self):
        """
        Using create classificator view an authenticated user can create
        new classificators with standard attributes only.
        """
        url = reverse("media_classification:classificator_create")
        data = self.classificator_standard_attrs.copy()
        data["classificator-name"] = "TEST_STANDARD"
        data["classificator-template"] = "inline"
        self.assert_redirect(url, method="post", data=data)
        self.assertTrue(
            Classificator.objects.filter(
                name="TEST_STANDARD", owner=self.alice
            ).exists()
        )

    def test_create_predefined(self):
        """
        Using the create classificator view an authenticated user can create
        new classificators with standard and predefined attributes.
        """
        url = reverse("media_classification:classificator_create")
        data = self.classificator_standard_attrs.copy()
        data["classificator-name"] = "TEST_PREDEFINED"
        data["classificator-template"] = "inline"
        data.update(self.classificator_predefined_attr)
        self.assert_redirect(url, method="post", data=data)
        self.assertTrue(
            Classificator.objects.filter(
                name="TEST_PREDEFINED",
                owner=self.alice,
                predefined_attrs__comments__target="S",
            ).exists()
        )

    def test_create_custom(self):
        """
        Using the create classificator view an authenticated user can create
        new classificators with standard and custom attributes.
        """
        url = reverse("media_classification:classificator_create")
        data = self.classificator_standard_attrs.copy()
        data["classificator-name"] = "TEST_CUSTOM"
        data["classificator-template"] = "inline"
        data.update(self.classificator_custom_attr)
        self.assert_redirect(url, method="post", data=data)
        self.assertTrue(
            Classificator.objects.filter(
                name="TEST_CUSTOM",
                owner=self.alice,
                custom_attrs__test_custom__target="D",
            ).exists()
        )

    def test_create_all(self):
        """
        Using the create classificator view an authenticated user can create
        new classificators with standard, predefined and custom attributes.
        """
        url = reverse("media_classification:classificator_create")
        data = self.classificator_standard_attrs.copy()
        data["classificator-name"] = "TEST_BOTH"
        data["classificator-template"] = "inline"
        data.update(self.classificator_predefined_attr)
        data.update(self.classificator_custom_attr)
        self.assert_redirect(url, method="post", data=data)
        self.assertTrue(
            Classificator.objects.filter(
                name="TEST_BOTH",
                owner=self.alice,
                predefined_attrs__comments__target="S",
                custom_attrs__test_custom__target="D",
            ).exists()
        )

    def test_update_standard(self):
        """
        Using the update classificator view an authenticated user that has
        enough permissions can update the existing classificator (standard
        attributes case).
        """
        red_deer = self.create_species(english="Red deer", latin="Cervus elaphus")
        params = {"name": "TEST_UPDATE_STANDARD", "sex": False}
        classificator = self.create_classificator(owner=self.alice, **params)
        classificator.species.add(red_deer)
        url = self.get_classificator_update_url(classificator=classificator)
        data = self.classificator_standard_attrs.copy()
        data["classificator-name"] = "TEST_UPDATE_STANDARD_OK"
        data["classificator-template"] = "inline"
        self.assert_redirect(url, method="post", data=data)
        self.assertTrue(
            Classificator.objects.filter(
                name="TEST_UPDATE_STANDARD_OK",
                owner=self.alice,
                species=self.species,
                sex=True,  # self.species is "Wolf"
            ).exists()
        )

    def test_update_predefined(self):
        """
        Using the update classificator view an authenticated user that has
        enough permissions can update the existing classificator (predefined
        attributes case).
        """
        params = {
            "name": "TEST_UPDATE_PREDEFINED",
            "predefined_attrs": {"comments": {"target": "D", "required": False}},
        }
        classificator = self.create_classificator(owner=self.alice, **params)
        url = self.get_classificator_update_url(classificator=classificator)
        data = self.classificator_standard_attrs.copy()
        data["classificator-name"] = "TEST_UPDATE_PREDEFINED_OK"
        data["classificator-template"] = "inline"
        data.update(self.classificator_predefined_attr)
        self.assert_redirect(url, method="post", data=data)
        self.assertTrue(
            Classificator.objects.filter(
                name="TEST_UPDATE_PREDEFINED_OK",
                owner=self.alice,
                predefined_attrs__comments__target="S",
                predefined_attrs__comments__required=True,
            ).exists()
        )

    def test_update_custom(self):
        """
        Using the update classificator view an authenticated user that has
        enough permissions can update the existing classificator (custom
        attributes case).
        """
        params = {
            "name": "TEST_UPDATE_CUSTOM",
            "custom_attrs": {
                "test_custom": {
                    "field_type": "I",
                    "target": "S",
                    "values": "",
                    "initial": 0,
                    "description": "number",
                }
            },
        }
        classificator = self.create_classificator(owner=self.alice, **params)
        url = self.get_classificator_update_url(classificator=classificator)
        data = self.classificator_standard_attrs.copy()
        data["classificator-name"] = "TEST_UPDATE_CUSTOM_OK"
        data["classificator-template"] = "inline"
        data.update(self.classificator_custom_attr)
        self.assert_redirect(url, method="post", data=data)
        self.assertTrue(
            Classificator.objects.filter(
                name="TEST_UPDATE_CUSTOM_OK",
                owner=self.alice,
                custom_attrs__test_custom__field_type="S",
                custom_attrs__test_custom__target="D",
                custom_attrs__test_custom__initial="val2",
            ).exists()
        )

    def test_delete_multiple(self):
        """Using create classificator view authenticated user that has enough
        permissions can delete multiple existing classificators at single
        request"""
        classificator1 = self.create_classificator(owner=self.alice)
        classificator2 = self.create_classificator(owner=self.alice)
        url = reverse("media_classification:classificator_delete_multiple")
        params = {"pks": ",".join([str(classificator1.pk), str(classificator2.pk)])}
        response = self.assert_access_granted(url, method="post", data=params)
        status = self.assert_json_context_variable(response, "status")
        self.assertTrue(status)
        self.assertFalse(
            Classificator.objects.filter(
                pk__in=[classificator1.pk, classificator2.pk]
            ).exists()
        )

    def test_clone(self):
        """
        Using the create classificator view an authenticated user can clone
        the existing classificator.
        """
        predefined_attrs = {"comments": {"target": "S", "required": "false"}}
        static_attrs_order = "comments"
        classificator = self.create_classificator(
            owner=self.ziutek,
            predefined_attrs=predefined_attrs,
            static_attrs_order=static_attrs_order,
        )
        url = reverse(
            "media_classification:classificator_clone", kwargs={"pk": classificator.pk}
        )
        response = self.client.post(url)
        self.assertEqual(response.status_code, 302)
        new_name = "Copy_of_{name}".format(name=classificator.name)
        qs = Classificator.objects.filter(name__contains=new_name)
        self.assertTrue(qs.exists())
        cloned_classificator = qs.first()
        self.assertEqual(cloned_classificator.owner, self.alice)

    def test_prepare_form_fields(self):
        """
        Test if all classificator's attribute definitions can be properly
        translated into the coressponding form fields.
        """
        params = {
            "name": "TEST_FORM_FIELDS",
            "sex": True,
            "age": True,
            "behaviour": True,
            "count_new": True,
            "individual_id": True,
            "classification_confidence": True,
            "predefined_attrs": {"comments": {"target": "S", "required": False}},
            "custom_attrs": {
                "test_integer": {
                    "field_type": "I",
                    "target": "S",
                    "description": "integer",
                    "values": "",
                    "initial": "",
                    "required": False,
                },
                "test_float": {
                    "field_type": "F",
                    "target": "S",
                    "description": "float",
                    "values": "",
                    "initial": "",
                    "required": False,
                },
                "test_boolean": {
                    "field_type": "B",
                    "target": "S",
                    "description": "boolean",
                    "values": "",
                    "initial": False,
                    "required": False,
                },
                "test_string_simple": {
                    "field_type": "S",
                    "target": "S",
                    "description": "boolean",
                    "values": "",
                    "initial": "",
                    "required": False,
                },
                "test_string_list": {
                    "field_type": "B",
                    "target": "S",
                    "description": "boolean",
                    "values": "val1,val2,val3",
                    "initial": "val2",
                    "required": False,
                },
            },
            "dynamic_attrs_order": ",".join(
                [
                    "observation_type",
                    "species",
                    "sex",
                    "age",
                    "behaviour",
                    "count",
                    "count_new",
                    "individual_id",
                    "classification_confidence",
                ]
            ),
            "static_attrs_order": ",".join(
                [
                    "is_setup",
                    "test_integer",
                    "test_float",
                    "test_boolean",
                    "test_string_simple",
                    "test_string_list",
                    "comments",
                ]
            ),
        }
        classificator = self.create_classificator(owner=self.alice, **params)
        classificator.species.add(self.species)
        fields = classificator.prepare_form_fields()
        AT = self.assertTrue
        AT(isinstance(fields["D"]["observation_type"], forms.TypedChoiceField))
        AT(isinstance(fields["D"]["species"], forms.ChoiceField))
        AT(isinstance(fields["D"]["count"], forms.IntegerField))
        AT(isinstance(fields["D"]["sex"], forms.TypedChoiceField))
        AT(isinstance(fields["D"]["age"], forms.TypedChoiceField))
        AT(isinstance(fields["D"]["behaviour"], forms.TypedChoiceField))
        AT(isinstance(fields["D"]["count_new"], forms.IntegerField))
        AT(isinstance(fields["D"]["individual_id"], forms.CharField))
        AT(isinstance(fields["D"]["classification_confidence"], forms.IntegerField))
        AT(isinstance(fields["S"]["is_setup"], forms.BooleanField))
        AT(isinstance(fields["S"]["comments"], forms.CharField))
        AT(isinstance(fields["S"]["test_integer"], forms.IntegerField))
        AT(isinstance(fields["S"]["test_float"], forms.FloatField))
        AT(isinstance(fields["S"]["test_boolean"], forms.BooleanField))
        AT(isinstance(fields["S"]["test_string_simple"], forms.CharField))
        AT(isinstance(fields["S"]["test_string_list"], forms.ChoiceField))
        self.assertEqual(
            list(fields["S"].keys()), params["static_attrs_order"].split(",")
        )
        self.assertEqual(
            list(fields["D"].keys()),
            params["dynamic_attrs_order"].split(",") + ["bboxes"],
        )
