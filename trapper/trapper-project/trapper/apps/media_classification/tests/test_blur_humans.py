import mock.mock
from django.contrib.auth import get_user_model
from django.test import TestCase, override_settings
from django.test.client import Client
from django.urls import reverse
import json
from mock import Mock, patch

from trapper.apps.media_classification.models import Classification
from trapper.apps.storage.models import Resource

User = get_user_model()


class TestBlurHumans(TestCase):
    fixtures = [
        "blur_human_fixtures.yaml",
    ]

    def _client(self, user_id: int):
        client = Client()
        user = User.objects.get(pk=user_id)
        client.force_login(user)
        return client

    def _url(self):
        return reverse("media_classification:project_collection_blur_humans")

    def test_blur_humans(self):
        client = self._client(user_id=1)

        with override_settings(CELERY_ENABLED=False):
            with mock.patch(
                "trapper.apps.media_classification.tasks.blur_humans.ProjectCollectionBlurHumans.blur_image",
                return_value=True,
            ) as blur_image_mock:
                response = client.post(self._url(), {"pks": "1"})

                # it should be run once for each image, resource 3 has file, preview and thumbnail,
                # and resource4 is missing a preview file
                self.assertEqual(blur_image_mock.call_count, 5)

        # We should get successful response
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content).get("success"), True)

        # check if resources were changed accordingly
        # resource1 was classified as animal
        resource1 = Resource.objects.get(pk=1)
        self.assertFalse(resource1.humans_blurred)

        # resource2 was classified as human, but user does is not set as resource's manager
        resource2 = Resource.objects.get(pk=2)
        self.assertFalse(resource2.humans_blurred)

        # resource3 was classified as human, user has necessary permissions,
        # was accepted as UserClassification
        resource3 = Resource.objects.get(pk=3)
        self.assertTrue(resource3.humans_blurred)

        # resource4 was classified as human, user has necessary permissions,
        # was accepted as AIClassification
        resource4 = Resource.objects.get(pk=4)
        self.assertTrue(resource4.humans_blurred)

        # resource5 was classified as human, user has necessary permissions,
        # was accepted as AIClassification
        resource5 = Resource.objects.get(pk=5)
        self.assertFalse(resource5.humans_blurred)
