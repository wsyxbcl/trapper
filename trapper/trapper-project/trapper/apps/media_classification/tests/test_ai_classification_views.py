from django.core.cache import cache
from django.test import TestCase
from django.contrib.auth import get_user_model
from django.test.client import Client
from django.urls import reverse

from mock import patch, Mock
from trapper.apps.media_classification.ai_providers.ai_providers_base import (
    RemoteClassificationParams,
)


from trapper.apps.media_classification.models import (
    AIClassification,
    AIProvider,
    AIProviderPermission,
)

User = get_user_model()


class TestAIClassificationScheduling(TestCase):
    fixtures = [
        "ai_classification_tests_fixtures.yaml",
    ]

    def setUp(self) -> None:
        super().setUp()
        cache.clear()

    def _client(self, user_id: int):
        client = Client()
        user = User.objects.get(pk=user_id)
        client.force_login(user)

        return client

    def _url(self, project_id: int):
        return reverse(
            "media_classification:classify_ai",
            kwargs={"pk": project_id, "obj": "classification"},
        )

    @patch(
        "trapper.apps.media_classification.views.classifications.get_ai_provider_manager"
    )
    def test_schedule_ai_classification(self, get_ai_provider_manager_mock: Mock):
        ai_provider_mock = Mock()
        get_ai_provider_manager_mock.return_value = ai_provider_mock

        client = self._client(user_id=1)  # a client with logged in user
        project_id = 4
        classification_pks = [72]
        provider_id = 2
        permission = AIProviderPermission(user_id=1, ai_provider_id=provider_id)
        permission.save()

        # When we make an AI classification request
        r = client.post(
            self._url(project_id),
            {
                "classifications_pks": ",".join(map(str, classification_pks)),
                "ai_model": provider_id,
            },
        )

        # We should get successful response
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.json().get("success"), True)

        print(r.json())

        # AI provider manager should be initialized with correct AI provider config
        get_ai_provider_manager_mock.assert_called_once_with(
            AIProvider.objects.get(pk=2)
        )

        # And manager's request_classification method should be called with coresponding parameters
        ai_provider_mock.request_classification.assert_called_once_with(
            params=RemoteClassificationParams(
                classification_ids=classification_pks,
                classify_preview_files=False,
                project_id=project_id,
            ),
            user_id=1,
        )


class TestAIClassificationBulkApproval(TestCase):
    fixtures = [
        "ai_classification_tests_fixtures.yaml",
        "ai_classification_approval_fixtures.yaml",
    ]

    def setUp(self) -> None:
        super().setUp()
        cache.clear()

    def _client(self, user_id: int):
        client = Client()
        user = User.objects.get(pk=user_id)
        client.force_login(user)

        return client

    def _url(self, project_id: int):
        return reverse(
            "media_classification:ai_classification_bulk_approve",
            kwargs={"project_id": project_id},
        )

    @patch(
        "trapper.apps.media_classification.views.ai_classifications.celery_approve_ai_classifications"
    )
    def test_bulk_approve_ai_classification(
        self, celery_approve_ai_classifications_mock: Mock
    ):
        celery_approve_ai_classifications_mock.delay.return_value.task_id = (
            "test_task_id"
        )
        client = self._client(user_id=1)  # a client with logged in user
        project_id = 4
        ai_classification_pks = [123]

        # When we make an AI bulk approval request
        r = client.post(
            self._url(project_id),
            {
                "ai_classification_pks": ",".join(map(str, ai_classification_pks)),
                "minimum_confidence": 0.0,
                "observation_type": "on",
                "species": "on",
                "count": "on",
                "bounding_boxes": "on",
                "overwrite_attrs": "on",
            },
        )

        assert r.json()["success"] == True

        celery_approve_ai_classifications_mock.delay.assert_called_once_with(
            user=User.objects.get(pk=1),
            project_id=project_id,
            ai_classification_pks=ai_classification_pks,
            fields_to_copy=[
                "observation_type",
                "species",
                "count",
                "bounding_boxes",
            ],
            mark_as_approved=False,
            minimum_confidence=0.0,
            overwrite_attrs=True,
        )
