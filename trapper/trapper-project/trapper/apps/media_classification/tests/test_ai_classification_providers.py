import re
from typing import List
from unittest import mock
import uuid
from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from requests.sessions import Request
from urllib.parse import urljoin
import json
from trapper.apps.extra_tables.models import Species

from trapper.apps.media_classification.models import (
    SpeciesAILabelMapping,
)
from trapper.apps.media_classification.ai_providers.ai_provider_factory import (
    get_ai_provider_manager,
)
from trapper.apps.media_classification.ai_providers.ai_providers_base import (
    AIProviderException,
    RemoteClassificationParams,
)
from trapper.apps.media_classification.ai_providers.megadetector import (
    MegadetectorBatchProcessingProviderManager,
)
from trapper.apps.media_classification.ai_providers.trapper_ai import (
    TrapperAIBatchData,
    TrapperAIBatchProcessingStage,
    TrapperAIBatchProcessingStatus,
    TrapperAIJobCreatedResponse,
    TrapperAIPredictionResults,
    TrapperAIProviderManager,
    TrapperAIResourceStatus,
    TrapperAIService,
    TrapperAISample,
    TrapperAIJobData,
    TrapperAIJobStatusDto,
    TrapperAIJobStatus,
    TrapperAIDetectionRequest,
    TrapperAIProgressMetrics,
    TrapperAITaskResult,
    TrapperAiResourceStatusDto,
)

from trapper.apps.media_classification.models import (
    AIClassification,
    AIClassificationJob,
    AIClassificationJobAdditionalResource,
    AIProvider,
    AIProviderPermission,
)

from unittest.mock import ANY, MagicMock, patch
import responses

User = get_user_model()


class TestAIProviderFactory(TestCase):
    fixtures = ["ai_classification_tests_fixtures.yaml"]

    def test_trapper_ai_provider_factory(self):
        trapper_ai = AIProvider.objects.get(id=2)
        manager = get_ai_provider_manager(trapper_ai)
        assert isinstance(manager, TrapperAIProviderManager)

    def test_megadetector_provider_factory(self):
        megadetector = AIProvider.objects.get(id=3)
        manager = get_ai_provider_manager(megadetector)
        assert isinstance(manager, MegadetectorBatchProcessingProviderManager)


class TestTrapperAIProvider(TestCase):
    fixtures = ["ai_classification_tests_fixtures.yaml"]

    trapper_ai_provider_id = 2

    def setUp(self) -> None:
        super().setUp()
        self.job_user_id = 1

        schedule_status_update_patcher = patch(
            "trapper.apps.media_classification.ai_providers.trapper_ai.schedule_status_update"
        )
        self.schedule_status_update_mock = schedule_status_update_patcher.start()
        self.addCleanup(schedule_status_update_patcher.stop)

        # trapper_ai_patcher = patch.object(
        #     TrapperAIProviderManager, "trapper_ai_service"
        # )
        # self.trapper_ai_service_mock = trapper_ai_patcher.start()
        # self.addCleanup(trapper_ai_patcher.stop)

        self.trapper_ai_manager = TrapperAIProviderManager(
            AIProvider.objects.get(pk=self.trapper_ai_provider_id)
        )

        self.trapper_ai_service_mock = MagicMock()

        self.trapper_ai_manager.trapper_ai_service = self.trapper_ai_service_mock

    def _init_job(self):
        self.job = self.trapper_ai_manager.init_job(self.job_user_id)

    def _init_job_with_params(self, params):
        self._init_job()
        self.trapper_ai_manager.save_remote_classification_params(params, self.job.id)

    def _init_job_with_default_params(self):
        self._init_job_with_params(
            RemoteClassificationParams(
                classification_ids=[72], classify_preview_files=False, project_id=4
            )
        )

    def _reload_job(self):
        self.job = AIClassificationJob.objects.get(id=self.job.id)

    def _get_test_job_data(
        self,
        batches: List[TrapperAIBatchData],
        status: TrapperAIJobStatus = TrapperAIJobStatus.InProgress,
        # stage: TrapperAIBatchProcessingStage = TrapperAIBatchProcessingStage.FINISHED,
    ):
        return TrapperAIJobData(
            id=str(uuid.uuid4()),
            status=TrapperAIJobStatusDto.create(status),
            progress_metrics=TrapperAIProgressMetrics(123, 123, 123),
            batches=batches,
        )

    def _get_test_data(self):
        return [
            TrapperAITaskResult(
                id=uuid.uuid4(),
                sample_id=72,
                processing_error_msg="",
                job=str(self.job.id),
                status=TrapperAiResourceStatusDto.create(
                    TrapperAIResourceStatus.FINISHED
                ),
                prediction_results=TrapperAIPredictionResults(
                    bboxes=[
                        [
                            0.46784156560897827,
                            0.1654667854309082,
                            0.6257930994033813,
                            0.3594273328781128,
                        ],
                        [
                            0.1704215556383133,
                            0.400938481092453,
                            0.6312470436096191,
                            0.5249083042144775,
                        ],
                    ],
                    scores=[0.9997418522834778, 0.9915930032730103],
                    classes=[1.0, 2.0],
                ),
            )
        ]

    def test_should_initialize(self):
        params = RemoteClassificationParams(
            classification_ids=[72], classify_preview_files=False, project_id=4
        )
        self.trapper_ai_manager.request_classification(params, user_id=1)

        self.schedule_status_update_mock.assert_called_once()

    def test_should_request_classifications(self):
        # Given running classification job with params
        self._init_job_with_params(
            RemoteClassificationParams(
                classification_ids=[72], classify_preview_files=False, project_id=4
            )
        )

        self.trapper_ai_service_mock.request_detections.return_value = (
            TrapperAIJobCreatedResponse(id=str(uuid.uuid4()))
        )

        # when current stage is "prepare_classification_run"
        self.trapper_ai_manager.set_stage(self.job, "prepare_classification_run")

        # and we run status update
        self.trapper_ai_manager.update_remote_classification_task_status(self.job)

        # remote classification should be requested
        self.trapper_ai_service_mock.request_detections.assert_called_once()

        # stage should be changed
        self._reload_job()
        assert self.job.stage == "fetch_results"

        # and status update should be scheduled
        self.schedule_status_update_mock.assert_called_once()

    def test_should_fail_job_when_trapper_ai_service_fails(self):

        # Given running classification job with some params
        self._init_job_with_params(
            RemoteClassificationParams(
                classification_ids=[72], classify_preview_files=False, project_id=4
            )
        )

        # when current stage is "prepare_classification_run"
        self.trapper_ai_manager.set_stage(self.job, "prepare_classification_run")

        # when Trapper AI service provider throws exception
        self.trapper_ai_service_mock.request_detections.side_effect = (
            AIProviderException("test messaage")
        )

        # and we run status update
        self.trapper_ai_manager.update_remote_classification_task_status(self.job)

        # remote classification should be requested
        self.trapper_ai_service_mock.request_detections.assert_called_once()

        # but job should be failed
        self._reload_job()
        assert self.job.stage == "rejected"

        # and status update should NOT be scheduled
        self.schedule_status_update_mock.assert_not_called()

    def test_ai_result_parsing(self):
        # Given running classification job with params
        self._init_job_with_default_params()
        self.trapper_ai_manager._save_job_data(self.job, {"id": str(uuid.uuid4())})

        # and valid detection data
        data = self._get_test_data()

        self.trapper_ai_service_mock.fetch_detections.return_value = data
        self.trapper_ai_service_mock.fetch_job_status.return_value = (
            self._get_test_job_data(
                status=TrapperAIJobStatus.Done,
                batches=[
                    TrapperAIBatchData.create_finished(),
                ],
            )
        )

        # on fetch_resulsts stage
        self.trapper_ai_manager.set_stage(self.job, "fetch_results")

        # when status is updated
        self.trapper_ai_manager.update_remote_classification_task_status(self.job)

        # then job status should change do finished
        self._reload_job()
        assert self.job.stage == "finished"

        # and classification results should be available
        ai_classifications = AIClassification.objects.all()
        assert ai_classifications.count() == 1

        # and all attributes for all detections should be present
        ai_classification: AIClassification = ai_classifications.first()

        assert len(ai_classification.get_bboxes()) == 2
        assert ai_classification.dynamic_attrs.count() == 2
        assert list(
            ai_classification.dynamic_attrs.values_list("observation_type", flat=True)
        ) == [
            "Animal",
            "Human",
        ]

    def test_ai_result_parsing_for_species_mapping(self):
        # Given running classification job with params
        self._init_job_with_default_params()
        self.trapper_ai_manager._save_job_data(self.job, {"id": str(uuid.uuid4())})

        # And mapping for species set up
        species = [
            Species.objects.create(
                latin_name="Test Animal 1", english_name="Test Animal 1"
            ),
            Species.objects.create(
                latin_name="Test Animal 2", english_name="Test Animal 2"
            ),
        ]
        SpeciesAILabelMapping.objects.create(
            ai_provider_id=self.trapper_ai_provider_id, value="1", species=species[0]
        )
        SpeciesAILabelMapping.objects.create(
            ai_provider_id=self.trapper_ai_provider_id, value="2", species=species[1]
        )

        # and valid detection data
        data = self._get_test_data()
        self.trapper_ai_service_mock.fetch_detections.return_value = data
        self.trapper_ai_service_mock.fetch_job_status.return_value = (
            self._get_test_job_data(
                status=TrapperAIJobStatus.Done,
                batches=[
                    TrapperAIBatchData.create_finished(),
                ],
            )
        )
        # on parse_results stage
        self.trapper_ai_manager.set_stage(self.job, "fetch_results")

        # when status is updated
        self.trapper_ai_manager.update_remote_classification_task_status(self.job)

        # then job status should change do finished
        self._reload_job()
        self.assertEqual(self.job.stage, "finished")

        # and classification results should be available
        ai_classifications = AIClassification.objects.all()
        assert ai_classifications.count() == 1

        # and all attributes for all detections should be present
        ai_classification: AIClassification = ai_classifications.first()

        assert len(ai_classification.get_bboxes()) == 2
        assert ai_classification.dynamic_attrs.count() == 2
        assert list(
            ai_classification.dynamic_attrs.values_list(
                "species__latin_name", flat=True
            )
        ) == [
            "Test Animal 1",
            "Test Animal 2",
        ]

    def test_detection_results_with_confidence_below_threshold_should_be_rejected(self):
        # Given running classification job with params
        self._init_job_with_default_params()
        self.trapper_ai_manager._save_job_data(self.job, {"id": str(uuid.uuid4())})

        # and valid detection data with detection bellow confidence threshold
        data = self._get_test_data()
        data[0].prediction_results.scores[1] = 0.80

        self.trapper_ai_service_mock.fetch_detections.return_value = data
        self.trapper_ai_service_mock.fetch_job_status.return_value = (
            self._get_test_job_data(
                status=TrapperAIJobStatus.Done,
                batches=[
                    TrapperAIBatchData.create_finished(),
                ],
            )
        )

        # on parse_results stage
        self.trapper_ai_manager.set_stage(self.job, "fetch_results")

        # when status is updated
        self.trapper_ai_manager.update_remote_classification_task_status(self.job)

        # only attributes for observations above the threshold should be preserved
        ai_classifications = AIClassification.objects.all()
        self.assertEqual(ai_classifications.count(), 1)
        self.assertEqual(ai_classifications.first().dynamic_attrs.count(), 1)


class TestTrapperAIService(TestCase):
    api_url = "http://localhost/test_url"
    model_id = uuid.uuid4()
    project_id = 4
    user_id = 1
    task_id = uuid.uuid4()

    def setUp(self) -> None:
        super().setUp()
        self.service = TrapperAIService(
            api_url="http://localhost/test_url",
            login="test_login",
            password="test_password",
            model_id=self.model_id,
        )

    def _get_test_job_data(
        self,
        batches: List[TrapperAIBatchData],
        status: TrapperAIJobStatus = TrapperAIJobStatus.InProgress,
        # stage: TrapperAIBatchProcessingStage = TrapperAIBatchProcessingStage.FINISHED,
    ) -> TrapperAIJobData:
        return TrapperAIJobData(
            id=str(uuid.uuid4()),
            status=TrapperAIJobStatusDto.create(status),
            progress_metrics=TrapperAIProgressMetrics(
                in_progress=len(
                    [
                        batch
                        for batch in batches
                        if batch.processing_status
                        == TrapperAIBatchProcessingStatus.IN_PROGRESS
                    ]
                ),
                finished=len(
                    [
                        batch
                        for batch in batches
                        if batch.processing_status
                        == TrapperAIBatchProcessingStatus.SUCCEEDED
                    ]
                ),
                failed=len(
                    [
                        batch
                        for batch in batches
                        if batch.processing_status
                        == TrapperAIBatchProcessingStatus.FAILED
                    ]
                ),
            ),
            batches=batches,
        )

    @responses.activate
    def test_should_post_params_to_trapper_ai(self):
        endpoint = urljoin(self.api_url, TrapperAIService.ENDPOINT_REQUEST_DETECTIONS)

        samples = [TrapperAISample(sample_id=1, sample_url="test_url")]

        request_params = TrapperAIDetectionRequest(
            ai_model_id=self.model_id,
            samples=samples,
        )

        expected_return_value = TrapperAIJobCreatedResponse(id=str(uuid.uuid4()))

        responses.add(
            responses.POST,
            endpoint,
            status=201,
            body=expected_return_value.to_json(),
            match=[
                responses.matchers.json_params_matcher(
                    json.loads(request_params.to_json())
                )
            ],
        )

        response = self.service.request_detections(
            samples=samples,
        )

        responses.assert_call_count(endpoint, 1)

        self.assertDictEqual(response.to_dict(), expected_return_value.to_dict())

    @responses.activate
    def test_fetch_job_status(self):
        endpoint = urljoin(self.api_url, TrapperAIService.ENDPOINT_REQUEST_JOB_STATUS)
        endpoint_pattern = re.compile(endpoint + ".*/")

        expected_return_value: TrapperAIJobData = self._get_test_job_data(
            batches=[TrapperAIBatchData.create_finished()]
        )

        responses.add(
            responses.GET,
            endpoint_pattern,
            status=201,
            body=expected_return_value.to_json(),
        )

        response = self.service.fetch_job_status(job_id=expected_return_value.id)

        self.assertDictEqual(response.to_dict(), expected_return_value.to_dict())

    @responses.activate
    def test_should_throw_exception_on_request_failure(self):
        endpoint = urljoin(self.api_url, TrapperAIService.ENDPOINT_REQUEST_DETECTIONS)
        responses.add(responses.POST, endpoint, status=400)

        samples = [TrapperAISample(sample_id=1, sample_url="test_url")]

        with self.assertRaises(AIProviderException):
            self.service.request_detections(
                samples=samples,
            )

        responses.assert_call_count(endpoint, 1)


class TestMegadetectorProvider(TestCase):
    fixtures = ["ai_classification_tests_fixtures.yaml"]

    megadetector_provider_id = 3
    job_user_id = 1

    def _init_job(self):
        self.job = self.megadetector_manager.init_job(self.job_user_id)

    def _init_job_with_params(self, params):
        self._init_job()
        self.megadetector_manager.save_remote_classification_params(params, self.job.id)

    def _init_job_with_default_params(self):
        self._init_job_with_params(
            RemoteClassificationParams(
                classification_ids=[72], classify_preview_files=False, project_id=4
            )
        )

    def _reload_job(self):
        self.job = AIClassificationJob.objects.get(id=self.job.id)

    def _assert_status(self, status: str):
        self._reload_job()
        assert self.job.stage == status

    def _use_default_request_params(self):
        request_params = {"request_params": "test"}

        AIClassificationJobAdditionalResource(
            resource_name="request_params",
            ai_provider_id=self.megadetector_provider_id,
            classification_job=self.job,
            data=request_params,
        ).save()

    def setUp(self) -> None:
        super().setUp()
        self.job_user_id = 1

        schedule_status_update_patcher = patch(
            "trapper.apps.media_classification.ai_providers.megadetector.schedule_status_update"
        )
        self.schedule_status_update_mock = schedule_status_update_patcher.start()
        self.addCleanup(schedule_status_update_patcher.stop)

        azure_patcher = patch.object(
            MegadetectorBatchProcessingProviderManager, "azure_service"
        )
        self.azure_service_mock = azure_patcher.start()
        self.addCleanup(azure_patcher.stop)

        # used for creating image list URL for image list stored in Azure Storage
        self.azure_service_mock.return_value.make_blob_url.return_value = "test_url"

        # used for determinign container URL when using Azure Storage for images
        self.azure_service_mock.return_value.make_container_url.return_value = (
            "container_url"
        )

        self.megadetector_manager = MegadetectorBatchProcessingProviderManager(
            AIProvider.objects.get(pk=self.megadetector_provider_id)
        )

    def test_should_initialize(self):
        params = RemoteClassificationParams(
            classification_ids=[72], classify_preview_files=False, project_id=4
        )
        self.megadetector_manager.request_classification(params, user_id=1)

        self.schedule_status_update_mock.assert_called_once()

    def _prepare_classification_run_test_grid(
        self, azure_storage_for_images: bool, azure_storage_for_image_list: bool
    ):
        # Given running classification job with params
        self._init_job_with_default_params()
        self.megadetector_manager.config.use_azure_storage_for_images = (
            azure_storage_for_images
        )
        self.megadetector_manager.config.use_azure_storage_for_image_list = (
            azure_storage_for_image_list
        )
        self.megadetector_manager.config.save()

        # when current stage is "prepare_classification_run"
        self.megadetector_manager.set_stage(self.job, "prepare_classification_run")

        # and we run status update
        self.megadetector_manager.update_remote_classification_task_status(self.job)

        # stage should be changed
        self._assert_status("start_classification_process")

        # and status update should be scheduled
        self.schedule_status_update_mock.assert_called_once_with(self.job.id, ANY)

    def test_prepare_classification_run_public_links_public_list(self):
        self._prepare_classification_run_test_grid(
            azure_storage_for_images=False, azure_storage_for_image_list=False
        )

        images_requested = AIClassificationJobAdditionalResource.objects.get(
            classification_job=self.job, resource_name="images_requested"
        )

        assert images_requested.data[0][0].startswith(
            self.megadetector_manager.config.trapper_instance_url
        )
        assert len(images_requested.data) == 1

        saved_params = AIClassificationJobAdditionalResource.objects.get(
            classification_job=self.job, resource_name="request_params"
        )

        assert saved_params.data["images_requested_json_sas"].endswith(
            reverse(
                "media_classification:api-classify-ai-extra",
                kwargs={
                    "ai_provider_token": self.megadetector_manager.config.token,
                    "classification_job_id": self.job.id,
                    "resource_name": "images_requested",
                },
            ),
        )

    def test_prepare_classification_run_public_links_azure_list(self):
        storage_mock = MagicMock()
        storage_mock._open.return_value = "test_data"
        with mock.patch(
            "django.core.files.storage.default_storage._wrapped", storage_mock
        ):
            self._prepare_classification_run_test_grid(
                azure_storage_for_images=False, azure_storage_for_image_list=True
            )

        saved_params = AIClassificationJobAdditionalResource.objects.get(
            classification_job=self.job, resource_name="request_params"
        )

        # megadetector image list saved in azure blob
        assert saved_params.data["images_requested_json_sas"] == "test_url"

    def test_prepare_classification_run_azure_links_public_list(self):
        storage_mock = MagicMock()
        storage_mock._open.return_value = "test_data"
        with mock.patch(
            "django.core.files.storage.default_storage._wrapped", storage_mock
        ):
            self._prepare_classification_run_test_grid(
                azure_storage_for_images=True, azure_storage_for_image_list=False
            )

        saved_params = AIClassificationJobAdditionalResource.objects.get(
            classification_job=self.job, resource_name="request_params"
        )

        assert saved_params.data["images_requested_json_sas"].endswith(
            reverse(
                "media_classification:api-classify-ai-extra",
                kwargs={
                    "ai_provider_token": self.megadetector_manager.config.token,
                    "classification_job_id": self.job.id,
                    "resource_name": "images_requested",
                },
            ),
        )

    def test_prepare_classification_run_azure_links_azure_list(self):
        storage_mock = MagicMock()
        storage_mock._open.return_value = "test_data"
        with mock.patch(
            "django.core.files.storage.default_storage._wrapped", storage_mock
        ):
            self._prepare_classification_run_test_grid(
                azure_storage_for_images=True, azure_storage_for_image_list=True
            )

        saved_params = AIClassificationJobAdditionalResource.objects.get(
            classification_job=self.job, resource_name="request_params"
        )

        # megadetector image list saved in azure blob
        assert saved_params.data["images_requested_json_sas"] == "test_url"

    @responses.activate
    def test_start_classification_process_when_megadetector_is_unavailable(self):
        # Given running classification job with params
        self._init_job_with_default_params()

        # and saved default params
        self._use_default_request_params()

        # when current stage is "start_classification_process"
        self.megadetector_manager.set_stage(self.job, "start_classification_process")

        # when megadetector "request_detections" endpoint returns 503
        responses.add(
            responses.POST, "https://localhost/request_detections", status=503
        )

        # and we run status update
        self.megadetector_manager.update_remote_classification_task_status(self.job)

        # stage should not be changed
        self._assert_status("start_classification_process")

        # and status update should be re-scheduled in 30 seconds
        self.schedule_status_update_mock.assert_called_once_with(self.job.id, 30)

    @responses.activate
    def test_start_classification_process(self):
        # Given running classification job with params
        self._init_job_with_default_params()

        # and saved default params
        self._use_default_request_params()

        # when current stage is "start_classification_process"
        self.megadetector_manager.set_stage(self.job, "start_classification_process")

        # when megadetector "request_detections" returns valid response
        megadetector_response = {"dummy": "value"}
        responses.add(
            responses.POST,
            "https://localhost/request_detections",
            json=megadetector_response,
        )

        # and we run status update
        self.megadetector_manager.update_remote_classification_task_status(self.job)

        # stage should be changed
        self._assert_status("fetch_results")

        # and status update should be re-scheduled
        self.schedule_status_update_mock.assert_called_once_with(self.job.id, ANY)

        # and response should be preserved
        job_data: AIClassificationJobAdditionalResource = (
            AIClassificationJobAdditionalResource.objects.get(
                resource_name="megadetector_job_data",
                ai_provider_id=self.megadetector_provider_id,
            )
        )

        assert job_data.data == megadetector_response

    @responses.activate
    def test_start_classification_process_when_megadetector_returns_failure(self):
        # Given running classification job with params
        self._init_job_with_default_params()

        # and saved default params
        self._use_default_request_params()

        # when current stage is "start_classification_process"
        self.megadetector_manager.set_stage(self.job, "start_classification_process")

        # when megadetector "request_detections" endpoint returns an error other than 503
        megadetector_response = {"dummy": "value"}
        responses.add(
            responses.POST,
            "https://localhost/request_detections",
            json={"error": "megadetector_error"},
            status=400,
        )

        # and we run status update
        self.megadetector_manager.update_remote_classification_task_status(self.job)

        # stage should be changed
        self._assert_status("rejected")

        # and status update should not be re-scheduled
        self.schedule_status_update_mock.assert_not_called()

    @responses.activate
    def test_fetch_results_when_process_still_running(self):
        # Given running classification job with params
        self._init_job_with_default_params()

        # and saved megadetector job data
        megadetector_job_data = AIClassificationJobAdditionalResource(
            resource_name="megadetector_job_data",
            ai_provider_id=self.megadetector_provider_id,
            classification_job=self.job,
            data={"request_id": "test-request-id"},
        )
        megadetector_job_data.save()

        # when current stage is "fetch_results"
        self.megadetector_manager.set_stage(self.job, "fetch_results")

        # when megadetector returns "running" status
        megadetector_response = {"Status": {"request_status": "running"}}
        responses.add(
            responses.GET,
            "https://localhost/task/test-request-id",
            json=megadetector_response,
        )

        # and we run status update
        self.megadetector_manager.update_remote_classification_task_status(self.job)

        # stage should NOT be changed
        self._assert_status("fetch_results")

        # and status update should be re-scheduled in 30 seconds
        self.schedule_status_update_mock.assert_called_once_with(self.job.id, 30)

    def _get_detections_message(self):
        return {
            "info": {
                "detector": "megadetector_v4.1",
                "format_version": "1.1",
                "detection_completion_time": "2021-08-05T15:21:06.397901Z",
            },
            "images": [
                {
                    "file": "000115_20090107210534.jpg",
                    "meta": '{"object_id": 72}',
                    "detections": [
                        {
                            "bbox": [0.764, 0.286, 0.211, 0.463],
                            "conf": 0.999,
                            "category": "1",
                        },
                        {
                            "bbox": [0.6626, 0.305, 0.03397, 0.05654],
                            "conf": 0.999,
                            "category": "2",
                        },
                    ],
                    "max_detection_conf": 0.999,
                },
            ],
            "detection_categories": {"1": "animal", "2": "person", "3": "vehicle"},
        }

    @responses.activate
    def test_fetch_results_when_process_completed_success(self):
        # Given running classification job with params
        self._init_job_with_default_params()

        # and saved megadetector job data
        megadetector_job_data = AIClassificationJobAdditionalResource(
            resource_name="megadetector_job_data",
            ai_provider_id=self.megadetector_provider_id,
            classification_job=self.job,
            data={"request_id": "test-request-id"},
        )
        megadetector_job_data.save()

        # when current stage is "fetch_results"
        self.megadetector_manager.set_stage(self.job, "fetch_results")

        # when megadetector returns "completed" status
        megadetector_response = {
            "Status": {
                "request_status": "completed",
                "message": {
                    "num_failed_shards": 0,
                    "output_file_urls": {
                        "detections": "http://localhost/detections_file_url"
                    },
                },
                "time": "2020-07-09 21:27:17",
            }
        }
        responses.add(
            responses.GET,
            "https://localhost/task/test-request-id",
            json=megadetector_response,
        )

        detections_message = self._get_detections_message()

        responses.add(
            responses.GET,
            "http://localhost/detections_file_url",
            json=detections_message,
        )

        # and we run status update
        self.megadetector_manager.update_remote_classification_task_status(self.job)

        # stage should be changed
        self._assert_status("parse_results")

        # and status update should be re-scheduled
        self.schedule_status_update_mock.assert_called_once_with(self.job.id)

        # and detection results should be preserved in database
        megadetector_results_resource = (
            AIClassificationJobAdditionalResource.objects.get(
                resource_name="megadetector_results",
                ai_provider_id=self.megadetector_provider_id,
                classification_job=self.job,
            )
        )

        assert megadetector_results_resource.data == detections_message

    @responses.activate
    def test_fetch_results_when_process_completed_failure(self):
        # Given running classification job with params
        self._init_job_with_default_params()

        # and saved megadetector job data
        megadetector_job_data = AIClassificationJobAdditionalResource(
            resource_name="megadetector_job_data",
            ai_provider_id=self.megadetector_provider_id,
            classification_job=self.job,
            data={"request_id": "test-request-id"},
        )
        megadetector_job_data.save()

        # when current stage is "fetch_results"
        self.megadetector_manager.set_stage(self.job, "fetch_results")

        # when megadetector returns "running" status
        megadetector_response = {
            "Status": {"request_status": "completed", "message": "error"}
        }
        responses.add(
            responses.GET,
            "https://localhost/task/test-request-id",
            json=megadetector_response,
        )

        # and we run status update
        self.megadetector_manager.update_remote_classification_task_status(self.job)

        # stage should be changed
        self._assert_status("failed")

        # and status update should NOT be re-scheduled
        self.schedule_status_update_mock.assert_not_called()

    def test_parse_results(self):
        # Given running classification job with params
        self._init_job_with_default_params()

        # and saved megadetector results
        detections_message = self._get_detections_message()
        megadetector_results_resource = AIClassificationJobAdditionalResource(
            resource_name="megadetector_results",
            ai_provider_id=self.megadetector_provider_id,
            classification_job=self.job,
            data=detections_message,
        )
        megadetector_results_resource.save()

        # when current stage is "parse_results"
        self.megadetector_manager.set_stage(self.job, "parse_results")

        # and we run status update
        self.megadetector_manager.update_remote_classification_task_status(self.job)

        # stage should be changed
        self._assert_status("finished")

        # and status update should NOT be re-scheduled
        self.schedule_status_update_mock.assert_not_called()

        # and classification results should be available
        ai_classifications = AIClassification.objects.all()
        assert ai_classifications.count() == 1

        # and all attributes for all detections should be present
        ai_classification: AIClassification = ai_classifications.first()

        assert len(ai_classification.get_bboxes()) == 2
        assert ai_classification.dynamic_attrs.count() == 2
        assert list(
            ai_classification.dynamic_attrs.values_list("observation_type", flat=True)
        ) == [
            "Animal",
            "Human",
        ]

    def test_parse_results_reject_observations_below_confidence_threshold(self):
        # Given running classification job with params
        self._init_job_with_default_params()

        # and saved megadetector results
        detections_message = self._get_detections_message()
        detections_message["images"][0]["detections"][1]["conf"] = 0.184
        megadetector_results_resource = AIClassificationJobAdditionalResource(
            resource_name="megadetector_results",
            ai_provider_id=self.megadetector_provider_id,
            classification_job=self.job,
            data=detections_message,
        )
        megadetector_results_resource.save()

        # when current stage is "parse_results"
        self.megadetector_manager.set_stage(self.job, "parse_results")

        # and we run status update
        self.megadetector_manager.update_remote_classification_task_status(self.job)

        # stage should be changed
        self._assert_status("finished")

        # and status update should NOT be re-scheduled
        self.schedule_status_update_mock.assert_not_called()

        # and classification results should be available
        ai_classifications = AIClassification.objects.all()
        assert ai_classifications.count() == 1

        # and all attributes for all detections should be present
        ai_classification: AIClassification = ai_classifications.first()

        assert len(ai_classification.get_bboxes()) == 1
        assert ai_classification.dynamic_attrs.count() == 1
        assert list(
            ai_classification.dynamic_attrs.values_list("observation_type", flat=True)
        ) == [
            "Animal",
        ]


class TestAIProviderPermissions(TestCase):
    fixtures = ["ai_classification_tests_fixtures.yaml"]

    def setUp(self):
        self.superuser = User.objects.get(id=1)
        self.superuser.is_superuser = True
        self.superuser.save()
        self.user = User.objects.get(id=2)

        self.provider1 = AIProvider.objects.get(id=2)
        self.provider2 = AIProvider.objects.get(id=3)

    def test_ai_provider_access_for_superuser(self):
        # all providers should be accessible to the superuser
        accessible = AIProvider.objects.get_accessible(user=self.superuser)
        assert self.provider1 in accessible
        assert self.provider2 in accessible

    def test_ai_provider_access(self):
        # by default, no providers should be accessible
        accessible = AIProvider.objects.get_accessible(user=self.user)
        assert self.provider1 not in accessible
        assert self.provider2 not in accessible

        # with permission, provider1 should be accessible
        permission = AIProviderPermission(user=self.user, ai_provider=self.provider1)
        permission.save()

        accessible = AIProvider.objects.get_accessible(user=self.user)
        assert self.provider1 in accessible
        assert self.provider2 not in accessible
