# -*- coding: utf-8 -*-
import json
from zipfile import ZipFile

import pandas
from django.test import TestCase
from django.urls import reverse
from trapper.apps.accounts.models import UserDataPackage
from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.common.utils.test_tools import BaseMediaClassificationTestCase
from trapper.apps.geomap.tests.factories.deployment import DeploymentFactory
from trapper.apps.geomap.tests.factories.location import LocationFactory
from trapper.apps.media_classification.models import AIProvider
from trapper.apps.media_classification.taxonomy import (
    ClassificationProjectRoleLevels,
    ClassificationStatus,
)
from trapper.apps.media_classification.tests.factories.classification import (
    ClassificationFactory,
)
from trapper.apps.media_classification.tests.factories.classification_project_collection import (
    ClassificationProjectCollectionFactory,
)
from trapper.apps.media_classification.tests.factories.classification_project_role import (
    ClassificationProjectRoleFactory,
)
from trapper.apps.media_classification.tests.factories.classificator import (
    ClassificatorFactory,
)
from trapper.apps.media_classification.tests.factories.user_classification import (
    UserClassificationFactory,
)
from trapper.apps.research.models import ResearchProjectCollection
from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)
from trapper.apps.storage.models import Resource
from trapper.apps.storage.taxonomy import ResourceStatus, CollectionStatus
from trapper.apps.storage.tests.factories.classification_project import (
    ClassificationProjectFactory,
)
from trapper.apps.storage.tests.factories.collection import CollectionFactory
from trapper.apps.storage.tests.factories.resource import ResourceFactory


class BaseClassificationTestCase(BaseMediaClassificationTestCase):
    pass


class AnonymousClassificationTestCase(BaseClassificationTestCase):
    """
    Classifications logic for anonymous users.
    """

    def test_classification_json(self):
        """
        Anonymous user can not see classification API data.
        """
        url = reverse("media_classification:api-classification-list")
        self.assert_forbidden(url)

    def test_user_classification_json(self):
        """
        Anonymous user can not see user classification API data.
        """
        url = reverse("media_classification:api-user-classification-list")
        self.assert_forbidden(url)

    def test_details(self):
        """
        Anonymous user has to login before seeing classification.
        """
        url = reverse(
            "media_classification:classify_user", kwargs={"pk": 1, "user_pk": 1}
        )
        self.assert_forbidden(url=url)

    def test_delete(self):
        """
        Anonymous user should not get access here.
        """
        url = reverse("media_classification:classification_delete", kwargs={"pk": 1})
        self.assert_forbidden(url=url)

    def test_feedback(self):
        """
        Anonymous user has to login before the feedback action.
        """
        url = reverse("media_classification:classification_feedback", kwargs={"pk": 1})
        self.assert_forbidden(url=url)


class ClassificationListPermissionTestCase(BaseClassificationTestCase):
    """
    TODO: docstrings
    """

    def setUp(self):
        """
        Login alice by default for all tests.
        """
        super().setUp()
        self.login_alice()

    def _call_helper(self, owner, roles=None):
        """
        All tests call the same logic annd logged in user will get always
        response status 200. Difference is in response data which is json.
        """
        super()._call_helper(owner, roles)
        url = (
            reverse(
                "media_classification:api-classification-list",
            )
            + f"?project={self.classification_project.pk}"
        )
        response = self.assert_access_granted(url)
        content = json.loads(response.content)["results"]
        pk_list = [item["pk"] for item in content]
        return pk_list

    def test_classification_owner_json(self):
        """
        Owner can see project's classifications.
        """
        owner = self.alice
        roles = None
        pk_list = self._call_helper(owner=owner, roles=roles)
        self.assertIn(self.classification.pk, pk_list)

    def test_classification_role_admin_json(self):
        """
        User with ADMIN role can see project's classifications.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        pk_list = self._call_helper(owner=owner, roles=roles)
        self.assertIn(self.classification.pk, pk_list)

    def test_classification_role_expert_json(self):
        """
        User with EXPERT role can not see project's classifications.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        pk_list = self._call_helper(owner=owner, roles=roles)
        self.assertNotIn(self.classification.pk, pk_list)

    def test_classification_role_collaborator_json(self):
        """
        User with COLLABORATOR role can see project's classifications.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        pk_list = self._call_helper(owner=owner, roles=roles)
        self.assertIn(self.classification.pk, pk_list)

    def test_classification_no_roles_json(self):
        """
        User with no roles can not see project's classifications.
        """
        owner = self.ziutek
        roles = None
        pk_list = self._call_helper(owner=owner, roles=roles)
        self.assertNotIn(self.classification.pk, pk_list)


class ClassificationDetailsPermissionTestCase(BaseClassificationTestCase):
    """
    TODO: docstrings
    """

    url = None

    def setUp(self):
        """
        Login alice by default for all tests.
        """
        super().setUp()
        self.login_alice()

    def _call_helper(self, owner, roles=None):
        super()._call_helper(owner, roles)
        self.url = self.get_classification_detail_url(
            classification=self.classification, user=owner
        )

    def test_owner(self):
        """
        Owner can see classification details.
        """
        owner = self.alice
        roles = None
        self._call_helper(owner=owner, roles=roles)
        self.assert_access_granted(self.url)

    def test_role_admin(self):
        """
        User with ADMIN role can see classification details.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(owner=owner, roles=roles)
        self.assert_access_granted(self.url)

    def test_role_expert(self):
        """
        User with EXPERT role can see classification details.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(owner=owner, roles=roles)
        self.assert_access_granted(self.url)

    def test_role_collaborator(self):
        """
        User with COLLABORATOR role can see classification details.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        self._call_helper(owner=owner, roles=roles)
        self.assert_access_granted(self.url)

    def test_no_roles(self):
        """
        User with no roles can not see classification details.
        """
        owner = self.ziutek
        roles = None
        self._call_helper(owner=owner, roles=roles)
        self.assert_forbidden(self.url)


class ClassificationDeletePermissionTestCase(BaseClassificationTestCase):
    """
    TODO: docstrings
    """

    url = None
    redirect_url = None

    def setUp(self):
        """
        Login alice by default for all tests.
        """
        super().setUp()
        self.login_alice()

    def _call_helper(self, owner, roles=None):
        super()._call_helper(owner, roles)
        self.url = self.get_classification_delete_url(
            classification=self.classification
        )
        self.redirect_url = reverse(
            "media_classification:classification_list",
            kwargs={"pk": self.classification_project.pk},
        )

    def test_owner(self):
        """
        Owner can delete classification.
        """
        owner = self.alice
        roles = None
        self._call_helper(owner=owner, roles=roles)
        self.assert_redirect(self.url, self.redirect_url)

    def test_role_admin(self):
        """
        User with ADMIN role can delete classification.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(owner=owner, roles=roles)
        self.assert_redirect(self.url, self.redirect_url)

    def test_role_expert(self):
        """
        User with EXPERT role can not delete classification.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(owner=owner, roles=roles)
        self.assert_forbidden(self.url)

    def test_role_collaborator(self):
        """
        User with COLLABORATOR role can not delete classification.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        self._call_helper(owner=owner, roles=roles)
        self.assert_forbidden(self.url)

    def test_no_roles(self):
        """
        User with no roles can not delete classification.
        """
        owner = self.ziutek
        roles = None
        self._call_helper(owner=owner, roles=roles)
        self.assert_forbidden(self.url)


class ClassificationFeedbackPermissionTestCase(BaseClassificationTestCase):
    """
    TODO: docstrings
    """

    response = None

    def setUp(self):
        """
        Login alice by default for all tests.
        """
        super().setUp()
        self.login_alice()

    def _call_helper(self, owner, roles, status):
        super()._call_helper(owner, roles)
        self.classification.status = status
        self.classification.save()
        self.params = {"classification_pks": self.classification.pk}
        self.url = reverse(
            "media_classification:classification_feedback",
            kwargs={"pk": self.classification_project.pk},
        )

    def test_owner(self):
        """
        Resource owner can not run the feedback action for not approved classifications.
        """
        owner = self.alice
        roles = None
        self._call_helper(
            owner=owner, roles=roles, status=ClassificationStatus.REJECTED
        )
        self.response = self.assert_access_granted(
            self.url, method="post", data=self.params
        )
        status = self.assert_json_context_variable(self.response, "success")
        self.assertFalse(status)

    def test_role_admin(self):
        """
        User with the ADMIN role can not run the feedback action for not approved
        classification.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(
            owner=owner, roles=roles, status=ClassificationStatus.REJECTED
        )
        self.response = self.assert_access_granted(
            self.url, method="post", data=self.params
        )
        status = self.assert_json_context_variable(self.response, "success")
        self.assertFalse(status)

    def test_role_expert(self):
        """
        User with the EXPERT role can not run the feedback action.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(
            owner=owner, roles=roles, status=ClassificationStatus.REJECTED
        )
        self.assert_forbidden(self.url)

    def test_role_collaborator(self):
        """
        User with the COLLABORATOR role can not run the feedback action.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        self._call_helper(
            owner=owner, roles=roles, status=ClassificationStatus.REJECTED
        )
        self.assert_forbidden(self.url)

    def test_no_roles(self):
        """
        User with no roles can not run the feedback action.
        """
        owner = self.ziutek
        roles = None
        self._call_helper(
            owner=owner, roles=roles, status=ClassificationStatus.REJECTED
        )
        self.assert_forbidden(self.url)

    def test_owner_approved(self):
        """
        Resource owner can run the feedback action for approved classifications.
        """
        owner = self.alice
        roles = None
        self._call_helper(
            owner=owner, roles=roles, status=ClassificationStatus.APPROVED
        )
        self.response = self.assert_access_granted(
            self.url, method="post", data=self.params
        )
        status = self.assert_json_context_variable(self.response, "status")
        self.assertTrue(status)

    def test_role_admin_approved(self):
        """
        User with the ADMIN role (but not a resouce owner) can not run the feedback
        action for selected approved classification.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(
            owner=owner, roles=roles, status=ClassificationStatus.APPROVED
        )
        self.response = self.assert_access_granted(
            self.url, method="post", data=self.params
        )
        status = self.assert_json_context_variable(self.response, "success")
        self.assertFalse(status)


class ClassificationExportDataPackageTestCase(BaseClassificationTestCase):
    """
    TODO: docstrings
    """

    params = None
    species = None
    url = None
    redirect_url = None

    def setUp(self):
        """Login alice by default for all tests"""
        super().setUp()
        self.login_alice()

    def _call_helper(self, owner, roles, **kwargs):
        super()._call_helper(owner, roles, **kwargs)
        self.params = {
            "name": "TEST_PACKAGE",
            "title": self.research_project.name,
            "version": "0.1.0",
            "keywords": "ecology,camera traps",
            "description": "",
            "methods": "",
        }
        self.species = self.create_species()
        dattrs = self.classification.dynamic_attrs.all()[0]
        dattrs.species = self.species
        dattrs.save()
        self.url = self.get_classification_export_url(
            project=self.classification_project
        )
        self.redirect_url = self.get_classification_project_details_url(
            project=self.classification_project
        )

    def export_package(self):
        self.assert_redirect(
            self.url, redirection=self.redirect_url, method="post", data=self.params
        )
        qs = UserDataPackage.objects.filter(user=self.alice, package_type="C")
        self.assertTrue(qs.exists())
        if qs.exists():
            package = qs.first()
            with ZipFile(package.package.file) as zip_file:
                with zip_file.open("datapackage.json", "r") as _file:
                    descriptor = json.loads(_file.read())
                with zip_file.open("data/locations.csv", "r") as _file:
                    loc_df = pandas.read_csv(_file)
                with zip_file.open("data/deployments.csv", "r") as _file:
                    dep_df = pandas.read_csv(_file)
                with zip_file.open("data/observations.csv", "r") as _file:
                    obs_df = pandas.read_csv(_file)
            self.assertEqual(descriptor.get("name"), "TEST_PACKAGE")
            self.assertEqual(
                descriptor.get("project").get("acronym"), self.research_project.acronym
            )
            self.assertEqual(len(loc_df), 1)
            self.assertEqual(
                loc_df.location_id[0], self.resource.deployment.location.location_id
            )
            self.assertEqual(len(dep_df), 1)
            self.assertEqual(
                dep_df.deployment_id[0], self.resource.deployment.deployment_id
            )
            self.assertEqual(len(obs_df), 1)
            self.assertEqual(obs_df.species_common[0], self.species.english_name)

    # def test_export_owner(self):
    #     self._call_helper(
    #         owner=self.alice, roles=None, status=ClassificationStatus.APPROVED
    #     )
    #     self.export_package()

    # def test_export_admin(self):
    #     roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
    #     self._call_helper(
    #         owner=self.ziutek, roles=roles, status=ClassificationStatus.APPROVED
    #     )
    #     self.export_package()

    def test_export_expert(self):
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(
            owner=self.ziutek, roles=roles, status=ClassificationStatus.APPROVED
        )
        self.assert_forbidden(self.url)

    def test_export_collaborator(self):
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        self._call_helper(
            owner=self.ziutek, roles=roles, status=ClassificationStatus.APPROVED
        )
        self.assert_forbidden(self.url)

    def test_export_noroles(self):
        roles = None
        self._call_helper(
            owner=self.ziutek, roles=roles, status=ClassificationStatus.APPROVED
        )
        self.assert_forbidden(self.url)
