import datetime
import json
from unittest.mock import Mock, patch

import pandas
from django.test import TestCase
from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.common.utils.test_tools import BaseMediaClassificationTestCase
from trapper.apps.geomap.tests.factories.deployment import DeploymentFactory
from trapper.apps.geomap.tests.factories.location import LocationFactory
from trapper.apps.media_classification.models import Classification, AIClassification
from trapper.apps.media_classification.tasks import (
    ClassificationImporter,
    SequencesBuilder,
    ResourceFeedback,
    ResultsDataPackageGenerator,
    celery_import_classifications,
    celery_build_sequences,
    celery_resource_feedback,
    PublishDataPackage,
    ApproveUserClassifications,
)
from trapper.apps.media_classification.taxonomy import ClassificationProjectRoleLevels
from trapper.apps.media_classification.tests.factories.classification import (
    ClassificationFactory,
)
from trapper.apps.media_classification.tests.factories.classification_project_collection import (
    ClassificationProjectCollectionFactory,
)
from trapper.apps.media_classification.tests.factories.classification_project_role import (
    ClassificationProjectRoleFactory,
)
from trapper.apps.media_classification.tests.factories.classificator import (
    ClassificatorFactory,
)
from trapper.apps.media_classification.tests.factories.user_classification import (
    UserClassificationFactory,
)
from trapper.apps.research.models import ResearchProjectCollection
from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)
from trapper.apps.storage.models import Resource
from trapper.apps.storage.taxonomy import ResourceStatus, CollectionStatus
from trapper.apps.storage.tests.factories.classification_project import (
    ClassificationProjectFactory,
)
from trapper.apps.storage.tests.factories.collection import CollectionFactory
from trapper.apps.storage.tests.factories.resource import ResourceFactory


class ClassificationImporterTestCase(BaseMediaClassificationTestCase):
    def setUp(self):
        super().setUp()
        self._owner = UserFactory()
        self._call_helper(self._owner, status=False)
        self._species = self.create_species()
        self.classificator.species.add(self._species)
        self.classificator.sex = True
        self.classificator.age = True
        self.classificator.behaviour = True
        self.classificator.save()
        self._observations = [
            {
                "observationType": "animal",
                "scientificName": self._species.latin_name,
                "count": 1,
                "lifeStage": "adult",
                "sex": "female",
                "behavior": "undefined",
                "_id": str(self.classification.pk),
                "classificationMethod": "human",
                "bboxes": "[0.25, 0.34, 0.1, 0.55]",
            },
            {
                "observationType": "animal",
                "scientificName": self._species.latin_name,
                "count": 1,
                "lifeStage": "adult",
                "sex": "male",
                "behavior": "undefined",
                "_id": str(self.classification.pk),
                "classificationMethod": "machine",
                "bboxes": "[0.25, 0.34, 0.1, 0.55]",
            },
        ]

    def get_params(
        self,
        observations=None,
        user=None,
        approve=True,
        import_bboxes=False,
        import_expert=True,
        import_ai=False,
        overwrite_attrs=False,
    ):
        observations = observations or self._observations
        return {
            "user": user or self._owner,
            "project": self.classification_project,
            "data": pandas.DataFrame(observations),
            "approve": approve,
            "import_bboxes": import_bboxes,
            "import_expert_classifications": import_expert,
            "import_ai_classifications": import_ai,
            "overwrite_attrs": overwrite_attrs,
        }

    def test_init(self):
        params = self.get_params(import_bboxes=True)
        importer = ClassificationImporter(**params)
        self.assertEqual(importer.user, self._owner)
        self.assertEqual(importer.data.to_dict("records"), self._observations)
        self.assertEqual(importer.species, {self._species.latin_name: self._species.pk})

    def test_import_classifications_success(self):
        self.classification.delete(clear=True)
        params = self.get_params()
        importer = ClassificationImporter(**params)
        importer.validate_table()
        self.assertTrue(importer.report.valid is True)
        importer.import_classifications()
        classification = Classification.objects.get(pk=self.classification.pk)
        self.assertTrue(classification.status)
        self.assertEqual(classification.approved_by, self._owner)
        self.assertTrue(
            classification.dynamic_attrs.filter(species=self._species).exists()
        )
        self.assertTrue(
            classification.dynamic_attrs.filter(observation_type="animal").exists()
        )

    def test_import_classifications_with_bboxes(self):
        self.classification.delete(clear=True)
        params = self.get_params(import_bboxes=True)
        importer = ClassificationImporter(**params)
        importer.validate_table()
        self.assertTrue(importer.report.valid is True)
        importer.import_classifications()

        classification = Classification.objects.get(pk=self.classification.pk)
        self.assertTrue(classification.status)
        self.assertTrue(classification.has_bboxes)
        self.assertTrue(classification.dynamic_attrs.exclude(bboxes="").exists())

    def test_import_classifications_wrong_id(self):
        invalid_data = self._observations.copy()
        invalid_data[0]["_id"] = -1
        params = self.get_params(observations=invalid_data)
        importer = ClassificationImporter(**params)
        importer.validate_table()
        self.assertTrue(importer.report.valid == False)
        self.assertEqual(importer.report.tasks[0].errors[0].type, "cell-error")

    def test_import_classifications_wrong_species(self):
        invalid_data = self._observations.copy()
        invalid_data[0]["scientificName"] = "Trelemorele ipsum"
        params = self.get_params(observations=invalid_data)
        importer = ClassificationImporter(**params)
        importer.validate_table()
        self.assertTrue(importer.report.valid == False)
        self.assertEqual(importer.report.tasks[0].errors[0].type, "constraint-error")

    def test_import_classifications_wrong_observation_type(self):
        invalid_data = self._observations.copy()
        invalid_data[0]["observationType"] = "Trelemorele ipsum"
        params = self.get_params(observations=invalid_data)
        importer = ClassificationImporter(**params)
        importer.validate_table()
        self.assertTrue(importer.report.valid == False)
        self.assertEqual(importer.report.tasks[0].errors[0].type, "constraint-error")

    def test_celery_import_classifications(self):
        self.classification.delete(clear=True)
        params = self.get_params()
        celery_import_classifications(**params)
        classification = Classification.objects.get(pk=self.classification.pk)
        self.assertTrue(classification.status)
        self.assertEqual(classification.approved_by, self._owner)
        self.assertTrue(
            classification.dynamic_attrs.filter(species=self._species).exists()
        )
        self.assertTrue(
            classification.dynamic_attrs.filter(observation_type="animal").exists()
        )

    def test_import_ai_classifications_approve_and_overwrite_attrs(self):
        self.classification.delete(clear=True)
        self.assertFalse(self.classification.dynamic_attrs.exists())
        params = self.get_params(
            approve=True, import_expert=False, import_ai=True, overwrite_attrs=True
        )
        params["ai_provider"] = self.create_ai_provider()
        importer = ClassificationImporter(**params)
        importer.validate_table()
        self.assertTrue(importer.report.valid is True)
        importer.import_classifications()

        classification = Classification.objects.get(pk=self.classification.pk)
        self.assertTrue(classification.status_ai)
        self.assertFalse(classification.status)
        self.assertEqual(classification.approved_ai_by, self._owner)
        self.assertTrue(
            AIClassification.objects.filter(classification=classification).exists()
        )
        self.assertTrue(
            classification.dynamic_attrs.filter(observation_type="animal").exists()
        )

    def test_import_ai_classifications_approve_without_overwriting_attrs(self):
        self.classification.delete(clear=True)
        self.assertFalse(self.classification.dynamic_attrs.exists())
        params = self.get_params(
            approve=True, import_expert=False, import_ai=True, overwrite_attrs=False
        )
        params["ai_provider"] = self.create_ai_provider()
        importer = ClassificationImporter(**params)
        importer.validate_table()
        self.assertTrue(importer.report.valid is True)
        importer.import_classifications()

        classification = Classification.objects.get(pk=self.classification.pk)
        self.assertTrue(classification.status_ai)
        self.assertFalse(classification.status)
        self.assertEqual(classification.approved_ai_by, self._owner)
        self.assertTrue(
            AIClassification.objects.filter(classification=classification).exists()
        )
        self.assertFalse(self.classification.dynamic_attrs.exists())

    def test_importing_both_expert_and_ai_classifications(self):
        """
        When importing from both sources, dynamic attributes from expert classification should be saved.
        In the example data, expert classification has sex=female and ai has sex=male
        """
        self.classification.delete(clear=True)
        self.assertFalse(self.classification.dynamic_attrs.exists())
        params = self.get_params(
            approve=True, import_expert=True, import_ai=True, overwrite_attrs=True
        )
        params["ai_provider"] = self.create_ai_provider()
        importer = ClassificationImporter(**params)
        importer.validate_table()
        self.assertTrue(importer.report.valid is True)
        importer.import_classifications()

        classification = Classification.objects.get(pk=self.classification.pk)
        self.assertTrue(classification.status_ai)
        self.assertTrue(classification.status)
        self.assertEqual(classification.approved_by, self._owner)
        self.assertEqual(classification.approved_ai_by, self._owner)
        self.assertTrue(
            AIClassification.objects.filter(classification=classification).exists()
        )
        self.assertEqual(classification.dynamic_attrs.count(), 1)
        self.assertEqual(
            classification.dynamic_attrs.filter(observation_type="animal").first().sex,
            "female",
        )

    @patch(
        "trapper.apps.media_classification.tasks.classification_import.ClassificationImporter.import_ai_classifications"
    )
    @patch(
        "trapper.apps.media_classification.tasks.classification_import.ClassificationImporter.import_expert_classifications"
    )
    def test_calling_both_imports(
        self, import_expert_classifications_mock, import_ai_classifications_mock
    ):
        params = self.get_params(
            approve=True, import_expert=True, import_ai=True, overwrite_attrs=False
        )
        params["ai_provider"] = self.create_ai_provider()
        importer = ClassificationImporter(**params)
        importer.import_classifications()
        import_ai_classifications_mock.assert_called_once()
        import_expert_classifications_mock.assert_called_once()

    @patch(
        "trapper.apps.media_classification.tasks.classification_import.ClassificationImporter.import_ai_classifications"
    )
    @patch(
        "trapper.apps.media_classification.tasks.classification_import.ClassificationImporter.import_expert_classifications"
    )
    def test_calling_only_expert_import(
        self, import_expert_classifications_mock, import_ai_classifications_mock
    ):
        params = self.get_params(
            approve=True, import_expert=True, import_ai=False, overwrite_attrs=False
        )
        importer = ClassificationImporter(**params)
        importer.import_classifications()
        import_expert_classifications_mock.assert_called_once()
        import_ai_classifications_mock.assert_not_called()

    @patch(
        "trapper.apps.media_classification.tasks.classification_import.ClassificationImporter.import_ai_classifications"
    )
    @patch(
        "trapper.apps.media_classification.tasks.classification_import.ClassificationImporter.import_expert_classifications"
    )
    def test_calling_only_ai_import(
        self, import_expert_classifications_mock, import_ai_classifications_mock
    ):
        params = self.get_params(
            approve=True, import_expert=False, import_ai=True, overwrite_attrs=False
        )
        importer = ClassificationImporter(**params)
        importer.import_classifications()
        import_ai_classifications_mock.assert_called_once()
        import_expert_classifications_mock.assert_not_called()


class SequencesBuilderTestCase(BaseMediaClassificationTestCase):
    def setUp(self):
        super().setUp()

        self._owner = UserFactory()
        self._call_helper(self._owner)
        self._data = {
            "time_interval": 10,
            "deployments": "",
            "overwrite": False,
            "collections_pks": self.collection,
            "project_collections": [
                self.create_classification_project_collection(
                    self.classification_project,
                    self.create_research_project_collection(
                        self.research_project, self.collection
                    ),
                )
            ],
        }

        self.resource1 = ResourceFactory(owner=self._owner)
        self.resource2 = ResourceFactory(owner=self._owner)

        self._sequence_builder = SequencesBuilder(self._data, self._owner)

    def test_process_data(self):
        self._sequence_builder.process_data()
        self.assertEqual(
            self._sequence_builder.log,
            [
                "You have successfully built sequences for 1 out of "
                "1 classification project collections."
            ],
        )

    def test_group_resources(self):
        self._sequence_builder.group_resources(
            [self.resource1, self.resource2], datetime.timedelta(minutes=10)
        )

    def test_create_sequences(self):
        user1 = UserFactory()
        research_project = ResearchProjectFactory(owner=user1)

        location1 = LocationFactory(owner=user1, research_project=research_project)

        deployment1 = DeploymentFactory(
            owner=user1,
            research_project=research_project,
            location=location1,
        )

        resource1 = ResourceFactory(owner=user1, deployment=deployment1)

        collection1 = CollectionFactory(owner=user1)

        collection1.resources.set((resource1,))

        collection = CollectionFactory(owner=user1, status=CollectionStatus.PRIVATE)
        collection.resources.set(Resource.objects.all())
        classificator = ClassificatorFactory(owner=user1)

        classification_project = ClassificationProjectFactory(
            research_project=research_project, owner=user1, classificator=classificator
        )

        research_project.collections.set([collection])

        collection = ResearchProjectCollection.objects.filter(
            project=classification_project.research_project.pk
        )[0]
        classification_project_collection = ClassificationProjectCollectionFactory(
            project=classification_project, collection=collection
        )

        self._sequence_builder.create_sequences(
            classification_project_collection, [[self.resource1, self.resource2]]
        )

    def test_celery_build_sequences(self):
        log_result = celery_build_sequences(self._data, self._owner)
        self.assertEqual(
            log_result,
            "You have successfully built sequences for 1 out of "
            "1 classification project collections.",
        )


class ResourceFeedbackTestCase(BaseMediaClassificationTestCase):
    def setUp(self):
        super().setUp()
        self._owner = UserFactory()
        self._call_helper(self._owner)
        self._species = self.create_species()
        self.classificator.species.add(self._species)
        # set species & custom attributes
        dattrs = self.classification.dynamic_attrs.all()
        for dattr in dattrs:
            dattr.species = self._species
            dattr.attrs["test_attr"] = "test_attr_value"
            dattr.save()
        self._params = {
            "user": self._owner,
            "classifications": [self.classification],
            "tags": ["custom_test_attr"],
        }

    def test_create_tags(self):
        feedback = ResourceFeedback(**self._params)
        feedback.run()
        resource = Resource.objects.get(pk=self.resource.pk)
        self.assertEqual(resource.data["pk"], self.classification.pk)
        self.assertEqual(resource.data["observations"][0]["observation_type"], "animal")
        self.assertEqual(
            resource.data["observations"][0]["species_id"], self._species.pk
        )
        self.assertEqual(
            resource.data["observations"][0]["attrs"], {"test_attr": "test_attr_value"}
        )
        self.assertTrue(resource.tags.filter(name="test_attr:test_attr_value").exists())

    def test_celery_create_tags(self):
        celery_resource_feedback(**self._params)
        resource = Resource.objects.get(pk=self.resource.pk)
        self.assertEqual(resource.data["pk"], self.classification.pk)


class PublishDataPackageTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.user1 = UserFactory()

        data_package = Mock()
        data_package.package = Mock()
        data_package.package.path = "test"

        cls.data = {
            "api_token": "token",
            "host_url": "https://url.com",
            "secure_connection": False,
            "taxonomic_coverage": False,
            "data_package": data_package,
            "data_hub": "Dataverse",
            "dataverse": "dataverse",
            "container": "container_name",
        }

        cls.publish_data_package = PublishDataPackage(cls.data, cls.user1)

    @patch("trapper.apps.media_classification.tasks.data_packages.requests")
    @patch("trapper.apps.media_classification.tasks.data_packages.mapper")
    @patch("trapper.apps.media_classification.tasks.data_packages.zipfile")
    @patch(
        "trapper.apps.media_classification.tasks.data_packages.open", return_value=True
    )
    def test_run(self, mock_open, mock_zipfile, mock_mapper, mock_requests):
        class Request:
            status_code = 201
            data = {"data": {"id": 1}}

            def json(self):
                return self.data

        dumped_value = json.dumps({"abc": "abc_value"})
        mock_zipped = Mock()
        mock_zipped.read = Mock(return_value=dumped_value)
        mock_zipfile.ZipFile = Mock(return_value=mock_zipped)
        mock_mapper.package2dataverse = Mock(
            return_value=(json.dumps({"abc": "cds"}), [])
        )
        mock_requests.post = Mock(return_value=Request())

        self.publish_data_package.run()


class ResultsDataPackageGeneratorTestCase(BaseMediaClassificationTestCase):
    def setUp(self):
        super().setUp()
        self._owner = UserFactory()
        self._call_helper(self._owner)
        self._data = {}
        self.host = "http://localhost"
        self._rdp_generator = ResultsDataPackageGenerator(
            self._data, self._owner, self.classification_project, self.host
        )
        self.admin = UserFactory(email="test@os-conservation.org")
        self.classification_project.classification_project_roles.create(
            user=self.admin, name=ClassificationProjectRoleLevels.ADMIN
        )

    def test_run(self):
        self._rdp_generator.run()

    def test_get_media_table(self):
        result = self._rdp_generator.get_media_table()
        self.assertTrue(isinstance(result, pandas.DataFrame))

    def test_get_deployments_table(self):
        result = self._rdp_generator.get_deployments_table()
        self.assertTrue(isinstance(result, pandas.DataFrame))

    def test_get_results_table(self):
        result = self._rdp_generator.get_results_table()
        self.assertTrue(isinstance(result, pandas.DataFrame))

    def test_get_contributors(self):
        contributors = self._rdp_generator.get_contributors()
        self.assertEqual(
            contributors,
            [
                {
                    "title": self.admin.get_full_name(),
                    "email": self.admin.email,
                    "role": "principalInvestigator",
                    "organization": self.admin.userprofile.institution or "",
                }
            ],
        )

    def test_get_package_metadata(self):
        metadata = self._rdp_generator.get_package_metadata()
        exp = {
            "contributors": [
                {
                    "title": self.admin.get_full_name(),
                    "email": self.admin.email,
                    "role": "principalInvestigator",
                    "organization": self.admin.userprofile.institution or "",
                }
            ],
            "coordinatePrecision": 0.0001,
            "description": "",
            "homepage": self.host,
            "id": f"{self._rdp_generator.package_id}",
            "keywords": [""],
            "licenses": [
                {"name": "private", "scope": "data"},
                {"name": "private", "scope": "media"},
            ],
            "name": f"{self._rdp_generator.package_name}",
            "project": {
                "acronym": f"{self.research_project.acronym}",
                "captureMethod": ["motionDetection"],
                "description": "",
                "id": f"{self._rdp_generator.project_id}",
                "individualAnimals": False,
                "observationLevel": ["media"],
                "path": "",
                "samplingDesign": "simpleRandom",
                "title": f"{self.research_project.name}",
            },
            "sources": [{"path": self.host, "title": "Trapper"}],
            "title": f"{self._rdp_generator.package_title}",
            "version": "1.0",
            "relatedIdentifiers": [],
        }
        self.assertDictEqual(
            metadata,
            exp,
        )

    def test_get_custom_attrs(self):
        self.classificator.custom_attrs = {
            "test-attr": {
                "field_type": "string",
                "format": "default",
                "description": "test-desc",
                "required": True,
                "values": "a,b,c",
            }
        }
        custom_attrs = (
            self._rdp_generator.project.classificator.get_custom_attrs_schema()
        )
        self.assertEqual(
            custom_attrs,
            [
                {
                    "constraints": {"enum": ["a", "b", "c"], "required": True},
                    "description": "test-desc",
                    "format": "default",
                    "name": "test-attr",
                    "type": None,
                }
            ],
        )


class MediaClassificationTasksTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.user1 = UserFactory()
        cls.user2 = UserFactory()
        cls.user3 = UserFactory()
        cls.user4 = UserFactory()

        cls.research_project = ResearchProjectFactory(owner=cls.user1)
        cls.location = LocationFactory(
            owner=cls.user1, research_project=cls.research_project
        )
        cls.deployment = DeploymentFactory(
            owner=cls.user1,
            location=cls.location,
            research_project=cls.research_project,
        )
        cls.resource1 = ResourceFactory(
            owner=cls.user1, deployment=cls.deployment, status=ResourceStatus.PRIVATE
        )

        ResourceFactory(
            owner=cls.user1, deployment=cls.deployment, status=ResourceStatus.PRIVATE
        )

        cls.collection = CollectionFactory(
            owner=cls.user1, status=CollectionStatus.PRIVATE
        )
        cls.collection.resources.set(Resource.objects.all())
        cls.classificator = ClassificatorFactory(owner=cls.user1)

        cls.classification_project = ClassificationProjectFactory(
            research_project=cls.research_project,
            owner=cls.user1,
            classificator=cls.classificator,
        )


class ApproveUserClassificationsTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.user1 = UserFactory()
        cls.user2 = UserFactory()
        cls.user3 = UserFactory()
        cls.user4 = UserFactory()

        cls.research_project = ResearchProjectFactory(owner=cls.user1)
        cls.location = LocationFactory(
            owner=cls.user1, research_project=cls.research_project
        )
        cls.deployment = DeploymentFactory(
            owner=cls.user1,
            location=cls.location,
            research_project=cls.research_project,
        )
        cls.resource1 = ResourceFactory(
            owner=cls.user1, deployment=cls.deployment, status=ResourceStatus.PRIVATE
        )

        ResourceFactory(
            owner=cls.user1, deployment=cls.deployment, status=ResourceStatus.PRIVATE
        )

        cls.collection = CollectionFactory(
            owner=cls.user1, status=CollectionStatus.PRIVATE
        )
        cls.collection.resources.set(Resource.objects.all())
        cls.classificator = ClassificatorFactory(owner=cls.user1)

        cls.classification_project = ClassificationProjectFactory(
            research_project=cls.research_project,
            owner=cls.user1,
            classificator=cls.classificator,
        )

        resources_array = Resource.objects.all()
        cls.research_project.collections.set([cls.collection])

        collection = ResearchProjectCollection.objects.filter(
            project=cls.classification_project.research_project.pk
        )[0]
        cls.classification_project_collection = ClassificationProjectCollectionFactory(
            project=cls.classification_project, collection=collection
        )

        for resource in resources_array:
            ClassificationFactory(
                resource=resource,
                owner=cls.user1,
                project=cls.classification_project,
                collection=cls.classification_project_collection,
            )

        cls.resource = ResourceFactory(
            owner=cls.user1, deployment=cls.deployment, status=ResourceStatus.PRIVATE
        )
        cls.classification = ClassificationFactory(
            resource=cls.resource,
            owner=cls.user1,
            project=cls.classification_project,
            collection=cls.classification_project_collection,
        )

        cls.classification_project_role1 = ClassificationProjectRoleFactory(
            name=ClassificationProjectRoleLevels.ADMIN,
            classification_project=cls.classification_project,
            user=cls.user2,
        )
        ClassificationProjectRoleFactory(
            name=ClassificationProjectRoleLevels.EXPERT,
            classification_project=cls.classification_project,
            user=cls.user3,
        )
        cls.user_classification1 = UserClassificationFactory(
            owner=cls.user2, classification=cls.classification
        )
        cls.user_classification2 = UserClassificationFactory(
            owner=cls.user2, classification=cls.classification
        )

    def test_run(self):
        approve_user_classifications = ApproveUserClassifications(
            self.user1,
            self.classification_project,
            [self.user_classification1.pk, self.user_classification2.pk],
        )
        approve_user_classifications.run()
