from django.conf import settings
from django.contrib.postgres.search import SearchVector
from django.utils.translation import gettext_lazy as _

from trapper.apps.media_classification.models import Classification
from trapper.apps.media_classification.serializers import ClassificationDataSerializer
from trapper.apps.storage.models import Resource


class ResourceFeedback:
    """
    TODO: docstrings
    """

    def __init__(self, user, classifications, tags=None, base_classification=False):
        self.user = user
        self.classifications = classifications
        if tags:
            self.tags = [k.split("custom_")[1] for k in tags]
        else:
            self.tags = None
        self.base_classification = base_classification

    def run(self):
        resources = []
        for classification in self.classifications:
            if self.base_classification:
                classification.base_classification = True
            resource = classification.resource
            data = ClassificationDataSerializer(classification, many=False).data
            resource.data = data
            # update search vector gin index
            resource.search_data_vector = SearchVector("data")
            resources.append(resource)
            if self.tags:
                resource_tags = []
                for observation in data["observations"]:
                    for tag in self.tags:
                        val = observation.get("attrs", {}).get(tag)
                        if val is not None:
                            resource_tags.append(f"{tag.lower()}:{val}")
                resource.tags.add(*set(resource_tags))
        Resource.objects.bulk_update(
            resources, fields=["data"], batch_size=settings.HEAVY_BULK_UPDATE_BATCH_SIZE
        )
        # trigger bulk_update once more to update gin index
        Resource.objects.bulk_update(
            resources,
            fields=["search_data_vector"],
            batch_size=settings.BULK_BATCH_SIZE,
        )
        if self.base_classification:
            Classification.objects.bulk_update(
                self.classifications,
                fields=["base_classification"],
                batch_size=settings.BULK_BATCH_SIZE,
            )
        msg = _(
            f"You have successfully processed {len(self.classifications)} classifications."
        )
        return msg
