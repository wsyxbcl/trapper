# -*- coding: utf-8 -*-
"""
Media classification asynchronous celery tasks.
"""

from celery import shared_task

from trapper.apps.media_classification.tasks.blur_humans import (
    ProjectCollectionBlurHumans,
)
from trapper.apps.media_classification.tasks.classification_approvals import (
    ApproveUserClassifications,
    ApproveAIClassifications,
)
from trapper.apps.media_classification.tasks.classification_import import (
    ClassificationImporter,
)
from trapper.apps.media_classification.tasks.copy_bboxes import CopyBboxesFromAI
from trapper.apps.media_classification.tasks.data_packages import (
    ResultsDataPackageGenerator,
    PublishDataPackage,
)
from trapper.apps.media_classification.tasks.resource_feedback import ResourceFeedback
from trapper.apps.media_classification.tasks.sequence_builder import SequencesBuilder
from trapper.apps.media_classification.tasks.standard_attribute_import import (
    ImportStandardAttributes,
)


@shared_task(serializer="pickle")
def celery_import_classifications(**kwargs):
    """
    Celery task that imports classifications from a csv file into given
    classification project.
    """
    importer = ClassificationImporter(**kwargs)
    return importer.import_classifications()


@shared_task(time_limit=3600, serializer="pickle")
def celery_build_sequences(data, user):
    """
    Celery task to automatically build sequences of resources.
    """
    importer = SequencesBuilder(data, user)
    log = importer.run_with_logger()
    return log


@shared_task(serializer="pickle")
def celery_approve_user_classifications(**kwargs):
    """
    TODO: docstrings
    """
    approve = ApproveUserClassifications(**kwargs)
    return approve.run()


@shared_task(serializer="pickle")
def celery_approve_ai_classifications(**kwargs):
    """
    TODO: docstrings
    """
    approve = ApproveAIClassifications(**kwargs)
    return approve.run()


@shared_task(serializer="pickle")
def celery_copy_bboxes_from_ai(**kwargs):
    copy_bboxes_task = CopyBboxesFromAI(**kwargs)
    return copy_bboxes_task.run()


@shared_task(serializer="pickle")
def celery_resource_feedback(**kwargs):
    """
    TODO: docstrings
    """
    feedback = ResourceFeedback(**kwargs)
    return feedback.run()


@shared_task(serializer="pickle")
def celery_results_to_data_package(data, user, project, host):
    """
    Celery task that create a data package (archive) containing the
    results of selected classification project.
    """
    gen = ResultsDataPackageGenerator(data, user, project, host=host)
    log = gen.run()
    return log


@shared_task(serializer="pickle")
def celery_publish_data_package(**kwargs):
    """
    TODO: docstrings
    """
    publish = PublishDataPackage(**kwargs)
    return publish.run()


@shared_task(serializer="pickle")
def celery_import_standard_attributes(**kwargs):
    """
    TODO: docstrings
    """
    import_attrs = ImportStandardAttributes(**kwargs)
    return import_attrs.run()


@shared_task(serializer="json")
def celery_update_remote_classification_task_status(classification_job_id):
    from trapper.apps.media_classification.ai_providers.async_tasks import (
        update_remote_classification_task_status,
    )

    update_remote_classification_task_status(classification_job_id)


@shared_task(serializer="pickle")
def celery_blur_humans(**kwargs):
    blur_humans_task = ProjectCollectionBlurHumans(**kwargs)
    return blur_humans_task.run()
