import itertools

from django.conf import settings
from django.utils.translation import gettext_lazy as _

from trapper.apps.media_classification.models import (
    UserClassification,
    Classification,
    ClassificationDynamicAttrs,
    ClassificationProject,
    AIClassification,
)


class ApproveUserClassifications:
    """
    Class for asynchronous task that approves specified User Classifications
    """

    def __init__(self, user, project, user_classifications_pks):
        self.user = user
        self.project = project
        self.user_classifications = (
            UserClassification.objects.filter(
                classification__project=self.project, pk__in=user_classifications_pks
            )
            .select_related("classification")
            .prefetch_related("dynamic_attrs")
        )

    def run(self):
        classificator = self.project.classificator
        classifications = {}
        dynamic_attrs = {}
        for uc in self.user_classifications:
            c = uc.classification

            if c.pk not in classifications:
                classifications[c.pk] = c
            # approval logic for multiple user classifications of same classification
            else:
                # already approved classification
                ac = classifications[c.pk]
                if ac.approved_source.updated_at < uc.updated_at:
                    # this user classification is newer than already approved one
                    classifications[c.pk] = c
                else:
                    continue

            c_dattrs = c.approve_user_classification(
                uc, classificator=classificator, user=self.user, commit=False
            )
            dynamic_attrs[c.pk] = c_dattrs

        classifications = list(classifications.values())
        dynamic_attrs = list(itertools.chain(*dynamic_attrs.values()))

        update_fields = classificator.active_standard_attrs("STATIC")
        update_fields.extend(
            [
                "has_initial_data",
                "has_bboxes",
                "static_attrs",
                "status",
                "approved_by",
                "approved_at",
                "approved_source",
                "updated_at",
                "updated_by",
            ]
        )
        # bulk update Classification objects
        Classification.objects.bulk_update(
            classifications,
            fields=update_fields,
            batch_size=settings.HEAVY_BULK_UPDATE_BATCH_SIZE,
        )
        # bulk delete ClassificationDynamicAttrs objects
        ClassificationDynamicAttrs.objects.filter(
            classification__in=classifications
        ).delete()
        # bulk create ClassificationDynamicAttrs objects
        ClassificationDynamicAttrs.objects.bulk_create(
            dynamic_attrs, batch_size=settings.BULK_BATCH_SIZE
        )
        msg = _(
            f"You have successfully approved {len(self.user_classifications)} user classifications."
        )
        return msg


class ApproveAIClassifications:
    """
    Class that implements asynchronous task that copies data from AI classification to Classification object
    and optionally marks it as Approved
    """

    def __init__(
        self,
        user,
        project_id,
        ai_classification_pks,
        fields_to_copy,
        minimum_confidence,
        mark_as_approved,
        overwrite_attrs,
    ):
        self.user = user
        self.project = ClassificationProject.objects.get(pk=project_id)
        self.ai_classification_pks = ai_classification_pks
        self.mark_as_approved = mark_as_approved
        self.overwrite_attrs = overwrite_attrs
        self.minimum_confidence = minimum_confidence
        self.fields_to_copy = fields_to_copy + ["classification_confidence"]
        self.ai_classifications = (
            AIClassification.objects.filter(
                classification__project=self.project,
                pk__in=self.ai_classification_pks,
            )
            .select_related("classification")
            .prefetch_related("dynamic_attrs")
        )

    def run(self):
        classificator = self.project.classificator
        classifications = []
        dynamic_attrs = []

        for ai_classification in self.ai_classifications:
            classification = ai_classification.classification
            c_dattrs = classification.approve_ai_classification(
                ai_classification=ai_classification,
                fields_to_copy=self.fields_to_copy,
                minimum_confidence=self.minimum_confidence,
                mark_as_approved=self.mark_as_approved,
                overwrite_attrs=self.overwrite_attrs,
                classificator=classificator,
                user=self.user,
                commit=False,
            )
            classifications.append(classification)
            if self.overwrite_attrs:
                dynamic_attrs.extend(c_dattrs)

        if self.overwrite_attrs:
            update_fields = classificator.active_standard_attrs("STATIC")
            update_fields.extend(
                [
                    "has_initial_data",
                    "has_bboxes",
                    "static_attrs",
                    "status_ai",
                    "approved_ai_by",
                    "approved_ai_at",
                    "approved_source_ai",
                    "updated_at",
                    "updated_by",
                ]
            )
        else:
            update_fields = [
                "status_ai",
                "approved_ai_by",
                "approved_ai_at",
                "approved_source_ai",
                "updated_at",
                "updated_by",
            ]

        # bulk update Classification objects
        Classification.objects.bulk_update(
            classifications,
            fields=update_fields,
            batch_size=settings.HEAVY_BULK_UPDATE_BATCH_SIZE,
        )

        if self.overwrite_attrs:
            # bulk delete ClassificationDynamicAttrs objects
            ClassificationDynamicAttrs.objects.filter(
                classification__in=classifications
            ).delete()
            # bulk create ClassificationDynamicAttrs objects
            ClassificationDynamicAttrs.objects.bulk_create(
                dynamic_attrs, batch_size=settings.BULK_BATCH_SIZE
            )

        msg = _(
            f"You have successfully approved {len(self.ai_classifications)} AI classifications."
        )
        return msg
