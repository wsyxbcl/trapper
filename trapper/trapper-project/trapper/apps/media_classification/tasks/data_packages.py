import json
import os
import time
import uuid
import zipfile
from shutil import rmtree

import pandas
import requests
from camtrap_package import mapper
from camtrap_package.package import CamTrapPackage
from django.conf import settings
from django.core.files.base import ContentFile
from django.db.models import Q, Max
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

from trapper.apps.media_classification.serializers import (
    prepare_media_table,
    prepare_results_table,
)
from trapper.apps.accounts.models import UserDataPackage
from trapper.apps.accounts.taxonomy import PackageType
from trapper.apps.geomap.models import Deployment
from trapper.apps.geomap.serializers import DeploymentTableSerializer
from trapper.apps.media_classification.taxonomy import ClassificationProjectRoleLevels


class ResultsDataPackageGenerator:
    """
    This code is used to generate a Camtrap DP package from classification project results.
    https://tdwg.github.io/camtrap-dp
    https://gitlab.com/oscf/camtrap-package
    """

    def __init__(self, data, user, project, host=None):
        self.data = data
        self.user = user
        self.project = project
        self.host = host
        self.schema_version = settings.CAMTRAP_PACKAGE_SCHEMAS_VERSION
        self.package = None

        # prepare querysets for serializers
        self.classifications = (
            self.project.classifications.all()
            .select_related("resource__deployment__location", "sequence")
            .prefetch_related("dynamic_attrs")
        )
        if self.data.get("approved_only", True):
            self.classifications = self.classifications.filter(
                Q(status=True) | Q(status_ai=True)
            )
        if self.data.get("exclude_blank", False):
            self.classifications = self.classifications.exclude(
                dynamic_attrs__observation_type="blank"
            )

        # exclude classifications of resources without assigned deployment or
        # assigned to deployment without start and end dates provided
        self.classifications = (
            self.classifications.exclude(resource__deployment_id__isnull=True)
            .exclude(resource__deployment__start_date__isnull=True)
            .exclude(resource__deployment__end_date__isnull=True)
        )

        all_deployments = self.data.get("all_dep", False)
        filter_deployments = self.data.get("filter_dep")

        deployment_pks = set(
            self.classifications.values_list("resource__deployment__pk", flat=True)
        )

        # include all deployments from the project, some of them may not have any records
        if all_deployments:
            project_deployments_pks = Deployment.objects.filter(
                research_project=self.project.research_project
            ).values_list("pk", flat=True)
            deployment_pks = set(list(deployment_pks) + list(project_deployments_pks))

        self.deployments = Deployment.objects.filter(pk__in=deployment_pks)

        if filter_deployments:
            self.deployments = self.deployments.filter(
                deployment_id__icontains=filter_deployments
            )
            self.classifications = self.classifications.filter(
                resource__deployment__in=self.deployments
            )

        # exclude deployments without start and end dates provided
        self.deployments = self.deployments.exclude(start_date__isnull=True).exclude(
            end_date__isnull=True
        )

        self.timestamp = now()

        self.package_version = self.data.get("version", "1.0")
        name_str = self.data.get("name", f"results-{self.project.pk}")
        self.package_name = "{name}-{version}".format(
            name=name_str.lower().replace(" ", "-"), version=self.package_version
        )
        self.package_id = str(uuid.uuid4())

        self.trapper_format = self.data.get("export_format", "camtrapdp") == "trapper"
        include_events = self.data.get("include_events", True)
        classificator = self.project.classificator
        self.individual_animals = classificator.individual_id
        self.include_events = include_events and classificator.count_new
        self.include_ids = self.data.get("include_ids", True)
        self.project_id = str(uuid.uuid4())
        self.package_title = self.data.get("title", self.project.research_project.name)

    def get_media_table(self):
        trapper_url_token = self.data.get("trapper_url_token", False)
        private_human = self.data.get("private_human", True)
        private_vehicle = self.data.get("private_vehicle", True)
        private_species = self.data.get("private_species", None)
        df = prepare_media_table(
            self.classifications,
            trapper_url_token=trapper_url_token,
            trapper_url=True,
            host=self.host,
            private_human=private_human,
            private_species=private_species,
            private_vehicle=private_vehicle,
        )
        return df

    def get_deployments_table(self):
        data = DeploymentTableSerializer(self.deployments, many=True).data
        df = pandas.DataFrame(data)
        return df

    def get_results_table(self):
        df = prepare_results_table(
            self.classifications,
            include_events=self.include_events,
            trapper_format=self.trapper_format,
        )
        return df

    def get_contributors(self):
        # TODO: add other contributors which are not project admins
        authors = self.project.classification_project_roles.filter(
            name=ClassificationProjectRoleLevels.ADMIN
        )
        contributors = []
        for a in authors:
            contributors.append(
                {
                    "title": a.user.get_full_name() or a.user.username,
                    "email": a.user.email,
                    "role": "principalInvestigator",
                    "organization": a.user.userprofile.institution or "",
                }
            )
        return contributors

    def get_package_metadata(self):
        rproject = self.project.research_project
        package_keywords = self.data.get(
            "keywords", list(rproject.keywords.values_list("name", flat=True))
        )
        package_licences = self.data.get("licences", [])
        package_description = self.data.get("description", "")
        observation_level = ["media"]

        # calculate coordination precision by first finding the max value from all project deployments
        # then calculate degrees from meters
        max_coord_uncertainty = self.deployments.aggregate(
            Max("location__coordinate_uncertainty")
        )["location__coordinate_uncertainty__max"]
        coordinate_precision = max_coord_uncertainty / 100000

        if self.include_events:
            observation_level.append("event")

        licenses = [
            {"name": k.name, "path": k.url, "title": k.title, "scope": k.scope}
            for k in package_licences
        ]
        licenses_scope = set([k.scope for k in package_licences])
        if "data" not in licenses_scope:
            licenses.append({"name": "private", "scope": "data"})
        if "media" not in licenses_scope:
            licenses.append({"name": "private", "scope": "media"})

        metadata = {
            # lower case characters with `.`, `_`, `-` and `/` are allowed.
            "name": self.package_name,
            "id": self.package_id,
            "title": self.package_title,
            "contributors": self.get_contributors(),
            "description": package_description,
            "version": self.package_version,
            "keywords": package_keywords or [""],
            "homepage": self.host or "",
            "sources": [{"title": "Trapper", "path": self.host}],
            "licenses": licenses,
            "project": {
                "id": self.project_id,
                "title": rproject.name,
                "acronym": rproject.acronym,
                "description": rproject.description or "",
                "path": "",
                "samplingDesign": rproject.get_sampling_design_display(),
                "captureMethod": rproject.get_sensor_method_array(),
                "individualAnimals": self.individual_animals,
                "observationLevel": observation_level,
            },
            "coordinatePrecision": coordinate_precision,
            "relatedIdentifiers": [],
        }

        return metadata

    def run(self, return_package=False):
        if self.classifications.count() == 0:
            msg = _(
                "There is no single valid & already approved classification to export."
            )
            return msg
        # get tables
        dep_df = self.get_deployments_table()
        med_df = self.get_media_table()
        res_df = self.get_results_table()

        # drop _id columns if necessary
        if not self.trapper_format and not self.include_ids:
            dep_df.drop(columns=["_id"], inplace=True)
            med_df.drop(columns=["_id"], inplace=True)
            res_df.drop(columns=["_id"], inplace=True)

        # initialize data package
        metadata = self.get_package_metadata()

        self.package = CamTrapPackage(
            metadata=metadata,
            media=med_df,
            deployments=dep_df,
            observations=res_df,
            schema_version=self.schema_version,
        )

        if return_package:
            return self.package

        # no validation as all required package-level metadata are provided
        # and all tables are already in proper formats
        self.package.valid_package = True

        # save package
        self.package.save(
            make_archive=True,
            generate_uuid4=False,
        )
        dpack_name = self.package.package.name + ".zip"
        dpack_tmp_path = os.path.join(self.package.base_path, dpack_name)
        user_data_package = UserDataPackage(
            user=self.user,
            date_created=self.timestamp,
            package_type=PackageType.CLASSIFICATION_RESULTS,
        )
        with open(dpack_tmp_path, "rb") as dpack:
            with ContentFile(dpack.read()) as dpack_content:
                user_data_package.package.save(dpack_name, dpack_content)
        user_data_package.uuid4 = self.package_id
        user_data_package.save()

        # remove temporary files
        rmtree(self.package.base_path)
        msg = _(
            f"The requested data package: {dpack_name} has been successfully generated!"
        )
        return msg


class PublishDataPackage:
    """
    TODO: docstrings
    """

    def __init__(self, data, user):
        self.user = user
        self.data = data

    def run(self):
        # connection settings
        token = self.data["api_token"]
        host = self.data["host_url"]
        secure = self.data["secure_connection"]
        # get zipped data package
        data_package = self.data["data_package"]
        zipped = zipfile.ZipFile(open(data_package.package.path, "rb"))
        # process package descriptor
        descriptor = json.loads(zipped.read("datapackage.json"))

        if self.data["data_hub"] == "Dataverse":
            dv_meta, dfiles = mapper.package2dataverse(descriptor, taxonomic_cov=False)
            dv_meta_json = json.dumps(dv_meta)
            # create dataset
            dataverse = self.data["container"]
            url = f"{host}/api/dataverses/{dataverse}/datasets"
            headers = {"X-Dataverse-key": token, "Content-Type": "application/json"}
            resp = requests.post(url, data=dv_meta_json, headers=headers, verify=secure)
            if resp.status_code == 201:
                dataset_data = resp.json()["data"]
                dataset_id = dataset_data["id"]
                # upload files
                url = f"{host}/api/datasets/{dataset_id}/add"
                for fdesc in dfiles:
                    files = {"file": zipped.open(fdesc["filename"])}
                    payload = dict(jsonData=json.dumps(fdesc))
                    resp = requests.post(
                        url,
                        data=payload,
                        files=files,
                        params={"key": token},
                        verify=secure,
                    )
                    time.sleep(2)
            else:
                raise Exception(resp.json()["message"])
            msg = _(
                f"You have successfully published your data package {data_package.filename()} at "
                f"{host} ! Details of the published dataset: {dataset_data}"
            )
            return msg

        elif self.data["data_hub"] == "Zenodo":
            # get package metadata in zenodo-friendly format and files list
            meta, datafiles = mapper.package2zenodo(descriptor)

            params = {"access_token": token}

            # upload files
            for fname in datafiles:
                fcontent = zipped.open(fname)
                r = requests.put(
                    "%s/%s" % (host, fname),
                    data=fcontent,
                    params=params,
                )
                if r.status_code != 200:
                    raise Exception(r.json()["message"])

            # send metadata
            headers = {"Content-Type": "application/json"}
            metadata_url = self.data["metadata_url"]
            r = requests.put(
                metadata_url, params=params, data=json.dumps(meta), headers=headers
            )
            if r.status_code != 200:
                raise Exception(r.json()["message"])

            msg = _(
                "You have successfully sent data package to Zenodo. It can be published manually from your "
                "Zenodo account."
            )
            return msg

        raise NotImplementedError
