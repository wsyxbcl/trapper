import json

import frictionless
import pandas
import requests
from django.conf import settings
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

from trapper.apps.accounts.models import User
from trapper.apps.common.frictionless_checks import DbExistingObjects
from trapper.apps.media_classification.taxonomy import (
    ClassificationSettings,
    ClassificatorSettings,
)
from trapper.apps.media_classification.models import (
    UserClassification,
    Classification,
    UserClassificationDynamicAttrs,
    ClassificationDynamicAttrs,
    AIClassification,
    AIClassificationDynamicAttrs,
)


class ClassificationImporter:
    """
    Used to import classifications from observations.csv file.
    """

    limit_validation_errors = 100
    schema_url = settings.CAMTRAP_PACKAGE_SCHEMAS_OBSERVATIONS
    schema_dtypes_dict = {
        "observationID": str,
        "eventID": str,
        "mediaID": str,
        "_id": str,
    }

    def __init__(
        self,
        data,
        project,
        user,
        approve=True,
        import_bboxes=True,
        import_expert_classifications=True,
        import_ai_classifications=False,
        overwrite_attrs=False,
        ai_provider=None,
    ):
        self.data = data
        self.project = project
        self.user = user
        self.species = self.get_species()
        self.approve = approve
        self.import_bboxes = import_bboxes
        self.import_expert_classifications_flag = import_expert_classifications
        self.import_ai_classifications_flag = import_ai_classifications
        self.overwrite_attrs = overwrite_attrs
        self.ai_provider = ai_provider
        self.schema = None
        self.timestamp = now()
        self.report = None
        self.provided_columns = list(self.data.columns)
        self.required_columns = list(ClassificationSettings.IMPORT_REQUIRED_COLUMNS)

        # first quickly check if all required columns are provided
        if not set(self.required_columns).issubset(set(self.provided_columns)):
            raise ValueError(
                "There are missing columns that are required:",
                self.provided_columns,
                self.required_columns,
            )

        self.classification_pks = list(
            self.project.classifications.values_list("pk", flat=True)
        )

        # add columns with custom attributes
        if "observationTags" in self.provided_columns:
            self.add_custom_attrs_columns()

        # modify Camtrap DP observations table schema to match Trapper internal import/export format
        self.new_schema = self.get_schema()

        # only use fields specified in the new schema
        schema_fields = [f.name for f in self.new_schema.fields]
        data_fields = [f for f in schema_fields if f in self.provided_columns]

        # fill nans and assign proper types to IDs columns for further frictionless validation
        self.data = self.data[data_fields].fillna("")
        dtypes_dict = {
            k: v for k, v in self.schema_dtypes_dict.items() if k in data_fields
        }
        self.data = self.data.astype(dtypes_dict)

        self.standard_attrs_S = ClassificatorSettings.STANDARD_ATTRS_STATIC
        self.standard_attrs_D = ClassificatorSettings.STANDARD_ATTRS_DYNAMIC
        self.attrs_to_columns = ClassificationSettings.IMPORT_ATTRS_MAP

        custom_attrs = self.project.classificator.custom_attrs
        self.custom_attrs_S = [
            k for k in custom_attrs if custom_attrs[k]["target"] == "S"
        ]
        self.custom_attrs_D = [
            k for k in custom_attrs if custom_attrs[k]["target"] == "D"
        ]

        self.all_attrs_S = self.standard_attrs_S + self.custom_attrs_S
        self.all_attrs_D = self.standard_attrs_D + self.custom_attrs_D

        if self.import_bboxes:
            self.all_attrs_D.append("bboxes")

    def add_custom_attrs_columns(self):
        # parse custom attributes in 'observationTags' column
        self.data["observationTags"] = self.data["observationTags"].apply(
            lambda x: dict(k.split(":") for k in str(x).split("|") if ":" in k)
        )

        # normalize the 'observationTags' column
        normalized_data = pandas.json_normalize(self.data.observationTags)

        # add custom attributes fields to self.provided_columns
        self.provided_columns += list(normalized_data.columns)

        # merge the normalized data with the original dataframe
        self.data = pandas.concat([self.data, normalized_data], axis=1).drop(
            columns=["observationTags"]
        )
        self.provided_columns.remove("observationTags")

    def get_schema(self):
        """
        Downloads a full schema for observations.csv file and rebuilds its structure.
        """
        full_schema = frictionless.Schema.from_descriptor(
            requests.get(self.schema_url).json()
        )
        custom_attrs = self.project.classificator.get_custom_attrs_schema()
        custom_attrs_dict = {a["name"]: a for a in custom_attrs}
        new_schema = frictionless.Schema()

        # first add all required fields to the new schema
        for field_name in self.required_columns:
            if field_name == "_id":
                field = frictionless.fields.IntegerField(name=field_name)
            else:
                field = full_schema.get_field(field_name)
            if field_name == "scientificName":
                field = full_schema.get_field(field_name)
                species_list = list(self.species.keys())
                field.constraints["enum"] = species_list
            new_schema.add_field(field)

        # then add all fields in the data that are either a part of the original schema or are specified
        # as custom attributes
        for field_name in self.provided_columns:
            if not new_schema.has_field(field_name):
                if full_schema.has_field(field_name):
                    field = full_schema.get_field(field_name)
                elif field_name in custom_attrs_dict:
                    field_spec = custom_attrs_dict[field_name]
                    field_type = field_spec.pop("type").capitalize()
                    field = getattr(frictionless.fields, f"{field_type}Field")(
                        **field_spec
                    )
                else:
                    continue
                field.constraints["required"] = False
                new_schema.add_field(field)

        # if specified in the form, add "bboxes" field to the schema
        if self.import_bboxes:
            bboxes_field = frictionless.fields.ArrayField(
                **{
                    "name": "bboxes",
                    "constraints": {"required": False},
                }
            )
            new_schema.add_field(bboxes_field)

        # add countNew field to the schema if provided
        if "countNew" in self.provided_columns:
            count_new_field = frictionless.fields.IntegerField(
                **{
                    "name": "countNew",
                    "constraints": {"required": False},
                }
            )
            new_schema.add_field(count_new_field)

        return new_schema

    def get_species(self):
        qs = self.project.classificator.species.values("pk", "latin_name")
        return {k["latin_name"]: k["pk"] for k in qs}

    def validate_table(self):
        """
        Validate the observations table using frictionless-py and provided schema.
        """
        records = self.data.to_dict("records")
        self.report = frictionless.validate(
            records, schema=self.new_schema, limit_errors=self.limit_validation_errors
        )

        if not self.report.valid:
            return

        self.report = frictionless.validate(
            records,
            schema=self.new_schema,
            checks=[
                DbExistingObjects(
                    field_name="_id",
                    values=self.classification_pks,
                    check_type="whitelist",
                )
            ],
            limit_errors=self.limit_validation_errors,
        )

    def import_expert_classifications(self, data, classifications):
        """
        This method is used to upload expert (user) classifications from a csv file.
        First, Classification objects are selected from db using data's _id column.
        """
        # 1) create UserClassification objects and set static attributes
        # update Classification objects if approve == True
        user_classifications = []
        uc_owners = {}
        for classification in classifications:
            # all static attributes are the same for multiple rows with the same
            # classification id; just use values from the first row
            row = data[data["_id"].astype(int) == classification.pk].iloc[0]

            # parse user classification owner (if available)
            uc_username = row.get("classifiedBy")
            if not uc_username:
                uc_owner = self.user
                uc_timestamp = self.timestamp
            else:
                uc_owner = uc_owners.get(uc_username)
                if not uc_owner:
                    try:
                        uc_owner = User.objects.get(username=uc_username)
                        uc_owners[uc_username] = uc_owner
                    except User.DoesNotExist:
                        uc_owner = self.user
                        uc_owners[uc_username] = self.user

            # parse user classification timestamp (if available)
            uc_timestamp = row.get("classificationTimestamp")
            if not uc_timestamp:
                uc_timestamp = self.timestamp
            else:
                try:
                    uc_timestamp = pandas.to_datetime(uc_timestamp).to_pydatetime()
                except Exception:
                    uc_timestamp = self.timestamp

            user_classification = UserClassification(
                classification=classification,
                owner=uc_owner,
                created_at=uc_timestamp,
                updated_at=uc_timestamp,
            )
            if self.approve:
                classification.status = True
                classification.approved_by = self.user
                classification.approved_at = now()
                classification.updated_at = self.timestamp
                classification.updated_by = self.user
                classification.has_initial_data = True

            for attr in self.all_attrs_S:
                # attrs_to_columns maps attribute names to corresponding columns in csv data
                # not all values are present in the dict - just columns from the standard,
                # when their names do not exactly match db fields
                attr_col_name = self.attrs_to_columns.get(attr, attr)
                val = row.get(attr_col_name, None)
                if not val or pandas.isna(val):
                    continue
                if attr in self.standard_attrs_S:
                    setattr(user_classification, attr, val)
                    if self.approve:
                        setattr(classification, attr, val)
                else:
                    user_classification.static_attrs.update({attr: val})
                    if self.approve:
                        classification.static_attrs.update({attr: val})
            user_classifications.append(user_classification)

        # bulk delete UserClassification objects
        # delete only user classifications owned by self.user or one of users in the uc_owners dict
        uc_users_pks = [k.pk for k in uc_owners.values()] + [self.user.pk]
        UserClassification.objects.filter(
            classification__in=classifications, owner__in=uc_users_pks
        ).delete()

        # bulk create UserClassification objects
        user_classifications = UserClassification.objects.bulk_create(
            user_classifications, batch_size=settings.BULK_BATCH_SIZE
        )

        if self.approve:
            # assigned already saved user classifications to classifications
            for i, uc in enumerate(user_classifications):
                classifications[i].approved_source = uc

            Classification.objects.bulk_update(
                classifications,
                self.standard_attrs_S
                + [
                    "static_attrs",
                    "status",
                    "approved_by",
                    "approved_at",
                    "approved_source",
                    "updated_at",
                    "updated_by",
                    "has_initial_data",
                ],
                batch_size=settings.HEAVY_BULK_UPDATE_BATCH_SIZE,
            )

        # 2) create UserClassificationDynamicAttrs objects and set dynamic attributes
        # update Classification objects if approve == True
        user_classifications_dattrs = []
        classifications_dattrs = []

        # if bboxes are imported, Classification ans UserClassification objects will need to be
        # updated again to correctly set the "has_bboxes" field
        uc_to_update = []
        c_to_update = []

        for uc in user_classifications:
            # get rows from the table
            rows = data[data["_id"].astype(int) == uc.classification_id]

            for _index, row in rows.iterrows():
                uc_dattrs = UserClassificationDynamicAttrs(userclassification=uc)

                if self.approve:
                    c_dattrs = ClassificationDynamicAttrs(
                        classification_id=uc.classification_id
                    )

                for attr in self.all_attrs_D:
                    if attr == "species":
                        attr = "species_id"
                        val = self.species.get(row.get("scientificName"), None)

                    else:
                        attr_col_name = self.attrs_to_columns.get(attr, attr)
                        val = row.get(attr_col_name, None)

                    if not val or pandas.isna(val):
                        continue

                    if attr == "bboxes" and self.import_bboxes:
                        # try to json decode bboxes; expected format is e.g.
                        # [[0.1, 0.2, 0.3, 0.4], [0.5, 0.6, 0.7, 0.8]]
                        try:
                            val = json.loads(val)
                        except json.JSONDecodeError:
                            continue

                        setattr(uc_dattrs, attr, val)
                        uc.has_bboxes = True
                        uc_to_update.append(uc)
                        if self.approve:
                            setattr(c_dattrs, attr, val)
                            c = uc.classification
                            c.has_bboxes = True
                            c_to_update.append(c)

                    elif attr not in self.custom_attrs_D:
                        setattr(uc_dattrs, attr, val)
                        if self.approve:
                            setattr(c_dattrs, attr, val)

                    else:
                        uc_dattrs.attrs.update({attr: val})
                        if self.approve:
                            c_dattrs.attrs.update({attr: val})

                user_classifications_dattrs.append(uc_dattrs)
                if self.approve:
                    classifications_dattrs.append(c_dattrs)
        # bulk create UserClassificationDynamicAttrs
        UserClassificationDynamicAttrs.objects.bulk_create(
            user_classifications_dattrs, batch_size=settings.BULK_BATCH_SIZE
        )
        if self.approve:
            # bulk delete ClassificationDynamicAttrs
            ClassificationDynamicAttrs.objects.filter(
                classification__in=classifications
            ).delete()
            # bulk create ClassificationDynamicAttrs
            ClassificationDynamicAttrs.objects.bulk_create(
                classifications_dattrs, batch_size=settings.BULK_BATCH_SIZE
            )

        # if bboxes were imported, update has_bboxes field in Classifications and UserClassifications
        if self.import_bboxes:
            UserClassification.objects.bulk_update(
                uc_to_update,
                [
                    "has_bboxes",
                ],
                batch_size=settings.HEAVY_BULK_UPDATE_BATCH_SIZE,
            )
            if self.approve:
                Classification.objects.bulk_update(
                    c_to_update,
                    [
                        "has_bboxes",
                    ],
                    batch_size=settings.HEAVY_BULK_UPDATE_BATCH_SIZE,
                )

        return _(
            f"You have successfully imported {len(classifications)} expert classifications."
        )

    def import_ai_classifications(self, data, classifications):
        """
        Method used for importing AIClassifications
        """
        # 1) create AIClassification objects and set static attributes
        ai_classifications = []
        for classification in classifications:
            # all static attributes are the same for multiple rows with the same
            # classification id; just use values from the first row
            row = data[data["_id"].astype(int) == classification.pk].iloc[0]

            # parse timestamp if possible
            aic_timestamp = row.get("classificationTimestamp")
            if not aic_timestamp:
                aic_timestamp = self.timestamp
            else:
                try:
                    aic_timestamp = pandas.to_datetime(aic_timestamp).to_pydatetime()
                except Exception:
                    aic_timestamp = self.timestamp

            # create AIClassification object
            ai_classification = AIClassification(
                classification=classification,
                owner=self.user,
                created_at=aic_timestamp,
                updated_at=aic_timestamp,
                model=self.ai_provider,
            )

            if self.approve:
                classification.status_ai = True
                classification.approved_ai_by = self.user
                classification.approved_ai_at = now()
                classification.updated_at = self.timestamp
                classification.updated_by = self.user
                classification.has_initial_data = self.overwrite_attrs

            # assign static attrs
            for attr in self.all_attrs_S:
                # attrs_to_columns maps attribute names to corresponding columns in csv data
                # not all values are present in the dict - just columns from the standard,
                # when their names do not exactly match db fields
                attr_col_name = self.attrs_to_columns.get(attr, attr)
                val = row.get(attr_col_name, None)
                if not val or pandas.isna(val):
                    continue
                if attr in self.standard_attrs_S:
                    setattr(ai_classification, attr, val)
                    if self.approve and self.overwrite_attrs:
                        setattr(classification, attr, val)
                else:
                    ai_classification.static_attrs.update({attr: val})
                    if self.approve and self.overwrite_attrs:
                        classification.static_attrs.update({attr: val})
            ai_classifications.append(ai_classification)

        # bulk delete AIClassifications
        # delete only AIClassifications produced by the same AI provider
        AIClassification.objects.filter(
            classification__in=classifications, model=self.ai_provider
        ).delete()

        # bulk create AIClassifications
        ai_classifications = AIClassification.objects.bulk_create(
            ai_classifications, batch_size=settings.BULK_BATCH_SIZE
        )

        # with AIClassifications created, update Classifications if necessary
        if self.approve:
            for i, aic in enumerate(ai_classifications):
                classifications[i].approved_source_ai = aic

            if self.overwrite_attrs:
                update_fields = self.standard_attrs_S + [
                    "static_attrs",
                    "status_ai",
                    "approved_ai_by",
                    "approved_ai_at",
                    "approved_source_ai",
                    "updated_at",
                    "updated_by",
                    "has_initial_data",
                ]
            else:
                update_fields = [
                    "status_ai",
                    "approved_ai_by",
                    "approved_ai_at",
                    "approved_source_ai",
                    "updated_at",
                    "updated_by",
                ]

            Classification.objects.bulk_update(
                classifications,
                update_fields,
                batch_size=settings.HEAVY_BULK_UPDATE_BATCH_SIZE,
            )

        # 2) create AIClassificationDynamicAttrs and add dynamic attrs
        # if necessary, remove existing ClassificationDynamicAttrs and create new
        aiclassification_dattrs = []
        classification_dattrs = []

        # collect AIClassifications and Classifications that have bboxes added, to update their has_bboxes field
        aic_to_update = []
        c_to_update = []

        for aic in ai_classifications:
            # get all rows from data (one row per one AIClassificationDynamicAttrs object)
            rows = data[data["_id"].astype(int) == aic.classification_id]

            for _index, row in rows.iterrows():
                aic_dattrs = AIClassificationDynamicAttrs(ai_classification=aic)

                if self.approve and self.overwrite_attrs:
                    c_dattrs = ClassificationDynamicAttrs(
                        classification_id=aic.classification_id
                    )

                for attr in self.all_attrs_D:
                    if attr == "species":
                        attr = "species_id"
                        val = self.species.get(row.get("scientificName"), None)

                    else:
                        attr_col_name = self.attrs_to_columns.get(attr, attr)
                        val = row.get(attr_col_name, None)

                    if not val or pandas.isna(val):
                        continue

                    if attr == "bboxes" and self.import_bboxes:
                        try:
                            val = json.loads(val)
                        except json.JSONDecodeError:
                            continue

                        setattr(aic_dattrs, attr, val)
                        aic.has_bboxes = True
                        aic_to_update.append(aic)
                        if self.approve and self.overwrite_attrs:
                            setattr(c_dattrs, attr, val)
                            c = aic.classification
                            c.has_bboxes = True
                            c_to_update.append(c)

                    elif attr not in self.custom_attrs_D:
                        setattr(aic_dattrs, attr, val)
                        if self.approve and self.overwrite_attrs:
                            setattr(c_dattrs, attr, val)

                    else:
                        aic_dattrs.attrs.update({attr: val})
                        if self.approve and self.overwrite_attrs:
                            c_dattrs.attrs.update({attr: val})

                aiclassification_dattrs.append(aic_dattrs)
                if self.approve and self.overwrite_attrs:
                    classification_dattrs.append(c_dattrs)

        # bulk create AIClassificationDynamicAttrs
        AIClassificationDynamicAttrs.objects.bulk_create(
            aiclassification_dattrs, batch_size=settings.BULK_BATCH_SIZE
        )
        if self.approve and self.overwrite_attrs:
            # bulk delete ClassificationDynamicAttrs
            ClassificationDynamicAttrs.objects.filter(
                classification__in=classifications
            ).delete()
            # bulk create ClassificationDynamicAttrs
            ClassificationDynamicAttrs.objects.bulk_create(
                classification_dattrs, batch_size=settings.BULK_BATCH_SIZE
            )

        # if bboxes were imported, update has_bboxes field in Classifications and UserClassifications
        if self.import_bboxes:
            AIClassification.objects.bulk_update(
                aic_to_update,
                [
                    "has_bboxes",
                ],
                batch_size=settings.HEAVY_BULK_UPDATE_BATCH_SIZE,
            )
            if self.approve and self.overwrite_attrs:
                Classification.objects.bulk_update(
                    c_to_update,
                    [
                        "has_bboxes",
                    ],
                    batch_size=settings.HEAVY_BULK_UPDATE_BATCH_SIZE,
                )

        return _(
            f"You have successfully imported {len(classifications)} AI classifications."
        )

    def import_classifications(self):
        ai_mask = self.data["classificationMethod"] == "machine"
        expert_mask = self.data["classificationMethod"] == "human"

        ai_data = self.data[ai_mask]
        expert_data = self.data[expert_mask]

        msg_ai = "No AI classifications to import."
        msg_expert = "No expert classifications to import."

        if self.import_ai_classifications_flag:
            classifications_ai = self.project.classifications.filter(
                pk__in=ai_data["_id"].values
            )
            if classifications_ai:
                msg_ai = self.import_ai_classifications(ai_data, classifications_ai)

        if self.import_expert_classifications_flag:
            classifications_expert = self.project.classifications.filter(
                pk__in=expert_data["_id"].values
            )
            if classifications_expert:
                msg_expert = self.import_expert_classifications(
                    expert_data, classifications_expert
                )

        return f"{msg_ai} {msg_expert}"
