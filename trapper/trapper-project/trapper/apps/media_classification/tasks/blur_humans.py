import os
from io import BytesIO

from PIL import ImageFilter, Image
from django.conf import settings
from django.core.files.storage import get_storage_class
from django.utils.translation import gettext_lazy as _
from django.core.files.uploadedfile import SimpleUploadedFile
from django.db.models import Q

from trapper.apps.media_classification.models import (
    ClassificationDynamicAttrs,
    ClassificationProjectCollection,
)
from trapper.apps.media_classification.taxonomy import ObservationType


class ProjectCollectionBlurHumans:
    """
    Used to blur bboxes classified as humans and approved and replace resource's storage files.
    The current implementation supports only images.
    """

    def __init__(self, user, pks, exclude_blurred, only_images=True):
        self.user = user
        self.pks = pks
        self.storage = get_storage_class()()
        self.only_images = only_images
        self.exclude_blurred = exclude_blurred

    def run(self):
        qs = ClassificationProjectCollection.objects.filter(pk__in=self.pks)
        for collection in qs:
            self.process_collection(collection)
        msg = _(f"You have successfully processed {qs.count()} collections.")
        return msg

    def prepare_resources(self, collection):
        classifications = collection.classifications.filter(
            Q(status=True) | Q(status_ai=True)
        )

        if self.only_images:
            classifications = classifications.filter(resource__resource_type="I")

        dyn_attrs = (
            ClassificationDynamicAttrs.objects.filter(
                classification__in=classifications
            )
            .filter(observation_type=ObservationType.HUMAN, bboxes__isnull=False)
            .select_related("classification__resource")
            # only editable resources
            .filter(
                Q(classification__resource__owner=self.user)
                | Q(classification__resource__managers=self.user)
            )
        )

        if self.exclude_blurred:
            dyn_attrs = dyn_attrs.exclude(classification__resource__humans_blurred=True)

        resources_dict = {}
        for da in dyn_attrs:
            resource = da.classification.resource
            if resources_dict.get(resource):
                resources_dict[resource].extend(da.bboxes)
            else:
                resources_dict[resource] = list(da.bboxes)
        return resources_dict

    def process_collection(self, collection):
        resources_dict = self.prepare_resources(collection)
        for r, bb in resources_dict.items():
            self.blur_humans_in_resource(r, bb)

    def calc_pil_coords(self, bbox, img_size):
        """
        Used to calculate coordinates to crop() PIL Image, based on classification's bbox and image's size.

        bbox = [xmin, ymin, box_width, box_height] -> relative
        img_size = (img_width, img_height) -> size of the entire image in pixels

        returns [x1, y1, x2, y2] -> absolute coords of top left and bottom right points (in pixels) of bbox
        to crop from original image
        """
        xmin, ymin, box_width, box_height = bbox
        img_width, img_height = img_size
        x1 = int(img_width * xmin)
        y1 = int(img_height * ymin)
        x2 = int(img_width * (xmin + box_width))
        y2 = int(img_height * (ymin + box_height))
        return x1, y1, x2, y2

    def blur_image(self, image_field, bboxes, mime_type):
        name = image_field.name
        filename = os.path.basename(name)
        # load image from the storage to PIL Image object
        img = Image.open(image_field)
        img_size = img.size
        for bbox in bboxes:
            # translate relative coords to pixels, cut the bbox, blur it and paste back into the image
            bbox_pil = self.calc_pil_coords(bbox, img_size)
            crop_img = img.crop(bbox_pil)
            blur_img = crop_img.filter(ImageFilter.BoxBlur(radius=settings.BLUR_RADIUS))
            img.paste(blur_img, bbox_pil)

        img_io = BytesIO()
        img.save(img_io, "jpeg")

        # remove old file from storage
        self.storage.delete(name)

        # save the blurred one
        suf = SimpleUploadedFile(filename, img_io.getvalue(), mime_type)
        image_field.save(filename, suf, save=False)

        suf.close()
        img.close()

        return True

    def blur_humans_in_resource(self, resource, bboxes):
        file_fields = [resource.file, resource.file_preview, resource.file_thumbnail]
        for ff in file_fields:
            # test with bool because resource.file evaluates to FileField regardless
            # of whether there's a file or not
            if bool(ff):
                try:
                    self.blur_image(ff, bboxes, resource.mime_type)
                    resource.humans_blurred = True
                    resource.save()
                except Exception:
                    pass
