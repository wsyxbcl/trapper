# -*- coding: utf-8 -*-
import json
from collections import OrderedDict
from typing import List

import pandas
import requests
from crispy_forms import layout
from crispy_forms.layout import Layout, HTML, Fieldset, Row, Column
from django import forms
from django.contrib.auth import get_user_model
from django.db.models import Q
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from extra_views import InlineFormSetFactory
from taggit.forms import TagField

from trapper.apps.accounts.models import UserDataPackage
from trapper.apps.accounts.taxonomy import PackageType
from trapper.apps.common.forms import (
    BaseCrispyModelForm,
    BaseCrispyForm,
    ProjectBaseInlineFormSet,
    BaseBulkUpdateForm,
    ReadOnlyFieldsMixin,
)
from trapper.apps.common.fields import OwnerModelChoiceField
from trapper.apps.common.tools import parse_pks
from trapper.apps.extra_tables.models import Species, Licence
from trapper.apps.media_classification.models import (
    ClassificationProject,
    ClassificationProjectCollection,
    ClassificationProjectRole,
    Classificator,
    Sequence,
    Classification,
    AIProvider,
    StandardDynamicAttrsMixin,
    AIClassification,
    ExternalAIProvider,
)
from trapper.apps.media_classification.taxonomy import (
    ClassificatorSettings,
    ClassificationProjectRoleLevels,
    AIClassificationSettings,
)
from trapper.apps.research.models import ResearchProject, ResearchProjectCollection
from trapper.apps.research.taxonomy import ResearchProjectRoleType
from trapper.apps.storage.models import Resource, Collection
from trapper.middleware import get_current_user

User = get_user_model()


class ProjectRoleForm(BaseCrispyModelForm):
    """
    Model form for creating or updating :class:`ResearchProjectRole` objects.
    """

    form_style = "inline"
    user = OwnerModelChoiceField(queryset=User.objects.all())

    class Meta:
        model = ClassificationProjectRole
        exclude = ["classification_project", "date_created"]

    def __init__(self, *args, **kwargs):
        """
        Remove labels for formset
        """
        super().__init__(*args, **kwargs)
        self.fields["user"].label = ""
        self.fields["user"].widget.attrs["class"] = "select2-formset"
        self.fields["name"].label = ""

    def has_changed(self):
        """
        https://stackoverflow.com/a/22789926/11207840
        """
        changed_data = super().has_changed()
        if self.instance.pk:
            return changed_data
        else:
            return bool(self.initial or changed_data)


class ProjectRoleInline(InlineFormSetFactory):
    """
    Utility-class: ProjectRoles displayed as a InlineFormset.
    """

    model = ClassificationProjectRole
    form_class = ProjectRoleForm
    formset_class = ProjectBaseInlineFormSet
    factory_kwargs = {"extra": 0}


class ProjectRoleCreateInline(ProjectRoleInline):
    factory_kwargs = {"extra": 1}

    def get_initial(self):
        initial = [
            {"user": self.request.user, "name": ClassificationProjectRoleLevels.ADMIN}
        ]
        return initial


class ProjectForm(BaseCrispyModelForm):
    """
    Form used for creating or updating :class:`ClassificationProject` objects.
    """

    template_path = "media_classification/projects/"

    class Meta:
        model = ClassificationProject
        exclude = ["owner"]

    def __init__(self, *args, **kwargs):
        """
        Preselect research project and check if user has access to given
        ResearchProject
        """
        super().__init__(*args, **kwargs)
        user = get_current_user()
        self.fields["classificator"].required = True

        projects_queryset = ResearchProject.objects.get_accessible(
            user=user, role_levels=ResearchProjectRoleType.EDIT
        )
        self.fields["research_project"].queryset = projects_queryset
        self.fields["default_ai_model"].queryset = AIProvider.objects.get_accessible()

        selected_value = self.initial.get("selected", None)
        if not self.instance.pk and selected_value:
            try:
                rproject = ResearchProject.objects.get(pk=selected_value)
            except ResearchProject.DoesNotExist:
                pass
            else:
                if rproject.can_update(user):
                    self.fields["research_project"].initial = rproject.pk
        if not self.instance.pk:
            self.fields["status"].widget = forms.HiddenInput()

    def include(self, template_name):
        return HTML(
            '{{% include "{path}{name}" %}}'.format(
                path=self.template_path, name=template_name
            )
        )

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset(
                "",
                "name",
                "research_project",
                "status",
                "classificator",
                "default_ai_model",
                self.include(template_name="form_inline.html"),
                self.include(template_name="role_formset.html"),
            ),
        )


class ProjectCollectionAddForm(BaseCrispyForm):
    """"""

    project = forms.ModelChoiceField(
        queryset=ClassificationProject.objects.none(),
        help_text=_(
            "To which classification project you want to add the selected collections?"
        ),
    )
    collections_pks = forms.CharField(
        widget=forms.HiddenInput(),
    )
    rproject = forms.IntegerField(
        widget=forms.HiddenInput(),
    )
    select2_ajax_fields = (
        ("project", "/media_classification/api/projects", "pk", "name"),
    )

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset("", "project", "collections_pks", "rproject"),
        )

    def clean(self):
        cleaned_data = super().clean()
        collections_pks = cleaned_data.pop("collections_pks", None)
        project = cleaned_data.get("project", None)
        rproject_pk = cleaned_data.get("rproject", None)
        user = get_current_user()

        if project and collections_pks:
            if not project.research_project.pk == rproject_pk:
                error = mark_safe(
                    _(
                        "The selected classification project belongs to a different research project."
                    )
                )
                cleaned_data["error"] = error
                return cleaned_data

            pks_list = parse_pks(collections_pks)
            collections = ResearchProjectCollection.objects.filter(
                pk__in=pks_list,
                project__project_roles__user=user,
                project__project_roles__name=1,  # admin
            )
            project_collections_pks = list(
                set(project.collections.values_list("pk", flat=True))
            )
            new_collections = collections.exclude(pk__in=project_collections_pks)
            if not new_collections:
                error = mark_safe(
                    _(
                        "All selected collections are already in this classification project."
                    )
                )
                cleaned_data["error"] = error
                return cleaned_data

            cleaned_data["new_collections"] = new_collections
        else:
            error = mark_safe(
                _(
                    "Nothing to process (most probably you have no permission "
                    "to run this action on the selected collections)."
                )
            )
            cleaned_data["error"] = error
        return cleaned_data

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = get_current_user()
        self.fields["project"].widget = forms.CharField.widget()
        self.fields["project"].queryset = ClassificationProject.objects.get_accessible(
            user=self.user, role_levels=[1, 3]  # 1: Admin, 3: Collaborator
        )


class ProjectCollectionBlurHumansForm(BaseCrispyForm):
    pks = forms.CharField(
        widget=forms.HiddenInput(),
    )

    exclude_blurred = forms.BooleanField(
        label=_("Exclude blurred"),
        help_text=_("Exclude already blurred resources"),
        required=False,
        initial=True,
    )

    def clean_pks(self):
        pks = parse_pks(self.data.get("pks"))
        for project_collection_id in pks:
            project_collection = ClassificationProjectCollection.objects.get(
                pk=project_collection_id
            )
            if not project_collection.can_update():
                raise forms.ValidationError(
                    _("You cannot edit one or more of chosen collections.")
                )
        return pks


class SequenceForm(BaseCrispyModelForm):
    """
    Form used for creating or updating :class:`Sequence` objects
    """

    resources = forms.CharField()

    class Meta:
        model = Sequence
        exclude = ["created", "user", "collection"]
        widgets = {"resources": forms.HiddenInput()}

    def clean_resources(self):
        """
        Convert resources from a string of comma separated integers into a list of
        integers.
        """
        resources = self.data.get("resources", None)
        return parse_pks(pks=resources)

    def clean(self):
        """Verify that sequence contain at least one resource"""
        cleaned_data = super().clean()
        resources = cleaned_data.get("resources")
        if not resources:
            raise forms.ValidationError(_("You did not select any resources."))
        return cleaned_data


class StandardDynamicAttrsForm(BaseCrispyModelForm):
    """"""

    class Meta:
        model = StandardDynamicAttrsMixin
        exclude = ["attrs"]


class PredefinedAttributeForm(BaseCrispyForm):
    """"""

    def __init__(self, *args, **kwargs):
        """
        Build form fields based on predefined attributes definition.
        """
        super().__init__(*args, **kwargs)
        attrs = ClassificatorSettings.PREDEFINED_ATTRIBUTES
        target_form_field = forms.ChoiceField(
            choices=ClassificatorSettings.TARGET_CHOICES,
            label=_("Target form"),
            help_text=_("choose a target form for this attribute"),
            required=False,
            initial="D",
        )
        required_form_field = forms.BooleanField(
            label=_("Required"),
            required=False,
            initial=False,
        )
        self.layout = []
        # create form fields for predefined attributes
        for attr in attrs:
            self.layout.append(HTML('<li class="panel-simple">'))
            attr_target = "{name}_target".format(name=attr)
            attr_required = "{name}_required".format(name=attr)
            self.fields[attr] = forms.BooleanField(
                required=False,
                label=attrs[attr]["label"],
                help_text=attrs[attr]["help_text"],
            )
            self.layout.append(attr)
            self.fields[attr_target] = target_form_field
            self.layout.append(attr_target)
            self.fields[attr_required] = required_form_field
            self.layout.append(attr_required)
            self.layout.append(HTML("</li>"))

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(*self.layout)


class CustomAttributeForm(BaseCrispyForm):
    """
    Form used to build a definition of a custom attribute.
    """

    name = forms.CharField(label=_("Name"), max_length=80, required=True)
    field_type = forms.ChoiceField(
        choices=ClassificatorSettings.FIELD_CHOICES,
        label=_("Type"),
        required=True,
        initial="S",
    )
    target = forms.ChoiceField(
        choices=ClassificatorSettings.TARGET_CHOICES,
        label=_("Target form"),
        help_text=_("choose a target form for this attribute"),
        required=True,
        initial="D",
    )
    values = forms.CharField(
        label=_("Values"),
        max_length=5000,
        help_text=(_("define possible values for this attribute (comma separated)")),
        required=False,
        widget=forms.Textarea(),
    )
    initial = forms.CharField(
        label=_("Initial"),
        max_length=100,
        help_text=_("define initial value"),
        required=False,
    )
    required = forms.BooleanField(
        label=_("Required"),
        required=False,
        initial=False,
    )
    description = forms.CharField(
        label=_("Description"), required=True, widget=forms.Textarea()
    )

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            "name",
            "field_type",
            "target",
            "values",
            "initial",
            "required",
            "description",
        )

    def clean(self):
        """
        Veirfy that all data describing a custom attribute is correct.
        """
        cleaned_data = super().clean()
        name = cleaned_data.get("name")
        field_type = cleaned_data.get("field_type")
        values = cleaned_data.get("values")
        initial = cleaned_data.get("initial")
        test_numeric_types = ClassificatorSettings.NUMERIC_MAPPERS
        reserved_names = list(
            ClassificatorSettings.STANDARD_ATTRS
            + ClassificatorSettings.PREDEFINED_NAMES
        )
        if not name:
            self._errors["name"] = self.error_class(
                [_("You have forgotten about this field.")]
            )
            return cleaned_data
        if name and name.lower() in reserved_names:
            self._errors["name"] = self.error_class(
                [_("This name is reserved for a standard or predefined attribute.")]
            )
            del cleaned_data["name"]
            return cleaned_data
        if name and "," in name:
            self._errors["name"] = self.error_class(
                [_("The name can not contain a comma.")]
            )
            del cleaned_data["name"]
            return cleaned_data
        if field_type == "B" and values:
            self._errors["values"] = self.error_class(
                [
                    _(
                        'Boolean type has only two possible values: "True" and '
                        '"False". You do not need to specify anything here.'
                    )
                ]
            )
            del cleaned_data["values"]
            return cleaned_data
        if values:
            values_list = list(values.split(","))
            # Make sure that this will be a list of unique values
            if len(values_list) != len(set(values_list)):
                self._errors["values"] = self.error_class(
                    [_("There are duplicated values in your list.")]
                )
                del cleaned_data["values"]
                return cleaned_data
            # Make sure that there is no leading or trailing spaces
            values_list = [k.strip() for k in values_list]
            # values_list.sort()
            if len(values_list) < 2:
                self._errors["values"] = self.error_class(
                    [_('Parser error. Are these values really "comma-separated"?')]
                )
                del cleaned_data["values"]
                return cleaned_data
            if initial and initial not in values_list:
                self._errors["initial"] = self.error_class(
                    [_('The "initial" value must be one of defined possible values.')]
                )
                del cleaned_data["initial"]
            if field_type in test_numeric_types:
                for value in values_list:
                    try:
                        test_numeric_types[field_type](value)
                    except ValueError as error:
                        self._errors["values"] = self.error_class(
                            [_(f"Value error: {error}")]
                        )
                        del cleaned_data["values"]
                        return cleaned_data
            cleaned_data["values"] = ",".join(values_list)
        else:
            if initial and field_type in test_numeric_types.keys():
                try:
                    test_numeric_types[field_type](initial)
                except ValueError as error:
                    self._errors["initial"] = self.error_class(
                        [_(f"Value error: {error}")]
                    )
                    del cleaned_data["initial"]
        return cleaned_data


class ClassificatorForm(BaseCrispyModelForm):
    """
    Basic form used to describe a classificator.
    """

    # except "species" all required dynamic standard attributes are just listed in the
    # classificator add/change view
    species = forms.CharField(
        required=False,
        help_text=_("Select subset of species available for classification."),
        label=_("Species"),
    )
    observation_type = forms.CharField(
        disabled=True,
        required=False,
        help_text=_("Type of observation (list): Human/Animal/Vehicle"),
        label=_("Observation type"),
    )
    count = forms.CharField(
        disabled=True,
        required=False,
        help_text=_("Number of individuals"),
        label=_("Count"),
    )
    is_setup = forms.CharField(
        disabled=True,
        required=False,
        help_text=_("Indicates records that are part of the camera setup process"),
        label=_("Is setup"),
    )
    sex = forms.BooleanField(
        initial=False,
        required=False,
        label=_("Sex"),
        help_text=_("Sex of individuals (list): Male/Female/Undefined"),
    )
    age = forms.BooleanField(
        initial=False,
        required=False,
        label=_("Age"),
        help_text=_("Age of individuals (list): Adult/Juvenile/Undefined"),
    )
    count_new = forms.BooleanField(
        initial=False,
        required=False,
        label=_("Count new"),
        help_text=_(
            "Number of NEW individuals taking into account the entire sequence"
        ),
    )
    behaviour = forms.BooleanField(
        initial=False,
        required=False,
        label=_("Behaviour"),
        help_text=_(
            "Dominant behaviour (list): Grazing/Browsing/Rooting/Vigilance/Running/"
            "Walking/Undefined"
        ),
    )
    individual_id = forms.BooleanField(
        initial=False,
        required=False,
        label=_("Individual ID"),
        help_text=_('Individual ID (for "marked" species)'),
    )
    classification_confidence = forms.BooleanField(
        initial=False,
        required=False,
        label=_("Classification confidence"),
        help_text=_(
            "Accuracy confidence of the classification. Expressed as a probability, with `1` being maximum confidence."
        ),
    )

    select2_ajax_fields = (
        ("species", "/tables/api/species", "pk", "english_name", "true"),
    )

    class Meta:
        model = Classificator
        fields = [
            "name",
            "template",
            "species",
            "observation_type",
            "is_setup",
            "count",
            "sex",
            "age",
            "behaviour",
            "count_new",
            "individual_id",
            "classification_confidence",
            "static_attrs_order",
            "dynamic_attrs_order",
        ]
        widgets = {
            "static_attrs_order": forms.HiddenInput(
                attrs={"id": "id_static_attrs_order"}
            ),
            "dynamic_attrs_order": forms.HiddenInput(
                attrs={"id": "id_dynamic_attrs_order"}
            ),
        }

    def __init__(self, *args, **kwargs):
        if kwargs.get("instance"):
            initial = kwargs.setdefault("initial", {})
            initial["species"] = ",".join(
                [str(k.pk) for k in kwargs["instance"].species.all()]
            )
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        species_pks = cleaned_data.get("species")
        species_pks = parse_pks(species_pks)
        if species_pks:
            cleaned_data["species"] = Species.objects.filter(pk__in=species_pks)
        return cleaned_data

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset(
                _("Required classificator properties"),
                "name",
                "template",
                "species",
                Row(
                    Column("observation_type", css_class="form-group col-md-4"),
                    Column("count", css_class="form-group col-md-4"),
                    Column("is_setup", css_class="form-group col-md-4"),
                    css_class="form-row",
                ),
            ),
            Fieldset(
                _("Non-required standard dynamic attributes"),
                Row(
                    Column("sex", css_class="form-group col-md-3"),
                    Column("age", css_class="form-group col-md-3"),
                    Column("count_new", css_class="form-group col-md-3"),
                    Column("behaviour", css_class="form-group col-md-3"),
                    css_class="form-row",
                ),
                Row(
                    Column("individual_id", css_class="form-group col-md-3"),
                    Column(
                        "classification_confidence", css_class="form-group col-md-3"
                    ),
                    css_class="form-row",
                ),
            ),
            "static_attrs_order",
            "dynamic_attrs_order",
        )


class ClassificationForm(ReadOnlyFieldsMixin, BaseCrispyForm):
    """
    Create classification forms (dynamic and static) dynamically based on
    given classificator object.
    """

    def __init__(self, *args, **kwargs):
        fields_defs = kwargs.pop("fields_defs")
        attrs_order = kwargs.pop("attrs_order")
        readonly = kwargs.pop("readonly")
        self.base_fields = OrderedDict(fields_defs)
        self.base_fields.keyOrder = attrs_order
        if readonly:
            self.readonly_fields = tuple(attrs_order)
        super().__init__(*args, **kwargs)
        for name, field in self.fields.items():
            field.widget.attrs["class"] = "form-control"

    @property
    def helper(self):
        new_helper = super().helper
        new_helper.form_show_labels = False
        return new_helper

    def clean_bboxes(self):
        bboxes = self.cleaned_data.get("bboxes")
        if bboxes:
            try:
                bboxes = json.loads(bboxes)
                return bboxes
            except ValueError:
                raise forms.ValidationError("Invalid JSON data.")
        return None

    def clean_species(self):
        observation_type = self.cleaned_data.get("observation_type", "")
        species = self.cleaned_data.get("species", "")
        if observation_type == "animal" and not species:
            raise forms.ValidationError(
                "You must provide species if observation type is 'animal'"
            )
        if observation_type == "blank" and species:
            raise forms.ValidationError(
                "You can not provide species if observation type is 'blank'"
            )

        return self.cleaned_data.get("species", None)

    def clean_count(self):
        count = self.cleaned_data.get("count")
        observation_type = self.cleaned_data.get("observation_type", "")

        # for blank observations on unclassified, clean the value
        if observation_type in ["blank", "unclassified"]:
            return 0

        # for classified observation, count has to be one or more
        else:
            if count <= 0:
                raise forms.ValidationError("Count has to be one or more")

        return count

    def clean_count_new(self):
        count_new = self.cleaned_data.get("count_new")
        if count_new is not None:
            if count_new < 0:
                raise forms.ValidationError("Count_new has to be zero or more")

        return count_new


class BulkUpdateClassificationForm(BaseBulkUpdateForm):
    """"""

    status = forms.BooleanField(
        required=False,
        label=_("Approved"),
        initial=False,
        widget=forms.Select(choices=((False, "False"), (True, "True"))),
        help_text=_("Set classifications status."),
    )

    status_ai = forms.BooleanField(
        required=False,
        label=_("Approved AI"),
        initial=False,
        widget=forms.Select(choices=((False, "False"), (True, "True"))),
        help_text=_("Set AI classifications status."),
    )

    class Meta:
        model = Classification
        fields = ("status", "status_ai")

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset("", "status", "status_ai", "records_pks"),
        )

    def filter_editable(self):
        user = get_current_user()
        return Classification.objects.filter(
            project__classification_project_roles__user=user,
            project__classification_project_roles__name=ClassificationProjectRoleLevels.ADMIN,
        )


class ClassificationFeedbackForm(BaseCrispyForm):
    """
    TODO: docstrings
    """

    classification_pks = forms.CharField(widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        self.user = get_current_user()
        project = kwargs.pop("project")
        super().__init__(*args, **kwargs)
        for k, v in project.classificator.custom_attrs.items():
            if v["field_type"] in ["S", "B"]:
                name = f"custom_{k}"
                desc = v["description"]
                self.fields[name] = forms.BooleanField(
                    required=False,
                    initial=False,
                    label=f"Custom attribute: {k}",
                    help_text=f"Description: {desc}",
                )

    def clean(self):
        cleaned_data = super().clean()
        classification_pks = cleaned_data.get("classification_pks", None)
        classification_pks = parse_pks(classification_pks)
        # get only approved classifications of editable resources
        classifications = Classification.objects.filter(
            pk__in=classification_pks, status=True
        )
        if not self.user.is_staff:
            classifications = classifications.filter(
                Q(resource__owner=self.user) | Q(resource__managers=self.user)
            )
        if classifications:
            cleaned_data["classifications"] = classifications.select_related("resource")
        else:
            errors = _(
                "Nothing to process (most probably you have no permission to run this "
                "action for the selected resources)."
            )
            raise forms.ValidationError(errors)
        return cleaned_data


class ClassificationExportAggForm(BaseCrispyForm):
    FUN_CHOICES = (
        ("sum", "sum"),
        ("min", "min"),
        ("max", "max"),
        ("mean", "mean"),
    )

    AGG_TARGET_LEVEL_CHOICES = (
        ("deployment", _("Deployment")),
        ("location", _("Location")),
    )

    COUNT_VAR_MAP = {
        "count": "count",
        "count_new": "countNew",
    }

    agg_target_level = forms.ChoiceField(
        initial="deployment",
        label=_("Aggregation target level"),
        choices=AGG_TARGET_LEVEL_CHOICES,
        help_text=_("Level of aggregation for the target data"),
    )
    event_fun = forms.ChoiceField(
        initial="max",
        label=_("Event-level aggregation function"),
        choices=FUN_CHOICES,
        help_text=_(
            "Aggregation function used for aggregating observations into events"
        ),
    )
    count_var = forms.ChoiceField(
        initial=None,
        label=_("Count variable"),
        help_text=_("Variable used for counting individuals"),
    )
    count_fun = forms.ChoiceField(
        initial="sum",
        label=_("Count function"),
        choices=FUN_CHOICES,
        help_text=_("Aggregation function used at a deployment / location level"),
    )
    all_dep = forms.BooleanField(
        initial=True,
        required=False,
        label=_("All deployments"),
        help_text=_(
            "Include all deployments from the associated research project; if not checked "
            "only deployments with at least one media file recorded are included; "
            "optionally filter deployments using the field below:"
        ),
    )
    filter_dep = forms.CharField(
        required=False,
        label="",
        widget=forms.TextInput(
            attrs={"placeholder": _("Filter deployments by provided text")}
        ),
    )
    geojson = forms.BooleanField(
        initial=False,
        required=False,
        label=_("Geojson"),
        help_text=_("Format output as geojson (CSV is returned otherwise)"),
    )
    api_url_filters = forms.CharField(widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        self.user = get_current_user()
        project = kwargs.pop("project")
        super().__init__(*args, **kwargs)
        c = project.classificator
        attrs = [
            self.COUNT_VAR_MAP[k]
            for k in c.get_numerical_attrs()
            if k in self.COUNT_VAR_MAP.keys()
        ]
        self.fields["count_var"].choices = ((a, a) for a in attrs)

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset(
                "",
                "agg_target_level",
                "event_fun",
                "count_var",
                "count_fun",
                "all_dep",
                "filter_dep",
                "geojson",
                "api_url_filters",
            ),
        )


class ClassificationImportAttrsForm(BaseCrispyForm):
    """
    TODO: docstrings
    """

    collections_pks = forms.CharField(widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        self.user = get_current_user()
        self.project = kwargs.pop("project", None)
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        collections_pks = cleaned_data.get("collections_pks", None)
        collections_pks = parse_pks(collections_pks)
        collections = ClassificationProjectCollection.objects.filter(
            project=self.project, pk__in=collections_pks
        )
        if collections:
            cleaned_data["collections"] = collections.prefetch_related(
                "classifications__resource"
            )
        else:
            errors = _(
                "Nothing to process (most probably you have no permission to run this "
                "action for the selected collections)."
            )
            raise forms.ValidationError(errors)
        return cleaned_data


class ClassificationImportForm(BaseCrispyForm):
    """
    TODO: docstrings
    """

    observations_csv = forms.FileField(
        required=True,
        label=_("Observations table (csv)"),
        help_text=_(
            "CSV file with observations to import in the internal Trapper format. CSV has to be separated by comma. "
            "A table must contain the '_id' column."
        ),
    )
    project = forms.ModelChoiceField(
        queryset=None, required=True, label=_("Classification project")
    )
    approve = forms.BooleanField(
        initial=True,
        required=False,
        label=_("Approve"),
        help_text=_("Approve all imported classifications."),
    )
    import_bboxes = forms.BooleanField(
        initial=True,
        required=False,
        label=_("Import bboxes"),
        help_text=_('Import bounding boxes if available in the "bboxes" column.'),
    )
    import_expert_classifications = forms.BooleanField(
        initial=True,
        required=False,
        label=_("Import expert classifications"),
        help_text=_("Import classifications with classificationMethod = human."),
    )
    import_ai_classifications = forms.BooleanField(
        initial=False,
        required=False,
        label=_("Import AI classifications"),
        help_text=_("Import classifications with classificationMethod = machine."),
    )
    overwrite_attrs = forms.BooleanField(
        initial=False,
        required=False,
        label=_(
            "Overwrite classification attributes when approving imported AI Classifications."
        ),
        help_text=_(
            "If you're importing AI Classifications and marking them as approved, check this flag to overwrite "
            "Classification attributes. Only Classification flags regarding AI Classification approval will be "
            "changed otherwise."
        ),
    )
    ai_provider = forms.ModelChoiceField(
        queryset=None,
        required=False,
        label=_("AI Provider for imported AI Classifications"),
        help_text=_(
            "AI Provider object to assign to newly created AI Classifications. Only required if you're importing "
            "AI Classifications."
        ),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = get_current_user()
        projects_qs = ClassificationProject.objects.get_accessible(
            user=self.user, role_levels=[1]
        )
        self.fields["project"].queryset = projects_qs
        # todo change to appropriate get_accessible method when AIProvider access issue is resolved
        ai_provider_qs = AIProvider.objects.all()
        self.fields["ai_provider"].queryset = ai_provider_qs

    def get_layout(self):
        return Layout(
            Fieldset(
                "",
                "project",
                "observations_csv",
                "approve",
                "import_bboxes",
                "import_expert_classifications",
                "import_ai_classifications",
                "overwrite_attrs",
                "ai_provider",
            ),
        )

    def clean(self):
        """
        TODO: docstrings
        """
        dtype_dict = {"individualID": str}
        cleaned_data = super().clean()
        if not self.errors:
            project = cleaned_data.get("project")
            if not project.is_project_admin(user=self.user):
                errors = _(
                    "You are not allowed to import classifications into selected project. "
                    "Admin role is required for that."
                )
                raise forms.ValidationError(errors)
            if not project.classificator:
                errors = _(
                    "The selected classification project has no classificator assigned."
                )
                raise forms.ValidationError(errors)

            import_expert_classifications = cleaned_data.get(
                "import_expert_classifications"
            )
            import_ai_classifications = cleaned_data.get("import_ai_classifications")
            if not (import_expert_classifications or import_ai_classifications):
                errors = _(
                    "You need to choose to import expert or AI classifications (or both)."
                )
                raise forms.ValidationError(errors)

            ai_provider = cleaned_data.get("ai_provider")
            if import_ai_classifications and not ai_provider:
                errors = _(
                    "When importing AI Classifications, you need to choose the AI Provider."
                )
                raise forms.ValidationError(errors)

            observations_csv = cleaned_data.get("observations_csv", None)
            # the table's structure and content is validated later in the view
            # here only make sure that the uploaded csv file can be parsed by pandas
            try:
                observations_df = pandas.read_csv(
                    observations_csv, sep=",", dtype=dtype_dict
                )
            except pandas.errors.ParserError:
                errors = _(
                    f'The file "{observations_csv.name}" is not a valid CSV file.'
                )
                raise forms.ValidationError(errors)
            cleaned_data["observations_df"] = observations_df
        return cleaned_data


class ClassificationExportForm(BaseCrispyForm):
    """
    TODO: docstrings
    """

    EXPORT_FORMAT_CHOICES = (
        ("camtrapdp", _("Camtrap DP (standard)")),
        ("trapper", _("Trapper (internal)")),
    )

    export_format = forms.ChoiceField(
        choices=EXPORT_FORMAT_CHOICES,
        label=_("Export format"),
        required=True,
        initial="camtrapdp",
        help_text=_(
            "Format of the exported data package. Choose Camtrap DP format (https://tdwg.github.io/camtrap-dp) "
            "to build a standardized camera trap data package when you want to publish your data or share it with "
            "others. Choose Trapper format for internal export, useful for migration (export/import) and backing "
            "up your Trapper data."
        ),
    )

    name = forms.CharField(
        label=_("Name"),
        max_length=30,
        required=True,
        help_text=_("Short and preferably human-readable name of the package"),
    )
    title = forms.CharField(
        label=_("Title"),
        max_length=255,
        required=True,
        help_text=_("Title or one sentence description for the package"),
    )
    version = forms.CharField(
        label=_("Version"),
        max_length=10,
        required=True,
        initial="1.0",
        help_text=_("Version of the package"),
    )
    keywords = TagField(required=False)
    licences = forms.ModelMultipleChoiceField(
        label=_("Licences"),
        required=False,
        queryset=Licence.objects.all(),
        help_text=_("Licence(s) under which the package is provided"),
    )
    approved_only = forms.BooleanField(
        initial=True,
        required=False,
        label=_("Approved classifications"),
        help_text=_("Include only already approved classifications"),
    )
    all_dep = forms.BooleanField(
        initial=True,
        required=False,
        label=_("All deployments"),
        help_text=_(
            "Include all deployments from the associated research project; if not checked "
            "only deployments with at least one media file recorded are included; "
            "optionally filter deployments using the field below:"
        ),
    )
    filter_dep = forms.CharField(
        required=False,
        label="",
        widget=forms.TextInput(
            attrs={"placeholder": _("Filter deployments by provided text")}
        ),
    )
    trapper_url_token = forms.BooleanField(
        label=_("URLs with token"),
        initial=False,
        required=False,
        help_text=_(
            "Indicates if urls to multimedia files in the media.csv table "
            "should contain access tokens"
        ),
    )
    include_events = forms.BooleanField(
        label=_("Include events"),
        initial=True,
        required=False,
        help_text=_(
            "Camtrap DP specific: whether to include event-based observations in the observations file. "
            "Aggregation can only work if 1) sequences have been generated and 2) the project classifier has "
            "the 'count_new' field.",
        ),
    )
    private_human = forms.BooleanField(
        label=_("Mark media with humans as private"),
        initial=True,
        required=False,
        help_text=_(
            "Whether to mark resources depicting humans as private and omit Trapper url tokens"
        ),
    )
    private_vehicle = forms.BooleanField(
        label=_("Mark media with vehicles as private"),
        initial=True,
        required=False,
        help_text=_(
            "Whether to mark resources depicting vehicles as private and omit Trapper url tokens"
        ),
    )
    private_species = forms.ModelMultipleChoiceField(
        label=_("Animal species to mark as private"),
        required=False,
        help_text=_(
            "Whether to mark resources depicting chosen animal species as private and omit trapper url tokens"
        ),
        queryset=Species.objects.all(),
    )
    include_ids = forms.BooleanField(
        label=_("Include Trapper's DB IDs"),
        initial=False,
        required=False,
        help_text=_(
            "Whether to include the '_id' column in exported tables. The '_id' column contains IDs "
            "from Trapper's database and is required to import data into Trapper."
        ),
    )
    description = forms.CharField(
        label=_("Description"),
        max_length=2000,
        required=False,
        widget=forms.Textarea,
        help_text=_(
            "Longer description of the package (the initial text comes from the research "
            "project's abstract)"
        ),
    )

    def __init__(self, *args, **kwargs):
        project = kwargs.pop("project")
        super().__init__(*args, **kwargs)
        keywords_project = project.research_project.keywords.all()
        self.fields["keywords"].initial = keywords_project
        self.fields["name"].initial = project.research_project.acronym.lower()
        self.fields["title"].initial = project.research_project.name
        self.fields["description"].initial = project.research_project.abstract
        self.fields["private_species"].queryset = project.classificator.species.all()

    def get_layout(self):
        """"""
        return Layout(
            Fieldset(
                "",
                "export_format",
                "name",
                "title",
                "version",
                "licences",
                "keywords",
                "approved_only",
                "all_dep",
                "filter_dep",
                "trapper_url_token",
                "include_events",
                "private_human",
                "private_vehicle",
                "private_species",
                "include_ids",
                "description",
            ),
        )


class ClassificationPublishForm(BaseCrispyForm):
    """
    TODO: docstrings
    """

    DATA_HUB_CHOICES = (("Dataverse", "Dataverse"), ("Zenodo", "Zenodo"))
    data_package = forms.ModelChoiceField(
        queryset=UserDataPackage.objects.none(),
        help_text=_("Choose a data package you want to publish."),
    )
    data_hub = forms.ChoiceField(
        choices=DATA_HUB_CHOICES,
        label=_("Data hub type"),
        required=True,
        initial="Dataverse",
    )
    host_url = forms.URLField(
        label=_("Data hub url"),
        help_text=_(
            "Url of the data hub instance. Required for Dataverse. For Zenodo leave empty to use "
            "default (https://zenodo.org/api) or provide API url."
        ),
        required=False,
    )
    api_token = forms.CharField(
        label=_("API token"),
        required=True,
        widget=forms.PasswordInput,
        help_text=_("Data hub API token."),
    )
    container = forms.CharField(
        label=_("Container"),
        required=False,
        help_text=_(
            "Data hub container/directory/account e.g. in Dataverse platform these containers "
            'are called "dataverses". Only required for Dataverse.'
        ),
    )
    secure_connection = forms.BooleanField(
        label=_("Verify SSL connection"),
        initial=True,
        required=False,
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = get_current_user()
        self.fields["data_package"].queryset = UserDataPackage.objects.filter(
            user=self.user, package_type=PackageType.CLASSIFICATION_RESULTS
        )
        self.fields[
            "data_package"
        ].label_from_instance = lambda obj: f"{obj.filename()}"

    def get_layout(self):
        """"""
        return Layout(
            Fieldset(
                "",
                "data_package",
                "data_hub",
                "host_url",
                "api_token",
                "container",
                "secure_connection",
            ),
        )

    def clean(self):
        cleaned_data = super().clean()
        data_hub = cleaned_data["data_hub"]
        api_token = cleaned_data.get("api_token")
        # test connection with a data hub
        if data_hub == "Dataverse":
            host_url = cleaned_data.get("host_url")
            if not host_url:
                errors = _("Field 'host_url' is required for Dataverse upload")
                raise forms.ValidationError(errors)

            dataverse = cleaned_data.get("container")
            if not dataverse:
                errors = _("Field 'container' is required for Dataverse upload")
                raise forms.ValidationError(errors)

            url = f"{host_url}/api/dataverses/{dataverse}"
            try:
                res = requests.get(
                    url,
                    params={"key": api_token},
                    verify=cleaned_data.get("secure_connection"),
                )
            except requests.exceptions.SSLError:
                errors = _(
                    "Certificate verify failed: unable to get local issuer certificate"
                )
                raise forms.ValidationError(errors)
            if not res.status_code == 200:
                msg = res.json()["message"]
                errors = _(f"Connection error: {msg}")
                raise forms.ValidationError(errors)

        elif data_hub == "Zenodo":
            host_url = cleaned_data.get("host_url", "https://zenodo.org/api")
            deposition_url = f"{host_url}/deposit/depositions"
            params = {"access_token": api_token}

            # first create deposition at zenodo to get upload links
            res = requests.post(deposition_url, params=params, json={})
            if res.status_code != 201:
                errors = res.json()["message"]
                raise forms.ValidationError(errors)

            res_json = res.json()
            cleaned_data["host_url"] = res_json["links"]["bucket"]
            cleaned_data["metadata_url"] = res_json["links"]["self"]

        return cleaned_data


class SequenceBuildForm(BaseCrispyForm):
    """
    TODO: docstrings
    """

    time_interval = forms.IntegerField(
        initial=5,
        required=True,
        help_text=_(
            "The time interval (minutes) that will be used to automatically group "
            "resources into sequences."
        ),
    )
    deployments = forms.BooleanField(
        initial=True,
        required=False,
        label=_("Aggregate by deployments"),
        help_text=_("If checked resources will be aggregated by deployments."),
    )
    collections_pks = forms.CharField(
        widget=forms.HiddenInput(),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = get_current_user()

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset("", "time_interval", "deployments", "collections_pks"),
        )

    def clean_collections_pks(self):
        collections_pks = self.cleaned_data.pop("collections_pks", None)
        if collections_pks:
            pks_list = parse_pks(collections_pks)
            collections = ClassificationProjectCollection.objects.filter(
                Q(pk__in=pks_list),
                Q(project__owner=self.user)
                | Q(
                    project__classification_project_roles__user=self.user,
                    project__classification_project_roles__name=ClassificationProjectRoleLevels.ADMIN,
                ),
            )
            if collections:
                self.cleaned_data["project_collections"] = collections


class CollectionResourceAppendForm(BaseCrispyForm):
    """
    TODO: docstrings
    """

    collection = forms.ModelChoiceField(
        queryset=Collection.objects.none(),
        help_text=_("To which collection you want to add selected resources?"),
    )
    resources_pks = forms.CharField(
        widget=forms.HiddenInput(),
    )
    select2_ajax_fields = (("collection", "/storage/api/collections", "pk", "name"),)

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset("", "collection", "resources_pks"),
        )

    def clean_resources_pks(self):
        resources_pks = self.cleaned_data.pop("resources_pks", None)
        collection = self.cleaned_data.get("collection", None)

        if collection and resources_pks:
            # this pks_list is in fact the list with pks of classifications (!)
            pks_list = parse_pks(resources_pks)
            # get proper pks for corresponding resources
            pks_list = Classification.objects.filter(pk__in=pks_list).values_list(
                "resource__pk", flat=True
            )
            # get only accessible resources
            resources = Resource.objects.get_accessible().filter(pk__in=pks_list)
            new_resources = resources.exclude(
                pk__in=collection.resources.values_list("pk", flat=True)
            )

            if not collection.status == "Private":
                n = resources.exclude(
                    Q(owner=self.user) | Q(managers=self.user)
                ).count()
                if n != 0:
                    error = mark_safe(
                        _(
                            "You have no permission to add some of the selected resources "
                            f'to this "{collection.status.lower()}" collection.'
                        )
                    )
                    self.cleaned_data["error"] = error

            if resources:
                if not new_resources:
                    error = mark_safe(
                        _("All selected resources are already in this collection.")
                    )
                    self.cleaned_data["error"] = error
                else:
                    self.cleaned_data["new_resources"] = new_resources
            else:
                error = mark_safe(
                    _(
                        "Nothing to process (most probably you have no permission "
                        "to run this action on the selected resources)."
                    )
                )
                self.cleaned_data["error"] = error

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["collection"].widget = forms.CharField.widget()
        self.user = get_current_user()
        self.fields["collection"].queryset = Collection.objects.get_editable(
            user=self.user
        )


class ClassifyAIForm(BaseCrispyForm):
    """
    TODO: docstrings
    """

    ai_model = forms.ModelChoiceField(
        queryset=AIProvider.objects.none(), help_text=_("AI model"), required=True
    )
    classifications_pks = forms.CharField(widget=forms.HiddenInput(), required=False)
    classify_preview_files = forms.BooleanField(
        required=False,
        initial=False,
        help_text=_(
            "Classify smaller preview files instead of larger original images."
        ),
    )

    def __init__(self, *args, **kwargs):
        self.project_pk = kwargs.pop("project_pk")
        self.classification_class = kwargs.pop("classification_class")
        super().__init__(*args, **kwargs)
        # ExternalAIProvider is not supported, as it is only supposed to be used for importing AIClassifications from
        # external sources
        self.fields[
            "ai_model"
        ].queryset = AIProvider.objects.get_accessible().not_instance_of(
            ExternalAIProvider
        )

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset("", "ai_model", "classify_preview_files", "classifications_pks"),
        )

    def clean(self):
        cleaned_data = super().clean()
        classifications_pks = cleaned_data.pop("classifications_pks", None)
        ai_model = cleaned_data.get("ai_model", None)

        if not ai_model:
            raise forms.ValidationError("ai_model is required")

        if classifications_pks:
            pks_list = parse_pks(classifications_pks)

            # self.classification_class is set in ClassifyAIView
            if self.classification_class is AIClassification:
                classifications = Classification.objects.filter(
                    project__pk=self.project_pk, ai_classifications__in=pks_list
                )
            else:
                classifications = Classification.objects.filter(
                    project__pk=self.project_pk, pk__in=pks_list
                )
            if not ai_model.video_support:
                classifications = classifications.filter(resource__resource_type="I")
            cleaned_data["classifications"] = classifications
        return cleaned_data


class BaseAIClassificationApproveForm(BaseCrispyForm):
    minimum_confidence = forms.FloatField(
        required=True,
        initial=0.0,
        min_value=0.0,
        max_value=1.0,
        help_text=_(
            "Between 0.0 and 1.0. Observations with lower confidence will not be copied "
        ),
    )

    mark_as_approved = forms.BooleanField(
        required=False,
        initial=False,
        help_text=_(
            "Select if you want to mark classifications as approved, or only copy classified attributes"
        ),
    )

    overwrite_attrs = forms.BooleanField(
        required=False,
        initial=False,
        label=_("Overwrite attributes"),
        help_text=_(
            "Select if you're marking AI classifications as approved and want to overwrite any attributes. "
            "Only flags regarding classification's approval will be changed otherwise"
        ),
    )

    additional_fields: List[str]

    def get_possible_fields_to_copy(self):
        return [
            k
            for k in (
                self.static_standard_fields
                + self.dynamic_standard_fields
                + ["bounding_boxes"]
            )
            if k in AIClassificationSettings.AI_ATTRIBUTES
        ] + ["bounding_boxes"]

    def get_layout(self):
        form_layout = layout.Layout(
            "minimum_confidence",
            "mark_as_approved",
            "overwrite_attrs",
            layout.Fieldset(
                _("Attributes to copy"),
                *self.get_possible_fields_to_copy(),
                style="padding-bottom: 20px;",
            ),
            *self.additional_fields,
        )
        return form_layout

    def __init__(self, *args, **kwargs):
        self.additional_fields = kwargs.pop("additional_fields", [])
        classificator = self.project.classificator
        self.static_standard_fields = classificator.active_standard_attrs("STATIC")
        self.dynamic_standard_fields = classificator.active_standard_attrs("DYNAMIC")

        super().__init__(*args, **kwargs)
        self.fields["minimum_confidence"].initial = getattr(
            self.project.default_ai_model, "minimum_confidence", 0.5
        )
        for field_name in self.get_possible_fields_to_copy():
            self.fields[field_name] = forms.BooleanField(
                required=False,
                initial=True,
            )

    def clean(self):
        cleaned_data = super().clean()
        fields_to_copy = []
        for field in self.get_possible_fields_to_copy():
            val = cleaned_data.pop(field)
            if val:
                fields_to_copy.append(field)

        cleaned_data["fields_to_copy"] = fields_to_copy
        overwrite_attrs = cleaned_data.get("overwrite_attrs", False)
        cleaned_data["overwrite_attrs"] = overwrite_attrs

        if overwrite_attrs:
            if not fields_to_copy:
                cleaned_data["error"] = _(
                    "Please select at least one attribute to copy"
                )
        else:  # overwrite_attrs = False
            if fields_to_copy:
                cleaned_data["error"] = _(
                    "Please check the 'Overwrite attributes' flag to overwrite attributes"
                )

        return cleaned_data


class BulkApproveAIClassificationForm(BaseAIClassificationApproveForm):
    ai_classification_pks = forms.CharField(widget=forms.HiddenInput(), required=False)

    def __init__(self, *args, **kwargs):
        self.project = kwargs.pop("project")
        super().__init__(*args, **kwargs, additional_fields=["ai_classification_pks"])

    def clean(self):
        cleaned_data = super().clean()
        ai_classification_pks = cleaned_data.pop("ai_classification_pks", None)
        if ai_classification_pks:
            pks_list = parse_pks(ai_classification_pks)
            ai_classification_pks = AIClassification.objects.filter(
                classification__project=self.project, pk__in=pks_list
            ).values_list("pk", flat=True)

            if ai_classification_pks.count() == 0:
                cleaned_data["error"] = _(
                    "No items to approve (most probably you don't have permissions to do that)."
                )
                return

            cleaned_data["ai_classification_pks"] = list(ai_classification_pks)
        else:
            cleaned_data["error"] = _("Please select at least one AI Classification")
            return
        return cleaned_data


class CopyBboxesFromAIForm(BaseCrispyForm):
    ai_classification_pks = forms.CharField(widget=forms.HiddenInput(), required=False)
    observation_type = forms.BooleanField(disabled=True, initial=True, required=False)
    species = forms.BooleanField(disabled=True, initial=True, required=False)
    sex = forms.BooleanField(required=False)
    age = forms.BooleanField(required=False)
    overwrite_bboxes = forms.BooleanField(
        required=False,
        help_text=_(
            "Do you want to overwrite existing bounding boxes for already approved classifications?"
        ),
    )

    def __init__(self, *args, **kwargs):
        self.project = kwargs.pop("project")
        super().__init__(*args, **kwargs)

    def get_layout(self):
        return Layout(
            Fieldset(
                "",
                "observation_type",
                "species",
                "sex",
                "age",
                HTML(_("Other options:")),
                "overwrite_bboxes",
                "ai_classification_pks",
            )
        )

    def clean(self):
        cleaned_data = super().clean()

        # clean and prepare AIClassification pks
        ai_classification_pks = cleaned_data.pop("ai_classification_pks", None)
        if ai_classification_pks:
            pks_list = parse_pks(ai_classification_pks)
            ai_classification_pks = AIClassification.objects.filter(
                classification__project=self.project, pk__in=pks_list
            ).values_list("pk", flat=True)

            if ai_classification_pks.count() == 0:
                cleaned_data["error"] = _(
                    "No items to approve (most probably you don't have permissions to do that)."
                )
                return
            cleaned_data["ai_classification_pks"] = list(ai_classification_pks)
        else:
            cleaned_data["error"] = _("Please select at least one AI Classification")

        # clean and prepare match_fields
        match_fields = ["observation_type", "species_id"]
        form_fields = ["sex", "age"]
        for f in form_fields:
            if cleaned_data.get(f):
                match_fields.append(f)

        cleaned_data["match_fields"] = match_fields

        return cleaned_data


class ApproveAIClassificationForm(BaseAIClassificationApproveForm):
    ai_classification: AIClassification

    def __init__(self, *args, **kwargs):
        self.ai_classification = kwargs.pop("ai_classification")
        self.project = self.ai_classification.classification.project
        super().__init__(*args, **kwargs)


class ClassifyBboxForm(BaseCrispyForm):
    """
    TODO: docstrings
    """

    bboxes = forms.CharField(widget=forms.HiddenInput(), required=False)

    def clean_bboxes(self):
        bboxes = self.cleaned_data.get("bboxes")
        if not bboxes:
            return []
        try:
            bboxes = json.loads(bboxes)
        except (ValueError, TypeError):
            raise forms.ValidationError("Invalid data or there is nothing to submit.")
        return bboxes

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = get_current_user()
