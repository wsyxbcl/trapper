# -*- coding: utf-8 -*-
"""Filters used in media classification application when backend is used to
limit data"""
import django_filters
from django import forms
from django.db.models import Q, F
from django.shortcuts import get_object_or_404

from trapper.apps.extra_tables.models import Species
from trapper.apps.geomap.models import Deployment
from trapper.apps.storage.taxonomy import ResourceType
from trapper.apps.media_classification.models import (
    Classification,
    Classificator,
    ClassificationProjectCollection,
    UserClassification,
    ClassificationProject,
    Sequence,
    AIClassification,
    AIProvider,
)
from trapper.apps.media_classification.taxonomy import (
    ClassificationProjectRoleLevels,
    ObservationType,
    SpeciesAge,
    SpeciesSex,
)
from trapper.apps.common.tools import parse_pks
from trapper.apps.common.filters import (
    BaseFilterSet,
    BaseOwnBooleanFilter,
    BaseDateFilter,
    BaseTimeFilter,
    BaseLocationsMapFilter,
)


class ClassificationProjectFilter(BaseFilterSet):
    """Filter for
    :class:`apps.media_classification.models.ClassificationProject` model
    when backend filtering is used
    """

    owner = BaseOwnBooleanFilter(
        managers=False,
        role_levels=ClassificationProjectRoleLevels.ANY,
        role_field="classification_project_roles",
    )
    research_project = django_filters.Filter(field_name="research_project")
    status = django_filters.Filter(field_name="status")

    class Meta:
        model = ClassificationProject
        fields = ["owner", "status", "research_project"]


class JSONAttrsFilter(django_filters.filters.Filter):
    """Filter field used for simple filtering of json values"""

    field_class = forms.CharField

    def filter(self, qs, value):
        if value:
            if isinstance(value, str):
                if value == "True":
                    value = True
                elif value == "False":
                    value = False

            s = {f"static_attrs__{self.field_name}": value}
            d = {f"dynamic_attrs__attrs__{self.field_name}": value}

            return self.get_method(qs)(Q(**s) | Q(**d)).distinct()
        return qs


class ClassificationFilter(BaseFilterSet):
    """Filter for
    :class:`apps.media_classification.models.Classification` model
    when backend filtering is used
    """

    project = django_filters.Filter(field_name="project__pk")
    owner = BaseOwnBooleanFilter(
        owner_field="resource__owner",
        managers_field="resource__managers",
        managers=True,
    )
    deployment = django_filters.MultipleChoiceFilter(field_name="resource__deployment")
    collection = django_filters.MultipleChoiceFilter(field_name="collection")
    locations_map = BaseLocationsMapFilter(field_name="resource__deployment__location")
    status = django_filters.BooleanFilter()
    status_ai = django_filters.BooleanFilter()
    rdate_from = BaseDateFilter(
        field_name="resource__date_recorded__date", lookup_expr=("gte")
    )
    rdate_to = BaseDateFilter(
        field_name="resource__date_recorded__date", lookup_expr=("lte")
    )
    rtime_from = BaseTimeFilter(
        field_name="resource__date_recorded", lookup_expr="gte", time_format="%H:%M"
    )
    rtime_to = BaseTimeFilter(
        field_name="resource__date_recorded",
        lookup_expr="lte",
        time_format="%H:%M",
    )
    ftype = django_filters.ChoiceFilter(
        field_name="resource__resource_type", choices=ResourceType.get_all_choices()
    )
    classified = django_filters.CharFilter(method="get_classified")
    classified_ai = django_filters.CharFilter(method="get_classified_ai")
    bboxes = django_filters.CharFilter(method="has_bboxes")
    # standard attributes
    species = django_filters.ModelMultipleChoiceFilter(
        field_name="dynamic_attrs__species", queryset=Species.objects.all()
    )
    observation_type = django_filters.ChoiceFilter(
        field_name="dynamic_attrs__observation_type",
        choices=ObservationType.get_all_choices(),
        distinct=True,
    )
    sex = django_filters.ChoiceFilter(
        field_name="dynamic_attrs__sex",
        choices=SpeciesSex.get_all_choices(),
        distinct=True,
    )
    age = django_filters.ChoiceFilter(
        field_name="dynamic_attrs__age",
        choices=SpeciesAge.get_all_choices(),
        distinct=True,
    )

    class Meta:
        model = Classification
        exclude = [
            "created_at",
            "updated_at",
            "updated_by",
            "approved_by",
            "approved_at",
            "approved_source",
            "static_attrs",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        project_pk = self.data.get("project", None)
        if project_pk:
            project = get_object_or_404(ClassificationProject, pk=project_pk)
            if project.classificator:
                for a in project.classificator.custom_attrs.keys():
                    if self.data.get(a):
                        self.filters[a] = JSONAttrsFilter(field_name=a)
        # set choices for multiple choice filters
        if self.data.get("collection", None):
            self.filters[
                "collection"
            ].field.choices = ClassificationProjectCollection.objects.filter(
                project__pk=project_pk
            ).values_list(
                "pk", "collection__collection__name"
            )
        if self.data.get("deployment", None):
            # for filtering retrieve all deployments in the ClassificationProject
            if project_pk:
                deployment_pks = set(
                    project.collections.values_list(
                        "collection__resources__deployment", flat=True
                    )
                )
                deployment_qs = Deployment.objects.filter(pk__in=deployment_pks)
                self.filters["deployment"].field.choices = set(
                    deployment_qs.values_list("pk", "deployment_id")
                )

    def get_classified(self, qs, name, value):
        value = value not in ["True", "true"]
        return qs.filter(user_classifications__isnull=value).distinct()

    def get_classified_ai(self, qs, name, value):
        value = value not in ["True", "true"]
        return qs.filter(ai_classifications__isnull=value)

    def has_bboxes(self, qs, name, value):
        value = value in ["True", "true"]
        return qs.filter(has_bboxes=value)


class UserClassificationFilter(BaseFilterSet):
    """Filter for
    :class:`apps.media_classification.models.UserClassification` model
    when backend filtering is used
    """

    project = django_filters.Filter(field_name="classification__project__pk")
    user = django_filters.MultipleChoiceFilter(field_name="owner")
    owner = BaseOwnBooleanFilter(managers=False)
    deployment = django_filters.MultipleChoiceFilter(
        field_name="classification__resource__deployment"
    )
    collection = django_filters.MultipleChoiceFilter(
        field_name="classification__collection"
    )
    species = django_filters.ModelMultipleChoiceFilter(
        field_name="dynamic_attrs__species", queryset=Species.objects.all()
    )
    ftype = django_filters.ChoiceFilter(
        field_name="classification__resource__resource_type",
        choices=ResourceType.get_all_choices(),
    )
    approved = django_filters.CharFilter(method="filter_approved")
    bboxes = django_filters.CharFilter(method="has_bboxes")
    observation_type = django_filters.ChoiceFilter(
        field_name="dynamic_attrs__observation_type",
        choices=ObservationType.get_all_choices(),
        distinct=True,
    )
    locations_map = BaseLocationsMapFilter(
        field_name="classification__resource__deployment__location"
    )
    rdate_from = BaseDateFilter(
        field_name="classification__resource__date_recorded", lookup_expr=("gte")
    )
    rdate_to = BaseDateFilter(
        field_name="classification__resource__date_recorded", lookup_expr=("lte")
    )
    rtime_from = BaseTimeFilter(
        time_format="%H:%M",
        field_name="classification__resource__date_recorded",
        lookup_expr="from",
    )
    rtime_to = BaseTimeFilter(
        time_format="%H:%M",
        field_name="classification__resource__date_recorded",
        lookup_expr="to",
    )

    class Meta:
        model = UserClassification
        exclude = ["created_at", "updated_at", "updated_by", "static_attrs"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        project_pk = self.data.get("project", None)
        # set choices for multiple choice filters
        if self.data.get("user", None):
            self.filters["user"].field.choices = (
                self.queryset.order_by("owner__username")
                .values_list("owner__pk", "owner__username")
                .distinct()
            )
        if self.data.get("collection", None):
            self.filters[
                "collection"
            ].field.choices = ClassificationProjectCollection.objects.filter(
                project__pk=project_pk
            ).values_list(
                "pk", "collection__collection__name"
            )
        if self.data.get("deployment", None):
            if project_pk is not None:
                project = ClassificationProject.objects.get(pk=project_pk)
                # for filtering retrieve all deployments in the ClassificationProject
                deployment_pks = set(
                    project.collections.values_list(
                        "collection__resources__deployment", flat=True
                    )
                )
                deployment_qs = Deployment.objects.filter(pk__in=deployment_pks)
                self.filters["deployment"].field.choices = set(
                    deployment_qs.values_list("pk", "deployment_id")
                )

    def filter_approved(self, qs, name, value):
        if value in ["True", "true"]:
            value = True
        elif value in ["False", "false"]:
            value = False
        else:
            return qs
        return qs.exclude(userclassification_approved__isnull=value)

    def has_bboxes(self, qs, name, value):
        value = value in ["True", "true"]
        return qs.filter(has_bboxes=value)


class ClassificatorFilter(BaseFilterSet):
    """Filter for
    :class:`apps.media_classification.models.Classificator` model
    when backend filtering is used
    """

    owner = BaseOwnBooleanFilter(managers=False)
    owners = django_filters.MultipleChoiceFilter(field_name="owner")
    udate_from = BaseDateFilter(field_name="updated_date", lookup_expr=("gte"))
    udate_to = BaseDateFilter(field_name="updated_date", lookup_expr=("lte"))

    class Meta:
        model = Classificator
        exclude = [
            "dynamic_attrs_order",
            "static_attrs_order",
            "updated_date",
            "created_date",
            "description",
            "copy_of",
            "disabled_at",
            "disabled_by",
            "updated",
            "custom_attrs",
            "predefined_attrs",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # set choices for multiple choice filters
        self.filters["owners"].field.choices = self.queryset.values_list(
            "owner__pk", "owner__username"
        ).distinct()


class ClassificationProjectCollectionFilter(BaseFilterSet):
    """Filter for
    :class:`apps.media_classification.models.ClassificationProjectCollection`
    model when backend filtering is used
    """

    class Meta:
        model = ClassificationProjectCollection
        fields = ["project", "owner", "status"]

    owner = BaseOwnBooleanFilter(
        owner_field="collection__collection__owner",
        managers_field="collection__collection__managers",
    )
    status = django_filters.Filter(field_name="collection__collection__status")


class SequenceFilter(BaseFilterSet):
    """Filter for
    :class:`apps.media_classification.models.Sequence` model when backend
    filtering is used
    """

    deployment = django_filters.CharFilter(method="filter_deployment")

    class Meta:
        model = Sequence
        exclude = ["description", "created_at", "created_by"]

    def filter_deployment(self, qs, name, value):
        pks = parse_pks(value)
        if not pks:
            return qs
        return qs.filter(resources__deployment__pk__in=pks).distinct()


class AIClassificationFilter(BaseFilterSet):
    """Filter for
    :class:`apps.media_classification.models.AIClassification` model
    when backend filtering is used
    """

    project = django_filters.Filter(field_name="classification__project__pk")
    deployment = django_filters.MultipleChoiceFilter(
        field_name="classification__resource__deployment"
    )
    collection = django_filters.MultipleChoiceFilter(
        field_name="classification__collection"
    )
    ftype = django_filters.ChoiceFilter(
        field_name="classification__resource__resource_type",
        choices=ResourceType.get_all_choices(),
    )
    species = django_filters.ModelMultipleChoiceFilter(
        field_name="dynamic_attrs__species", queryset=Species.objects.all()
    )
    observation_type = django_filters.ChoiceFilter(
        field_name="dynamic_attrs__observation_type",
        choices=ObservationType.get_all_choices(),
        distinct=True,
    )
    bboxes = django_filters.CharFilter(method="has_bboxes")
    approved = django_filters.CharFilter(method="filter_approved")
    confidence = django_filters.NumberFilter(
        field_name="dynamic_attrs__classification_confidence",
        lookup_expr="gte",
        distinct=True,
    )
    ai_provider = django_filters.MultipleChoiceFilter(field_name="model")

    class Meta:
        model = AIClassification
        fields = [
            "ftype",
            "bboxes",
            "observation_type",
            "species",
            "collection",
            "deployment",
            "confidence",
            "ai_provider",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        project_pk = self.data.get("project", None)
        if project_pk:
            project = get_object_or_404(ClassificationProject, pk=project_pk)
            if project.classificator:
                for a in project.classificator.custom_attrs.keys():
                    if self.data.get(a):
                        self.filters[a] = JSONAttrsFilter(field_name=a)
        # set choices for multiple choice filters
        if self.data.get("collection", None):
            self.filters[
                "collection"
            ].field.choices = ClassificationProjectCollection.objects.filter(
                project__pk=project_pk
            ).values_list(
                "pk", "collection__collection__name"
            )
        if self.data.get("deployment", None):
            if project_pk is not None:
                project = ClassificationProject.objects.get(pk=project_pk)
                # for filtering retrieve all deployments in the ClassificationProject
                deployment_pks = set(
                    project.collections.values_list(
                        "collection__resources__deployment", flat=True
                    )
                )
                deployment_qs = Deployment.objects.filter(pk__in=deployment_pks)
                self.filters["deployment"].field.choices = set(
                    deployment_qs.values_list("pk", "deployment_id")
                )

        if self.data.get("ai_provider"):
            self.filters[
                "ai_provider"
            ].field.choices = AIProvider.objects.all().values_list("pk", "name")

    def get_classified(self, qs, name, value):
        value = value not in ["True", "true"]
        return qs.filter(user_classifications__isnull=value)

    def get_classified_ai(self, qs, name, value):
        value = value not in ["True", "true"]
        return qs.filter(ai_classifications__isnull=value)

    def has_bboxes(self, qs, name, value):
        value = value in ["True", "true"]
        return qs.filter(has_bboxes=value)

    def filter_approved(self, qs, name, value):
        if value in ["True", "true"]:
            return qs.filter(classification__approved_source_ai=F("id"))
        elif value in ["False", "false"]:
            return qs.exclude(classification__approved_source_ai=F("id"))
        else:
            return qs
