# -*- coding: utf-8 -*-

import os
from unittest.mock import Mock, patch

import pytz
from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse, reverse_lazy
from django.utils.lorem_ipsum import words
from django.utils.timezone import get_default_timezone_name
from rest_framework.authtoken.models import Token

from trapper.apps.accounts.admin import TrapperUserAdmin
from trapper.apps.accounts.models import UserProfile, active_notifiction, UserRemoteTask
from trapper.apps.accounts.taxonomy import ExternalStorageSettings, UserRemoteTaskStatus
from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.common.utils.identity import create_hashcode
from trapper.apps.common.utils.test_tools import ExtendedTestCase

User = get_user_model()


class UserRegistrationTestCase(ExtendedTestCase):
    """Tests related to user registration"""

    def setUp(self):
        super(UserRegistrationTestCase, self).setUp()
        self.email = "amanda@somedomain.com"
        self.passwd = "amanda"
        os.environ["RECAPTCHA_TESTING"] = "True"

    def tearDown(self):
        del os.environ["RECAPTCHA_TESTING"]
        super(UserRegistrationTestCase, self).tearDown()

    def register_user(self):
        """Handler for registering user using signup"""
        url = reverse("account_signup")

        response = self.client.post(
            url,
            {
                "email": self.email,
                "password1": self.passwd,
                "password2": self.passwd,
                "g-recaptcha-response": "PASSED",
            },
        )
        return response

    def activate_user(self, email):
        user = User.objects.get(email=email)
        user.is_active = True
        user.save()
        return user

    def test_register_account_inactive(self):
        """Newly registered user should be inactive"""
        self.assertFalse(User.objects.filter(email=self.email).exists())

        response = self.register_user()

        # redirect to inactive page
        self.assertEqual(response.status_code, 302)

        user = User.objects.get(email=self.email)
        self.assertFalse(user.is_active)

    # TODO: Fails before updating packages
    # def test_register_account_email_notification(self):
    #     """Registration of new user sends two emails, to admins and to user"""
    #
    #     self.register_user()
    #
    #     self.assertEqual(len(self.mail.outbox), 2)
    #
    #     # First email is sent to user
    #     user_email = self.mail.outbox[0].to[0]
    #
    #     self.assertEqual(user_email, self.email)
    #
    #     # Second is sent to all admins
    #     admin_emails = self.mail.outbox[1].to
    #
    #     for name, email in settings.ADMINS:
    #         self.assertIn(
    #             email,
    #             admin_emails,
    #             u"{name} is not in email receivers".format(name=name)
    #         )

    # TODO: Fails before updating packages
    # def test_register_account_activate(self):
    #     """Account activation sends email to user"""
    #
    #     self.register_user()
    #     self.mail.outbox = []
    #
    #     self.activate_user(email=self.email)
    #
    #     self.assertEqual(len(self.mail.outbox), 1)
    #     self.assertEqual(self.mail.outbox[0].to[0], self.email)

    def test_register_external_dirs(self):
        """Account activation creates new directories for external media"""

        self.register_user()

        user = self.activate_user(email=self.email)
        for directory in ExternalStorageSettings.DIRS:
            self.assertTrue(
                os.path.exists(
                    os.path.join(settings.EXTERNAL_MEDIA_ROOT, user.username, directory)
                )
            )


class UserProfileTestCase(ExtendedTestCase):
    """Tests related to user profile editing"""

    def setUp(self):
        super(UserProfileTestCase, self).setUp()
        self.email = "amanda@somedomain.com"
        self.passwd = "amanda"

        self.user = User.objects.create_user(
            username="amanda",
            email=self.email,
            password=self.passwd,
        )

        self.profile_url = reverse("accounts:mine_profile")

    def test_profile_change(self):
        """Update user profile"""
        logged_in = self.client.login(username=self.user.username, password=self.passwd)

        self.assertTrue(logged_in)

        first_name = "Amanda"
        last_name = "Amandasia"
        institution = "Hogwart"

        about_me = words(5)

        self.client.post(
            self.profile_url,
            data={
                "first_name": first_name,
                "last_name": last_name,
                "institution": institution,
                "about_me": about_me,
                "update-profile": "",
            },
        )

        user = User.objects.get(pk=self.user.pk)
        profile = user.userprofile

        self.assertEqual(user.first_name, first_name)
        self.assertEqual(user.last_name, last_name)
        self.assertEqual(profile.institution, institution)
        self.assertEqual(profile.about_me, about_me)

    def test_user_change_password(self):
        """
        After password has been changed user can authenticate with new one
        """

        # Auth with base passwd
        logged_in = self.client.login(username=self.user.username, password=self.passwd)
        self.assertTrue(logged_in)

        new_passwd = create_hashcode()
        self.client.post(
            self.profile_url,
            data={
                "old_password": self.passwd,
                "new_password1": new_passwd,
                "new_password2": new_passwd,
                "change-password": "",
            },
        )

        # After change passwd, old one stops working
        self.client.logout()
        logged_in = self.client.login(username=self.user.username, password=self.passwd)
        self.assertFalse(logged_in)

        # but new does
        logged_in = self.client.login(username=self.user.username, password=new_passwd)
        self.assertTrue(logged_in)

    def test_timezone_change(self):
        """
        Test updating user's working timezone using profile form
        """

        logged_in = self.client.login(username=self.user.username, password=self.passwd)
        self.assertTrue(logged_in)

        initial_tz_name = get_default_timezone_name()
        user_profile = self.user.userprofile
        self.assertEqual(pytz.timezone(initial_tz_name), user_profile.timezone)

        new_tz_name = "America/Los_Angeles"
        self.client.post(
            self.profile_url,
            data={
                "timezone": new_tz_name,
                "set-timezone": "",
            },
        )
        user = User.objects.get(pk=self.user.pk)
        self.assertEqual(pytz.timezone(new_tz_name), user.userprofile.timezone)


class UserAdminTestCase(TestCase):
    """Tests related to user django admin page"""

    @classmethod
    def setUpTestData(cls):
        super(UserAdminTestCase, cls).setUpTestData()
        cls.user1 = UserFactory()
        cls.user2 = UserFactory()

        UserProfile.objects.get_or_create(user=cls.user1)
        cls.user_profile = UserProfile.objects.all().first()

        UserRemoteTask.objects.get_or_create(
            name="name_test",
            user=cls.user1,
            task_id="4643b59e-2d12-452e-b703-597a969e2a09",
            status=UserRemoteTaskStatus.SUCCESS,
        )

        cls.user_remote_task = UserRemoteTask.objects.all().first()

    def test_has_ftp(self):
        item = Mock()
        item.userprofile = Mock()
        item.userprofile.has_ftp_account = True

        admin_site = Mock()
        admin_site.admin_view = Mock(return_value=Mock(return_value=True))
        trapper_user_admin = TrapperUserAdmin(User, admin_site)

        self.assertTrue(trapper_user_admin.has_ftp(item=item))

    def test_main_index(self):
        self.client.get(reverse_lazy("trapper_index"))
        self.client.force_login(self.user1)
        self.client.get(reverse_lazy("trapper_index"))

    def test_mine_profile(self):
        self.client.get(reverse_lazy("accounts:mine_profile"))
        self.client.post(reverse_lazy("accounts:mine_profile"))
        self.client.force_login(self.user1)
        self.client.get(reverse_lazy("accounts:mine_profile"))
        self.client.post(reverse_lazy("accounts:mine_profile"))
        self.client.post(
            reverse_lazy("accounts:mine_profile"), data={"update-profile": True}
        )
        self.client.post(
            reverse_lazy("accounts:mine_profile"),
            data={"update-profile": True, "first_name": "a" * 100},
        )
        self.client.post(
            reverse_lazy("accounts:mine_profile"),
            data={
                "change-password": True,
                "old_password": "a" * 10,
                "new_password1": "b" * 10,
                "new_password2": "b" * 10,
            },
        )
        self.client.post(
            reverse_lazy("accounts:mine_profile"),
            data={"set-timezone": True, "timezone": pytz.UTC},
        )
        self.client.post(
            reverse_lazy("accounts:mine_profile"),
            data={"set-timezone": True, "timezone": "tz"},
        )

    def test_user_profile(self):
        self.client.get(
            reverse("accounts:show_profile", kwargs={"username": self.user1.username})
        )
        self.client.force_login(self.user1)
        self.client.get(
            reverse("accounts:show_profile", kwargs={"username": self.user1.username})
        )

    def test_dashboard(self):
        self.client.get(reverse_lazy("accounts:dashboard"))
        self.client.force_login(self.user1)
        self.client.get(reverse_lazy("accounts:dashboard"))

    def test_celery_task_cancel(self):
        self.client.post(
            reverse_lazy("accounts:celery_task_cancel"), data={"task_id": 0}
        )
        self.client.force_login(self.user1)
        self.client.post(
            reverse_lazy("accounts:celery_task_cancel"), data={"task_id": 0}
        )

    def test_data_package_delete(self):
        self.client.get(reverse("accounts:data_package_delete", kwargs={"pk": 0}))
        self.client.force_login(self.user1)
        self.client.get(reverse("accounts:data_package_delete", kwargs={"pk": 0}))

    def test_user_profile_avatar_url(self):
        self.user_profile.avatar_url

    def test_user_profile_has_unread_messages(self):
        self.user_profile.has_unread_messages()

    def test_active_notifiction(self):
        active_notifiction("", self.user1)

    def test_user_remote_task_str(self):
        print(self.user_remote_task)


class UserAuthTokenTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user = UserFactory()
        cls.new_token_url = reverse("accounts:new_auth_token")

    def test_auth_token(self):
        # at first, there should not be a token for the new user
        self.assertFalse(Token.objects.filter(user=self.user).exists())

        self.client.force_login(self.user)
        self.client.post(self.new_token_url)
        # token for user should be generated
        self.assertTrue(Token.objects.filter(user=self.user).exists())

        old_token = Token.objects.get(user=self.user)
        old_token_key = old_token.key
        old_token_pk = old_token.pk

        self.client.post(self.new_token_url)
        new_token = Token.objects.filter(user=self.user).exclude(pk__in=[old_token_pk])
        # another token should be generated
        self.assertTrue(new_token.exists())

        # and it should be different than the old token
        self.assertNotEqual(old_token_key, new_token.first().key)

        # there should always be one token per user
        self.assertEqual(Token.objects.filter(user=self.user).count(), 1)
