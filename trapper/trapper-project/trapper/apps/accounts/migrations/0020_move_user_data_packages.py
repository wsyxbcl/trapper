import os
import shutil

from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.db import migrations


def move_packages_to_media(apps, schema_editor):
    """
    Before the migration, UserDataPackage objects can be located:
    - either in /external_media/{username}/data_packages (from a time they were mistakenly saved in external_media),
    - or in /media/{username}/data_packages.
    The purpose of this migration is to copy the files to
        /media/protected/accounts/user_data_package/user_id_{user_id}/
    """
    UserDataPackage = apps.get_model("accounts", "UserDataPackage")

    qs = UserDataPackage.objects.all()
    ext_media_url = settings.EXTERNAL_MEDIA_ROOT

    for package_obj in qs:
        username = package_obj.user.username
        filename = os.path.basename(package_obj.package.name)
        path = f"{ext_media_url}/{username}/data_packages/{filename}"

        # check if file exists
        if os.path.isfile(path):
            with open(path, "rb") as zip_file:
                temp_zip = SimpleUploadedFile.from_dict(
                    {'content': zip_file.read(), 'filename': filename, 'content-type': 'application/zip'}
                )
                package_obj.package.save(filename, temp_zip)

    # remove data_packages dirs
    dirs = [l for l in os.walk(ext_media_url) if l[0].endswith("data_packages")]
    for dir_path, _, _ in dirs:
        shutil.rmtree(dir_path)


class Migration(migrations.Migration):

    dependencies = [
        ("accounts", "0019_alter_userdatapackage_package"),
    ]

    operations = [
        migrations.RunPython(move_packages_to_media)
    ]