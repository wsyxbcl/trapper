# Generated by Django 3.2.5 on 2022-07-21 09:45

from django.db import migrations
import timezone_field.fields


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0020_move_user_data_packages'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='timezone',
            field=timezone_field.fields.TimeZoneField(default='Europe/Warsaw', verbose_name='Timezone'),
        ),
    ]
