# -*- coding: utf-8 -*-
"""
Simple admin interface to control changes in models.

Admin is used to activate or deactivate accounts
"""
import docker
from django.conf import settings
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Permission, ContentType
from django.core.mail import send_mail
from django.shortcuts import render
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from trapper.apps.accounts.forms import (
    AdminSetUserRolesForm,
    AdminMailUsersForm,
    AdminCreateFtpUsersForm,
)
from trapper.apps.accounts.models import (
    UserProfile,
    UserTask,
    UserRemoteTask,
    UserDataPackage,
)
from trapper.apps.media_classification.models import ClassificationProjectRole
from trapper.apps.research.models import ResearchProjectRole

User = get_user_model()


class TrapperUserAdmin(UserAdmin):
    list_display = (
        "username",
        "email",
        "first_name",
        "last_name",
        "is_staff",
        "is_active",
        "has_ftp",
    )
    readonly_fields = ("date_joined", "last_login")
    list_select_related = ("userprofile",)
    search_fields = ("username", "email", "first_name", "last_name")
    list_filter = (
        "is_staff",
        "is_active",
        "last_login",
        "userprofile__has_ftp_account",
    )
    actions = [
        "set_user_roles_action",
        "mail_users_action",
        "create_ftp_users_action",
        "delete_ftp_users_action",
    ]

    def has_ftp(self, item):
        return item.userprofile.has_ftp_account

    has_ftp.boolean = True
    has_ftp.short_description = _("FTP account")

    def set_user_roles_action(self, request, queryset):
        if "do_action" in request.POST:
            form = AdminSetUserRolesForm(request.POST)
            if form.is_valid():
                rprojects = form.cleaned_data["rproject"]
                rproject_role = form.cleaned_data["rproject_role"]
                cprojects = form.cleaned_data["cproject"]
                cproject_role = form.cleaned_data["cproject_role"]
                activate_all = form.cleaned_data["activate_all"]
                for user in queryset:
                    if activate_all and not user.is_active:
                        # Activate user
                        user.is_active = True
                        user.save()
                    for rp in rprojects:
                        rpr, created = ResearchProjectRole.objects.get_or_create(
                            user=user, project=rp, defaults={"name": int(rproject_role)}
                        )
                        if not created:
                            rpr.name = int(rproject_role)
                            rpr.save()
                    for cp in cprojects:
                        cpr, created = ClassificationProjectRole.objects.get_or_create(
                            user=user,
                            classification_project=cp,
                            defaults={"name": int(cproject_role)},
                        )
                        if not created:
                            cpr.name = int(cproject_role)
                            cpr.save()
                self.message_user(
                    request,
                    _("You have successfully set all the selected user roles."),
                    level=25,
                )
                return
        else:
            form = AdminSetUserRolesForm(
                initial={
                    "_selected_action": request.POST.getlist(
                        admin.helpers.ACTION_CHECKBOX_NAME
                    )
                }
            )
        return render(
            request,
            "admin/accounts/action_set_user_roles.html",
            {"title": _("Set user roles"), "objects": queryset, "form": form},
        )

    set_user_roles_action.short_description = _("Set roles for selected users")

    def create_ftp_users_action(self, request, queryset):
        """Create FTPD accounts for selected users"""
        if "do_action" in request.POST:
            form = AdminCreateFtpUsersForm(request.POST)
            if form.is_valid():
                max_nfiles = form.cleaned_data["max_nfiles"]
                max_quota = form.cleaned_data["max_quota"] * 1024 ** 3
                client = docker.from_env()
                filters = {"name": "trapper_ftpd"}
                container_list = client.containers.list(filters=filters)
                container = client.containers.get(container_list[0].id)
                queryset = queryset.exclude(userprofile__has_ftp_account=True)
                n_users = len(queryset)
                i = 0
                for user in queryset:
                    try:
                        p = user.password.split("$")
                        password = "$" + "$".join([p[1], p[3], p[4]])
                        user_row = (
                            "{username}:{password}:{user_uid}:{user_gid}::{external_media}/"
                            "{username}/./::::::{max_nfiles}:{max_quota}::::::\n"
                        ).format(
                            username=user.username,
                            password=password,
                            max_nfiles=max_nfiles,
                            max_quota=max_quota,
                            external_media=settings.EXTERNAL_MEDIA_ROOT,
                            user_uid=settings.TRAPPER_USER_UID,
                            user_gid=settings.TRAPPER_USER_GID,
                        )
                        cmd = "echo '{user_row}' >> /etc/pure-ftpd/passwd/pureftpd.passwd; \
                        pure-pw mkdb /etc/pure-ftpd/pureftpd.pdb -f \
                        /etc/pure-ftpd/passwd/pureftpd.passwd".format(
                            user_row=user_row
                        )
                        container.exec_run(["sh", "-c", cmd], detach=True)
                        user.userprofile.has_ftp_account = True
                        user.userprofile.save()
                        i += 1
                    except Exception as e:
                        self.message_user(
                            request,
                            _(
                                f'Error when creating ftpd account for "{user.username}": {str(e)}'
                            ),
                            level=40,
                        )
                if i >= 1:
                    self.message_user(
                        request,
                        _(
                            f"You have successfully created {i}/{n_users} FTP account(s)."
                        ),
                        level=25,
                    )
                return
        else:
            form = AdminCreateFtpUsersForm(
                initial={
                    "_selected_action": request.POST.getlist(
                        admin.helpers.ACTION_CHECKBOX_NAME
                    )
                }
            )
        return render(
            request,
            "admin/accounts/action_create_ftp_users.html",
            {"title": _("Create FTP users"), "objects": queryset, "form": form},
        )

    create_ftp_users_action.short_description = _(
        "Create FTP accounts for selected users"
    )

    def delete_ftp_users_action(self, request, queryset):
        client = docker.from_env()
        filters = {"name": "trapper_ftpd"}
        container_list = client.containers.list(filters=filters)
        container = client.containers.get(container_list[0].id)
        queryset = queryset.exclude(userprofile__has_ftp_account=False)
        n_users = len(queryset)
        for user in queryset:
            cmd = "pure-pw userdel {username} -f \
            /etc/pure-ftpd/passwd/pureftpd.passwd -m".format(
                username=user.username
            )
            container.exec_run(["sh", "-c", cmd], detach=True)
            user.userprofile.has_ftp_account = False
            user.userprofile.save()
        self.message_user(
            request,
            _(f"You have successfully deleted {n_users} FTP account(s)."),
            level=25,
        )
        return

    delete_ftp_users_action.short_description = _(
        "Delete FTP accounts of selected users"
    )

    def mail_users_action(self, request, queryset):
        if "do_action" in request.POST:
            form = AdminMailUsersForm(request.POST)
            if form.is_valid():
                subject = form.cleaned_data["subject"]
                text = form.cleaned_data["text"]
                for user in queryset:
                    if user.userprofile.system_notifications:
                        send_mail(
                            subject=subject,
                            message=text,
                            from_email=None,
                            recipient_list=[user.email],
                            fail_silently=True,
                        )

                self.message_user(
                    request,
                    _("You have successfully send an email to selected users."),
                    level=25,
                )
                return
        else:
            form = AdminMailUsersForm(
                initial={
                    "_selected_action": request.POST.getlist(
                        admin.helpers.ACTION_CHECKBOX_NAME
                    )
                }
            )

        return render(
            request,
            "admin/accounts/action_mail_users.html",
            {"title": _("Send an email"), "objects": queryset, "form": form},
        )

    mail_users_action.short_description = _("Send an email to selected users")


class UserDataPackageAdmin(admin.ModelAdmin):
    list_display = (
        "filename",
        "package_type",
        "user",
        "date_created",
        "uuid4",
        "description",
        "link",
    )
    search_fields = ("user__username", "package", "description")
    list_filter = ("package_type", "date_created", "user")

    def link(self, item):
        return mark_safe(
            f'<a href="{item.get_download_url()}" target="_blank">' + "Link" + "</a>"
        )

    link.short_description = _("Get file")


class UserRemoteTaskAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "user",
        "task_id",
        "status",
        "created_at",
        "updated_at",
    )
    search_fields = ("user__username", "name")
    list_filter = ("status", "created_at", "updated_at")
    raw_id_fields = ("user",)


admin.site.register(User, TrapperUserAdmin)
admin.site.register(UserProfile)
admin.site.register(UserTask)
admin.site.register(UserRemoteTask, UserRemoteTaskAdmin)
admin.site.register(UserDataPackage, UserDataPackageAdmin)
admin.site.register(Permission)
admin.site.register(ContentType)
