# -*- coding: utf-8 -*-
"""
Module containing various values used with accounts application
"""

from celery import states
from django.utils.translation import gettext_lazy as _

from trapper.apps.common.taxonomy import BaseTaxonomy


class StateSettings(BaseTaxonomy):
    STOPPABLE = [states.PENDING, states.RECEIVED]

    STATE_MAP = {
        states.PENDING: {
            "css": u"default",
            "icon": u"fa-clock-o",
            "title": _("Task status: PENDING"),
            "action_detail": True,
            "action_stop": True,
        },
        states.RECEIVED: {
            "css": u"default",
            "icon": u"fa-clock-o",
            "title": _("Task status: RECEIVED"),
            "action_detail": True,
            "action_stop": True,
        },
        states.STARTED: {
            "css": u"primary",
            "icon": u"fa-spin fa-refresh",
            "title": _("Task status: STARTED"),
            "action_detail": True,
            "action_stop": True,
        },
        states.SUCCESS: {
            "css": u"success",
            "icon": u"fa-check",
            "title": _("Task status: SUCCESS"),
            "action_detail": True,
            "action_stop": False,
        },
        states.FAILURE: {
            "css": u"danger",
            "icon": u"fa-close",
            "title": _("Task status: FAILURE"),
            "action_detail": True,
            "action_stop": False,
        },
        states.REVOKED: {
            "css": u"warning",
            "icon": u"fa-exclamation",
            "title": _("Task status: REVOKED"),
            "action_detail": True,
            "action_stop": False,
        },
        states.RETRY: {
            "css": u"warning",
            "icon": u"fa-exclamation",
            "title": _("Task status: RETRY"),
            "action_detail": True,
            "action_stop": False,
        },
        states.IGNORED: {
            "css": u"warning",
            "icon": u"fa-exclamation",
            "title": _("Task status: IGNORED"),
            "action_detail": True,
            "action_stop": False,
        },
        states.REJECTED: {
            "css": u"danger",
            "icon": u"fa-close",
            "title": _("Task status: REJECTED"),
            "action_detail": True,
            "action_stop": False,
        },
    }


class ExternalStorageSettings(BaseTaxonomy):
    COLLECTIONS = "collections"
    RESOURCES = "resources"
    LOCATIONS = "locations"
    DATA_PACKAGES = "data_packages"

    DIRS = [COLLECTIONS, RESOURCES, LOCATIONS, DATA_PACKAGES]


class PackageType(BaseTaxonomy):
    """Data package types handled by :class:`accounts.UserDataPackage`"""

    MEDIA_FILES = "M"
    CLASSIFICATION_RESULTS = "C"

    CHOICES = (
        (MEDIA_FILES, _("Media files")),
        (CLASSIFICATION_RESULTS, _("Classification results")),
    )


class UserRemoteTaskStatus(BaseTaxonomy):
    """Status of a remote task handled by :class:`accounts.UserRemoteTask`"""

    STARTED = "STARTED"
    SUCCESS = "SUCCESS"
    REJECTED = "REJECTED"
    FAILURE = "FAILURE"

    CHOICES = (
        (STARTED, _("STARTED")),
        (SUCCESS, _("SUCCESS")),
        (REJECTED, _("REJECTED")),
        (FAILURE, _("FAILURE")),
    )
