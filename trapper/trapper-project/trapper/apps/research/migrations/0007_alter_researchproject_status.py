# Generated by Django 3.2.4 on 2021-11-03 17:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('research', '0006_auto_20210831_1645'),
    ]

    operations = [
        migrations.AlterField(
            model_name='researchproject',
            name='status',
            field=models.BooleanField(choices=[(None, 'Not yet processed'), (False, 'Rejected'), (True, 'Approved')], null=True, verbose_name='Status'),
        ),
    ]
