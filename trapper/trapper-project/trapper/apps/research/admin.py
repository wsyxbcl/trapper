# -*- coding: utf-8 -*-
"""
Simple admin interface to control changes in models.

Admin is used to approve or reject research projects created
by users
"""

from django.contrib import admin

from trapper.apps.research.models import (
    ResearchProject,
    ResearchProjectRole,
    ResearchProjectCollection,
)


class ProjectRoleInline(admin.TabularInline):
    model = ResearchProjectRole
    extra = 0
    raw_id_fields = ("user",)


class ResearchProjectCollectionInline(admin.TabularInline):
    model = ResearchProjectCollection
    extra = 0
    raw_id_fields = ("collection",)
    readonly_fields = (
        "resources_count",
        "collection_storage",
    )


class ResearchProjectAdmin(admin.ModelAdmin):
    inlines = [ProjectRoleInline, ResearchProjectCollectionInline]
    filter_horizontal = ("collections",)
    list_display = ("acronym", "name", "description", "date_created", "status")
    search_fields = ("acronym", "name", "keywords__name")
    list_filter = ("status", "date_created", "owner")
    raw_id_fields = ("owner",)


admin.site.register(ResearchProject, ResearchProjectAdmin)
