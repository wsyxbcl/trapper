# -*- coding: utf-8 -*-
"""
Views used to handle logic related to research project management in research
application
"""
from braces.views import JSONResponseMixin, UserPassesTestMixin
from django.conf import settings
from django.contrib import messages
from django.core.mail import mail_managers
from django.db.models.signals import post_save
from django.http import HttpResponseRedirect
from django.template.loader import render_to_string
from django.utils.translation import gettext_lazy as _
from django.urls import reverse
from django.urls import reverse_lazy
from django.views import generic
from extra_views import CreateWithInlinesView, UpdateWithInlinesView, NamedFormsetsMixin

from trapper.apps.common.views import LoginRequiredMixin, BaseDeleteView
from trapper.apps.media_classification.views.projects import (
    ClassificationProjectGridContextMixin,
)
from trapper.apps.research.forms import (
    ResearchProjectForm,
    ProjectCollectionAddForm,
    ProjectRoleInline,
    ProjectRoleCreateInline,
)
from trapper.apps.research.models import ResearchProject, ResearchProjectCollection
from trapper.apps.storage.views.collection import CollectionGridContextMixin


class ResearchProjectListView(LoginRequiredMixin, generic.TemplateView):
    """View used for rendering template with research projects grid."""

    model = ResearchProject
    template_name = "research/project_list.html"

    def get_context_data(self, **kwargs):
        """All we need to render base grid is:
        * Model name as title
        * Base context to build filters

        This view is not serving any data. Data is read using DRF API
        """
        research_project_context = {
            "data_url": reverse("research:api-research-project-list"),
            "keywords": ResearchProject.keywords.values_list("pk", "name"),
            "model_name": "research projects",
            "update_redirect": "true",
            "sortable": [
                "name",
            ],
        }
        return {"research_project_context": research_project_context}


view_research_project_list = ResearchProjectListView.as_view()


class ResearchProjectDetailView(
    UserPassesTestMixin,
    generic.DetailView,
    CollectionGridContextMixin,
    ClassificationProjectGridContextMixin,
):
    """View used for rendering details of specified research project.

    Research projects are accessible to everyone, but anonymous users
    can see limited version of details

    This view uses

    * :class:`apps.storage.views.collection.CollectionGridContextMixin`
      for altering behaviour collection grid rendered in details
    * :class:`apps.media_classification.views.projects.ClassificationProjectGridContextMixin`
      for altering behaviour classification projects grid rendered in details
    """

    template_name = "research/project_detail.html"
    model = ResearchProject
    raise_exception = True
    context_object_name = "research_project"

    def test_func(self, user):
        """Details are available only if project is accepted"""
        return self.get_object().status

    def get_collection_delete_url(self, **kwargs):
        """Return standard url used for removing multiple collections"""
        return reverse("research:project_collection_delete_multiple")

    def get_collection_url(self, **kwargs):
        """Alter url for collections DRF API, to get only collections that
        belongs to research project and are accessible for currently logged in
        user"""
        research_project = kwargs.get("research_project")
        return reverse(
            "research:api-research-project-collection-list",
            kwargs={"project_pk": research_project.pk},
        )

    def get_classification_project_url(self, **kwargs):
        """Return standard url used for classification project list limited
        to those, which are related to given research project"""
        research_project = kwargs.get("research_project")
        return "{url}?research_project={pk}".format(
            url=reverse("media_classification:api-classification-project-list"),
            pk=research_project.pk,
        )

    def get_context_data(self, **kwargs):
        """
        Alter context data used for research projects details:

        * for collection context:

            * hide add research project
            * hide filter research project
            * set collection primary key field
            * hide collection delete action
            * hide collection update action

        * for classification project:

            * hide research project column in grid
            * hide classification project delete action
            * hide classification project update action
        """
        context = super().get_context_data(**kwargs)
        context["collection_context"] = self.get_collection_context(
            research_project=context["object"]
        )
        context["collection_context"]["hide_add_research_project"] = True
        context["collection_context"]["hide_filter_research_project"] = True
        context["collection_context"]["collection_field"] = "collection_pk"
        context["collection_context"]["hide_collection_delete"] = True
        context["collection_context"]["hide_collection_update"] = True

        classification_key = "classification_project_context"
        context[classification_key] = self.get_classification_project_context(
            research_project=context["object"]
        )
        context[classification_key]["hide_research_project_col"] = True
        project_delete_key = "hide_classification_project_delete"
        project_update_key = "hide_classification_project_update"
        context[classification_key][project_delete_key] = True
        context[classification_key][project_update_key] = True
        return context


view_research_project_detail = ResearchProjectDetailView.as_view()


class ProjectCreateView(LoginRequiredMixin, CreateWithInlinesView, NamedFormsetsMixin):
    """Research project's create view.
    Handle the creation of the :class:`apps.research.models.ResearchProject`
    objects.
    """

    model = ResearchProject
    form_class = ResearchProjectForm
    template_name = "research/project_change.html"
    inlines = [ProjectRoleCreateInline]
    inlines_names = [
        "projectrole_formset",
    ]
    success_url = reverse_lazy("research:project_list")

    def forms_valid(self, form, inlines):
        """If form is valid then set `owner` as currently logged in user,
        and add message that project has been created"""

        user = self.request.user
        form.instance.owner = user
        self.object = form.save(commit=False)
        if user.is_staff:
            self.object.status = True
        self.object.save()
        self.object.keywords.clear()
        for keyword in form.cleaned_data["keywords"]:
            self.object.keywords.add(keyword)
        projectrole_formset = inlines[0]
        projectrole_formset.save()
        if user.is_staff:
            messages.add_message(
                self.request,
                messages.SUCCESS,
                _("Your new research project has been successfully created! "),
            )
        else:
            # display a message to a user
            messages.add_message(
                self.request,
                messages.SUCCESS,
                _(
                    "Your new research project has been successfully created! "
                    "However, it needs to be activated by the administrators. "
                    "When this is done we will notify you immediately."
                ),
            )
            # notify managers
            if settings.EMAIL_NOTIFICATIONS_RESEARCH_PROJECT:
                mail_managers(
                    _("New request for Trapper research project"),
                    _(
                        "The following research project needs your verification: "
                        f"{self.object.get_admin_url()}."
                    ),
                    fail_silently=True,
                )
        return HttpResponseRedirect(self.get_success_url())

    def forms_invalid(self, form, inlines):
        """If form is not valid it is re-rendered with error details."""
        messages.add_message(
            self.request, messages.ERROR, _("Error creating new project")
        )
        return super().forms_invalid(form, inlines)


view_research_project_create = ProjectCreateView.as_view()


class ProjectUpdateView(
    LoginRequiredMixin,
    UserPassesTestMixin,
    UpdateWithInlinesView,
    NamedFormsetsMixin,
    JSONResponseMixin,
):
    """Research project's update view.
    Handle the update of the :class:`apps.research.models.ResearchProject`
    objects.
    """

    model = ResearchProject
    form_class = ResearchProjectForm
    template_name = "research/project_change.html"
    inlines = [ProjectRoleInline]
    inlines_names = [
        "projectrole_formset",
    ]
    raise_exception = True

    def test_func(self, user):
        """Update is available only for users that have enough permissions"""
        return self.get_object().can_update(user)

    def forms_valid(self, form, inlines):
        self.object = form.save(commit=False)
        self.object.save()
        self.object.keywords.clear()
        for keyword in form.cleaned_data["keywords"]:
            self.object.keywords.add(keyword)
        # update project roles
        self.object.project_roles.all().delete()
        projectrole_formset = inlines[0]
        for projectrole_form in projectrole_formset:
            projectrole_form.save()
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(
                f"The research project {form.instance.acronym} has been successfully updated!"
            ),
        )
        return HttpResponseRedirect(self.object.get_absolute_url())

    def forms_invalid(self, form, inlines):
        messages.add_message(
            self.request, messages.ERROR, _("Your form contains errors.")
        )
        return super().forms_invalid(form, inlines)


view_research_project_update = ProjectUpdateView.as_view()


class ProjectDeleteView(BaseDeleteView):
    """View responsible for handling deletion of single or multiple
    research projects.

    Only projects that user has enough permissions for can be deleted
    """

    model = ResearchProject
    redirect_url = "research:project_list"


view_research_project_delete = ProjectDeleteView.as_view()


class ProjectCollectionAddView(LoginRequiredMixin, generic.FormView, JSONResponseMixin):
    """
    View used to add collections to existing research project

    User is required to have at least project update permissions to
    add collections, and each collection has to be accessible by user.
    """

    template_name = "forms/simple_crispy_form.html"
    form_class = ProjectCollectionAddForm
    raise_exception = True

    def form_valid(self, form):
        """"""
        error = form.cleaned_data.get("error", None)
        if not error:
            new_collections = form.cleaned_data.get("new_collections", None)
            project = form.cleaned_data.get("project", None)
            bulk_create_list = [
                ResearchProjectCollection(project=project, collection=k)
                for k in new_collections
            ]
            ResearchProjectCollection.objects.bulk_create(bulk_create_list)
            for i in bulk_create_list:
                post_save.send(i.__class__, instance=i, created=True)
            msg = _(
                f"You have successfully added {new_collections.count()} new collections to "
                "selected research project."
            )
            context = {
                "success": True,
                "msg": msg,
            }
        else:
            context = {
                "success": False,
                "msg": error,
            }
        return self.render_json_response(context_dict=context)

    def form_invalid(self, form):
        """If form is not valid, form is re-rendered with error details,
        and message about unsuccessfull operation is shown"""
        context = {
            "success": False,
            "msg": _("Your form contain errors"),
            "form_html": render_to_string(self.template_name, {"form": form}),
        }
        return self.render_json_response(context_dict=context)


view_research_project_collection_add = ProjectCollectionAddView.as_view()


class ProjectCollectionDeleteView(BaseDeleteView):
    """View responsible for handling deletion of single or multiple
    research project collections.

    Only collections that user has enough permissions for can be deleted
    """

    model = ResearchProjectCollection
    success_msg_tmpl = _('Research project collection "{name}" has been deleted')
    fail_msg_tmpl = _('You cannot remove research project collection "{name}"')
    redirect_url = "research:project_list"

    def filter_editable(self, queryset, user):
        """Overwrite this method to check if user is an admin of a project
        that selected collections belong to"""
        to_delete = []
        for obj in queryset:
            if obj.can_delete(user):
                to_delete.append(obj)
        return to_delete

    def bulk_delete(self, queryset):
        for obj in queryset:
            obj.delete()

    def add_message(self, status, template, item):
        """ResearchProjectCollection has no `name` attribute so we have
        to overwrite this method"""
        messages.add_message(
            self.request, status, template.format(name=item.collection.name)
        )


view_research_project_collection_delete = ProjectCollectionDeleteView.as_view()
