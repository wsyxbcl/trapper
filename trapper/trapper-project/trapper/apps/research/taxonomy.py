# -*- coding: utf-8 -*-

from django.utils.translation import gettext_lazy as _
from trapper.apps.common.taxonomy import BaseTaxonomy

__all__ = ["ResearchProjectRoleType"]


class ResearchProjectRoleType(BaseTaxonomy):
    ADMIN = 1
    EXPERT = 2
    COLLABORATOR = 3

    ANY = (
        ADMIN,
        EXPERT,
        COLLABORATOR,
    )
    EDIT = (ADMIN,)
    DELETE = (ADMIN,)
    UPLOAD = (
        ADMIN,
        COLLABORATOR,
    )

    CHOICES = (
        (ADMIN, _("Admin")),
        (EXPERT, _("Expert")),
        (COLLABORATOR, _("Collaborator")),
    )


class ResearchProjectStatus(BaseTaxonomy):
    APPROVED = True
    REJECTED = False
    NOT_PROCESSED = None

    CHOICES = (
        (NOT_PROCESSED, _("Not yet processed")),
        (REJECTED, _("Rejected")),
        (APPROVED, _("Approved")),
    )


class ResearchProjectSamplingDesign(BaseTaxonomy):
    SIMPLE = 1
    SYSTEMATIC = 2
    CLUSTERED = 3
    EXPERIMENTAL = 4
    TARGETED = 5
    ADHOC = 6

    CHOICES = (
        (SIMPLE, _("simpleRandom")),
        (SYSTEMATIC, _("systematicRandom")),
        (CLUSTERED, _("clusteredRandom")),
        (EXPERIMENTAL, _("experimental")),
        (TARGETED, _("targeted")),
        (ADHOC, _("opportunistic")),
    )


class ResearchProjectSensorMethod(BaseTaxonomy):
    MOTION = 1
    TIMELAPSE = 2
    BOTH = 3

    CHOICES = (
        (MOTION, _("motionDetection")),
        (TIMELAPSE, _("timeLapse")),
        (BOTH, _("both")),
    )


class ResearchProjectAnimalTypes(BaseTaxonomy):
    UNMARKED = 1
    MARKED = 2
    BOTH = 3

    CHOICES = ((UNMARKED, _("unmarked")), (MARKED, _("marked")), (BOTH, _("both")))


class ResearchProjectBaitUse(BaseTaxonomy):
    NONE = 1
    SCENT = 2
    FOOD = 3
    VISUAL = 4
    ACOUSTIC = 5
    OTHER = 6

    CHOICES = (
        (NONE, _("none")),
        (SCENT, _("scent")),
        (FOOD, _("food")),
        (VISUAL, _("visual")),
        (ACOUSTIC, _("acoustic")),
        (OTHER, _("other")),
    )


class ResearchProjectCollectionChoices(BaseTaxonomy):
    pass
