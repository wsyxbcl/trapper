from unittest.mock import patch, Mock

from django.contrib.auth.models import AnonymousUser
from django.http import HttpRequest
from django.test import TestCase
from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.geomap.tests.factories.deployment import DeploymentFactory
from trapper.apps.geomap.tests.factories.location import LocationFactory
from trapper.apps.media_classification.tests.factories.classificator import (
    ClassificatorFactory,
)
from trapper.apps.research.models import ResearchProject, project_activated_notifiction
from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)
from trapper.apps.storage.models import Resource
from trapper.apps.storage.taxonomy import ResourceStatus, CollectionStatus
from trapper.apps.storage.tests.factories.collection import CollectionFactory
from trapper.apps.storage.tests.factories.resource import ResourceFactory


class ResearchProjectTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(ResearchProjectTest, cls).setUpTestData()

        cls.user1 = UserFactory()
        cls.user2 = UserFactory()
        cls.user3 = UserFactory()
        cls.user4 = UserFactory()

        cls.research_project = ResearchProjectFactory(owner=cls.user1)

        cls.location1 = LocationFactory(
            owner=cls.user1, research_project=cls.research_project
        )
        cls.deployment = DeploymentFactory(
            owner=cls.user1,
            location=cls.location1,
            research_project=cls.research_project,
        )
        ResourceFactory.create_batch(
            2, owner=cls.user1, deployment=cls.deployment, status=ResourceStatus.PRIVATE
        )
        cls.collection = CollectionFactory(
            owner=cls.user1, status=CollectionStatus.PRIVATE
        )
        cls.collection.resources.set(Resource.objects.all())
        cls.collection.managers.set([cls.user3])
        cls.classificator = ClassificatorFactory(owner=cls.user1)

        cls.research_project.collections.set([cls.collection])

    def test_rp_get_accessible(self):
        ResearchProject.objects.get_accessible(user=self.user1)
        ResearchProject.objects.get_accessible(user=self.user2)
        ResearchProject.objects.get_accessible(user=self.user3)
        ResearchProject.objects.get_accessible(user=AnonymousUser())
        ResearchProject.objects.get_accessible(
            user=self.user1, base_queryset=ResearchProject.objects.all()
        )

    def test_rp_api_update_context(self):
        ResearchProject.objects.api_update_context(self.research_project, self.user1)
        ResearchProject.objects.api_update_context(self.research_project, self.user2)
        ResearchProject.objects.api_update_context(
            self.research_project, AnonymousUser()
        )

    def test_rp_api_detail_context(self):
        ResearchProject.objects.api_detail_context(self.research_project, self.user1)
        ResearchProject.objects.api_detail_context(self.research_project, self.user2)
        ResearchProject.objects.api_detail_context(
            self.research_project, AnonymousUser()
        )

    def test_rp_api_delete_context(self):
        ResearchProject.objects.api_delete_context(self.research_project, self.user1)
        ResearchProject.objects.api_delete_context(self.research_project, self.user2)
        ResearchProject.objects.api_delete_context(
            self.research_project, AnonymousUser()
        )

    def test_rp_get_roles(self):
        self.research_project.get_roles()

    def test_rp_can_create_classification_project(self):
        self.research_project.can_create_classification_project(user=self.user1)
        self.research_project.can_create_classification_project(user=self.user2)
        self.research_project.can_create_classification_project(user=AnonymousUser())

    @patch("trapper.apps.research.models.get_current_request")
    def test_rp_get_admin_url(self, mock_get_current_request):
        request = Mock()
        request.build_absolute_uri = Mock(return_value="https://route.com")
        mock_get_current_request.return_value = request
        self.assertEqual(self.research_project.get_admin_url(), "https://route.com")

    def test_rp_send_create_message(self):
        self.research_project.send_create_message()

    def test_rp_project_activated_notifiction(self):
        project_activated_notifiction(None, self.research_project)
