# -*- coding: utf-8 -*-
from trapper.apps.common.filters import BaseFilterSet
from trapper.apps.extra_tables.models import Species


class SpeciesFilter(BaseFilterSet):
    """"""

    class Meta:
        model = Species
        fields = ["english_name", "latin_name"]
