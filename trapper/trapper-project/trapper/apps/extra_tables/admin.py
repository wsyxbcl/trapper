# -*- coding: utf-8 -*-
"""
Simple admin interface to control changes in models.
"""
from django.contrib import admin
from django.urls import path
from django.utils.safestring import mark_safe

from trapper.apps.extra_tables.admin_views import import_from_col
from trapper.apps.extra_tables.models import Species, Licence


class TaxonIDFilter(admin.SimpleListFilter):
    title = "has taxon_id"
    parameter_name = "taxon_id__isnull"

    def lookups(self, request, model_admin):
        return (
            ("False", "has taxon_id"),
            ("True", "has no taxon_id"),
        )

    def queryset(self, request, queryset):
        if self.value() == "False":
            return queryset.filter(taxon_id__isnull=False)
        if self.value() == "True":
            return queryset.filter(taxon_id__isnull=True)


class SpeciesAdmin(admin.ModelAdmin):
    list_display = [
        "english_name",
        "latin_name",
        "family",
        "genus",
        "taxon_id",
        "taxon_rank",
        "eol_link",
    ]
    list_filter = ["family", "genus", "taxon_rank", TaxonIDFilter]
    search_fields = ["english_name", "latin_name", "family", "genus"]
    change_list_template = "admin/species_changelist.html"

    def eol_link(self, instance):
        return mark_safe(
            ('<a href="http://eol.org/search?q=%s&search=Go" ' 'target="_blank">Go</a>')
            % instance.latin_name
        )

    eol_link.short_description = "EOL link"

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path(
                "import-col",
                self.admin_site.admin_view(import_from_col),
                name="import-col",
            )
        ]
        return urls + my_urls


class LicenceAdmin(admin.ModelAdmin):
    list_display = ["name", "url", "title"]


admin.site.register(Species, SpeciesAdmin)
admin.site.register(Licence, LicenceAdmin)
