from django.conf import settings
from django.contrib import messages
from django.shortcuts import render, redirect
from django.urls import reverse

from trapper.apps.accounts.models import UserTask
from trapper.apps.extra_tables.tasks import celery_import_species_from_col
from trapper.apps.extra_tables.forms import SpeciesImportForm


def import_from_col(request):
    form = SpeciesImportForm(request.POST or None)

    if form.is_valid():
        classes = form.cleaned_data["chosen_class"]
        params = {"classes": classes}
        if settings.CELERY_ENABLED:
            task = celery_import_species_from_col.delay(**params)
            user_task = UserTask(user=request.user, task_id=task.task_id)
            user_task.save()
            msg = "You have successfully run a celery task. Species are being imported now."
        else:
            msg = celery_import_species_from_col(**params)
        messages.success(request, msg)
        return redirect(reverse("admin:extra_tables_species_changelist"))

    return render(
        request,
        "admin/import_col.html",
        {"title": "Import from Catalogue of Life", "form": form},
    )
