from celery import shared_task
from django.conf import settings
import pandas as pd

from trapper.apps.extra_tables.models import Species


@shared_task(serializer="pickle")
def celery_import_species_from_col(**kwargs):
    """
    Celery task that imports species from chosen classes from COL.
    """
    classes = kwargs.get("classes", [])
    sp_to_create = []
    sp_to_update = []
    for cls in classes:
        url = settings.TAXA_URL.format(cls)
        taxa_df = pd.read_csv(url).fillna("")

        # get the existing Species objects that only need to be updated
        species = Species.objects.filter(latin_name__in=taxa_df.latin_name.unique())

        # build dict {"latin_name": species_object}
        species_dict = {k.latin_name: k for k in species}

        for _, row in taxa_df.iterrows():
            latin_name = row.latin_name
            # if english name not provided, use latin name
            english_name = row.english_name or row.latin_name
            species_obj = species_dict.get(latin_name)

            if species_obj:
                species_obj.taxon_id = row.taxon_id
                species_obj.taxon_rank = row.taxon_rank
                sp_to_update.append(species_obj)
            else:
                sp = Species(
                    latin_name=latin_name,
                    taxon_id=row.taxon_id,
                    taxon_rank=row.taxon_rank,
                    english_name=english_name,
                )
                sp_to_create.append(sp)
    if sp_to_create:
        Species.objects.bulk_create(sp_to_create, batch_size=settings.BULK_BATCH_SIZE)
    if sp_to_update:
        Species.objects.bulk_update(
            sp_to_update,
            ["taxon_id", "taxon_rank"],
            batch_size=settings.BULK_BATCH_SIZE,
        )

    return f"You have successfully imported taxa from {classes} classes."
