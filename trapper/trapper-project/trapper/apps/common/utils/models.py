# -*- coding: utf-8 -*-
"""
Various functions that could be used in other applications models.
"""
import os
from django import forms


TRUE_FALSE_CHOICES = ((False, "False"), (True, "True"))


def checkbox2select(boolfield):
    """"""
    boolfield.widget = forms.Select(choices=TRUE_FALSE_CHOICES)
    return boolfield


def delete_old_file(instance, field):
    """
    Check if a file for given field has been changed and delete an old file if
    this is the case.
    """
    if instance.pk:
        try:
            old_instance = instance.__class__.objects.get(pk=instance.pk)
        except instance.__class__.DoesNotExist:
            # Some celery async tasks can throw errors here
            pass
        else:
            old_file = getattr(old_instance, field)
            new_file = getattr(instance, field)
            if old_file and old_file != new_file and os.path.exists(old_file.path):
                os.remove(old_file.path)
