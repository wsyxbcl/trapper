# -*- coding: utf-8 -*-
"""Extensions that helps testing code"""
import json
import os
import tempfile
import uuid
import random

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.gis.geos.point import Point
from django.core import mail
from django.core.cache import cache
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test.testcases import TestCase
from django.test.utils import override_settings
from django.urls import reverse
from django.utils.lorem_ipsum import words
from django.utils.timezone import now
from trapper.apps.accounts.utils import create_external_media
from trapper.apps.common.utils.identity import create_hashcode
from trapper.apps.extra_tables.models import Species
from trapper.apps.geomap.models import Location, Deployment
from trapper.apps.media_classification.models import (
    ClassificationProject,
    ClassificationProjectRole,
    ClassificationProjectCollection,
    Classificator,
    Sequence,
    Classification,
    ClassificationDynamicAttrs,
    UserClassification,
    UserClassificationDynamicAttrs,
    AIClassification,
    AIClassificationDynamicAttrs,
    AIProvider,
)
from trapper.apps.media_classification.taxonomy import ClassificationStatus
from trapper.apps.research.models import (
    ResearchProject,
    ResearchProjectRole,
    ResearchProjectCollection,
)
from trapper.apps.research.taxonomy import ResearchProjectStatus
from trapper.apps.storage.models import Resource, Collection, CollectionMember
from trapper.apps.storage.taxonomy import ResourceStatus, CollectionStatus

User = get_user_model()

__all__ = ["ExtendedTestCase", "TestActorsMixin", "ResourceTestMixin"]


def create_user(username, is_active=True, is_superuser=False):
    """
    Method used by metaclass for dynamic createion `create_<username>`
    methods.
    """

    def _inner(self):
        email = "{u}@example.com".format(u=username)
        is_admin = bool(is_superuser)
        params = {
            "username": username,
            "email": email,
            "is_active": bool(is_active),
            "is_staff": is_admin,
            "is_superuser": is_admin,
        }
        user = User(**params)
        user.set_password(username)
        user.save()
        setattr(self, "{u}_email".format(u=username), email)
        setattr(self, "{u}_passwd".format(u=username), username)
        setattr(self, username, user)

    return _inner


def login_user(username):
    """
    Method used by metaclass for dynamic createion `login_<username>`
    methods.
    """

    def _inner(self):
        logged_in = self.client.login(username=username, password=username)
        self.assertTrue(logged_in)
        create_external_media(username)
        return logged_in

    return _inner


class ActorsMixinMetaclass(type):
    """
    TODO: docstrings
    """

    def __new__(mcs, name, bases, attrs):
        new_class = super(ActorsMixinMetaclass, mcs).__new__(mcs, name, bases, attrs)
        active_users = attrs.pop("active_users", [])
        inactive_users = attrs.pop("inactive_users", [])
        sysadmins = attrs.pop("sysadmins", [])
        for username in active_users:
            summon_name = "summon_{u}".format(u=username)
            login_name = "login_{u}".format(u=username)
            setattr(new_class, summon_name, create_user(username))
            setattr(new_class, login_name, login_user(username))
        for username in sysadmins:
            summon_name = "summon_{u}".format(u=username)
            login_name = "login_{u}".format(u=username)
            setattr(new_class, summon_name, create_user(username, is_superuser=True))
            setattr(new_class, login_name, login_user(username))
        for username in inactive_users:
            summon_name = "summon_{u}".format(u=username)
            setattr(new_class, summon_name, create_user(username, is_active=False))
        return new_class


class TestActorsMixin(metaclass=ActorsMixinMetaclass):
    """
    Mixin with actors used for testing.

    * active_users - each user will be able to login to project
    * sysadmins - like `active_users` but users will be superusers
    * inactive_users - those users will have created disabled accounts

    For each user special method is create: `summon_<username>`. I.e. for
    `alice` method will be called `summon_alice`

    For each active user (active_users and sysadmins) will additionaly have
    own method `login_<username>`. I.e. for `alice` method will be called
    `login_alice`. Inactive users won't get such methods since they cannot
    login anyway.

    If more actors are needed just add them to proper list.
    """

    active_users = ["alice", "ziutek", "john"]
    sysadmins = ["root"]
    inactive_users = ["eric"]


class LocationTestMixin:
    """
    TODO: docstrings
    """

    def create_location(self, owner, location_id=None, **kwargs):
        params = {
            "location_id": location_id
            or "".join(random.choice(uuid.uuid4().hex) for x in range(10)),
            "name": words(3, common=False),
            "is_public": True,
            "owner": owner,
            "coordinates": Point(x=50, y=50),
        }
        params.update(kwargs or {})
        location = Location.objects.create(**params)
        return location


class DeploymentTestMixin:
    """
    TODO: docstrings
    """

    def create_deployment(self, owner, location=None, **kwargs):
        params = {
            "deployment_code": words(1, common=False),
            "location": location or self.create_location(owner=owner),
            "start_date": now(),
            "end_date": now(),
            "owner": owner,
        }
        params.update(kwargs or {})
        deployment = Deployment.objects.create(**params)
        return deployment


class ResourceTestMixin(LocationTestMixin, DeploymentTestMixin):
    """
    TODO: docstrings
    """

    def create_resource(
        self,
        owner,
        status=None,
        file_content=None,
        managers=None,
        deployment=None,
        **resource_kwargs
    ):
        status = status or ResourceStatus.PRIVATE
        if not file_content:
            file_path = os.path.join(self.SAMPLE_MEDIA_PATH, "image_1.jpg")
            with open(file_path, "rb") as handler:
                file_content = handler.read()
        file_name = u"{name}.png".format(name=create_hashcode())
        if not deployment:
            deployment = self.create_deployment(owner=owner)
        params = {
            "name": words(3, common=False),
            "file": SimpleUploadedFile(file_name, file_content),
            "date_recorded": now(),
            "status": status,
            "owner": owner,
            "deployment": deployment,
        }
        params.update(**resource_kwargs)
        resource = Resource.objects.create(**params)
        if managers:
            resource.managers.set(managers)
        return resource


class CollectionTestMixin(ResourceTestMixin):
    """
    TODO: docstrings
    """

    def create_collection(
        self, owner, status=None, resources=None, managers=None, roles=None
    ):
        status = status or CollectionStatus.ON_DEMAND
        resources = resources or [self.create_resource(owner=owner)]
        collection = Collection.objects.create(
            name=words(3, common=False), status=status, owner=owner
        )
        collection.resources.set(resources)
        if managers:
            collection.managers.set(managers)
        if roles:
            for user, level in roles:
                CollectionMember.objects.create(
                    user=user, collection=collection, level=level
                )
        return collection


class ResearchProjectTestMixin:
    """
    TODO: docstrings
    """

    def create_research_project(
        self, owner, status=None, collections=None, roles=None, name=None, acronym=None
    ):
        if status is None:
            status = ResearchProjectStatus.APPROVED
        name = name or words(3, common=False)
        acronym = acronym or "".join(random.choice(uuid.uuid4().hex) for x in range(10))
        project = ResearchProject.objects.create(
            name=name, acronym=acronym, status=status, owner=owner
        )
        if collections:
            project.collections = collections
        if roles:
            for user, role_level in roles:
                ResearchProjectRole.objects.create(
                    project=project, user=user, name=role_level
                )
        return project

    def create_research_project_collection(self, project, collection):
        return ResearchProjectCollection.objects.create(
            project=project, collection=collection
        )


class ClassificationProjectTestMixin:
    """
    TODO: docstrings
    """

    def create_classification_project(
        self,
        owner,
        research_project,
        classificator=None,
        collections=None,
        roles=None,
        **kwargs
    ):
        if "name" not in kwargs:
            name = words(3, common=False)
        else:
            name = kwargs["name"]
        params = {
            "name": name,
            "research_project": research_project,
            "owner": owner,
            "classificator": classificator,
        }
        params.update(kwargs or {})
        project = ClassificationProject.objects.create(**params)
        if collections:
            project.collections = collections
        if roles:
            for user, role_level in roles:
                ClassificationProjectRole.objects.create(
                    classification_project=project, user=user, name=role_level
                )
        return project

    def create_classification_project_collection(self, project, collection):
        return ClassificationProjectCollection.objects.create(
            project=project, collection=collection
        )


class SpeciesTestMixin:
    """
    TODO: docstrings
    """

    def create_species(self, english="Wolf", latin="Canis lupus"):
        species = Species.objects.create(english_name=english, latin_name=latin)
        return species


class ClassificatorTestMixin:
    """
    TODO: docstrings
    """

    def create_classificator(self, owner, **kwargs):
        if "name" not in kwargs:
            name = words(3, common=False)
        else:
            name = kwargs["name"]
        params = {"name": name, "owner": owner}
        params.update(kwargs or {})
        classificator = Classificator.objects.create(**params)
        return classificator


class SequenceTestMixin:
    """
    TODO: docstrings
    """

    def create_sequence(self, owner, collection, resources=None, **kwargs):
        if "sequence_id" not in kwargs:
            sequence_id = words(3, common=False)
        else:
            sequence_id = kwargs["sequence_id"]
        params = {
            "sequence_id": sequence_id,
            "created_by": owner,
            "collection": collection,
        }
        params.update(kwargs or {})
        sequence = Sequence.objects.create(**params)
        if resources is not None:
            sequence.resources = resources
        return sequence


class ClassificationTestMixin:
    """
    TODO: docstrings
    """

    def create_user_classification_dynamic_attrs(self, user_classification, **kwargs):
        params = {
            "userclassification": user_classification,
            "observation_type": "animal",
            "species": None,
            "count": 1,
        }
        params.update(kwargs or {})
        uc_dynamic_attrs = UserClassificationDynamicAttrs.objects.create(**params)
        return uc_dynamic_attrs

    def create_user_classification(self, owner, classification, dattrs=False, **kwargs):
        params = {
            "created_at": now(),
            "static_attrs": {"Item1": "Val1", "Item2": "Val2"},
        }
        params.update(kwargs or {})
        user_classification, created = UserClassification.objects.get_or_create(
            classification=classification, owner=owner, defaults=params
        )
        if not created:
            user_classification.updated_at = now()
        if dattrs:
            self.create_user_classification_dynamic_attrs(user_classification)
        return user_classification

    def create_ai_classification_dynamic_attrs(self, ai_classification, **kwargs):
        params = {
            "aiclassification": ai_classification,
            "observation_type": "animal",
            "species": None,
            "count": 1,
        }
        params.update(kwargs or {})
        aic_dynamic_attrs = AIClassificationDynamicAttrs.objects.create(**params)
        return aic_dynamic_attrs

    def create_ai_provider(self, **kwargs):
        params = {
            "name": "External provider",
        }
        params.update(kwargs or {})
        provider, _ = AIProvider.objects.get_or_create(**params)
        return provider

    def create_ai_classification(self, owner, classification, dattrs=None, **kwargs):
        params = {"created_at": now(), "model": self.create_ai_provider()}
        params.update(kwargs or {})
        ai_classification, _ = AIClassification.objects.get_or_create(
            classification=classification, owner=owner, defaults=params
        )
        if dattrs:
            self.create_ai_classification_dynamic_attrs(ai_classification, **dattrs)
        return ai_classification

    def create_classification(
        self, resource, collection, project, owner, status=None, **kwargs
    ):
        params = {"created_at": now(), "static_attrs": {}, "owner": owner}
        params.update(kwargs or {})
        classification, created = Classification.objects.get_or_create(
            resource=resource, project=project, collection=collection, defaults=params
        )
        if not created:
            classification.updated_at = now()
            classification.updated_by = owner
        if status:
            cl = project.classificator
            static_standard_fields = cl.active_standard_attrs("STATIC")
            dynamic_standard_fields = cl.active_standard_attrs("DYNAMIC")
            user_classification = self.create_user_classification(
                owner, classification, dattrs=True
            )
            classification.status = status
            classification.approved_at = now()
            classification.approved_by = owner
            classification.approved_source = user_classification
            # static attributes
            for attr_name in static_standard_fields:
                attr_value = getattr(user_classification, attr_name)
                setattr(classification, attr_name, attr_value)
            classification.static_attrs = user_classification.static_attrs
            classification.save()
            # dynamic attributes
            for user_dattrs in user_classification.dynamic_attrs.all():
                class_dattrs = ClassificationDynamicAttrs(
                    classification=classification,
                )
                for attr_name in dynamic_standard_fields:
                    attr_value = getattr(user_dattrs, attr_name)
                    setattr(class_dattrs, attr_name, attr_value)
                class_dattrs.attrs = user_dattrs.attrs
                class_dattrs.save()
        else:
            user_classification = None
        return classification, user_classification


@override_settings(
    MEDIA_ROOT=tempfile.mkdtemp(suffix="-trapper"),
    EXTERNAL_MEDIA_ROOT=tempfile.mkdtemp(suffix="-trapper-external"),
    CELERY_DATA_ROOT=tempfile.mkdtemp(suffix="-celery-data"),
    CELERY_ENABLED=False,
)
class ExtendedTestCase(TestCase, TestActorsMixin):
    """Extended version of :class:`TestCase` that
    add by default:

    * clean cache
    * logout client on :func:`tearDown`

    This class also add new assertions
    """

    TEST_DATA_PATH = os.path.join(settings.PROJECT_ROOT, "test_data")
    SAMPLE_MEDIA_PATH = os.path.join(TEST_DATA_PATH, "media_samples")
    TEST_PK = 1000

    def setUp(self):
        """Before each test is runned run some default actions:
        * clear cache
        * assign mailbox
        """
        super(ExtendedTestCase, self).setUp()
        cache.clear()

        self.mail = mail
        self.mail.outbox = []

    def _process_request(self, url, method, data, follow=False):
        data = data or {}
        if method == "post":
            response = self.client.post(url, data=data, follow=follow)
        else:
            response = self.client.get(url, follow=follow)
        return response

    def _process_messages(self, response):
        """Prepare list of messages from django messaging framework"""
        try:
            storage = response.context["messages"]
        except TypeError:
            messages = []
        else:
            messages = storage._loaded_data

        message_body_list = []
        for message in messages:
            message_body_list.append(message.message)
        return message_body_list

    def assert_auth_required(self, url, method="get", data=None, follow=False):
        """Checks that user is required to login before access given url"""

        response = self._process_request(url, method, data, follow)

        self.assertEqual(response.status_code, 302, response.status_code)
        self.assertTrue(response.has_header("location"))
        location = response.headers["location"]

        response_url = "{login_url}?next={dest_url}".format(
            login_url=settings.LOGIN_URL, dest_url=url
        )
        self.assertTrue(response_url in location, response_url)
        return response

    def assert_auth_required_json(self, url, method="get", data=None, follow=False):
        """
        Checks that user is required to login before access given url.
        This is json response assert.
        """
        response = self._process_request(url, method, data, follow)
        self.assertEqual(
            response.status_code, 200, msg="Authentication should be required here"
        )
        content = json.loads(response.content)
        status = content.get("status") or content.get("success")
        self.assertFalse(status)
        self.assertEqual(content["msg"], "Authentication required", content["msg"])
        return response

    def assert_forbidden(self, url, method="get", data=None, follow=False):
        """Assert that user should not have access to given url"""
        response = self._process_request(url, method, data, follow)
        self.assertEqual(response.status_code, 403, response.status_code)
        return response

    def assert_access_granted(self, url, method="get", data=None, follow=False):
        """Assert that user should have access to given url"""
        response = self._process_request(url, method, data, follow)
        self.assertEqual(response.status_code, 200, response.status_code)
        return response

    def assert_not_allowed(self, url, method="get", data=None, follow=False):
        """Assert that method is not allowed for given url"""
        response = self._process_request(url, method, data, follow)
        self.assertEqual(response.status_code, 405, response.status_code)
        return response

    def assert_redirect(
        self, url, redirection=None, method="get", data=None, follow=False
    ):
        """Assert that user should have access to given url"""
        response = self._process_request(url, method, data, follow=follow)
        self.assertEqual(response.status_code, 302, response.status_code)
        if redirection:
            self.assertTrue(response.has_header("location"))
            location = response.headers["location"]
            self.assertTrue(redirection in location, location)
        return response

    def assert_context_variable(self, response, variable):
        """Assert that response context holds given variable"""
        context = response.context
        self.assertIn(variable, context)
        return context[variable]

    def assert_json_context_variable(self, response, variable):
        """Assert that response content is json and holds given variable"""
        context = json.loads(response.content)
        self.assertIn(variable, context)
        return context[variable]

    def assert_invalid_pk(self, response, field="changed"):
        """Assert that invalid pk is not returned in list of changed
        items when post data is submitted using `pks`"""
        field_data = self.assert_json_context_variable(response, field)
        self.assertNotIn(self.TEST_PK, field_data)

    def assert_has_message(self, response, msg):
        """Check if given message was added using django messages framework"""
        message_list = self._process_messages(response)
        self.assertIn(
            msg,
            message_list,
            u"Message: '{msg}' should be displayed but is not.".format(msg=msg),
        )

    def assert_has_no_message(self, response, msg):
        """
        Check if given message was not added using django messages framework
        """
        message_list = self._process_messages(response)
        self.assertNotIn(
            msg,
            message_list,
            u"Message: '{msg}' should not be displayed but is.".format(msg=msg),
        )


class BaseMediaClassificationTestCase(
    ExtendedTestCase,
    SequenceTestMixin,
    ResearchProjectTestMixin,
    ClassificationProjectTestMixin,
    CollectionTestMixin,
    ClassificationTestMixin,
    SpeciesTestMixin,
    ClassificatorTestMixin,
):
    """
    TODO: docstrings
    """

    resource = None
    collection = None
    research_project = None
    research_collection = None
    classificator = None
    classification_project = None
    classification_collection = None
    classification = None
    user_classification = None

    def setUp(self):
        super().setUp()
        self.summon_alice()
        self.summon_ziutek()

    def _call_helper(
        self, owner, roles=None, status=ClassificationStatus.APPROVED, rp_roles=None
    ):
        """
        Create basic objects.
        """
        self.resource = self.create_resource(owner=owner)
        self.collection = self.create_collection(owner=owner, resources=[self.resource])
        self.research_project = self.create_research_project(
            owner=owner, roles=rp_roles
        )
        self.research_collection = self.create_research_project_collection(
            project=self.research_project, collection=self.collection
        )
        self.classificator = self.create_classificator(owner=owner)
        self.classification_project = self.create_classification_project(
            owner=owner,
            roles=roles,
            research_project=self.research_project,
            classificator=self.classificator,
        )
        self.classification_collection = self.create_classification_project_collection(
            project=self.classification_project, collection=self.research_collection
        )
        self.classification, self.user_classification = self.create_classification(
            resource=self.resource,
            collection=self.classification_collection,
            project=self.classification_project,
            status=status,
            owner=owner,
        )
        self.ai_classification = self.create_ai_classification(
            owner=owner,
            classification=self.classification,
        )

    def get_classification_project_details_url(self, project):
        return reverse("media_classification:project_detail", kwargs={"pk": project.pk})

    def get_classification_project_update_url(self, project):
        return reverse("media_classification:project_update", kwargs={"pk": project.pk})

    def get_classification_project_delete_url(self, project):
        return reverse("media_classification:project_delete", kwargs={"pk": project.pk})

    def get_classification_project_collection_delete_url(self, collection):
        return reverse(
            "media_classification:project_collection_delete",
            kwargs={"pk": collection.pk},
        )

    def get_classification_project_list_url(self):
        return reverse("media_classification:project_list")

    def get_classification_list_url(self, project):
        return reverse(
            "media_classification:classification_list", kwargs={"pk": project.pk}
        )

    def get_classification_detail_url(self, classification, user):
        return reverse(
            "media_classification:classify_user",
            kwargs={"pk": classification.pk, "user_pk": user.pk},
        )

    def get_classification_delete_url(self, classification):
        return reverse(
            "media_classification:classification_delete",
            kwargs={"pk": classification.pk},
        )

    def get_classificator_detail_url(self, classificator):
        return reverse(
            "media_classification:classificator_detail", kwargs={"pk": classificator.pk}
        )

    def get_classificator_update_url(self, classificator):
        return reverse(
            "media_classification:classificator_update", kwargs={"pk": classificator.pk}
        )

    def get_classificator_delete_url(self, classificator):
        return reverse(
            "media_classification:classificator_delete", kwargs={"pk": classificator.pk}
        )

    def get_classificator_clone_url(self, classificator):
        return reverse(
            "media_classification:classificator_clone", kwargs={"pk": classificator.pk}
        )

    def get_classificator_list_url(self):
        return reverse("media_classification:classificator_list")

    def get_classificator_list_api_url(self):
        return reverse("media_classification:api-classificator-list")

    def get_classification_export_url(self, project):
        return reverse(
            "media_classification:classification_export", kwargs={"pk": project.pk}
        )
