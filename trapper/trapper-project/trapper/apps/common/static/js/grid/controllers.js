'use strict';
(function(window, angular) {

    const trapperGrid = angular.module('trapperGrid');

    /* TRAPPER CUSTOM MODULES */
    const alert = window.TrapperApp.Alert,
        modal = window.TrapperApp.Modal,
        plugins = window.TrapperApp.Plugins;


    /* GRID CONTROLLER */
    trapperGrid.controller(
	'GridController', ['$window', '$scope', 'config', 'dataService', 'filterService',
        function($window, $scope, cfg, dataService, filterService) {

            $scope.data = {
                records: [],
                selected: [],
                request: {
                    url: '',
                    queryParams: {},
                },
                selectedCounter: 0,
                total: 0,
            };
            $scope.filters = {};

            $scope.pageSizes = cfg.grid.pageSizes;
            $scope.optionList = cfg.optionList;
            $scope.dateFormat = cfg.dateFormat;
            $scope.status = {
                error: false,
                loading: false
            };
            $scope.listStatuses = cfg.listStatuses;
            $scope.columns = cfg.columns;

        // sorting
            $scope.reverse = true;
            $scope.orderRecords = function (propertyName) {
                $scope.reverse = !$scope.reverse;
                $scope.propertyName = propertyName;
                let queryParams = angular.extend({}, $scope.data.request.queryParams,
                    {'sort_by': propertyName, 'reverse': $scope.reverse})
                $scope.load(
                    $scope.data.request.url, queryParams, false
                );
            };

	    // gallery
	    $scope.gallery = {
		initialized: false,
		$gallery: null 
	    };

	    $scope.openGallery = function($event) {
		if (!$scope.gallery.initialized) {
            const $container = plugins.gallery('#lightgallery', '.img-thumbnail');
            $scope.gallery.initialized = true;
		    $scope.gallery.$gallery = $container;
		    $event.target.click();
		}
	    };
	    
            $scope.filter = filterService.create(function(updated) {
                if (updated === 'clear') {
                    $scope.data.total = 0;
                    $scope.data.request.queryParams = {};
                    $scope.map_filters_applied = false;
                    $scope.$broadcast('filters:clear');
                    $scope.load();
                }
            });

            $scope.flattenFilters = function flattenObject(ob) {
                const toReturn = {};
                for (let i in ob) {
                    if (!ob.hasOwnProperty(i)) continue;
                    if (ob[i] === null) {
                        continue;
                    }
                    if ((typeof ob[i]) == 'object' && ob[i].constructor !== Array) {
                        const flatObject = flattenObject(ob[i]);
                        for (let x in flatObject) {
                            if (!flatObject.hasOwnProperty(x)) continue;

                            toReturn[i + '_' + x] = flatObject[x];
                        }
                    } else {
                        toReturn[i] = ob[i];
                    }
                }
                return toReturn;
            };

            $scope.select = function($event, record) {
                if ($event && ['a', 'button'].indexOf($event.target.tagName.toLowerCase()) > -1) {
                    return;
                }
                record.$selected = !record.$selected;
                if (record.$selected) {
                    $scope.data.selected.push(record.pk);
                    $scope.data.selectedCounter++;
                } else {
                    const index = $scope.data.selected.indexOf(record.pk);
                    $scope.data.selected.splice(index, 1);
                    $scope.data.selectedCounter--;
                }
            };

            $scope.selectPage = function() {
                $scope.data.records.forEach(
                    function(item) {
                        if ($scope.data.selected.indexOf(item.pk) < 0) {
                            item.$selected = true;
                            $scope.data.selected.push(item.pk);
                            $scope.data.selectedCounter++;
                        }
                    }
                );
            };

            $scope.selectAllFiltered = function() {
                $scope.status.error = false;
                $scope.status.loading = true;
                const queryParams = (angular.extend({}, $scope.data.request.queryParams, {'all_filtered': true}));
                dataService.load(
                    $scope.data.request.url, queryParams, false
                ).then(function(data) {
		    if (data.length === 0) {
			alert.error(
                            global['Translations']['GRID']['CONTROLLERS']['select100k']
			);
		    } else {
			$scope.data.selected = data;
			$scope.data.selectedCounter = data.length;
			$scope.data.records.forEach(
                            function(item) {
				if ($scope.data.selected.indexOf(item.pk) > -1) {
                                    item.$selected = true;
				}
                            }
			);
		    }
                }).catch(function() {
                    $scope.status.error = true;
                }).finally(function() {
                    $scope.status.loading = false;
                });
            };

            $scope.clearSelection = function() {
                $scope.data.selected = [];
                $scope.data.records.forEach(
                    function(item) {
                        item.$selected = false;
                    }
                );
                $scope.data.selectedCounter = 0;
            };

            $scope.filterOwner = function(forceAll = false) {
                $scope.filters.owner = !$scope.filters.owner;
                $scope.load($scope.data.request.url, {}, true, undefined, forceAll);
            };

            $scope.load = function(url, queryParams, filterData, pagination, forceAll) {
                url = (
                    typeof url === 'undefined'
                ) ? $scope.data.request.url : url;
                queryParams = (
                    typeof queryParams === 'undefined'
                ) ? {} : queryParams;
                filterData = (
                    typeof filterData === 'undefined'
                ) ? false : filterData;
                pagination = (
                    typeof pagination === 'undefined'
                ) ? true : pagination;
                $scope.status.error = false;
                $scope.status.loading = true;
                queryParams = (angular.extend(
                    $scope.data.request.queryParams, queryParams
                ));

                if ((url === '/research/api/projects' || url === '/media_classification/api/projects') && !forceAll) {
                    $scope.filters.owner = true;
                    filterData = true;
                }

                if (filterData) {
                    queryParams = angular.extend(
                        queryParams, $scope.flattenFilters($scope.filters), {page: 1}
                    );
                }

                // Filter items by location's pks sent from the map view  
                const locations = $window.localStorage.getItem('trapper.geomap.locations');
                $window.localStorage.removeItem('trapper.geomap.locations');

                if (locations) {
                    $scope.map_filters_applied = true;
                    queryParams = angular.extend(
                        queryParams, { locations_map: locations }
                    );
                }

                $scope.data.request = {
                    url: url,
                    queryParams: queryParams,
                };

                dataService.load(url, queryParams, pagination).then(function(data) {
                    if (pagination) {
                        $scope.data.records = data.results;
                        $scope.data.pagination = data.pagination;

                        if ($scope.data.total === 0) {
                            $scope.data.total = $scope.data.pagination.count;
                        }
                    } else {
                        $scope.data.records = data;
                    }

                    if ($scope.data.selectedCounter > 0) {
                        $scope.data.records.forEach(
                            function(item) {
                                if ($scope.data.selected.indexOf(item.pk) > -1) {
                                    item.$selected = true;
                                }
                            }
                        );
                    }
		    angular.element(document).ready(function () {
			if ($scope.gallery.initialized) {
			    $scope.gallery.$gallery.data("lightGallery").destroy(true);
			    $scope.gallery.initialized = false;
			}
		    });
                }).catch(function() {
                    $scope.status.error = true;
                }).finally(function() {
                    $scope.status.loading = false;
                    // remove clear_cache param if present
                    delete $scope.data.request.queryParams.clear_cache;
                });
            };

            $scope.remove = function(url, records) {
                alert.info(
                    global['Translations']['GRID']['CONTROLLERS']['dont_leave_page']
                );
                $scope.status.deleting = true;
                dataService.remove(url, records).then(function(response) {
                    if (response.data.status) {
                        alert.success(response.data.msg);
                    } else {
                        alert.error(response.data.msg);
                    }
                    $scope.clearSelection();
                    $scope.status.deleting = false;
                    $scope.load();
                }).catch(function(error) {
                    alert.error(error || global['Translations']['GRID']['CONTROLLERS']['records_couldnt_delete']);
                    $scope.status.deleting = false;
                });
            };
        }
    ]);



    /* SEQUENCES CONTROLLER */

    trapperGrid.controller(
	'SequenceController', ['$interpolate', '$window', '$scope', 'config', 'dataService', 'filterService', '$http', '$controller',
	function($interpolate, $window, $scope, cfg, dataService, filterService, $http, $controller) {
						      
            angular.extend(this, $controller('GridController', { $scope: $scope }));

            $scope.sequenceSizes = [2, 5, 10, 50, 100, 500];
            $scope.filters = {
		size: 5,
                page_size: 50
            };
            $scope.sequences = {
                original: [],
                selected: null
            };
            $scope.data.request = {
                queryParams: {},
                urlRes: '',
                urlSeq: '',
		urlForm: '',
            };
	    
            $scope.load_resources = function(url, queryParams, filterData) {
                url = (
                    typeof url === 'undefined'
                ) ? $scope.data.request.urlRes : url;
                queryParams = (
                    typeof queryParams === 'undefined'
                ) ? {} : queryParams;
                filterData = (
                    typeof filterData === 'undefined'
                ) ? false : filterData;
                $scope.status.error = false;
                $scope.status.loading = true;
                queryParams = angular.extend(
                    $scope.data.request.queryParams, queryParams
                );
                if (filterData) {
                    queryParams = angular.extend(
                        queryParams, $scope.flattenFilters($scope.filters), {page: 1}
                    );
                }
                $scope.data.request.urlRes = url;
                $scope.data.request.queryParams = queryParams;
		
                dataService.load(url, queryParams, true).then(function(data) {
                    $scope.data.records = data.results;
                    $scope.data.pagination = data.pagination;
                    if ($scope.data.selectedCounter > 0) {
                        $scope.data.records.forEach(
                            function(item) {
                                if ($scope.data.selected.indexOf(item.pk) > -1) {
                                    item.$selected = true;
                                }
                            }
                        );
                    }
		    // remove clear_cache param if present
		    delete $scope.data.request.queryParams.clear_cache;
		    
		    angular.element(document).ready(function () {
			$('.btn-current').trigger('click');
			$scope.status.loading = false;
			if ($scope.gallery.initialized) {
			    $scope.gallery.$gallery.data("lightGallery").destroy(true);
			    $scope.gallery.initialized = false;
			}
		    });
                }).catch(function() {
                    $scope.status.error = true;
                });
            };

            $scope.load_sequences = function(url) {
                url = (
                    typeof url === 'undefined'
                ) ? $scope.data.request.urlSeq : url;
                $scope.status.error = false;
                $scope.status.loading = true;
                $scope.data.request.urlSeq = url;
                const params = {
                    deployment: $scope.filters.deployment
                };
                dataService.load(url, params, false).then(
                    function(data) {
                        $scope.sequences.original = [
                            { blank: true, sequence_id: 'New', resources: [], description: '' }
                        ].concat(data);
                        $scope.sequences.selected = $scope.sequences.original[0];
                    }).catch(function() {
			$scope.status.error = true;
                    }).finally(function() {
			$scope.status.loading = false;
                    });
            };

            $scope.load = function(urlRes, urlSeq, collection_pk, current) {
                $scope.status.loading = true;
                $scope.data.collection_pk = collection_pk;
                $scope.current = current;
                const filters_data = $scope.getSavedFilters();
                if (filters_data) {
                    $scope.filters = angular.extend(
                        $scope.filters, filters_data.filters);
                }
                $scope.load_sequences(urlSeq);
                $scope.load_resources(urlRes, {}, true);
                setTimeout(function() {
                    $scope.filter.rebuildFilters();
                }, 0);
            };

            $scope.reload = function() {
                $scope.load_sequences();
                $scope.load_resources(
                    $scope.data.request.Resurl,
                    $scope.data.request.queryParams,
                    true
                );
            };

	    $scope.classFormHtml = null;	    
	    $scope.loadClassForm = function(url) {
		url = (
		    typeof url === 'undefined'
                ) ? $scope.data.request.urlForm : url;
                $scope.data.request.urlForm = url;
                dataService.load(url, {}, false).then(
                    function(data) {
			if (!data.status) {
                            alert.error(data.msg);
                        } else {
			    $scope.classFormHtml = data.form_html
			    angular.element(document).ready(function () {
				plugins.select2();
				plugins.dynamicTable('#table-repeatable');
				plugins.dynamicTabs('#tabs-repeatable');
				$('a#classify').click(function() {
				    $scope.submitClassForm(false);
				});
				$('a#approve').click(function() {
				    $scope.submitClassForm(true)
				});
			    });		    
			    if (data.msg) {
				alert.success(data.msg);
			    }
                        }
                    }).catch(function() {
			$scope.status.error = true;
                    });
	    };
	    
	    $scope.submitClassForm = function(approve) {
		// update form with selected resources
		$('#resources-pks').val(getSelected());
            const url = $scope.data.request.urlForm;
            const data = $('#classify-form').serializeArray();
            if (approve) {
		    data.push({name: 'approve', value: approve});
		}
		dataService.post(url, data).then(
                    function(data) {
			if (!data.status) {
                            alert.error(data.msg);
                        } else {
			    $scope.clearSelection();
			    alert.success(data.msg);
			    // workaround to update the box with user classifications
			    setTimeout(function() {
				location.reload();
			    }, 1000);
			    //$scope.reload();
                        }
			if (data.form_html) {
			    // String(Math.random()) is a temporary workaround to force
			    // html binding watch directive to always compile html response 
			    $scope.classFormHtml = data.form_html +
				'<span style="display:none">' + String(Math.random()) + '</span>';
			    angular.element(document).ready(function () {
				plugins.dynamicTable('#table-repeatable');
				plugins.dynamicTabs('#tabs-repeatable');
				$('a#classify').click(function() {
				    $scope.submitClassForm(false);
				});
				$('a#approve').click(function() {
				    $scope.submitClassForm(true)
				});
			    });
			}
                    }).catch(function() {
			$scope.status.error = true;
                    });
	    };
	    
            function getSelected() {
                return $scope.data.selected.join(',');
            }

            $scope.select = function($event, record) {
                if ($event && ['a', 'button', 'img'].indexOf($event.target.tagName.toLowerCase()) > -1) {
                    return;
                }
                record.$selected = !record.$selected;
                if (record.$selected) {
                    $scope.data.selected.push(record.pk);
                    $scope.data.selectedCounter++;
                } else {
                    const index = $scope.data.selected.indexOf(record.pk);
                    $scope.data.selected.splice(index, 1);
                    $scope.data.selectedCounter--;
                }
                const selected = getSelected();
                $('#id_selected_resources').val(selected);
            };

            $scope.clearSelection = function() {
                $scope.data.selected = [];
                $scope.data.records.forEach(
                    function(item) {
                        item.$selected = false;
                    }
                );
                $scope.data.selectedCounter = 0;
                $('#id_selected_resources').val('');
		$scope.sequences.selected = $scope.sequences.original[0];
            };

            $scope.sequenceChanged = function() {
                const s = $scope.sequences.selected;
                $scope.data.records.forEach(function(seq) {
                    if (s.resources.indexOf(seq.pk) > -1) {
                        seq.$selected = true;
                    } else {
                        seq.$selected = false;
                    }
                });
                $scope.data.selected = [];
                $scope.data.selectedCounter = s.resources.length;
                s.resources.forEach(
                    function(item) {
                        $scope.data.selected.push(item);
                    }
                );
                const selected = getSelected();
                $('#id_selected_resources').val(selected);
            };

	    $scope.sequenceSelected = function($event, sequence) {
		setTimeout(function() {
		    $('#sequence-picker').val(sequence).trigger('change');
		});
	    };

            $scope.filter = filterService.create(function(updated) {
                if (updated === 'clear') {
                    localStorage.removeItem('trapper.classify.settings');
                    $scope.$broadcast('filters:clear');
                    setTimeout(function() {
                        $scope.reload();
                    }, 0);
                    alert.info(global['Translations']['GRID']['CONTROLLERS']['filters_cleared']);
                }
                if (updated === 'rebuild') {
                    $scope.$broadcast('filters:rebuild');
                }
            });

            $scope.saveFilters = function() {
                const filters_data = {
                    collection_pk: $scope.data.collection_pk,
                    filters: $scope.filters,
                };
                localStorage.setItem('trapper.classify.settings', JSON.stringify(filters_data));
                alert.info(global['Translations']['GRID']['CONTROLLERS']['filters_saved']);
            };

            $scope.getSavedFilters = function() {
                const filters_data = JSON.parse(localStorage.getItem('trapper.classify.settings'));
                if (filters_data && filters_data.collection_pk === $scope.data.collection_pk) {
                    return filters_data;
                } else {
                    return null;
                }
            };

            $scope.saveSequence = function(url) {
                const s = $scope.sequences.selected;
                const resources = getSelected();
                if (!resources.length) {
                    alert.warning(global['Translations']['GRID']['CONTROLLERS']['empty_sequence']);
                    return;
                }
                $http({
                    method: 'POST',
                    url: url,
                    data: {
                        pk: s.pk,
                        description: s.description,
                        resources: resources,
                        collection_pk: $scope.data.collection_pk,
                    }
                }).then(function(response) {
                    const seq = response.data.record;
                    if (!seq) {
                        alert.error(global['Translations']['GRID']['CONTROLLERS']['couldnt_create_sequence'] + response.data.msg);
                        return;
                    }
		    if (s.blank) {
			$scope.sequences.original.push(response.data.record);
		    }
		    $scope.sequences.selected = seq;
                    $scope.load_resources();
                    alert.info(global['Translations']['GRID']['CONTROLLERS']['sequence_saved']);
                });
            };

            $scope.deleteSequence = function(url) {
                const s = $scope.sequences.selected;
                if (s.blank) {
                    alert.warning(global['Translations']['GRID']['CONTROLLERS']['couldnt_delete_sequence']);
                    return;
                }
                modal.confirm({
                    title: global['Translations']['GRID']['CONTROLLERS']['confirm_delete_sequence'],
                    content: s.sequence_id,
                    buttons: [{
                        label: global['Translations']['GLOBAL']['delete'],
                        type: 'danger',
                        onClick: function() {
                            $http({
                                method: 'POST',
                                url: url,
                                data: {
                                    pks: s.pk
                                }
                            }).then(function(response) {
                                if (!response.data.status) {
                                    alert.error(global['Translations']['GRID']['CONTROLLERS']['couldnt_delete_sequence']);
                                    return;
                                }
                                const index = $scope.sequences.original.indexOf(s);
                                $scope.sequences.original.splice(index, 1);
                                $scope.sequences.selected = $scope.sequences.original[0]; // the blank one
                                $scope.reload();
                                alert.info(global['Translations']['GRID']['CONTROLLERS']['sequence_deleted']);
                            });
			}
		    }, {
                        label: global['Translations']['GLOBAL']['cancel'],
                        type: 'default'
                    }]
	    	});
	    };

        }
			       
    ]);
}(window, angular));
