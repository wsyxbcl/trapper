'use strict';

(function(global, namespace, moduleName) {

    var module = {};

    var plugins = global[namespace].Plugins;
    var alert = global[namespace].Alert;
    var modal = global[namespace].Modal;
    var uploader = global[namespace].Uploader;

    function createCookie(name, value, days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toUTCString();
        } else var expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }

    function eraseCookie(name) {
        createCookie(name, "", -1);
    }

    function checkCookie(cookie, value) {
        if (readCookie(cookie) === value) {
            return "0";
        } else {
            return "1";
        }
    }

    window.onload = initLanguageSelect;

    function initLanguageSelect() {
        document.querySelector('.language-select-container').selectedIndex = checkCookie("trapper_language", "pl");
        document.querySelector('.language-select-container').addEventListener("change", (event) => {
            createCookie("trapper_language", event.target.value, 180);
            window.location.reload(true);
        });
    }

    // if passed namespace does not exist, create one
    global[namespace] = global[namespace] || {};

    // append module to given namespace
    global[namespace][moduleName] = module;

}(window, 'TrapperApp', 'Language'));