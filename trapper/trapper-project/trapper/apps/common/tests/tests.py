# -*- coding: utf-8 -*-
import json

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from requests.auth import HTTPBasicAuth
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient, APITestCase, RequestsClient

from trapper.apps.common.taxonomy import BaseTaxonomy
from trapper.apps.common.tools import parse_pks, clean_html, json_loads_helper
from trapper.apps.common.utils.identity import base_decode
from trapper.apps.common.utils.test_tools import ExtendedTestCase


class ParsePksTestCase(ExtendedTestCase):
    """Test case for method that is used to convert in most cases
    request.POST param `pks` that is used later to retrieve values
    from database"""

    def test_invalid_type(self):
        """`parse_pks` can handle invalid types by returning empty list"""
        self.assertEqual(parse_pks(None), [])
        self.assertEqual(parse_pks([]), [])
        self.assertEqual(parse_pks(()), [])
        self.assertEqual(parse_pks(b"test"), [])

    def test_empty(self):
        """`parse_pks` can handle empty string by returning empty list"""
        self.assertEqual(parse_pks(""), [])
        self.assertEqual(parse_pks(u""), [])

    def test_invalid_separator(self):
        """`parse_pks` can handle invalid separator by returning empty list"""
        self.assertEqual(parse_pks("1:2"), [])
        self.assertEqual(parse_pks("1;2"), [])

    def test_invalid_items(self):
        """`parse_pks` strip non-int values"""
        self.assertEqual(parse_pks("1,2,a,b"), [1, 2])
        self.assertEqual(parse_pks("a,b,1,2"), [1, 2])
        self.assertEqual(parse_pks("1,a,b,2"), [1, 2])
        self.assertEqual(parse_pks("a,1,b,2"), [1, 2])
        self.assertEqual(parse_pks("a,b"), [])

    def test_items(self):
        """`parse_pks` will retrun in list all values that are integers.
        Extra spaces are allowed
        """
        self.assertEqual(parse_pks("1"), [1])
        self.assertEqual(parse_pks("1,2"), [1, 2])
        self.assertEqual(parse_pks("1, 2, 3,"), [1, 2, 3])
        self.assertEqual(parse_pks("-1, 1"), [-1, 1])
        self.assertEqual(parse_pks("1, 1"), [1, 1])


class CleanHtmlTestCase(ExtendedTestCase):
    """Tests related to function that is used in `SafeTextField` to
    ensure safety of data stored in field"""

    def test_clean(self):
        """Potentialy dangerous code is stripped off"""
        self.assertEqual(clean_html(""), "")
        self.assertEqual(clean_html("<p></p>"), "")
        self.assertEqual(clean_html("<p>text</p>"), "text")
        self.assertEqual(clean_html('<p style="text-align: right;"></p>'), "")
        self.assertEqual(clean_html('<p style="text-align: right;">text</p>'), "text")
        self.assertEqual(
            clean_html('<script type="text/javascript">alert("a";);</script>'),
            "<div></div>",
        )
        self.assertEqual(
            clean_html("<p><strong>text</strong></p>"), "<strong>text</strong>"
        )
        self.assertEqual(
            clean_html('<p><strong style="color: red;">text</strong></p>'),
            "<strong>text</strong>",
        )


class IdentityTest(TestCase):
    def test_identity_base_decode(self):
        self.assertEqual(26086738530, base_decode("string"))


class TaxonomyTest(TestCase):
    def test_taxonomy_get_filter_choices(self):
        taxonomy = BaseTaxonomy()
        taxonomy.get_filter_choices()


class ToolsTests(TestCase):
    def test_json_loads_helper(self):
        dict_value = {"test": "abc"}
        json_value = json.dumps(dict_value)
        json_loads_helper_value = json_loads_helper(json_value)
        self.assertEqual(json_loads_helper_value, dict_value)


class APIAuthenticationTestCase(APITestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.username = "user1"
        cls.password_str = "secret_password"
        User = get_user_model()
        cls.user = User.objects.create_user(
            cls.username, password=cls.password_str, is_active=True
        )
        cls.url = reverse("accounts:test_api_view")

    def test_no_credentials(self):
        client = APIClient()
        client.credentials()
        response = client.get(self.url)
        self.assertEqual(response.status_code, 403)

    def test_session_auth(self):
        client = APIClient()
        logged_in = client.login(username=self.username, password=self.password_str)
        self.assertTrue(logged_in)

        response = client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_token_auth(self):
        token = Token.objects.create(user=self.user)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Token " + token.key)
        response = client.get(self.url)
        self.assertEqual(response.status_code, 200)
