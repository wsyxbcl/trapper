# -*- coding: utf-8 -*-
"""Views related to **Django Rest Framework** application that are used
in other applications to define REST API"""
import hashlib

from django.conf import settings
from django.core.cache import caches
from django.core.paginator import Paginator
from django.core.exceptions import EmptyResultSet
from django.utils.functional import cached_property
from django_filters.rest_framework import DjangoFilterBackend

from rest_framework import pagination, permissions
from rest_framework import renderers
from rest_framework import viewsets
from rest_framework.filters import SearchFilter
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from rest_framework.views import APIView

from trapper.middleware import get_current_user


class TrapperPaginator(Paginator):
    """
    TODO: docstrings
    """

    cache = caches["default"]
    timeout = getattr(settings, "CACHE_COUNT_TIMEOUT", 60 * 60)

    @cached_property
    def count(self):
        """
        Returns the total number of objects, across all pages.
        """
        try:
            user = get_current_user()
            cache_key = (
                "api-count:"
                + user.username
                + ":"
                + hashlib.md5(str(self.object_list.query).encode("utf8")).hexdigest()
            )
            # return existing value, if any
            value = self.cache.get(cache_key)
            if value is not None:
                return value
            # cache new value
            value = self.object_list.select_related(None).count()
            self.cache.set(cache_key, value, self.timeout)
            return value
        except (AttributeError, TypeError, EmptyResultSet):
            # AttributeError if object_list has no count() method.
            # TypeError if object_list.count() requires arguments
            # (i.e. is of type list).
            return len(self.object_list)


class ListPagination(pagination.PageNumberPagination):
    page_size = 10
    page_size_query_param = "page_size"
    max_page_size = 500
    django_paginator_class = TrapperPaginator

    def get_paginated_response(self, data):
        return Response(
            {
                "pagination": {
                    "page": self.page.number,
                    "page_size": self.page.paginator.per_page,
                    "pages": self.page.paginator.num_pages,
                    "count": self.page.paginator.count,
                },
                "results": data,
            }
        )


class PaginatedReadOnlyModelViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Base class for readonly ModelViewSets.
    """

    # Pagination
    pagination_class = ListPagination
    # filter backends
    filter_backends = (DjangoFilterBackend, SearchFilter)

    def list(self, request, *args, **kwargs):
        """
        TODO: docstrings
        """
        qs = self.get_queryset()
        qs_f = self.filter_queryset(qs)

        if self.request.GET.get("all_filtered", None):
            if qs_f.count() > 100000:
                return Response([])
            return Response(qs_f.values_list("pk", flat=True))

        # clear user's cache if requested
        if self.request.GET.get("clear_cache", None):
            cache = caches["default"]
            cache_count_key = (
                "api-count:"
                + request.user.username
                + ":"
                + hashlib.md5(str(qs_f.query).encode("utf8")).hexdigest()
            )
            cache.delete(cache_count_key)

        sort_by = self.request.GET.get("sort_by")
        if sort_by:
            reverse = self.request.GET.get("reverse") == "true"
            sort_by_str = "-" + sort_by if reverse else sort_by
            qs_f = qs_f.order_by(sort_by_str)

        page = self.paginate_queryset(qs_f)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(qs_f, many=True)
        return Response(serializer.data)


class PlainTextRenderer(renderers.BaseRenderer):
    media_type = "text/plain"
    format = "txt"

    def render(self, data, media_type=None, renderer_context=None):
        return data


class TestAPIView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        return Response(status=HTTP_200_OK)


test_api_view = TestAPIView.as_view()
