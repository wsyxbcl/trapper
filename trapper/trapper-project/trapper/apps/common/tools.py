# -*- coding: utf-8 -*-
"""
Helper functions to decode structured data strored by django-hstore fields
"""
import datetime
import json

import lxml.html.clean as clean
from django.utils import timezone
import numpy as np


def json_loads_helper(obj):
    """Try convert given object into json"""
    try:
        return json.loads(obj)
    except (TypeError, ValueError):
        return obj


def parse_hstore_field(data):
    """Convert hstore value stored in database into python dictionary"""
    return dict(map(lambda kv: (kv[0], json_loads_helper(kv[1])), data.iteritems()))


def datetime_aware(data=None):
    """
    Make datetime object timezone aware.
    By default return timezone aware datatime.datetime.now()
    """
    if data is None:
        data = datetime.datetime.now()
    return timezone.make_aware(data, timezone.get_current_timezone())


def parse_pks(pks):
    """Method used to parse string with comma separated numbers"""
    output = []
    if isinstance(pks, str):
        for value in pks.split(","):
            try:
                value = int(value.strip())
            except ValueError:
                pass
            else:
                output.append(value)
    return output


def clean_html(value):
    """
    Clean html value and strip potentially dangerous code using
    :class:`lxml.html.clean.Cleaner`
    """
    cleaned = ""
    if value and type(value) is str and value.strip():
        cleaner = clean.Cleaner(safe_attrs_only=True, safe_attrs=frozenset(["href"]))
        cleaned = cleaner.clean_html(value)
        # Cleaner wraps with p tag, it should be removed
        if cleaned.startswith("<p>") and cleaned.endswith("</p>"):
            cleaned = cleaned[3:-4]
    return cleaned


def df_to_geojson(df, properties, lat="latitude", lon="longitude"):
    geojson = {"type": "FeatureCollection", "features": []}
    for _, row in df.iterrows():
        feature = {
            "type": "Feature",
            "properties": {},
            "geometry": {"type": "Point", "coordinates": []},
        }
        feature["geometry"]["coordinates"] = [row[lon], row[lat]]
        for prop in properties:
            feature["properties"][prop] = row[prop]
        geojson["features"].append(feature)
    return geojson


def aggregate_results(
    df_obs,
    df_dep,
    agg_source_level="media",
    agg_target_level="deployment",
    count_var="count",
    count_fun="sum",
    event_fun="max",
    min_days=1,
):
    # OBSERVATIONS AGGREGATION

    # remove blank and unclassified observations if present
    df_obs = df_obs[~df_obs.observationType.isin(["blank", "unclassified"])]

    # filter by observation level
    df_obs = df_obs[df_obs.observationLevel == agg_source_level]

    # replace empty strings in count_var column with NaN and fill NaN with 0
    df_obs[count_var].replace("", np.nan, inplace=True)
    df_obs[count_var].fillna(0.0, inplace=True)
    df_obs[count_var] = df_obs[count_var].astype(float)

    group_by_columns = [
        "deploymentID",
        "eventID",
        "observationType",
        "scientificName",
    ]

    if agg_source_level == "media":
        # first aggregate at a single-media event level (always sum)
        g0 = (
            df_obs.groupby(["mediaID", "eventStart", "eventEnd"] + group_by_columns)
            .agg(
                {
                    count_var: "sum",
                }
            )
            .reset_index()
        )

        g1 = (
            g0.groupby(group_by_columns)
            .agg(
                {
                    count_var: event_fun,
                    "eventStart": "min",
                    "eventEnd": "max",
                }
            )
            .reset_index()
        )
    else:
        g1 = df_obs[group_by_columns + [count_var, "eventStart", "eventEnd"]]

    g1.rename(columns={count_var: "count"}, inplace=True)

    # DEPLOYMENTS/LOCATIONS AGGREGATION

    # remove deployments with days < min_days
    df_dep = df_dep[df_dep.days > min_days]

    if agg_target_level == "location":
        # estimate correct number of days per location and add as a new column "location_days" to df_dep
        df_dep["location_days"] = df_dep.groupby("locationID")["days"].transform("sum")

    # remove eventID from group_by_columns
    group_by_columns.remove("eventID")

    g2 = (
        g1.groupby(group_by_columns)
        .agg(
            {
                "count": count_fun,
            }
        )
        .reset_index()
    )

    deployments_agg = df_dep.merge(g2, how="left", on="deploymentID")

    if agg_target_level == "location":
        group_by_columns[0] = "locationID"
        deployments_agg = (
            deployments_agg.groupby(group_by_columns)
            .agg(
                {
                    "count": count_fun,
                    "location_days": "first",
                    "longitude": "first",
                    "latitude": "first",
                }
            )
            .reset_index()
        )
        # rename location_days to days
        deployments_agg.rename(columns={"location_days": "days"}, inplace=True)

    deployments_agg.drop_duplicates(inplace=True)
    deployments_agg["count"].fillna(0, inplace=True)
    deployments_agg["trapRate"] = deployments_agg["count"] / deployments_agg["days"]
    deployments_agg.trapRate.fillna(0, inplace=True)

    # replace all remaining NaNs with ""
    deployments_agg.replace(np.nan, "", inplace=True)

    # round trapRate to 5 decimal places
    deployments_agg.trapRate = deployments_agg.trapRate.round(5)

    return deployments_agg


def is_request_ajax(request):
    return request.META.get("HTTP_X_REQUESTED_WITH") == "XMLHttpRequest"
