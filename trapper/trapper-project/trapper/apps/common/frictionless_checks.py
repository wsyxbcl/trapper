from frictionless import Check, errors


class DbExistingObjectsRowError(errors.CellError):
    code = "db-existing-objects-row"
    name = "Database objects"
    tags = ["#table", "#row", "#dbobjects"]
    template = (
        'Type error in the cell "{cell}" in row "{rowPosition}" and field "{fieldName}" '
        'at position "{fieldPosition}": {note}'
    )
    description = ""


class DbExistingObjects(Check):
    code = "db-existing-objects"
    Errors = [DbExistingObjectsRowError]

    def __init__(self, field_name=None, values=None, check_type=None):
        super().__init__()
        self.field_name = field_name
        self.values = [str(k) for k in values]
        self.check_type = check_type

    def validate_row(self, row):
        value = row.get(self.field_name)
        condition = str(value) in self.values
        # blacklist
        if self.check_type == "blacklist":
            if condition is True:
                note = "The object exists in the database."
                yield DbExistingObjectsRowError.from_row(
                    row, note=note, field_name=self.field_name
                )
        # whitelist
        if self.check_type == "whitelist":
            if condition is False:
                note = "The object does not exist in the database."
                yield DbExistingObjectsRowError.from_row(
                    row, note=note, field_name=self.field_name
                )
