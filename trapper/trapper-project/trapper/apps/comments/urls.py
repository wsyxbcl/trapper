# -*- coding: utf-8 -*-

from django.urls import re_path

from trapper.apps.comments import views as comments_views

app_name = "comments"

urlpatterns = [
    re_path(r"^create/$", comments_views.view_comment_create, name="comment_create"),
    re_path(
        r"^delete/(?P<pk>\d+)/$",
        comments_views.view_comment_delete,
        name="comment_delete",
    ),
]
