from django.views import generic

from trapper.apps.common.views import LoginRequiredMixin


class JupyterLiteLabView(LoginRequiredMixin, generic.RedirectView):
    """"""

    url = "/static/jupyterlite/lab/?path=dashboard.ipynb"


view_jupyterlite_lab = JupyterLiteLabView.as_view()
