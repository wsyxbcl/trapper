from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class JupyterliteConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "trapper.apps.jupyterlite"
    verbose_name = _("Jupyter Lite")
