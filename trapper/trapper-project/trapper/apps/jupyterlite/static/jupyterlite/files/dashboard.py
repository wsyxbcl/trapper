"""
Inspired by:
https://github.com/awesome-panel/awesome-panel/blob/master/examples/volume_profile_analysis.py
"""
from io import StringIO

import pandas as pd
import panel as pn
import param
from panel.io.loading import start_loading_spinner, stop_loading_spinner

import utils

pn.extension("tabulator", notifications=True)
pn.config.notifications = True

ACCENT_COLOR = "#C01754"

# taxonomies
TAX_OBSERVATION_TYPE = ["animal", "human", "vehicle", "blank"]
TAX_TABLES = ["observations", "media", "deployments"]

CURRENT_LAYER_NAME = "Current (based on applied filters)"

# -----------------------------------------------------------------------------
# Utility functions
# -----------------------------------------------------------------------------


async def _get_data(*args, **kwargs) -> utils.TrapperPackage:
    return await utils.get_package(*args, **kwargs)


def _transform_data(raw_data: utils.TrapperPackage) -> utils.TrapperPackage:
    data = raw_data
    return data


# -----------------------------------------------------------------------------
# Dashboard sections - Abstract Base Classes
# -----------------------------------------------------------------------------


class BaseSection(param.Parameterized):
    """Abstract Class for Section of App"""

    data = param.ClassSelector(class_=utils.TrapperPackage)
    data_view = param.DataFrame()
    data_agg = param.ClassSelector(class_=utils.TrapperPackageAgg)
    data_agg_view = param.DataFrame()
    loading = param.Boolean(default=False)
    view = param.ClassSelector(class_=pn.Column)

    filter_observation_type = param.ListSelector(
        [], TAX_OBSERVATION_TYPE, label="Observation Type"
    )
    filter_species = param.ListSelector([], [], label="Species")

    notification = param.Tuple(default=None, length=2, precedence=-1)

    def __init__(self, **params):
        super().__init__(**params)
        self._init_view()

    @pn.depends("notification", watch=True)
    def _show_notification(self):
        if self.notification:
            getattr(pn.state.notifications, self.notification[1])(self.notification[0])
            self.notification = None

    def _init_view(self):
        raise NotImplementedError()

    def _update_view(self, *events):
        raise NotImplementedError()

    @pn.depends("loading", watch=True)
    def _update_loading(self):
        if self.loading:
            start_loading_spinner(self.view)
        else:
            stop_loading_spinner(self.view)


# -----------------------------------------------------------------------------
# Dashboard sections - Load Data
# -----------------------------------------------------------------------------


class LoadDataSection(BaseSection):
    """Section describing the loading of data"""

    # data loading controls
    project_id = param.Integer(default=1, label="Project ID (integer)")
    export_format = param.ObjectSelector("camtrapdp", ["camtrapdp", "trapper"])
    clear_cache = param.Boolean(default=False, label="Clear Cache")
    exclude_blank = param.Boolean(default=True, label="Exclude Blank")
    all_deployments = param.Boolean(
        default=False, label="All Deployments (from Research Project)"
    )
    filter_deployments = param.String(default="", label="Filter Deployments by text")
    include_events = param.Boolean(default=True, label="Include Events")
    include_ids = param.Boolean(default=True, label="Include Trapper DB IDs")
    get_data_btn = param.Event(label="Get data")

    # data aggregation controlers
    agg_source_level = param.ObjectSelector(
        "media", ["media", "event"], label="Aggregation source level"
    )
    agg_target_level = param.ObjectSelector(
        "deployment", ["deployment", "location"], label="Aggregation target level"
    )
    count_var = param.ObjectSelector(
        "countNew", ["count", "countNew"], label="Aggregation variable"
    )
    count_fun = param.ObjectSelector(
        "sum", ["sum", "mean", "max"], label="Aggregation function"
    )
    event_fun = param.ObjectSelector(
        "sum", ["sum", "mean", "max"], label="Aggregation function (event-level)"
    )
    min_days = param.Integer(
        default=1, bounds=(1, 100), label="Minimum deployment days"
    )
    agg_data_btn = param.Event(label="Aggregate data")

    # data filters
    filter_table = param.ObjectSelector("observations", TAX_TABLES, label="")
    filter_data_btn = param.Event(label="Filter data")

    # download buttons
    data_agg_view_download_btn = param.Event(label="Download")

    descriptor = param.Dict()

    def _init_view(self):
        self.notification = ("Panel succesfully initialized", "success")
        self.info_main = """# 🦊 <span style="color:#C01754"> TRAPPER's Camera Trap Data Package Interactive Dashboard </span> 🦬\n
### see [https://tdwg.github.io/camtrap-dp/](https://tdwg.github.io/camtrap-dp/)"""
        self.info_project = """## You are exploring data from this Trapper project:
### [{project_name}]({project_url})"""
        info_head = """## Data (preview, first 10 rows) """
        info_head_agg = """## Data (aggregated)"""
        info_descriptor = """### Data Package Descriptor"""
        self._dataframe = pn.widgets.DataFrame(pd.DataFrame())
        self._dataframe = pn.widgets.Tabulator(
            pagination=None,
            page_size=10,
            sizing_mode="stretch_width",
            header_filters=False,
        )
        self._dataframe_agg = pn.widgets.Tabulator(
            pagination="local",
            page_size=10,
            sizing_mode="stretch_width",
            header_filters=True,
        )
        self._total_rows = pn.pane.Markdown()
        self._total_rows_agg = pn.pane.Markdown()
        self._descriptor = pn.pane.JSON(
            name="Data Package Descriptor", depth=0, hover_preview=True, theme="light"
        )
        self._info_project = pn.pane.Markdown()
        self.view = pn.Column(
            # main info
            pn.pane.Markdown(self.info_main),
            # project info
            self._info_project,
            # descriptor
            pn.Row(
                pn.pane.Markdown(info_descriptor),
                self._descriptor,
            ),
            pn.Row(
                # data controllers
                pn.Column(
                    pn.pane.Markdown("## Data Load"),
                    pn.Param(
                        self,
                        parameters=[
                            "project_id",
                            "export_format",
                            "clear_cache",
                            "exclude_blank",
                            "all_deployments",
                            "filter_deployments",
                            "include_events",
                            "include_ids",
                            "get_data_btn",
                        ],
                        widgets={"get_data_btn": {"button_type": "success"}},
                        show_name=False,
                    ),
                ),
                # data table
                pn.Column(
                    pn.Row(
                        pn.pane.Markdown(info_head),
                        pn.Param(
                            self,
                            parameters=["filter_table"],
                            show_name=False,
                        ),
                    ),
                    self._dataframe,
                    pn.Row(
                        self._total_rows,
                        pn.widgets.FileDownload(
                            callback=self._download_data_view,
                            label="Download",
                            filename=f"{self.filter_table}.csv",
                            align="center",
                        ),
                    ),
                ),
            ),
            pn.Row(
                # data aggregation controllers
                pn.Column(
                    pn.pane.Markdown("## Data Aggregation"),
                    pn.Param(
                        self,
                        parameters=[
                            "agg_source_level",
                            "agg_target_level",
                            "count_var",
                            "count_fun",
                            "event_fun",
                            "min_days",
                            "agg_data_btn",
                        ],
                        widgets={"agg_data_btn": {"button_type": "success"}},
                        show_name=False,
                    ),
                ),
                # data aggregation table
                pn.Column(
                    pn.Row(
                        pn.pane.Markdown(info_head_agg),
                        pn.Param(
                            self,
                            parameters=["filter_table"],
                            show_name=False,
                        ),
                    ),
                    self._dataframe_agg,
                    pn.Row(
                        self._total_rows_agg,
                        pn.Param(
                            self,
                            parameters=["data_agg_view_download_btn"],
                            show_name=False,
                        ),
                    ),
                ),
            ),
            # Data filters
            pn.Row(pn.pane.Markdown("## Data Filters")),
            pn.Param(
                self,
                parameters=[
                    # "filter_table",
                    "filter_observation_type",
                    "filter_species",
                    "filter_data_btn",
                ],
                widgets={
                    "filter_data_btn": {
                        "align": "center",
                        "button_type": "success",
                    },
                },
                default_layout=pn.Row,
                show_name=False,
            ),
        )

    @pn.depends("get_data_btn", watch=True)
    async def _load_data(self):
        self.loading = True
        self.notification = ("Loading data package...", "info")
        raw_data, msg = await _get_data(
            project_id=self.project_id,
            export_format=self.export_format,
            clear_cache=self.clear_cache,
            exclude_blank=self.exclude_blank,
            all_deployments=self.all_deployments,
            filter_deployments=self.filter_deployments,
            include_events=self.include_events,
            include_ids=self.include_ids,
        )

        if not raw_data:
            self.loading = False
            self.notification = (msg, "error")
            return

        self.notification = (msg, "success")
        data = _transform_data(raw_data)
        self.data = data
        # clear agg data & data views
        self.data_agg = None
        self.data_agg_view = None
        self.descriptor = data.descriptor
        self.param.filter_species.objects = utils.get_species(data.descriptor)
        self.loading = False
        self.clear_cache = False

        # set reasonable defaults for aggregation
        if self.export_format == "camtrapdp":
            self.count_var = "count"
            if self.include_events:
                self.agg_source_level = "event"
        if self.export_format == "trapper":
            self.agg_source_level = "media"

    def _download_data_view(self):
        self.notification = ("Downloading CSV table...", "info")
        if self.data_view is not None:
            data = self.data_view
        else:
            data = pd.DataFrame()
        sio = StringIO()
        data.to_csv(sio, index=False)
        sio.seek(0)
        return sio

    @pn.depends("data_agg_view_download_btn", watch=True)
    def _download_data_agg_view(self):
        if self.data_agg_view is not None:
            self.notification = ("Downloading CSV table...", "info")
            self._dataframe_agg.download(f"{self.filter_table}_agg.csv")

    @pn.depends("agg_data_btn", watch=True)
    def _aggregate_data(self):
        self.loading = True
        if self.data:
            self.notification = ("Aggregating data...", "info")
            data_agg = utils.aggregate_data(
                self.data.observations,
                self.data.deployments,
                agg_source_level=self.agg_source_level,
                agg_target_level=self.agg_target_level,
                count_var=self.count_var,
                count_fun=self.count_fun,
                event_fun=self.event_fun,
                min_days=self.min_days,
            )
            self.data_agg = data_agg
        self.loading = False

    @pn.depends("filter_data_btn", "filter_table", "data", "data_agg", watch=True)
    def _filter_data(self):
        self.loading = True
        self.notification = ("Filtering data...", "info")

        # data_view
        if self.data:
            data_view = getattr(self.data, self.filter_table)
            if self.filter_table == "observations":
                if self.filter_observation_type:
                    data_view = data_view[
                        data_view["observationType"].isin(self.filter_observation_type)
                    ]
                if self.filter_species:
                    data_view = data_view[
                        data_view["scientificName"].isin(self.filter_species)
                    ]
            self.data_view = data_view

        # data_agg_view
        if self.data_agg and self.filter_table in ["observations", "deployments"]:
            data_agg_view = getattr(self.data_agg, self.filter_table)
            if self.filter_observation_type:
                data_agg_view = data_agg_view[
                    data_agg_view["observationType"].isin(self.filter_observation_type)
                ]
            if self.filter_species:
                data_agg_view = data_agg_view[
                    data_agg_view["scientificName"].isin(self.filter_species)
                ]
            self.data_agg_view = data_agg_view
        else:
            self.data_agg_view = None

        self.loading = False

    @pn.depends("data", "data_view", "data_agg_view", watch=True)
    def _update_view(self, *events):
        if self.data_view is not None:
            self._dataframe.value = self.data_view.head(10)
            self._total_rows.object = f"Total rows: {len(self.data_view)}"
            self._descriptor.object = self.data.descriptor

            # build project's url
            project_url = f"https://{utils.HOSTNAME}/media_classification/project/detail/{self.project_id}/"
            self._info_project.object = self.info_project.format(
                project_name=self.data.descriptor["title"],
                project_url=project_url,
            )
        else:
            self._dataframe.value = pd.DataFrame()
            self._total_rows.object = "Total rows: 0"
        if self.data_agg_view is not None:
            self._dataframe_agg.value = self.data_agg_view
            self._total_rows_agg.object = f"Total rows: {len(self.data_agg_view)}"
        else:
            self._dataframe_agg.value = pd.DataFrame()
            self._total_rows_agg.object = "Total rows: 0"


# -----------------------------------------------------------------------------
# Dashboard sections - Map
# -----------------------------------------------------------------------------


class MapSection(BaseSection):
    """Section with mapping tools"""

    lmap = param.ClassSelector(class_=utils.Map)
    layer_name = param.String(default="Animal observations", label="Layer name")
    layer_color = param.Color(default=ACCENT_COLOR, label="Layer color")
    layer_opacity = param.Number(
        default=0.2, bounds=(0.0, 1.0), step=0.1, label="Layer opacity"
    )
    layer_scalling_factor = param.Integer(
        default=10, bounds=(1, 50), step=1, label="Layer scalling factor"
    )
    layer_save_btn = param.Event(label="Save layer")
    layer_redraw_btn = param.Event(label="Redraw layer")
    clear_layers_btn = param.Event(label="Clear layers")

    agg_target_level = param.ObjectSelector(
        "deployment", ["deployment", "location"], label="Aggregation target level"
    )
    filters_triggered = param.Event()

    def _init_view(self):
        info = """## Interactive map"""
        self.lmap = utils.get_leaflet_map(center=(0, 0), zoom=1, height=500)
        self.view = pn.Column(
            pn.pane.Markdown(info),
            pn.Param(
                self,
                parameters=[
                    "layer_name",
                    "layer_color",
                    "layer_opacity",
                    "layer_scalling_factor",
                    "layer_redraw_btn",
                    "layer_save_btn",
                    "clear_layers_btn",
                ],
                widgets={
                    "layer_redraw_btn": {"button_type": "primary", "align": "end"},
                    "layer_save_btn": {"button_type": "success", "align": "end"},
                    "clear_layers_btn": {"button_type": "danger", "align": "end"},
                },
                default_layout=pn.Row,
                show_name=False,
            ),
            sizing_mode="stretch_both",
        )

    @pn.depends("layer_save_btn", "layer_redraw_btn", watch=True)
    def _save_layer(self):
        if self.data_agg_view is not None:
            if self.lmap.current_layer:
                self.lmap.remove_layer(self.lmap.current_layer)
                self.lmap.current_layer = None
            self.loading = True
            save = True if self.layer_save_btn else False
            layer_name = self.layer_name if self.layer_save_btn else CURRENT_LAYER_NAME
            utils.add_bubble_markers(
                self.lmap,
                self.data_agg_view,
                layer_name=layer_name,
                color=self.layer_color,
                opacity=self.layer_opacity,
                scalling_factor=self.layer_scalling_factor,
                save=save,
            )
            self.loading = False

    @pn.depends("clear_layers_btn", watch=True)
    def _clear_layers(self):
        layers = self.lmap.saved_layers + [self.lmap.current_layer]
        for layer in layers:
            self.lmap.remove_layer(layer)
        self.lmap.saved_layers = []
        self.lmap.current_layer = None

    @pn.depends("data_agg", "filters_triggered", watch=True)
    def _update_data_agg_view(self):
        if self.data_agg and self.data_agg.deployments is not None:
            data_agg_view = self.data_agg.deployments
            if self.filter_observation_type:
                data_agg_view = data_agg_view[
                    data_agg_view["observationType"].isin(self.filter_observation_type)
                ]
            if self.filter_species:
                data_agg_view = data_agg_view[
                    data_agg_view["scientificName"].isin(self.filter_species)
                ]
            # aggregate by deployment/location
            data_agg_view = (
                data_agg_view.groupby(f"{self.agg_target_level}ID")
                .agg(
                    {
                        "count": "sum",
                        "days": "first",
                        "latitude": "first",
                        "longitude": "first",
                    }
                )
                .reset_index()
            )
            data_agg_view["trapRate"] = data_agg_view["count"] / data_agg_view["days"]
            data_agg_view.trapRate.fillna(0, inplace=True)
            self.data_agg_view = data_agg_view
        else:
            self.data_agg_view = None

    @pn.depends("data", "data_agg", "filters_triggered", watch=True)
    def _update_view(self, *events):
        if not self.data:
            return
        if self.data_agg_view is not None:
            # TODO: add logic to keep selected layers
            if self.lmap.current_layer:
                self.lmap.remove_layer(self.lmap.current_layer)
                self.lmap.current_layer = None
            self.loading = True
            utils.add_bubble_markers(
                self.lmap,
                self.data_agg_view,
                color=self.layer_color,
                opacity=self.layer_opacity,
                scalling_factor=self.layer_scalling_factor,
                layer_name=CURRENT_LAYER_NAME,
            )
            self.loading = False
        else:
            map_center = utils.get_study_area_center(self.data.descriptor)
            self.lmap.center = map_center
            self.lmap.zoom = 10


# -----------------------------------------------------------------------------
# Dashboard sections - Activity Histogram
# -----------------------------------------------------------------------------

# TODO

# -----------------------------------------------------------------------------
# Dashboard App
# -----------------------------------------------------------------------------


def app_view() -> pn.Column:
    """Returns the app"""
    load_data_section = LoadDataSection()
    map_section = MapSection(data=load_data_section.data)

    @pn.depends(load_data_section.param.data, watch=True)
    def _update_data(data):
        map_section.data = data

    @pn.depends(load_data_section.param.data_agg, watch=True)
    def _update_data_agg(data_agg):
        map_section.data_agg = data_agg

    @pn.depends(load_data_section.param.filter_observation_type, watch=True)
    def _update_filter_observation_type(filter_observation_type):
        map_section.filter_observation_type = filter_observation_type

    @pn.depends(load_data_section.param.filter_species, watch=True)
    def _update_filter_species(filter_species):
        map_section.filter_species = filter_species

    @pn.depends(load_data_section.param.agg_target_level, watch=True)
    def _update_agg_target_level(agg_target_level):
        map_section.agg_target_level = agg_target_level

    @pn.depends(load_data_section.param.filter_data_btn, watch=True)
    def _update_filters_triggered(filter_data_btn):
        map_section.filters_triggered = filter_data_btn

    @pn.depends(loading=load_data_section.param.loading, watch=True)
    def _update_loading(loading):
        map_section.loading = loading

    # return (
    return (
        load_data_section,
        map_section,
    )
