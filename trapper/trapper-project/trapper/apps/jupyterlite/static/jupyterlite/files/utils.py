from dataclasses import dataclass
import io
import json
import time
import zipfile

from ipyleaflet import (
    basemaps,
    basemap_to_tiles,
    CircleMarker,
    FullScreenControl,
    LayersControl,
    LayerGroup,
    Map,
    ScaleControl,
    TileLayer,
)
from ipywidgets import HTML, Layout
import numpy as np
import pandas
from urllib import parse as url_parse

try:
    PYODIDE_KERNEL = True
    import js
    from pyodide.http import pyfetch

    def get_hostname() -> str:
        return url_parse.urlparse(str(js.location)).hostname

    HOSTNAME = get_hostname()

except ImportError:
    PYODIDE_KERNEL = False
    import os
    import requests

    HOSTNAME = os.environ.get("HOSTNAME", "localhost")
    API_TOKEN = os.environ.get("API_TOKEN", None)
    VERIFY_SSL = os.environ.get("VERIFY_SSL", False)


# For available query params see https://{hostname}/docs/api/
API_URLS = {
    # media_classification
    "package": "https://{hostname}/media_classification/api/package/{project_id}/?{query}",
    "observations": "https://{hostname}/media_classification/api/classifications/results/{project_id}/?{query}",
    "observations_agg": "https://{hostname}/media_classification/api/classifications/results/agg/{project_id}/?{query}",
    "media": "https://{hostname}/media_classification/api/media/{project_id}?{query}/",
    # geomap
    "deployments": "https://{hostname}/geomap/api/deployments/export/?classification_project={project_id}&{query}",
}

if PYODIDE_KERNEL:

    async def get_data(api_url, resp_type="json") -> tuple:
        """ """
        if resp_type not in ["json", "bytes"]:
            raise ValueError("Wrong 'resp_type'")
        resp = await pyfetch(api_url)
        if resp.status != 200:
            return (None, resp)
        if resp_type == "json":
            data = await resp.json()
        if resp_type == "bytes":
            data = await resp.bytes()
        return (data, resp)

else:

    async def get_data(api_url, resp_type="json") -> tuple:
        """ """
        auth_header = {"Authorization": f"Token {API_TOKEN}"}
        print(api_url)
        if resp_type not in ["json", "bytes"]:
            raise ValueError("Wrong 'resp_type'")
        resp = requests.get(api_url, headers=auth_header, verify=VERIFY_SSL)
        resp.status = resp.status_code
        if resp.status != 200:
            return (None, resp)
        if resp_type == "json":
            data = resp.json()
        if resp_type == "bytes":
            data = resp.content
        return (data, resp)


@dataclass
class TrapperPackage:
    descriptor: dict
    observations: pandas.DataFrame
    media: pandas.DataFrame
    deployments: pandas.DataFrame


async def get_package(
    project_id,
    clear_cache=False,
    export_format="camtrapdp",
    exclude_blank=True,
    all_deployments=False,
    filter_deployments="",
    include_events=True,
    trapper_url_token=True,
    private_human=False,
    private_vehicle=False,
    include_ids=True,
    extra_query="",
) -> TrapperPackage:
    """ """
    query = (
        f"clear_cache={clear_cache}&"
        f"export_format={export_format}&"
        f"exclude_blank={exclude_blank}&"
        f"all_deployments={all_deployments}&"
        f"filter_deployments={filter_deployments}&"
        f"include_events={include_events}&"
        f"trapper_url_token={trapper_url_token}&"
        f"private_human={private_human}&"
        f"private_vehicle={private_vehicle}&"
        f"include_ids={include_ids}&"
        f"{extra_query}"
    )
    api_url = API_URLS["package"].format(
        hostname=HOSTNAME, project_id=project_id, query=query
    )
    package, resp = await get_data(api_url, resp_type="bytes")
    if resp.status == 200:
        msg = f"Data package for project {project_id} downloaded successfully!"
        # read all files from zip and get descriptor and 3 csv dataframes
        with zipfile.ZipFile(io.BytesIO(package)) as zip_file:
            with zip_file.open("metadata.json") as f:
                descriptor = json.load(f)
            with zip_file.open("observations.csv") as f:
                df_obs = pandas.read_csv(f, keep_default_na=False)
            with zip_file.open("media.csv") as f:
                df_media = pandas.read_csv(f, keep_default_na=False)
            with zip_file.open("deployments.csv") as f:
                df_dep = pandas.read_csv(f, keep_default_na=False)

        return (
            TrapperPackage(
                descriptor=descriptor,
                observations=df_obs,
                media=df_media,
                deployments=df_dep,
            ),
            msg,
        )
    elif resp.status == 403:
        msg = "You are not logged in to TRAPPER or you have no permission to get this data package."
    else:
        msg = f"Error when getting data. Status code: {resp.status}"
    return (None, msg)


def get_species(descriptor) -> list:
    """ """
    species = []
    for item in descriptor["taxonomic"]:
        scientific_name = item.get("scientificName")
        if scientific_name:
            species.append(scientific_name)
    return species


@dataclass
class TrapperPackageAgg:
    observations: pandas.DataFrame
    deployments: pandas.DataFrame


def aggregate_data(
    df_obs,
    df_dep,
    agg_source_level="media",
    agg_target_level="deployment",
    count_var="count",
    count_fun="sum",
    event_fun="sum",
    min_days=1,
) -> TrapperPackageAgg:
    """ """

    # OBSERVATIONS AGGREGATION

    # remove blank and unclassified observations if present
    df_obs = df_obs[~df_obs.observationType.isin(["blank", "unclassified"])]

    # filter by observation level
    df_obs = df_obs[df_obs.observationLevel == agg_source_level]

    # if df_obs is empty after filtering return empty TrapperPackageAgg
    if df_obs.empty or count_var not in df_obs.columns:
        return TrapperPackageAgg(None, None)

    # replace empty strings in count_var column with NaN and fill NaN with 0
    df_obs[count_var].replace("", np.nan, inplace=True)
    df_obs[count_var].fillna(0.0, inplace=True)
    df_obs[count_var] = df_obs[count_var].astype(float)

    group_by_columns = [
        "deploymentID",
        "eventID",
        "observationType",
        "scientificName",
    ]

    if agg_source_level == "media":
        # first aggregate at a single-media event level (always sum)
        g0 = (
            df_obs.groupby(["mediaID", "eventStart", "eventEnd"] + group_by_columns)
            .agg(
                {
                    count_var: "sum",
                }
            )
            .reset_index()
        )

        g1 = (
            g0.groupby(group_by_columns)
            .agg(
                {
                    count_var: event_fun,
                    "eventStart": "min",
                    "eventEnd": "max",
                }
            )
            .reset_index()
        )
    else:
        g1 = df_obs[group_by_columns + [count_var, "eventStart", "eventEnd"]]

    # g1 will be returned as aggregated observations
    g1.rename(columns={count_var: "count"}, inplace=True)
    observations_agg = g1

    # DEPLOYMENTS/LOCATIONS AGGREGATION

    # remove rows with missing deploymentStart or deploymentEnd
    df_dep = df_dep[~df_dep.deploymentStart.isna() & ~df_dep.deploymentEnd.isna()]

    # get df_dep days
    _format = "%Y-%m-%dT%H:%M:%S%z"
    start = df_dep["deploymentStart"].apply(
        lambda x: time.mktime(time.strptime(x, _format))
    )
    end = df_dep["deploymentEnd"].apply(
        lambda x: time.mktime(time.strptime(x, _format))
    )
    df_dep["days"] = (end - start) / (24 * 60 * 60)

    # remove deployments with days < min_days
    df_dep = df_dep[df_dep.days > min_days]

    if agg_target_level == "location":
        # estimate correct number of days per location and add as a new column "location_days" to df_dep
        df_dep["location_days"] = df_dep.groupby("locationID")["days"].transform("sum")

    # remove eventID from group_by_columns
    group_by_columns.remove("eventID")

    g2 = (
        g1.groupby(group_by_columns)
        .agg(
            {
                "count": count_fun,
            }
        )
        .reset_index()
    )

    deployments_agg = df_dep.merge(g2, how="left", on="deploymentID")

    if agg_target_level == "location":
        group_by_columns[0] = "locationID"
        deployments_agg = (
            deployments_agg.groupby(group_by_columns)
            .agg(
                {
                    "count": count_fun,
                    "location_days": "first",
                    "longitude": "first",
                    "latitude": "first",
                }
            )
            .reset_index()
        )
        # rename location_days to days
        deployments_agg.rename(columns={"location_days": "days"}, inplace=True)

    deployments_agg.drop_duplicates(inplace=True)
    deployments_agg["count"].fillna(0, inplace=True)
    deployments_agg["trapRate"] = deployments_agg["count"] / deployments_agg["days"]
    deployments_agg.trapRate.fillna(0, inplace=True)

    return TrapperPackageAgg(observations_agg, deployments_agg)


def get_study_area_center(descriptor):
    xlim = (descriptor["spatial"]["bbox"][0], descriptor["spatial"]["bbox"][2])
    ylim = (descriptor["spatial"]["bbox"][1], descriptor["spatial"]["bbox"][3])
    center = (
        ylim[0] + (ylim[1] - ylim[0]) / 2,
        xlim[0] + (xlim[1] - xlim[0]) / 2,
    )
    return center


def get_leaflet_map(
    center,
    zoom=10,
    height=500,
) -> Map:
    """ """
    positron = basemap_to_tiles(basemaps.CartoDB.Positron)
    positron.base = True
    positron.name = "CartoDB Positron"

    mapnik = basemap_to_tiles(basemaps.OpenStreetMap.Mapnik)
    mapnik.base = True
    mapnik.name = "OSM Mapnik"

    esri = basemap_to_tiles(basemaps.Esri.WorldImagery)
    esri.base = True
    esri.name = "Esri World Imagery"

    google = TileLayer(
        url="https://mt1.google.com/vt/lyrs=y&x={x}&y={y}&z={z}",
        attribution="Google",
        name="Google Satellite",
    )
    google.base = True

    lmap = Map(
        center=center,
        zoom=zoom,
        layout=Layout(width="100%", height=f"{height}px"),
        layers=[mapnik, esri, google, positron],
        scroll_wheel_zoom=True,
    )
    lmap.add_control(FullScreenControl())
    lmap.add_control(LayersControl())
    lmap.add_control(ScaleControl(position="bottomleft"))
    lmap.current_layer = None
    lmap.current_layer_bounds = None
    lmap.saved_layers = []
    return lmap


def get_layer_bounds(lat: list, lon: list) -> tuple:
    """ """
    return ((min(lat), min(lon)), (max(lat), max(lon)))


def add_bubble_markers(
    lmap,
    df,
    variable="trapRate",
    color="green",
    opacity=0.2,
    scalling_factor=10,
    layer_name="Trapping rates",
    save=False,
):
    if df.empty:
        return

    # add column with markers fill_color
    df["fill_color"] = "black"

    # set fill_color to "green" if variable > 0
    df.loc[df[variable] > 0, "fill_color"] = color

    # get layer bounds
    layer_bounds = get_layer_bounds(df.latitude, df.longitude)

    # get CircleMarkers (bubbles)
    markers_list = []
    for i, row in df.iterrows():
        fill_color = row.pop("fill_color")
        row_html = row.to_frame().to_html(header=False)
        popup_msg = HTML()
        popup_msg.value = row_html

        if row[variable] == 0:
            radius = 1
        else:
            radius = round(scalling_factor * row[variable])

        marker = CircleMarker(
            location=(row.latitude, row.longitude),
            radius=radius,
            color="black",
            fill_color=fill_color,
            fill_opacity=opacity,
            weight=1,
        )
        marker.popup = popup_msg
        markers_list.append(marker)

    layer = LayerGroup(layers=markers_list, name=layer_name)
    lmap.add_layer(layer)
    lmap.fit_bounds(layer_bounds)
    if save:
        lmap.saved_layers.append(layer)
    else:
        lmap.current_layer = layer
        lmap.current_layer_bounds = layer_bounds
