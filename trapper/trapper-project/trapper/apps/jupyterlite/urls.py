"""
Urls for jupyterlite app
"""
from django.urls import re_path
from trapper.apps.jupyterlite import views

app_name = "jupyterlite"

urlpatterns = [
    re_path(r"^$", views.view_jupyterlite_lab, name="jupyterlite_lab"),
]
