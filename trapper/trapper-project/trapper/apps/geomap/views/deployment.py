# -*- coding: utf-8 -*-
from braces.views import UserPassesTestMixin, JSONResponseMixin
from django.conf import settings
from django.contrib import messages
from django.shortcuts import render
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.dateparse import parse_datetime
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from django.views import generic

from trapper.apps.accounts.models import UserTask
from trapper.apps.common.utils.datetime_tools import localize_datetime_dst
from trapper.apps.common.views import (
    LoginRequiredMixin,
    BaseDeleteView,
    BaseUpdateView,
    BaseBulkUpdateView,
)
from trapper.apps.geomap.forms import (
    DeploymentForm,
    SimpleDeploymentForm,
    BulkCreateDeploymentForm,
    BulkUpdateDeploymentForm,
    DeploymentImportForm,
)
from trapper.apps.geomap.models import Deployment
from trapper.apps.geomap.tasks import DeploymentImporter, celery_import_deployments
from trapper.apps.research.models import ResearchProject


class DeploymentDetailView(LoginRequiredMixin, UserPassesTestMixin, generic.DetailView):
    """View used for rendering details of specified deployment.

    Before details are rendered, permissions are checked and if currently
    logged in user has not enough permissions to view details,
    proper message is displayed.
    """

    template_name = "geomap/deployment_detail.html"
    model = Deployment
    raise_exception = True

    def test_func(self, user):
        """
        Deployment details can be seen only if user has enough permissions
        """
        return self.get_object().can_view(user)

    def get_context_data(self, **kwargs):
        """Update context used to render template with data related to
        deployments
        """
        context = super().get_context_data(**kwargs)
        return context


view_deployment_detail = DeploymentDetailView.as_view()


class DeploymentListView(LoginRequiredMixin, generic.TemplateView):
    model = Deployment
    template_name = "geomap/deployment_list.html"

    def get_context_data(self, **kwargs):
        """All we need to render base grid is:
        * Model name as title
        * Filter form instance

        This view is not serving any data. Data is read using DRF API
        """
        deployments = Deployment.objects.get_accessible()

        context = {
            "data_url": reverse("geomap:api-deployment-list"),
            "owners": deployments.order_by("owner__username")
            .values_list("owner__pk", "owner__username")
            .distinct(),
            "research_projects": ResearchProject.objects.get_accessible(
                user=self.request.user
            )
            .distinct()
            .values_list("pk", "acronym"),
            "tags": Deployment.tags.values_list("pk", "name"),
            "locations": deployments.values_list(
                "location__pk", "location__location_id"
            ).order_by("location__location_id"),
            "model_name": "deployments",
            "update_redirect": False,
        }
        return {"deployment_context": context}


view_deployment_list = DeploymentListView.as_view()


class DeploymentCreateView(LoginRequiredMixin, generic.CreateView):
    """Deployment's create view.
    Handle the creation of the :class:`apps.geomap.models.Deployment` objects.
    """

    template_name = "geomap/deployment_change.html"
    model = Deployment
    form_class = DeploymentForm

    def form_valid(self, form):
        """If form is valid then set `owner` as currently logged in user,
        and add message that deployment has been created"""
        user = self.request.user
        form.instance.owner = user
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _("New deployment has been successfully added"),
        )
        self.object = form.save()
        return super().form_valid(form)

    def form_invalid(self, form):
        """If form is not valid, form is re-rendered with error details,
        and message about unsuccessfull operation is shown"""
        messages.add_message(
            self.request, messages.ERROR, _("Error creating new deployment")
        )
        return super().form_invalid(form)


view_deployment_create = DeploymentCreateView.as_view()


class DeploymentDeleteView(BaseDeleteView):
    """View responsible for handling deletion of single or multiple
    deployments.

    Only deployments that user has enough permissions for can be deleted
    """

    model = Deployment
    protected_msg_tmpl = _(
        'Deployment "{name}" can not be deleted. To delete it you have to delete/unlink all'
        " resources that refer to this deployment."
    )
    item_name_field = "deployment_id"
    redirect_url = "geomap:deployment_list"


view_deployment_delete = DeploymentDeleteView.as_view()


class DeploymentUpdateView(BaseUpdateView):
    """Deployment update view."""

    template_name = "geomap/deployment_change.html"
    template_name_modal = "geomap/deployment_form.html"
    model = Deployment
    raise_exception = True
    form_class = DeploymentForm
    form_class_modal = SimpleDeploymentForm
    item_name_field = "deployment_id"


view_deployment_update = DeploymentUpdateView.as_view()


class DeploymentBulkUpdateView(BaseBulkUpdateView):
    """Deployment bulk update view."""

    template_name = "forms/simple_crispy_form.html"
    form_class = BulkUpdateDeploymentForm
    raise_exception = True
    tags_field = "tags"


view_deployment_bulk_update = DeploymentBulkUpdateView.as_view()


class DeploymentBulkCreateView(
    LoginRequiredMixin, generic.edit.FormView, JSONResponseMixin
):
    """Deployment bulk create view."""

    template_name = "geomap/deployment_bulk_create_form.html"
    form_class = BulkCreateDeploymentForm

    def form_valid(self, form):
        """If form is valid then set `owner` as currently logged in user,
        and add message that deployments have been successfully created"""
        user = self.request.user
        deployment_list = []
        timestamp = now()
        managers = form.cleaned_data["managers"]

        for location in form.cleaned_data["locations"]:
            # localize datetime objects using a a proper timezone and DST flag
            start_date = localize_datetime_dst(
                parse_datetime(form.cleaned_data["start_date"]),
                location.timezone,
                location.ignore_DST,
            )
            end_date = localize_datetime_dst(
                parse_datetime(form.cleaned_data["end_date"]),
                location.timezone,
                location.ignore_DST,
            )

            deployment = Deployment(
                location=location,
                deployment_code=form.cleaned_data["deployment_code"],
                start_date=start_date,
                end_date=end_date,
                research_project=form.cleaned_data["research_project"],
                owner=user,
                date_created=timestamp,
            )
            deployment.deployment_id = "-".join(
                [deployment.deployment_code, location.location_id]
            )
            deployment_list.append(deployment)
        Deployment.objects.bulk_create(deployment_list)

        if managers:
            managers_through_list = []
            managers_through_model = Deployment.managers.through
            deployments_created_pks = Deployment.objects.filter(
                date_created=timestamp
            ).values_list("pk", flat=True)
            for deployment_pk in deployments_created_pks:
                for manager in managers:
                    managers_through_obj = managers_through_model(
                        deployment_id=deployment_pk, user=manager
                    )
                    managers_through_list.append(managers_through_obj)
            managers_through_model.objects.bulk_create(managers_through_list)

        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(f"{len(deployment_list)} new deployments have been successfully added."),
        )
        context = {"success": True, "msg": "", "url": reverse("geomap:deployment_list")}
        return self.render_json_response(context_dict=context)

    def form_invalid(self, form):
        """If form is not valid, form is re-rendered with error details,
        and message about unsuccessfull operation is shown"""
        context = {
            "success": False,
            "msg": _("Your form contain errors"),
            "form_html": render_to_string(self.template_name, {"form": form}),
        }
        return self.render_json_response(context_dict=context)


view_deployment_bulk_create = DeploymentBulkCreateView.as_view()


class DeploymentImportView(LoginRequiredMixin, generic.FormView):
    """Imports deployments from csv table."""

    template_name = "geomap/deployment_import.html"
    template_errors = "common/table_errors.html"
    form_class = DeploymentImportForm
    success_url = None

    def form_valid(self, form):
        params = {
            "user": self.request.user,
            "data": form.cleaned_data.get("csv_data"),
            "timezone": form.cleaned_data.get("timezone"),
            "research_project": form.cleaned_data.get("research_project"),
            "update": form.cleaned_data.get("update", False),
            "create_locations": form.cleaned_data.get("create_locations", False),
            "ignore_DST": form.cleaned_data.get("ignore_DST", False),
        }

        # initialize classification importer to do table validation
        try:
            importer = DeploymentImporter(**params)
        except ValueError as e:
            if e.args[0] == "column-missing":
                return render(
                    self.request,
                    self.template_errors,
                    {
                        "error_type": e.args[0],
                        "exception": e.args[1],
                        "provided_columns": ", ".join(e.args[2]),  # provided_columns
                        "required_columns": ", ".join(e.args[3]),  # required_columns
                    },
                )
            elif e.args[0] == "error-with-message":
                return render(
                    self.request,
                    self.template_errors,
                    {
                        "error_type": e.args[0],
                        "exception": e.args[1],
                    },
                )

        # first validate a table with frictionless
        importer.validate_table()
        if not importer.report.valid:
            return render(
                self.request,
                self.template_errors,
                {
                    "report": importer.report.to_json(),
                    "provided_columns": ", ".join(importer.provided_columns),
                    "required_columns": ", ".join(importer.required_columns),
                },
            )
        params["data"] = importer.data
        if settings.CELERY_ENABLED:
            task = celery_import_deployments.delay(**params)
            user_task = UserTask(user=self.request.user, task_id=task.task_id)
            user_task.save()
            msg = _(
                "You have successfully run a celery task. Deployments are being imported now."
            )
        else:
            msg = celery_import_deployments(**params)
        messages.success(request=self.request, message=msg)
        self.success_url = reverse("accounts:dashboard")
        return super().form_valid(form)


view_deployment_import = DeploymentImportView.as_view()
