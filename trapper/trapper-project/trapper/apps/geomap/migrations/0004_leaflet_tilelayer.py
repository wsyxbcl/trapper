from django.db import migrations


def insert_data(apps, schema_editor):
    TileLayer = apps.get_model("umap", "TileLayer")
    db_alias = schema_editor.connection.alias

    TileLayer.objects.using(db_alias).bulk_create(
        [
            TileLayer(
                url_template="http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                minZoom=0,
                maxZoom=18,
                attribution="OSM",
                rank=1,
                name="OSM",
            ),
            TileLayer(
                url_template="http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}",
                minZoom=0,
                maxZoom=1,
                attribution="Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping,"
                " Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community",
                rank=2,
                name="Esri Imagery",
            ),
            TileLayer(
                url_template="http://server.arcgisonline.com/ArcGIS/rest/services/NatGeo_World_Map/MapServer/tile/"
                "{z}/{y}/{x}",
                minZoom=0,
                maxZoom=18,
                attribution="Tiles &copy; Esri &mdash; National Geographic, Esri, DeLorme, NAVTEQ, UNEP-WCMC, USGS, "
                "NASA, ESA, METI, NRCAN, GEBCO, NOAA, iPC",
                rank=3,
                name="ESRI NatGeo",
            ),
        ]
    )


class Migration(migrations.Migration):
    dependencies = [
        ("geomap", "0003_auto_20200325_1428"),
        ("umap", "0007_auto_20190416_1757"),
    ]

    operations = [
        migrations.RunPython(insert_data),
    ]
