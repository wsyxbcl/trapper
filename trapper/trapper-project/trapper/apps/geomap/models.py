# -*- coding: utf-8 -*-
from django.apps import apps
from django.conf import settings
from django.contrib.gis.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db.models import Manager as GeoManager
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from taggit.managers import TaggableManager
from timezone_field import TimeZoneField

from trapper.apps.geomap.taxonomy import DeploymentFeatureType, DeploymentCaptureMethod
from trapper.apps.common.fields import SafeTextField
from trapper.apps.common.utils.datetime_tools import set_correct_offset_dst

from trapper.apps.geomap.taxonomy import DeploymentViewQuality
from trapper.apps.research.taxonomy import ResearchProjectRoleType
from trapper.apps.storage.mixins import APIContextManagerMixin
from trapper.apps.storage.taxonomy import CollectionMemberLevels
from trapper.middleware import get_current_user
from umap.models import Map


class MapManagerUtils(object):
    @classmethod
    def get_accessible(cls, user=None):
        user = user or get_current_user()
        if user.is_authenticated:
            queryset = Map.objects.filter(models.Q(owner=user) | models.Q(editors=user))
        else:
            queryset = Map.objects.filter(share_status=Map.PUBLIC)
        return queryset


class LocationManager(GeoManager):
    url_update = "geomap:map_view"
    url_detail = "geomap:location_detail"
    url_delete = "geomap:location_delete"

    def get_available(self, user=None, base_queryset=None, editable_only=False):
        user = user or get_current_user()
        if base_queryset is None:
            queryset = self.get_queryset()
        else:
            queryset = base_queryset
        if not user.is_authenticated:
            return queryset.none()
        if user.is_staff:
            return queryset
        if editable_only:
            qs = queryset.exclude(~models.Q(owner=user), ~models.Q(managers=user))
        else:
            rproject_role_model = apps.get_model("research", "researchprojectrole")
            rproject_pks = (
                rproject_role_model.objects.filter(user=user)
                .values_list("project_id", flat=True)
                .distinct()
            )
            qs = queryset.exclude(
                ~models.Q(is_public=True),
                ~models.Q(owner=user),
                ~models.Q(managers=user),
                ~models.Q(research_project__project_roles__user=user),
                ~models.Q(research_projects_member__in=rproject_pks),
            )
        return qs

    def api_update_context(self, item, user):
        context = None
        if item.can_update(user):
            context = "{url}?locations={pk}&edit=true".format(
                url=reverse(self.url_update), pk=item.pk
            )
        return context

    def api_delete_context(self, item, user):
        context = None
        if item.can_delete(user):
            context = reverse(self.url_delete, kwargs={"pk": item.pk})
        return context


class Location(models.Model):
    """Single location (Point) on map.
    This model is often referred by other models for establishing a
    spatial context.
    """

    location_id = models.CharField(max_length=100, verbose_name=_("Location id"))
    name = models.CharField(
        max_length=255, null=True, blank=True, verbose_name=_("Name")
    )
    date_created = models.DateTimeField(
        blank=True, editable=False, verbose_name=_("Date created")
    )
    description = models.TextField(null=True, blank=True, verbose_name=_("Description"))
    is_public = models.BooleanField(default=False, verbose_name=_("Is public"))
    coordinates = models.PointField(srid=4326, verbose_name=_("Coordinates"))
    timezone = TimeZoneField(
        default=timezone.get_default_timezone_name(), verbose_name=_("Timezone")
    )
    ignore_DST = models.BooleanField(default=False, verbose_name=_("Ignore DST"))
    coordinate_uncertainty = models.IntegerField(null=True, blank=True, default=10)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="owned_locations",
        on_delete=models.DO_NOTHING,
        verbose_name=_("Owner"),
    )
    managers = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        related_name="managed_locations",
        blank=True,
        verbose_name=_("Managers"),
    )
    country = models.CharField(
        max_length=100, blank=True, null=True, editable=False, verbose_name=_("Country")
    )
    state = models.CharField(
        max_length=100, blank=True, null=True, editable=False, verbose_name=_("State")
    )
    county = models.CharField(
        max_length=100, blank=True, null=True, editable=False, verbose_name=_("County")
    )
    city = models.CharField(
        max_length=100, blank=True, null=True, editable=False, verbose_name=_("City")
    )
    research_project = models.ForeignKey(
        "research.ResearchProject",
        related_name="locations",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_("Research project"),
    )

    research_projects_member = models.ManyToManyField(
        "research.ResearchProject",
        blank=True,
        related_name="shared_locations",
    )

    objects = LocationManager()

    class Meta:
        ordering = ["-pk"]
        verbose_name = _("Location")
        verbose_name_plural = _("Locations")
        constraints = [
            models.UniqueConstraint(
                fields=["location_id", "research_project"], name="unique_location_id"
            )
        ]

    @property
    def get_x(self):
        return self.coordinates.x

    @property
    def get_y(self):
        return self.coordinates.y

    @property
    def owner_name(self):
        return self.owner.username

    @property
    def latlng(self):
        return "{}, {}".format(self.coordinates.y, self.coordinates.x)

    def can_view(self, user=None):
        """Determines whether user can view the location.
        :param user: user for which the test is made
        :type user: :py:class:`django.contrib.auth.User`
        :return: True if user can see the location, False otherwise
        :rtype: bool
        """
        user = user or get_current_user()
        if self.is_public or user == self.owner or user in self.managers.all():
            return True
        if (
            self.research_project
            and self.research_project.project_roles.filter(user=user).exists()
        ):
            return True
        if self.deployments.filter(
            resources__collection__members=user,
            resources__collection__members__collectionmember__level__in=[
                CollectionMemberLevels.ACCESS,
                CollectionMemberLevels.ACCESS_REQUEST,
            ],
        ).exists():
            return True
        return False

    def can_update(self, user=None):
        user = user or get_current_user()
        is_rp_admin = (
            self.research_project
            and self.research_project.project_roles.filter(
                user=user, name=ResearchProjectRoleType.ADMIN
            ).exists()
        )
        return (
            user.is_staff
            or is_rp_admin
            or user == self.owner
            or user in self.managers.all()
        )

    def can_delete(self, user=None):
        user = user or get_current_user()
        return user == self.owner

    def save(self, **kwargs):
        if self.date_created is None:
            self.date_created = timezone.now()
        old_instance = None
        if self.pk:
            old_instance = Location.objects.get(pk=self.pk)
        super(Location, self).save(**kwargs)
        if old_instance and self.coordinates != old_instance.coordinates:
            for deployment in self.deployments.all():
                for resource in deployment.resources.all():
                    resource.refresh_collection_bbox()

    def __str__(self):
        return f"Location: {self.location_id}"


class DeploymentManager(APIContextManagerMixin, models.Manager):
    """Manager for :class:`Deployment` model.

    This manager contains additional logic used by DRF serializers like
    details/update/delete urls
    """

    url_update = "geomap:deployment_update"
    url_detail = "geomap:deployment_detail"
    url_delete = "geomap:deployment_delete"

    def get_accessible(self, user=None, base_queryset=None, editable_only=False):
        """Return all :class:`Deployment` instances that given user
        has access to. If user is not defined, then currently logged in user
        is used.

        :param user: if not none then that user will be used to filter
            accessible deployments. If user is None
            then currently logged in user is used.
        :param base_queryset: queryset used to limit checked deployments.
            by default it's all deployments.

        :return: deployments queryset
        """
        user = user or get_current_user()
        if base_queryset is None:
            queryset = self.get_queryset()
        else:
            queryset = base_queryset
        if not user.is_authenticated:
            return queryset.none()
        if user.is_staff:
            return queryset
        if editable_only:
            qs = queryset.exclude(~models.Q(owner=user), ~models.Q(managers=user))
        else:
            rproject_role_model = apps.get_model("research", "researchprojectrole")
            rproject_pks = (
                rproject_role_model.objects.filter(user=user)
                .values_list("project_id", flat=True)
                .distinct()
            )
            qs = queryset.exclude(
                ~models.Q(owner=user),
                ~models.Q(managers=user),
                ~models.Q(research_project__project_roles__user=user),
                ~models.Q(location__research_projects_member__in=rproject_pks),
            )
        return qs

    def api_update_context(self, item, user):
        context = None
        if item.can_update(user):
            context = reverse(self.url_update, kwargs={"pk": item.pk})
        return context

    def api_detail_context(self, item, user):
        context = None
        context = reverse(self.url_detail, kwargs={"pk": item.pk})
        return context

    def api_delete_context(self, item, user):
        context = None
        if item.can_delete(user):
            context = reverse(self.url_delete, kwargs={"pk": item.pk})
        return context


class Deployment(models.Model):
    """Deployment model links Resource with Location.
    This model enriches a spatial context given by Location.
    The extra information includes observation period and sensor
    (e.g. camera trap or ordinary camera) used to record a given
    set of resources.
    """

    deployment_code = models.CharField(
        max_length=100, verbose_name=_("Deployment code")
    )
    location = models.ForeignKey(
        Location,
        related_name="deployments",
        on_delete=models.PROTECT,
        verbose_name=_("Location"),
    )
    deployment_id = models.CharField(
        blank=True, editable=False, max_length=100, verbose_name=_("Deployment id")
    )
    start_date = models.DateTimeField(
        blank=True, null=True, verbose_name=_("Start date")
    )
    end_date = models.DateTimeField(blank=True, null=True, verbose_name=_("End date"))
    correct_setup = models.BooleanField(default=True, verbose_name=_("Correct setup"))
    correct_tstamp = models.BooleanField(default=True, verbose_name=_("Correct tstamp"))
    view_quality = models.CharField(
        choices=DeploymentViewQuality.CHOICES,
        max_length=255,
        null=True,
        blank=True,
        default="Good",
        verbose_name=_("View quality"),
    )
    research_project = models.ForeignKey(
        "research.ResearchProject",
        related_name="deployments",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_("Research project"),
    )
    date_created = models.DateTimeField(
        blank=True, editable=False, verbose_name=_("Date created")
    )
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="owned_deployments",
        on_delete=models.DO_NOTHING,
        verbose_name=_("Owner"),
    )
    comments = SafeTextField(blank=True, null=True, verbose_name=_("Comments"))
    managers = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        related_name="managed_deployments",
        blank=True,
        verbose_name=_("Managers"),
    )

    # fields added by camtrap-package version 0.1.6
    setup_by = models.CharField(max_length=50, default="", blank=True)
    camera_id = models.CharField(max_length=50, default="", blank=True)
    camera_model = models.CharField(max_length=50, default="", blank=True)
    camera_interval = models.IntegerField(
        null=True,
        blank=True,
        default=0,
        validators=[MinValueValidator(0)],
    )
    camera_height = models.DecimalField(
        null=True, blank=True, decimal_places=2, max_digits=4
    )
    camera_tilt = models.IntegerField(
        null=True,
        blank=True,
        validators=[MinValueValidator(-90), MaxValueValidator(90)],
    )
    camera_heading = models.IntegerField(
        null=True, blank=True, validators=[MinValueValidator(0), MaxValueValidator(360)]
    )

    session = models.CharField(max_length=255, default="", blank=True)
    array = models.CharField(max_length=50, default="", blank=True)
    feature_type = models.CharField(
        choices=DeploymentFeatureType.CHOICES,
        default=DeploymentFeatureType.NONE,
        max_length=16,
        verbose_name=_("Feature type"),
    )
    habitat = SafeTextField(verbose_name=_("Habitat"), default="", blank=True)
    detection_distance = models.DecimalField(
        null=True,
        blank=True,
        verbose_name=_("Detection distance"),
        decimal_places=2,
        max_digits=5,
    )
    capture_method = models.PositiveSmallIntegerField(
        choices=DeploymentCaptureMethod.CHOICES,
        default=1,
        verbose_name=_("Capture method"),
    )

    tags = TaggableManager(blank=True)
    objects = DeploymentManager()

    class Meta:
        ordering = [
            "deployment_id",
        ]
        verbose_name = _("Deployment")
        verbose_name_plural = _("Deployments")
        constraints = [
            models.UniqueConstraint(
                fields=["deployment_id", "research_project"],
                name="unique_deployment_id",
            )
        ]

    @property
    def timezone(self):
        return self.location.timezone

    @property
    def ignore_DST(self):
        return self.location.ignore_DST

    @property
    def start_date_tz(self):
        """Return local datetime according to location's timezone."""
        if not self.start_date:
            return None
        return set_correct_offset_dst(self.start_date, self.timezone, self.ignore_DST)

    @property
    def end_date_tz(self):
        """Return local datetime according to location's timezone."""
        if not self.end_date:
            return None
        return set_correct_offset_dst(self.end_date, self.timezone, self.ignore_DST)

    def get_absolute_url(self):
        """Get the absolute url for an instance of this model."""
        return reverse("geomap:deployment_detail", kwargs={"pk": self.pk})

    def update_deployment_id(self, save=True):
        self.deployment_id = "-".join([self.deployment_code, self.location.location_id])
        if save:
            self.save()

    def save(self, **kwargs):
        if self.date_created is None:
            self.date_created = timezone.now()
        self.update_deployment_id(save=False)
        super(Deployment, self).save(**kwargs)

    def can_view(self, user=None):
        """Determines whether user can view the deployment.
        :param user: user for which the test is made
        :type user: :py:class:`django.contrib.auth.User`
        :return: True if user can see the deployment, False otherwise
        :rtype: bool
        """
        user = user or get_current_user()
        if user.is_staff or user == self.owner or user in self.managers.all():
            return True
        if (
            self.research_project
            and self.research_project.project_roles.filter(user=user).exists()
        ):
            return True
        if self.resources.filter(
            collection__members=user,
            collection__members__collectionmember__level__in=[
                CollectionMemberLevels.ACCESS,
                CollectionMemberLevels.ACCESS_REQUEST,
            ],
        ).exists():
            return True
        return False

    def can_update(self, user=None):
        user = user or get_current_user()
        return user.is_staff or user == self.owner or user in self.managers.all()

    def can_delete(self, user=None):
        """Deployment can not be deleted when there are resources that
        refer to it through a protected foreign key.
        """
        return self.can_update()

    def __str__(self):
        return str(self.deployment_id)
