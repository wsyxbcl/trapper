# -*- coding: utf-8 -*-

from django.urls import reverse
from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from trapper.apps.common.serializers import BasePKSerializer, PrettyUserField
from trapper.apps.geomap.models import Location, Deployment
from trapper.apps.geomap.taxonomy import DeploymentFeatureType
from umap.models import Map


class LocationSerializer(BasePKSerializer):
    """
    TODO: docstrings
    """

    class Meta:
        model = Location
        fields = (
            "pk",
            "name",
            "date_created",
            "description",
            "location_id",
            "is_public",
            "coordinates",
            "owner",
            "owner_profile",
            "city",
            "county",
            "state",
            "country",
            "research_project",
            "timezone",
            # data for action columns
            "update_data",
            "delete_data",
        )

    owner = PrettyUserField()
    research_project = serializers.ReadOnlyField(source="research_project.acronym")
    timezone = serializers.ReadOnlyField(source="timezone.__str__")
    owner_profile = serializers.SerializerMethodField()
    update_data = serializers.SerializerMethodField()
    delete_data = serializers.SerializerMethodField()
    coordinates = serializers.SerializerMethodField()

    def get_coordinates(self, item):
        return "{}, {}".format(round(item.get_x, 5), round(item.get_y, 5))

    def get_owner_profile(self, item):
        return reverse(
            "accounts:show_profile", kwargs={"username": item.owner.username}
        )

    def get_update_data(self, item):
        return Location.objects.api_update_context(
            item=item, user=self.context["request"].user
        )

    def get_delete_data(self, item):
        return Location.objects.api_delete_context(
            item=item, user=self.context["request"].user
        )


class LocationGeoSerializer(GeoFeatureModelSerializer):
    """
    A class to serialize locations as GeoJSON compatible data
    """

    class Meta:
        model = Location
        geo_field = "coordinates"
        fields = (
            "pk",
            "location_id",
            "name",
        )


class DeploymentSerializer(BasePKSerializer):
    """
    TODO: docstrings
    """

    class Meta:
        model = Deployment
        fields = (
            "pk",
            "deployment_code",
            "deployment_id",
            "location",
            "location_id",
            "start_date",
            "end_date",
            "owner",
            "owner_profile",
            "research_project",
            "tags",
            "correct_setup",
            "correct_tstamp",
            # data for action columns
            "detail_data",
            "update_data",
            "delete_data",
        )

    owner = PrettyUserField()
    location_id = serializers.ReadOnlyField(source="location.location_id")
    start_date = serializers.ReadOnlyField(source="start_date_tz")
    end_date = serializers.ReadOnlyField(source="end_date_tz")
    research_project = serializers.ReadOnlyField(source="research_project.acronym")
    tags = serializers.SerializerMethodField()
    owner_profile = serializers.SerializerMethodField()
    detail_data = serializers.SerializerMethodField()
    update_data = serializers.SerializerMethodField()
    delete_data = serializers.SerializerMethodField()

    def get_owner_profile(self, item):
        return reverse(
            "accounts:show_profile", kwargs={"username": item.owner.username}
        )

    def get_tags(self, obj):
        """Custom method for retrieving deployment tags"""
        return [k.name for k in obj.tags.all()]

    def get_detail_data(self, item):
        return Deployment.objects.api_detail_context(
            item=item, user=self.context["request"].user
        )

    def get_update_data(self, item):
        return Deployment.objects.api_update_context(
            item=item, user=self.context["request"].user
        )

    def get_delete_data(self, item):
        return Deployment.objects.api_delete_context(
            item=item, user=self.context["request"].user
        )


class MapSerializer(BasePKSerializer):
    """
    TODO: docstrings
    """

    class Meta:
        model = Map
        fields = (
            "pk",
            "slug",
            "description",
            "center",
            "zoom",
            "locate",
            "licence",
            "modified_at",
            "tilelayer",
            "owner",
            "owner_profile",
            "edit_status",
            "share_status",
            "settings",
            "delete_data",
            "detail_data",
        )

    owner = PrettyUserField()
    owner_profile = serializers.SerializerMethodField()
    licence = serializers.ReadOnlyField(source="licence.name")
    edit_status = serializers.ReadOnlyField(source="get_edit_status_display")
    share_status = serializers.ReadOnlyField(source="get_share_status_display")
    tilelayer = serializers.ReadOnlyField(source="tilelayer.url_template")

    delete_data = serializers.SerializerMethodField()
    detail_data = serializers.SerializerMethodField()

    def get_delete_data(self, item):
        user = self.context["request"].user
        if item.owner == user:
            return reverse("geomap:map_delete", kwargs={"pk": item.pk})

    def get_detail_data(self, item):
        user = self.context["request"].user
        if item.owner == user or user in item.editors.all():
            return reverse("geomap:map", kwargs={"pk": item.pk, "slug": item.slug})

    def get_owner_profile(self, item):
        return reverse(
            "accounts:show_profile", kwargs={"username": item.owner.username}
        )


class LocationTableSerializer(serializers.ModelSerializer):
    """
    TODO: docstrings
    """

    class Meta:
        model = Location
        fields = [
            "locationID",
            "longitude",
            "latitude",
            "country",
            "timezone",
            "ignoreDST",
            "habitat",
            "comments",
            "_id",
            "researchProject",
        ]

    locationID = serializers.ReadOnlyField(source="location_id")
    longitude = serializers.SerializerMethodField()
    latitude = serializers.SerializerMethodField()
    timezone = serializers.ReadOnlyField(source="timezone.__str__")
    ignoreDST = serializers.ReadOnlyField(source="ignore_DST")
    comments = serializers.ReadOnlyField(source="description")
    _id = serializers.ReadOnlyField(source="pk")
    researchProject = serializers.ReadOnlyField(source="research_project_id")

    # The rest are temporary "mockup" fields which are part of the standard.
    # TODO: add corresponding model fields when the consensus about the camera
    # trap data exchange standard is finally achieved
    habitat = serializers.ReadOnlyField(default=None)

    def get_longitude(self, item):
        return round(item.get_x, 5)

    def get_latitude(self, item):
        return round(item.get_y, 5)


class DeploymentTableSerializer(serializers.ModelSerializer):
    """
    Built according to Camtrap DP specifications for deployments.csv file
    Schema available at settings.CAMTRAP_PACKAGE_SCHEMAS_DEPLOYMENTS
    See: https://tdwg.github.io/camtrap-dp
    """

    class Meta:
        model = Deployment
        fields = [
            "deploymentID",
            "locationID",
            "locationName",
            "latitude",
            "longitude",
            "coordinateUncertainty",
            "deploymentStart",
            "deploymentEnd",
            "setupBy",
            "cameraID",
            "cameraModel",
            "cameraDelay",
            "cameraHeight",
            "cameraTilt",
            "cameraHeading",
            "detectionDistance",
            "timestampIssues",
            "baitUse",
            "featureType",
            "habitat",
            "deploymentGroups",
            "deploymentTags",
            "deploymentComments",
            "_id",
        ]

    # required fields
    deploymentID = serializers.ReadOnlyField(source="deployment_id")
    longitude = serializers.SerializerMethodField()
    latitude = serializers.SerializerMethodField()
    deploymentStart = serializers.SerializerMethodField()
    deploymentEnd = serializers.SerializerMethodField()

    # other fields specified by the standard
    locationID = serializers.ReadOnlyField(source="location.location_id")
    locationName = serializers.ReadOnlyField(source="location.name")
    coordinateUncertainty = serializers.ReadOnlyField(
        source="location.coordinate_uncertainty"
    )

    setupBy = serializers.ReadOnlyField(source="setup_by")
    cameraID = serializers.ReadOnlyField(source="camera_id")
    cameraModel = serializers.ReadOnlyField(source="camera_model")
    cameraDelay = serializers.ReadOnlyField(source="camera_interval")
    cameraHeight = serializers.ReadOnlyField(source="camera_height")
    cameraTilt = serializers.ReadOnlyField(source="camera_tilt")
    cameraHeading = serializers.ReadOnlyField(source="camera_heading")
    detectionDistance = serializers.ReadOnlyField(source="detection_distance")
    timestampIssues = serializers.SerializerMethodField()
    baitUse = serializers.SerializerMethodField(source="research_project")
    featureType = serializers.SerializerMethodField()
    habitat = serializers.ReadOnlyField()
    deploymentGroups = serializers.SerializerMethodField()
    deploymentTags = serializers.SerializerMethodField()
    deploymentComments = serializers.ReadOnlyField(source="comments")
    _id = serializers.ReadOnlyField(source="pk")

    def get_deploymentStart(self, item):
        if not item.start_date_tz:
            return ""
        return item.start_date_tz.strftime("%Y-%m-%dT%H:%M:%S%z")

    def get_deploymentEnd(self, item):
        if not item.end_date_tz:
            return ""
        return item.end_date_tz.strftime("%Y-%m-%dT%H:%M:%S%z")

    def get_longitude(self, item):
        return round(item.location.get_x, 5)

    def get_latitude(self, item):
        return round(item.location.get_y, 5)

    def get_timestampIssues(self, item):
        return not item.correct_tstamp

    def get_deploymentTags(self, item):
        return " | ".join([k.name for k in item.tags.all()])

    def get_baitUse(self, item):
        if not item.research_project:
            return False
        bait_use = item.research_project.get_bait_use_display()
        return bait_use != "none"

    def get_featureType(self, item):
        return DeploymentFeatureType.EXPORT_MAP.get(item.feature_type, "")

    def get_deploymentGroups(self, item):
        groups = []
        if item.session:
            groups.append(f"session:{item.session}")
        if item.array:
            groups.append(f"array:{item.array}")
        return " | ".join(groups)
