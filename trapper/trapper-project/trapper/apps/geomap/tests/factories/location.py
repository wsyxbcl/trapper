import datetime
import factory
from django.contrib.gis.geos import Point

from django.utils import timezone

from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)
from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.geomap.models import Location


class LocationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Location
        exclude = ("now", "point")

    location_id = factory.Sequence(lambda n: str(n))
    name = factory.lazy_attribute(lambda a: f"location_{a.location_id}")

    now = factory.LazyFunction(timezone.now)
    date_created = factory.LazyAttribute(lambda o: o.now - datetime.timedelta(days=10))

    description = factory.Faker("paragraph", nb_sentences=3)
    is_public = False

    point = factory.Faker("latlng")
    coordinates = factory.LazyAttribute(
        lambda o: Point([float(cord) for cord in o.point])
    )
    timezone = timezone.get_default_timezone()

    owner = factory.SubFactory(UserFactory)
    country = factory.Faker("country")
    state = factory.Faker("word")
    county = factory.Faker("word")
    city = factory.Faker("city")

    research_project = factory.SubFactory(ResearchProjectFactory)
