import datetime
from unittest.mock import patch

import pytz
from django.contrib.auth.models import AnonymousUser
from django.urls import reverse
from django.utils import timezone
from rest_framework.test import APITestCase
from trapper.apps.accounts.tests.factories.user import UserFactory

from trapper.apps.common.utils.test_tools import (
    ExtendedTestCase,
    LocationTestMixin,
    DeploymentTestMixin,
)
from trapper.apps.geomap.filters import (
    ResourceFilter,
    CollectionFilter,
    ClassificationFilter,
    MapOwnBooleanFilter,
)
from trapper.apps.geomap.models import Deployment
from trapper.apps.geomap.serializers import DeploymentSerializer
from trapper.apps.geomap.taxonomy import DeploymentViewQuality
from trapper.apps.geomap.tests.factories.deployment import DeploymentFactory
from trapper.apps.geomap.tests.factories.location import LocationFactory
from trapper.apps.media_classification.models import ClassificationProject
from trapper.apps.media_classification.tests.factories.classification import (
    ClassificationFactory,
)
from trapper.apps.media_classification.tests.factories.classification_project_collection import (
    ClassificationProjectCollectionFactory,
)
from trapper.apps.media_classification.tests.factories.classificator import (
    ClassificatorFactory,
)
from trapper.apps.research.models import ResearchProjectCollection
from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)
from trapper.apps.storage.models import Resource, Collection
from trapper.apps.storage.taxonomy import CollectionStatus, ResourceStatus
from trapper.apps.storage.tests.factories.classification_project import (
    ClassificationProjectFactory,
)
from trapper.apps.storage.tests.factories.collection import CollectionFactory
from trapper.apps.storage.tests.factories.resource import ResourceFactory


class BaseGeomapTestCase(
    ExtendedTestCase, LocationTestMixin, DeploymentTestMixin, APITestCase
):
    def setUp(self):
        super(BaseGeomapTestCase, self).setUp()
        self.summon_alice()
        self.summon_ziutek()


class AnonymousDeploymentTestCase(BaseGeomapTestCase):
    """Deployment logic for anonymous users"""

    def test_deployment_list(self):
        """Anonymous user can not access list"""
        url = reverse("geomap:deployment_list")

        response = self.client.get(url)

        self.assertEqual(response.status_code, 302)

    # def test_deployment_json(self):
    #     """Anonymous user can not access API"""
    #     url = reverse('geomap:api-deployment-list')
    #
    #     response = self.client.get(url)
    #
    #     print(response)
    #     self.assertEqual(response.status_code, 302)

    def test_deployment_details(self):
        """Anonymous user can not access details"""
        url = reverse("geomap:deployment_detail", kwargs={"pk": 1})
        self.assert_forbidden(url=url)

    def test_deployment_delete(self):
        """Anonymous user should not get access here."""
        url = reverse("geomap:deployment_delete", kwargs={"pk": 1})
        self.assert_forbidden(url=url, method="get")
        self.assert_forbidden(url=url, method="post")


class DeploymentTestCase(BaseGeomapTestCase):
    """Deployment permission logic for authenticated users"""

    def setUp(self):
        """Login alice by default for all tests"""
        super(DeploymentTestCase, self).setUp()
        self.login_alice()

    def test_deployment_list(self):
        """Authenticated user can access to deployment list"""
        url = reverse("geomap:deployment_list")
        self.assert_access_granted(url)

    # TODO: Fails before updating packages
    # def test_deployment_json(self):
    #     """Authenticated user can see accessible deployments"""
    #     name_public = 'ID_PUBLIC'
    #     name_private = 'ID_PRIVATE'
    #     name_private2 = 'ID_PRIVATE_2'
    #
    #     deployment_public = self.create_deployment(
    #         owner=self.alice, deployment_code=name_public,
    #     )
    #     deployment_private = self.create_deployment(
    #         owner=self.alice, deployment_code=name_private,
    #     )
    #     deployment_private2 = self.create_deployment(
    #         owner=self.ziutek, deployment_code=name_private2,
    #     )
    #
    #     url = reverse('geomap:api-deployment-list')
    #     response = self.client.get(url)
    #     content = json.loads(response.content)['results']
    #
    #     deployment_pk_list = [item['pk'] for item in content]
    #
    #     self.assertIn(deployment_public.pk, deployment_pk_list, 'Public not shown')
    #     self.assertIn(
    #         deployment_private.pk, deployment_pk_list, 'Private not shown'
    #     )
    #     self.assertNotIn(
    #         deployment_private2.pk, deployment_pk_list, 'Private2 shown'
    #     )

    def test_deployment_delete_owner(self):
        """Owner can delete own deployment. Public status doesn't matter."""
        deployment = self.create_deployment(owner=self.alice, deployment_code="ID_1")
        url = reverse("geomap:deployment_delete", kwargs={"pk": deployment.pk})
        redirection = reverse("geomap:deployment_list")
        self.assert_redirect(url, redirection=redirection)

        self.assertFalse(Deployment.objects.filter(pk=deployment.pk).exists())

    def test_deployment_delete_other(self):
        """Authenticated user cannot delete someone else deployment."""
        deployment = self.create_deployment(owner=self.ziutek, deployment_code="ID_2")
        url = reverse("geomap:deployment_delete", kwargs={"pk": deployment.pk})
        self.assert_forbidden(url)
        self.assertTrue(Deployment.objects.filter(pk=deployment.pk).exists())

    # TODO: Fails before updating packages
    # def test_deployment_delete_multiple(self):
    #     """Owner can delete own deployment using multiple delete.
    #     """
    #     deployment_owner = self.create_deployment(
    #         owner=self.alice, deployment_code='ID_1'
    #     )
    #     deployment_other = self.create_deployment(
    #         owner=self.ziutek, deployment_code='ID_2'
    #     )
    #
    #     params = {
    #         'pks': ",".join([
    #             str(deployment_owner.pk),
    #             str(deployment_other.pk),
    #             str(self.TEST_PK)
    #         ]),
    #     }
    #     url = reverse('geomap:deployment_delete_multiple')
    #     response = self.assert_access_granted(url, method='post', data=params)
    #     status = self.assert_json_context_variable(response, 'status')
    #     self.assertTrue(status)
    #
    #     self.assert_invalid_pk(response)
    #     self.assertFalse(
    #         Deployment.objects.filter(pk=deployment_owner.pk).exists()
    #     )
    #     self.assertTrue(
    #         Deployment.objects.filter(pk=deployment_other.pk).exists()
    #     )


class DeploymentTest(BaseGeomapTestCase):
    @classmethod
    def setUpTestData(cls):
        super(DeploymentTest, cls).setUpTestData()

        cls.user1 = UserFactory()
        cls.user2 = UserFactory()
        cls.user3 = UserFactory()
        cls.user4 = UserFactory()

        cls.research_project = ResearchProjectFactory(owner=cls.user1)

        cls.timezone = pytz.timezone("Europe/Warsaw")
        cls.location1 = LocationFactory(
            owner=cls.user1,
            research_project=cls.research_project,
            timezone="Europe/Warsaw",
        )

        cls.deployment1 = DeploymentFactory(
            owner=cls.user1,
            research_project=cls.research_project,
            location=cls.location1,
        )

        cls.resource1 = ResourceFactory(owner=cls.user1, deployment=cls.deployment1)
        cls.resource2 = ResourceFactory(owner=cls.user1, deployment=cls.deployment1)
        cls.resource3 = ResourceFactory(
            owner=cls.user1, deployment=cls.deployment1, inherit_prefix=True
        )
        cls.resource4 = ResourceFactory(owner=cls.user1)

        # Set resource managers
        cls.resource1.managers.set((cls.user2,))

        cls.collection1 = CollectionFactory(owner=cls.user1)
        cls.collection2 = CollectionFactory(owner=cls.user1)

        # # Set collection resources
        cls.collection1.resources.set((cls.resource1,))
        cls.collection2.resources.set((cls.resource1, cls.resource3))

        cls.collection = CollectionFactory(
            owner=cls.user1, status=CollectionStatus.PRIVATE
        )
        cls.collection.resources.set(Resource.objects.all())
        cls.classificator = ClassificatorFactory(owner=cls.user1)

        cls.classification_project = ClassificationProjectFactory(
            research_project=cls.research_project,
            owner=cls.user1,
            classificator=cls.classificator,
        )

        resources_array = Resource.objects.all()
        cls.research_project.collections.set([cls.collection])

        collection = ResearchProjectCollection.objects.filter(
            project=cls.classification_project.research_project.pk
        )[0]
        cls.classification_project_collection = ClassificationProjectCollectionFactory(
            project=cls.classification_project, collection=collection
        )

        for resource in resources_array:
            ClassificationFactory(
                resource=resource,
                owner=cls.user1,
                project=cls.classification_project,
                collection=cls.classification_project_collection,
            )

        cls.resource = ResourceFactory(
            owner=cls.user1, deployment=cls.deployment1, status=ResourceStatus.PRIVATE
        )
        cls.classification = ClassificationFactory(
            resource=cls.resource,
            owner=cls.user1,
            project=cls.classification_project,
            collection=cls.classification_project_collection,
        )

    def test_timezone_property(self):
        self.assertTrue(self.deployment1.timezone)

    def test_deployment_index_route(self):
        self.client.force_login(self.user1)
        response = self.client.get(reverse("geomap:deployment_index"))
        self.assertEqual(response.status_code, 302)

    def test_deployment_list_route(self):
        self.client.force_login(self.user1)
        response = self.client.get(reverse("geomap:deployment_list"))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Deployment")

    def test_deployment_detail_route(self):
        self.client.force_login(self.user1)
        response = self.client.get(
            reverse("geomap:deployment_detail", kwargs={"pk": self.deployment1.pk})
        )
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Deployment")

    def test_deployment_delete_route(self):
        self.client.force_login(self.user1)
        deployment1 = DeploymentFactory(
            owner=self.user1,
            research_project=self.research_project,
            location=self.location1,
        )
        self.assertTrue(Deployment.objects.filter(id=deployment1.pk).exists())
        response = self.client.get(
            reverse("geomap:deployment_delete", kwargs={"pk": deployment1.pk})
        )
        self.assertEqual(response.status_code, 302)
        self.assertFalse(Deployment.objects.filter(id=deployment1.pk).exists())

    def test_deployment_create_route(self):
        self.client.force_login(self.user1)

        new_deployment = {
            "deployment_id": 0,
            "deployment_code": "word",
            "owner": self.user1,
            "comments": ["comments"],
            "research_project": self.research_project,
            "location": self.location1,
            "now": timezone.now(),
            "date_created": "datetime.datetime.now() - datetime.timedelta(days=11)",
            "start_date": datetime.datetime.now() - datetime.timedelta(days=10),
            "end_date": datetime.datetime.now() + datetime.timedelta(days=10),
            "correct_setup": True,
            "correct_tstamp": True,
            "view_quality": DeploymentViewQuality.GOOD,
        }

        response = self.client.get(reverse("geomap:deployment_create"), new_deployment)
        self.assertEqual(response.status_code, 200)

        response = self.client.post(reverse("geomap:deployment_create"), new_deployment)
        self.assertEqual(response.status_code, 200)
        new_deployment["date_created"] = datetime.datetime.now() - datetime.timedelta(
            days=11
        )
        response = self.client.post(reverse("geomap:deployment_create"), new_deployment)
        self.assertEqual(response.status_code, 200)

    def test_deployment_update_route(self):
        self.client.force_login(self.user1)
        deployment1 = DeploymentFactory(
            deployment_code="test",
            owner=self.user1,
            research_project=self.research_project,
            location=self.location1,
        )
        self.assertEqual(deployment1.deployment_code, "test")
        response = self.client.get(
            reverse("geomap:deployment_update", kwargs={"pk": deployment1.pk})
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(deployment1.deployment_code, "test")

    def test_deployment_bulk_create_route(self):
        self.client.force_login(self.user1)
        new_deployment = {
            "deployment_id": 0,
            "deployment_code": "word",
            "location_pks": "word",
            "research_project": self.research_project.pk,
            "start_date": (
                datetime.datetime.now() - datetime.timedelta(days=10)
            ).strftime("%d.%m.%Y %H:%M:%S"),
            "end_date": (
                datetime.datetime.now() + datetime.timedelta(days=10)
            ).strftime("%d.%m.%Y %H:%M:%S"),
            "managers": [self.user1.pk],
        }

        response = self.client.get(reverse("geomap:deployment_bulk_create"))
        self.assertEqual(response.status_code, 200)

        response = self.client.post(
            reverse("geomap:deployment_bulk_create"), new_deployment
        )
        self.assertEqual(response.status_code, 200)

    # @patch("trapper.apps.research.models.ResearchProject.objects.get_accessible")
    # @patch("trapper.apps.geomap.models.Location.objects.get_available")
    # def test_deployment_bulk_create_form_valid(self, mock_rp_get_accessible, mock_location_get_available):
    #     mock_rp_get_accessible.return_value = ResearchProject.objects.all()
    #     mock_location_get_available.return_value = Location.objects.all()
    #     new_deployment = {
    #         'deployment_id': 0,
    #         'deployment_code': 'word',
    #         'location_pks': 'word',
    #         'research_project': self.research_project,
    #         'start_date': (datetime.datetime.now() - datetime.timedelta(days=10)).strftime('%d.%m.%Y %H:%M:%S'),
    #         'end_date': (datetime.datetime.now() + datetime.timedelta(days=10)).strftime('%d.%m.%Y %H:%M:%S'),
    #         'managers': [],
    #     }
    #
    #     bulk_create_form = BulkCreateDeploymentForm(data=new_deployment)
    #     self.assertTrue(bulk_create_form.is_valid())
    #     # DeploymentBulkCreateView.form_valid(bulk_create_form)

    def test_deployment_import_route(self):
        self.client.force_login(self.user1)

        response = self.client.get(reverse("geomap:deployment_import"))
        self.assertEqual(response.status_code, 200)
        response = self.client.post(reverse("geomap:deployment_import"))
        self.assertEqual(response.status_code, 200)

    def test_deployment_get_accessible(self):
        Deployment.objects.get_accessible(user=self.user1)
        Deployment.objects.get_accessible(user=self.user2)
        Deployment.objects.get_accessible(user=AnonymousUser())
        Deployment.objects.get_accessible(
            user=self.user1, base_queryset=Deployment.objects.all()
        )

    def test_deployment_api_update_context(self):
        Deployment.objects.api_update_context(self.deployment1, self.user1)
        Deployment.objects.api_update_context(self.deployment1, self.user2)
        Deployment.objects.api_update_context(self.deployment1, AnonymousUser())

    def test_deployment_api_detail_context(self):
        Deployment.objects.api_detail_context(self.deployment1, self.user1)
        Deployment.objects.api_detail_context(self.deployment1, self.user2)
        Deployment.objects.api_detail_context(self.deployment1, AnonymousUser())

    @patch("trapper.apps.geomap.models.get_current_user")
    def test_deployment_api_delete_context(self, mock_get_current_user):
        mock_get_current_user.return_value = self.user1
        Deployment.objects.api_delete_context(self.deployment1, self.user1)
        mock_get_current_user.return_value = self.user2
        Deployment.objects.api_delete_context(self.deployment1, self.user2)
        mock_get_current_user.return_value = AnonymousUser()
        Deployment.objects.api_delete_context(self.deployment1, AnonymousUser())

    def test_deployment_can_view(self):
        self.deployment1.can_view(user=self.user1)
        self.deployment1.can_view(user=self.user2)
        self.deployment1.can_view(user=self.user3)

    def test_deployment_resource_filter(self):
        queryset = Resource.objects.all()
        ResourceFilter().filter(queryset, "0,1")

    def test_deployment_collection_filter(self):
        queryset = Collection.objects.all()
        CollectionFilter().filter(queryset, "0,1")

    def test_deployment_classification_filter(self):
        queryset = ClassificationProject.objects.all()
        ClassificationFilter().filter(queryset, "0")

    @patch("trapper.apps.geomap.filters.get_current_user")
    def test_map_own_filter(self, mock_get_current_user):
        mock_get_current_user.return_value = self.user1
        queryset = Resource.objects.all()
        MapOwnBooleanFilter().filter(queryset, "")
        MapOwnBooleanFilter().filter(queryset, True)

    def test_deployment_serializer(self):
        serializer = DeploymentSerializer(self.deployment1)

        serializer.get_owner_profile(self.deployment1)

        serializer.get_tags(self.deployment1)

        # serializer.get_detail_data(self.deployment1)
        #
        # serializer.get_update_data(self.deployment1)
        #
        # serializer.get_delete_data(self.deployment1)

    def test_deployment_timezone_behaviour(self):
        self.client.force_login(self.user1)
        start_date = datetime.datetime(2022, 7, 1, 8, 0, tzinfo=None)
        end_date = datetime.datetime(2022, 7, 14, 16, 0, tzinfo=None)
        new_deployment_data = {
            "deployment_code": "word",
            "research_project": self.research_project.pk,
            "location": self.location1.pk,
            "start_date": start_date,
            "end_date": end_date,
            "correct_setup": True,
            "correct_tstamp": True,
            "view_quality": DeploymentViewQuality.GOOD,
            "feature_type": "road overpass",
            "capture_method": 1,
        }

        self.client.post(reverse("geomap:deployment_create"), new_deployment_data)
        new_deployment = Deployment.objects.get(deployment_code="word")

        # assert tz data is copied from location
        self.assertEqual(new_deployment.timezone, self.timezone)
        self.assertFalse(new_deployment.ignore_DST)

        # assert dates are correctly calculated to UTC
        self.assertEqual(
            new_deployment.start_date,
            datetime.datetime(2022, 7, 1, 6, 0, tzinfo=pytz.UTC),
        )
        self.assertEqual(
            new_deployment.end_date,
            datetime.datetime(2022, 7, 14, 14, 0, tzinfo=pytz.UTC),
        )

        # assert dates are serialized with correct offsets
        self.assertEqual(
            new_deployment.start_date_tz.strftime("%Y-%m-%d %H:%M %z"),
            "2022-07-01 08:00 +0200",
        )
        self.assertEqual(
            new_deployment.end_date_tz.strftime("%Y-%m-%d %H:%M %z"),
            "2022-07-14 16:00 +0200",
        )
