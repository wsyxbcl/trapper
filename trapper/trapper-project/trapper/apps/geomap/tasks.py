import dateutil
import frictionless
import pandas
import requests
from celery import shared_task
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.gis.geos import Point
from django.utils.timezone import now
from taggit.models import Tag

from trapper.apps.common.frictionless_checks import DbExistingObjects
from trapper.apps.common.utils.datetime_tools import localize_datetime_dst
from trapper.apps.geomap.models import Location, Deployment
from trapper.apps.geomap.taxonomy import LocationSettings, DeploymentSettings


class LocationImporter:
    """
    Locations importer. See https://tdwg.github.io/camtrap-dp
    """

    # Camtrap DP does not have a separate table for storing locations only; for validation
    # use the schema available for deployments.csv table
    limit_validation_errors = 100
    schema_url = settings.CAMTRAP_PACKAGE_SCHEMAS_DEPLOYMENTS
    schema_dtypes_dict = {"locationID": str}

    def __init__(
        self,
        user,
        research_project,
        timezone,
        ignore_DST=False,
        csv_data=None,
        gpx_data=None,
    ):
        self.user = user
        self.project = research_project
        self.timezone = timezone
        self.ignore_DST = ignore_DST
        self.data = None
        self.schema = None
        self.report = None
        if csv_data is not None:
            self.data = csv_data
        else:
            self.data = self.gpx_to_table(gpx_data)
        self.provided_columns = self.data.columns
        self.required_columns = LocationSettings.IMPORT_REQUIRED_COLUMNS

        # first quickly check if all required columns are provided
        if not set(self.required_columns).issubset(set(self.provided_columns)):
            raise ValueError(
                "There are missing columns that are required:",
                self.provided_columns,
                self.required_columns,
            )

        self.extra_columns = {
            k: v
            for k, v in LocationSettings.IMPORT_COLUMNS_MAP.items()
            if k in self.provided_columns
        }

        self.new_schema = self.get_schema()
        # only use fields specified in new schema - if required fields are not provided
        # the validation error will be raised later
        schema_fields = [f.name for f in self.new_schema.fields]
        data_fields = [f for f in schema_fields if f in self.provided_columns]
        # drop duplicates but preserve nan's for validation
        self.data = self.data[
            (~self.data.duplicated("locationID")) | (self.data["locationID"].isnull())
        ]
        self.data = self.data[data_fields].fillna("")
        self.data = self.data.astype(self.schema_dtypes_dict)

    def gpx_to_table(self, gpx_data):
        records = []
        for waypoint in getattr(gpx_data, "waypoints", []):
            records.append(
                {
                    "locationID": waypoint.name,
                    "longitude": waypoint.longitude,
                    "latitude": waypoint.latitude,
                }
            )
        return pandas.DataFrame(records)

    def get_schema(self):
        full_schema = frictionless.Schema.from_descriptor(
            requests.get(self.schema_url).json()
        )
        new_schema = frictionless.Schema()

        # first add all required fields to the new schema
        for field_name in self.required_columns:
            field = full_schema.get_field(field_name)
            field.constraints["required"] = True
            new_schema.add_field(field)

        # then add non-required fields if they come with the data
        for field_name in self.extra_columns:
            field = full_schema.get_field(field_name)
            new_schema.add_field(field)

        return new_schema

    def validate_table(self):
        """
        Validate the locations table using frictionless and provided schema.
        """
        records = self.data.to_dict("records")
        self.report = frictionless.validate(
            records, schema=self.new_schema, limit_errors=self.limit_validation_errors
        )
        if not self.report.valid:
            return

        # check for already existing locations with given location_id within a specified
        # research project use "blacklist" check
        project_locations = self.project.locations.get_available(
            user=self.user, editable_only=True
        )
        location_ids = list(project_locations.values_list("location_id", flat=True))
        self.report = frictionless.validate(
            records,
            schema=self.new_schema,
            checks=[
                DbExistingObjects(
                    field_name="locationID",
                    values=location_ids,
                    check_type="blacklist",
                )
            ],
            limit_errors=self.limit_validation_errors,
        )

    def import_locations(self, return_dict=False):
        locations = []
        for _index, row in self.data.iterrows():
            # first set required properties
            coords = Point((float(row.longitude), float(row.latitude)), srid=4326)
            location = Location(
                location_id=row.locationID,
                coordinates=coords,
                timezone=self.timezone,
                ignore_DST=self.ignore_DST,
            )
            # set non-required fields
            for attr_data, attr_trapper in self.extra_columns.items():
                val = row.get(attr_data)
                if val:
                    setattr(location, attr_trapper, val)
            location.owner = self.user
            location.research_project = self.project
            location.date_created = now()
            locations.append(location)

        Location.objects.bulk_create(locations, batch_size=settings.BULK_BATCH_SIZE)
        if return_dict:
            return {loc.location_id: loc.pk for loc in locations}
        return f"You have successfully imported {len(locations)} locations."


@shared_task(serializer="pickle")
def celery_import_locations(**kwargs):
    """
    Celery task that imports locations from csv or gpx file.
    """
    importer = LocationImporter(**kwargs)
    return importer.import_locations()


class DeploymentImporter:
    """
    Deployments importer. See https://tdwg.github.io/camtrap-dp
    """

    limit_validation_errors = 100
    schema_url = settings.CAMTRAP_PACKAGE_SCHEMAS_DEPLOYMENTS
    schema_dtypes_dict = {"locationID": str, "deploymentID": str, "_id": str}
    datetime_fields = ["deploymentStart", "deploymentEnd"]

    def __init__(
        self,
        user,
        research_project,
        timezone,
        data=None,
        update=False,
        create_locations=False,
        ignore_DST=False,
    ):
        self.user = user
        self.project = research_project
        self.timezone = timezone
        self.update = update
        self.create_locations = create_locations
        self.ignore_DST = ignore_DST
        self.data = data
        self.schema = None
        self.report = None
        self.provided_columns = list(self.data.columns)
        self.required_columns = list(DeploymentSettings.IMPORT_REQUIRED_COLUMNS)
        if not create_locations:
            self.required_columns.remove("latitude")
            self.required_columns.remove("longitude")
        if update:
            self.required_columns.append("_id")

        # first quickly check if all required columns are provided
        if not set(self.required_columns).issubset(set(self.provided_columns)):
            raise ValueError(
                "column-missing",
                "There are missing columns that are required:",
                self.provided_columns,
                self.required_columns,
            )

        project_locations = self.project.locations.get_available(
            user=self.user, editable_only=True
        )
        self.project_locations_ids = list(
            project_locations.values_list("location_id", flat=True)
        )
        project_deployments = self.project.deployments.get_accessible(
            user=self.user, editable_only=True
        ).values_list("pk", "deployment_id")
        project_deployments = list(zip(*project_deployments))
        if len(project_deployments) > 0:
            self.project_deployments_pks = list(project_deployments[0])
            self.project_deployments_ids = list(project_deployments[1])
        else:
            self.project_deployments_pks = []
            self.project_deployments_ids = []

        self.new_schema = self.get_schema()
        # only use fields specified in the new schema
        schema_fields = self.new_schema.field_names
        data_fields = [f for f in schema_fields if f in self.provided_columns]

        # fill nans and assign proper types to IDs columns for further frictionless validation
        self.data = self.data[data_fields].fillna("")
        dtypes_dict = {
            k: v for k, v in self.schema_dtypes_dict.items() if k in data_fields
        }
        self.data = self.data.astype(dtypes_dict)

        # if using existing locations, check that locations referenced in the data have the same timezone info
        # as provided in the form
        if not self.create_locations:
            data_location_ids = self.data["locationID"].unique()
            data_locations = Location.objects.filter(
                research_project=self.project, location_id__in=data_location_ids
            )
            if not len(data_locations) == len(data_location_ids):
                raise ValueError(
                    "error-with-message",
                    "Locations referenced in the data do not exist in the database, choose the 'Create locations' "
                    "option in the import form to create them.",
                )

            dl_timezone = list(
                data_locations.values_list("timezone", flat=True).distinct().order_by()
            )
            dl_ignore_DST = list(
                data_locations.values_list("ignore_DST", flat=True)
                .distinct()
                .order_by()
            )

            if len(dl_timezone) > 1 or len(dl_ignore_DST) > 1:
                raise ValueError(
                    "error-with-message",
                    "Locations referenced in the data have different timezones. You can only import data from deployments "
                    "in the same timezone and with the same ignore_DST flag.",
                )

            if dl_timezone != [self.timezone] or dl_ignore_DST != [self.ignore_DST]:
                raise ValueError(
                    "error-with-message",
                    "Locations referenced in the data have different timezone info than provided in the form.",
                )

        # cut the time offset from datetime values
        for col in self.datetime_fields:
            # The regular expression matches a string that starts with a + or - sign, followed by either four digits
            # or two digits, a colon, and two digits, and then an end-of-line character.
            self.data[col] = self.data[col].str.replace(
                r"[+-](\d{4}|\d{2}\:\d{2})$", "", regex=True
            )

        if create_locations:
            self.loc_importer = LocationImporter(
                user=self.user,
                research_project=self.project,
                timezone=self.timezone,
                csv_data=self.data,
                ignore_DST=self.ignore_DST,
            )

    def get_schema(self):
        """
        Downloads a full schema for deployments.csv file and rebuilds its structure.
        """
        full_schema = frictionless.Schema.from_descriptor(
            requests.get(self.schema_url).json()
        )
        new_schema = frictionless.Schema()

        # first add all required fields to the new schema
        for field_name in self.required_columns:
            if field_name == "_id":
                field = frictionless.fields.IntegerField(name=field_name)
            else:
                field = full_schema.get_field(name=field_name)
            field.constraints["required"] = True
            # overwrite timestamp fields required format to validate dates without tz info
            if field_name in self.datetime_fields:
                field.example = ""
                field.format = "%Y-%m-%dT%H:%M:%S"
            new_schema.add_field(field)

        # then add all fields in the data that are part of the original schema
        for field_name in self.provided_columns:
            if not new_schema.has_field(field_name):
                if full_schema.has_field(field_name):
                    field = full_schema.get_field(field_name)
                    field.constraints["required"] = False
                    new_schema.add_field(field)
        return new_schema

    def validate_table(self):
        """
        Validate the deployments table using frictionless-py and provided schema.
        """
        if self.create_locations:
            self.loc_importer.validate_table()
            if not self.loc_importer.report.valid:
                self.report = self.loc_importer.report
                return
        records = self.data.to_dict("records")
        self.report = frictionless.validate(
            records, schema=self.new_schema, limit_errors=self.limit_validation_errors
        )
        if not self.report.valid:
            return

        frictionless_checks = []
        if not self.update:
            # check already existing deployments - "blacklist" check
            frictionless_checks.append(
                DbExistingObjects(
                    field_name="deploymentID",
                    values=self.project_deployments_ids,
                    check_type="blacklist",
                )
            )
        else:
            # check already existing deployments - "whitelist" check
            frictionless_checks.append(
                DbExistingObjects(
                    field_name="_id",
                    values=self.project_deployments_pks,
                    check_type="whitelist",
                )
            )
        if not self.create_locations:
            # check that the table references only already existing locations - "whitelist" check
            frictionless_checks.append(
                DbExistingObjects(
                    field_name="locationID",
                    values=self.project_locations_ids,
                    check_type="whitelist",
                )
            )

        self.report = frictionless.validate(
            records,
            schema=self.new_schema,
            checks=frictionless_checks,
            limit_errors=self.limit_validation_errors,
        )

    def get_dates(self, start_date, end_date):
        # parse %Y-%m-%dT%H:%M:%S string to datetime
        start = dateutil.parser.parse(start_date)
        start = localize_datetime_dst(start, self.timezone, self.ignore_DST)

        end = dateutil.parser.parse(end_date)
        end = localize_datetime_dst(end, self.timezone, self.ignore_DST)
        return start, end

    def prepare_tags(self):
        """
        Create or get tags from the deploymentTags column.
        """
        unique_tags = list(
            self.data.deploymentTags.str.split("|").explode().str.strip().unique()
        )
        tags_dict = {}
        for tag_name in unique_tags:
            tag = Tag.objects.get_or_create(name=tag_name)[0]
            tags_dict[tag_name] = tag
        return tags_dict

    def import_deployments(self):
        # create or get locations first
        if self.create_locations:
            locations = self.loc_importer.import_locations(return_dict=True)
        else:
            unique_location_ids = list(self.data.locationID.unique())
            locations = self.project.locations.filter(
                research_project=self.project, location_id__in=unique_location_ids
            ).values_list("location_id", "pk")
            locations = dict(locations)

        deployments = []
        for _index, row in self.data.iterrows():
            start, end = self.get_dates(row.deploymentStart, row.deploymentEnd)
            deployment_id = row.deploymentID

            # get deployment_code
            try:
                # try to parse deployment_id following Trapper's naming convention
                dcode, locid = deployment_id.split("-")
                if locid == row.locationID:
                    deployment_code = dcode
                else:
                    deployment_code = deployment_id
            except ValueError:
                deployment_code = deployment_id

            deployment = Deployment(
                deployment_id=deployment_id,
                deployment_code=deployment_code,
                location_id=locations[row.locationID],
                start_date=start,
                end_date=end,
            )

            # set non-required properties
            extra_columns = {
                k: v
                for k, v in DeploymentSettings.IMPORT_COLUMNS_MAP.items()
                if k in self.provided_columns
            }
            for attr_data, attr_trapper in extra_columns.items():
                val = row.get(attr_data)
                if val:
                    if attr_data == "timestampIssues":
                        val = not val
                    setattr(deployment, attr_trapper, val)

            if self.update:
                deployment.pk = int(row["_id"])
            else:
                deployment.owner = self.user
                deployment.research_project = self.project
                deployment.date_created = now()

            # parse deploymentGroups column to recover session and array
            # from "session:12 | array:s4" to {"session": 12, "array": "s4"}
            if "deploymentGroups" in self.provided_columns:
                deployment_groups = row.get("deploymentGroups")
                if deployment_groups:
                    deployment_groups_dict = {
                        k.strip(): v.strip()
                        for k, v in [x.split(":") for x in deployment_groups.split("|")]
                    }
                    if "session" in deployment_groups_dict:
                        deployment.session = deployment_groups_dict["session"]
                    if "array" in deployment_groups_dict:
                        deployment.array = deployment_groups_dict["array"]

            deployments.append(deployment)

        if self.update:
            cols_to_update = list(extra_columns.values())
            if "deploymentGroups" in self.provided_columns:
                cols_to_update.append("session")
                cols_to_update.append("array")

            Deployment.objects.bulk_update(
                deployments,
                fields=[
                    "deployment_id",
                    "deployment_code",
                    "location_id",
                    "start_date",
                    "end_date",
                ]
                + cols_to_update,
                batch_size=settings.BULK_BATCH_SIZE,
            )
            msg = "updated"
        else:
            deployments = Deployment.objects.bulk_create(
                deployments, batch_size=settings.BULK_BATCH_SIZE
            )
            msg = "imported"

        # process deployment tags if needed
        if "deploymentTags" in self.provided_columns:
            tags_dict = self.prepare_tags()
            tags_through_model = Deployment.tags.through
            content_type = ContentType.objects.get_for_model(Deployment)
            tags_to_add = []
            for _index, row in self.data.iterrows():
                tags = row.get("deploymentTags")
                if tags:
                    for tag in tags.split("|"):
                        tags_to_add.append(
                            tags_through_model(
                                tag=tags_dict[tag.strip()],
                                object_id=deployments[_index].pk,
                                content_type=content_type,
                            )
                        )
            deployments_pks = [k.pk for k in deployments]
            # delete already existing TaggedItems
            tags_through_model.objects.filter(object_id__in=deployments_pks).delete()
            # create new TaggedItems
            tags_through_model.objects.bulk_create(
                tags_to_add, batch_size=settings.BULK_BATCH_SIZE
            )

        return f"You have successfully {msg} {len(deployments)} deployments."


@shared_task(serializer="pickle")
def celery_import_deployments(**kwargs):
    """
    Celery task that imports deployments from csv file.
    """
    importer = DeploymentImporter(**kwargs)
    return importer.import_deployments()
