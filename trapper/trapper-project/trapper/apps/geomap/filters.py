# -*- coding: utf-8 -*-
import django_filters
from django import forms
from django.contrib.auth import get_user_model
from django.contrib.gis.geos import Polygon, GEOSException, Point
from django.contrib.gis.measure import D
from django.db.models import Exists, OuterRef
from django.shortcuts import get_object_or_404

from trapper.apps.common.filters import (
    BaseFilterSet,
    BaseOwnBooleanFilter,
    BaseDateFilter,
    BaseLocationsMapFilter,
)
from trapper.apps.common.tools import parse_pks
from trapper.apps.geomap.models import Location, Deployment, Map
from trapper.apps.media_classification.models import ClassificationProject
from trapper.apps.research.models import ResearchProject
from trapper.middleware import get_current_user

User = get_user_model()


class BaseFilter(django_filters.Filter):
    def _format_value(self, value):
        return parse_pks(pks=value)


class ResourceFilter(BaseFilter):
    def filter(self, qs, value):
        ids = self._format_value(value)
        if len(ids) <= 0:
            return qs
        resources = Deployment.objects.filter(
            location=OuterRef("pk"), resources__pk__in=ids
        ).values("pk")
        qs = qs.annotate(resource=Exists(resources)).filter(resource=True)
        return qs


class CollectionFilter(BaseFilter):
    def filter(self, qs, value):
        ids = self._format_value(value)
        if len(ids) <= 0:
            return qs
        collections = Deployment.objects.filter(
            location=OuterRef("pk"), resources__collection__pk__in=ids
        ).values("pk")
        qs = qs.annotate(collection=Exists(collections)).filter(collection=True)
        return qs


class ClassificationFilter(BaseFilter):
    def filter(self, qs, value):
        ids = self._format_value(value)
        if len(ids) <= 0:
            return qs
        classifications = Deployment.objects.filter(
            location=OuterRef("pk"), resources__classifications__pk__in=ids
        ).values("pk")
        qs = qs.annotate(classification=Exists(classifications)).filter(
            classification=True
        )
        return qs


class BBFilter(django_filters.Filter):
    field_class = forms.CharField

    def filter(self, qs, value):
        try:
            bbox = value.split(",")
            geom = Polygon.from_bbox(bbox)
        except (ValueError, GEOSException):
            return qs
        qs = qs.filter(coordinates__within=geom)
        return qs


class RadiusFilter(django_filters.Filter):
    field_class = forms.CharField

    def filter(self, qs, value):
        try:
            radius = value.split(",")
            radius = [float(x) for x in radius]
            geom = Point(radius[:2])
        except (ValueError, GEOSException):
            return qs
        qs = qs.filter(coordinates__distance_lte=(geom, D(m=radius[-1])))
        return qs


class LocationFilter(BaseFilterSet):
    name = django_filters.CharFilter(lookup_expr="icontains")
    description = django_filters.CharFilter(lookup_expr="icontains")
    owner = BaseOwnBooleanFilter()
    owners = django_filters.MultipleChoiceFilter(
        field_name="owner", choices=User.objects.values_list("pk", "username")
    )
    research_project = django_filters.MultipleChoiceFilter(
        choices=ResearchProject.objects.values_list("pk", "acronym")
    )
    locations_map = BaseLocationsMapFilter(field_name="pk")
    deployments = django_filters.CharFilter(method="filter_deployments")

    class Meta:
        model = Location
        exclude = ["timezone", "coordinates"]

    def filter_deployments(self, qs, name, value):
        if value.strip() == "":
            return qs
        ids = parse_pks(pks=value)
        qs = qs.filter(deployments__pk__in=ids)
        return qs


class LocationGeoFilter(LocationFilter):
    research_project = django_filters.ChoiceFilter(
        choices=ResearchProject.objects.values_list("pk", "acronym")
    )
    locations = BaseLocationsMapFilter(field_name="pk")
    colls = CollectionFilter(field_name="colls")
    reses = ResourceFilter(field_name="reses")
    classes = ClassificationFilter(field_name="classes")
    radius = RadiusFilter()

    class Meta:
        model = Location
        exclude = ["timezone", "coordinates"]


# Deployments filters


class DeploymentFilter(BaseFilterSet):
    """Filter used in deployment list view"""

    location = django_filters.MultipleChoiceFilter()
    research_project = django_filters.MultipleChoiceFilter(
        choices=ResearchProject.objects.values_list("pk", "acronym")
    )
    tags = django_filters.MultipleChoiceFilter()
    owner = BaseOwnBooleanFilter()
    sdate_from = BaseDateFilter(field_name="start_date", lookup_expr=("gte"))
    sdate_to = BaseDateFilter(field_name="start_date", lookup_expr=("lte"))
    edate_from = BaseDateFilter(field_name="end_date", lookup_expr=("gte"))
    edate_to = BaseDateFilter(field_name="end_date", lookup_expr=("lte"))
    classification_project = django_filters.CharFilter(method="filter_cproject")
    correct_setup = django_filters.BooleanFilter()
    correct_tstamp = django_filters.BooleanFilter()

    class Meta:
        model = Deployment
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(DeploymentFilter, self).__init__(*args, **kwargs)
        # set choices for multiple choice filters
        if self.data.get("location", None):
            self.filters["location"].field.choices = (
                self.queryset.values_list("location__pk", "location__location_id")
                .order_by("id")
                .distinct("id")
            )
        if self.data.get("tags", None):
            self.filters["tags"].field.choices = Deployment.tags.values_list(
                "pk", "name"
            )

    def filter_cproject(self, qs, name, value):
        try:
            int(value)
        except ValueError:
            return qs.none()
        cproject = get_object_or_404(ClassificationProject, pk=value)
        pks = set(
            cproject.collections.values_list(
                "collection__resources__deployment", flat=True
            )
        )
        return qs.filter(pk__in=pks)


class MapOwnBooleanFilter(django_filters.Filter):
    """Base boolean filter class for limiting queryset to values that are
    owned by currently logged in user"""

    field_class = forms.BooleanField

    def filter(self, queryset, value):
        user = get_current_user()
        if value is True:
            # Only owner or manager resources
            queryset = queryset.filter(owner=user)
        return queryset


class MapFilter(BaseFilterSet):
    """Filter used in map list view"""

    owner = MapOwnBooleanFilter()

    class Meta:
        model = Map
        fields = [
            "owner",
        ]
