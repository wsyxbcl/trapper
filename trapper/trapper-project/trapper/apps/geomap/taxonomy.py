# -*- coding: utf-8 -*-
from django.utils.translation import gettext_lazy as _

from trapper.apps.common.taxonomy import BaseTaxonomy


class LocationSettings(BaseTaxonomy):
    MEDIA_EXTENSIONS = [
        ".gpx",
    ]

    IMPORT_REQUIRED_COLUMNS = ["locationID", "longitude", "latitude"]

    IMPORT_COLUMNS_MAP = {
        "locationName": "name",
        "coordinateUncertainty": "coordinate_uncertainty",
    }


class DeploymentSettings(BaseTaxonomy):
    IMPORT_REQUIRED_COLUMNS = [
        "deploymentID",
        "locationID",
        "longitude",
        "latitude",
        "deploymentStart",
        "deploymentEnd",
    ]

    # maps non-required columns from deployments.csv file to fields in
    # the Deployment model
    IMPORT_COLUMNS_MAP = {
        "setupBy": "setup_by",
        "cameraID": "camera_id",
        "cameraModel": "camera_model",
        "cameraDelay": "camera_interval",
        "cameraHeight": "camera_height",
        "cameraTilt": "camera_tilt",
        "cameraHeading": "camera_heading",
        "detectionDistance": "detection_distance",
        "timestampIssues": "correct_tstamp",
        "featureType": "feature_type",
        "habitat": "habitat",
        "deploymentComments": "comments",
    }


class DeploymentViewQuality(BaseTaxonomy):
    EXCLUDE = "Exclude"
    ACCEPTABLE = "Acceptable"
    GOOD = "Good"
    PERFECT = "Perfect"

    CHOICES = (
        (EXCLUDE, _("Exclude")),
        (ACCEPTABLE, _("Acceptable")),
        (GOOD, _("Good")),
        (PERFECT, _("Perfect")),
    )


class DeploymentFeatureType(BaseTaxonomy):
    NONE = "none"
    ROAD_PAVED = "road paved"
    ROAD_DIRT = "road_dirt"
    TRAIL_HIKING = "trail hiking"
    TRAIL_GAME = "trail game"
    ROAD_UNDERPASS = "road underpass"
    ROAD_OVERPASS = "road overpass"
    ROAD_BRIDGE = "road bridge"
    CULVERT = "culvert"
    BURROW = "burrow"
    NEST_SITE = "nest site"
    CARCASS = "carcass"
    WATER_SOURCE = "water source"
    FRUITING_TREE = "fruiting tree"
    OTHER = "other"

    CHOICES = (
        (NONE, _("None")),
        (ROAD_PAVED, _("Road paved")),
        (ROAD_DIRT, _("Road dirt")),
        (TRAIL_HIKING, _("Trail hiking")),
        (TRAIL_GAME, _("Trail game")),
        (ROAD_UNDERPASS, _("Road underpass")),
        (ROAD_OVERPASS, _("Road overpass")),
        (ROAD_BRIDGE, _("Road bridge")),
        (CULVERT, _("Culvert")),
        (BURROW, _("Burrow")),
        (NEST_SITE, _("Nest site")),
        (CARCASS, _("Carcass")),
        (WATER_SOURCE, _("Water source")),
        (FRUITING_TREE, _("Fruiting tree")),
        (OTHER, _("Other")),
    )

    EXPORT_MAP = {
        NONE: "",
        ROAD_PAVED: "roadPaved",
        ROAD_DIRT: "roadDirt",
        TRAIL_HIKING: "trailHiking",
        TRAIL_GAME: "trailGame",
        ROAD_UNDERPASS: "roadUnderpass",
        ROAD_OVERPASS: "roadOverpass",
        ROAD_BRIDGE: "roadBridge",
        CULVERT: "culvert",
        BURROW: "burrow",
        NEST_SITE: "nestSite",
        CARCASS: "carcass",
        WATER_SOURCE: "waterSource",
        FRUITING_TREE: "fruitingTree",
        OTHER: "",
    }


class DeploymentCaptureMethod(BaseTaxonomy):
    MOTION = 1
    TIMELAPSE = 2

    CHOICES = ((MOTION, _("motion detection")), (TIMELAPSE, _("timelapse")))

    EXPORT_MAP = {
        MOTION: "motionDetection",
        TIMELAPSE: "timeLapse",
    }
