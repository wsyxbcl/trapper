# -*- coding: utf-8 -*-
"""
Simple admin interface to control changes in models.
"""
from django.contrib.gis import admin
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from trapper.apps.geomap.models import Location, Deployment


class LocationAdmin(admin.ModelAdmin):
    list_display = (
        "location_id",
        "name",
        "coordinates",
        "timezone",
        "ignore_DST",
        "city",
        "county",
        "state",
        "country",
        "is_public",
        "date_created",
        "owner",
        "link",
    )
    search_fields = ("location_id", "name", "description", "owner__username")
    list_filter = ("research_project", "is_public", "date_created", "owner")
    raw_id_fields = ("owner", "managers")

    def link(self, item):
        url = "{url}?locations={pk}".format(url=reverse("geomap:map_view"), pk=item.pk)
        return mark_safe(u'<a href="{url}" target="_blank">Link</a>'.format(url=url))

    link.short_description = _("Show on map")


class DeploymentAdmin(admin.ModelAdmin):
    list_display = (
        "deployment_id",
        "deployment_code",
        "location",
        "research_project",
        "start_date",
        "end_date",
        "view_quality",
        "correct_setup",
        "correct_tstamp",
    )
    search_fields = ("deployment_id", "owner__username")
    list_filter = (
        "research_project",
        "date_created",
        "view_quality",
        "correct_setup",
        "correct_tstamp",
        "owner",
    )
    raw_id_fields = ("owner", "managers", "location")

    def get_queryset(self, request):
        return (
            super(DeploymentAdmin, self)
            .get_queryset(request)
            .prefetch_related("location", "research_project")
        )


admin.site.register(Location, LocationAdmin)
admin.site.register(Deployment, DeploymentAdmin)
