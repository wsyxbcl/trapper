# -*- coding: utf-8 -*-
import pandas
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset
from taggit.forms import TagField
from timezone_field import TimeZoneFormField
from umap.models import Map

from django import forms
from django.contrib.auth import get_user_model
from django.core.validators import MaxValueValidator, MinValueValidator
from django.forms.widgets import DateTimeInput, HiddenInput
from django.utils import timezone
from django.utils.dateparse import parse_datetime
from django.utils.translation import gettext_lazy as _

import gpxpy

from trapper.apps.geomap.taxonomy import DeploymentCaptureMethod
from trapper.apps.common.fields import OwnerModelMultipleChoiceField
from trapper.apps.common.forms import (
    BaseCrispyForm,
    BaseCrispyModelForm,
    BaseBulkUpdateForm,
)
from trapper.apps.common.tools import parse_pks
from trapper.apps.common.utils.datetime_tools import (
    localize_datetime_dst,
    set_correct_offset_dst,
)
from trapper.apps.geomap.models import Location, Deployment
from trapper.apps.research.models import ResearchProject
from trapper.apps.research.taxonomy import ResearchProjectRoleType
from trapper.middleware import get_current_user

User = get_user_model()


# Location forms


class LocationFilterForm(forms.ModelForm):
    """
    TODO: docstrings
    """

    search = forms.CharField(
        label=_("Search term"),
        required=False,
        widget=forms.TextInput(
            attrs={
                "placeholder": _("What are you looking for?"),
                "onkeypress": "return event.keyCode!=13",
            }
        ),
        help_text=_("You can search by locations & deployments metadata."),
    )
    research_project = forms.ModelChoiceField(queryset=None, required=False)
    locations_map = forms.CharField(widget=forms.HiddenInput(), required=False)
    in_bbox = forms.CharField(widget=forms.HiddenInput(), required=False)
    # radius = forms.CharField(
    #     widget=forms.HiddenInput(),
    #     required=False,
    # )

    class Meta:
        model = Location
        exclude = ["owner", "is_public"]

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.form_id = "filter-locations-form"
        self.helper.error_text_inline = True
        self.helper.form_show_errors = False
        self.helper.help_text_inline = False
        self.helper.form_show_labels = True
        self.helper.layout = Layout(
            Fieldset("Basic filters", "locations_map", "search", "research_project"),
            Fieldset("Spatial filters", "in_bbox"),
        )
        super().__init__(*args, **kwargs)
        user = get_current_user()
        self.fields[
            "research_project"
        ].queryset = ResearchProject.objects.get_accessible(
            user=user, role_levels=ResearchProjectRoleType.EDIT
        )


class LocationImportForm(BaseCrispyForm):
    """
    Upload locations form.
    """

    research_project = forms.ModelChoiceField(
        queryset=None, required=True, label=_("Research project")
    )
    csv_file = forms.FileField(
        required=False,
        label=_("CSV file"),
        help_text=_("CSV has to be separated by comma."),
    )
    gpx_file = forms.FileField(required=False, label=_("GPX file"))
    timezone = TimeZoneFormField(
        initial=timezone.get_current_timezone(),
        label=_("Timezone"),
        help_text=_(
            "This timezone will be set for all imported locations (by default it is your "
            "current timezone)."
        ),
        choices_display="WITH_GMT_OFFSET",
    )
    ignore_DST = forms.BooleanField(
        initial=False,
        label=_("Ignore DST"),
        required=False,
        help_text=_(
            "Whether the camera traps at uploaded locations ignore Daylight Saving Time. The choice will be saved "
            "for all added locations."
        ),
    )
    select2_fields = ("timezone",)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        user = get_current_user()
        self.fields[
            "research_project"
        ].queryset = ResearchProject.objects.get_accessible(
            user=user, role_levels=ResearchProjectRoleType.UPLOAD
        )

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset(
                "", "research_project", "csv_file", "gpx_file", "timezone", "ignore_DST"
            ),
        )

    def clean_gpx_file(self):
        """Validate gpx file that has been uploaded."""
        gpx_file = self.cleaned_data.get("gpx_file")
        if gpx_file:
            try:
                gpx = gpxpy.parse(gpx_file.read().decode())
            except gpxpy.gpx.GPXXMLSyntaxException:
                errors = _(f'The file "{gpx_file.name}" is not a valid GPX file.')
                raise forms.ValidationError(errors)
            else:
                self.cleaned_data["gpx_data"] = gpx
        return gpx_file

    def clean_csv_file(self):
        csv_file = self.cleaned_data.get("csv_file")
        # the table's structure and content is validated later in the view
        # here only make sure that the uploaded csv file can be parsed by pandas
        if csv_file:
            try:
                dtypes_dict = {"location_id": str}
                csv_df = pandas.read_csv(csv_file, sep=",", dtype=dtypes_dict)
            except pandas.errors.ParserError:
                errors = _(f'The file "{csv_file.name}" is not a valid CSV file.')
                raise forms.ValidationError(errors)
            self.cleaned_data["csv_data"] = csv_df
        return csv_file

    def clean(self):
        cleaned_data = self.cleaned_data
        if not self.errors:
            csv_file = cleaned_data.get("csv_file")
            gpx_file = cleaned_data.get("gpx_file")
            if not csv_file and not gpx_file:
                errors = _("You have to provide either csv file or gpx file.")
                raise forms.ValidationError(errors)
            if csv_file and gpx_file:
                errors = _("You can not upload both CSV and GPX file at the same time.")
                raise forms.ValidationError(errors)
        return cleaned_data


class CreateLocationForm(BaseCrispyModelForm):
    """
    Create location form.
    """

    latitude = forms.FloatField(
        max_value=90.0,
        min_value=-90.0,
        validators=[MaxValueValidator(90.0), MinValueValidator(-90.0)],
    )
    longitude = forms.FloatField(
        max_value=180.0,
        min_value=-180.0,
        validators=[MaxValueValidator(180.0), MinValueValidator(-180.0)],
    )

    class Meta:
        model = Location
        fields = (
            "location_id",
            "coordinates",
            "latitude",
            "longitude",
            "timezone",
            "ignore_DST",
            "name",
            "description",
            "research_project",
        )

    def __init__(self, *args, **kwargs):
        # TODO: Unused variable
        request = kwargs.pop("request", None)  # noqa
        super().__init__(*args, **kwargs)
        self.fields[
            "research_project"
        ].queryset = ResearchProject.objects.get_accessible(
            role_levels=ResearchProjectRoleType.UPLOAD
        )
        self.fields["coordinates"].widget = HiddenInput()
        instance = kwargs.get("instance")
        if instance is not None:
            self.fields["latitude"].initial = instance.coordinates.y
            self.fields["longitude"].initial = instance.coordinates.x
        else:
            self.fields["timezone"].initial = timezone.get_current_timezone()

        self.fields["ignore_DST"].help_text = _(
            "Check if the camera at the location ignores Daylight Savings Time"
        )

    def clean(self):
        cleaned_data = super().clean()
        rp = cleaned_data.get("research_project")
        lid = cleaned_data.get("location_id")
        if self.instance.pk is None:
            if (
                rp
                and lid
                and Location.objects.filter(
                    research_project=rp, location_id=lid
                ).exists()
            ):
                raise forms.ValidationError("Location with this id already exists.")
        return cleaned_data


class UpdateMapPermissionsForm(forms.ModelForm):
    """Change permissions form with select2 widget
    for changing permissions on maps"""

    def __init__(self, *args, **kwargs):
        owner = kwargs.pop("owner", None)
        super().__init__(*args, **kwargs)
        self.fields["editors"].help_text = None
        self.fields["editors"].queryset = User.objects.exclude(pk=owner.pk)

    class Meta:
        model = Map
        fields = ("edit_status", "editors", "share_status")


class BulkUpdateLocationForm(BaseBulkUpdateForm):
    """"""

    research_project = forms.ModelChoiceField(queryset=None, required=False)
    is_public = forms.ChoiceField(
        choices=[(False, "False"), (True, "True")], widget=forms.Select()
    )
    ignore_DST = forms.ChoiceField(
        choices=[(False, "False"), (True, "True")], widget=forms.Select()
    )

    select2_fields = ("timezone",)

    class Meta:
        model = Location
        fields = ("research_project", "timezone", "is_public", "ignore_DST")

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset(
                "",
                "research_project",
                "timezone",
                "is_public",
                "ignore_DST",
                "records_pks",
            ),
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["research_project"].help_text = None
        self.fields["timezone"].help_text = None
        self.fields[
            "research_project"
        ].queryset = ResearchProject.objects.get_accessible(
            role_levels=ResearchProjectRoleType.UPLOAD
        )


# Deployment forms


class DeploymentForm(BaseCrispyModelForm):
    """
    Modelform for creating :class:`apps.geomap.models.Deployment` objects
    """

    # use CharField instead of DateTimeField to overpass timezone middleware logic;
    # we want to use timezone specified in the Location object instead of user's
    # default one; also see "__init__" and "save" methods
    start_date = forms.CharField(
        widget=DateTimeInput(format="%Y-%m-%d %H:%M:%S"),
        label=_("Start date"),
    )
    end_date = forms.CharField(
        widget=DateTimeInput(format="%Y-%m-%d %H:%M:%S"),
        label=_("End date"),
    )
    managers = OwnerModelMultipleChoiceField(
        queryset=User.objects.all(), required=False, label=_("Managers")
    )
    location = forms.ModelChoiceField(queryset=None, label=_("Location"))
    tags = TagField(
        required=False, label=_("Tags"), help_text="Comma or space delimited tags."
    )

    camera_interval = forms.IntegerField(
        required=False,
        help_text=_(
            "Time specified between shutter triggers when activity in the sensor will "
            "not trigger the shutter. Expressed in seconds."
        ),
    )
    camera_height = forms.DecimalField(
        min_value=0,
        help_text=_("Height at which the camera was deployed. Expressed in meters."),
        required=False,
    )
    camera_tilt = forms.IntegerField(
        required=False,
        help_text=_(
            "Angle at which the camera was deployed in the vertical plane. Expressed in "
            "degrees, with -90 facing down, 0 horizontal and 90 facing up."
        ),
    )
    camera_heading = forms.IntegerField(
        required=False,
        help_text=_(
            "Angle at which the camera was deployed in the horizontal plane. Expressed in "
            "decimal degrees clockwise from north, with values ranging from 0 to 360: "
            "0 = north, 90 = east, 180 = south, 270 = west."
        ),
    )
    session = forms.CharField(
        required=False,
        help_text=_(
            "Temporal deployment group. Common sessions are seasons (wet and dry), months, "
            "years or other logical groupings when sampling occurred. For groupings without "
            "context, use `tags`."
        ),
    )
    array = forms.CharField(
        required=False,
        help_text=_(
            "Spatial deployment group. Common arrays are grids, arrays, clusters or other "
            "logical groupings where sampling occurred. For groupings without context, use `tags`."
        ),
    )
    habitat = forms.CharField(
        required=False, help_text=_("Short characterization of the habitat.")
    )
    capture_method = forms.ChoiceField(
        choices=DeploymentCaptureMethod.CHOICES, required=False, initial=1
    )

    select2_ajax_fields = (("location", "/geomap/api/locations", "pk", "location_id"),)

    class Meta:
        model = Deployment
        exclude = ["owner"]

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset(
                "",
                "deployment_code",
                "location",
                # HTML(
                #     '{% include "geomap/deployment_form_map.html" %}'
                # ),
                "research_project",
                "start_date",
                "end_date",
                "correct_setup",
                "correct_tstamp",
                "view_quality",
                "tags",
                "comments",
                "managers",
                "setup_by",
                "camera_id",
                "camera_model",
                "camera_interval",
                "camera_height",
                "camera_tilt",
                "camera_heading",
                "session",
                "array",
                "feature_type",
                "habitat",
                "capture_method",
            ),
        )

    def save(self):
        deployment = super().save(commit=False)

        # localize datetime fields using a proper timezone and DST flag
        deployment.start_date = localize_datetime_dst(
            parse_datetime(self.cleaned_data["start_date"]),
            deployment.timezone,
            deployment.ignore_DST,
        )
        deployment.end_date = localize_datetime_dst(
            parse_datetime(self.cleaned_data["end_date"]),
            deployment.timezone,
            deployment.ignore_DST,
        )

        deployment.save()
        deployment.managers.set(self.cleaned_data["managers"])
        deployment.tags.clear()
        for tag in self.cleaned_data["tags"]:
            deployment.tags.add(tag)
        return deployment

    def __init__(self, *args, **kwargs):
        """Customize each instance of a form by setting a queryset for
        a location if it is available in a form.
        """
        super().__init__(*args, **kwargs)

        for fieldname in ["research_project", "managers"]:
            self.fields[fieldname].help_text = None

        if "location" in self.fields:
            self.fields["location"].queryset = Location.objects.get_available(
                editable_only=True
            )
            self.fields["location"].widget = forms.CharField.widget()

        if "research_project" in self.fields:
            self.fields[
                "research_project"
            ].queryset = ResearchProject.objects.get_accessible(
                role_levels=ResearchProjectRoleType.UPLOAD
            )

        # set initial values for datetime fields using a proper timezone and DST flag
        if self.instance.pk:
            self.initial["start_date"] = set_correct_offset_dst(
                self.instance.start_date,
                self.instance.timezone,
                self.instance.ignore_DST,
            )
            self.initial["end_date"] = set_correct_offset_dst(
                self.instance.end_date,
                self.instance.timezone,
                self.instance.ignore_DST,
            )


class SimpleDeploymentForm(DeploymentForm):
    """Simple version of :class:`DeploymentForm` that is used for
    dynamic edit form"""

    class Meta:
        model = Deployment
        exclude = ["owner", "comments", ""]

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset(
                "",
                "deployment_code",
                "location",
                "research_project",
                "start_date",
                "end_date",
                "correct_setup",
                "correct_tstamp",
                "view_quality",
                "tags",
                "managers",
                "setup_by",
                "camera_id",
                "camera_model",
                "camera_interval",
                "camera_height",
                "camera_tilt",
                "camera_heading",
                "session",
                "array",
                "feature_type",
                "habitat",
            ),
        )


class BulkUpdateDeploymentForm(BaseBulkUpdateForm):
    """"""

    TRUE_FALSE_CHOICES = ((True, _("Yes")), (None, _("No")))

    correct_setup = forms.ChoiceField(
        choices=TRUE_FALSE_CHOICES,
        label=_("Correct setup"),
        required=False,
        initial=_("Yes"),
    )
    correct_tstamp = forms.ChoiceField(
        choices=TRUE_FALSE_CHOICES,
        label=_("Correct timestamp"),
        required=False,
        initial=_("Yes"),
    )
    tags2add = TagField(
        required=False,
        label=_("Tags to add"),
        help_text="Comma or space delimited tags.",
    )
    tags2remove = TagField(
        required=False,
        label=_("Tags to remove"),
        help_text="Comma or space delimited tags.",
    )
    research_project = forms.ModelChoiceField(queryset=None, required=False)

    class Meta:
        model = Deployment
        fields = (
            "deployment_code",
            "research_project",
            "view_quality",
            "correct_setup",
            "correct_tstamp",
            "managers",
            "camera_id",
            "camera_model",
            "camera_interval",
            "camera_height",
            "camera_tilt",
            "camera_heading",
            "session",
            "array",
            "feature_type",
            "habitat",
        )

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset(
                "",
                "deployment_code",
                "research_project",
                "view_quality",
                "correct_setup",
                "correct_tstamp",
                "tags2add",
                "tags2remove",
                "managers",
                "camera_id",
                "camera_model",
                "camera_interval",
                "camera_height",
                "camera_tilt",
                "camera_heading",
                "session",
                "array",
                "feature_type",
                "habitat",
                "records_pks",
            ),
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["managers"].help_text = ""
        self.fields["research_project"].help_text = None
        self.fields[
            "research_project"
        ].queryset = ResearchProject.objects.get_accessible(
            role_levels=ResearchProjectRoleType.UPLOAD
        )


class BulkCreateDeploymentForm(DeploymentForm):
    """"""

    locations_pks = forms.CharField(widget=forms.HiddenInput())

    class Meta:
        model = Deployment
        exclude = ["owner", "location", "feature_type"]

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset(
                "",
                "deployment_code",
                "research_project",
                "start_date",
                "end_date",
                "managers",
                "locations_pks",
            ),
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if "research_project" in self.fields:
            self.fields[
                "research_project"
            ].queryset = ResearchProject.objects.get_accessible(
                role_levels=ResearchProjectRoleType.UPLOAD
            )
        self.fields["managers"].help_text = None
        self.fields["location"].required = False

    def clean_locations_pks(self):
        locations_pks = self.cleaned_data.pop("locations_pks", None)
        if locations_pks:
            pks_list = parse_pks(locations_pks)
            locations = Location.objects.get_available(editable_only=True).filter(
                pk__in=pks_list
            )
            if locations:
                self.cleaned_data["locations"] = locations


class DeploymentImportForm(BaseCrispyForm):
    """
    TODO: docstrings
    """

    csv_file = forms.FileField(
        label=_("CSV file"),
        required=True,
        help_text=_("CSV has to be separated by comma."),
    )
    research_project = forms.ModelChoiceField(
        queryset=None, label=_("Research project")
    )
    timezone = TimeZoneFormField(
        help_text=_(
            'Provide a timezone to correctly process deployments "start" and "end" timestamps.'
        ),
        label=_("Timezone"),
        choices_display="WITH_GMT_OFFSET",
    )
    update = forms.BooleanField(
        initial=False,
        required=False,
        label=_("Update deployments"),
        help_text=_(
            "To update the existing deployments your CSV table has to contain the column "
            '"_id" with the primary keys of the existing database objects.'
        ),
    )
    create_locations = forms.BooleanField(
        initial=False,
        required=False,
        label=_("Create locations"),
        help_text=_(
            "To automatically create locations your CSV has to contain the following "
            'columns: "locationID", "longitude", "latitude".'
        ),
    )
    ignore_DST = forms.BooleanField(
        initial=False,
        label=_("Locations ignore DST"),
        required=False,
        help_text=_(
            "Whether the camera traps at uploaded locations ignore Daylight Saving Time. If locations are created now, "
            "choice will be saved for all added locations. If locations already exist in the database, their timezone "
            "information has to match data provided in the form."
        ),
    )

    select2_fields = ("timezone",)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        user = get_current_user()
        self.fields[
            "research_project"
        ].queryset = ResearchProject.objects.get_accessible(
            user=user, role_levels=ResearchProjectRoleType.UPLOAD
        )
        self.fields["timezone"].initial = timezone.get_current_timezone()

    def get_layout(self):
        return Layout(
            Fieldset(
                "",
                "research_project",
                "csv_file",
                "timezone",
                "update",
                "create_locations",
                "ignore_DST",
            ),
        )

    def clean_csv_file(self):
        csv_file = self.cleaned_data.get("csv_file")
        # the table's structure and content is validated later in the view
        # here only make sure that the uploaded csv file can be parsed by pandas
        if csv_file:
            try:
                csv_df = pandas.read_csv(csv_file, sep=",")
            except pandas.errors.ParserError:
                errors = _(f'The file "{csv_file.name}" is not a valid CSV file.')
                raise forms.ValidationError(errors)
            self.cleaned_data["csv_data"] = csv_df
        return csv_file
