from django.urls import reverse_lazy, re_path
from django.views.generic import RedirectView
from trapper.apps.messaging import views as messaging_views

app_name = "messaging"

urlpatterns = [
    re_path(
        r"^$",
        RedirectView.as_view(url=reverse_lazy("messaging:message_inbox")),
        name="index",
    ),
    re_path(
        r"message/detail/(?P<hashcode>\w+)/$",
        messaging_views.view_message_detail,
        name="message_detail",
    ),
    re_path(
        r"message/inbox/$", messaging_views.view_message_inbox, name="message_inbox"
    ),
    re_path(
        r"message/outbox/$", messaging_views.view_message_outbox, name="message_outbox"
    ),
    re_path(
        r"message/create/$", messaging_views.view_message_create, name="message_create"
    ),
    re_path(
        r"collection-request/list/$",
        messaging_views.view_collection_request_list,
        name="collection_request_list",
    ),
    re_path(
        r"collection-request/resolve/(?P<pk>\d+)/$",
        messaging_views.view_collection_request_resolve,
        name="collection_request_resolve",
    ),
    re_path(
        r"collection-request/revoke/(?P<pk>\d+)/$",
        messaging_views.view_collection_request_revoke,
        name="collection_request_revoke",
    ),
    re_path(
        r"collection-request/delete/(?P<pk>\d+)/$",
        messaging_views.view_collection_request_delete,
        name="collection_request_delete",
    ),
]
