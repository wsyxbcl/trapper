from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class VariablesConfig(AppConfig):
    name = "trapper.apps.variables"
    verbose_name = _("Variables")
