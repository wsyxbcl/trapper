from django.test import TestCase
from trapper.apps.dashboard.models import DashboardButton


class DashboardTests(TestCase):
    def test_dashboard_save(self):
        DashboardButton(
            name="dashboard_name",
            href="href",
        ).save()

        dashboard_button = DashboardButton.objects.all().first()
        dashboard_button.name = "dashboard_name_2"
        dashboard_button.save()

        DashboardButton(
            name="dashboard_name", href="href", url="https://url.com"
        ).save()
