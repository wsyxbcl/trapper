# -*- coding: utf-8 -*-

from django.urls import re_path

from trapper.apps.sendfile import views as sendfile_views

app_name = "sendfile"

urlpatterns = [
    re_path(
        r"^direct/$",
        sendfile_views.view_direct_serve_file,
        name="sendfile_direct_serve",
    ),
]
