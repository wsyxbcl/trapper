# -*- utf-8 -*-
import os
import re
from urllib.request import urlopen
from urllib.error import URLError, HTTPError

from django.conf import settings
from django.http import FileResponse


class SendFileDevServerMiddleware:
    """
    Middleware for serving files requested with SendFileResponse.
    Mimics real web server behavior so it should be included at the end
    of MIDDLEWARE.
    WARNING: *Don't even try to use it in production!*
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        if settings.SENDFILE_DEV_SERVER_ENABLED:
            path = response.get(settings.SENDFILE_HEADER)
            if path:
                content_type = response.get("Content-Type")
                content_disposition = response.get("Content-Disposition")
                # serve media files from a remote or local storage

                # Use the same X-Accel-Redirect parsing method for remote storage as used in trapper-nginx
                # If matched, try to download directly from signed remote storage url
                remote_download_match = re.match(
                    "/remote_download/(.*?)/(.*?)/(.*)", path
                )
                if remote_download_match:
                    protocol = remote_download_match.group(1)
                    host = remote_download_match.group(2)
                    path = remote_download_match.group(3)
                    full_url = f"{protocol}://{host}/{path}"

                    try:
                        _file = urlopen(full_url)
                    except (ValueError, URLError, HTTPError):
                        _file = open(
                            os.path.join(
                                settings.STATIC_ROOT,
                                settings.RESOURCE_FORBIDDEN_THUMBNAIL,
                            ),
                            "rb",
                        )
                else:
                    try:
                        path = os.path.dirname(settings.PROJECT_ROOT) + path
                        _file = open(path, "rb")
                    except IOError:
                        _file = open(
                            os.path.join(
                                settings.STATIC_ROOT,
                                settings.RESOURCE_FORBIDDEN_THUMBNAIL,
                            ),
                            "rb",
                        )
                response = FileResponse(_file, content_type=content_type)
                if content_disposition:
                    response["Content-Disposition"] = content_disposition
        return response
