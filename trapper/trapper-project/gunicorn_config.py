# Gunicorn configuration file
# Gunicorn should be now started like: `gunicorn -c gunicorn_config.py trapper.wsgi`


import multiprocessing

# Set gunicorn variables
workers = 2 * multiprocessing.cpu_count() + 1
bind = "0.0.0.0:8000"
loglevel = "info"
accesslog = "logs/gunicorn_access"
errorlog = "logs/gunicorn_errors"
max_requests = 1000
max_requests_jitter = 100
timeout = 600
keepalive = 5
user = "trapper"
group = "trapper"
