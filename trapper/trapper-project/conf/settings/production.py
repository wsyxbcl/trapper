from .base import *

DEBUG = False

# https://docs.djangoproject.com/en/1.11/ref/settings/#secure-proxy-ssl-header
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

USE_SENTRY = str2bool(get_env_value("USE_SENTRY"))
if USE_SENTRY:
    SENTRY_DSN_CONFIG_KEY = get_env_value("SENTRY_DNS_CONFIG_KEY")

SECRET_KEY = get_env_value("SECRET_KEY")

DOMAIN_NAME = get_env_value("DOMAIN_NAME")

EMAIL_NOTIFICATIONS = str2bool(get_env_value("EMAIL_NOTIFICATIONS"))
EMAIL_NOTIFICATIONS_RESEARCH_PROJECT = str2bool(
    get_env_value("EMAIL_NOTIFICATIONS_RESEARCH_PROJECT")
)

if EMAIL_NOTIFICATIONS:
    EMAIL_HOST = get_env_value("EMAIL_HOST")
    EMAIL_PORT = get_env_value("EMAIL_PORT")
    EMAIL_HOST_USER = get_env_value("EMAIL_HOST_USER")
    EMAIL_HOST_PASSWORD = get_env_value("EMAIL_HOST_PASSWORD")
    EMAIL_USE_TLS = str2bool(get_env_value("EMAIL_USE_TLS"))
    DEFAULT_FROM_EMAIL = SERVER_EMAIL = EMAIL_HOST_USER

MAX_UPLOAD_SIZE = int(get_env_value("MAX_UPLOAD_SIZE"))
MAX_UPLOAD_COLLECTION_SIZE = int(get_env_value("MAX_UPLOAD_COLLECTION_SIZE"))
DATA_PACKAGE_MAX_SIZE = int(get_env_value("DATA_PACKAGE_MAX_SIZE"))

USE_RECAPTCHA = str2bool(get_env_value("USE_RECAPTCHA"))
if USE_RECAPTCHA:
    RECAPTCHA_PUBLIC_KEY = get_env_value("RECAPTCHA_PUBLIC_KEY")
    RECAPTCHA_PRIVATE_KEY = get_env_value("RECAPTCHA_PRIVATE_KEY")
    ACCOUNT_SIGNUP_FORM_CLASS = "trapper.apps.accounts.forms.TrapperSignupForm"


CELERY_COLLECTION_UPLOAD_HOURS = int(os.getenv("CELERY_COLLECTION_UPLOAD_HOURS") or 2)
CELERY_UPDATE_THUMBNAILS_HOURS = int(os.getenv("CELERY_UPDATE_THUMBNAILS_HOURS") or 6)

CELERY_ANNOTATIONS = {
    "trapper.apps.storage.tasks.celery_process_collection_upload": {
        "time_limit": 60 * 60 * CELERY_COLLECTION_UPLOAD_HOURS
    },  # 2h
    "trapper.apps.storage.tasks.celery_update_thumbnails": {
        "time_limit": 60 * 60 * CELERY_UPDATE_THUMBNAILS_HOURS
    },  # 6h
}
