"""
Those settings are not required to run tests. They are defined to make little
better look & feel when you want to run coverage.

Also you can define own tablespace which with postgres will be used
to alter postgres partition (to place it i.e. in ramdisk)

To use those settings you need to install at least:

* django-coverage
* coverage

"""
import os.path

from .base import *

COVERAGE_MODULE_EXCLUDES = [
    "tests$",
    "settings$",
    "urls$",
    "locale$",
    "common.views.test",
    "__init__",
    "django",
    "migrations",
    "^(?!trapper).*$",
    "static",
    "templates",
    "fixtures",
    "schema",
]

BROKER_URL = "memory://"

MEDIA_ROOT = os.path.join(MEDIA_ROOT, "test_media")
