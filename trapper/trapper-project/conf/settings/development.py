from .base import *

DEBUG = True
USE_SENTRY = False

SECRET_KEY = get_env_value("SECRET_KEY")
DOMAIN_NAME = get_env_value("DOMAIN_NAME")

EMAIL_NOTIFICATIONS = str2bool(get_env_value("EMAIL_NOTIFICATIONS"))
EMAIL_NOTIFICATIONS_RESEARCH_PROJECT = str2bool(
    get_env_value("EMAIL_NOTIFICATIONS_RESEARCH_PROJECT")
)

if EMAIL_NOTIFICATIONS:
    EMAIL_HOST = get_env_value("EMAIL_HOST")
    EMAIL_PORT = get_env_value("EMAIL_PORT")
    EMAIL_HOST_USER = get_env_value("EMAIL_HOST_USER")
    EMAIL_HOST_PASSWORD = get_env_value("EMAIL_HOST_PASSWORD")
    EMAIL_USE_TLS = str2bool(get_env_value("EMAIL_USE_TLS"))
    DEFAULT_FROM_EMAIL = SERVER_EMAIL = EMAIL_HOST_USER

MAX_UPLOAD_SIZE = int(get_env_value("MAX_UPLOAD_SIZE"))
MAX_UPLOAD_COLLECTION_SIZE = int(get_env_value("MAX_UPLOAD_COLLECTION_SIZE"))
DATA_PACKAGE_MAX_SIZE = int(get_env_value("DATA_PACKAGE_MAX_SIZE"))

USE_RECAPTCHA = str2bool(get_env_value("USE_RECAPTCHA"))
if USE_RECAPTCHA:
    RECAPTCHA_PUBLIC_KEY = get_env_value("RECAPTCHA_PUBLIC_KEY")
    RECAPTCHA_PRIVATE_KEY = get_env_value("RECAPTCHA_PRIVATE_KEY")
    ACCOUNT_SIGNUP_FORM_CLASS = "trapper.apps.accounts.forms.TrapperSignupForm"

CELERY_COLLECTION_UPLOAD_HOURS = int(os.getenv("CELERY_COLLECTION_UPLOAD_HOURS") or 2)
CELERY_UPDATE_THUMBNAILS_HOURS = int(os.getenv("CELERY_UPDATE_THUMBNAILS_HOURS") or 2)

CELERY_ANNOTATIONS = {
    "trapper.apps.storage.tasks.celery_process_collection_upload": {
        "time_limit": 60 * 60 * CELERY_COLLECTION_UPLOAD_HOURS
    },  # 2h
    "trapper.apps.storage.tasks.celery_update_thumbnails": {
        "time_limit": 60 * 60 * CELERY_UPDATE_THUMBNAILS_HOURS
    },  # 2h
}

SENDFILE_DEV_SERVER_ENABLED = str2bool(
    os.environ.get("SENDFILE_DEV_SERVER_ENABLED", "true")
)

MIDDLEWARE = MIDDLEWARE + [
    "trapper.apps.sendfile.middleware.SendFileDevServerMiddleware",
]

REST_FRAMEWORK["DEFAULT_RENDERER_CLASSES"] = ("rest_framework.renderers.JSONRenderer",)

REQUESTS_VERIFY_SSL = False
