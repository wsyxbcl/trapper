# Django settings for trapper project.
import os
import sys
import multiprocessing

from django.contrib.messages import constants as messages
from django.core.exceptions import ImproperlyConfigured
from django_auth_ldap.config import LDAPSearch
from django.utils.translation import gettext_lazy as _

# Sentry SDK integration (sentry.io)
import ldap
import sentry_sdk
from sentry_sdk.integrations.celery import CeleryIntegration
from sentry_sdk.integrations.django import DjangoIntegration


def str2bool(v):
    if not v:
        return False
    return v.lower() in ("true",)


def get_env_value(env_variable):
    try:
        return os.environ[env_variable]
    except KeyError:
        error_msg = "Set the {} environment variable".format(env_variable)
        raise ImproperlyConfigured(error_msg)


# System-level user ids
TRAPPER_USER_UID = get_env_value("TRAPPER_USER_UID")
TRAPPER_USER_GID = get_env_value("TRAPPER_USER_GID")

DEBUG = True

USE_SENTRY = False
SENTRY_DSN_CONFIG_KEY = ""

ADMINS = tuple(
    [
        k
        for k in [
            get_env_value("ADMIN1").split(","),
            get_env_value("ADMIN2").split(","),
        ]
        if k[0]
    ]
)
MANAGERS = tuple(
    [
        k
        for k in [
            get_env_value("MANAGER1").split(","),
            get_env_value("MANAGER2").split(","),
        ]
        if k[0]
    ]
)
PROJECT_ROOT = os.path.abspath(
    os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
)

# Uncomment this line if your postgis is 2.1 or newer
# (input the correct version in that case)
POSTGIS_VERSION = (2, 1, 3)

if "DB_NAME" in os.environ:
    DATABASES = {
        "default": {
            "ENGINE": "django.contrib.gis.db.backends.postgis",
            "NAME": os.environ["DB_NAME"],
            "USER": os.environ["DB_USER"],
            "PASSWORD": os.environ["DB_PASS"],
            "HOST": os.environ["DB_HOST"],
            "PORT": os.environ["DB_PORT"],
        }
    }
else:
    DATABASES = {
        "default": {
            "ENGINE": "django.contrib.gis.db.backends.postgis",
            "NAME": "trapper",
            "USER": "trapper",
            "PASSWORD": "trapper",
            "HOST": "postgres",
            "PORT": "",
        }
    }

# you have to configure the email backend to use notifications
EMAIL_NOTIFICATIONS = False
EMAIL_NOTIFICATIONS_RESEARCH_PROJECT = False
EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"

# EMAIL_USE_TLS = True
# EMAIL_HOST = 'smtp.gmail.com'
# EMAIL_HOST_USER = 'xxx@gmail.com'
# EMAIL_HOST_PASSWORD = ''
# EMAIL_PORT = 587

DOMAIN_NAME = "localhost"

DATE_INPUT_FORMATS = ("%d-%m-%Y", "%Y-%m-%d", "%d.%m.%Y", "%Y.%m.%d")
DATETIME_INPUT_FORMATS = (
    "%d-%m-%Y %H:%M:%S",
    "%Y-%m-%d %H:%M:%S",
    "%d.%m.%Y %H:%M:%S",
    "%Y.%m.%d %H:%M:%S",
)
TIME_INPUT_FORMATS = ("%H:%M:%S", "%H:%M")

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ["*"]

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = "Europe/Warsaw"

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = "en-us"

LANGUAGES = [
    ("en", _("English")),
    ("pl", _("Polish")),
    # add more languages here
]

LANGUAGE_COOKIE_NAME = "trapper_language"

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Changed in Django 4.0: zoneinfo was made the default timezone implementation. You may continue to use pytz during
# the 4.x release cycle via the USE_DEPRECATED_PYTZ setting.
USE_DEPRECATED_PYTZ = True

# Use local storage by default
REMOTE_STORAGE = False

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = os.path.join(os.path.dirname(PROJECT_ROOT), "media")
EXTERNAL_MEDIA_ROOT = os.path.join(os.path.dirname(PROJECT_ROOT), "external_media")

# File path to i18 files
LOCALE_PATHS = [os.path.join(os.path.dirname(PROJECT_ROOT), "locale")]

# Configure upload limits
UPLOAD_LIMIT = True
MAX_UPLOAD_SIZE = 10 * 1024 * 1024
MAX_UPLOAD_COLLECTION_SIZE = 500 * 1024 * 1024
MAX_UPLOAD_YAML_SIZE = 5 * 1024 * 1024

# max size of a raw data package
DATA_PACKAGE_MAX_SIZE = 10 * 1024 * 1024

# Allowed file types
ALLOWED_FILE_TYPES = (
    "aac",
    "ace",
    "ai",
    "aiff",
    "avi",
    "bmp",
    "fla",
    "flv",
    "jpg",
    "jpeg",
    "mov",
    "mp3",
    "mp4",
    "mpc",
    "mkv",
    "mpg",
    "mpeg",
    "ogg",
    "odg",
    "ogv",
    "pdf",
    "png",
    "swf",
    "tif",
    "tiff",
    "wav",
    "webm",
    "wma",
    "wmv",
    "zip",
    "x-zip-compressed",
    "csv",
    "txt",
    "gpx",
    "xml",
    "yaml",
    "x-yaml",
    # other possible content types for uploaded csv files
    "force-download",
    "vnd.ms-excel",
    "octet-stream",
)

CELERY_DATA_ROOT = os.path.join(PROJECT_ROOT, "celery_data")
CELERY_ACCEPT_CONTENT = ["pickle", "json", "msgpack", "yaml"]

# Setting used to determine if task should be run as celery task or regular one
CELERY_ENABLED = True
# Min image size for resource to be processed by celery.
# If size is lower - image will be processed without celery
CELERY_MIN_IMAGE_SIZE = 10 * 1024 * 1024

if "BROKER_URL" in os.environ:
    BROKER_URL = os.environ["BROKER_URL"]
else:
    BROKER_URL = "amqp://guest:guest@rabbitmq:5672//"

CELERY_RESULT_BACKEND = "django-db"
CELERY_TRACK_STARTED = True
CELERY_SEND_EVENTS = True

BROKER_POOL_LIMIT = int(os.environ.get("BROKER_POOL_LIMIT") or 10)
CELERYD_CONCURRENCY = int(
    os.environ.get("CELERYD_CONCURRENCY") or (2 * multiprocessing.cpu_count() + 1)
)
CELERY_DISABLE_RATE_LIMITS = True
CELERYD_MAX_TASKS_PER_CHILD = 20
CELERYD_SOFT_TASK_TIME_LIMIT = 50 * 60  # 50min
CELERYD_TASK_TIME_LIMIT = 60 * 60  # 1h

BULK_BATCH_SIZE = int(os.environ.get("BULK_BATCH_SIZE") or 5000)
HEAVY_BULK_UPDATE_BATCH_SIZE = int(
    os.environ.get("HEAVY_BULK_UPDATE_BATCH_SIZE") or 1000
)

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = "/media/"
EXTERNAL_MEDIA_URL = "/external_media/"

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = os.path.join(os.path.dirname(PROJECT_ROOT), "static")

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = "/static/"

LOGIN_URL = "/account/login/"

AUTHENTICATION_BACKENDS = (
    "django.contrib.auth.backends.ModelBackend",
    "allauth.account.auth_backends.AuthenticationBackend",
)

if str2bool(os.environ.get("LDAP_AUTHENTICATION")):
    AUTHENTICATION_BACKENDS = (
        "django.contrib.auth.backends.ModelBackend",
        "allauth.account.auth_backends.AuthenticationBackend",
        "django_auth_ldap.backend.LDAPBackend",
    )

    AUTH_LDAP_SERVER_URI = os.environ.get("LDAP_SERVER_URI")
    AUTH_LDAP_BIND_DN = os.environ.get("LDAP_BIND_DN")
    AUTH_LDAP_BIND_PASSWORD = os.environ.get("LDAP_BIND_PASSWORD")
    AUTH_LDAP_USER_SEARCH = LDAPSearch(
        os.environ.get("LDAP_USER_SEARCH_HOST"),
        ldap.SCOPE_SUBTREE,
        "({}=%(user)s)".format(os.environ.get("LDAP_USER_SEARCH_AD")),
    )

    AUTH_LDAP_USER_ATTR_MAP = {
        "first_name": "givenName",
        "last_name": "sn",
        "email": "mail",
    }
    # Use LDAP group membership to calculate group permissions.
    AUTH_LDAP_FIND_GROUP_PERMS = True

    # Cache distinguised names and group memberships for an hour to minimize
    # LDAP traffic.
    AUTH_LDAP_CACHE_TIMEOUT = 3600

AUTH_USER_MODEL = "accounts.User"
AUTH_PROFILE_MODULE = "accounts.UserProfile"

PASSWORD_HASHERS = [
    "trapper.apps.common.utils.hashers.CryptSHA512PasswordHasher",
    "django.contrib.auth.hashers.PBKDF2PasswordHasher",
    "django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher",
    "django.contrib.auth.hashers.BCryptSHA256PasswordHasher",
    "django.contrib.auth.hashers.BCryptPasswordHasher",
    "django.contrib.auth.hashers.SHA1PasswordHasher",
    "django.contrib.auth.hashers.MD5PasswordHasher",
    "django.contrib.auth.hashers.UnsaltedSHA1PasswordHasher",
    "django.contrib.auth.hashers.UnsaltedMD5PasswordHasher",
    "django.contrib.auth.hashers.CryptPasswordHasher",
]

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    "django.contrib.staticfiles.finders.FileSystemFinder",
)

# Make this unique, and don't share it with anybody.
# This should be kept in your settings_local.py
SECRET_KEY = "YOUR_SECRET_KEY_HERE"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            # 'theme_templates' directory is not commited to git; this is a simple
            # way to customize your Trapper instance
            os.path.join(PROJECT_ROOT, "trapper", "theme_templates"),
            os.path.join(PROJECT_ROOT, "trapper", "templates"),
        ],
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
            "builtins": ["trapper.apps.accounts.templatetags.accounts_tags"],
        },
        "APP_DIRS": True,
    },
]

MIDDLEWARE = [
    # ThreadLocals middleware should be first since it stores data in
    # local thread and since it's first - data cleared in process_response and
    # process_exception will be available for all other middlewares
    "trapper.middleware.ThreadLocals",
    "django.middleware.common.CommonMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "trapper.middleware.TimezoneMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "debug_toolbar.middleware.DebugToolbarMiddleware",
]

ROOT_URLCONF = "trapper.urls"

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = "trapper.wsgi.application"

DEFAULT_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.postgres",
    "django.contrib.staticfiles",
    "django.contrib.gis",
    "django_filters",
    "formtools",
    "grappelli",
    "django.contrib.admin",
    "django_extensions",
    "crispy_forms",
    "rest_framework",
    "rest_framework_gis",
    "rest_framework.authtoken",
    "drf_yasg",  # swagger
    "allauth",
    "allauth.account",
    "mptt",
    "taggit",
    "django_celery_results",
    "django_celery_beat",
    "timezone_field",
    "umap",
    "debug_toolbar",
    "captcha",
    "polymorphic",
]

LOCAL_APPS = [
    "trapper.apps.accounts.apps.AccountsConfig",
    "trapper.apps.comments.apps.CommentsConfig",
    "trapper.apps.dashboard.apps.DashboardConfig",
    "trapper.apps.variables.apps.VariablesConfig",
    "trapper.apps.extra_tables.apps.ExtraTablesConfig",
    "trapper.apps.storage.apps.StorageConfig",
    "trapper.apps.research.apps.ResearchConfig",
    "trapper.apps.geomap.apps.GeomapConfig",
    "trapper.apps.media_classification.apps.MediaClassificationConfig",
    "trapper.apps.messaging.apps.MessagingConfig",
    "trapper.apps.common.apps.CommonConfig",
    "trapper.apps.sendfile.apps.SendfileConfig",
    "trapper.apps.jupyterlite.apps.JupyterliteConfig",
]

INSTALLED_APPS = DEFAULT_APPS + LOCAL_APPS

MESSAGE_TAGS = {
    messages.ERROR: "danger",
    messages.DEBUG: "info",
}

SESSION_SERIALIZER = "django.contrib.sessions.serializers.JSONSerializer"

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.

LOG_DIR = os.path.join(PROJECT_ROOT, "logs")

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "standard": {
            "format": ("[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s"),
            "datefmt": "%d/%b/%Y %H:%M:%S",
        },
    },
    "filters": {"require_debug_false": {"()": "django.utils.log.RequireDebugFalse"}},
    "handlers": {
        "mail_admins": {
            "level": "ERROR",
            "filters": ["require_debug_false"],
            "class": "django.utils.log.AdminEmailHandler",
        },
        "logfile": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": os.path.join(LOG_DIR, "logfile"),
            "maxBytes": 50000,
            "backupCount": 2,
            "formatter": "standard",
        },
        "log_to_stdout": {
            "level": "INFO",
            "class": "logging.StreamHandler",
            "stream": sys.stdout,
        },
    },
    "loggers": {
        "django.request": {
            "handlers": ["mail_admins", "logfile"],
            "level": "ERROR",
            "propagate": True,
        },
        "main_stdout": {
            "handlers": ["log_to_stdout"],
            "level": "INFO",
            "propagate": True,
        },
    },
}

# CRISPY FORMS
CRISPY_TEMPLATE_PACK = "bootstrap3"
CRISPY_FAIL_SILENTLY = not DEBUG
CRISPY_CLASS_CONVERTERS = {
    # 'select': "select2-default",
    "selectmultiple": "select2-default",
    "textarea": "textarea-wysiswyg",
    "datetimeinput": "datetimepicker-control",
    "dateinput": "datepicker-control",
    "tagwidget": "select2-tags",
}

# Time (in seconds) how often request for resource/collection can be sent
REQUEST_FLOOD_DELAY = 86400

# Django REST Framework settings:
REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "rest_framework.authentication.SessionAuthentication",
        "rest_framework.authentication.BasicAuthentication",
        "rest_framework.authentication.TokenAuthentication",
    ),
    "DEFAULT_PERMISSION_CLASSES": ("rest_framework.permissions.IsAuthenticated",),
    "PAGINATE_BY": None,
    "PAGINATE_BY_PARAM": "page_size",
    "MAX_PAGINATE_BY": 999,
    "DEFAULT_FILTER_BACKENDS": (
        "django_filters.rest_framework.DjangoFilterBackend",
        "rest_framework.filters.SearchFilter",
    ),
    "DEFAULT_RENDERER_CLASSES": ("rest_framework.renderers.JSONRenderer",),
    "DEFAULT_SCHEMA_CLASS": "rest_framework.schemas.coreapi.AutoSchema",
}

USE_RECAPTCHA = False
# update recaptcha keys in settings_local.py
RECAPTCHA_PUBLIC_KEY = ""
RECAPTCHA_PRIVATE_KEY = ""
RECAPTCHA_USE_SSL = True
NOCAPTCHA = True

# Django allauth settings
ACCOUNT_ADAPTER = "trapper.apps.accounts.adapters.TrapperAdapter"
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_AUTHENTICATION_METHOD = "email"
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_SESSION_REMEMBER = False
ACCOUNT_EMAIL_VERIFICATION = "none"
ACCOUNT_LOGOUT_ON_GET = True
if os.environ.get("USER_REGISTRATION_OPEN"):
    USER_REGISTRATION_OPEN = str2bool(os.environ["USER_REGISTRATION_OPEN"])
else:
    USER_REGISTRATION_OPEN = True

# Dashboard settings
MESSAGES_COUNT = 5

# Variables settings
VARIABLES_DEBUG = DEBUG

# Comments settings
COMMENTS_REDIRECT_URL = "/"

DEFAULT_THUMBNAIL_SIZE = (136, 136)
DEFAULT_PREVIEW_SIZE = (860, 860)
VIDEO_THUMBNAIL_ENABLED = True

CACHE_UNDEFINED = "_UNDEFINED_"
CACHE_TIMEOUT = 43200  # 30 days
CACHE_COUNT_TIMEOUT = 60 * 60  # 1 hour

DASHBOARD_TASKS = 10

# Exclude the number-based attributes (e.g. number of animals)
# from the classification list filters
EXCLUDE_CLASSIFICATION_NUMBERS = True

# Base place for media that should be served through x-sendfile
SENDFILE_ROOT = MEDIA_URL

# Default directory within SENDFILE_ROOT for files served through x-sendfile
SENDFILE_MEDIA_PREFIX = "protected/"

# Used by middleware to fake sendfile on development instances
# DO NOT USE IT ON PRODUCTION
SENDFILE_DEV_SERVER_ENABLED = False

# Header that will be added to response. Differes between web servers.
# This one is for nginx
SENDFILE_HEADER = "X-Accel-Redirect"

RESOURCE_FORBIDDEN_THUMBNAIL = "trapper_storage/img/thumb_forbidden.jpg"

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.memcached.PyLibMCCache",
        "LOCATION": "memcached:11211",
    }
}

# settings for management command delete_orphaned
ORPHANED_APPS_MEDIABASE_DIRS = {
    "storage": {
        "root": os.path.join(MEDIA_ROOT, "protected/storage"),
        "exclude": (".gitignore",),
    },
}

USE_DEBUG_TOOLBAR = str2bool(get_env_value("USE_DEBUG_TOOLBAR"))
SHOW_DEBUG_TOOLBAR_USERS = tuple(get_env_value("DEBUG_TOOLBAR_USERS").split(","))


def show_toolbar(request):
    return request.user and request.user.username in SHOW_DEBUG_TOOLBAR_USERS


DEBUG_TOOLBAR_PATCH_SETTINGS = False
DEBUG_TOOLBAR_CONFIG = {
    "SHOW_TOOLBAR_CALLBACK": "conf.settings.base.show_toolbar",
}

# settings for Sentry
if USE_SENTRY:
    sentry_sdk.init(
        dsn=SENTRY_DSN_CONFIG_KEY,
        integrations=[DjangoIntegration(), CeleryIntegration()],
    )

UMAP_ALLOW_ANONYMOUS = False
UMAP_EXTRA_URLS = {
    "routing": "http://www.openstreetmap.org/directions?engine=osrm_car&route={lat},{lng}&locale={locale}#map={zoom}/{lat}/{lng}",
    # noqa
    "ajax_proxy": "/ajax-proxy/?url={url}&ttl={ttl}",
}
UMAP_KEEP_VERSIONS = 10
SITE_URL = "http://umap.org"
SITE_NAME = "uMap"
UMAP_DEMO_SITE = False
UMAP_EXCLUDE_DEFAULT_MAPS = False
UMAP_MAPS_PER_PAGE = 5
UMAP_MAPS_PER_PAGE_OWNER = 10
UMAP_USE_UNACCENT = False
UMAP_FEEDBACK_LINK = "https://wiki.openstreetmap.org/wiki/UMap#Feedback_and_help"
USER_MAPS_URL = "user_maps"
UMAP_READONLY = False
USER_URL_FIELD = "pk"
FROM_EMAIL = None
UMAP_DEFAULT_EDIT_STATUS = 3  # owner only
UMAP_DEFAULT_SHARE_STATUS = 2  # anyone with link
UMAP_GZIP = True

CAMTRAP_PACKAGE_SCHEMAS_VERSION = "1.0-rc.1"
CAMTRAP_PACKAGE_SCHEMAS_MEDIA = f"https://raw.githubusercontent.com/tdwg/camtrap-dp/{CAMTRAP_PACKAGE_SCHEMAS_VERSION}/media-table-schema.json"
CAMTRAP_PACKAGE_SCHEMAS_DEPLOYMENTS = f"https://raw.githubusercontent.com/tdwg/camtrap-dp/{CAMTRAP_PACKAGE_SCHEMAS_VERSION}/deployments-table-schema.json"
CAMTRAP_PACKAGE_SCHEMAS_OBSERVATIONS = f"https://raw.githubusercontent.com/tdwg/camtrap-dp/{CAMTRAP_PACKAGE_SCHEMAS_VERSION}/observations-table-schema.json"

TAXONOMIC_REFERENCE = "https://www.catalogueoflife.org"

# TAXONOMY - used to import taxa from Catalogue of Life
TAXA_URL = "https://gitlab.com/trapper-project/trapper-taxonomy/-/raw/main/data/{}.csv"

# REMOTE/CLOUD STORAGES CONFIGURATION

# DEFAULT_FILE_STORAGE_ENGINE is deprecated and will be removed in Django 5.1 to be replaced by STORAGES
DEFAULT_FILE_STORAGE_ENGINE = os.environ.get("DEFAULT_FILE_STORAGE")
DEFAULT_FILE_STORAGE = "trapper.apps.storage.cloud_storages.TrapperMediaStorage"
MEDIA_LOCATION = os.environ.get("MEDIA_LOCATION") or "media"

# 1) AZURE
if get_env_value("DEFAULT_FILE_STORAGE") == "azure":
    REMOTE_STORAGE = True
    AZURE_ACCOUNT_NAME = get_env_value("AZURE_ACCOUNT_NAME")
    AZURE_ACCOUNT_KEY = get_env_value("AZURE_ACCOUNT_KEY")
    AZURE_CONTAINER = get_env_value("AZURE_CONTAINER")
    AZURE_CUSTOM_DOMAIN = get_env_value("AZURE_CUSTOM_DOMAIN")

    # Those variables are needed for Azurite support
    AZURE_SSL = str2bool(os.environ.get("AZURE_SSL", "true"))
    AZURE_EMULATED_MODE = str2bool(os.environ.get("AZURE_EMULATED_MODE", "false"))

# 2) AMAZON S3, OVH
if get_env_value("DEFAULT_FILE_STORAGE") in ["amazon_s3", "ovh"]:
    REMOTE_STORAGE = True
    AWS_STORAGE_BUCKET_NAME = get_env_value("AWS_STORAGE_BUCKET_NAME")
    AWS_ACCESS_KEY_ID = get_env_value("AWS_ACCESS_KEY_ID")
    AWS_SECRET_ACCESS_KEY = get_env_value("AWS_SECRET_ACCESS_KEY")

    AWS_DEFAULT_ACL = "private"
    AWS_S3_SIGNATURE_VERSION = os.environ.get("AWS_S3_SIGNATURE_VERSION", "s3v4")

    AWS_S3_ENDPOINT_URL = get_env_value("AWS_S3_ENDPOINT_URL") or None
    AWS_S3_REGION_NAME = os.environ.get("AWS_S3_REGION_NAME") or None

DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

# BLUR RADIUS USED BY ProjectCollectionBlurHumans
BLUR_RADIUS = os.environ.get("BLUR_RADIUS") or 5

# requests verify SSL flag
REQUESTS_VERIFY_SSL = True
