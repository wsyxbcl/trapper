.. _configuration:

=============
Configuration
=============

You can override many default settings in TRAPPER by adjusting the environmental variables listed
in the file `.env`. Further in this section we provide some examples of how to customize selected
aspects of TRAPPER.

Basic settings
++++++++++++++

Provide a name of your TRAPPER instance domain:

.. code-block:: bash

   DOMAIN_NAME=demo.trapper-project.org

The ``SECRET_KEY`` is used to provide cryptographic signing, and should be set to a unique, unpredictable value
(see `django docs <https://docs.djangoproject.com/en/3.0/ref/settings/#secret-key>`_):

.. code-block:: bash
		
   SECRET_KEY='cudv86xkzva)o1lkt-ow72)y__*xwsb1@43wtp1cc@zak=i@@e'

Define users that are admins and managers of your TRAPPER instance. If the :ref:`configuration-email` is properly
configured, admins will be notified about all server errors (500) and managers will be sent notifications about new
user registrations and requests for approving new research projects. You can add 2 admins and 2 managers by providing
username and email as shown below.

.. code-block:: bash
		
   ADMIN1=admin1,admin1@oscf.org # {username},{email}
   ADMIN2=admin2,admin2@oscf.org

   MANAGER1=manager1,manager1@oscf.org
   MANAGER2=manager2,manager2@oscf.org

HTTP & HTTPS ports mapping definition is used to configure the `trapper-nginx <https://gitlab.com/trapper-project/trapper-nginx>`_ docker container
(see `nginx docker docs <https://hub.docker.com/_/nginx>`_):

.. code-block:: bash
		
   HTTP_PORTS=80:80
   HTTPS_PORTS=443:443


.. _configuration-ssl-certificates:

SSL certificates
++++++++++++++++

If you already have the SSL certificates for your TRAPPER website you can use them by setting the
following variables:

.. code-block:: bash

   # default configuration
   SSL_CERTIFICATE=${PWD}/ssl/cert.pem
   SSL_CERTIFICATE_KEY=${PWD}/ssl/key.pem

The example with the `Let's Encrypt <https://letsencrypt.org>`_ certificates:
   
.. code-block:: bash
   
   SSL_CERTIFICATE=/etc/letsencrypt/live/trapper/fullchain.pem
   SSL_CERTIFICATE_KEY=/etc/letsencrypt/live/trapper/privkey.pem

These certificates will be used to configure the HTTPS nginx proxy-server within a dedicated container
(see `trapper-nginx <https://gitlab.com/trapper-project/trapper-nginx>`_) and to secure the FTP connections (TLS)
to the `pure-ftpd <https://github.com/stilliard/docker-pure-ftpd>`_ docker container.
   
.. note::
   When there is no SSL certificates provided they will be generated automatically using the
   `openssl <https://www.openssl.org/>`_ toolkit (self-signed certificates).

.. _configuration-database:

Using an external database
++++++++++++++++++++++++++

TRAPPER comes with a pre-configured postgresql docker image (see `trapper-posgresql <https://gitlab.com/trapper-project/trapper-postgresql>`_)
and has been proved to work well with the recent `postgresql-12 <https://www.postgresql.org/docs/12/index.html>`_ and
`postgis-3.0 <https://postgis.net/docs/manual-3.0/>`_. The basic credentials needed to access this dockerized postgresql database
volume can be configured with these parameters:

.. code-block:: bash
   
   POSTGRES_USER=trapper
   POSTGRES_PASSWORD=trapper
   POSTGRES_DB=trapper

However, for a production-grade TRAPPER instance we recommend to configure an external postgresql database either running on the
same virtual machine or on different remote server. In the `.env` file there are the following variables that can be used to
configure a connection with an external database:     

.. code-block:: bash

   DB_NAME=trapper
   DB_USER=trapper
   DB_PASS=trapper
   DB_HOST=172.17.0.1
   DB_PORT=5432

.. note::
   The host address ``172.17.0.1`` is for the case when the postgresql database is running on the same machine as TRAPPER docker
   container. This host ip should work for most of the standard Linux distributions, including the recent Ubuntu Server LTS 20.04. 

Now you can run TRAPPER using the script `start.sh` and the extra flag `-d`:

.. code-block:: bash
		     
   $ ./start.sh -pbd prod

.. note::
   You will still need to properly configure your external postgresql server which is out of the scope of this documentation.
   Here we only give you a couple of quick cues to help you make the connection between TRAPPER and postgresql working
   as expected (but be aware that likely you need more restrictive configuration!):
   
   1) In the file ``/etc/postgresql/12/main/postgresql.conf`` update the following line:
      
      .. code-block:: bash

	 listen_addresses = '*'

   2) In the file ``/etc/postgresql/12/main/pg_hba.conf`` add the following entry:
      
      .. code-block:: bash

	 # TYPE   DATABASE   USER   ADDRESS       METHOD
	 host     all        all    172.0.0.0/8   md5

      172.0.0.0/8 matches all ip addresses starting with 172.X.X.X


Storage
+++++++

The multimedia files uploaded to TRAPPER (i.e. camera trapping images & videos) can be stored either locally, i.e. using a storage
infrastructure available at a virtual machine running TRAPPER docker container, or remotely in a cloud storage (see the subsection
:ref:`configuration-storage-cloud` for details about supported cloud storage providers).     

To use a local storage (default option) leave the following parameter empty:

.. code-block:: bash

   DEFAULT_FILE_STORAGE=

Optionally, you can provide paths to local directories which will be used to mount the corresponding docker volumes:

.. code-block:: bash
   
   VOL_MEDIA=/storage/trapper_data/media
   VOL_EXTERNAL_MEDIA=/storage/trapper_data/external_media

.. note::
   In a directory mounted as the volume ``VOL_MEDIA`` all images & videos uploaded to TRAPPER are stored in the following
   format:

   .. code-block:: bash

      # original files
      protected/storage/resources/{user_id}/{date_uploaded}/

      # preview files (for images)
      protected/storage/resources/{user_id}/{date_uploaded}/previews/

      # thumbnails (for both images & videos)
      protected/storage/resources/{user_id}/{date_uploaded}/thumbnails/

   The volume ``VOL_EXTERNAL_MEDIA`` provides a storage for users' data including FTP data (e.g. users' uploaded ``.zip``
   files with multimedia collections) and all data packages exported from TRAPPER. 

.. _configuration-storage-cloud:

Cloud storage
=============

The TRAPPER support for a cloud storage is based on a wonderful django package `django-storages <https://github.com/jschneier/django-storages>`_.
Here is the list of cloud storage providers currently supported by TRAPPER and their corresponding configuration parameters:

1) Azure

   .. code-block:: bash

      DEFAULT_FILE_STORAGE=azure

      AZURE_ACCOUNT_NAME=trapper
      AZURE_ACCOUNT_KEY=key
      AZURE_CONTAINER=trapper_data

      AZURE_CUSTOM_DOMAIN=trapper.blob.core.windows.net
      MEDIA_LOCATION=media

2) Amazon S3

`AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` - credentials for authorizing access to S3 storage.

`AWS_STORAGE_BUCKET_NAME` - S3 bucket name for storing all media files

`AWS_S3_REGION_NAME` - Name of AWS S3 region to use. Optional.

`AWS_S3_ENDPOINT_URL` - S3 Endpoint, required in order to use alternative S3-compatible storages such as Minio or Backblaze B2

Setting default file storage as `amazon_s3` is required in order to use S3 integration in TRAPPER

   .. code-block:: bash

    DEFAULT_FILE_STORAGE=amazon_s3

.. _configuration-email:

Email service
+++++++++++++

The working email service is required to make the notification framework working correctly. This feature is turned off
by default. To configure the email notification backend you should set the following parameters (example with gmail): 

.. code-block:: bash
		
   EMAIL_NOTIFICATIONS=True
   EMAIL_NOTIFICATIONS_RESEARCH_PROJECT=True
   EMAIL_HOST=smtp.gmail.com
   EMAIL_PORT=587
   EMAIL_HOST_USER=project@gmail.com
   EMAIL_HOST_PASSWORD=password
   EMAIL_USE_TLS=True

.. note::
   If local SMTP server is used it has to be configured to be able to send
   email messages. Configuration of the SMTP server is out of the scope of this
   documentation.


Users registration
++++++++++++++++++

Here you can define if users registration (signing-up) is open or not and optionally secure a user registration form with
Google's recaptcha authentication.

.. code-block:: bash

   USER_REGISTRATION_OPEN=True
		
   # Google recaptcha settings
   USE_RECAPTCHA=True
   RECAPTCHA_PUBLIC_KEY=public_key
   RECAPTCHA_PRIVATE_KEY=private_key


Django Debug Toolbar
++++++++++++++++++++

If you are a developer of TRAPPER you can turn on a very cool tool for debugging called
`Django Debug Toolbar <https://github.com/jazzband/django-debug-toolbar>`_:

.. code-block:: bash

   USE_DEBUG_TOOLBAR=True
   DEBUG_TOOLBAR_USERS=admin1,admin2


Advanced system-level variables
+++++++++++++++++++++++++++++++

These variables are set automatically when starting TRAPPER with the script ``start.sh``. You can override them, but please keep
defaults when you are not really sure what you can change here.

1) Set UID & GID for a user that will be used to run TRAPPER `Gunicorn <https://gunicorn.org/>`_ server within a docker container
   (by default these values are taken from a user starting docker containers using the ``start.sh`` script). The example custom
   configuration:

   .. code-block:: bash

      TRAPPER_USER_UID=1000
      TRAPPER_USER_GID=1000

2) Set GID for a docker group which will  be created within a TRAPPER docker container. The example custom configuration:
   
   .. code-block:: bash

      DOCKER_GID=131
   
