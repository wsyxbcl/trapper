.. _tutorial:

========
Tutorial
========

`by Tim R. Hofmeester (Swedish University of Agricultural Sciences, Department of Wildlife, Fish and Environmental Studies)`

`adapted by Karolina Kuczkowska and Jakub Bubnicki (Open Science Conservation Fund)`

The guides below describe how to use TRAPPER core functionalities.

The first manual describes the process of uploading your data to the Trapper server.
To start classifying *Resources*, you need to make a *Classification project* and a *Classificator*,
as described in a second manual, and a third one for classifying *Resources* once you have a running
*Classification project*.


Opening and logging in to TRAPPER
+++++++++++++++++++++++++++++++++

Go to `demo instance <https://demo.trapper-project.org/>`_ and register an account at the TRAPPER server.
After your account is activated by admin, you can login on the same page using your email address and
password.

How to upload data into TRAPPER
+++++++++++++++++++++++++++++++

1. Create a research project
============================

In the top menu, select *Research > Add research project*.

.. image:: images/tutorial/top_menu.png
    :alt: top menu with Add Research project option shown

You now get a menu with different options to fill in:

.. image:: images/tutorial/add_research_project_form.png
    :alt: add research project form

Fill in all information for your research project and click *Add research project*. Make sure
you choose an acronym that is more than 3 characters long, which will make it easier to find
it with the auto-fill function in Trapper.

.. note::
    Right after adding your research project you will see the notification that your project
    has been successfully added but needs to be activated by an admin. In practice, it means that
    your project request has been placed in the Admin dashboard, and has to be manually
    approved by an admin. This is one of the security measures and ways to control the amount
    and content of the projects in your institution/team. If you are an admin yourself you can go
    to the Admin dashboard and approve the project.

As soon as an Admin approves your new project, it will appear in your *Research* view in a
*Research projects* table presenting the project Name, Acronym, Keywords, and Actions. In
*Research projects* all the projects, you have access to, are listed. You can filter your projects
according to the content and/or the role you play in them.

.. _tutorial-add-locations:

2. Add locations to TRAPPER
===========================

Once you have created a *Research project*, you can add *Locations* to your project. There are
two ways to do this in TRAPPER: You can add locations manually, or you can upload a file
with location data.

To manually add locations, select *Map* > *Locations* > *Add location*.

.. image:: images/tutorial/top_menu_add_location.png
    :alt: top menu with add locations button shown

You now go to the map view where you can manually add information for a camera trap
location.

.. image:: images/tutorial/add_location_map.png
    :alt: map view for adding a single location

Most of the time, you want to add multiple locations at once, which can be done using a .csv
or a .gpx file with location information. It is easiest with a .csv file, which should be comma-
separated and should contain the following columns:

- ``locationID``: the ID/name of the location
- ``longitude``: the longitude of the location in WSG84 decimal degrees (e.g., 19.58536776)
- ``latitude``: the latitude of the location in WSG84 decimal degrees (e.g., 63.55417296)

Once you made the file, you can add the locations to TRAPPER using *Map* > *Locations* >
*Import locations*.

.. image:: images/tutorial/import_locations_form.png
    :alt: import locations form

Select the .csv file, make sure you select the right Timezone (e.g., Europe/Stockholm) and
select the *Research project* to which you want to add the locations. Once you have done this,
you can select *Upload* to add the *Locations* to your *Research project*.

3. Set up Trapper-Client
========================

The developers of TRAPPER provide a standalone software package to deal with the
different steps of uploading data called ‘Trapper Client’. Trapper Client can also be used to
create the correct file to upload *Deployments* based on the camera trap data. Trapper Client is
available for Windows and Linux from the GitLab of the Open Science Conservation Fund
(the foundation behind Trapper): `<https://gitlab.com/oscf/trapper-client-bin>`_.

The download consists of a .zip file that you need to extract anywhere on your computer
where you have writing rights. Make sure you download the latest version and extract the complete
.zip file in one location and keep the folder structure to ensure that the application works.

After unzipping, you can start the Trapper Client by double clicking the TrapperClient.bat
file. First go to the ‘Settings’ page. Here you can add the credentials of your Trapper account.

.. image:: images/tutorial/trapper_client_settings.png
    :alt: trapper client settings

.. note::
    Make sure you add ``http(s)://`` before the URL of the Trapper server and have your full
    email as login.

Make sure you add the acronym of the research project you want to upload data to in the
‘Project’ field. You can test if you have the right acronym by clicking ‘Check & set as
active’. You can also check if the Trapper Client can connect to the server by clicking
‘Connect to Trapper’.

As the last step make sure you have an FTP account connected to your Trapper account (ask
the admin of your Trapper server for this).

In order to upload data to an external FTP server linked to your TRAPPER instance, to switch
between passive and active FTP mode or to modify the list of supported image and video extensions,
press F1 to see the advanced settings. 

.. image:: images/tutorial/trapper_client_advanced.png
    :alt: trapper client advanced settings

4. Add deployments to TRAPPER
=============================

Now that you have added your locations to trapper, you can start adding *Deployments*. The
main differences between Locations and Deployments is that *Locations* only specify the
coordinates at which a camera trap was located, while *Deployments* also specify the start and
end date and time of a camera trap in the field. A *Location* can have several *Deployments*.
Usually, a *Deployment* is determined as one session of putting out the camera and picking it
up, or a session between changing cards and batteries.

Again it is possible to manually add *Deployments*, but this is rather error prone. Therefore, in
this manual I will only deal with adding *Deployments* based on the camera trap data itself.
This will make sure that deployment times are correct for all *Deployments*, also the ones with
cameras that stopped working before they were picked up.

You can create a .csv file with your *Deployment* information in Trapper Client. For this to
work, the data has to be ordered in the correct way: A main directory which you can select in
the client, with sub-directories for each *Collection*. A *Collection* can for example be one
round of *Deployments*. Within each *Collection*, there are further sub-directories for each
*Deployment*. These sub-directories should be labelled in congruence with other information
provided to TRAPPER. The *Deployment* label is a combination of a unique code and the
*Location* label. For example, you can use a unique code ``R1`` (for round 1) for the first
*Deployment* at a certain *Location* e.g. ``32_01``. The Deployment label then becomes:
``R1-32_01``.

Go to the ‘Package’ tab in your Trapper Client. Select the main directory as described above
in the ‘Media root’ box. Also, select a folder where you want the .csv file with the
*Deployment* table to be stored as ‘Output path’. Now select the *Collection* for which you want
to create the *Deployment* table and select the file type for the images (for Reconyx cameras
this is .jpg). You can then click ``Get DT`` to create the *Deployment* table.

.. image:: images/tutorial/trapper_client_package.png
    :alt: exporting data package in trapper client

You can now rename the .csv file (as the standard output doesn’t include the collection name
which means that it will be overwritten if you make a new *Deployment* table) and upload the
.csv to TRAPPER by selecting *Map* > *Deployments* > *Import deployments*.

.. image:: images/tutorial/import_deployments.png
    :alt: import deployments form

.. note::
    The automatically generated *Deployment* table contains only a minimum set of metadata fields
    needed to import deployments into TRAPPER. See https://tdwg.github.io/camtrap-dp/data/#deployments
    for a full list of supported metadata fields that can be imported into TRAPPER.

.. note::
    Please note that you if your *Deployment* table contains ``longitude`` and ``latitude`` fields,
    location objects associated with imported deployments can be created automatically and you can
    skip the previous step of importing the *Location* table (:ref:`tutorial-add-locations`).
	  
Select the previously generated .csv file and the *Research project* to which the round of
*Deployments* belongs and click *Submit*.

5. Add resources to TRAPPER
===========================

Once the *Deployments* are added, the *Resources* (e.g. images) made by the camera traps can be
uploaded into TRAPPER. This process consists of three steps:

1. Create a data package
2. Upload the data package via FTP
3. Load the data into Trapper

**Create a data package**

Go to the ‘Package’ tab in your Trapper Client. Select the main directory as described above
in the ‘Media root’ box. Also, select a folder where you want the .csv file with the
*Deployment* table to be stored as ‘Output path’. Now select the *Collection* for which you want
to create the *Deployment* table and select the file type for the images (for Reconyx cameras
this is .jpg).

.. note::
    These steps are the same as for creating your Deployment table.

Now click ‘Validate’. This will check on the Trapper server if the deployments you want to
add have been added as deployments in the previous step. If this is successful, you can create
the data package by clicking ‘Run’. This will create two files, a .zip file and a .yaml file. The
.zip file contains all your data, while the .yaml file contains the meta-data (it’s a package
definition file).

**Upload the data package via FTP**

Once you have generated the files, you can upload them to the Trapper server. Make sure
your settings are correct (see above). Then go to the ‘Upload’ tab. Here you have to select
both the .zip and the .yaml file that you just created. You upload them by clicking on ‘Upload
(new)’.

.. note::
    You can also upload your files to FTP server using other tools, for example via command line with
    `ftp` command, using your trapper account credentials to login.

.. image:: images/tutorial/trapper_client_upload_collection.png
    :alt: upload collection view in trapper client

**Load the data into Trapper**

Once you have uploaded the files go to *Storage* > *Upload collection* on your Trapper server.
Here you can select the .yaml file that you just uploaded and click ‘Upload’.

.. note::
    You can also upload the file through this interface, but it is recommended to do it
    through FTP.

.. image:: images/tutorial/upload_collection_1.png
    :alt: upload collection form step 1

You then need to select the .zip file you just uploaded in the second window. Alternatively,
you can upload the .zip file here, but again it is recommended to do that via FTP and Trapper
Client. Click ‘Upload’ to load you data package into Trapper.

.. image:: images/tutorial/upload_collection_2.png
    :alt: upload collection form step 2

You have now created a *Collection* containing the *Resources* of all the *Deployments* that you
used while creating the *Collection*. This *Collection* is added to the correct *Research project*
automatically. You can browse the *Resources* in the *Collection* by going to *Storage* >
*Resources (available)*.


How to create a Classificator and Classification project in TRAPPER
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

1. Create a Classificator
=========================

In TRAPPER, images are classified in *Classification projects* using a *Classificator*. One of
the great flexibilities of TRAPPER is that researchers or students can build their own
*Classificator* and *Classification project* depending on the research question. A *Classificator*
basically describes a set of attributes that is used while annotating an image or sequence of
images.

In the top menu, select *Classification* > *Add classificator*. First there are some required
classificator properties that you have to fill in, such as name and template. Furthermore there
are several attributes that have become mandatory in the latest version of Trapper, which are
species, observation type, count and ‘is_setup’. The ‘is_setup’ and observation type were
added to make it easier to include a first screening of images by Microsofts MegaDetector, a
Machine Learning Algorithm that automatically identifies empty images from images
containing a human, an animal or a vehicle. Also, you can add species you want to classify
based on entries in the species list.

.. note::
    Admins can add species to the overall species list in the Admin panel under `Species`
    either a single species at the time or using a .csv file, or download taxa for entire classes
    from Catalogue of Life.

.. image:: images/tutorial/classificator_form_1.png
    :alt: classificator form part 1

Further down on the page you see a lot of different options, which I will explain step by step.

There are two types of attributes: static and dynamic attributes. Static attributes are attributes
that are only given once for each resource, e.g., ‘is setup’. Dynamic attributes are attributes
that can be given multiple times for one resource, e.g., the species or sex of an animal
recorded by the camera trap.

There are several pre-defined attributes available to quickly add to your *Classificator* by
checking the boxes.

.. image:: images/tutorial/classificator_form_2.png
    :alt: classificator form part 2

Then you can also add custom attributes in the custom attribute form. This form gives a lot of
freedom to determine your own attributes. Below you find the attributes of an example
*Classificator*.

.. image:: images/tutorial/example_classificator.png
    :alt: finished example classificator

You can check all the *Classificators* on your TRAPPER server by going to *Classification* >
*Classificators*.

2. Create a Classification project
==================================

Once you have created your *Classificator*, you can make a *Classification project* to start
classifying your resources. You can make a new *Classification project* by going to
*Classification* > *Add classification* project.

.. image:: images/tutorial/classification_project_form.png
    :alt: add classification project form

Here you can specify the name of the *Classification project*, to which *Research project* the
*Classification project* belongs, which *Classificator* and default *AIProvider* to use and which
users are involved in the *Classification project*. When you are finished filling in all details,
click *Add classification project*. You have now created a *Classification project*.

You can check all the Classification projects to which you have access by going to
*Classification* > *Classification projects*.

.. image:: images/tutorial/classification_projects_list.png
    :alt: classification projects list

3. Add Resources to a Classification project
===============================================

In order to be able to classify *Resources*, you need to add them to your *Classification project*.
To do this, first go to your *Research project* by going to *Research* > *Research project* and
clicking on the magnifying glass next to your research project.

Now you see a selection of all *Collections* that are linked to the *Research project*.

.. image:: images/tutorial/research_project_collections.png
    :alt: research project collections list

Select a *Collection* by clicking on it and under *Actions* > *More* select ‘Add to Classification
project’.

You now have *Resources* in your *Classification project* that can be classified.


How to classify images in TRAPPER
+++++++++++++++++++++++++++++++++

1. Go to the classification page
================================

In the top menu, select *Classification* > *Classification projects*. You are now at the start
screen of all classification projects that you have access to. You will see information on each
*Classification project*, including its name, the *Research project* it belongs to, its status and
*Actions*.

.. image:: images/tutorial/classification_projects_list_2.png
    :alt: classification projects list

To select the *Classification project* you want to work in, select the magnifying glass
underneath *Actions*.

.. image:: images/tutorial/classification_project_details.png
    :alt: classification projects details

You are now in the start screen of the selected *Classification project*. Here you see more
information on the *Classification project*, including which *Classificator* is used, which
*Collections* are in the project, and to which *Research project* the *Classification project*
belongs. Furthermore, you can see how many of the *Resources* have been Classified and
Approved.

Go to the bottom of the page, where you can find the different *Collections* in the
*Classification project* (for example the different rounds during which cameras where placed
out in the field). Type the name of the *Deployment* within a *Collection* that you would like to
classify and select it. This makes sure that you are classifying a specific *Deployment*, which
makes it easier to keep an overview during the classification. Once you have selected the
*Deployment*, select the eye symbol to start the classification.

.. note::
    Make sure you generate sequences for your collection by selecting the collection and
    clicking ‘(Re)build sequences’. You can specify a time interval between sequences. The
    default is 5, regular use is 15 minutes.

2. Classify a sequence using the initial classification forms
=============================================================

You will now see a screen such as the one below. You see the different *Resources* (images)
with an orange number. This is the *Sequence* number.

.. image:: images/tutorial/classify_sequences.png
    :alt: list of resources in sequences to classify

.. note::
    To circumvent problems of two people classifying the same Deployment, only start
    classifying a Deployment if none of the Sequences have been classified yet. You can see if
    Resource has been classified by a green mark (note 3 Resources - one Sequence - classified
    and others not classified in the screen cap above).
    At the same time, make sure that you classify all Sequences in a Deployment (keep track of which
    Deployment you are classifying). If you cannot finish a deployment, contact your admin.

You can now enlarge pictures by clicking on the thumbnail. You can scroll through the
enlarged pictures with you arrow keys. It’s always good to check the whole sequence before
classifying it to check you didn't miss any individuals in the thumbnails.

.. image:: images/tutorial/resource_enlarged.png
    :alt: single resource view

When you have decided how many individuals and which species is visible in the *Sequence*,
it is time to classify the *Sequence*. To do this, click on the sequence number. This will select
all images in the sequence. You can now click ‘Classify’ in the top of the screen to classify
the whole sequence. **Make sure to check for empty images at the end of the sequence as it is**
**good to classify those separately as ‘observation_type = blank’.**

In the classification view, fill in the right information for the sequence. You classify ‘is_setup’ as
`True` for setup and test images, you can then delete all dynamic attributes by clicking on
the red cross.

.. note::
    Classify **empty resources** as `Observation type = Blank`.


Otherwise, keep the default `False` and add the dynamic attribute information.

.. image:: images/tutorial/classify_modal_form.png
    :alt: modal with classification form

Dynamic attributes are attributes that can be given multiple times for one sequence. In this
case, it gives information about the type of observation, the species of animal, if the
observation contains one or more animals, sex, age and behaviour of the animals,
and optional custom str value (as specified in the *Classifier*) and individual id.

In the standard classification form there are three required attributes:

1. Observation type: the type of observation (Human/Vehicle/Animal/Blank/Unknown/Unclassified)

2. Species: The animal species shown in the image (to be selected from *Species* from *Classificator*)

3. Count: The number of animals/humans/vehicles, default is 1

There is also several attributes that can be assigned, as described in *Classificator* adding manual.

If there are multiple animals in the field with different attributes, you can add a row by
clicking the green button with a plus sign to specify the attributes of all animals in the
sequence.

Click ‘Classify’ to finish the classification.

The *Sequence* is now classified and you can go to the next *Sequence( by clicking on the
orange sequence number. Follow the steps above until you have classified all
resources in a *Deployment*.

.. note::
    **FOR GROUPS OF ANIMALS**: If there is one image in which all animals in the group
    are present, please classify the sequence per picture. In this way we can estimate retention
    time of individuals (especially with groups <5). With big groups, or groups were animals are
    never all in the picture, classify the whole group in one sequence. This ensures that we have
    the right number of animals passing in the analysis by just selecting the maximum per
    sequence. Again, make sure that empty pictures that are part of the sequence are classified as
    empty.

.. note::
    If a person is in the frame with a dog, add **two observations**: one with observation_type = human
    and species `Homo sapiens` and the other with observation_type = animal and species `Canis familiaris`.

3. Adding bounding boxes for training dataset for Machine Learning
==================================================================

A new function in Trapper is that we can now add bounding boxes to each classification.
These delimit where the object (an animal, human or vehicle) is and will aid in the training of
Machine Learning algorithms for automatic species identification. To add bounding boxes to
your classification, you need to go to the ‘classic’ classification view by clicking on ‘Details’
underneath a thumbnail.

.. image:: images/tutorial/bboxes1.png
    :alt: classify resource detail view

You can now add a bounding box by clicking on the green hand symbol on the user
classification. This will lead you to the new bounding box view.

.. image:: images/tutorial/bboxes2.png
    :alt: draw bboxes view

Here you can draw a box around the object. Please do this as tight around the animal as
possible. You can then specify which dynamic attribute field should be linked to this
bounding box.

.. image:: images/tutorial/bboxes3.png
    :alt: draw bboxes view with bboxes drawn

Click ‘Save’ to add the bounding box to the classification. You can now also see it in the
‘classic’ classification screen.

.. image:: images/tutorial/bboxes4.png
    :alt: classify resource detail view with bboxes drawn

Timezone behaviour in TRAPPER
+++++++++++++++++++++++++++++

The default timezone for TRAPPER app is **Europe/Warsaw**. You can set your working timezone in your User Profile
settings tab (click your username in top right corner to access this view).

.. image:: images/tutorial/user_profile.png
    :alt: user profile settings view

Your working timezone will be used:

- to correctly display system notifications (e.g. asynchronous task completion times on your dashboard),
- as a default timezone value in forms.

Daylight Savings Time
=====================

While not recommended, it is possible to bypass default behaviour regarding DST (Daylight Saving Time).

If your camera trap at a given location *does not use* DST (i.e. does not move its timer one hour forward in the
spring and one hour back in the fall), you should mark its ``ignore_DST`` flag ``True``.
With that setting, all timestamps in recorded metadata from that location (``Deployment.start_date``, ``Deployment.end_date`` and
``Resource.date_recorded``) will be processed ignoring DST (it would be considered in processing timestamps otherwise).

.. note::
    Whenever this documentation mentions two *Locations* having **the same timezone specification**, it means both the
    ``timezone`` field and the ``ignore_DST`` flag having the same value, as only both values together correctly describe the logic of processing timestamps (i.e. datetime objects) in TRAPPER.

Timezone is set for Location
============================

*Location* objects have a *timezone* field and an ``ignore_DST`` flag. You have to specify them in the form when adding location in one of the following ways:

- using the map view with the form for adding a single location,
- importing locations from CSV or GPX file (bulk import),
- creating locations automatically using deployments bulk import.

It is very important to correctly set timezone information for every *Location*, because this data is used to process timestamps in all *Deployments* and *Resources* connected to given *Location*.

Timezone behaviour during CSV import
====================================

When importing *Deployments* or *Locations* from CSV file, you have to provide their ``timezone`` and ``ignore_DST`` values in the import forms.

This means that you can only import locations from the same ``timezone`` and with the same ``ignore_DST`` flag value. Data that do not meet this condition has to be imported using separate CSV files.

When importing *Locations*, values from the form will be set for all imported objects.

When importing *Deployments*, they can be linked to different locations, but all locations need to have the same timezone specification. If locations are being created, their timezone info will be set to data from the form.

If *Deployments* are uploaded to existing *Locations*, the file will be validated to make sure all locations referenced in the data have the same timezone specification as in the form.

This also applies to uploading *Collections* - in `trapper-client <https://trapper-client.readthedocs.io/en/latest/overview.html>`_, *Collections* can only be
created from *Resources* linked to *Locations* with the same timezone specification. The timezone specification (as specified in the trapper-client settings screen) is provided in the YAML file describing given *Collection*. When uploading a *Collection*, the validation process is
performed to check that provided timezone  matches *Locations* in the database as well.
