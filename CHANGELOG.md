# Changelog
All notable changes to this project will be documented in this file.

## [1.4.0] - 31.01.2022

### Added
-

### Changed
-

## [1.3.1] - 20.03.2020

### Added
- Refactored code, scripts and settings
- New configuration of docker and docker-compose
- Gitlab-CI configuration with dependency check (security, compatibility, stale dependency), code check, tests and auto build image
- Refactored code to match pep8

### Changed
- Updated python version from 2.7 to 3.7
- Updated django to 1.11.29
- Updated dependencies to newer version compatible with django 1.11.29
- Changed .gitignore configuration


## [1.3.0] - 5.03.2020

## [1.2.0] - 23.04.2019

## [1.1.0] - 19.10.2018

## [1.0.1] - 13.04.2016

## [1.0-beta] - 3.02.2016
